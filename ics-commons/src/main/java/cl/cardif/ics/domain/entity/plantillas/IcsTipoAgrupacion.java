package cl.cardif.ics.domain.entity.plantillas;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the ICS_TIPO_AGRUPACION database table.
 * 
 */
@Entity
@Table(name = "ICS_TIPO_AGRUPACION")
@NamedQuery(name = "IcsTipoAgrupacion.findAll", query = "SELECT i FROM IcsTipoAgrupacion i")
public class IcsTipoAgrupacion {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_AGRUPACION")
	private long idAgrupacion;

	@Column(name = "NOMBRE_AGRUPACION")
	private String nombreAgrupacion;

	@Column(name = "VIGENCIA_AGRUPACION", columnDefinition = "CHAR")
	private String vigenciaAgrupacion;

	public IcsTipoAgrupacion() {
		super();
	}

	public long getIdAgrupacion() {
		return idAgrupacion;
	}

	public void setIdAgrupacion(long idAgrupacion) {
		this.idAgrupacion = idAgrupacion;
	}

	public String getNombreAgrupacion() {
		return nombreAgrupacion;
	}

	public void setNombreAgrupacion(String nombreAgrupacion) {
		this.nombreAgrupacion = nombreAgrupacion;
	}

	public String getVigenciaAgrupacion() {
		return vigenciaAgrupacion;
	}

	public void setVigenciaAgrupacion(String vigenciaAgrupacion) {
		this.vigenciaAgrupacion = vigenciaAgrupacion;
	}
}
