package cl.cardif.ics.domain.entity.plantillas;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * The persistent class for the ICS_ATRIBUTOS_SALIDA database table.
 * 
 */
@Entity
@Table(name = "ICS_ATRIBUTOS_SALIDA")
@NamedQuery(name = "IcsAtributosSalida.findAll", query = "SELECT i FROM IcsAtributosSalida i")
public class IcsAtributosSalida {
	@Id
	@Column(name = "ID_ATRIBUTO_SALIDA")
	private Long idAtributoSalida;

	@OneToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "ID_ATRIBUTO_NEGOCIO", insertable = false, updatable = false)
	private IcsAtributoNegocio icsAtributoNegocio;

	@OneToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "ID_ATRIBUTO_USUARIO", insertable = false, updatable = false)
	private IcsAtributoUsuario icsAtributoUsuario;

	@OneToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "ID_ATRIBUTO", insertable = false, updatable = false)
	private IcsAtributos icsAtributos;

	@Column(name = "ID_ARCHIVO")
	private Long idArchivo;

	public IcsAtributosSalida() {
		super();
	}

	public Long getIdAtributoSalida() {
		return idAtributoSalida;
	}

	public void setIdAtributoSalida(Long idAtributoSalida) {
		this.idAtributoSalida = idAtributoSalida;
	}

	public IcsAtributoNegocio getIcsAtributoNegocio() {
		return icsAtributoNegocio;
	}

	public void setIcsAtributoNegocio(IcsAtributoNegocio icsAtributoNegocio) {
		this.icsAtributoNegocio = icsAtributoNegocio;
	}

	public IcsAtributos getIcsAtributos() {
		return icsAtributos;
	}

	public void setIcsAtributos(IcsAtributos icsAtributos) {
		this.icsAtributos = icsAtributos;
	}

	public Long getIdArchivo() {
		return idArchivo;
	}

	public void setIdArchivo(Long idArchivo) {
		this.idArchivo = idArchivo;
	}

	public IcsAtributoUsuario getIcsAtributoUsuario() {
		return icsAtributoUsuario;
	}

	public void setIcsAtributoUsuario(IcsAtributoUsuario icsAtributoUsuario) {
		this.icsAtributoUsuario = icsAtributoUsuario;
	}
}
