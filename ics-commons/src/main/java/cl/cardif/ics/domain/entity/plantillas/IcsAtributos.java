package cl.cardif.ics.domain.entity.plantillas;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import cl.cardif.ics.domain.entity.common.IcsTipoDato;

/**
 * The persistent class for the ICS_ATRIBUTOS database table.
 * 
 */
@Entity
@Table(name = "ICS_ATRIBUTOS")
@NamedQuery(name = "IcsAtributos.findAll", query = "SELECT i FROM IcsAtributos i")
public class IcsAtributos implements Serializable {
	private static final long serialVersionUID = -3525990609652291090L;

	@Id
	@Column(name = "ID_ATRIBUTO")
	private Long idAtributo;

	@Column(name = "ID_ARCHIVO")
	private Long idArchivo;

	@Column(name = "NOMBRE_COLUMNA")
	private String nombreColumna;

	@Column(name = "NUMERO_COLUMNA")
	private Long numeroColumna;

	@Column(name = "POSICION_INICIAL", nullable = true)
	private Long posicionInicial;

	@Column(name = "POSICION_FINAL", nullable = true)
	private Long posicionFinal;

	@Column(name = "FLG_AGRUPACION", columnDefinition = "CHAR", nullable = true)
	private String flgAgrupacion;

	@Column(name = "FLG_CARGA", columnDefinition = "CHAR", nullable = true)
	private String flgCarga;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "TIPO_DATO")
	private IcsTipoDato icsTipoDato;
	
	public IcsAtributos() {
		super();
	}

	public Long getIdAtributo() {
		return idAtributo;
	}

	public void setIdAtributo(Long idAtributo) {
		this.idAtributo = idAtributo;
	}

	public Long getIdArchivo() {
		return idArchivo;
	}

	public void setIdArchivo(Long idArchivo) {
		this.idArchivo = idArchivo;
	}

	public String getNombreColumna() {
		return nombreColumna;
	}

	public void setNombreColumna(String nombreColumna) {
		this.nombreColumna = nombreColumna;
	}

	public Long getNumeroColumna() {
		return numeroColumna;
	}

	public void setNumeroColumna(Long numeroColumna) {
		this.numeroColumna = numeroColumna;
	}

	public Long getPosicionInicial() {
		return posicionInicial;
	}

	public void setPosicionInicial(Long posicionInicial) {
		this.posicionInicial = posicionInicial;
	}

	public Long getPosicionFinal() {
		return posicionFinal;
	}

	public void setPosicionFinal(Long posicionFinal) {
		this.posicionFinal = posicionFinal;
	}

	public String getFlgAgrupacion() {
		return flgAgrupacion;
	}

	public void setFlgAgrupacion(String flgAgrupacion) {
		this.flgAgrupacion = flgAgrupacion;
	}

	public String getFlgCarga() {
		return flgCarga;
	}

	public void setFlgCarga(String flgCarga) {
		this.flgCarga = flgCarga;
	}

	public IcsTipoDato getIcsTipoDato() {
		return icsTipoDato;
	}

	public void setIcsTipoDato(IcsTipoDato icsTipoDato) {
		this.icsTipoDato = icsTipoDato;
	}
}
