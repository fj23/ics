package cl.cardif.ics.domain.entity.perfilamiento;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the ICS_CORE database table.
 * 
 */
@Entity
@Table(name = "ICS_DETALLE_ROL")
@NamedQuery(name = "IcsDetalleRol.findAll", query = "SELECT i FROM IcsDetalleRol i")
public class IcsDetalleRol {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_DETALLE")
	private Long idDetalle;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_ROL")
	private IcsTipoRol icsTipoRol;

	@Column(name = "CODIGO")
	private Long codigo;

	@Column(name = "FECHA_CREACION")
	private Date fechaCreacion;

	@Column(name = "VIGENCIA", columnDefinition = "CHAR")
	private String vigencia;

	public Long getIdDetalle() {
		return idDetalle;
	}

	public void setIdDetalle(Long idDetalle) {
		this.idDetalle = idDetalle;
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public Date getFechaCreacion() {
		return (Date) fechaCreacion.clone();
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = (Date) fechaCreacion.clone();
	}

	public String getVigencia() {
		return vigencia;
	}

	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}

	public IcsTipoRol getIcsTipoRol() {
		return icsTipoRol;
	}

	public void setIcsTipoRol(IcsTipoRol icsTipoRol) {
		this.icsTipoRol = icsTipoRol;
	}
}
