package cl.cardif.ics.domain.entity.estructuras;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import cl.cardif.ics.domain.entity.common.IcsTipoDato;

/**
 * The persistent class for the ICS_DETALLE_SUBESTRUCTURA database table.
 * 
 */
@Entity
@Table(name = "ICS_DETALLE_SUBESTRUCTURA")
@NamedQuery(name = "IcsDetalleSubestructura.findAll", query = "SELECT i FROM IcsDetalleSubestructura i")
public class IcsDetalleSubestructura extends DetalleEstructura implements Serializable {
	private static final long serialVersionUID = -2193703659841444287L;

	@Id
	@SequenceGenerator(name = "DB_SEQ_DETALLE_SUBESTRUCTURA", sequenceName = "SEQ_ICS_DETALLE_SUBESTRUCTURA", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DB_SEQ_DETALLE_SUBESTRUCTURA")
	@Column(name = "ID_DETALLE_SUBEST")
	private long idDetalleSubest;

	@Column(name = "DESCRIPCION_DETALLE_SUBEST")
	private String descripcionDetalleSubestructura;

	@Column(name = "CODIGO_DETALLE_SUBESTRUCTURA")
	private String codigoDetalleSubestructura;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_TIPO_DATO")
	private IcsTipoDato icsTipoDato;

	@Column(name = "VIGENCIA", columnDefinition = "CHAR")
	private String vigenciaDetalleSubestructura;

	public IcsDetalleSubestructura() {
		super();
	}

	public long getIdDetalleSubestructura() {
		return idDetalleSubest;
	}

	public void setIdDetalleSubestructura(long idDetalleSubestructura) {
		this.idDetalleSubest = idDetalleSubestructura;
	}

	public String getDescripcionDetalleSubestructura() {
		return descripcionDetalleSubestructura;
	}

	public void setDescripcionDetalleSubestructura(String descripcionDetalleSubestructura) {
		this.descripcionDetalleSubestructura = descripcionDetalleSubestructura;
	}

	public String getCodigoDetalleSubestructura() {
		return codigoDetalleSubestructura;
	}

	public void setCodigoDetalleSubestructura(String codigoDetalleSubestructura) {
		this.codigoDetalleSubestructura = codigoDetalleSubestructura;
	}

	public IcsTipoDato getIcsTipoDato() {
		return icsTipoDato;
	}

	public void setIcsTipoDato(IcsTipoDato icsTipoDato) {
		this.icsTipoDato = icsTipoDato;
	}

	public String getVigenciaDetalleSubestructura() {
		return vigenciaDetalleSubestructura;
	}

	public void setVigenciaDetalleSubestructura(String vigenciaDetalleSubestructura) {
		this.vigenciaDetalleSubestructura = vigenciaDetalleSubestructura;
	}
}
