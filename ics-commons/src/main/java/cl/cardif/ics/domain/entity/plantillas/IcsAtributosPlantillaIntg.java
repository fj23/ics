package cl.cardif.ics.domain.entity.plantillas;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

/**
 * The persistent class for the ICS_ATRIBUTOS_PLANTILLA_INTG database table.
 * 
 */
@Entity
@Table(name = "ICS_ATRIBUTOS_PLANTILLA_INTG")
@NamedQuery(name = "IcsAtributosPlantillaIntg.findAll", query = "SELECT i FROM IcsAtributosPlantillaIntg i")
public class IcsAtributosPlantillaIntg implements Serializable {
	private static final long serialVersionUID = 3631330902777325234L;

	@Id
	@Column(name = "ID_ATRIBUTO")
	private Long idAtributo;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_PLANTILLA")
	private IcsPlantilla icsPlantilla;

	@Column(name = "NOMBRE_COLUMNA")
	private String nombreColumna;

	@Column(name = "NUMERO_COLUMNA")
	private BigDecimal numeroColumna;

	@Column(name = "ORDEN_COLUMNA_ARCHIVO", nullable = true)
	private BigDecimal ordenColumnaArchivo;

	@Column(name = "FORMULA_SQL", nullable = true)
	private String formulaSql;

	@Column(name = "FORMULA_JSON", nullable = true)
	private String formulaJson;

	@NotFound(action = NotFoundAction.IGNORE)
	@OneToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "ID_ATRIBUTO_INTEGRACION", nullable = false, insertable = false, updatable = false)
	private IcsAtributosIntg icsAtributosIntg;

	public IcsAtributosPlantillaIntg() {
		super();
	}
	
	public Long getIdAtributo() {
		return idAtributo;
	}

	public void setIdAtributo(Long idAtributo) {
		this.idAtributo = idAtributo;
	}

	public IcsPlantilla getIcsPlantilla() {
		return icsPlantilla;
	}

	public void setIcsPlantilla(IcsPlantilla icsPlantilla) {
		this.icsPlantilla = icsPlantilla;
	}

	public String getNombreColumna() {
		return nombreColumna;
	}

	public void setNombreColumna(String nombreColumna) {
		this.nombreColumna = nombreColumna;
	}

	public BigDecimal getNumeroColumna() {
		return numeroColumna;
	}

	public void setNumeroColumna(BigDecimal numeroColumna) {
		this.numeroColumna = numeroColumna;
	}

	public BigDecimal getOrdenColumnaArchivo() {
		return ordenColumnaArchivo;
	}

	public void setOrdenColumnaArchivo(BigDecimal ordenColumnaArchivo) {
		this.ordenColumnaArchivo = ordenColumnaArchivo;
	}

	public String getFormulaSql() {
		return formulaSql;
	}

	public void setFormulaSql(String formulaSql) {
		this.formulaSql = formulaSql;
	}

	public String getFormulaJson() {
		return formulaJson;
	}

	public void setFormulaJson(String formulaJson) {
		this.formulaJson = formulaJson;
	}

	public IcsAtributosIntg getIcsAtributosIntg() {
		return icsAtributosIntg;
	}

	public void setIcsAtributosIntg(IcsAtributosIntg icsAtributosIntg) {
		this.icsAtributosIntg = icsAtributosIntg;
	}
}
