package cl.cardif.ics.domain.entity.plantillas;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * The persistent class for the ICS_TIPO_PLANTILLA database table.
 * 
 */
@Entity
@Table(name = "ICS_TIPO_PLANTILLA")
@NamedQuery(name = "IcsTipoPlantilla.findAll", query = "SELECT i FROM IcsTipoPlantilla i")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class IcsTipoPlantilla implements Serializable {
	private static final long serialVersionUID = -4970776562054335608L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_TIPO_PLANTILLA")
	private long idTipoPlantilla;

	@Column(name = "NOMBRE_PLANTILLA")
	private String nombrePlantilla;

	@Column(name = "VIGENCIA_PLANTILLA", columnDefinition = "CHAR")
	private String vigenciaPlantilla;

	public long getIdTipoPlantilla() {
		return idTipoPlantilla;
	}

	public void setIdTipoPlantilla(long idTipoPlantilla) {
		this.idTipoPlantilla = idTipoPlantilla;
	}

	public String getNombrePlantilla() {
		return nombrePlantilla;
	}

	public void setNombrePlantilla(String nombrePlantilla) {
		this.nombrePlantilla = nombrePlantilla;
	}

	public String getVigenciaPlantilla() {
		return vigenciaPlantilla;
	}

	public void setVigenciaPlantilla(String vigenciaPlantilla) {
		this.vigenciaPlantilla = vigenciaPlantilla;
	}
}
