package cl.cardif.ics.domain.entity.homologaciones;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import cl.cardif.ics.domain.entity.common.IcsCore;
import cl.cardif.ics.domain.entity.common.IcsSocio;

/**
 * The persistent class for the ICS_TIPO_HOMOLOGACION database table.
 * 
 */
@Entity
@Table(name = "ICS_TIPO_HOMOLOGACION")
@NamedQuery(name = "IcsTipoHomologacion.findAll", query = "SELECT i FROM IcsTipoHomologacion i")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class IcsTipoHomologacion implements Serializable {
	private static final long serialVersionUID = 9168935298003767520L;

	@Id
	@SequenceGenerator(name = "DB_SEQ_TIPO_HOMOLOGACION", sequenceName = "SEQ_ICS_TIPO_HOMOLOGACION", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DB_SEQ_TIPO_HOMOLOGACION")
	@Column(name = "ID_DE_TIPO_HOMOLOGACION")
	private Long idDeTipoHomologacion;

	@Column(name = "NOMBRE_HOMOLOGACION")
	private String nombreHomologacion;

	@Column(name = "VIGENCIA_HOMOLOGACION", columnDefinition = "CHAR")
	private String vigenciaTipoHomologacion;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_SISTEMA1")
	private IcsCore icsCore1;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_SISTEMA2")
	private IcsCore icsCore2;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_EMPRESA1")
	private IcsSocio icsSocio1;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_EMPRESA2")
	private IcsSocio icsSocio2;

	public IcsTipoHomologacion() {
		super();
	}

	public Long getIdDeTipoHomologacion() {
		return idDeTipoHomologacion;
	}

	public void setIdDeTipoHomologacion(Long idDeTipoHomologacion) {
		this.idDeTipoHomologacion = idDeTipoHomologacion;
	}

	public String getNombreHomologacion() {
		return nombreHomologacion;
	}

	public void setNombreHomologacion(String nombreHomologacion) {
		this.nombreHomologacion = nombreHomologacion;
	}

	public String getVigenciaTipoHomologacion() {
		return vigenciaTipoHomologacion;
	}

	public void setVigenciaTipoHomologacion(String vigenciaTipoHomologacion) {
		this.vigenciaTipoHomologacion = vigenciaTipoHomologacion;
	}

	public IcsCore getIcsCore1() {
		return icsCore1;
	}

	public void setIcsCore1(IcsCore icsCore1) {
		this.icsCore1 = icsCore1;
	}

	public IcsCore getIcsCore2() {
		return icsCore2;
	}

	public void setIcsCore2(IcsCore icsCore2) {
		this.icsCore2 = icsCore2;
	}

	public IcsSocio getIcsSocio1() {
		return icsSocio1;
	}

	public void setIcsSocio1(IcsSocio icsSocio1) {
		this.icsSocio1 = icsSocio1;
	}

	public IcsSocio getIcsSocio2() {
		return icsSocio2;
	}

	public void setIcsSocio2(IcsSocio icsSocio2) {
		this.icsSocio2 = icsSocio2;
	}
}
