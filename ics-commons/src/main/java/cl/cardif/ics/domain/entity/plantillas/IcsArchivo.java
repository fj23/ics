package cl.cardif.ics.domain.entity.plantillas;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import cl.cardif.ics.domain.entity.common.IcsTipoExtension;

/**
 * The persistent class for the ICS_ARCHIVOS database table.
 * 
 */
@Entity
@Table(name = "ICS_ARCHIVOS")
@NamedQuery(name = "IcsArchivo.findAll", query = "SELECT i FROM IcsArchivo i")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class IcsArchivo implements Serializable {
	private static final long serialVersionUID = 6277251298544077437L;

	@Id
	@Column(name = "ID_ARCHIVO")
	private long idArchivo;

	@Column(name = "ID_PLANTILLA")
	private Long idPlantilla;

	@Column(name = "DELIMITADOR", nullable = true)
	private String delimitador;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_EXTENSION")
	private IcsTipoExtension icsTipoExtension;

	@Column(name = "ID_GRUPO_ESTRUCTURAS", nullable = true)
	private Long idGrupoEstructuras;

	@Column(name = "LINEA_FINAL", nullable = true)
	private BigDecimal lineaFinal;

	@Column(name = "LINEA_INICIAL", nullable = true)
	private BigDecimal lineaInicial;

	@Column(name = "NOMBRE_ARCHIVO")
	private String nombreArchivo;

	@Column(name = "TIPO_ARCHIVO")
	private String tipoArchivo;

	@Column(name = "VIGENCIA_ARCHIVO", columnDefinition = "CHAR")
	private String vigenciaArchivo;

	@Column(name = "NUMERO_HOJA", nullable = true)
	private Long numeroHoja;

	@Column(name = "FLG_SIN_TRANSFORMACION", columnDefinition = "CHAR", nullable = true)
	private String flgSinTransformacion;

	@Column(name = "FLG_CABECERA", columnDefinition = "CHAR", nullable = false)
	private String flgCabecera;

	public IcsArchivo() {
		super();
	}
	
	public long getIdArchivo() {
		return idArchivo;
	}

	public void setIdArchivo(long idArchivo) {
		this.idArchivo = idArchivo;
	}

	public Long getIdPlantilla() {
		return idPlantilla;
	}

	public void setIdPlantilla(Long idPlantilla) {
		this.idPlantilla = idPlantilla;
	}

	public String getDelimitador() {
		return delimitador;
	}

	public void setDelimitador(String delimitador) {
		this.delimitador = delimitador;
	}

	public IcsTipoExtension getIcsTipoExtension() {
		return icsTipoExtension;
	}

	public void setIcsTipoExtension(IcsTipoExtension icsTipoExtension) {
		this.icsTipoExtension = icsTipoExtension;
	}

	public Long getIdGrupoEstructuras() {
		return idGrupoEstructuras;
	}

	public void setIdGrupoEstructuras(Long idGrupoEstructuras) {
		this.idGrupoEstructuras = idGrupoEstructuras;
	}

	public BigDecimal getLineaFinal() {
		return lineaFinal;
	}

	public void setLineaFinal(BigDecimal lineaFinal) {
		this.lineaFinal = lineaFinal;
	}

	public BigDecimal getLineaInicial() {
		return lineaInicial;
	}

	public void setLineaInicial(BigDecimal lineaInicial) {
		this.lineaInicial = lineaInicial;
	}

	public String getNombreArchivo() {
		return nombreArchivo;
	}

	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	public String getTipoArchivo() {
		return tipoArchivo;
	}

	public void setTipoArchivo(String tipoArchivo) {
		this.tipoArchivo = tipoArchivo;
	}

	public String getVigenciaArchivo() {
		return vigenciaArchivo;
	}

	public void setVigenciaArchivo(String vigenciaArchivo) {
		this.vigenciaArchivo = vigenciaArchivo;
	}

	public Long getNumeroHoja() {
		return numeroHoja;
	}

	public void setNumeroHoja(Long numeroHoja) {
		this.numeroHoja = numeroHoja;
	}

	public String getFlgSinTransformacion() {
		return flgSinTransformacion;
	}

	public void setFlgSinTransformacion(String flgSinTransformacion) {
		this.flgSinTransformacion = flgSinTransformacion;
	}

	public String getFlgCabecera() {
		return flgCabecera;
	}

	public void setFlgCabecera(String flgCabecera) {
		this.flgCabecera = flgCabecera;
	}
}
