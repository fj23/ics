package cl.cardif.ics.domain.entity.common;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the ICS_DIRECTORIOS_SALIDA database table.
 * 
 */
@Entity
@Table(name = "ICS_DIRECTORIOS_SALIDA")
@NamedQuery(name = "IcsDirectoriosSalida.findAll", query = "SELECT i FROM IcsDirectoriosSalida i")
public class IcsDirectoriosSalida implements Serializable {
	private static final long serialVersionUID = -2884091325715182586L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_DIRECTORIO_SALIDA")
	private long idDirectorioSalida;

	@Column(name = "DESCRIPCION")
	private String descripcionDirectorio;

	@Column(name = "TIPO")
	private Long tipoDirectorio;
	
	@Column(name = "DIRECTORIO_SALIDA")
	private String directorioSalida;

	@Column(name = "FLAG", columnDefinition = "CHAR")
	private String flag;
	
	public IcsDirectoriosSalida() {
		super();
	}

	public long getIdDirectorioSalida() {
		return idDirectorioSalida;
	}

	public void setIdDirectorioSalida(long idDirectorioSalida) {
		this.idDirectorioSalida = idDirectorioSalida;
	}

	public String getDescripcionDirectorio() {
		return descripcionDirectorio;
	}

	public void setDescripcionDirectorio(String descripcionDirectorio) {
		this.descripcionDirectorio = descripcionDirectorio;
	}

	public Long getTipoDirectorio() {
		return tipoDirectorio;
	}

	public void setTipoDirectorio(Long tipoDirectorio) {
		this.tipoDirectorio = tipoDirectorio;
	}

	public String getDirectorioSalida() {
		return directorioSalida;
	}

	public void setDirectorioSalida(String directorioSalida) {
		this.directorioSalida = directorioSalida;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}
}
