package cl.cardif.ics.domain.entity.common;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the ICS_TIPO_PERSONA database table.
 * 
 */
@Entity
@Table(name = "ICS_TIPO_PERSONA")
@NamedQuery(name = "IcsTipoPersona.findAll", query = "SELECT i FROM IcsTipoPersona i")
public class IcsTipoPersona implements Serializable {
	private static final long serialVersionUID = 8754130460376303998L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_PERSONA")
	private Long idPersona;

	@Column(name = "TIPO_PERSONA")
	private String tipoPersona;

	@Column(name = "CARDINALIDAD_MAXIMA")
	private Long cardinalidadMaxima;

	public Long getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}

	public String getTipoPersona() {
		return tipoPersona;
	}

	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	public Long getCardinalidadMaxima() {
		return cardinalidadMaxima;
	}

	public void setCardinalidadMaxima(Long cardinalidadMaxima) {
		this.cardinalidadMaxima = cardinalidadMaxima;
	}
}
