package cl.cardif.ics.domain.entity.common;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the ICS_TIPO_SALIDA database table.
 * 
 */
@Entity
@Table(name = "ICS_TIPO_SALIDA")
@NamedQuery(name = "IcsTipoSalida.findAll", query = "SELECT i FROM IcsTipoSalida i")
public class IcsTipoSalida implements Serializable {
	private static final long serialVersionUID = -2046918956305194610L;

	@Id
	@Column(name = "ID_TIPO_SALIDA")
	private Long idTipoSalida;

	@Column(name =  "DESCRIPCION")
	private String descripcion;

	@Column(name = "VIGENCIA", columnDefinition = "CHAR")
	private String vigencia;

	public Long getIdTipoSalida() {
		return idTipoSalida;
	}

	public void setIdTipoSalida(Long idTipoSalida) {
		this.idTipoSalida = idTipoSalida;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getVigencia() {
		return vigencia;
	}

	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}
}
