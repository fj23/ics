package cl.cardif.ics.domain.entity.estructuras;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import cl.cardif.ics.domain.entity.common.IcsTipoDato;

/**
 * The persistent class for the ICS_DETALLE_ESTRUCTURA database table.
 * 
 */
@Entity
@Table(name = "ICS_DETALLE_ESTRUCTURA")
@NamedQuery(name = "IcsDetalleEstructura.findAll", query = "SELECT i FROM IcsDetalleEstructura i")
public class IcsDetalleEstructura extends DetalleEstructura implements Serializable{
	private static final long serialVersionUID = 7649747372218134417L;

	@Id
	@SequenceGenerator(name = "DB_SEQ_DETALLE_ESTRUCTURA", sequenceName = "SEQ_ICS_DETALLE_ESTRUCTURA", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DB_SEQ_DETALLE_ESTRUCTURA")
	@Column(name = "ID_DETALLE_ESTRUCTURA")
	private long idDetalleEstructura;

	@Column(name = "DESCRIPCION_DETALLE_EST")
	private String descripcionDetalleEstructura;

	@Column(name = "CODIGO_ESTRUCTURA")
	private String codigoEstructura;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_ESTRUCTURA")
	private IcsEstructura icsEstructura;

	@Column(name = "VIGENCIA", columnDefinition = "CHAR")
	private String vigenciaDetalleEstructura;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_TIPO_DATO")
	private IcsTipoDato icsTipoDato;

	public IcsDetalleEstructura() {
		super();
	}

	public long getIdDetalleEstructura() {
		return idDetalleEstructura;
	}

	public void setIdDetalleEstructura(long idDetalleEstructura) {
		this.idDetalleEstructura = idDetalleEstructura;
	}

	public String getDescripcionDetalleEstructura() {
		return descripcionDetalleEstructura;
	}

	public void setDescripcionDetalleEstructura(String descripcionDetalleEstructura) {
		this.descripcionDetalleEstructura = descripcionDetalleEstructura;
	}

	public String getCodigoEstructura() {
		return codigoEstructura;
	}

	public void setCodigoEstructura(String codigoEstructura) {
		this.codigoEstructura = codigoEstructura;
	}

	public IcsEstructura getIcsEstructura() {
		return icsEstructura;
	}

	public void setIcsEstructura(IcsEstructura icsEstructura) {
		this.icsEstructura = icsEstructura;
	}
	
	public String getVigenciaDetalleEstructura() {
		return vigenciaDetalleEstructura;
	}

	public void setVigenciaDetalleEstructura(String vigenciaDetalleEstructura) {
		this.vigenciaDetalleEstructura = vigenciaDetalleEstructura;
	}

	public IcsTipoDato getIcsTipoDato() {
		return icsTipoDato;
	}

	public void setIcsTipoDato(IcsTipoDato icsTipoDato) {
		this.icsTipoDato = icsTipoDato;
	}
}
