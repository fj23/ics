package cl.cardif.ics.domain.entity.estructuras;

import javax.persistence.Embeddable;

/**
 * The primary key class for the ICS_ESTRUCTURAS_AGRUPADAS database table.
 * 
 */
@Embeddable
public class IcsEstructurasAgrupadaPK {
	private Long idGrupoEstructuras;

	private Long idEstructura;

	public IcsEstructurasAgrupadaPK() {
		super();
	}

	public Long getIdGrupoEstructuras() {
		return this.idGrupoEstructuras;
	}

	public void setIdGrupoEstructuras(Long idGrupoEstructuras) {
		this.idGrupoEstructuras = idGrupoEstructuras;
	}

	public Long getIdEstructura() {
		return this.idEstructura;
	}

	public void setIdEstructura(Long idEstructura) {
		this.idEstructura = idEstructura;
	}

	@Override
	public boolean equals(Object other) {
		if (!(other instanceof IcsEstructurasAgrupadaPK)) {
			return false;
		} else {
		
			if (this == other) {
				return true;
			}
			
			return (this.idEstructura.equals(((IcsEstructurasAgrupadaPK) other).getIdEstructura()) && 
					this.idGrupoEstructuras.equals(((IcsEstructurasAgrupadaPK) other).getIdGrupoEstructuras()));
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.idGrupoEstructuras ^ (this.idGrupoEstructuras >>> 32)));
		hash = hash * prime + ((int) (this.idEstructura ^ (this.idEstructura >>> 32)));

		return hash;
	}
}
