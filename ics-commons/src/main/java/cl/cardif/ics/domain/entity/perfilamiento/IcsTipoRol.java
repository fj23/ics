package cl.cardif.ics.domain.entity.perfilamiento;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the ICS_CORE database table.
 * 
 */
@Entity
@Table(name = "ICS_TIPO_ROL")
@NamedQuery(name = "IcsTipoRol.findAll", query = "SELECT i FROM IcsTipoRol i")
public class IcsTipoRol {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_ROL")
	private Long idRol;

	@Column(name = "NOMBRE_ROL")
	private String nombreRol;

	@Column(name = "FECHA_CREACION")
	private Date fechaCreacion;

	@Column(name = "VIGENCIA", columnDefinition = "CHAR")
	private String vigencia;

	@OneToMany(mappedBy = "icsTipoRol", fetch = FetchType.EAGER)
	private List<IcsDetalleRol> detalles;

	public IcsTipoRol() {
		super();
	}

	public Long getIdRol() {
		return idRol;
	}

	public void setIdRol(Long idRol) {
		this.idRol = idRol;
	}

	public String getNombreRol() {
		return nombreRol;
	}

	public void setNombreRol(String nombreRol) {
		this.nombreRol = nombreRol;
	}

	public Date getFechaCreacion() {
		if (fechaCreacion != null) {
			return (Date)fechaCreacion.clone();
		} else {
			return null;
		}
	}

	public void setFechaCreacion(Date fechaCreacion) {
		if (fechaCreacion != null) {
			this.fechaCreacion = (Date)fechaCreacion.clone();
		}
	}

	public String getVigencia() {
		return vigencia;
	}

	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}

	public List<IcsDetalleRol> getDetalles() {
		if (detalles != null) {
			return new ArrayList<>(detalles);
		} else {
			return new ArrayList<>();
		}
	}
}
