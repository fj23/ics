package cl.cardif.ics.domain.entity.carga;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * The persistent class for the ICS_GENERACION_ARCHIVOS database table.
 * 
 */
@Entity
@Table(name = "ICS_GENERACION_ARCHIVOS")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class IcsGeneracionArchivo implements Serializable {
	private static final long serialVersionUID = 5313036075537681882L;

	@Id
	@Column(name = "LINEA", nullable = false)
	private Long linea;

	@Column(name = "DATOS", columnDefinition = "CLOB")
	@Lob
	private String datos;

	@Column(name = "NOMBRE_ARCHIVO")
	private String nombreArchivo;

	@Column(name = "EXTENSION_ARCHIVO")
	private String extensionArchivo;

	@Column(name = "DIRECTORIO")
	private String directorio;

	@Column(name = "HOJA_DESTINO")
	private String hojaDestino;

	@Column(name = "TIPO_DATO")
	private String tipoDato;

	@Column(name = "ID_TRANSACCION", nullable = false)
	private Long idTransaccion;

	@Column(name = "TIPO_ARCHIVO", nullable = false)
	private Long tipoArchivo;

	public IcsGeneracionArchivo() {
		super();
	}

	public String getDatos() {
		return datos;
	}

	public void setDatos(String datos) {
		this.datos = datos;
	}

	public String getNombreArchivo() {
		return nombreArchivo;
	}

	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	public String getExtensionArchivo() {
		return extensionArchivo;
	}

	public void setExtensionArchivo(String extensionArchivo) {
		this.extensionArchivo = extensionArchivo;
	}

	public String getDirectorio() {
		return directorio;
	}

	public void setDirectorio(String directorio) {
		this.directorio = directorio;
	}

	public String getHojaDestino() {
		return hojaDestino;
	}

	public void setHojaDestino(String hojaDestino) {
		this.hojaDestino = hojaDestino;
	}

	public String getTipoDato() {
		return tipoDato;
	}

	public void setTipoDato(String tipoDato) {
		this.tipoDato = tipoDato;
	}

	public Long getIdTransaccion() {
		return idTransaccion;
	}

	public void setIdTransaccion(Long idTransaccion) {
		this.idTransaccion = idTransaccion;
	}

	public Long getLinea() {
		return linea;
	}

	public void setLinea(Long linea) {
		this.linea = linea;
	}

	public Long getTipoArchivo() {
		return tipoArchivo;
	}

	public void setTipoArchivo(Long tipoArchivo) {
		this.tipoArchivo = tipoArchivo;
	}

}
