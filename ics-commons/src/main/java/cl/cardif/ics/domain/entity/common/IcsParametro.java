package cl.cardif.ics.domain.entity.common;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * The persistent class for the ICS_PARAMETROS database table.
 * 
 */
@Entity
@Table(name = "ICS_PARAMETROS")
@NamedQuery(name = "IcsParametro.findAll", query = "SELECT i FROM IcsParametro i")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class IcsParametro implements Serializable {
	private static final long serialVersionUID = -2249090152765016322L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_PARAMETRO")
	private Long idParametro;

	@Column(name = "COD_PARAMETRO")
	private String codParametro;

	@Column(name = "DESCRIPCION_PARAMETRO")
	private String descripcionParametro;

	@Column(name = "CONCEPTO_PARAMETRO")
	private String conceptoParametro;

	@Column(name = "VALOR_PARAMETRO")
	private String valorParametro;

	@Column(name = "VIGENCIA", columnDefinition = "CHAR")
	private String vigencia;

	@Column(name = "FECHA_CREACION", length=255)
	private Date fechaCreacion;

	@Column(name = "VALOR_AUXILIAR")
	private String valorAuxiliar;

	public Long getIdParametro() {
		return idParametro;
	}

	public void setIdParametro(Long idParametro) {
		this.idParametro = idParametro;
	}

	public String getCodParametro() {
		return codParametro;
	}

	public void setCodParametro(String codParametro) {
		this.codParametro = codParametro;
	}

	public String getDescripcionParametro() {
		return descripcionParametro;
	}

	public void setDescripcionParametro(String descripcionParametro) {
		this.descripcionParametro = descripcionParametro;
	}

	public String getConceptoParametro() {
		return conceptoParametro;
	}

	public void setConceptoParametro(String conceptoParametro) {
		this.conceptoParametro = conceptoParametro;
	}

	public String getValorParametro() {
		return valorParametro;
	}

	public void setValorParametro(String valorParametro) {
		this.valorParametro = valorParametro;
	}

	public String getVigencia() {
		return vigencia;
	}

	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}

	public Date getFechaCreacion() {
		if (fechaCreacion != null) {
			return (Date) fechaCreacion.clone();
		} else {
			return null;
		}
	}

	public void setFechaCreacion(Date fechaCreacion) {
		if (fechaCreacion != null) {
			this.fechaCreacion = (Date) fechaCreacion.clone();
		}
	}

	public String getValorAuxiliar() {
		return valorAuxiliar;
	}

	public void setValorAuxiliar(String valorAuxiliar) {
		this.valorAuxiliar = valorAuxiliar;
	}
}
