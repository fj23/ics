package cl.cardif.ics.domain.entity.carga;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * The persistent class for the ICS_ARCHIVO_IMPORTADO database table.
 * 
 */
@Entity
@Table(name = "ICS_ARCHIVO_IMPORTADO")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class IcsArchivoImportado implements Serializable {
	private static final long serialVersionUID = 4125966042997526446L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DB_SEQ_ICS_ARCHIVO_IMPORTADO")
	@SequenceGenerator(name = "DB_SEQ_ICS_ARCHIVO_IMPORTADO", sequenceName = "SEQ_ICS_ARCHIVO_IMPORTADO", allocationSize = 1)
	@Column(name = "ID_ARCHIVO_IMPORTADO")
	private Long idArchivoImportado;

	@Column(name = "ID_TRANSACCION", nullable = false)
	private Long idTransaccion;

	@Column(name = "LINEA_ARCHIVO", length = 1500, nullable = false)
	private String lineaArchivo;

	@Column(name = "LINEA", precision = 6, nullable = false)
	private Integer linea;

	@Column(name = "HOJA", precision = 2, nullable = false)
	private Integer hoja;

	public IcsArchivoImportado() {
		super();
	}

	public Long getIdArchivoImportado() {
		return idArchivoImportado;
	}

	public void setIdArchivoImportado(Long idArchivoImportado) {
		this.idArchivoImportado = idArchivoImportado;
	}

	public Long getIdTransaccion() {
		return idTransaccion;
	}

	public void setIdTransaccion(Long idTransaccion) {
		this.idTransaccion = idTransaccion;
	}

	public String getLineaArchivo() {
		return lineaArchivo;
	}

	public void setLineaArchivo(String lineaArchivo) {
		this.lineaArchivo = lineaArchivo;
	}

	public Integer getLinea() {
		return linea;
	}

	public void setLinea(Integer linea) {
		this.linea = linea;
	}

	public Integer getHoja() {
		return hoja;
	}

	public void setHoja(Integer hoja) {
		this.hoja = hoja;
	}

}
