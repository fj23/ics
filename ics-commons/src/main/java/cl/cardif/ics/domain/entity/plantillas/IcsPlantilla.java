package cl.cardif.ics.domain.entity.plantillas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import cl.cardif.ics.domain.entity.common.IcsCore;
import cl.cardif.ics.domain.entity.common.IcsDirectoriosSalida;
import cl.cardif.ics.domain.entity.common.IcsEvento;
import cl.cardif.ics.domain.entity.common.IcsSocio;
import cl.cardif.ics.domain.entity.common.IcsTipoSalida;

/**
 * The persistent class for the ICS_PLANTILLA database table.
 * 
 */
@Entity
@Table(name = "ICS_PLANTILLA")
@NamedQuery(name = "IcsPlantilla.findAll", query = "SELECT i FROM IcsPlantilla i")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class IcsPlantilla implements Serializable {
	private static final long serialVersionUID = -8803385407024469201L;

	@Id
	@SequenceGenerator(name = "DB_SEQ_ICS_PLANTILLA", sequenceName = "SEQ_ICS_PLANTILLA", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator = "DB_SEQ_ICS_PLANTILLA")
	@Column(name = "ID_PLANTILLA")
	private long idPlantilla;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_ARCHIVO")
	private IcsArchivo icsArchivo;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_CORE")
	private IcsCore icsCore;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_CORE_DESTINO")
	private IcsCore icsCoreDestino;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_EVENTO")
	private IcsEvento icsEvento;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_SOCIO")
	private IcsSocio icsSocio;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_SOCIO_DESTINO")
	private IcsSocio icsSocioDestino;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_TIPO_PLANTILLA")
	private IcsTipoPlantilla icsTipoPlantilla;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_TIPO_SALIDA")
	private IcsTipoSalida icsTipoSalida;

	@Column(name = "NOMBRE_PLANTILLA")
	private String nombrePlantilla;

	@Column(name = "OBSERVACION_PLANTILLA")
	private String observacionPlantilla;

	@Column(name = "PLANTILLA_MULTIARCHIVO", columnDefinition = "CHAR")
	private String plantillaMultiarchivo;

	@Column(name = "VERSION_PLANITLLA")
	private Long versionPlantilla;

	@Column(name = "VIGENCIA", columnDefinition = "CHAR")
	private String vigencia;

	@Column(name = "FECHA_CREACION")
	private Date fechaCreacion;

	@Column(name = "COD_USUARIO")
	private String codUsuario;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_DIRECTORIO_SALIDA")
	private IcsDirectoriosSalida icsDirectorioSalida;

	public IcsPlantilla() {
		super();
	}

	public long getIdPlantilla() {
		return idPlantilla;
	}

	public void setIdPlantilla(long idPlantilla) {
		this.idPlantilla = idPlantilla;
	}

	public IcsArchivo getIcsArchivo() {
		return icsArchivo;
	}

	public void setIcsArchivo(IcsArchivo icsArchivo) {
		this.icsArchivo = icsArchivo;
	}

	public IcsCore getIcsCore() {
		return icsCore;
	}

	public void setIcsCore(IcsCore icsCore) {
		this.icsCore = icsCore;
	}

	public IcsCore getIcsCoreDestino() {
		return icsCoreDestino;
	}

	public void setIcsCoreDestino(IcsCore icsCoreDestino) {
		this.icsCoreDestino = icsCoreDestino;
	}

	public IcsEvento getIcsEvento() {
		return icsEvento;
	}

	public void setIcsEvento(IcsEvento icsEvento) {
		this.icsEvento = icsEvento;
	}

	public IcsSocio getIcsSocio() {
		return icsSocio;
	}

	public void setIcsSocio(IcsSocio icsSocio) {
		this.icsSocio = icsSocio;
	}

	public IcsSocio getIcsSocioDestino() {
		return icsSocioDestino;
	}

	public void setIcsSocioDestino(IcsSocio icsSocioDestino) {
		this.icsSocioDestino = icsSocioDestino;
	}

	public IcsTipoPlantilla getIcsTipoPlantilla() {
		return icsTipoPlantilla;
	}

	public void setIcsTipoPlantilla(IcsTipoPlantilla icsTipoPlantilla) {
		this.icsTipoPlantilla = icsTipoPlantilla;
	}

	public IcsTipoSalida getIcsTipoSalida() {
		return icsTipoSalida;
	}

	public void setIcsTipoSalida(IcsTipoSalida icsTipoSalida) {
		this.icsTipoSalida = icsTipoSalida;
	}

	public String getNombrePlantilla() {
		return nombrePlantilla;
	}

	public void setNombrePlantilla(String nombrePlantilla) {
		this.nombrePlantilla = nombrePlantilla;
	}

	public String getObservacionPlantilla() {
		return observacionPlantilla;
	}

	public void setObservacionPlantilla(String observacionPlantilla) {
		this.observacionPlantilla = observacionPlantilla;
	}

	public String getPlantillaMultiarchivo() {
		return plantillaMultiarchivo;
	}

	public void setPlantillaMultiarchivo(String plantillaMultiarchivo) {
		this.plantillaMultiarchivo = plantillaMultiarchivo;
	}

	public Long getVersionPlantilla() {
		return versionPlantilla;
	}

	public void setVersionPlantilla(Long versionPlantilla) {
		this.versionPlantilla = versionPlantilla;
	}

	public String getVigencia() {
		return vigencia;
	}

	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}

	public Date getFechaCreacion() {
		if (fechaCreacion != null) {
			return (Date)fechaCreacion.clone();
		} else {
			return null;
		}
	}

	public void setFechaCreacion(Date fechaCreacion) {
		if (fechaCreacion != null) {
			this.fechaCreacion = (Date)fechaCreacion.clone();
		}
	}

	public String getCodUsuario() {
		return codUsuario;
	}

	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}

	public IcsDirectoriosSalida getIcsDirectorioSalida() {
		return icsDirectorioSalida;
	}

	public void setIcsDirectorioSalida(IcsDirectoriosSalida icsDirectorioSalida) {
		this.icsDirectorioSalida = icsDirectorioSalida;
	}

	@Override
	public String toString() {
		return "IcsPlantilla [idPlantilla=" + idPlantilla + ", icsArchivo=" + icsArchivo + ", icsCore=" + icsCore + ", icsCoreDestino="
			+ icsCoreDestino + ", icsEvento=" + icsEvento + ", icsSocio=" + icsSocio + ", icsSocioDestino=" + icsSocioDestino
			+ ", icsTipoPlantilla=" + icsTipoPlantilla + ", icsTipoSalida=" + icsTipoSalida + ", nombrePlantilla=" + nombrePlantilla
			+ ", observacionPlantilla=" + observacionPlantilla + ", plantillaMultiarchivo=" + plantillaMultiarchivo + ", versionPlantilla="
			+ versionPlantilla + ", vigencia=" + vigencia + ", fechaCreacion=" + fechaCreacion + ", codUsuario=" + codUsuario
			+ ", icsDirectorioSalida=" + icsDirectorioSalida + "]";
	}
}
