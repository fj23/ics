package cl.cardif.ics.domain.entity.plantillas;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import cl.cardif.ics.domain.entity.common.IcsTipoFormula;
import cl.cardif.ics.domain.entity.estructuras.IcsDetalleEstructura;
import cl.cardif.ics.domain.entity.estructuras.IcsDetalleSubestructura;
import cl.cardif.ics.domain.entity.formatos.IcsDetalleFormato;
import cl.cardif.ics.domain.entity.homologaciones.IcsTipoHomologacion;

/**
 * The persistent class for the ICS_ATRIBUTOS_CNF_SALIDA database table.
 * 
 */
@Entity
@Table(name = "ICS_ATRIBUTOS_CNF_SALIDA")
@NamedQuery(name = "IcsAtributoCnfSalida.findAll", query = "SELECT i FROM IcsAtributoCnfSalida i")
public class IcsAtributoCnfSalida implements Serializable {
	private static final long serialVersionUID = 4113756279212925278L;

	@Id
	@Column(name = "ID_ATRIBUTO_ANULACION")
	private Long idAtributoAnulacion;

	@JoinColumn(name = "ID_DETALLE_ESTRUCTURA")
	@ManyToOne(fetch = FetchType.LAZY)
	private IcsDetalleEstructura icsDetalleEstructura;

	@JoinColumn(name = "ID_DETALLE_SUBESTRUCTA")
	@ManyToOne(fetch = FetchType.LAZY)
	private IcsDetalleSubestructura icsDetalleSubestructura;

	@JoinColumn(name = "ID_FORMULA")
	@ManyToOne(fetch = FetchType.LAZY)
	private IcsTipoFormula icsTipoFormula;

	@JoinColumn(name = "ID_HOMOLOGACION")
	@ManyToOne(fetch = FetchType.LAZY)
	private IcsTipoHomologacion icsTipoHomologacion;

	@JoinColumn(name = "ID_FORMATO")
	@ManyToOne(fetch = FetchType.LAZY)
	private IcsDetalleFormato icsFormato;

	@JoinColumn(name = "ID_ATRIBUTO_EST")
	@ManyToOne(fetch = FetchType.LAZY)
	private IcsAtributoSalidaEstruc icsAtributoSalidaEstruc;

	@JoinColumn(name = "ID_PLANTILLA")
	@ManyToOne(fetch = FetchType.LAZY)
	private IcsPlantilla icsPlantilla;

	@Column(name = "VALOR")
	private String valor;

	@Column(name = "FORMULA_JSON")
	private String formulaJSON;
	
	public IcsAtributoCnfSalida() {
		super();
	}
	
	public Long getIdAtributoAnulacion() {
		return idAtributoAnulacion;
	}

	public void setIdAtributoAnulacion(Long idAtributoAnulacion) {
		this.idAtributoAnulacion = idAtributoAnulacion;
	}

	public IcsDetalleEstructura getIcsDetalleEstructura() {
		return icsDetalleEstructura;
	}

	public void setIcsDetalleEstructura(IcsDetalleEstructura detalleEstructura) {
		this.icsDetalleEstructura = detalleEstructura;
	}

	public IcsDetalleSubestructura getIcsDetalleSubestructura() {
		return icsDetalleSubestructura;
	}

	public void setIcsDetalleSubestructura(IcsDetalleSubestructura detalleSubestructura) {
		this.icsDetalleSubestructura = detalleSubestructura;
	}

	public IcsTipoFormula getIcsTipoFormula() {
		return icsTipoFormula;
	}

	public void setIcsTipoFormula(IcsTipoFormula tipoFormula) {
		this.icsTipoFormula = tipoFormula;
	}

	public IcsTipoHomologacion getIcsTipoHomologacion() {
		return icsTipoHomologacion;
	}

	public void setIcsTipoHomologacion(IcsTipoHomologacion tipoHomologacion) {
		this.icsTipoHomologacion = tipoHomologacion;
	}

	public IcsDetalleFormato getIcsDetalleFormato() {
		return icsFormato;
	}

	public void setIcsDetalleFormato(IcsDetalleFormato icsFormato) {
		this.icsFormato = icsFormato;
	}

	public IcsAtributoSalidaEstruc getIcsAtributoSalidaEstruc() {
		return icsAtributoSalidaEstruc;
	}

	public void setIcsAtributoSalidaEstruc(IcsAtributoSalidaEstruc atributoEstructura) {
		this.icsAtributoSalidaEstruc = atributoEstructura;
	}
	
	public IcsPlantilla getIcsPlantilla() {
		return icsPlantilla;
	}

	public void setIcsPlantilla(IcsPlantilla plantilla) {
		this.icsPlantilla = plantilla;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public String getFormulaJSON() {
		return formulaJSON;
	}

	public void setFormulaJSON(String formulaJSON) {
		this.formulaJSON = formulaJSON;
	}	
}
