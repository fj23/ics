package cl.cardif.ics.domain.entity.common;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the ICS_TIPO_ARCHIVO database table.
 * 
 */
@Entity
@Table(name = "ICS_TIPO_ARCHIVO")
@NamedQuery(name = "IcsTipoArchivo.findAll", query = "SELECT i FROM IcsTipoArchivo i")
public class IcsTipoArchivo implements Serializable {
	private static final long serialVersionUID = -270550460383293587L;

	@Id
	@Column(name = "ID_TIPO_ARCHIVO")
	private long idTipoArchivo;

	@Column(name = "NOMBRE_ARCHIVO")
	private String nombreTipoArchivo;

	@Column(name = "FLG_VISIBLE", columnDefinition = "CHAR")
	private String flgVisible;

	public long getIdTipoArchivo() {
		return idTipoArchivo;
	}

	public void setIdTipoArchivo(long idTipoArchivo) {
		this.idTipoArchivo = idTipoArchivo;
	}

	public String getNombreTipoArchivo() {
		return nombreTipoArchivo;
	}

	public void setNombreTipoArchivo(String nombreTipoArchivo) {
		this.nombreTipoArchivo = nombreTipoArchivo;
	}

	public String getFlgVisible() {
		return flgVisible;
	}

	public void setFlgVisible(String flgVisible) {
		this.flgVisible = flgVisible;
	}
}
