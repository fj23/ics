package cl.cardif.ics.domain.entity.plantillas;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

/**
 * The persistent class for the ICS_ATRIBUTO_USER_INTG database table.
 * 
 */
@Entity
@Table(name = "ICS_ATRIBUTO_USER_INTG")
@NamedQuery(name = "IcsAtributoUserIntg.findAll", query = "SELECT i FROM IcsAtributoUserIntg i")
public class IcsAtributoUserIntg implements Serializable {
	private static final long serialVersionUID = -2071992516318739663L;

	@Id
	@Column(name = "ID_ATRIBUTO_USER", nullable = true)
	private Long idAtributoUser;

	@NotFound(action = NotFoundAction.IGNORE)
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "ID_INTEGRACION_EVENTO", insertable = false, updatable = false, nullable = false)
	private IcsAtributosIntgEvento icsAtributoIntegracionEvento;

	@Column(name = "TIPO_PERSONA", nullable = true)
	private String tipoPersona;

	@Column(name = "NOMBRE_COLUMNA", nullable = true)
	private String nombreColumna;

	@Column(name = "FLG_CARDINALIDAD", columnDefinition = "CHAR")
	private String flgCardinalidad;

	public Long getIdAtributoUser() {
		return idAtributoUser;
	}

	public void setIdAtributoUser(Long idAtributoUser) {
		this.idAtributoUser = idAtributoUser;
	}

	public IcsAtributosIntgEvento getIcsAtributoIntegracionEvento() {
		return icsAtributoIntegracionEvento;
	}

	public void setIcsAtributoIntegracionEvento(IcsAtributosIntgEvento icsAtributoIntegracionEvento) {
		this.icsAtributoIntegracionEvento = icsAtributoIntegracionEvento;
	}

	public String getTipoPersona() {
		return tipoPersona;
	}

	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	public String getNombreColumna() {
		return nombreColumna;
	}

	public void setNombreColumna(String nombreColumna) {
		this.nombreColumna = nombreColumna;
	}

	public String getFlgCardinalidad() {
		return flgCardinalidad;
	}

	public void setFlgCardinalidad(String flgCardinalidad) {
		this.flgCardinalidad = flgCardinalidad;
	}
}
