package cl.cardif.ics.domain.entity.common;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * The persistent class for the ICS_EVENTO database table.
 * 
 */
@Entity
@Table(name = "ICS_EVENTO")
@NamedQuery(name = "IcsEvento.findAll", query = "SELECT i FROM IcsEvento i")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class IcsEvento implements Serializable {
	private static final long serialVersionUID = 3993856522753279166L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_EVENTO")
	private Long idEvento;

	@Column(name = "NOMBRE_EVENTO")
	private String nombreEvento;

	@Column(name = "VIGENCIA_EVENTO", columnDefinition = "CHAR")
	private String vigenciaEvento;

	public Long getIdEvento() {
		return idEvento;
	}

	public void setIdEvento(Long idEvento) {
		this.idEvento = idEvento;
	}

	public String getNombreEvento() {
		return nombreEvento;
	}

	public void setNombreEvento(String nombreEvento) {
		this.nombreEvento = nombreEvento;
	}

	public String getVigenciaEvento() {
		return vigenciaEvento;
	}

	public void setVigenciaEvento(String vigenciaEvento) {
		this.vigenciaEvento = vigenciaEvento;
	}
}
