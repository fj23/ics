package cl.cardif.ics.domain.entity.procesos;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import cl.cardif.ics.domain.entity.common.IcsSocio;

/**
 * The persistent class for the ICS_DETALLE_TRANSACCION database table.
 * 
 */
@Entity
@Table(name = "ICS_DETALLE_TRANSACCION")
@NamedQuery(name = "IcsDetalleTransaccion.findAll", query = "SELECT i FROM IcsDetalleTransaccion i")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class IcsDetalleTransaccion {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_DETALLE_TRANSACCION")
	private Long idDetalleTransaccion;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_PROCESO")
	private IcsProceso icsProceso;

	@Column(name = "ID_TRANSACCION", columnDefinition = "NUMBER")
	private Long idTransaccion;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_SOCIO")
	private IcsSocio icsSocio;

	@Column(name = "DETALLE_ETAPA")
	private String detalleEtapa;

	@Column(name = "FECHA_INICIO")
	private Date fechaInicio;

	@Column(name = "FECHA_FIN")
	private Date fechaFin;

	public Long getIdDetalleTransaccion() {
		return idDetalleTransaccion;
	}

	public void setIdDetalleTransaccion(Long idDetalleTransaccion) {
		this.idDetalleTransaccion = idDetalleTransaccion;
	}

	public IcsProceso getIcsProceso() {
		return icsProceso;
	}

	public void setIcsProceso(IcsProceso icsProceso) {
		this.icsProceso = icsProceso;
	}

	public Long getIdTransaccion() {
		return idTransaccion;
	}

	public void setIdTransaccion(Long idTransaccion) {
		this.idTransaccion = idTransaccion;
	}

	public IcsSocio getIcsSocio() {
		return icsSocio;
	}

	public void setIcsSocio(IcsSocio icsSocio) {
		this.icsSocio = icsSocio;
	}

	public String getDetalleEtapa() {
		return detalleEtapa;
	}

	public void setDetalleEtapa(String detalleEtapa) {
		this.detalleEtapa = detalleEtapa;
	}

	public Date getFechaInicio() {
		if (fechaInicio != null) {
			return (Date)fechaInicio.clone();
		} else {
			return null;
		}
	}

	public void setFechaInicio(Date fechaInicio) {
		if (fechaInicio != null) {
			this.fechaInicio = (Date) fechaInicio.clone();
		}
	}

	public Date getFechaFin() {
		if (fechaFin != null) {
			return (Date)fechaFin.clone();
		} else {
			return null;
		}
	}

	public void setFechaFin(Date fechaFin) {
		if (fechaFin != null) {
			this.fechaFin = (Date)fechaFin.clone();
		}
	}
}
