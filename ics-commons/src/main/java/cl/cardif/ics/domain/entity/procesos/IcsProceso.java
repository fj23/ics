package cl.cardif.ics.domain.entity.procesos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import cl.cardif.ics.domain.entity.common.IcsCallCenter;
import cl.cardif.ics.domain.entity.common.IcsDirectoriosSalida;
import cl.cardif.ics.domain.entity.common.IcsTipoArchivo;

/**
 * The persistent class for the ICS_PROCESOS database table.
 * 
 */
@Entity
@Table(name = "ICS_PROCESOS")
@NamedQuery(name = "IcsProceso.findAll", query = "SELECT i FROM IcsProceso i")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class IcsProceso implements Serializable {
	private static final long serialVersionUID = 1109068661469000342L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DB_SEQ_ICS_PROCESO")
	@SequenceGenerator(name = "DB_SEQ_ICS_PROCESO", sequenceName = "SEQ_ICS_PROCESO", allocationSize = 1)
	@Column(name = "ID_PROCESO")
	private long idProceso;

	@Column(name = "NOMBRE_PROCESO")
	private String nombreProceso;

	@Column(name = "VIGENCIA_PROCESO", columnDefinition = "CHAR")
	private String vigenciaProceso;

	@Column(name = "CORREO_SALIDA")
	private String correoSalida;

	@Column(name = "CORREO_EMAIL")
	private String correoEmail;

	@Column(name = "CORREO_HITOS")
	private String correoHitos;

	@Column(name = "FLG_ANETO", columnDefinition = "CHAR")
	private String flagAneto;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_CALL_CENTER")
	private IcsCallCenter icsCallCenter;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_TIPO_ARCHIVO")
	private IcsTipoArchivo icsTipoArchivo;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_DIRECTORIO_SALIDA")
	private IcsDirectoriosSalida icsDirectorioSalidaError;

	@OneToMany(mappedBy = "icsProceso")
	private List<IcsDetalleProceso> detalles;

	public IcsProceso() {
		super();
	}

	public long getIdProceso() {
		return idProceso;
	}

	public void setIdProceso(long idProceso) {
		this.idProceso = idProceso;
	}

	public String getNombreProceso() {
		return nombreProceso;
	}

	public void setNombreProceso(String nombreProceso) {
		this.nombreProceso = nombreProceso;
	}

	public String getVigenciaProceso() {
		return vigenciaProceso;
	}

	public void setVigenciaProceso(String vigenciaProceso) {
		this.vigenciaProceso = vigenciaProceso;
	}

	public String getCorreoSalida() {
		return correoSalida;
	}

	public void setCorreoSalida(String correoSalida) {
		this.correoSalida = correoSalida;
	}

	public String getCorreoEmail() {
		return correoEmail;
	}

	public void setCorreoEmail(String correoEmail) {
		this.correoEmail = correoEmail;
	}

	public String getCorreoHitos() {
		return correoHitos;
	}

	public void setCorreoHitos(String correoHitos) {
		this.correoHitos = correoHitos;
	}

	public String getFlagAneto() {
		return flagAneto;
	}

	public void setFlagAneto(String flagAneto) {
		this.flagAneto = flagAneto;
	}

	public List<IcsDetalleProceso> getDetalles() {
		if (this.detalles != null) {
			return new ArrayList<>(this.detalles);
		} else {
			return new ArrayList<>();
		}
	}

	public void setDetalles(List<IcsDetalleProceso> detalles) {
		if (detalles != null) {
			this.detalles = new ArrayList<>(detalles);
		}
	}

	public IcsCallCenter getIcsCallCenter() {
		return icsCallCenter;
	}

	public void setIcsCallCenter(IcsCallCenter icsCallCenter) {
		this.icsCallCenter = icsCallCenter;
	}

	public IcsTipoArchivo getIcsTipoArchivo() {
		return icsTipoArchivo;
	}

	public void setIcsTipoArchivo(IcsTipoArchivo icsTipoArchivo) {
		this.icsTipoArchivo = icsTipoArchivo;
	}

	public IcsDirectoriosSalida getIcsDirectorioSalidaError() {
		return icsDirectorioSalidaError;
	}

	public void setIcsDirectorioSalidaError(IcsDirectoriosSalida icsDirectorioSalidaError) {
		this.icsDirectorioSalidaError = icsDirectorioSalidaError;
	}

	@Override
	public String toString() {
		return "IcsProceso [idProceso=" + idProceso + ", nombreProceso=" + nombreProceso + ", vigenciaProceso=" + vigenciaProceso
			+ ", correoSalida=" + correoSalida + ", correoEmail=" + correoEmail + ", correoHitos=" + correoHitos + ", flagAneto="
			+ flagAneto + ", icsCallCenter=" + icsCallCenter + ", icsTipoArchivo=" + icsTipoArchivo + ", icsDirectorioSalidaError="
			+ icsDirectorioSalidaError + ", detalles=" + detalles + "]";
	}
}
