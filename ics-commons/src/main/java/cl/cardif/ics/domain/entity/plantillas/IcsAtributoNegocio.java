package cl.cardif.ics.domain.entity.plantillas;

import java.io.Serializable;

import javax.persistence.*;

import cl.cardif.ics.domain.entity.common.IcsEvento;

/**
 * The persistent class for the ICS_ATRIBUTO_NEGOCIO database table.
 * 
 */
@Entity
@Table(name = "ICS_ATRIBUTO_NEGOCIO")
@NamedQuery(name = "IcsAtributoNegocio.findAll", query = "SELECT i FROM IcsAtributoNegocio i")
public class IcsAtributoNegocio implements Serializable {
	private static final long serialVersionUID = 6403465438821111171L;

	@Id
	@Column(name = "ID_ATRIBUTO_NEGOCIO")
	private Long idAtributoNegocio;

	@Column(name = "ID_EVENTO", nullable = false)
	private Long idEvento;

	@OneToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "ID_EVENTO", insertable = false, updatable = false)
	private IcsEvento icsEvento;

	@Column(name = "NOMBRE_COLUMNA_NEGOCIO")
	private String nombreColumnaNegocio;

	@Column(name = "NOMBRE_TABLA_DESTINO")
	private String nombreTablaDestino;

	public Long getIdAtributoNegocio() {
		return idAtributoNegocio;
	}

	public void setIdAtributoNegocio(Long idAtributoNegocio) {
		this.idAtributoNegocio = idAtributoNegocio;
	}

	public Long getIdEvento() {
		return idEvento;
	}

	public void setIdEvento(Long idEvento) {
		this.idEvento = idEvento;
	}

	public IcsEvento getIcsEvento() {
		return icsEvento;
	}

	public void setIcsEvento(IcsEvento icsEvento) {
		this.icsEvento = icsEvento;
	}

	public String getNombreColumnaNegocio() {
		return nombreColumnaNegocio;
	}

	public void setNombreColumnaNegocio(String nombreColumnaNegocio) {
		this.nombreColumnaNegocio = nombreColumnaNegocio;
	}

	public String getNombreTablaDestino() {
		return nombreTablaDestino;
	}

	public void setNombreTablaDestino(String nombreTablaDestino) {
		this.nombreTablaDestino = nombreTablaDestino;
	}
}
