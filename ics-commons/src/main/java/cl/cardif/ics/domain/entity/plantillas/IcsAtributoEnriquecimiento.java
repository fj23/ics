package cl.cardif.ics.domain.entity.plantillas;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the ICS_ATRIBUTO_ENRIQUECIMIENTO database table.
 * 
 */
@Entity
@Table(name = "ICS_ATRIBUTO_ENRIQUECIMIENTO")
@NamedQuery(name = "IcsAtributoEnriquecimiento.findAll", query = "SELECT i FROM IcsAtributoEnriquecimiento i")
public class IcsAtributoEnriquecimiento implements Serializable {
	private static final long serialVersionUID = 1798320248308987174L;

	@Id
	@Column(name = "ID_ATRIBUTO_ENRIQUECIMIENTO")
	private Long idAtributoEnriquecimiento;

	@Column(name = "ID_CORE")
	private BigDecimal idCore;

	@Column(name = "NOMBRE_COLUMNA")
	private String nombreColumna;

	@Column(name = "NOMBRE_TABLA")
	private String nombreTabla;

	public Long getIdAtributoEnriquecimiento() {
		return idAtributoEnriquecimiento;
	}

	public void setIdAtributoEnriquecimiento(Long idAtributoEnriquecimiento) {
		this.idAtributoEnriquecimiento = idAtributoEnriquecimiento;
	}

	public BigDecimal getIdCore() {
		return idCore;
	}

	public void setIdCore(BigDecimal idCore) {
		this.idCore = idCore;
	}

	public String getNombreColumna() {
		return nombreColumna;
	}

	public void setNombreColumna(String nombreColumna) {
		this.nombreColumna = nombreColumna;
	}

	public String getNombreTabla() {
		return nombreTabla;
	}

	public void setNombreTabla(String nombreTabla) {
		this.nombreTabla = nombreTabla;
	}
}
