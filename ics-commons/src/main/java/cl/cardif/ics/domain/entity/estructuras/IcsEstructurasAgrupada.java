package cl.cardif.ics.domain.entity.estructuras;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the ICS_ESTRUCTURAS_AGRUPADAS database table.
 * 
 */
@Entity
@Table(name = "ICS_ESTRUCTURAS_AGRUPADAS")
@IdClass(IcsEstructurasAgrupada.class)
@NamedQuery(name = "IcsEstructurasAgrupada.findAll", query = "SELECT i FROM IcsEstructurasAgrupada i")
public class IcsEstructurasAgrupada implements Serializable {
	private static final long serialVersionUID = -7716550236346998031L;

	@Id
	@Column(name = "ID_GRUPO_ESTRUCTURAS")
	private Long idGrupoEstructuras;

	@Id
	@Column(name = "ID_ESTRUCTURA")
	private Long idEstructura;

	@Column(name = "DESCRIPCION_AGRUPACION")
	private String descripcionAgrupacion;

	@Column(name = "INDICE")
	private String indice;

	public IcsEstructurasAgrupada() {
		super();
	}

	public Long getIdGrupoEstructuras() {
		return idGrupoEstructuras;
	}

	public void setIdGrupoEstructuras(Long idGrupoEstructuras) {
		this.idGrupoEstructuras = idGrupoEstructuras;
	}

	public Long getIdEstructura() {
		return idEstructura;
	}

	public void setIdEstructura(Long idEstructura) {
		this.idEstructura = idEstructura;
	}

	public String getDescripcionAgrupacion() {
		return descripcionAgrupacion;
	}

	public void setDescripcionAgrupacion(String descripcionAgrupacion) {
		this.descripcionAgrupacion = descripcionAgrupacion;
	}

	public String getIndice() {
		return indice;
	}

	public void setIndice(String indice) {
		this.indice = indice;
	}
}
