package cl.cardif.ics.domain.entity.carga;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * The persistent class for the ICS_RESUMEN_IMPORTACION database table.
 * 
 */
@Entity
@Table(name = "ICS_RESUMEN_IMPORTACION")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class IcsResumenImportacion implements Serializable {
	private static final long serialVersionUID = -340333300416224099L;

	@EmbeddedId
	private IcsResumenImportacionPK id;

	@Column(name = "NOMBRE_PROCESO")
	private String procesoCarga;

	@Column(name = "NOMBRE_ARCHIVO")
	private String nombreArchivo;

	@Column(name = "FECHA_PROCESO")
	private Date fechaProceso;

	@Column(name = "CANTIDAD_REGISTROS")
	private Integer cantidadRegistros;

	public IcsResumenImportacion() {
		super();
	}

	public IcsResumenImportacionPK getId() {
		return id;
	}

	public void setId(IcsResumenImportacionPK id) {
		this.id = id;
	}

	public String getNombreArchivo() {
		return nombreArchivo;
	}

	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	public Date getFechaProceso() {
		return fechaProceso != null ? (Date) fechaProceso.clone() : null;
	}

	public void setFechaProceso(Date fechaProceso) {
		this.fechaProceso = fechaProceso != null ? (Date) fechaProceso.clone() : null;
	}

	public Integer getCantidadRegistros() {
		return cantidadRegistros;
	}

	public void setCantidadRegistros(Integer cantidadRegistros) {
		this.cantidadRegistros = cantidadRegistros;
	}

	public String getProcesoCarga() {
		return procesoCarga;
	}

	public void setProcesoCarga(String procesoCarga) {
		this.procesoCarga = procesoCarga;
	}
}
