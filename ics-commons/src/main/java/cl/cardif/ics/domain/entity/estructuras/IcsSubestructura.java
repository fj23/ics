package cl.cardif.ics.domain.entity.estructuras;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the ICS_SUBESTRUCTURA database table.
 * 
 */
@Entity
@Table(name = "ICS_SUBESTRUCTURA")
@NamedQuery(name = "IcsSubestructura.findAll", query = "SELECT i FROM IcsSubestructura i")
public class IcsSubestructura {
	@Id
	@SequenceGenerator(name = "DB_SEQ_SUBESTRUCTURA", sequenceName = "SEQ_ICS_SUBESTRUCTURA", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DB_SEQ_SUBESTRUCTURA")
	@Column(name = "ID_SUBESTRUCTURA")
	private long idSubestructura;

	@Column(name = "CODIGO_SUBESTRUCTURA")
	private String codigoSubestructura;

	@Column(name = "DESCRIPCION_SUBESTRUCTURA")
	private String descripcionSubestructura;

	@Column(name = "VIGENCIA", columnDefinition = "CHAR")
	private String vigenciaSubestructura;

	@OneToMany(mappedBy = "icsSubestructura")
	private List<IcsDetalleSubestructura> icsDetalleSubestructuras;

	public IcsSubestructura() {
		super();
	}

	public long getIdSubestructura() {
		return idSubestructura;
	}

	public void setIdSubestructura(long idSubestructura) {
		this.idSubestructura = idSubestructura;
	}

	public String getCodigoSubestructura() {
		return codigoSubestructura;
	}

	public void setCodigoSubestructura(String codigoSubestructura) {
		this.codigoSubestructura = codigoSubestructura;
	}

	public String getDescripcionSubestructura() {
		return descripcionSubestructura;
	}

	public void setDescripcionSubestructura(String descripcionSubestructura) {
		this.descripcionSubestructura = descripcionSubestructura;
	}

	public String getVigenciaSubestructura() {
		return vigenciaSubestructura;
	}

	public void setVigenciaSubestructura(String vigenciaSubestructura) {
		this.vigenciaSubestructura = vigenciaSubestructura;
	}

	public List<IcsDetalleSubestructura> getIcsDetalleSubestructuras() {
		if (icsDetalleSubestructuras != null) {
			return new ArrayList<>(icsDetalleSubestructuras);
		} else {
			return new ArrayList<>();
		}
	}
}
