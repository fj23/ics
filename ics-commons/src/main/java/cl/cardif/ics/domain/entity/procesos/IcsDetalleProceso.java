package cl.cardif.ics.domain.entity.procesos;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import cl.cardif.ics.domain.entity.plantillas.IcsPlantilla;

/**
 * The persistent class for the ICS_DETALLE_PROCESO database table.
 * 
 */
@Entity
@Table(name = "ICS_DETALLE_PROCESO")
@NamedQuery(name = "IcsDetalleProceso.findAll", query = "SELECT i FROM IcsDetalleProceso i")
public class IcsDetalleProceso implements Serializable {
	private static final long serialVersionUID = -6865090231592915830L;

	@Id
	@SequenceGenerator(name = "DB_SEQ_ICS_DETALLE_PROCESO", sequenceName = "SEQ_ICS_DETALLE_PROCESO", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DB_SEQ_ICS_DETALLE_PROCESO")
	@Column(name = "ID_DETALLE_PROCESO")
	private long idDetalleProceso;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_PROCESO")
	private IcsProceso icsProceso;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_PLANTILLA")
	private IcsPlantilla icsPlantilla;

	public IcsDetalleProceso() {
		super();
	}

	public long getIdDetalleProceso() {
		return idDetalleProceso;
	}

	public void setIdDetalleProceso(long idDetalleProceso) {
		this.idDetalleProceso = idDetalleProceso;
	}

	public IcsProceso getIcsProceso() {
		return icsProceso;
	}

	public void setIcsProceso(IcsProceso icsProceso) {
		this.icsProceso = icsProceso;
	}

	public IcsPlantilla getIcsPlantilla() {
		return icsPlantilla;
	}

	public void setIcsPlantilla(IcsPlantilla icsPlantilla) {
		this.icsPlantilla = icsPlantilla;
	}

	@Override
	public String toString() {
		return "IcsDetalleProceso [idDetalleProceso=" + idDetalleProceso + ", icsProceso=" + icsProceso.getIdProceso() + ", icsPlantilla=" + icsPlantilla.getIdPlantilla()
			+ "]";
	}
}
