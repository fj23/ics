package cl.cardif.ics.domain.entity.plantillas;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import cl.cardif.ics.domain.entity.common.IcsEvento;

/**
 * The persistent class for the ICS_ATRIBUTOS_INTG_EVENTO database table.
 * 
 */
@Entity
@Table(name = "ICS_ATRIBUTOS_INTG_EVENTO")
@NamedQuery(name = "IcsAtributosIntgEvento.findAll", query = "SELECT i FROM IcsAtributosIntgEvento i")
public class IcsAtributosIntgEvento implements Serializable {
	private static final long serialVersionUID = 6957294676649822423L;

	@Id
	@Column(name = "ID_INTEGRACION_EVENTO")
	private Long idIntegracionEvento;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_EVENTO")
	private IcsEvento icsEvento;

	@Column(name = "NOMBRE_COLUMNA_INTEGRACION")
	private String nombreColumnaIntegracion;

	@Column(name = "NOMBRE_TABLA_DESTINO")
	private String nombreTablaDestino;

	@Column(name = "FLG_VISIBLE", columnDefinition = "CHAR")
	private String flgVisible;

	@Column(name = "TRADUCCION")
	private String traduccion;
	
	public Long getIdIntegracionEvento() {
		return idIntegracionEvento;
	}

	public void setIdIntegracionEvento(Long idIntegracionEvento) {
		this.idIntegracionEvento = idIntegracionEvento;
	}

	public IcsEvento getIcsEvento() {
		return icsEvento;
	}

	public void setIcsEvento(IcsEvento icsEvento) {
		this.icsEvento = icsEvento;
	}

	public String getNombreColumnaIntegracion() {
		return nombreColumnaIntegracion;
	}

	public void setNombreColumnaIntegracion(String nombreColumnaIntegracion) {
		this.nombreColumnaIntegracion = nombreColumnaIntegracion;
	}

	public String getNombreTablaDestino() {
		return nombreTablaDestino;
	}

	public void setNombreTablaDestino(String nombreTablaDestino) {
		this.nombreTablaDestino = nombreTablaDestino;
	}

	public String getFlgVisible() {
		return flgVisible;
	}

	public void setFlgVisible(String flgVisible) {
		this.flgVisible = flgVisible;
	}

	public String getTraduccion() {
		return traduccion;
	}

	public void setTraduccion(String traduccion) {
		this.traduccion = traduccion;
	}
}
