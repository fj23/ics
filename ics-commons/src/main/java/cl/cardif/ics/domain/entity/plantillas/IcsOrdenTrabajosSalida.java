package cl.cardif.ics.domain.entity.plantillas;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the ICS_ORDEN_TRABAJOS_SALIDA database table.
 * 
 */
@Entity
@Table(name = "ICS_ORDEN_TRABAJOS_SALIDA")
@NamedQuery(name = "IcsOrdenTrabajosSalida.findAll", query = "SELECT i FROM IcsOrdenTrabajosSalida i")
public class IcsOrdenTrabajosSalida implements Serializable {
	private static final long serialVersionUID = 225798292304782031L;

	@Id
	@Column(name = "ID_ATRIBUTO_ANULACION")
	private Long idAtributoAnulacion;

	@Column(name = "TIPO_TRABAJO")
	private String tipoTrabajo;

	@Id
	@Column(name = "ID_TRABAJO")
	private Long idTrabajo;

	@Column(name = "SECUENCIA")
	private Long secuencia;

	public IcsOrdenTrabajosSalida() {
		super();
	}

	public Long getIdAtributoAnulacion() {
		return idAtributoAnulacion;
	}

	public void setIdAtributoAnulacion(Long idAtributoAnulacion) {
		this.idAtributoAnulacion = idAtributoAnulacion;
	}

	public String getTipoTrabajo() {
		return tipoTrabajo;
	}

	public void setTipoTrabajo(String tipoTrabajo) {
		this.tipoTrabajo = tipoTrabajo;
	}

	public Long getIdTrabajo() {
		return idTrabajo;
	}

	public void setIdTrabajo(Long idTrabajo) {
		this.idTrabajo = idTrabajo;
	}

	public Long getSecuencia() {
		return secuencia;
	}

	public void setSecuencia(Long secuencia) {
		this.secuencia = secuencia;
	}
}
