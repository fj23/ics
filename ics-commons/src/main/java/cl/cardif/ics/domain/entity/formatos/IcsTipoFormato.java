package cl.cardif.ics.domain.entity.formatos;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the ICS_TIPO_FORMATO database table.
 * 
 */
@Entity
@Table(name = "ICS_TIPO_FORMATO")
@NamedQuery(name = "IcsTipoFormato.findAll", query = "SELECT i FROM IcsTipoFormato i")
public class IcsTipoFormato  implements Serializable {
	private static final long serialVersionUID = 2808652319672883232L;

	@Id
	@Column(name = "ID_TIPO_FORMATO")
	private Long idTipoFormato;

	@Column(name = "NOMBRE_FORMATO")
	private String nombreFormato;

	@Column(name = "VIGENCIA_FORMATO")
	private String vigenciaFormato;
	
	public Long getIdTipoFormato() {
		return idTipoFormato;
	}

	public void setIdTipoFormato(Long idTipoFormato) {
		this.idTipoFormato = idTipoFormato;
	}

	public String getNombreFormato() {
		return nombreFormato;
	}

	public void setNombreFormato(String nombreFormato) {
		this.nombreFormato = nombreFormato;
	}

	public String getVigenciaFormato() {
		return vigenciaFormato;
	}

	public void setVigenciaFormato(String vigenciaFormato) {
		this.vigenciaFormato = vigenciaFormato;
	}
}
