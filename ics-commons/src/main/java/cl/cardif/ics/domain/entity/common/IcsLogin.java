package cl.cardif.ics.domain.entity.common;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The persistent class for the ICS_LOGIN database table.
 * 
 */
@Entity
@Table(name = "ICS_LOGIN")
public class IcsLogin implements Serializable {
	private static final long serialVersionUID = -6414001662854950436L;

	@Id
	@Column(name = "TOKEN")
	private String token;

	@Column(name = "USERNAME")
	private String userName;

	@Column(name = "LOGIN_DATE")
	private Date loginDate;

	public IcsLogin() {
		super();
		this.loginDate = new Date();
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Date getLoginDate() {
		if (loginDate != null) {
			return (Date)loginDate.clone();
		} else {
			return null;
		}
	}

	public void setLoginDate(Date loginDate) {
		if (loginDate != null) {
			this.loginDate = (Date)loginDate.clone();
		}
	}
}
