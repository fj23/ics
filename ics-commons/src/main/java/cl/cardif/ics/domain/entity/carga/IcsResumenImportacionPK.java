package cl.cardif.ics.domain.entity.carga;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The primary key class for the ICS_RESUMEN_IMPORTACION database table.
 * 
 */
@Embeddable
public class IcsResumenImportacionPK implements Serializable {
	private static final long serialVersionUID = -340333300416224099L;

	@Column(name = "ID_TRANSACCION")
	private Long idTransaccion;

	@Column(name = "FECHA_CARGA")
	private Date fechaCarga;

	public IcsResumenImportacionPK() {
		super();
	}

	public Long getIdTransaccion() {
		return idTransaccion;
	}

	public void setIdTransaccion(Long idTransaccion) {
		this.idTransaccion = idTransaccion;
	}

	public Date getFechaCarga() {
		if (fechaCarga != null) {
			return (Date)fechaCarga.clone();
		} else {
			return null;
		}
	}

	public void setFechaCarga(Date fechaCarga) {
		if (fechaCarga != null) {
			this.fechaCarga = (Date)fechaCarga.clone();
		}
	}
}
