package cl.cardif.ics.domain.entity.procesos;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import cl.cardif.ics.domain.entity.common.IcsEvento;
import cl.cardif.ics.domain.entity.common.IcsSocio;

/**
 * The persistent class for the ICS_RESUMEN_TRANSACCION database table.
 * 
 */
@Entity
@Table(name = "ICS_RESUMEN_TRANSACCION")
@NamedQuery(name = "IcsResumenTransaccion.findAll", query = "SELECT i FROM IcsResumenTransaccion i")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class IcsResumenTransaccion implements Serializable {
	private static final long serialVersionUID = -4086726254365367963L;

	@Id
	@Column(name = "ID_TRANSACCION")
	private Long idTransaccion;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_PROCESO")
	private IcsProceso icsProceso;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_SOCIO")
	private IcsSocio icsSocio;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_EVENTO")
	private IcsEvento icsEvento;

	@Column(name = "FECHA_INICIO")
	private Date fechaInicio;

	@Column(name = "FECHA_FIN")
	private Date fechaFin;

	@Column(name = "NOMBRE_ARCHIVO")
	private String nombreArchivo;

	@Column(name = "ESTADO_DE_TRANSACCION")
	private String estadoDeTransaccion;

	@Column(name = "LINEAS_ORIGINAL")
	private Integer lineasOriginal;

	@Column(name = "LINEAS_ICS_OK")
	private Integer lineasIcsOk;

	@Column(name = "LINEAS_ICS_ERROR")
	private Integer lineasIcsError;

	@Column(name = "LINEAS_CORE_OK")
	private Integer lineasCoreOk;

	@Column(name = "LINEAS_CORE_ERROR")
	private Integer lineasCoreError;

	@Column(name = "ID_EXTERNO")
	private String idExterno;

	public void setIdTransaccion(Long idTransaccion) {
		this.idTransaccion = idTransaccion;
	}

	public Long getIdTransaccion() {
		return idTransaccion;
	}

	public IcsProceso getIcsProceso() {
		return icsProceso;
	}

	public void setIcsProceso(IcsProceso icsProceso) {
		this.icsProceso = icsProceso;
	}

	public IcsSocio getIcsSocio() {
		return icsSocio;
	}

	public void setIcsSocio(IcsSocio icsSocio) {
		this.icsSocio = icsSocio;
	}

	public IcsEvento getIcsEvento() {
		return icsEvento;
	}

	public void setIcsEvento(IcsEvento icsEvento) {
		this.icsEvento = icsEvento;
	}

	public Date getFechaInicio() {
		if (this.fechaInicio != null) {
			return (Date) this.fechaInicio.clone();
		} else {
			return null;
		}
	}

	public void setFechaInicio(Date fechaInicio) {
		if (fechaInicio != null) {
			this.fechaInicio = (Date) fechaInicio.clone();
		}
	}

	public Date getFechaFin() {
		if (this.fechaFin != null) {
			return (Date) this.fechaFin.clone();
		} else {
			return null;
		}
	}

	public void setFechaFin(Date fechaFin) {
		if (fechaFin != null) {
			this.fechaFin = (Date) fechaFin.clone();
		}
	}

	public String getNombreArchivo() {
		return nombreArchivo;
	}

	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	public String getEstadoDeTransaccion() {
		return estadoDeTransaccion;
	}

	public void setEstadoDeTransaccion(String estadoDeTransaccion) {
		this.estadoDeTransaccion = estadoDeTransaccion;
	}

	public Integer getLineasOriginal() {
		return lineasOriginal;
	}

	public void setLineasOriginal(Integer lineasOriginal) {
		this.lineasOriginal = lineasOriginal;
	}

	public Integer getLineasIcsOk() {
		return lineasIcsOk;
	}

	public void setLineasIcsOk(Integer lineasIcsOk) {
		this.lineasIcsOk = lineasIcsOk;
	}

	public Integer getLineasIcsError() {
		return lineasIcsError;
	}

	public void setLineasIcsError(Integer lineasIcsError) {
		this.lineasIcsError = lineasIcsError;
	}

	public Integer getLineasCoreOk() {
		return lineasCoreOk;
	}

	public void setLineasCoreOk(Integer lineasCoreOk) {
		this.lineasCoreOk = lineasCoreOk;
	}

	public Integer getLineasCoreError() {
		return lineasCoreError;
	}

	public void setLineasCoreError(Integer lineasCoreError) {
		this.lineasCoreError = lineasCoreError;
	}

	public String getIdExterno() {
		return idExterno;
	}

	public void setIdExterno(String idExterno) {
		this.idExterno = idExterno;
	}
}
