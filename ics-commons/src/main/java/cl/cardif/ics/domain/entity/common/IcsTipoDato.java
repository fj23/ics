package cl.cardif.ics.domain.entity.common;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the ICS_TIPO_DATO database table.
 * 
 */
@Entity
@Table(name = "ICS_TIPO_DATO")
@NamedQuery(name = "IcsTipoDato.findAll", query = "SELECT i FROM IcsTipoDato i")
public class IcsTipoDato implements Serializable {
	private static final long serialVersionUID = -270550460383293587L;

	@Id
	@Column(name = "ID_TIPO_DATO")
	private long idTipoDato;

	@Column(name = "NOMBRE_TIPO_DATO")
	private String nombreTipoDato;

	@Column(name = "VIGENCIA", columnDefinition = "CHAR")
	private String vigencia;

	public long getIdTipoDato() {
		return idTipoDato;
	}

	public void setIdTipoDato(long idTipoDato) {
		this.idTipoDato = idTipoDato;
	}

	public String getNombreTipoDato() {
		return nombreTipoDato;
	}

	public void setNombreTipoDato(String nombreTipoDato) {
		this.nombreTipoDato = nombreTipoDato;
	}

	public String getVigencia() {
		return vigencia;
	}

	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}
}
