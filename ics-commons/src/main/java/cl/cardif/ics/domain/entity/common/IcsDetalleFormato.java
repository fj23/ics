package cl.cardif.ics.domain.entity.common;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import java.io.Serializable;

/**
 * The persistent class for the ICS_DETALLE_FORMATO database table.
 * 
 */
@Entity
@Table(name = "ICS_DETALLE_FORMATO")
@NamedQuery(name = "IcsDetalleFormato.findAll", query = "SELECT i FROM IcsDetalleFormato i")
public class IcsDetalleFormato implements Serializable {
	private static final long serialVersionUID = -5614325278466876795L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_FORMATO")
	private long idDetalleFormato;

	@NotFound(action = NotFoundAction.IGNORE)
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "ID_TIPO_FORMATO")
	private IcsTipoFormato icsTipoFormato;

	@Column(name = "FORMULA_FORMATO")
	private String formulaFormato;

	@Column(name = "TIPO_DATO")
	private String tipoDato;

	@Column(name = "FORMATO")
	private String formato;

	@Column(name = "NOMBRE_DETALLE_FORMATO")
	private String nombreDetalleFormato;

	public IcsDetalleFormato() {
		super();
	}

	public long getIdDetalleFormato() {
		return idDetalleFormato;
	}

	public void setIdDetalleFormato(long idFormato) {
		this.idDetalleFormato = idFormato;
	}

	public IcsTipoFormato getIcsTipoFormato() {
		return icsTipoFormato;
	}

	public void setIcsTipoFormato(IcsTipoFormato icsTipoFormato) {
		this.icsTipoFormato = icsTipoFormato;
	}

	public String getFormulaFormato() {
		return formulaFormato;
	}

	public void setFormulaFormato(String formulaFormato) {
		this.formulaFormato = formulaFormato;
	}

	public String getTipoDato() {
		return tipoDato;
	}

	public void setTipoDato(String tipoDato) {
		this.tipoDato = tipoDato;
	}

	public String getFormato() {
		return formato;
	}

	public void setFormato(String formato) {
		this.formato = formato;
	}

	public String getNombreDetalleFormato() {
		return nombreDetalleFormato;
	}

	public void setNombreDetalleFormato(String nombreDetalleFormato) {
		this.nombreDetalleFormato = nombreDetalleFormato;
	}
}
