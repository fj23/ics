package cl.cardif.ics.domain.entity.estructuras;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;

@MappedSuperclass
public abstract class DetalleEstructura {
	@Column(name = "TIPO_DATO")
	protected String tipoDato;

	@Column(name = "LARGO")
	protected BigDecimal largo;

	@Column(name = "ORDEN")
	protected BigDecimal orden;

	@OneToOne(fetch = FetchType.LAZY, cascade = { CascadeType.DETACH })
	@JoinColumn(name = "ID_SUBESTRUCTURA")
	protected IcsSubestructura icsSubestructura;

	public String getTipoDato() {
		return tipoDato;
	}

	public void setTipoDato(String tipoDato) {
		this.tipoDato = tipoDato;
	}

	public BigDecimal getLargo() {
		return largo;
	}

	public void setLargo(BigDecimal largo) {
		this.largo = largo;
	}

	public BigDecimal getOrden() {
		return orden;
	}

	public void setOrden(BigDecimal orden) {
		this.orden = orden;
	}

	public IcsSubestructura getIcsSubestructura() {
		return icsSubestructura;
	}

	public void setIcsSubestructura(IcsSubestructura icsSubestructura) {
		this.icsSubestructura = icsSubestructura;
	}
}
