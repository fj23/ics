package cl.cardif.ics.domain.entity.plantillas;

import javax.persistence.*;

/**
 * The persistent class for the ICS_FILTROS_MULTIARCHIVO database table.
 * 
 */
@Entity
@Table(name = "ICS_FILTROS_MULTIARCHIVO")
@NamedQuery(name = "IcsFiltrosMultiArchivo.findAll", query = "SELECT i FROM IcsFiltrosMultiArchivo i")
public class IcsFiltrosMultiArchivo {
    @Id
    @Column(name = "ID_MULTIARCHIVO")
    private Long idMultiArchivo;

    @Column(name = "ID_PLANTILLA")
    private Long idPlantilla;

    @Column(name = "ID_ARCHIVO_1")
    private Long idArchivo1;

    @Column(name = "ID_ARCHIVO_2", nullable = true)
    private Long idArchivo2;

    @Column(name = "ID_ATRIBUTO_1")
    private Long idAtributo1;

    @Column(name = "ID_ATRIBUTO_2", nullable = true)
    private Long idAtributo2;

    @Column(name = "CONSTANTE", nullable = true)
    private String constante;

    @Column(name = "TIPO_FILTRO")
    private Long tipoFiltro;

    @Column(name = "ID_ATRIBUTO_SOCIO")
    private Long idAtributoSocio;

    public IcsFiltrosMultiArchivo() {
    	super();
    }

    public Long getIdMultiArchivo() {
        return idMultiArchivo;
    }

    public void setIdMultiArchivo(Long idMultiArchivo) {
        this.idMultiArchivo = idMultiArchivo;
    }

    public Long getIdPlantilla() {
        return idPlantilla;
    }

    public void setIdPlantilla(Long idPlantilla) {
        this.idPlantilla = idPlantilla;
    }

    public Long getIdArchivo1() {
        return idArchivo1;
    }

    public void setIdArchivo1(Long idArchivo1) {
        this.idArchivo1 = idArchivo1;
    }

    public Long getIdArchivo2() {
        return idArchivo2;
    }

    public void setIdArchivo2(Long idArchivo2) {
        this.idArchivo2 = idArchivo2;
    }

    public Long getIdAtributo1() {
        return idAtributo1;
    }

    public void setIdAtributo1(Long idAtributo1) {
        this.idAtributo1 = idAtributo1;
    }

    public Long getIdAtributo2() {
        return idAtributo2;
    }

    public void setIdAtributo2(Long idAtributo2) {
        this.idAtributo2 = idAtributo2;
    }

    public String getConstante() {
        return constante;
    }

    public void setConstante(String constante) {
        this.constante = constante;
    }

    public Long getTipoFiltro() {
        return tipoFiltro;
    }

    public void setTipoFiltro(Long tipoFiltro) {
        this.tipoFiltro = tipoFiltro;
    }

	public Long getIdAtributoSocio() {
		return idAtributoSocio;
	}

	public void setIdAtributoSocio(Long idAtributoSocio) {
		this.idAtributoSocio = idAtributoSocio;
	}
}
