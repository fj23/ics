package cl.cardif.ics.domain.entity.plantillas;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the ICS_ATRIBUTOS_SALIDA_ESTRUC database table.
 * 
 */
@Entity
@Table(name = "ICS_ATRIBUTOS_SALIDA_ESTRUC")
@NamedQuery(name = "IcsAtributoSalidaEstruc.findAll", query = "SELECT i FROM IcsAtributoSalidaEstruc i")
public class IcsAtributoSalidaEstruc implements Serializable {
	private static final long serialVersionUID = -8469361403503800405L;

	@Id
	@Column(name = "ID_ATRIBUTO_EST")
	private Long idAtributoEst;

	@Column(name = "NOMBRE_COLUMNA")
	private String nombreColumna;

	@Column(name = "NOMBRE_TABLA")
	private String nombreTabla;

	public Long getIdAtributoEst() {
		return idAtributoEst;
	}

	public void setIdAtributoEst(Long idAtributoEst) {
		this.idAtributoEst = idAtributoEst;
	}

	public String getNombreColumna() {
		return nombreColumna;
	}

	public void setNombreColumna(String nombreColumna) {
		this.nombreColumna = nombreColumna;
	}

	public String getNombreTabla() {
		return nombreTabla;
	}

	public void setNombreTabla(String nombreTabla) {
		this.nombreTabla = nombreTabla;
	}
	
	
}
