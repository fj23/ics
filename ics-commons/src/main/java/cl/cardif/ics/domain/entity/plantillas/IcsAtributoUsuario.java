package cl.cardif.ics.domain.entity.plantillas;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * The persistent class for the ICS_ATRIBUTO_USUARIO database table.
 * 
 */
@Entity
@Table(name = "ICS_ATRIBUTO_USUARIO")
@NamedQuery(name = "IcsAtributoUsuario.findAll", query = "SELECT i FROM IcsAtributoUsuario i")
public class IcsAtributoUsuario implements Serializable {
	private static final long serialVersionUID = -5614325278566876795L;
	
	@Id
	@Column(name = "ID_ATRIBUTO_USUARIO")
	private Long idAtributoUsuario;

	@Column(name = "ID_ATRIBUTO_NEGOCIO")
	private Long idAtributosNegocio;

	@OneToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "ID_ATRIBUTO_NEGOCIO", insertable = false, updatable = false, nullable = false)
	private IcsAtributoNegocio icsAtributoNegocio;

	@Column(name = "TIPO_PERSONA", nullable = true)
	private String tipoPersona;

	@Column(name = "NOMBRE_COLUMNA", nullable = true)
	private String nombreColumna;

	@Column(name = "FLG_CARDINALIDAD", columnDefinition = "CHAR")
	private String flgCardinalidad;

	public Long getIdAtributoUsuario() {
		return idAtributoUsuario;
	}

	public void setIdAtributoUsuario(Long idAtributoUsuario) {
		this.idAtributoUsuario = idAtributoUsuario;
	}

	public Long getIdAtributosNegocio() {
		return idAtributosNegocio;
	}

	public void setIdAtributosNegocio(Long idAtributosNegocio) {
		this.idAtributosNegocio = idAtributosNegocio;
	}

	public IcsAtributoNegocio getIcsAtributoNegocio() {
		return icsAtributoNegocio;
	}

	public void setIcsAtributoNegocio(IcsAtributoNegocio icsAtributoNegocio) {
		this.icsAtributoNegocio = icsAtributoNegocio;
	}

	public String getTipoPersona() {
		return tipoPersona;
	}

	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	public String getNombreColumna() {
		return nombreColumna;
	}

	public void setNombreColumna(String nombreColumna) {
		this.nombreColumna = nombreColumna;
	}

	public String getFlgCardinalidad() {
		return flgCardinalidad;
	}

	public void setFlgCardinalidad(String flgCardinalidad) {
		this.flgCardinalidad = flgCardinalidad;
	}
}
