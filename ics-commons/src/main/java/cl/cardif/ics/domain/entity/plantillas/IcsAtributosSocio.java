package cl.cardif.ics.domain.entity.plantillas;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import cl.cardif.ics.domain.entity.estructuras.IcsDetalleEstructura;

/**
 * The persistent class for the ICS_ATRIBUTOS_SOCIO database table.
 * 
 */
@Entity
@Table(name = "ICS_ATRIBUTOS_SOCIO")
@NamedQuery(name = "IcsAtributosSocio.findAll", query = "SELECT i FROM IcsAtributosSocio i")
public class IcsAtributosSocio {
	@Id
	@Column(name = "ID_ATRIBUTOS_SOCIO")
	private Long idAtributosSocio;

	@Column(name = "ID_ATRIBUTO_NEGOCIO")
	private Long idAtributoNegocio;

	@Column(name = "ID_ATRIBUTO", nullable = true)
	private Long idAtributo;

	@Column(name = "COLUMNA_OBLIGATORIA", columnDefinition = "CHAR")
	private String columnaObligatoria;

	@Column(name = "ID_DETALLE_SUBESTRUCTURA", nullable = true)
	private Long idDetalleSubEstructura;

	@Column(name = "ID_DETALLE_ESTRUCTURA", nullable = true)
	private Long idDetalleEstructura;

	@Column(name = "ID_ARCHIVO")
	private Long idArchivo;
	
	@Column(name = "FLG_ENCRIPTACION", columnDefinition = "CHAR")
	private String encriptado;

	@OneToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "ID_ATRIBUTO", insertable = false, updatable = false, nullable = false)
	private IcsAtributos icsAtributos;

	@OneToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "ID_ARCHIVO", insertable = false, updatable = false, nullable = false)
	private IcsArchivo icsArchivo;

	@OneToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "ID_ATRIBUTO_NEGOCIO", insertable = false, updatable = false, nullable = false)
	private IcsAtributoNegocio icsAtributoNegocio;

	@OneToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "ID_DETALLE_ESTRUCTURA", insertable = false, updatable = false, nullable = false)
	private IcsDetalleEstructura icsDetalleEstructura;

	@OneToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "ID_ATRIBUTO_USUARIO", insertable = false, updatable = false, nullable = false)
	private IcsAtributoUsuario icsAtributoUsuario;

	@Column(name = "LOTE", nullable = true)
	private Long lote;

	public IcsAtributosSocio() {
		super();
	}

	public Long getIdAtributosSocio() {
		return idAtributosSocio;
	}

	public void setIdAtributosSocio(Long idAtributosSocio) {
		this.idAtributosSocio = idAtributosSocio;
	}

	public Long getIdAtributoNegocio() {
		return idAtributoNegocio;
	}

	public void setIdAtributoNegocio(Long idAtributoNegocio) {
		this.idAtributoNegocio = idAtributoNegocio;
	}

	public Long getIdAtributo() {
		return idAtributo;
	}

	public void setIdAtributo(Long idAtributo) {
		this.idAtributo = idAtributo;
	}

	public String getColumnaObligatoria() {
		return columnaObligatoria;
	}

	public void setColumnaObligatoria(String columnaObligatoria) {
		this.columnaObligatoria = columnaObligatoria;
	}

	public Long getIdDetalleSubEstructura() {
		return idDetalleSubEstructura;
	}

	public void setIdDetalleSubEstructura(Long idDetalleSubEstructura) {
		this.idDetalleSubEstructura = idDetalleSubEstructura;
	}

	public Long getIdDetalleEstructura() {
		return idDetalleEstructura;
	}

	public void setIdDetalleEstructura(Long idDetalleEstructura) {
		this.idDetalleEstructura = idDetalleEstructura;
	}

	public Long getIdArchivo() {
		return idArchivo;
	}

	public void setIdArchivo(Long idArchivo) {
		this.idArchivo = idArchivo;
	}

	public String getEncriptado() {
		return encriptado;
	}

	public void setEncriptado(String encriptado) {
		this.encriptado = encriptado;
	}

	public IcsAtributos getIcsAtributos() {
		return icsAtributos;
	}

	public void setIcsAtributos(IcsAtributos icsAtributos) {
		this.icsAtributos = icsAtributos;
	}

	public IcsArchivo getIcsArchivo() {
		return icsArchivo;
	}

	public void setIcsArchivo(IcsArchivo icsArchivo) {
		this.icsArchivo = icsArchivo;
	}

	public IcsAtributoNegocio getIcsAtributoNegocio() {
		return icsAtributoNegocio;
	}

	public void setIcsAtributoNegocio(IcsAtributoNegocio icsAtributoNegocio) {
		this.icsAtributoNegocio = icsAtributoNegocio;
	}

	public IcsDetalleEstructura getIcsDetalleEstructura() {
		return icsDetalleEstructura;
	}

	public void setIcsDetalleEstructura(IcsDetalleEstructura icsDetalleEstructura) {
		this.icsDetalleEstructura = icsDetalleEstructura;
	}

	public IcsAtributoUsuario getIcsAtributoUsuario() {
		return icsAtributoUsuario;
	}

	public void setIcsAtributoUsuario(IcsAtributoUsuario icsAtributoUsuario) {
		this.icsAtributoUsuario = icsAtributoUsuario;
	}

	public Long getLote() {
		return lote;
	}

	public void setLote(Long lote) {
		this.lote = lote;
	}
}
