package cl.cardif.ics.domain.entity.plantillas;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import cl.cardif.ics.domain.entity.common.IcsTipoFormula;
import cl.cardif.ics.domain.entity.formatos.IcsDetalleFormato;
import cl.cardif.ics.domain.entity.homologaciones.IcsTipoHomologacion;

/**
 * The persistent class for the ICS_ATRIBUTOS_INTG database table.
 * 
 */
@Entity
@Table(name = "ICS_ATRIBUTOS_INTG")
@NamedQuery(name = "IcsAtributosIntg.findAll", query = "SELECT i FROM IcsAtributosIntg i")
public class IcsAtributosIntg implements Serializable {
	private static final long serialVersionUID = -5614325278466876797L;

	@Id
	@Column(name = "ID_ATRIBUTO_INTEGRACION")
	private Long idAtributoIntegracion;

	@Column(name = "FLG_OBLIGATORIO", columnDefinition = "CHAR", nullable = true)
	private String flgObligatorio;

	@Column(name = "FLG_VALIDAR", columnDefinition = "CHAR", nullable = true)
	private String flgValidar;

	@NotFound(action = NotFoundAction.IGNORE)
	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "ID_ATRIBUTO_ENRIQUECIMIENTO", nullable = false, insertable = false, updatable = false)
	private IcsAtributoEnriquecimiento icsAtributoEnriquecimiento;

	@NotFound(action = NotFoundAction.IGNORE)
	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "ID_ATRIBUTO_NEGOCIO", nullable = false, insertable = false, updatable = false)
	private IcsAtributoNegocio icsAtributoNegocio;

	@NotFound(action = NotFoundAction.IGNORE)
	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "ID_DE_FORMATO", referencedColumnName = "ID_FORMATO", nullable = true, insertable = false, updatable = false)
	private IcsDetalleFormato icsDetalleFormato;

	@NotFound(action = NotFoundAction.IGNORE)
	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "ID_DE_TIPO_HOMOLOGACION", nullable = false, insertable = false, updatable = false)
	private IcsTipoHomologacion icsTipoHomologacion;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "ID_INTEGRACION_EVENTO", nullable = false, insertable = false, updatable = false)
	private IcsAtributosIntgEvento icsAtributosIntgEvento;

	@NotFound(action = NotFoundAction.IGNORE)
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "ID_PLANTILLA", nullable = false, insertable = false, updatable = false)
	private IcsPlantilla icsPlantilla;

	@NotFound(action = NotFoundAction.IGNORE)
	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "ID_TIPO_FORMULA", nullable = false, insertable = false, updatable = false)
	private IcsTipoFormula icsTipoFormula;

	@NotFound(action = NotFoundAction.IGNORE)
	@OneToMany(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_ATRIBUTO_INTEGRACION", nullable = false, insertable = false, updatable = false)
	private List<IcsOrdenTrabajosIntg> icsOrdenTrabajosIntg;

	@NotFound(action = NotFoundAction.IGNORE)
	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "ID_ATRIBUTO", nullable = false, insertable = false, updatable = false)
	private IcsAtributos icsAtributos;

	@NotFound(action = NotFoundAction.IGNORE)
	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "ID_ATRIBUTO_USUARIO", nullable = false, insertable = false, updatable = false)
	private IcsAtributoUsuario icsAtributoUsuario;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "ID_ATRIBUTO_USER", nullable = false, insertable = false, updatable = false)
	private IcsAtributoUserIntg icsAtributoUserIntg;

	public IcsAtributosIntg() {
		super();
	}

	public Long getIdAtributoIntegracion() {
		return idAtributoIntegracion;
	}

	public void setIdAtributoIntegracion(Long idAtributoIntegracion) {
		this.idAtributoIntegracion = idAtributoIntegracion;
	}

	public String getFlgObligatorio() {
		return flgObligatorio;
	}

	public void setFlgObligatorio(String flgObligatorio) {
		this.flgObligatorio = flgObligatorio;
	}

	public String getFlgValidar() {
		return flgValidar;
	}

	public void setFlgValidar(String flgValidar) {
		this.flgValidar = flgValidar;
	}

	public IcsAtributoEnriquecimiento getIcsAtributoEnriquecimiento() {
		return icsAtributoEnriquecimiento;
	}

	public void setIcsAtributoEnriquecimiento(IcsAtributoEnriquecimiento icsAtributoEnriquecimiento) {
		this.icsAtributoEnriquecimiento = icsAtributoEnriquecimiento;
	}

	public IcsAtributoNegocio getIcsAtributoNegocio() {
		return icsAtributoNegocio;
	}

	public void setIcsAtributoNegocio(IcsAtributoNegocio icsAtributoNegocio) {
		this.icsAtributoNegocio = icsAtributoNegocio;
	}

	public IcsDetalleFormato getIcsDetalleFormato() {
		return icsDetalleFormato;
	}

	public void setIcsDetalleFormato(IcsDetalleFormato icsDetalleFormato) {
		this.icsDetalleFormato = icsDetalleFormato;
	}

	public IcsTipoHomologacion getIcsTipoHomologacion() {
		return icsTipoHomologacion;
	}

	public void setIcsTipoHomologacion(IcsTipoHomologacion icsTipoHomologacion) {
		this.icsTipoHomologacion = icsTipoHomologacion;
	}

	public IcsAtributosIntgEvento getIcsAtributosIntgEvento() {
		return icsAtributosIntgEvento;
	}

	public void setIcsAtributosIntgEvento(IcsAtributosIntgEvento icsAtributosIntgEvento) {
		this.icsAtributosIntgEvento = icsAtributosIntgEvento;
	}

	public IcsPlantilla getIcsPlantilla() {
		return icsPlantilla;
	}

	public void setIcsPlantilla(IcsPlantilla icsPlantilla) {
		this.icsPlantilla = icsPlantilla;
	}

	public IcsTipoFormula getIcsTipoFormula() {
		return icsTipoFormula;
	}

	public void setIcsTipoFormula(IcsTipoFormula icsTipoFormula) {
		this.icsTipoFormula = icsTipoFormula;
	}

	public List<IcsOrdenTrabajosIntg> getIcsOrdenTrabajosIntg() {
		if (icsOrdenTrabajosIntg != null) {
			return new ArrayList<>(icsOrdenTrabajosIntg);
		} else {
			return new ArrayList<>();
		}
	}

	public IcsAtributos getIcsAtributos() {
		return icsAtributos;
	}

	public void setIcsAtributos(IcsAtributos icsAtributos) {
		this.icsAtributos = icsAtributos;
	}

	public IcsAtributoUserIntg getIcsAtributoUserIntg() {
		return icsAtributoUserIntg;
	}

	public void setIcsAtributoUserIntg(IcsAtributoUserIntg icsAtributoUserIntg) {
		this.icsAtributoUserIntg = icsAtributoUserIntg;
	}

	public IcsAtributoUsuario getIcsAtributoUsuario() {
		return icsAtributoUsuario;
	}

	public void setIcsAtributoUsuario(IcsAtributoUsuario icsAtributoUsuario) {
		this.icsAtributoUsuario = icsAtributoUsuario;
	}
}
