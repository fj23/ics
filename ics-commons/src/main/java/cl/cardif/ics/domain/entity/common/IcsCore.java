package cl.cardif.ics.domain.entity.common;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * The persistent class for the ICS_CORE database table.
 * 
 */
@Entity
@Table(name = "ICS_CORE")
@NamedQuery(name = "IcsCore.findAll", query = "SELECT i FROM IcsCore i")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class IcsCore implements Serializable {
	private static final long serialVersionUID = -2249090152765016322L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_CORE")
	private Long idCore;

	@Column(name = "FLG_VISIBLE", columnDefinition = "CHAR")
	private String flgVisible;

	@Column(name = "NOMBRE_CORE")
	private String nombreCore;

	@Column(name = "VIGENCIA_CORE", columnDefinition = "CHAR")
	private String vigenciaCore;

	public Long getIdCore() {
		return idCore;
	}

	public void setIdCore(Long idCore) {
		this.idCore = idCore;
	}

	public String getFlgVisible() {
		return flgVisible;
	}

	public void setFlgVisible(String flgVisible) {
		this.flgVisible = flgVisible;
	}

	public String getNombreCore() {
		return nombreCore;
	}

	public void setNombreCore(String nombreCore) {
		this.nombreCore = nombreCore;
	}

	public String getVigenciaCore() {
		return vigenciaCore;
	}

	public void setVigenciaCore(String vigenciaCore) {
		this.vigenciaCore = vigenciaCore;
	}
}
