package cl.cardif.ics.domain.entity.procesos;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the ICS_RESPALDO_INTERFACES database table.
 * 
 */
@Entity
@Table(name = "ICS_RESPALDO_INTERFACES")
@NamedQuery(name = "IcsRespaldoInterfaces.findAll", query = "SELECT i FROM IcsRespaldoInterfaces i")
public class IcsRespaldoInterfaces implements Serializable {

	private static final long serialVersionUID = 8138858745688932076L;

	@Id
	@Column(name = "ID_TRANSACCION", nullable = false)
	private Long idTrasaccion;

	@Column(name = "ID_SOCIO", nullable = false)
	private Long idSocio;

	@Column(name = "FECHA_INGRESO", nullable = false)
	private Date fechaIngreso;

	@Column(name = "ESTADO", nullable = false)
	private String estado;

	@Column(name = "ARCHIVO", columnDefinition = "CLOB NOT NULL")
	@Lob
	@Basic(fetch = FetchType.LAZY)
	private String archivo;

	@Id
	@Column(name = "ID_ARCHIVO", nullable = false)
	private Long idArchivo;

	@Column(name = "ARCHIVO_ORIGINAL", columnDefinition = "BLOB")
	@Lob
	@Basic(fetch = FetchType.LAZY)
	private byte[] archivoOriginal;

	@Column(name = "ID_RESPALDO")
	private Long idRespaldo;

	public IcsRespaldoInterfaces() {
		super();
	}

	public Long getIdTrasaccion() {
		return idTrasaccion;
	}

	public void setIdTrasaccion(Long idTrasaccion) {
		this.idTrasaccion = idTrasaccion;
	}

	public Long getIdSocio() {
		return idSocio;
	}

	public void setIdSocio(Long idSocio) {
		this.idSocio = idSocio;
	}

	public Date getFechaIngreso() {
		if (this.fechaIngreso != null) {
			return (Date)this.fechaIngreso.clone();
		} else {
			return null;
		}
		
	}

	public void setFechaIngreso(Date fechaIngreso) {
		if (fechaIngreso != null) {
			this.fechaIngreso = (Date)fechaIngreso.clone();
		}
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getArchivo() {
		return archivo;
	}

	public void setArchivo(String archivo) {
		this.archivo = archivo;
	}

	public Long getIdArchivo() {
		return idArchivo;
	}

	public void setIdArchivo(Long idArchivo) {
		this.idArchivo = idArchivo;
	}

	public byte[] getArchivoOriginal() {
		if (this.archivoOriginal != null) {
			return (byte[])this.archivoOriginal.clone();
		} else {
			return new byte[]{};
		}
	}

	public void setArchivoOriginal(byte[] archivoOriginal) {
		if (archivoOriginal != null) {
			this.archivoOriginal = (byte[])archivoOriginal.clone();
		}
	}

	public Long getIdRespaldo() {
		return idRespaldo;
	}

	public void setIdRespaldo(Long idRespaldo) {
		this.idRespaldo = idRespaldo;
	}
}
