package cl.cardif.ics.domain.entity.common;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * The persistent class for the ICS_SOCIO database table.
 * 
 */
@Entity
@Table(name = "ICS_SOCIO")
@NamedQuery(name = "IcsSocio.findAll", query = "SELECT i FROM IcsSocio i")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class IcsSocio implements Serializable {
	private static final long serialVersionUID = -2350885726688167140L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_SOCIO")
	private long idSocio;

	@Column(name = "FLG_VISIBLE", columnDefinition = "CHAR")
	private String flgVisible;

	@Column(name = "NOMBRE_SOCIO")
	private String nombreSocio;

	@Column(name = "VIGENCIA_SOCIO", columnDefinition = "CHAR")
	private String vigenciaSocio;

	public long getIdSocio() {
		return idSocio;
	}

	public void setIdSocio(long idSocio) {
		this.idSocio = idSocio;
	}

	public String getFlgVisible() {
		return flgVisible;
	}

	public void setFlgVisible(String flgVisible) {
		this.flgVisible = flgVisible;
	}

	public String getNombreSocio() {
		return nombreSocio;
	}

	public void setNombreSocio(String nombreSocio) {
		this.nombreSocio = nombreSocio;
	}

	public String getVigenciaSocio() {
		return vigenciaSocio;
	}

	public void setVigenciaSocio(String vigenciaSocio) {
		this.vigenciaSocio = vigenciaSocio;
	}
}
