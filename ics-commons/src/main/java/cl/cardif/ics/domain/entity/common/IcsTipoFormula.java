package cl.cardif.ics.domain.entity.common;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the ICS_TIPO_FORMULA database table.
 * 
 */
@Entity
@Table(name = "ICS_TIPO_FORMULA")
@NamedQuery(name = "IcsTipoFormula.findAll", query = "SELECT i FROM IcsTipoFormula i")
public class IcsTipoFormula implements Serializable {
	private static final long serialVersionUID = 8754130460376303998L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_TIPO_FORMULA")
	private Long idTipoFormula;

	@Column(name = "ESTRUCTURA_FORMULA")
	private String estructuraFormula;

	@Column(name = "NOMBRE_FORMULA")
	private String nombreFormula;

	@Column(name = "TIPO_DATO_RESULTANTE")
	private String tipoDatoResultante;

	@Column(name = "VIGENCIA_FORMULA", columnDefinition = "CHAR")
	private String vigenciaFormula;
	
	public Long getIdTipoFormula() {
		return idTipoFormula;
	}

	public void setIdTipoFormula(Long idTipoFormula) {
		this.idTipoFormula = idTipoFormula;
	}

	public String getEstructuraFormula() {
		return estructuraFormula;
	}

	public void setEstructuraFormula(String estructuraFormula) {
		this.estructuraFormula = estructuraFormula;
	}

	public String getNombreFormula() {
		return nombreFormula;
	}

	public void setNombreFormula(String nombreFormula) {
		this.nombreFormula = nombreFormula;
	}

	public String getTipoDatoResultante() {
		return tipoDatoResultante;
	}

	public void setTipoDatoResultante(String tipoDatoResultante) {
		this.tipoDatoResultante = tipoDatoResultante;
	}

	public String getVigenciaFormula() {
		return vigenciaFormula;
	}

	public void setVigenciaFormula(String vigenciaFormula) {
		this.vigenciaFormula = vigenciaFormula;
	}
}
