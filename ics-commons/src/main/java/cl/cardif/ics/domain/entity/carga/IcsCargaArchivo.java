package cl.cardif.ics.domain.entity.carga;

import java.sql.Blob;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the ICS_CARGA_ARCHIVO database table.
 * 
 */
@Entity
@Table(name = "ICS_CARGA_ARCHIVO")
@NamedQuery(name = "IcsCargaArchivo.findAll", query = "SELECT i FROM IcsCargaArchivo i")
public class IcsCargaArchivo {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DB_SEQ_ICS_CARGA_ARCHIVO")
	@SequenceGenerator(name = "DB_SEQ_ICS_CARGA_ARCHIVO", sequenceName = "SEQ_ICS_CARGA_ARCHIVO", allocationSize = 1)
	@Column(name = "ID_CARGA_ARCHIVO", updatable = false, nullable = false)
	private long idCarga;

	@Column(name = "NOMBRE_CARGA")
	private String nombreCarga;

	@Column(name = "FECHA_RECEPCION_CARGA")
	private Date fechaRecepcionCarga;

	@Column(name = "CODIGO_CARGA")
	private String codigoCarga;

	@Column(name = "OBSERVACION_CARGA")
	private String observacionCarga;

	@Column(name = "ARCHIVO_CARGA")
	@Lob
	private Blob archivoCarga;

	@Column(name = "NOMBRE_ARCHIVO")
	private String nombreArchivo;

	@Column(name = "EXTENSION_ARCHIVO")
	private String extensionArchivo;

	@Column(name = "ID_PROCESO")
	private long idProceso;

	public IcsCargaArchivo() {
		super();
	}

	public long getIdCarga() {
		return idCarga;
	}

	public void setIdCarga(long idCarga) {
		this.idCarga = idCarga;
	}

	public String getNombreCarga() {
		return nombreCarga;
	}

	public void setNombreCarga(String nombreCarga) {
		this.nombreCarga = nombreCarga;
	}

	public Date getFechaRecepcionCarga() {
		if (fechaRecepcionCarga != null) {
			return (Date) fechaRecepcionCarga.clone();
		} else {
			return null;
		}
	}

	public void setFechaRecepcionCarga(Date fechaRecepcionCarga) {
		if (fechaRecepcionCarga != null) {
			this.fechaRecepcionCarga = (Date) fechaRecepcionCarga.clone();
		}
	}

	public String getCodigoCarga() {
		return codigoCarga;
	}

	public void setCodigoCarga(String codigoCarga) {
		this.codigoCarga = codigoCarga;
	}

	public String getObservacionCarga() {
		return observacionCarga;
	}

	public void setObservacionCarga(String observacionCarga) {
		this.observacionCarga = observacionCarga;
	}

	public Blob getArchivoCarga() {
		return archivoCarga;
	}

	public void setArchivoCarga(Blob archivoCarga) {
		this.archivoCarga = archivoCarga;
	}

	public String getNombreArchivo() {
		return nombreArchivo;
	}

	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	public String getExtensionArchivo() {
		return extensionArchivo;
	}

	public void setExtensionArchivo(String extensionArchivo) {
		this.extensionArchivo = extensionArchivo;
	}

	public long getIdProceso() {
		return idProceso;
	}

	public void setIdProceso(long idProceso) {
		this.idProceso = idProceso;
	}
}
