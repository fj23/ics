package cl.cardif.ics.domain.entity.common;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * The persistent class for the ICS_TIPO_EXTENSION database table.
 * 
 */
@Entity
@Table(name = "ICS_TIPO_EXTENSION")
@NamedQuery(name = "IcsTipoExtension.findAll", query = "SELECT i FROM IcsTipoExtension i")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class IcsTipoExtension implements Serializable {
	private static final long serialVersionUID = 6991001098384933419L;

	@Id
	@Column(name = "ID_EXTENSION")
	private long idExtension;

	@Column(name = "FORMATO_EXTENSION")
	private String formatoExtension;

	@Column(name = "NOMBRE_EXTENSION")
	private String nombreExtension;

	@Column(name = "VIGENCIA", columnDefinition = "CHAR")
	private String vigencia;

	@Column(name = "FLG_VISIBLE_INTG", columnDefinition = "CHAR")
	private String flgVisibleIntg;
	
	public long getIdExtension() {
		return idExtension;
	}

	public void setIdExtension(long idExtension) {
		this.idExtension = idExtension;
	}

	public String getFormatoExtension() {
		return formatoExtension;
	}

	public void setFormatoExtension(String formatoExtension) {
		this.formatoExtension = formatoExtension;
	}

	public String getNombreExtension() {
		return nombreExtension;
	}

	public void setNombreExtension(String nombreExtension) {
		this.nombreExtension = nombreExtension;
	}

	public String getVigencia() {
		return vigencia;
	}

	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}

	public String getFlgVisibleIntg() {
		return flgVisibleIntg;
	}

	public void setFlgVisibleIntg(String flgVisibleIntg) {
		this.flgVisibleIntg = flgVisibleIntg;
	}
}
