package cl.cardif.ics.domain.entity.homologaciones;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the ICS_DETALLE_HOMOLOGACION database table.
 * 
 */
@Entity
@Table(name = "ICS_DETALLE_HOMOLOGACION")
@NamedQuery(name = "IcsDetalleHomologacion.findAll", query = "SELECT i FROM IcsDetalleHomologacion i")
public class IcsDetalleHomologacion {
	@Id
	@SequenceGenerator(name = "DB_SEQ_ICS_DETALLE_HOMOLOGACION", sequenceName = "SEQ_ICS_DETALLE_HOMOLOGACION", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DB_SEQ_ICS_DETALLE_HOMOLOGACION")
	@Column(name = "ID_HOMOLOGACION")
	private long idHomologacion;

	@Column(name = "VALOR_CORE")
	private String valorCore;

	@Column(name = "VALOR_SOCIO")
	private String valorSocio;

	@Column(name = "VIGENCIA", columnDefinition = "CHAR")
	private String vigenciaHomologacion;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_DE_TIPO_HOMOLOGACION")
	private IcsTipoHomologacion icsTipoHomologacion;

	public IcsDetalleHomologacion() {
		super();
	}

	public long getIdHomologacion() {
		return idHomologacion;
	}

	public void setIdHomologacion(long idHomologacion) {
		this.idHomologacion = idHomologacion;
	}

	public String getValorCore() {
		return valorCore;
	}

	public void setValorCore(String valorCore) {
		this.valorCore = valorCore;
	}

	public String getValorSocio() {
		return valorSocio;
	}

	public void setValorSocio(String valorSocio) {
		this.valorSocio = valorSocio;
	}

	public String getVigenciaHomologacion() {
		return vigenciaHomologacion;
	}

	public void setVigenciaHomologacion(String vigenciaHomologacion) {
		this.vigenciaHomologacion = vigenciaHomologacion;
	}

	public IcsTipoHomologacion getIcsTipoHomologacion() {
		return icsTipoHomologacion;
	}

	public void setIcsTipoHomologacion(IcsTipoHomologacion icsTipoHomologacion) {
		this.icsTipoHomologacion = icsTipoHomologacion;
	}
}
