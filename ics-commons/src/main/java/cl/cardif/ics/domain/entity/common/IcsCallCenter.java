package cl.cardif.ics.domain.entity.common;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * The persistent class for the ICS_CALL_CENTER database table.
 * 
 */
@Entity
@Table(name = "ICS_CALL_CENTER")
@NamedQuery(name = "IcsCallCenter.findAll", query = "SELECT i FROM IcsCallCenter i")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class IcsCallCenter implements Serializable {
	private static final long serialVersionUID = 5573614914614883179L;

	@Id
	@Column(name = "ID_CALL_CENTER")
	private long idCallCenter;

	@Column(name = "NOMBRE_CALL_CENTER")
	private String nombreCallCenter;

	@Column(name = "FECHA_CREACION")
	private Date fechaCreacion;

	@Column(name = "FLG_ACTIVO", columnDefinition = "CHAR")
	private String flgActivo;

	public long getIdCallCenter() {
		return idCallCenter;
	}

	public void setIdCallCenter(long idCallCenter) {
		this.idCallCenter = idCallCenter;
	}

	public String getNombreCallCenter() {
		return nombreCallCenter;
	}

	public void setNombreCallCenter(String nombreCallCenter) {
		this.nombreCallCenter = nombreCallCenter;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getFlgActivo() {
		return flgActivo;
	}

	public void setFlgActivo(String flgActivo) {
		this.flgActivo = flgActivo;
	}
}
