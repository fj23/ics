package cl.cardif.ics.domain.entity.procesos;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * The persistent class for the ICS_JOBS database table.
 * 
 */
@Entity
@Table(name = "ICS_JOBS")
@NamedQuery(name = "IcsJobs.findAll", query = "SELECT i FROM IcsJobs i")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class IcsJobs implements Serializable {

	private static final long serialVersionUID = 2451816873678357601L;

	@Id
	@Column(name = "ID_JOB")
	private Long idTransaccion;

	@Column(name = "NOMBRE_JOB")
	private String nombreJob;

	@Column(name = "FECHA_EJECUCION")
	private Date fechaEjecucion;

	@Column(name = "OBSERVACION")
	private String observacion;

	public Long getIdTransaccion() {
		return idTransaccion;
	}

	public void setIdTransaccion(Long idTransaccion) {
		this.idTransaccion = idTransaccion;
	}

	public String getNombreJob() {
		return nombreJob;
	}

	public void setNombreJob(String nombreJob) {
		this.nombreJob = nombreJob;
	}

	public Date getFechaEjecucion() {
		if (this.fechaEjecucion != null) {
			return (Date)this.fechaEjecucion.clone();	
		} else {
			return null;
		}
	}

	public void setFechaEjecucion(Date fechaEjecucion) {
		if (fechaEjecucion != null) {
			this.fechaEjecucion = (Date)fechaEjecucion.clone();
		}
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
}
