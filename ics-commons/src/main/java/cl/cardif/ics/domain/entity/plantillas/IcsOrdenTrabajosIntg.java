package cl.cardif.ics.domain.entity.plantillas;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the ICS_ORDEN_TRABAJOS_INTG database table.
 * 
 */
@Entity
@Table(name = "ICS_ORDEN_TRABAJOS_INTG")
@NamedQuery(name = "IcsOrdenTrabajosIntg.findAll", query = "SELECT i FROM IcsOrdenTrabajosIntg i")
public class IcsOrdenTrabajosIntg implements Serializable {
	private static final long serialVersionUID = 3010397657527506828L;


	@Id
	@Column(name = "ID_TRABAJO")
	private Long idTrabajo;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = true)                                                        
	@JoinColumn(name = "ID_ATRIBUTO_INTEGRACION", nullable = false, insertable = false, updatable = false) 
	private IcsAtributosIntg icsAtributoIntegracion;

	@Column(name = "TIPO_TRABAJO")
	private String tipoTrabajo;

	@Column(name = "SECUENCIA")
	private Long secuencia;

	public IcsOrdenTrabajosIntg() {
		super();
	}
	
	public IcsAtributosIntg getIcsAtributoIntegracion() {
		return icsAtributoIntegracion;
	}

	public void setIcsAtributoIntegracion(IcsAtributosIntg icsAtributoIntegracion) {
		this.icsAtributoIntegracion = icsAtributoIntegracion;
	}

	public String getTipoTrabajo() {
		return tipoTrabajo;
	}

	public void setTipoTrabajo(String tipoTrabajo) {
		this.tipoTrabajo = tipoTrabajo;
	}

	public Long getIdTrabajo() {
		return idTrabajo;
	}

	public void setIdTrabajo(Long idTrabajo) {
		this.idTrabajo = idTrabajo;
	}

	public Long getSecuencia() {
		return secuencia;
	}

	public void setSecuencia(Long secuencia) {
		this.secuencia = secuencia;
	}
}
