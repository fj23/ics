package cl.cardif.ics.domain.entity.common;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "ICS_CALLXEVENTOXSOCIO")
@NamedQuery(name = "IcsCallXEventoXSocio.findAll", query = "SELECT i FROM IcsCallXEventoXSocio i")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class IcsCallXEventoXSocio implements Serializable {
	private static final long serialVersionUID = -7524275309974907512L;

	@Id
	@Column(name = "ID_CXEXS")
	private long idCallXEventoXSocio;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "ID_SOCIO")
	private IcsSocio icsSocio;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "ID_EVENTO")
	private IcsEvento icsEvento;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "ID_CALL_CENTER")
	private IcsCallCenter icsCallCenter;

	@Column(name = "FLG_ACTIVO", columnDefinition = "CHAR")
	private String flgActivo;

	public long getIdCallXEventoXSocio() {
		return idCallXEventoXSocio;
	}

	public void setIdCallXEventoXSocio(long idCallXEventoXSocio) {
		this.idCallXEventoXSocio = idCallXEventoXSocio;
	}

	public IcsSocio getIcsSocio() {
		return icsSocio;
	}

	public void setIcsSocio(IcsSocio icsSocio) {
		this.icsSocio = icsSocio;
	}

	public IcsEvento getIcsEvento() {
		return icsEvento;
	}

	public void setIcsEvento(IcsEvento icsEvento) {
		this.icsEvento = icsEvento;
	}

	public IcsCallCenter getIcsCallCenter() {
		return icsCallCenter;
	}

	public void setIdCallCenter(IcsCallCenter icsCallCenter) {
		this.icsCallCenter = icsCallCenter;
	}

	public String getFlgActivo() {
		return flgActivo;
	}

	public void setFlgActivo(String flgActivo) {
		this.flgActivo = flgActivo;
	}
}
