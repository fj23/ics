package cl.cardif.ics.domain.entity.estructuras;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the ICS_ESTRUCTURA database table.
 * 
 */
@Entity
@Table(name = "ICS_ESTRUCTURA")
@NamedQuery(name = "IcsEstructura.findAll", query = "SELECT i FROM IcsEstructura i")
public class IcsEstructura implements Serializable {
	private static final long serialVersionUID = 6766000545222017788L;

	@Id
	@SequenceGenerator(name = "DB_SEQ_ESTRUCTURA", sequenceName = "SEQ_ICS_ESTRUCTURA", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DB_SEQ_ESTRUCTURA")
	@Column(name = "ID_ESTRUCTURA")
	private long idEstructura;

	@Column(name = "DESCRIPCION_ESTRUCTURA")
	private String descripcionEstructura;

	@Column(name = "CARDINALIDAD_MAXIMA_ESTRUCTURA")
	private BigDecimal cardinalidadMaximaEstructura;

	@Column(name = "CARDINALIDAD_MINIMA_ESTRUCTURA")
	private BigDecimal cardinalidadMinimaEstructura;

	@Column(name = "CODIGO_ESTRUCTURA")
	private String codigoEstructura;

	@Column(name = "FLG_SATELITE", columnDefinition = "CHAR")
	private String flgSatelite;

	@Column(name = "VIGENCIA", columnDefinition = "CHAR")
	private String vigenciaEstructura;

	@OneToMany(mappedBy = "icsEstructura")
	private List<IcsDetalleEstructura> icsDetalleEstructuras;

	public IcsEstructura() {
		super();
	}

	public long getIdEstructura() {
		return idEstructura;
	}

	public void setIdEstructura(long idEstructura) {
		this.idEstructura = idEstructura;
	}

	public String getDescripcionEstructura() {
		return descripcionEstructura;
	}

	public void setDescripcionEstructura(String descripcionEstructura) {
		this.descripcionEstructura = descripcionEstructura;
	}

	public BigDecimal getCardinalidadMaximaEstructura() {
		return cardinalidadMaximaEstructura;
	}

	public void setCardinalidadMaximaEstructura(BigDecimal cardinalidadMaximaEstructura) {
		this.cardinalidadMaximaEstructura = cardinalidadMaximaEstructura;
	}

	public BigDecimal getCardinalidadMinimaEstructura() {
		return cardinalidadMinimaEstructura;
	}

	public void setCardinalidadMinimaEstructura(BigDecimal cardinalidadMinimaEstructura) {
		this.cardinalidadMinimaEstructura = cardinalidadMinimaEstructura;
	}

	public String getCodigoEstructura() {
		return codigoEstructura;
	}

	public void setCodigoEstructura(String codigoEstructura) {
		this.codigoEstructura = codigoEstructura;
	}

	public String getFlgSatelite() {
		return flgSatelite;
	}

	public void setFlgSatelite(String flgSatelite) {
		this.flgSatelite = flgSatelite;
	}

	public String getVigenciaEstructura() {
		return vigenciaEstructura;
	}

	public void setVigenciaEstructura(String vigenciaEstructura) {
		this.vigenciaEstructura = vigenciaEstructura;
	}

	public List<IcsDetalleEstructura> getIcsDetalleEstructuras() {
		if (icsDetalleEstructuras != null) {
			return new ArrayList<>(icsDetalleEstructuras);
		} else {
			return new ArrayList<>();
		}
	}

	public void setIcsDetalleEstructuras(List<IcsDetalleEstructura> icsDetalleEstructuras) {
		if (icsDetalleEstructuras != null) {
			this.icsDetalleEstructuras = new ArrayList<>(icsDetalleEstructuras);
		}
	}
}
