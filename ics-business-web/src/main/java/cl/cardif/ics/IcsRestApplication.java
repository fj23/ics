package cl.cardif.ics;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;

import cl.cardif.ics.icsgeneratefile.config.IcsProperties;

@SpringBootApplication
@ComponentScan(basePackages = { "cl.cardif.ics.icsrest", "cl.cardif.ics.icsgeneratefile" })
@EntityScan(basePackages = { "cl.cardif.ics.domain.entity" })
@PropertySource(value = { "classpath:application.properties", IcsProperties.PROPERTIES_QUERY })
public class IcsRestApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		ConfigurableApplicationContext context = null;
		try {
			context = SpringApplication.run(IcsRestApplication.class, args);
		} finally {
			if (context != null) {
				context.close();
			}
		}
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(IcsRestApplication.class);
	}

}
