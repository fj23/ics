package cl.cardif.ics.icsgeneratefile.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import cl.cardif.ics.icsgeneratefile.config.IcsProperties;
import cl.cardif.ics.icsgeneratefile.dao.DeleteTempInfoDao;

@Service
public class DeleteTempInfoDaoImpl implements DeleteTempInfoDao {
	private static final Logger LOG = Logger.getLogger(DeleteTempInfoDaoImpl.class);

	@Value("${" + IcsProperties.QUERY_DELETE_TEMPORARY_INFORMATION + "}")
	private String QUERY_DELETE_TEMPORARY_INFORMATION;

	private JdbcTemplate jdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	private int executeDelete(Long idTransaction, Long tipoArchivo, Connection con) {
		int countDeleted = 0;
		String sql = this.QUERY_DELETE_TEMPORARY_INFORMATION;
		
		try (PreparedStatement preparedStatement = con.prepareStatement(sql)) {
			preparedStatement.setInt(1, idTransaction.intValue());
			if (tipoArchivo != null) {
				preparedStatement.setInt(2, tipoArchivo.intValue());
			}
			countDeleted = preparedStatement.executeUpdate();
			LOG.debug("deleteInformation - " + countDeleted + " registros eliminados");
			con.commit();
		} catch (SQLException e) {
			LOG.error("deleteInformation - Error al limpiar la informacion: ", e);
		}
		return countDeleted;
	}

	@Override
	public int deleteInformation(Long idTransaction, Long tipoArchivo) {
		int countDeleted = 0;
		try (Connection con = this.jdbcTemplate.getDataSource().getConnection()) {
			con.setAutoCommit(false);

			countDeleted = this.executeDelete(idTransaction, tipoArchivo, con);
		} catch (SQLException e) {
			LOG.error("deleteInformation - Error al obtener la conexion: ", e);
		}
		return countDeleted;
	}
}
