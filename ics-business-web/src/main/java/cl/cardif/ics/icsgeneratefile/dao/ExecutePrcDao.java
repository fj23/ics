package cl.cardif.ics.icsgeneratefile.dao;

import cl.cardif.ics.icsgeneratefile.entity.PrcInformation;

@FunctionalInterface
public interface ExecutePrcDao {

	PrcInformation executePrc(String idTransaction, String idFileType);
}
