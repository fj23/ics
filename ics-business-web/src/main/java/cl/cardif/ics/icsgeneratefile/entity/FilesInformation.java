package cl.cardif.ics.icsgeneratefile.entity;

public class FilesInformation {

	private String nameFile;
	private String extensionFile;

	public FilesInformation(String nameFile, String extensionFile) {
		this.nameFile = nameFile;
		this.extensionFile = extensionFile;
	}

	public String getNameFile() {
		return nameFile;
	}

	public String getExtensionFile() {
		return extensionFile;
	}

}
