package cl.cardif.ics.icsgeneratefile.dao;

@FunctionalInterface
public interface DeleteTempInfoDao {

public int deleteInformation(Long idTransaction, Long tipoArchivo);
}
