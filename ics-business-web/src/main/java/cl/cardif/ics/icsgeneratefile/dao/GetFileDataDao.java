package cl.cardif.ics.icsgeneratefile.dao;

import java.util.List;

import cl.cardif.ics.icsgeneratefile.entity.FilesDatas;

@FunctionalInterface
public interface GetFileDataDao {

	List<FilesDatas> getData(String idTransaction, String nameFile, String extensionFile);
}
