package cl.cardif.ics.icsgeneratefile.entity;

public class FilesDatas {

    private String data;
    private String sheet;
    private String dataType;

    public FilesDatas(String data, String sheet, String dataType) {
        this.data = data;
        this.sheet = sheet;
        this.dataType = dataType;
    }

    public String getData() {
        return data;
    }

    public String getSheet() {
        return sheet;
    }

    public String getDataType() {
        return dataType;
    }

    @Override
    public String toString() {
        return "FilesDatas{" +
                "data='" + data + '\'' +
                ", sheet='" + sheet + '\'' +
                ", dataType='" + dataType + '\'' +
                '}';
    }
}
