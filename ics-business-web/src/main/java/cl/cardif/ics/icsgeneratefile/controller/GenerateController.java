package cl.cardif.ics.icsgeneratefile.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import cl.cardif.ics.icsgeneratefile.config.IcsProperties;
import cl.cardif.ics.icsgeneratefile.dao.DeleteTempInfoDao;
import cl.cardif.ics.icsgeneratefile.dao.ExecutePrcDao;
import cl.cardif.ics.icsgeneratefile.dao.GetFileDataDao;
import cl.cardif.ics.icsgeneratefile.dao.GetFilesDao;
import cl.cardif.ics.icsgeneratefile.entity.FilesDatas;
import cl.cardif.ics.icsgeneratefile.entity.FilesInformation;
import cl.cardif.ics.icsgeneratefile.entity.PrcInformation;
import cl.cardif.ics.icsrest.exceptions.icsimportaarchivo.IcsImportaArchivoServiceException;
import cl.cardif.ics.icsrest.exceptions.icstrazabilidad.IcsTrazabilidadServiceException;

@Component
public class GenerateController {
	private static final Logger LOG = Logger.getLogger(GenerateController.class);

	@Autowired
	private DeleteTempInfoDao deleteTempInfoDao;
	@Autowired
	private ExecutePrcDao executePrcDao;
	@Autowired
	private GetFileDataDao getFileDataDao;
	@Autowired
	private GetFilesDao getFilesDao;

	private static final String IN_TIPO_ARCHIVO = "2";

	private List<List<String>> ordenarDatosEnListaDeListasString(List<FilesDatas> listData, List<String> listSheet,
			List<String> listDataType) {
		LOG.info("ordenarDatosEnListaDeListasString");

		List<List<String>> data = new ArrayList<>();

		List<FilesDatas> dataForSheet;
		List<String> dateForSheetSort;
		for (String value : listSheet) {
			dataForSheet = new ArrayList<>();
			for (FilesDatas listDatum : listData) {
				if (value.equalsIgnoreCase(listDatum.getSheet())) {
					dataForSheet.add(listDatum);
				}
			}
			dateForSheetSort = new ArrayList<>();
			for (String s : listDataType) {
				for (FilesDatas filesDatas : dataForSheet) {
					if (s.equalsIgnoreCase(filesDatas.getDataType())) {
						dateForSheetSort.add(filesDatas.getData());
					}
				}
			}
			data.add(dateForSheetSort);
		}

		return data;
	}

	private List<String> hacerListaTiposDato(List<FilesDatas> listData) {
		LOG.info("hacerListaTiposDato");

		// set de string unicos
		Set<String> tiposDatoSet = new HashSet<>();
		for (FilesDatas filesDatas : listData) {
			String tipoDato = filesDatas.getDataType();
			tiposDatoSet.add(tipoDato);
		}

		List<String> tiposDato = Arrays.asList((String[]) tiposDatoSet.toArray(new String[0]));
		Collections.sort(tiposDato);

		return tiposDato;
	}

	private List<String> hacerListaHojas(List<FilesDatas> listData) {
		LOG.info("hacerListaHojas");

		// set de string unicos
		Set<String> listaHojasSet = new HashSet<>();
		for (FilesDatas filesDatas : listData) {
			String tipoDato = filesDatas.getSheet();
			listaHojasSet.add(tipoDato);
		}

		List<String> listaHojas = Arrays.asList((String[]) listaHojasSet.toArray(new String[0]));
		Collections.sort(listaHojas);

		return listaHojas;
	}

	private byte[] generateExcel(FilesInformation file, List<FilesDatas> listData, Workbook workbook) {
		LOG.info("generateExcel");

		List<String> hojasExcel = this.hacerListaHojas(listData);
		List<String> tiposDato = this.hacerListaTiposDato(listData);

		List<List<String>> dataAllSheet = this.ordenarDatosEnListaDeListasString(listData, hojasExcel, tiposDato);

		if (hojasExcel.size() == 0) {
			workbook.createSheet("Sin Informacion");
		} else {
			for (int x = 0; x < hojasExcel.size(); x++) {
				Sheet sheet = workbook.createSheet(hojasExcel.get(x));
				for (int i = 0; i < dataAllSheet.get(x).size(); i++) {
					Row row = sheet.createRow(i);
					if (dataAllSheet.get(x).get(i) != null) {
						String rowData = dataAllSheet.get(x).get(i);
						String[] cellData = rowData.split(IcsProperties.DELIMITER_COMMAND);
						for (int j = 0; j < cellData.length; j++) {
							if (cellData[j] != null) {
								row.createCell(j).setCellValue(cellData[j]);
							}
						}
					} else {
						row.createCell(0);
					}
				}
			}
		}

		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		try {
			workbook.write(stream);
		} catch (IOException e) {
			throw new IcsImportaArchivoServiceException("Error al escribir en el stream de salida", e);
		}

		try {
			stream.close();
		} catch (IOException e) {
			throw new IcsImportaArchivoServiceException("Error al cerrar el stream de salida", e);
		}

		LOG.debug("El archivo Excel con nombre '" + file.getNameFile() + "' fue generado correctamente");
		return stream.toByteArray();
	}

	private byte[] generateXlsx(FilesInformation file, List<FilesDatas> listData) {
		LOG.info("generateXlsx");

		try (Workbook workbook = new XSSFWorkbook()) {
			return this.generateExcel(file, listData, workbook);
		} catch (Exception e) {
			LOG.error("ERROR AL CREAR EL ARCHIVO EXCEL XLSX", e);
			return new byte[] {};
		}
	}

	private byte[] generateXls(FilesInformation file, List<FilesDatas> listData) {
		LOG.info("generateXls");

		try (Workbook workbook = new HSSFWorkbook()) {
			return this.generateExcel(file, listData, workbook);
		} catch (Exception e) {
			LOG.error("ERROR AL CREAR EL ARCHIVO EXCEL XLS", e);
			return new byte[] {};
		}
	}

	private byte[] generateCsv(FilesInformation file, List<FilesDatas> listData) {
		LOG.info("generateCsv");

		PrintWriter writer;
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		writer = new PrintWriter(stream);

		FilesDatas head = null;
		for (FilesDatas filesDatas : listData) {
			if (Integer.valueOf(filesDatas.getDataType()) == 0) {
				head = filesDatas;
				break;
			}
		}

		if (head != null) {
			writer.println(head.getData() + IcsProperties.DELIMITER_COMMAND);
		}
		for (FilesDatas body : listData) {
			if (Integer.parseInt(body.getDataType()) != 0) {
				writer.println(body.getData() + IcsProperties.DELIMITER_COMMAND);
			}
		}
		writer.close();
		LOG.debug("GENERADO CORRECTAMENTE EL ARCHIVO CSV : " + file.getNameFile());
		return stream.toByteArray();
	}

	private byte[] generateTxt(FilesInformation file, List<FilesDatas> listData) {
		LOG.info("generateTxt");

		PrintWriter writer;
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		writer = new PrintWriter(stream);

		FilesDatas head = null;
		for (FilesDatas filesDatas : listData) {
			if (Integer.valueOf(filesDatas.getDataType()) == 0) {
				head = filesDatas;
				break;
			}
		}

		if (head != null) {
			writer.println(head.getData());
		}
		for (FilesDatas body : listData) {
			if (Integer.parseInt(body.getDataType()) != 0) {
				writer.println(body.getData());
			}
		}
		writer.close();
		LOG.debug("GENERADO CORRECTAMENTE EL ARCHIVO TXT : " + file.getNameFile());
		return stream.toByteArray();
	}

	public FilesInformation generateFiles(String idTransaction) {
		PrcInformation resultadoGeneracionArchivo;
		try {
			resultadoGeneracionArchivo = executePrcDao.executePrc(idTransaction, IN_TIPO_ARCHIVO);
		} catch (Exception e) {
			throw new IcsTrazabilidadServiceException(e.getMessage(), e);
		}

		// la 'ejecucion' del proceso puede ser exitosa,
		// pero el proceso en si puede tener errores internos
		if (resultadoGeneracionArchivo.getErrorType() != 0) {
			throw new IcsTrazabilidadServiceException(resultadoGeneracionArchivo.getErrorDesc());
		}

		// consulta nombres de archivo
		List<FilesInformation> listFile = getFilesDao.getFiles(idTransaction);
		if (listFile.isEmpty()) {
			throw new IcsTrazabilidadServiceException("El archivo no existe o no posee datos.");
		} else {
			return listFile.get(0);
		}
	}

	public byte[] generateFileFromInfoData(FilesInformation file, List<FilesDatas> listData) {
		switch (file.getExtensionFile()) {
		case IcsProperties.EXCEL_XLSX:
			return generateXlsx(file, listData);
		case IcsProperties.EXCEL_XLS:
			return generateXls(file, listData);
		case IcsProperties.CSV:
			return generateCsv(file, listData);
		case IcsProperties.TXT:
			return generateTxt(file, listData);
		default:
			LOG.debug("ARCHIVO CON EXTENSION INCORRECTA");
			return new byte[] {};
		}
	}

	public byte[] getFile(String idTransaction, FilesInformation file) {
		List<FilesDatas> listData = this.getFileDataDao.getData(idTransaction, file.getNameFile(),
				file.getExtensionFile());
		return generateFileFromInfoData(file, listData);
	}

	public int deleteInformation(Long idTransaction, Long tipoArchivo) {
		return this.deleteTempInfoDao.deleteInformation(idTransaction, tipoArchivo);
	}
}
