package cl.cardif.ics.icsgeneratefile.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import cl.cardif.ics.icsgeneratefile.config.IcsProperties;
import cl.cardif.ics.icsgeneratefile.dao.GetFileDataDao;
import cl.cardif.ics.icsgeneratefile.entity.FilesDatas;

@Service
public class GetFileDataDaoImpl implements GetFileDataDao {

	@Value("${" + IcsProperties.QUERY_FILE_DATA + "}")
	private String QUERY_FILE_DATA;

	private static final Logger logger = Logger.getLogger(DeleteTempInfoDaoImpl.class);

	private JdbcTemplate jdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public List<FilesDatas> getData(String idTransaction, String nameFile, String extensionFile) {
		List<FilesDatas> listData = new ArrayList<>();
		FilesDatas data;
		String sql = this.QUERY_FILE_DATA;
		
		try (
			Connection connection = this.jdbcTemplate.getDataSource().getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(sql)
		) {
			connection.setAutoCommit(false);
			preparedStatement.setInt(1, Integer.parseInt(idTransaction));
			preparedStatement.setString(2, nameFile);
			preparedStatement.setString(3, extensionFile);
			try (ResultSet resultSet = preparedStatement.executeQuery();) {
				while (resultSet.next()) {
					data = new FilesDatas(resultSet.getString("DATOS"), resultSet.getString("HOJA_DESTINO"),
							resultSet.getString("TIPO_DATO"));
					listData.add(data);
				}
			}
			connection.commit();
			connection.setAutoCommit(true);
		} catch (SQLException e) {
			logger.error("BASE DE DATOS CONEXIÓN FALLIDA - GLOSA {" + e + "}");
		}
		return listData;
	}
}
