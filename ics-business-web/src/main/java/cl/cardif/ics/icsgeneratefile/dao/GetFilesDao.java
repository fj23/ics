package cl.cardif.ics.icsgeneratefile.dao;

import java.util.List;

import cl.cardif.ics.icsgeneratefile.entity.FilesInformation;

@FunctionalInterface
public interface GetFilesDao {

	List<FilesInformation> getFiles(String idTransaction);

}
