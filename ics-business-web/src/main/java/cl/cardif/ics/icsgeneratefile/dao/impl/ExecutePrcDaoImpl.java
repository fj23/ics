package cl.cardif.ics.icsgeneratefile.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import cl.cardif.ics.icsgeneratefile.config.IcsProperties;
import cl.cardif.ics.icsgeneratefile.dao.ExecutePrcDao;
import cl.cardif.ics.icsgeneratefile.entity.PrcInformation;
import oracle.jdbc.OracleTypes;

@Service
public class ExecutePrcDaoImpl implements ExecutePrcDao {

	@Value("${" + IcsProperties.PRC_FILE_INFORMATION + "}")
	private String PRC_FILE_INFORMATION;

	private static final Logger logger = Logger.getLogger(ExecutePrcDaoImpl.class);

	private JdbcTemplate jdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public PrcInformation executePrc(String idTransaction, String idFileType) {
		PrcInformation prcInformation = null;
		logger.debug("prc_ics_archivos_salida EJECUTANDO");
		String preparedQuery = this.PRC_FILE_INFORMATION;
		try (
			Connection connection = this.jdbcTemplate.getDataSource().getConnection();
			CallableStatement callableStatement = connection.prepareCall(preparedQuery)
		) {
			connection.setAutoCommit(false);
			callableStatement.setInt(1, Integer.parseInt(idTransaction));
			callableStatement.setInt(2, Integer.parseInt(idFileType));
			callableStatement.registerOutParameter(3, OracleTypes.CURSOR);
			callableStatement.registerOutParameter(4, OracleTypes.CURSOR);
			callableStatement.registerOutParameter(5, OracleTypes.NUMERIC);
			callableStatement.registerOutParameter(6, OracleTypes.VARCHAR);
			callableStatement.registerOutParameter(7, OracleTypes.VARCHAR);
			callableStatement.execute();
			prcInformation = new PrcInformation(
					callableStatement.getInt(5), 
					callableStatement.getString(6),
					callableStatement.getInt(7)
			);
			
			if (prcInformation.getErrorType() == 0) {
				logger.debug("prc_ics_archivos_salida EJECUCIÓN EXITOSA");
			} else {
				logger.error("prc_ics_archivos_salida EJECUCIÓN FALLIDA GLOSA {" + prcInformation.getErrorDesc() + "}");
			}
			
			connection.commit();
			connection.setAutoCommit(true);
		} catch (SQLException e) {
			logger.error("BASE DE DATOS CONEXIÓN FALLIDA - " + e);
		}
		return prcInformation;
	}
}
