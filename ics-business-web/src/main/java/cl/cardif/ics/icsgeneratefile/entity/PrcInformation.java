package cl.cardif.ics.icsgeneratefile.entity;

import org.springframework.util.StringUtils;

public class PrcInformation {

	private int errorType;
	private String errorDesc;
	private int outType;

	public PrcInformation(int errorType, String errorDesc, int outType) {
		this.errorType = errorType;
		this.errorDesc = StringUtils.deleteAny(errorDesc, "\n\"");
		this.outType = outType;
	}

	public int getOutType() {
		return outType;
	}

	public int getErrorType() {
		return errorType;
	}

	public String getErrorDesc() {
		return errorDesc;
	}
}
