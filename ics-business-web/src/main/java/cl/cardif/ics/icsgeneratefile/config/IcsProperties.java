package cl.cardif.ics.icsgeneratefile.config;

public class IcsProperties {

	// QUERYS
	public static final String PRC_FILE_INFORMATION = "prc.file.information";
	public static final String QUERY_FILE_INFORMATION = "query.file.information";
	public static final String QUERY_FILE_DATA = "query.file.data";
	public static final String QUERY_DELETE_TEMPORARY_INFORMATION = "query.delete.temporary.information";

	// QUERYS
	public static final String PROPERTIES_QUERY = "classpath:query.properties";

	public static final String DELIMITER_COMMAND = ";";
	public static final String SLASH = "/";
	public static final String DOT = ".";
	public static final String UTF8 = "UTF-8";

	// Extension
	public static final String EXCEL_XLS = ".xls";
	public static final String EXCEL_XLSX = ".xlsx";
	public static final String CSV = ".csv";
	public static final String TXT = ".txt";

}
