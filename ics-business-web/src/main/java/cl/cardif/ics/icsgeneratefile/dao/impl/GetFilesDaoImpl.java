package cl.cardif.ics.icsgeneratefile.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import cl.cardif.ics.icsgeneratefile.config.IcsProperties;
import cl.cardif.ics.icsgeneratefile.dao.GetFilesDao;
import cl.cardif.ics.icsgeneratefile.entity.FilesInformation;

@Service
public class GetFilesDaoImpl implements GetFilesDao {

	@Value("${" + IcsProperties.QUERY_FILE_INFORMATION + "}")
	private String QUERY_FILE_INFORMATION;

	private static final Logger logger = Logger.getLogger(DeleteTempInfoDaoImpl.class);

	private JdbcTemplate jdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public List<FilesInformation> getFiles(String idTransaction) {
		List<FilesInformation> listFiles = new ArrayList<>();
		FilesInformation files;
		
		String sql  = this.QUERY_FILE_INFORMATION;
		try (
			Connection connection = this.jdbcTemplate.getDataSource().getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(sql)
		) {
			connection.setAutoCommit(false);
			preparedStatement.setInt(1, Integer.parseInt(idTransaction));
			try (ResultSet resultSet = preparedStatement.executeQuery();) {
				while (resultSet.next()) {
					files = new FilesInformation(resultSet.getString("NOMBRE_ARCHIVO"),
							resultSet.getString("EXTENSION_ARCHIVO"));
					listFiles.add(files);
				}
			}
			connection.commit();
			connection.setAutoCommit(true);
		} catch (SQLException e) {
			logger.error("BASE DE DATOS CONEXIÓN FALLIDA - GLOSA {" + e + "}");
		}
		return listFiles;
	}
}
