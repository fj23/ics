package cl.cardif.ics.icsrest.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.xml.ws.soap.SOAPFaultException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ws.soap.SoapFaultException;

import com.querydsl.core.BooleanBuilder;

import cl.cardif.ics.domain.entity.common.IcsLogin;
import cl.cardif.ics.domain.entity.common.IcsParametro;
import cl.cardif.ics.domain.entity.common.QIcsLogin;
import cl.cardif.ics.icsrest.config.ConfigProperties;
import cl.cardif.ics.icsrest.domain.pojo.Login;
import cl.cardif.ics.icsrest.exceptions.login.IcsLoginException;
import cl.cardif.ics.icsrest.message.response.AuthLoginResponse;
import cl.cardif.ics.icsrest.repository.LoginRepository;
import cl.cardif.ics.icsrest.repository.ParametrosRepository;
import cl.cardif.ics.icsrest.security.jwt.JwtTokenProvider;
import cl.cardif.ics.icsrest.service.LoginService;
import cl.cardif.schema.ebm.authentication.authenticate.v1.AuthenticateAuthenticationREQType;
import cl.cardif.schema.ebm.authentication.authenticate.v1.AuthenticateAuthenticationRSPType;
import cl.cardif.schema.ebm.authentication.authenticate.v1.AuthenticateAuthenticationRequestType;
import cl.cardif.schema.ebm.authentication.authenticate.v1.AuthenticationInformationType;
import cl.cardif.schema.ebm.authentication.authenticate.v1.ObjectFactory;
import cl.cardif.schema.ebm.authentication.logout.v1.LogoutAuthenticationREQType;
import cl.cardif.schema.ebm.authentication.logout.v1.LogoutAuthenticationRSPType;
import cl.cardif.schema.ebm.authentication.logout.v1.LogoutAuthenticationRequestType;
import cl.cardif.schema.ebm.authentication.validatetoken.v1.ValidateTokenAuthenticationREQType;
import cl.cardif.schema.ebm.authentication.validatetoken.v1.ValidateTokenAuthenticationRSPType;
import cl.cardif.schema.ebm.authentication.validatetoken.v1.ValidateTokenAuthenticationRequestType;
import cl.cardif.schema.ebm.authorization.getroles.v1.GetRolesAuthorizationREQType;
import cl.cardif.schema.ebm.authorization.getroles.v1.GetRolesAuthorizationRSPType;
import cl.cardif.schema.ebm.authorization.getroles.v1.GetRolesAuthorizationRequestType;
import cl.cardif.schema.eso.messageheader.v1.ChannelType;
import cl.cardif.schema.eso.messageheader.v1.ConsumerType;
import cl.cardif.schema.eso.messageheader.v1.CountryType;
import cl.cardif.schema.eso.messageheader.v1.QoSType;
import cl.cardif.schema.eso.messageheader.v1.RequestHeaderType;
import cl.cardif.schema.eso.messageheader.v1.TraceType;
import cl.cardif.service.ebsc.authentication.v1.AuthenticateFaultMessage;
import cl.cardif.service.ebsc.authentication.v1.AuthenticationPortType;
import cl.cardif.service.ebsc.authentication.v1.LogoutFaultMessage;
import cl.cardif.service.ebsc.authentication.v1.ValidateTokenFaultMessage;
import cl.cardif.service.ebsc.authorization.v1.AuthorizationPortType;
import cl.cardif.service.ebsc.authorization.v1.GetRolesFaultMessage;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtParser;

@Service
public class LoginServiceImpl implements LoginService {

	static final Logger LOG = LoggerFactory.getLogger(LoginServiceImpl.class);

	// private static final String FACTORY_CREADO = "Factory Creado";

	// private static final String HEADER_AGREGADO = "Header agregado al request";
	// private static final String BODY_AGREGADO = "Body agregado al request";
	private static final String RESPONSE_OBTENIDO = "Response Obtenido";
	private static final String RESPUESTA_DE_SERVICIO = "Respuesta de servicio : ";
	private static final String MENSAJE_ERROR_CLASE = "Mensaje de error de clase: ";
	private static final String MENSAJE_ERROR_SOAP = "Mensaje de error soap : ";
	private static final String MENSAJE_ERROR_GENERICO = "Mensaje de error generico : ";
	private static final String MENSAJE_RESPUESTA_ERROR = "Error al procesar mensaje de respuesta";

	private static final String APP_DOMAIN = "INTEGRA_SOCIOS";
	private static final String AUTH_SOURCE_TYPE = "1";

	@Autowired
	private ParametrosRepository parametrosRepository;
	@Autowired
	private LoginRepository loginRepository;

	@Autowired
	private AuthenticationPortType authenticationPortType;
	@Autowired
	private AuthorizationPortType authorizationPortType;
	@Autowired
	private JwtParser jwtParser;
	@Autowired
	private JwtTokenProvider jwtTokenProvider;

	@Autowired
	private ConfigProperties configProperties;

	@Override
	public AuthLoginResponse autenticar(Login loginInput) throws IcsLoginException {
		LOG.info("autenticar");

		AuthenticateAuthenticationREQType request = new ObjectFactory().createAuthenticateAuthenticationREQType();
		request.setRequestHeader(this.buildAuthenticationRequestHeader());
		request.setBody(this.buildAuthenticationRequestBody(loginInput));

		// Autenticar
		AuthenticateAuthenticationRSPType response = null;
		try {
			response = authenticationPortType.authenticate(request);
		} catch (AuthenticateFaultMessage e) {
			final String errorDesc = e.getFaultInfo().getBody().getError().getErrorDescription();
			LOG.error(errorDesc, e);
			throw new IcsLoginException(errorDesc);
		} catch (SOAPFaultException e) {
			LOG.error(MENSAJE_RESPUESTA_ERROR, e);
			throw new IcsLoginException(MENSAJE_RESPUESTA_ERROR);
		}

		if (response == null) {
			return null;
		} else {
			LOG.debug(RESPONSE_OBTENIDO);

			// Obtener boolean result
			AuthenticationInformationType authInfo = response.getBody().getAuthenticateResponse()
					.getAuthenticationInformation();
			AuthLoginResponse authResponse = new AuthLoginResponse();
			authResponse.setResult(authInfo.isAuthenticateResult());
			authResponse.setToken(authInfo.getToken());
			authResponse.setUsername(loginInput.getUsername());

			final String respLog = RESPUESTA_DE_SERVICIO + authResponse.getResult();
			LOG.debug(respLog);

			return authResponse;
		}
	}

	private AuthenticateAuthenticationREQType.Body buildAuthenticationRequestBody(Login loginInput) {
		LOG.info("getAuthenticationBody");
		AuthenticateAuthenticationREQType.Body body = new AuthenticateAuthenticationREQType.Body();

		AuthenticateAuthenticationRequestType requestType = new AuthenticateAuthenticationRequestType();
		requestType.setAuthenticationSourceType(AUTH_SOURCE_TYPE);
		requestType.setUsername(loginInput.getUsername());
		requestType.setPassword(loginInput.getPassword());

		body.setAuthenticateAuthenticationRequest(requestType);

		return body;
	}

	private RequestHeaderType buildAuthenticationRequestHeader() {
		LOG.info("getAuthenticationHeader");

		RequestHeaderType requestHeaderType = new RequestHeaderType();

		ConsumerType consumerType = new ConsumerType();
		consumerType.setCode(1);
		consumerType.setName("Cardif");
		requestHeaderType.setConsumer(consumerType);

		requestHeaderType.setTrace(new TraceType());

		CountryType countryType = new CountryType();
		countryType.setCode(1);
		countryType.setName("LAM");
		requestHeaderType.setCountry(countryType);

		ChannelType channelType = new ChannelType();
		channelType.setCode("NI");
		channelType.setMode("NI");
		requestHeaderType.setChannel(channelType);

		requestHeaderType.setQoS(new QoSType());

		return requestHeaderType;
	}

	private GetRolesAuthorizationREQType.Body buildAuthorizationRolesRequestBody(String token) {
		LOG.info("getGetRolesBody");
		GetRolesAuthorizationREQType.Body body = new GetRolesAuthorizationREQType.Body();

		GetRolesAuthorizationRequestType requestType = new GetRolesAuthorizationRequestType();
		requestType.setToken(token);
		requestType.setAppDomain(APP_DOMAIN);
		requestType.setAuthorizationSourceType(AUTH_SOURCE_TYPE);

		body.setGetRolesAuthorizationRequest(requestType);

		return body;
	}

	@Override
	public List<String> getRoles(String token) {
		final String nombreMetodo = "getRoles";
		LOG.info(nombreMetodo);

		List<String> roles = new ArrayList<>();
		try {

			// Object Factory de Autenticacion
			cl.cardif.schema.ebm.authorization.getroles.v1.ObjectFactory factory = new cl.cardif.schema.ebm.authorization.getroles.v1.ObjectFactory();

			// Request de Autenticacion
			GetRolesAuthorizationREQType request = factory.createGetRolesAuthorizationREQType();
			request.setRequestHeader(buildAuthenticationRequestHeader());
			request.setBody(buildAuthorizationRolesRequestBody(token));

			// Autenticar
			GetRolesAuthorizationRSPType response = authorizationPortType.getRoles(request);

			// Obtener boolean result
			roles = response.getBody().getListOfRoles().getRole();

			final String respLog = nombreMetodo + " - " + RESPUESTA_DE_SERVICIO + roles.toString();
			LOG.info(respLog);

		} catch (GetRolesFaultMessage e) {
			final String errorLog = nombreMetodo + " - " + MENSAJE_ERROR_CLASE
					+ e.getFaultInfo().getBody().getError().getErrorDescription();
			LOG.error(errorLog, e);
		} catch (SoapFaultException e) {
			final String errorLog = nombreMetodo + " - " + MENSAJE_ERROR_SOAP + e.getRootCause().getMessage();
			LOG.error(errorLog, e);
		} catch (NullPointerException e) {
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			final String errorLog = nombreMetodo + " - " + MENSAJE_ERROR_GENERICO + e.getMessage();
			LOG.error(errorLog, e);
		}

		return roles;
	}

	private ValidateTokenAuthenticationREQType.Body buildTokenValidationRequestBody(String token) {
		LOG.info("getValidateTokenBody");

		ValidateTokenAuthenticationRequestType requestType = new ValidateTokenAuthenticationRequestType();
		requestType.setAuthenticationSourceType(AUTH_SOURCE_TYPE);
		requestType.setToken(token);

		ValidateTokenAuthenticationREQType.Body body = new ValidateTokenAuthenticationREQType.Body();
		body.setValidateTokenAuthenticationRequest(requestType);

		return body;
	}

	private LogoutAuthenticationREQType.Body buildLogoutRequestBody(String token) {
		LOG.info("getLogoutBody");
		LogoutAuthenticationREQType.Body body = new LogoutAuthenticationREQType.Body();

		LogoutAuthenticationRequestType requestType = new LogoutAuthenticationRequestType();
		requestType.setAuthenticationSourceType(AUTH_SOURCE_TYPE);
		requestType.setToken(token);

		LOG.info("RequestType creado con token : " + requestType.getToken());

		body.setLogoutAuthenticationRequest(requestType);

		return body;
	}

	/**
	 * Llama a sesame para validar el token ingresado.
	 * @param sToken
	 * @return true/false según la validez del token, o null si se produce una excepción durante el intento
	 * @throws ValidateTokenFaultMessage 
	 */
	public Boolean validarTokenSesame(String sToken) throws ValidateTokenFaultMessage {	
			cl.cardif.schema.ebm.authentication.validatetoken.v1.ObjectFactory factory = new cl.cardif.schema.ebm.authentication.validatetoken.v1.ObjectFactory();

			ValidateTokenAuthenticationREQType request = factory.createValidateTokenAuthenticationREQType();
			request.setRequestHeader(this.buildAuthenticationRequestHeader());
			request.setBody(this.buildTokenValidationRequestBody(sToken));

			ValidateTokenAuthenticationRSPType response = this.authenticationPortType.validateToken(request);

			return response.getBody().isValidateTokenResult();
	}

	@Override
	public boolean validarToken(String token) {
		LOG.info("validarToken");

		if (token.startsWith("Bearer ")) {
			String jwtToken = token.substring(7);
			return jwtTokenProvider.validateJwtToken(jwtToken);
		}

		return false;
	}

	@Override
	public void logout(String token) {
		LOG.info("logout");

		try {
			cl.cardif.schema.ebm.authentication.logout.v1.ObjectFactory factory = new cl.cardif.schema.ebm.authentication.logout.v1.ObjectFactory();

			Jws<Claims> claims = this.jwtParser.parseClaimsJws(token);
			String sesameToken = (String) claims.getBody().get("sesame-token");

			LogoutAuthenticationREQType request = factory.createLogoutAuthenticationREQType();
			request.setRequestHeader(this.buildAuthenticationRequestHeader());
			request.setBody(this.buildLogoutRequestBody(sesameToken));

			LogoutAuthenticationRSPType response = this.authenticationPortType.logout(request);

			String result = response.getResponseHeader().getResult().getStatus();
			LOG.info("Logout de session token {} resultado {}", sesameToken, result);

		} catch (LogoutFaultMessage e) {
			final String errorLog = MENSAJE_ERROR_CLASE + e.getFaultInfo().getBody().getError().getErrorDescription();
			LOG.error(errorLog, e);
		} catch (SoapFaultException e) {
			final String errorLog = MENSAJE_ERROR_GENERICO + e.getMessage();
			LOG.error(errorLog, e);
		}

	}

	@Override
	public Integer getInactivityNoticeTime() {
		// 1000148 | WEB_SESION | Tiempo de Inactividad Permitida Minutos | 30 | Y
		IcsParametro parametro = this.parametrosRepository.findOne(1000148L);
		return parametro != null ? Integer.parseInt(parametro.getValorParametro()) : 30;
	}

	private Iterable<IcsLogin> obtenerSesionesConNombreUsuario(String userName) {
		QIcsLogin qIcsLogin = QIcsLogin.icsLogin;
		BooleanBuilder bb = new BooleanBuilder().and(qIcsLogin.userName.equalsIgnoreCase(userName));

		return loginRepository.findAll(bb);
	}

	@Override
	public void saveSession(AuthLoginResponse authLoginResponse) {

		String userName = authLoginResponse.getUsername();

		Iterable<IcsLogin> sesiones = this.obtenerSesionesConNombreUsuario(userName);
		loginRepository.deleteInBatch(sesiones);

		IcsLogin session = new IcsLogin();
		session.setUserName(userName);
		session.setToken(authLoginResponse.getToken());
		loginRepository.saveAndFlush(session);
	}

	@Override
	public List<Integer> getValidationConfig() {
		List<Integer> data = new ArrayList<>();
		data.add(configProperties.getValidationIntervalTime());
		data.add(configProperties.getValidationAttemptLimit());
		return data;
	}

}
