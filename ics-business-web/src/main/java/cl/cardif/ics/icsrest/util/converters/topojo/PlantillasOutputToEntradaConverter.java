package cl.cardif.ics.icsrest.util.converters.topojo;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import cl.cardif.ics.icsrest.domain.dto.output.PlantillaEntradaOutput;
import cl.cardif.ics.icsrest.domain.dto.output.PlantillaOutput;

@Component
public class PlantillasOutputToEntradaConverter implements Converter<PlantillaOutput, PlantillaEntradaOutput> {

	@Override
	public PlantillaEntradaOutput convert(PlantillaOutput source) {
		PlantillaEntradaOutput target = new PlantillaEntradaOutput();

		target.setId(source.getId());
		target.setNombre(source.getNombre());
		target.setObservaciones(source.getObservaciones());
		target.setPlantillaMultiarchivo(source.getPlantillaMultiarchivo());
		target.setVersion(source.getVersion());
		target.setVigencia(source.getVigencia());
		target.setFechaCreacion(source.getFechaCreacion());
		target.setTipo(source.getTipo());
		target.setTipoSalida(source.getTipoSalida());
		target.setCodUsuario(source.getCodUsuario());
		
		if (source.getCore() != null) {
			target.setCore(source.getCore());
		}
		
		if (source.getEvento() != null) {
			target.setEvento(source.getEvento());
		}
		
		if (source.getSocio() != null) {
			target.setSocio(source.getSocio());
		}

		return target;
	}
}
