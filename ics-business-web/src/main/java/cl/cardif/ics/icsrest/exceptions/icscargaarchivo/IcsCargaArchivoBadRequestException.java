package cl.cardif.ics.icsrest.exceptions.icscargaarchivo;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class IcsCargaArchivoBadRequestException extends RuntimeException {

  private static final long serialVersionUID = -6459654653826812166L;

  public IcsCargaArchivoBadRequestException(String exception) {
    super(exception);
  }

  public IcsCargaArchivoBadRequestException(String exception, Throwable t) {
    super(exception, t);
  }
}
