package cl.cardif.ics.icsrest.exceptions.icssubestructura;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class IcsSubestructuraBadRequestException extends RuntimeException {

  private static final long serialVersionUID = -6459654653826812166L;

  public IcsSubestructuraBadRequestException(String exception) {
    super(exception);
  }

  public IcsSubestructuraBadRequestException(String exception, Throwable t) {
    super(exception, t);
  }
}
