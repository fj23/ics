package cl.cardif.ics.icsrest.domain.pojo;

public class Formula {

  private long id;
  private String contenido;

  public Formula() {
    // Constructor JavaBean
  }

  public String getContenido() {
    return contenido;
  }

  public void setContenido(String contenido) {
    this.contenido = contenido;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  @Override
  public String toString() {
    return "Formula [id=" + id + ", contenido=" + contenido + "]";
  }
}
