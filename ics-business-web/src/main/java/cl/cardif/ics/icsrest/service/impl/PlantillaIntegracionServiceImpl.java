package cl.cardif.ics.icsrest.service.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.OrderSpecifier;

import cl.cardif.ics.domain.entity.common.IcsCore;
import cl.cardif.ics.domain.entity.common.IcsParametro;
import cl.cardif.ics.domain.entity.common.IcsTipoExtension;
import cl.cardif.ics.domain.entity.common.QIcsParametro;
import cl.cardif.ics.domain.entity.plantillas.IcsArchivo;
import cl.cardif.ics.domain.entity.plantillas.IcsAtributoEnriquecimiento;
import cl.cardif.ics.domain.entity.plantillas.IcsAtributoUserIntg;
import cl.cardif.ics.domain.entity.plantillas.IcsAtributoUsuario;
import cl.cardif.ics.domain.entity.plantillas.IcsAtributos;
import cl.cardif.ics.domain.entity.plantillas.IcsAtributosIntg;
import cl.cardif.ics.domain.entity.plantillas.IcsAtributosPlantillaIntg;
import cl.cardif.ics.domain.entity.plantillas.IcsOrdenTrabajosIntg;
import cl.cardif.ics.domain.entity.plantillas.IcsPlantilla;
import cl.cardif.ics.domain.entity.plantillas.QIcsAtributosPlantillaIntg;
import cl.cardif.ics.domain.entity.plantillas.QIcsOrdenTrabajosIntg;
import cl.cardif.ics.icsrest.domain.dto.input.ColumnaArchivoSalidaIntegracionInput;
import cl.cardif.ics.icsrest.domain.dto.input.ColumnaTrabajoIntegracionInput;
import cl.cardif.ics.icsrest.domain.dto.input.MapeoIntegracionInput;
import cl.cardif.ics.icsrest.domain.dto.input.PlantillaIntegracionInput;
import cl.cardif.ics.icsrest.domain.dto.output.AtributoEnriquecimientoOutput;
import cl.cardif.ics.icsrest.domain.dto.output.AtributoUserIntgOutput;
import cl.cardif.ics.icsrest.domain.dto.output.AtributoUsuarioOutput;
import cl.cardif.ics.icsrest.domain.dto.output.PlantillaIntegracionOutput;
import cl.cardif.ics.icsrest.domain.dto.output.PlantillaOutput;
import cl.cardif.ics.icsrest.domain.dto.output.RequestResultOutput;
import cl.cardif.ics.icsrest.domain.dto.output.integracion.AtributosIntgOutput;
import cl.cardif.ics.icsrest.domain.dto.output.integracion.ColumnaArchivoSalidaIntegracionOutput;
import cl.cardif.ics.icsrest.domain.dto.output.integracion.ColumnaTrabajoIntgOutput;
import cl.cardif.ics.icsrest.domain.pojo.Core;
import cl.cardif.ics.icsrest.domain.pojo.TipoExtension;
import cl.cardif.ics.icsrest.exceptions.icsplantilla.IcsPlantillaServiceException;
import cl.cardif.ics.icsrest.formula.IFormulaFactor;
import cl.cardif.ics.icsrest.formula.IFormulaParent;
import cl.cardif.ics.icsrest.formula.operadores.CampoIntegracion;
import cl.cardif.ics.icsrest.repository.AtributoUserIntegracionRepository;
import cl.cardif.ics.icsrest.repository.AtributosIntegracionPlantillaRepository;
import cl.cardif.ics.icsrest.repository.AtributosNegocioRepository;
import cl.cardif.ics.icsrest.repository.AtributosUsuarioRepository;
import cl.cardif.ics.icsrest.repository.OrdenAtributosIntgRepository;
import cl.cardif.ics.icsrest.repository.ParametrosRepository;
import cl.cardif.ics.icsrest.repository.PlantillaRepository;
import cl.cardif.ics.icsrest.service.AtributosService;
import cl.cardif.ics.icsrest.service.PlantillaIntegracionService;
import cl.cardif.ics.icsrest.service.PlantillasService;
import cl.cardif.ics.icsrest.util.Constants;
import cl.cardif.ics.icsrest.util.FormulaOrigenKey;
import cl.cardif.ics.icsrest.util.PlantillaUtil;

//import cl.cardif.ics.icsrest.domain.dto.output.ColumnaTrabajoOutput;

@Service
public class PlantillaIntegracionServiceImpl implements PlantillaIntegracionService {
	private static final Logger LOG = LoggerFactory.getLogger(PlantillaIntegracionServiceImpl.class);

	private static final String MSJ_ERROR_GENERAR_SQL = "Error al generar SQL";

	private PlantillaUtil utilities = new PlantillaUtil();
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private ConversionService conversionService;
	@Autowired
	private AtributosService atributoService;
	@Autowired
	private PlantillasService plantillaService;
	@Autowired
	private AtributosNegocioRepository attrNegocioRepo;
	@Autowired
	private AtributosIntegracionPlantillaRepository attrIntgTplRepo;
	@Autowired
	private AtributosUsuarioRepository attrUsuarioRepo;
	@Autowired
	private AtributoUserIntegracionRepository attrUserIntgRepo;
	@Autowired
	private PlantillaRepository plantillaRepo;
	@Autowired
	private ParametrosRepository paramRepo;
	@Autowired
	private OrdenAtributosIntgRepository ordenTrabajoRepo;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	/**
	 * Recursivamente busca todas las instancias del operador de fórmula
	 * CampoIntegracion. Éste requiere dicho servicio para obtener ciertos datos
	 * complejos a partir de queries.
	 *
	 * @param formula
	 *            El nodo de fórmula en el que se buscan la o las instancias
	 */
	private void inyectarAtributosServiceEnFormula(IFormulaFactor formula) {

		if (formula instanceof CampoIntegracion) {
			CampoIntegracion campoIntg = (CampoIntegracion) formula;
			campoIntg.setAttrSvc(this.atributoService);
			if (campoIntg.getValor() != null && campoIntg.getValor().getFormula() != null) {
				this.inyectarAtributosServiceEnFormula(campoIntg.getValor().getFormula());
			}
		} else if (formula instanceof IFormulaParent) {

			IFormulaParent padre = (IFormulaParent) formula;

			if (padre.getHijos() != null) {
				for (IFormulaFactor hijo : padre.getHijos()) {
					this.inyectarAtributosServiceEnFormula(hijo);
				}
			}
		}
	}

	/**
	 * Obtiene los mapeos cuyo dato de origen provenga de una columna de trabajo
	 * determinada.
	 *
	 * @param listaMapeos
	 *            La lista de mapeos completa de la plantilla asociada
	 * @param columnaOrden
	 *            El numero de la columna de trabajo (su 'orden')
	 * @return Una lista de objetos de mapeo con el mismo tamaño que la lista
	 *         proveída, rellena con nulls para mapeos que no son de la col trabajo
	 *         proveída
	 */
	public List<MapeoIntegracionInput> filtrarMapeosParaColumna(List<MapeoIntegracionInput> listaMapeos,
			long columnaOrden) {
		boolean hayMapeo = false;

		List<MapeoIntegracionInput> maps = new ArrayList<>();
		for (MapeoIntegracionInput mapeo : listaMapeos) {

			Long mapeoColTrabajoOrden = mapeo.getColumnaTrabajoOrden();

			// conserva los indices originales, rellena con null si el mapeo no corresponde
			if (mapeoColTrabajoOrden != null && mapeoColTrabajoOrden == columnaOrden) {
				maps.add(mapeo);
				hayMapeo = true;
			} else {
				maps.add(null);
			}
		}

		if (!hayMapeo) {
			throw new IcsPlantillaServiceException("No hay mapeos para una columna de trabajo.");
		} else {
			return maps;
		}
	}

	private ColumnaArchivoSalidaIntegracionInput getColumnaArchivoSalidaPorIndiceMapeo(
			List<ColumnaArchivoSalidaIntegracionInput> columnasArchivo, int indiceMapeoActual) {
		ColumnaArchivoSalidaIntegracionInput columnaArchivo = null;

		for (ColumnaArchivoSalidaIntegracionInput colArchivo : columnasArchivo) {
			if (indiceMapeoActual == colArchivo.getIndiceMapeoOrigen()) {
				columnaArchivo = colArchivo;
				break;
			}
		}
		return columnaArchivo;
	}

	/**
	 * Cada string contiene la siguiente información, separada por comas:
	 * 
	 * <ul>
	 * <li>id_atributo_evento (destino)</li>
	 * <li>dato_obligatorio (Y/N)</li>
	 * <li>orden_columna_en_archivo</li>
	 * <li>separar_archivos (Y/N)</li>
	 * <li>posicion_inicial_columna</li>
	 * <li>posicion_final_columna</li>
	 * <li>id_atributo_user (destino)</li>
	 * <li>incluir_en_archivo_manual (Y/N)</li>
	 * <li>validar_contenido (Y/N)</li>
	 * </ul>
	 * 
	 * @param mapeo
	 * @param columnaArchivo
	 *            Puede ser null
	 * @return
	 */
	private String mapeoToArregloUtilString(MapeoIntegracionInput mapeo,
			ColumnaArchivoSalidaIntegracionInput columnaArchivo) {
		String obligatorio = (mapeo.isEsObligatorio() ? "Y" : "N");
		String validar = (mapeo.isSeValida() ? "Y" : "N");

		if (columnaArchivo == null) {
			StringBuilder sb = new StringBuilder()
					.append(mapeo.getColumnaDestino()).append(",")
					.append(obligatorio).append(",")
					.append("0").append(",")
					.append("N").append(",")
					.append("0").append(",")
					.append("0").append(",")
					.append(mapeo.getIdAtributoUser()).append(",")
					.append("N").append(",")
					.append(validar);

			return sb.toString();
		} else {
			String separar = (columnaArchivo.getSepararArchivo() ? "Y" : "N");
			String incluir = (columnaArchivo.getIncluirEnArchivo() ? "Y" : "N");

			StringBuilder sb = new StringBuilder()
					.append(mapeo.getColumnaDestino()).append(",")
					.append(obligatorio).append(",")
					.append(columnaArchivo.getOrden()).append(",")
					.append(separar).append(",")
					.append(columnaArchivo.getPosicionInicial()).append(",")
					.append(columnaArchivo.getPosicionFinal()).append(",")
					.append(mapeo.getIdAtributoUser()).append(",")
					.append(incluir).append(",")
					.append(validar);

			return sb.toString();
		}
	}

	/**
	 * Obtiene todos los ID's de los atributos de negocio, junto al tipo de persona,
	 * usados por subnodos de una formula.
	 * 
	 * @param nodoFormula
	 * @return Una colección de FormulaOrigenKey. En la implementación se usa un Set
	 *         para no tener duplicados.
	 */
	private List<FormulaOrigenKey> extraerOrigenesDeNodoFormulaConDescendientes(IFormulaFactor nodoFormula) {
		LOG.info("extraerOrigenesUnicosDeNodosFormulaEHijos");

		Set<FormulaOrigenKey> set = new HashSet<>();
		if (nodoFormula instanceof CampoIntegracion) {
			ColumnaTrabajoIntegracionInput colTrabajo = ((CampoIntegracion) nodoFormula).getValor();
			if (colTrabajo != null) {
				Long idAtributoUsuario = colTrabajo.getColumnaOrigenUser();

				if (idAtributoUsuario != null && idAtributoUsuario != 0) {

					IcsAtributoUsuario icsAtributoUsuario = attrUsuarioRepo.findOne(idAtributoUsuario);
					Long idAtributoNegocio = icsAtributoUsuario.getIcsAtributoNegocio().getIdAtributoNegocio();
					String tipoPersona = icsAtributoUsuario.getTipoPersona();

					if (idAtributoNegocio != null && idAtributoNegocio > 0 && tipoPersona != null
							&& !tipoPersona.isEmpty()) {

						String nombreTablaDestino = attrNegocioRepo.findOne(idAtributoNegocio).getNombreTablaDestino();

						FormulaOrigenKey fKey = new FormulaOrigenKey(idAtributoUsuario, idAtributoNegocio,
								nombreTablaDestino, tipoPersona);
						set.add(fKey);
					}
				}

				IFormulaFactor nodoFormulaHija = colTrabajo.getFormula();

				if (nodoFormulaHija != null) {
					Collection<FormulaOrigenKey> setOrigenesNodoFormulaHija = this
							.extraerOrigenesDeNodoFormulaConDescendientes(nodoFormulaHija);
					set.addAll(setOrigenesNodoFormulaHija);
				}
			}
		} else if (nodoFormula instanceof IFormulaParent) {
			Collection<? extends IFormulaFactor> hijos = ((IFormulaParent) nodoFormula).getHijos();
			for (IFormulaFactor hijo : hijos) {
				Collection<FormulaOrigenKey> setOrigenesNodoFormulaHija = this
						.extraerOrigenesDeNodoFormulaConDescendientes(hijo);
				set.addAll(setOrigenesNodoFormulaHija);
			}
		}

		List<FormulaOrigenKey> lista = new ArrayList<>();
		lista.addAll(set);

		return lista;
	}

	/**
	 * Llama a la función 'fnc_assignsbusinessid' para obtener el atributo de
	 * negocio y atributo de usuario a partir de un id de 'atributo user'
	 * 
	 * @param atributoUserIntgId
	 * @return
	 */
	private String getAtributosDestinoFromAtributoUserIntg(Long atributoUserIntgId) {
		LOG.info("getAtributosDestinoFromAtributoUserIntg");

		SimpleJdbcCall call = new SimpleJdbcCall(jdbcTemplate).withCatalogName(Constants.PACKAGE_UTIL_ICS)
				.withFunctionName(Constants.FUNCTION_FETCH_BUSINESS_ID)
				.declareParameters(new SqlParameter(Constants.PV_USER_ATTRIBUTE_ID, oracle.jdbc.OracleTypes.NUMBER));

		Map<String, Object> args = new HashMap<>();
		args.put(Constants.PV_USER_ATTRIBUTE_ID, atributoUserIntgId);

		try {
			String atributos = call.executeFunction(String.class, new MapSqlParameterSource(args));

			if (atributos != null && !atributos.isEmpty()) {
				return atributos;
			} else {
				return null;
			}
		} catch (Exception e) {
			LOG.error("Error al obtener ID de atributo de negocio", e);
			return null;
		}
	}

	@Override
	public RequestResultOutput callSpAlmacenarPlantilla(PlantillaIntegracionInput plantilla, boolean isManual) {

		Map<String, Object> map = new HashMap<>();

		String nombre = plantilla.getNombre().toUpperCase();
		String version = plantilla.getVersion();
		String idTipo = utilities.safeLongToStringParser(plantilla.getTipo());
		String idSocio = utilities.safeLongToStringParser(plantilla.getSocio());
		String idEvento = utilities.safeLongToStringParser(plantilla.getEvento());
		String idCoreDestino = plantilla.getCore().toString();
		String observaciones = plantilla.getObservaciones();
		String idTipoExtension = utilities.safeLongToStringParser(plantilla.getTipoExtension());
		String idTipoSalida = utilities.safeLongToStringParser(plantilla.getTipoSalida());
		String delimitador = (isManual ? plantilla.getDelimitadorColumnas() : null);
		Integer incluirEncabezados = (plantilla.getIncluirEncabezados() ? 1 : 0);
		String codigoUsuario = plantilla.getCodUsuario();

		map.put(Constants.PV_TEMPLATE_CREATION_NOMBRE, nombre);
		map.put(Constants.PN_TEMPLATE_CREATION_VERSION, version);
		map.put(Constants.PN_TEMPLATE_CREATION_IDTIPOPLANTILLA, idTipo);
		map.put(Constants.PN_TEMPLATE_CREATION_IDSOCIO, idSocio);
		map.put(Constants.PN_TEMPLATE_CREATION_IDEVENTO, idEvento);
		map.put(Constants.PN_TEMPLATE_CREATION_IDCORE, idCoreDestino);
		map.put(Constants.PV_TEMPLATE_CREATION_OBSERVACIONES, observaciones);
		map.put(Constants.PN_TEMPLATE_CREATION_IDTIPOEXTENSION, idTipoExtension);
		map.put(Constants.PN_TEMPLATE_CREATION_IDTIPOSALIDA, idTipoSalida);
		map.put(Constants.PV_TEMPLATE_CREATION_DELIMITADOR, delimitador);
		// este parámetro, aunque mal nombrado, corresponde efectivamente a 'incluir
		// encabezados'
		map.put(Constants.PV_TEMPLATE_CREATION_UNIONARCHIVOS, null);
		map.put(Constants.PN_TEMPLATE_CREATION_IDVERSIONANTERIOR, incluirEncabezados);
		map.put(Constants.PV_TEMPLATE_CREATION_USUARIO, codigoUsuario);
		map.put(Constants.PV_TEMPLATE_CREATION_DIRECTORIOSALIDA, null);

		try {
			return plantillaService.llamarSPAlmacenarPlantilla(map);
		} catch (IcsPlantillaServiceException e) {
			throw new IcsPlantillaServiceException("Error al crear plantilla de integracion", e);
		}

	}

	/**
	 * Procesa los atributos de negocio, generando una clausula FROM de tablas con
	 * alias, y una cláusula WHERE para hacer el join entre ellas. Adicionalmente,
	 * mientras itera sobre los atributos de negocio, agrega a un Map el par "alias,
	 * nombre" de cada tabla.
	 * 
	 * @param tablas
	 *            Un mapa donde se guarden los pares "alias, nombre" de las tablas
	 *            de negocio.
	 * @param safe
	 *            Indica si la salida debe generar SQL Safe.
	 * @return El string SQL concatenado desde las cláusulas FROM y WHERE generadas.
	 */
	public String processSQLClauses(Map<FormulaOrigenKey, Integer> tablas, boolean safe) {

		StringBuilder fromClauseBuilder = new StringBuilder();
		StringBuilder whereClauseBuilder = new StringBuilder();
		int tableCount = tablas.size();

		int i = 0;
		for (Iterator<FormulaOrigenKey> it = tablas.keySet().iterator(); it.hasNext();) {
			FormulaOrigenKey key = it.next();

			Integer indice = tablas.get(key);
			String alias = "T" + (indice);
			String tablaDestino = key.getTablaOrigen();
			int nivelTabla = this.getNivelTabla(tablaDestino, true);

			fromClauseBuilder.append(tablaDestino).append(" ").append(alias);

			if (nivelTabla != 1 || nivelTabla > 3) {

				if (i > 0) {
					whereClauseBuilder.append(" AND ");
				}

				whereClauseBuilder.append(alias).append(".TIPO_ASEG").append(" = ").append("'")
						.append(key.getTipoPersona()).append("'");
			}

			if (i + 1 < tableCount) {
				fromClauseBuilder.append(",");

				String aliasSiguiente = "T" + (indice + 1);

				if (whereClauseBuilder.length() > 0) {
					whereClauseBuilder.append(" AND ");
				}

				whereClauseBuilder.append(alias).append(".ID_TRANSACCION = ").append(aliasSiguiente)
						.append(".ID_TRANSACCION").append(" AND ").append(alias).append(".ID_REGISTRO = ")
						.append(aliasSiguiente).append(".ID_REGISTRO");
			} else if (!safe) {
				whereClauseBuilder.append(" AND T1.ID_TRANSACCION = T0.ID_TRANSACCION")
						.append(" AND T1.ID_REGISTRO = T0.ID_REGISTRO").append(" AND ROWNUM <= 1");
			}
			i++;
		}

		if (safe) {
			return " FROM " + fromClauseBuilder.toString();
		} else {
			return " FROM " + fromClauseBuilder.toString() + " WHERE " + whereClauseBuilder.toString();
		}
	}

	private String generateParentFormulaSQL(IFormulaParent fParent, boolean safe,
			Map<FormulaOrigenKey, Integer> atributos) {

		if (atributos != null && !atributos.isEmpty()) {

			// genera clausulas from y where
			String clauses = this.processSQLClauses(atributos, safe);

			try {
				if (safe) {
					return "SELECT " + fParent.toSafeSQL(atributos) + clauses;
				} else {
					return "SELECT " + fParent.toSQL(atributos) + clauses;
				}
			} catch (Exception exc) {
				LOG.error(MSJ_ERROR_GENERAR_SQL, exc);
			}
		} else {

			try {
				if (safe) {
					return "SELECT " + fParent.toSafeSQL(null) + " FROM DUAL";
				} else {
					return fParent.toSQL(null);
				}
			} catch (Exception exc) {
				LOG.error(MSJ_ERROR_GENERAR_SQL, exc);
			}
		}

		return null;
	}

	public String generateNonParentFormulaSQL(IFormulaFactor formula, boolean safe,
			Map<FormulaOrigenKey, Integer> atributos) {

		if (atributos != null && !atributos.isEmpty()) {

			try {
				if (safe) {
					return "SELECT " + formula.toSafeSQL(atributos) + " FROM DUAL";
				} else {
					return formula.toSQL(atributos);
				}
			} catch (Exception exc) {
				LOG.error(MSJ_ERROR_GENERAR_SQL, exc);
			}
		} else {

			try {
				if (safe) {
					return "SELECT " + formula.toSafeSQL(null) + " FROM DUAL";
				} else {
					return formula.toSQL(null);
				}
			} catch (Exception exc) {
				LOG.error(MSJ_ERROR_GENERAR_SQL, exc);
			}
		}

		return null;
	}

	/**
	 * Genera un mapa de "índices de alias" según una llave (FormulaOrigenKey). Esto
	 * es para que en la generación de SQL, la clase CampoIntegración descendiente
	 * de IFormulaFactor pueda encontrar fácilmente el alias de la tabla que debe
	 * anteponer al campo del que hará select Es decir, "t.campo from tabla t"
	 * 
	 * @param atributos
	 * @return
	 */
	private Map<FormulaOrigenKey, Integer> generateTablasNegocioAliasMap(Collection<FormulaOrigenKey> atributos) {

		Map<FormulaOrigenKey, Integer> map = new HashMap<>();
		int i = 0;
		for (FormulaOrigenKey atr : atributos) {
			map.put(atr, i + 1);
			i++;
		}

		return map;
	}

	private boolean ejecutarSQL(String sql) {
		LOG.info("SQL: {}", sql);
		try {
			jdbcTemplate.execute(sql);
			return true;
		} catch (DataAccessException e) {
			LOG.error("Error al ejecutar la consulta", e);
			return false;
		}
	}

	private IFormulaFactor cargarFormula(String formulaJSON) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		mapper = mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		IFormulaFactor formula = mapper.readValue(formulaJSON, IFormulaFactor.class);
		this.inyectarAtributosServiceEnFormula(formula);
		return formula;
	}

	/**
	 * Intenta convertir un JSON en un objeto fórmula, para luego intenta ejecutar y
	 * así validar el SQL que la fórmula resultante genera.
	 * 
	 * @param formulaJSON
	 *            El JSON a parsear
	 * @return true si el SQL se ejecuta correctamente, false si cualquier proceso
	 *         interno falla
	 */
	@Override
	public boolean testFormulaIntegracion(String formulaJSON) {
		try {
			IFormulaFactor formula = this.cargarFormula(formulaJSON);

			if (formula instanceof IFormulaParent) {
				String sql = this.generateParentFormulaSQL((IFormulaParent) formula, true, null);
				return this.ejecutarSQL(sql);
			} else {
				String sql = this.generateNonParentFormulaSQL(formula, true, null);
				return this.ejecutarSQL(sql);
			}
		} catch (JsonParseException e) {
			LOG.error("JSON malformado", e);
		} catch (JsonMappingException e) {
			LOG.error("No hay clase a la que traducir el JSON", e);
		} catch (IOException e) {
			LOG.error("Error de I/O", e);
		}

		return false;
	}

	/**
	 * Consulta el nivel de datos de la tabla según la tabla ICS_PARAMETROS
	 * 
	 * @param tableName
	 *            El nombre de la tabla
	 * @param socioTable
	 *            true si es una tabla de origen/socio, false si es de
	 *            destino/integracion
	 * @return
	 */
	private Integer getNivelTabla(String tableName, boolean socioTable) {

		QIcsParametro qIcsParams = QIcsParametro.icsParametro;
		BooleanBuilder bb = new BooleanBuilder().and(qIcsParams.conceptoParametro.eq(tableName));

		if (socioTable) {
			bb.and(qIcsParams.descripcionParametro.eq("Jerarquia de Carga Tablon Socio"))
					.and(qIcsParams.codParametro.eq("TBL_SOCIOS"));
		} else {
			bb.and(qIcsParams.descripcionParametro.eq("Jerarquia de Carga Tablon Integracion"))
					.and(qIcsParams.codParametro.eq("TBL_INTEG"));
		}

		IcsParametro param = paramRepo.findOne(bb);
		if (param != null) {
			String parametro = param.getValorParametro();
			return Integer.valueOf(parametro);
		}
		return 0;
	}

	private Map<String, Integer> hacerMapaTablasDestinoNiveles(List<MapeoIntegracionInput> mapeos) {
		Map<String, Integer> nvlsTablasDestino = new HashMap<>();
		for (MapeoIntegracionInput mapeo : mapeos) {
			if (mapeo != null) {
				IcsAtributoUserIntg icsAttrUser = attrUserIntgRepo.findOne(mapeo.getIdAtributoUser());
				if (icsAttrUser != null) {
					String nombreTablaDestino = icsAttrUser.getIcsAtributoIntegracionEvento().getNombreTablaDestino();
					if (nombreTablaDestino != null) {
						Integer nvlTablaDestino = this.getNivelTabla(nombreTablaDestino, false);
						if (!nvlsTablasDestino.containsKey(nombreTablaDestino)) {
							nvlsTablasDestino.put(nombreTablaDestino, nvlTablaDestino);
						}
					}
				}
			}
		}
		return nvlsTablasDestino;
	}

	public String generarStringOrdenTransformaciones(Long formatoLong, Long homologacionLong) {

		if (formatoLong != 0L && homologacionLong != 0L && formatoLong != null && homologacionLong != null) {

			if (formatoLong < homologacionLong) {
				return "F2;H0";
			} else if (homologacionLong < formatoLong) {
				return "H0;F2";
			}
		}

		return null;
	}

	private List<String> cargarColumnasOrigenEnListaMapeosString(ColumnaTrabajoIntegracionInput columna,
			List<MapeoIntegracionInput> mapeos, Collection<String> datosMapeosOriginal, FormulaOrigenKey origenUnico,
			int nvlsDestino) {
		LOG.info("cargarColumnasOrigenEnListaMapeosString");

		List<String> datosMapeosCopia = new ArrayList<>(datosMapeosOriginal);

		// si solo hay 1 tabla/tipo persona en origen, y una sola en destino tambien
		if (origenUnico != null && nvlsDestino == 1) {

			for (int i = 0; i < mapeos.size(); i++) {
				MapeoIntegracionInput mapeo = mapeos.get(i);
				String datosMapeo = datosMapeosCopia.get(i);
				if (mapeo != null && datosMapeo != null) {
					Long atrUserIntgId = mapeo.getIdAtributoUser();

					if (atrUserIntgId == null || atrUserIntgId == 0) {
						throw new IcsPlantillaServiceException("La columna de destino del mapeo es indeterminable.");
					} else {
						String nuevosDatosMapeo = new StringBuilder().append(datosMapeo).append(",")
								.append(origenUnico.getIdAtributoNegocio()).append(",")
								.append(origenUnico.getIdAtributoUsuario()).toString();

						datosMapeosCopia.set(i, nuevosDatosMapeo);
					}
				}
			}
		} else {

			for (int i = 0; i < mapeos.size(); i++) {

				MapeoIntegracionInput mapeo = mapeos.get(i);
				String mapeoComoString = datosMapeosCopia.get(i);
				if (mapeo != null && mapeoComoString != null) {
					Long atrUserId = mapeo.getIdAtributoUser();

					if (atrUserId == null || atrUserId == 0) {
						throw new IcsPlantillaServiceException("La columna de destino del mapeo es indeterminable.");
					} else {
						String atrNegocios = this.getAtributosDestinoFromAtributoUserIntg(atrUserId);
						String nuevosdatosMapeo = new StringBuilder().append(mapeoComoString).append(",")
								.append(atrNegocios).toString();

						datosMapeosCopia.set(i, nuevosdatosMapeo);
					}
				}
			}
		}

		return datosMapeosCopia;
	}

	private boolean tablasOrigenDestinoTienenMuchosNiveles(Collection<FormulaOrigenKey> atributosOrigen,
			Map<String, Integer> tablasDestinoConNiveles) {
		Set<Integer> nivelesTablas = new HashSet<>();

		for (FormulaOrigenKey origen : atributosOrigen) {
			String nombreTabla = origen.getTablaOrigen();
			Integer nivelTabla = this.getNivelTabla(nombreTabla, true);
			nivelesTablas.add(nivelTabla);
		}

		for (Map.Entry<String, Integer> relacion : tablasDestinoConNiveles.entrySet()) {
			Integer nivelTabla = relacion.getValue();
			nivelesTablas.add(nivelTabla);
		}
		return (nivelesTablas.size() > 1);
	}

	/**
	 * Filtra los mapeos cuyo dato de origen provenga de una columna de trabajo
	 * determinada.
	 *
	 * @param listaMapeos
	 *            La lista de mapeos completa de la plantilla asociada
	 * @param columnaOrden
	 *            El numero de la columna de trabajo (su 'orden')
	 * @return Una lista de objetos de mapeo con el mismo tamaño que la lista
	 *         proveída, rellena con nulls para mapeos que no son de la col trabajo
	 *         proveída
	 */
	public List<MapeoIntegracionInput> filtrarMapeosPorOrdenColumnaTrabajo(List<MapeoIntegracionInput> listaMapeos,
			long columnaOrden) {
		boolean hayMapeo = false;

		List<MapeoIntegracionInput> maps = new ArrayList<>();
		for (MapeoIntegracionInput mapeo : listaMapeos) {

			Long mapeoColTrabajoOrden = mapeo.getColumnaTrabajoOrden();

			// conserva los indices originales, rellena con null si el mapeo no corresponde
			if (mapeoColTrabajoOrden != null && mapeoColTrabajoOrden == columnaOrden) {
				maps.add(mapeo);
				hayMapeo = true;
			} else {
				maps.add(null);
			}
		}

		if (!hayMapeo) {
			throw new IcsPlantillaServiceException("No hay mapeos para una columna de trabajo.");
		} else {
			return maps;
		}
	}

	private List<String> hacerDataMapeosArregloUtil(PlantillaIntegracionInput newTemplate,
			List<MapeoIntegracionInput> mapeosParaColumnaTrabajo) {
		LOG.info("hacerDataMapeosArregloUtil");
		List<String> mapeosComoListaString = new ArrayList<>();

		List<ColumnaArchivoSalidaIntegracionInput> columnasArchivo = newTemplate.getColumnasArchivoSalida();
		if (columnasArchivo != null) {

			for (int i = 0; i < mapeosParaColumnaTrabajo.size(); i++) {

				ColumnaArchivoSalidaIntegracionInput columnaArchivo = this
						.getColumnaArchivoSalidaPorIndiceMapeo(columnasArchivo, i);

				MapeoIntegracionInput mapeo = mapeosParaColumnaTrabajo.get(i);
				if (mapeo == null) {
					mapeosComoListaString.add(null);
				} else {
					String mapeoToString = this.mapeoToArregloUtilString(mapeo, columnaArchivo);
					if (mapeo.isSeValida()) { LOG.info(mapeoToString); } 
					mapeosComoListaString.add(mapeoToString);
				}
			}
		} else {
			for (MapeoIntegracionInput mapeo : mapeosParaColumnaTrabajo) {
				if (mapeo == null) {
					mapeosComoListaString.add(null);
				} else {
					String mapeoToString = this.mapeoToArregloUtilString(mapeo, null);
					mapeosComoListaString.add(mapeoToString);
				}
			}
		}

		return mapeosComoListaString;
	}

	/**
	 * Crea un mapa de parámetros para contener la información de una columna de
	 * trabajo.
	 * 
	 * @param newTemplate
	 * @param columna
	 * @return
	 * @throws IcsPlantillaServiceException
	 */
	private Map<Integer, String> columnaTrabajoDataToParamsMap(PlantillaIntegracionInput newTemplate,
			ColumnaTrabajoIntegracionInput columna) {
		LOG.info("columnaTrabajoDataToParamsMap");

		List<MapeoIntegracionInput> mapeosParaColumnaTrabajo = this
				.filtrarMapeosPorOrdenColumnaTrabajo(newTemplate.getMapeos(), columna.getOrden());

		List<String> mapeosComoListaString = this.hacerDataMapeosArregloUtil(newTemplate, mapeosParaColumnaTrabajo);
		LOG.debug("hacerDataMapeosArregloUtil={}", mapeosComoListaString);

		IFormulaFactor formula = columna.getFormula();
		String formulaSQL = null;

		if (formula == null) {

			Long idAtributoNegocio = null;
			Long idAtributoUsuario = columna.getColumnaOrigenUser();
			if (idAtributoUsuario != null) {
				IcsAtributoUsuario icsAtributoUsuario = attrUsuarioRepo.findOne(idAtributoUsuario);
				if (icsAtributoUsuario != null) {
					idAtributoNegocio = icsAtributoUsuario.getIcsAtributoNegocio().getIdAtributoNegocio();
				}
			} else {
				idAtributoNegocio = 0L;
				idAtributoUsuario = 0L;
			}

			for (int i = 0; i < mapeosParaColumnaTrabajo.size(); i++) {

				MapeoIntegracionInput mapeo = mapeosParaColumnaTrabajo.get(i);
				if (mapeo == null) {
					mapeosComoListaString.set(i, null);
				} else {
					String mapeoToString = mapeosComoListaString.get(i);
					StringBuilder sb = new StringBuilder().append(mapeoToString).append(",").append(idAtributoNegocio)
							.append(",").append(idAtributoUsuario);
					mapeosComoListaString.set(i, sb.toString());
				}
			}

		} else {
			this.inyectarAtributosServiceEnFormula(formula);

			FormulaOrigenKey origen = null;

			// id atr usuario + id atr negocio + nombre de tabla + tipo de persona (recordar
			// que puede ser 'no aplica')
			List<FormulaOrigenKey> atributosOrigen = this.extraerOrigenesDeNodoFormulaConDescendientes(formula);
			if (atributosOrigen.size() == 1) {
				origen = atributosOrigen.iterator().next();
			}

			// nunca se repiten niveles
			Map<String, Integer> mapaTablaDestinoNivel = this.hacerMapaTablasDestinoNiveles(mapeosParaColumnaTrabajo);
			LOG.debug("getTablasDestinoConNiveles={}", mapaTablaDestinoNivel);

			int nivelesDestino = mapaTablaDestinoNivel.size();

			// carga columnas de entrada para determinar tablas y alias a registrar
			mapeosComoListaString = this.cargarColumnasOrigenEnListaMapeosString(columna, mapeosParaColumnaTrabajo,
					mapeosComoListaString, origen, nivelesDestino);
			LOG.debug("cargarColumnasOrigenEnListaMapeosString={}", mapeosComoListaString);

			boolean forzarSubquery = this.tablasOrigenDestinoTienenMuchosNiveles(atributosOrigen,
					mapaTablaDestinoNivel);

			// al final del procesamiento, generar la consulta SQL
			// si tiene modo forzar subquery: aun cuando no hayan tablas de origen
			// definidas, hacer join a 'T0'

			Map<FormulaOrigenKey, Integer> atributosMap = this.generateTablasNegocioAliasMap(atributosOrigen);
			int atrCount = atributosMap.size();

			StringBuilder sqlBuilder;
			if (formula instanceof IFormulaParent) {
				IFormulaParent fParent = (IFormulaParent) formula;

				if (atrCount != 1) {

					// si hay 0 o varias tablas de origen...
					formulaSQL = this.generateParentFormulaSQL(fParent, false, atributosMap);

				} else if (forzarSubquery) {

					// si hay 1 tabla de origen pero de distinto nivel a la/las de destino...
					String nombreTablaOrigen = atributosOrigen.iterator().next().getTablaOrigen();
					formulaSQL = this.generateParentFormulaSQL(fParent, false, null);
					if (!Pattern.compile("SELECT").matcher(formulaSQL).find()) {
						sqlBuilder = new StringBuilder().append("(SELECT ").append(formulaSQL).append(" ")
								.append("FROM ").append(nombreTablaOrigen).append(" T1 ")
								.append("WHERE T1.ID_TRANSACCION = T0.ID_TRANSACCION ")
								.append("AND T1.ID_REGISTRO = T0.ID_REGISTRO ").append("AND ROWNUM <= 1)");
						formulaSQL = sqlBuilder.toString();
					}

				} else {

					// si hay 1 tabla de origen y 1 de destino, y ambas son del mismo nivel
					formulaSQL = this.generateParentFormulaSQL(fParent, false, null);

				}
			} else {
				if (atrCount != 1) {

					// si hay 0 o varias tablas de origen...
					formulaSQL = this.generateNonParentFormulaSQL(formula, false, atributosMap);

				} else if (forzarSubquery) {

					// si hay 1 tabla de origen pero de distinto nivel a la/las de destino
					String nombreTablaOrigen = atributosOrigen.iterator().next().getTablaOrigen();
					formulaSQL = this.generateNonParentFormulaSQL(formula, false, null);

					sqlBuilder = new StringBuilder().append("(SELECT ").append(formulaSQL).append(" ").append("FROM ")
							.append(nombreTablaOrigen).append(" T1 ")
							.append("WHERE T1.ID_TRANSACCION = T0.ID_TRANSACCION ")
							.append("AND T1.ID_REGISTRO = T0.ID_REGISTRO ").append("AND ROWNUM <= 1)");
					formulaSQL = sqlBuilder.toString();

				} else {

					// si hay 1 tabla de origen, de destino, y ambas son del mismo nivel
					formulaSQL = this.generateNonParentFormulaSQL(formula, false, null);

				}
			}

		}

		String nombreColumna = columna.getNombre();
		String ordenColumna = utilities.safeLongToStringParser(columna.getOrden());
		String formatoId = utilities.safeLongToStringParser(columna.getFormato());
		String homologacionId = utilities.safeLongToStringParser(columna.getHomologacion());
		String datosMapeosFinal = utilities.concatenarStringsConDelimitador(mapeosComoListaString, ";");
		String enriquecimientoId = utilities.safeLongToStringParser(columna.getColumnaEnriquecimiento());
		String integrationOrder = this.generarStringOrdenTransformaciones(columna.getOrdenFormato(),
				columna.getOrdenHomologacion());
		String sql = formulaSQL;
		String json = formula != null ? formula.toJSON() : null;

		Map<Integer, String> paramsMap = new HashMap<>();
		paramsMap.put(0, nombreColumna);
		paramsMap.put(1, ordenColumna); // indice base 1
		paramsMap.put(2, null);
		paramsMap.put(3, formatoId);
		paramsMap.put(4, homologacionId);
		paramsMap.put(5, null);
		paramsMap.put(6, datosMapeosFinal);
		paramsMap.put(7, enriquecimientoId);
		paramsMap.put(8, integrationOrder);
		paramsMap.put(9, sql);
		paramsMap.put(10, json);
		return paramsMap;
	}

	/**
	 * Procesa los datos de la plantilla de integración, hacia un mapa de parámetros
	 * para llamar al SP de creación propiamente tal. Nota: anteriormente existian
	 * 13 parámetros de envío; algunos se han deprecado a favor de los que ahora son
	 * 10 parámetros a enviar.
	 *
	 * @param newTemplate
	 *            La plantilla de integración a crear o actualizar.
	 */
	@Override
	public RequestResultOutput addOrUpdatePlantillaIntegracion(PlantillaIntegracionInput newTemplate) {
		LOG.info("addOrUpdatePlantillaIntegracion");

		List<Map<Integer, String>> arrayListColumnas = new ArrayList<>();

		// cada col. de trabajo va como una serie de registros en la tabla
		// ICS_ARREGLO_UTIL con sus configuraciones
		for (ColumnaTrabajoIntegracionInput columna : newTemplate.getColumnasTrabajo()) {

			Map<Integer, String> paramsMap = this.columnaTrabajoDataToParamsMap(newTemplate, columna);
			arrayListColumnas.add(paramsMap);
		}

		boolean isManual = (newTemplate.getTipoSalida() == Constants.ID_TIPO_SALIDA_MANUAL);

		plantillaService.almacenarArrayBidimensionalEnArregloUtil(arrayListColumnas);

		return this.callSpAlmacenarPlantilla(newTemplate, isManual);
	}

	private List<IcsAtributosPlantillaIntg> getAtributosPlantillaIntg(Long idPlantilla) {

		BooleanBuilder atributosDePlantilla = new BooleanBuilder()
				.and(QIcsAtributosPlantillaIntg.icsAtributosPlantillaIntg.icsPlantilla.idPlantilla.eq(idPlantilla));

		OrderSpecifier<BigDecimal> ordernarPorNumero = QIcsAtributosPlantillaIntg.icsAtributosPlantillaIntg.numeroColumna
				.asc();

		Iterable<IcsAtributosPlantillaIntg> iteraAtributos = attrIntgTplRepo.findAll(atributosDePlantilla,
				ordernarPorNumero);

		List<IcsAtributosPlantillaIntg> columnas = new ArrayList<>();
		for (IcsAtributosPlantillaIntg icsAtributosPlantillaIntg : iteraAtributos) {
			columnas.add(icsAtributosPlantillaIntg);
		}

		return columnas;
	}

	private List<ColumnaArchivoSalidaIntegracionOutput> hacerColumnasArchivoSalida(
			List<IcsAtributosPlantillaIntg> columnas) {

		Set<ColumnaArchivoSalidaIntegracionOutput> columnasArchivo = new HashSet<>();

		for (IcsAtributosPlantillaIntg icsAttrPltIntg : columnas) {
			if (icsAttrPltIntg.getIcsAtributosIntg() != null) {
				IcsAtributosIntg columna = icsAttrPltIntg.getIcsAtributosIntg();
				// long idIntegracion =
				try {
					IcsAtributos icsAtributo = columna.getIcsAtributos();
					if (icsAtributo != null) {
						ColumnaArchivoSalidaIntegracionOutput columnaArchivo = conversionService.convert(icsAtributo,
								ColumnaArchivoSalidaIntegracionOutput.class);
						columnaArchivo.setIdIntegracion(columna.getIdAtributoIntegracion());
						columnasArchivo.add(columnaArchivo);
					}
				} catch (Exception e) {
					throw new IcsPlantillaServiceException("Error al obtener columnas de archivos", e);
				}
			}
		}

		List<ColumnaArchivoSalidaIntegracionOutput> listaColumnas = new ArrayList<>(columnasArchivo.size());
		for (int i = 0; i < columnasArchivo.size(); i++) {
			listaColumnas.add(null);
		}

		for (ColumnaArchivoSalidaIntegracionOutput col : columnasArchivo) {
			int index = (int) (col.getOrden() - 1);
			listaColumnas.set(index, col);
		}

		return listaColumnas;
	}

	private List<MapeoIntegracionInput> hacerMapeosPlantillaIntegracion(
			List<IcsAtributosPlantillaIntg> attrsPlantillaIntg) {

		List<MapeoIntegracionInput> listaMapeos = new ArrayList<>();

		for (IcsAtributosPlantillaIntg icsAtributoPlantillaIntg : attrsPlantillaIntg) {

			IcsAtributosIntg icsAtributosIntg = icsAtributoPlantillaIntg.getIcsAtributosIntg();
			if (icsAtributosIntg != null) {
				MapeoIntegracionInput mapeo = new MapeoIntegracionInput();
				mapeo.setId(icsAtributosIntg.getIdAtributoIntegracion());
				mapeo.setEsObligatorio("Y".equalsIgnoreCase(icsAtributosIntg.getFlgObligatorio()));
				mapeo.setSeValida("Y".equalsIgnoreCase(icsAtributosIntg.getFlgValidar()));
				mapeo.setIdAtributo(icsAtributoPlantillaIntg.getIdAtributo());

				IcsAtributoUserIntg atributoUserIntg = icsAtributosIntg.getIcsAtributoUserIntg();
				AtributoUserIntgOutput columnaDestinoEM = conversionService.convert(atributoUserIntg,
						AtributoUserIntgOutput.class);
				mapeo.setColumnaDestinoEM(columnaDestinoEM);
				listaMapeos.add(mapeo);
			}
		}

		return listaMapeos;
	}

	private void setDatosArchivoEnPlantillaIntegracion(IcsPlantilla icsPlantilla,
			PlantillaIntegracionOutput plantillaIntegracion) {
		IcsArchivo icsArchivo = icsPlantilla.getIcsArchivo();
		if (icsArchivo != null) {
			plantillaIntegracion.setDelimitadorColumnas(icsArchivo.getDelimitador());
			plantillaIntegracion.setIncluirEncabezados("Y".equals(icsArchivo.getFlgCabecera()));

			IcsTipoExtension icsTipoExtension = icsArchivo.getIcsTipoExtension();
			if (icsTipoExtension != null) {
				TipoExtension tipoExtensionIntg = conversionService.convert(icsTipoExtension, TipoExtension.class);
				plantillaIntegracion.setTipoExtensionIntg(tipoExtensionIntg);
			}
		}
	}

	private List<ColumnaTrabajoIntgOutput> hacerColumnasTrabajo(
			List<IcsAtributosPlantillaIntg> icsAtributosPlantillaIntg) {
		List<ColumnaTrabajoIntgOutput> columnasTrabajo = new ArrayList<>();
		for (IcsAtributosPlantillaIntg icsAtributoPlantillaIntg : icsAtributosPlantillaIntg) {

			ColumnaTrabajoIntgOutput columnaTrabajo = conversionService.convert(icsAtributoPlantillaIntg,
					ColumnaTrabajoIntgOutput.class);
			IcsAtributosIntg icsAtributosIntg = icsAtributoPlantillaIntg.getIcsAtributosIntg();
			Long idAtributoIntegracion = icsAtributosIntg.getIdAtributoIntegracion();

			AtributosIntgOutput atributoIntegracion = conversionService.convert(icsAtributosIntg,
					AtributosIntgOutput.class);

			IcsAtributoUsuario icsAtributoUsuario = icsAtributosIntg.getIcsAtributoUsuario();
			if (icsAtributoUsuario != null) {
				AtributoUsuarioOutput atributoUsuario = conversionService.convert(icsAtributoUsuario,
						AtributoUsuarioOutput.class);
				atributoIntegracion.setColumnaOrigen(atributoUsuario);
			}

			IcsAtributoEnriquecimiento icsAtributoEnriquecimiento = icsAtributosIntg.getIcsAtributoEnriquecimiento();
			if (icsAtributoEnriquecimiento != null) {
				AtributoEnriquecimientoOutput atributoEnriquecimiento = conversionService
						.convert(icsAtributoEnriquecimiento, AtributoEnriquecimientoOutput.class);
				atributoIntegracion.setColumnaEnriquecimiento(atributoEnriquecimiento);
			}

			BooleanBuilder bb = new BooleanBuilder()
					.and(QIcsOrdenTrabajosIntg.icsOrdenTrabajosIntg.icsAtributoIntegracion.idAtributoIntegracion
							.eq(idAtributoIntegracion));
			Iterable<IcsOrdenTrabajosIntg> iteraOrdenTrabajosIntg = ordenTrabajoRepo.findAll(bb);
			for (IcsOrdenTrabajosIntg icsOrden : iteraOrdenTrabajosIntg) {
				switch (icsOrden.getTipoTrabajo()) {
				case "F2":
					atributoIntegracion.setOrdenFormato(icsOrden.getSecuencia().intValue());
					break;
				case "H0":
					atributoIntegracion.setOrdenHomologacion(icsOrden.getSecuencia().intValue());
					break;
				default:
					LOG.warn("Orden de trabajo desconocido {}", icsOrden);
					break;
				}
			}
			columnaTrabajo.setAtributoIntegracion(atributoIntegracion);

			boolean found = false;
			for (ColumnaTrabajoIntgOutput c : columnasTrabajo) {
				if (columnaTrabajo.getId().equals(c.getId())) {
					found = true;
					break;
				}
			}

			if (!found) {
				columnasTrabajo.add(columnaTrabajo);
			}
		}
		return columnasTrabajo;
	}

	@Override
	public PlantillaOutput viewTemplateDetalle(PlantillaOutput plantilla) {
		LOG.info("viewTemplateDetalle");

		IcsPlantilla icsPlantilla = plantillaRepo.findOne(plantilla.getId());

		PlantillaIntegracionOutput plantillaIntegracion = conversionService.convert(plantilla,
				PlantillaIntegracionOutput.class);

		// encabezados, delimitador y extension
		this.setDatosArchivoEnPlantillaIntegracion(icsPlantilla, plantillaIntegracion);

		IcsCore icsCoreDestino = icsPlantilla.getIcsCoreDestino();
		if (icsCoreDestino != null) {
			Core coreDestino = conversionService.convert(icsCoreDestino, Core.class);
			plantillaIntegracion.setCoreDestino(coreDestino);
		}

		// La tabla de 'AtributosPlantillaIntg' es el punto mas alto en la jerarquia de
		// la metadata
		List<IcsAtributosPlantillaIntg> icsAtributosPlantillaIntg = this.getAtributosPlantillaIntg(plantilla.getId());

		List<ColumnaTrabajoIntgOutput> columnas = this.hacerColumnasTrabajo(icsAtributosPlantillaIntg);
		plantillaIntegracion.setColumnasTrabajo(columnas);

		List<MapeoIntegracionInput> mapeos = this.hacerMapeosPlantillaIntegracion(icsAtributosPlantillaIntg);
		plantillaIntegracion.setMapeosIntegracion(mapeos);

		List<ColumnaArchivoSalidaIntegracionOutput> columnasArchivo = this
				.hacerColumnasArchivoSalida(icsAtributosPlantillaIntg);
		plantillaIntegracion.setColumnasArchivoSalida(columnasArchivo);

		return plantillaIntegracion;
	}

	@SuppressWarnings("unchecked")
	public List<AtributoUsuarioOutput> getAtributosUsuarioFromPlantillaEntradaMapeos(long plantillaId) {
		LOG.info("getAtributosUsuarioFromPlantillaEntradaMapeos");
		List<AtributoUsuarioOutput> lista = new ArrayList<>();

		List<Long> atributosUsuarioIDs;

		try {
			RowMapper<Long> rowMapper = new RowMapper<Long>() {
				@Override
				public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
					return rs.getLong(1);
				}
			};

			SimpleJdbcCall call = new SimpleJdbcCall(jdbcTemplate).withCatalogName(Constants.PACKAGE_UTIL_APP)
					.withProcedureName(Constants.PROCEDURE_GET_ATRIBUTOS_USUARIO).declareParameters(
							new SqlParameter(Constants.PV_GET_ATRIBUTOS_USUARIO_PLANTILLA,
									oracle.jdbc.OracleTypes.NUMBER),
							new SqlOutParameter(Constants.PV_GET_ATRIBUTOS_USUARIO_OUT, oracle.jdbc.OracleTypes.CURSOR,
									rowMapper));

			Map<String, Long> paramMap = new HashMap<>();
			paramMap.put(Constants.PV_GET_ATRIBUTOS_USUARIO_PLANTILLA, plantillaId);
			Map<String, Object> result = call.execute(new MapSqlParameterSource(paramMap));

			atributosUsuarioIDs = (List<Long>) (result.get(Constants.PV_GET_ATRIBUTOS_USUARIO_OUT));

		} catch (Exception e) {
			LOG.error("No se pudieron obtener los mapeos de la plantilla de entrada ID " + plantillaId, e);
			atributosUsuarioIDs = null;
		}

		if (atributosUsuarioIDs != null) {
			for (Long id : atributosUsuarioIDs) {
				IcsAtributoUsuario icsAtrUsuario = attrUsuarioRepo.findOne(id);
				AtributoUsuarioOutput icsAtrUsuOutput = conversionService.convert(icsAtrUsuario,
						AtributoUsuarioOutput.class);
				lista.add(icsAtrUsuOutput);
			}
		}

		return lista;
	}
}
