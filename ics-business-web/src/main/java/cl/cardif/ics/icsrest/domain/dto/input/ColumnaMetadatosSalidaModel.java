package cl.cardif.ics.icsrest.domain.dto.input;

public class ColumnaMetadatosSalidaModel extends ColumnaInput {
  private String nombre;
  private Long estructura;
  private Long columnaOrigen;
  private Long posicionInicial;
  private Long posicionFinal;
  private String flgAgrupacion;
  private TipoDatoModel tipoDato;

  public ColumnaMetadatosSalidaModel() {
    super();
  }

  public Long getColumnaOrigen() {
    return columnaOrigen;
  }

  public void setColumnaOrigen(Long columnaOrigen) {
    this.columnaOrigen = columnaOrigen;
  }

  public Long getEstructura() {
    return estructura;
  }

  public void setEstructura(Long estructura) {
    this.estructura = estructura;
  }

  public String getFlgAgrupacion() {
    return flgAgrupacion;
  }

  public void setFlgAgrupacion(String flgAgrupacion) {
    this.flgAgrupacion = flgAgrupacion;
  }

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public Long getPosicionFinal() {
    return posicionFinal;
  }

  public void setPosicionFinal(Long posicionFinal) {
    this.posicionFinal = posicionFinal;
  }

  public Long getPosicionInicial() {
    return posicionInicial;
  }

  public void setPosicionInicial(Long posicionInicial) {
    this.posicionInicial = posicionInicial;
  }

  public TipoDatoModel getTipoDato() {
    return tipoDato;
  }

  public void setTipoDato(TipoDatoModel tipoDato) {
    this.tipoDato = tipoDato;
  }
}
