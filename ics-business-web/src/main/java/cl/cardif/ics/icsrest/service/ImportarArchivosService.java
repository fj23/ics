package cl.cardif.ics.icsrest.service;

import java.util.List;
import java.util.Map;

import org.springframework.security.access.prepost.PostFilter;
import org.springframework.web.multipart.MultipartFile;

import cl.cardif.ics.domain.entity.carga.IcsArchivoImportado;
import cl.cardif.ics.domain.entity.carga.IcsGeneracionArchivo;
import cl.cardif.ics.domain.entity.carga.IcsResumenImportacion;
import cl.cardif.ics.domain.entity.common.IcsEvento;
import cl.cardif.ics.domain.entity.procesos.IcsResumenTransaccion;
import cl.cardif.ics.icsgeneratefile.entity.FilesInformation;
import cl.cardif.ics.icsrest.exceptions.icsimportaarchivo.IcsImportaArchivoServiceException;
import cl.cardif.ics.icsrest.util.OutputListWrapper;

public interface ImportarArchivosService {

	List<IcsResumenTransaccion> getTransacciones(Long idProceso);

	@PostFilter("hasAnyAuthority('EVENT_'+filterObject.id)")
	List<IcsEvento> getEventos(Long idProceso);

	List<IcsResumenTransaccion> getArchivosTransaccion(Map<String, String> filters);

	List<IcsGeneracionArchivo> descargarArchivo(Long idTransaccion);

	byte[] generateFile(String idTransaction, FilesInformation file);

	List<IcsArchivoImportado> cargarArchivo(MultipartFile file, Long idTransaccion)
			throws IcsImportaArchivoServiceException;

	void actualizaImportacion(Long idTransaction) throws IcsImportaArchivoServiceException;

	void eliminaImportacion(List<IcsArchivoImportado> archivoImportados);

	OutputListWrapper<IcsResumenImportacion> getHistorialArchivos(Long idTransaccion, int page, int size,
			String sortColumn, String sortOrder);

	List<IcsResumenImportacion> exportResumenImportacion(Long idTransaccion);

	void deleteGeneratedFile(Long idTransaction, Long tipoArchivo);

}
