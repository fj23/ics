package cl.cardif.ics.icsrest.exceptions.icscargaarchivo;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class IcsCargaArchivoUnauthorizedException extends RuntimeException {

  private static final long serialVersionUID = 3815682526998866691L;

  public IcsCargaArchivoUnauthorizedException(String exception) {
    super(exception);
  }

  public IcsCargaArchivoUnauthorizedException(String exception, Throwable t) {
    super(exception, t);
  }
}
