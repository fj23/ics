package cl.cardif.ics.icsrest.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import cl.cardif.ics.icsrest.domain.dto.input.CargaArchivoInput;
import cl.cardif.ics.icsrest.domain.dto.output.RequestResultOutput;
import cl.cardif.ics.icsrest.exceptions.icsproceso.IcsProcesoNotFoundException;
import cl.cardif.ics.icsrest.exceptions.icsproceso.IcsProcesoServiceException;
import cl.cardif.ics.icsrest.exceptions.login.IcsLoginException;
import cl.cardif.ics.icsrest.service.CargaArchivoService;

@RestController
@RequestMapping("/api/cargaArchivos")
public class CargaArchivoController {
	private static final Logger LOG = LoggerFactory.getLogger(CargaArchivoController.class);
	@Autowired private CargaArchivoService cargaArchivoService;

	@PostMapping("/verifyFileName")
	public ResponseEntity<RequestResultOutput> verifyFileName(@RequestBody CargaArchivoInput cargaArchivo) {
		LOG.info("verifyFileName");
		RequestResultOutput result = new RequestResultOutput();
		result.setCode(0);
		long idProceso = cargaArchivo.getIdProceso();
		if (idProceso != 0) {
			result = cargaArchivoService.verifyFileNameWithSp(cargaArchivo);
		}
		return new ResponseEntity<>(result, HttpStatus.CREATED);
	}

	@PostMapping("/setFile")
	public ResponseEntity<CargaArchivoInput> setFile(@RequestParam("file") MultipartFile file) {
		LOG.info("setFile");
		if (file != null) {
			CargaArchivoInput rowWithFile = cargaArchivoService.createRowWithFile(file);
			return new ResponseEntity<>(rowWithFile, HttpStatus.CREATED);
		}

		return ResponseEntity.badRequest().build();
	}

	@PostMapping("/setCargaArchivo")
	public ResponseEntity<Void> setCargaArchivo(@RequestBody CargaArchivoInput cargaArchivo) {
		LOG.info("setCargaArchivo");
		cargaArchivoService.nuevo(cargaArchivo, false);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@ExceptionHandler({ IcsProcesoServiceException.class })
	public ResponseEntity<String> exceptionHandler(IcsProcesoServiceException ex) {
		return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler({ IcsProcesoNotFoundException.class })
	public ResponseEntity<IcsLoginException> exceptionHandler(IcsProcesoNotFoundException ex) {
		return ResponseEntity.notFound().build();
	}
}
