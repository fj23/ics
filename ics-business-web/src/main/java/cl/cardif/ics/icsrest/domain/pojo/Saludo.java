package cl.cardif.ics.icsrest.domain.pojo;

public class Saludo {

  private int idSaludo;
  private String msgSaludo;

  public Saludo() {
    // Constructor JavaBean
  }

  public int getIdSaludo() {
    return idSaludo;
  }

  public void setIdSaludo(int idSaludo) {
    this.idSaludo = idSaludo;
  }

  public String getMsgSaludo() {
    return msgSaludo;
  }

  public void setMsgSaludo(String msgSaludo) {
    this.msgSaludo = msgSaludo;
  }
}
