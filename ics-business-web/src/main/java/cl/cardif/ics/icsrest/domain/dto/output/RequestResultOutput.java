package cl.cardif.ics.icsrest.domain.dto.output;

import cl.cardif.ics.icsrest.util.Constants;

import java.util.Map;

public class RequestResultOutput {

  private long code;
  private String message;

  /**
   * El constructor sin parametros asigna el codigo y descripcion por defecto
   *
   * @see Constants
   */
  public RequestResultOutput() {
    this.code = Constants.DB_RESULT_CODE_DEFAULT;
    this.message = Constants.DB_RESULT_DESCRIPTION_DEFAULT;
  }

  public RequestResultOutput(long code, String message) {
    super();
    this.code = code;
    this.message = message;
  }

  /**
   * Constructor con mapa
   *
   * @param map Mapa desde el cual se obtiene DB_RESULT_CODE_NAME DB_RESULT_DESCRIPTION_NAME.
   * @see Constants
   */
  public RequestResultOutput(Map<String, Object> map) {
    super();
    this.code = Long.parseLong(map.get(Constants.DB_RESULT_CODE_NAME).toString());
    this.message = map.get(Constants.DB_RESULT_DESCRIPTION_NAME).toString();
  }

  public long getCode() {
    return code;
  }

  public void setCode(long code) {
    this.code = code;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }
}
