package cl.cardif.ics.icsrest.domain.dto.input;

import cl.cardif.ics.icsrest.domain.pojo.DetalleEstructura;

public class DetalleEstructuraInput extends DetalleEstructura {
  private long idTipoDato;

  public DetalleEstructuraInput() {
    // Constructor JavaBean
  }

  public long getIdTipoDato() {
    return idTipoDato;
  }

  public void setIdTipoDato(long tipoDato) {
    this.idTipoDato = tipoDato;
  }
}
