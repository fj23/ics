package cl.cardif.ics.icsrest.domain.dto.output;

import cl.cardif.ics.icsrest.domain.dto.input.BaseColumnaModel;

import java.util.ArrayList;
import java.util.List;

public class SubEstructuraOutput extends BaseColumnaModel {

	private List<DetalleSubEstructuraOutput> columnas;
	private String vigencia;
	private int largo;

	public SubEstructuraOutput() {
		super();
	}

	public List<DetalleSubEstructuraOutput> getColumnas() {
		if (this.columnas != null) {
			return new ArrayList<>(this.columnas);
		} else {
			return new ArrayList<>();
		}
	}

	public void setColumnas(List<DetalleSubEstructuraOutput> columnas) {
		if (columnas != null) {
			this.columnas = new ArrayList<>(columnas);
		}
	}

	public String getVigencia() {
		return vigencia;
	}

	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}

	public int getLargo() {
		return largo;
	}

	public void setLargo(int largo) {
		this.largo = largo;
	}

}
