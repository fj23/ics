package cl.cardif.ics.icsrest.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cl.cardif.ics.icsrest.domain.dto.output.ProcesoOutput;
import cl.cardif.ics.icsrest.domain.pojo.Core;
import cl.cardif.ics.icsrest.domain.pojo.DetalleFormato;
import cl.cardif.ics.icsrest.domain.pojo.Evento;
import cl.cardif.ics.icsrest.domain.pojo.Socio;
import cl.cardif.ics.icsrest.domain.pojo.TipoDato;
import cl.cardif.ics.icsrest.domain.pojo.TipoExtension;
import cl.cardif.ics.icsrest.domain.pojo.TipoFormula;
import cl.cardif.ics.icsrest.domain.pojo.TipoPlantilla;
import cl.cardif.ics.icsrest.domain.pojo.TipoSalida;
import cl.cardif.ics.icsrest.service.CommonService;

@RestController
@RequestMapping("/api")
public class CommonController {
	private static final Logger LOG = LoggerFactory.getLogger(CommonController.class);

	@Autowired private CommonService commonService;

	@GetMapping("/cores")
	public List<Core> getCores() {
		LOG.info("getCores");
		return commonService.getAllCores();
	}

	@GetMapping("/eventos")
	public List<Evento> getEventos() {
		LOG.info("getEventos");
		return commonService.getAllEvents();
	}

	@GetMapping("/socios")
	public List<Socio> getSocios() {
		LOG.info("getSocios");
		return commonService.getAllPartners();
	}

	@GetMapping("/tipos_datos")
	public List<TipoDato> getTiposDatos() {
		LOG.info("getTiposDatos");
		return commonService.getAllDataTypes();
	}

	@GetMapping("/tipos_extension")
	public List<TipoExtension> getTiposExtension() {
		LOG.info("getTiposExtension");
		return commonService.getAllExtensionTypes();
	}

	@GetMapping("/tipos_formato")
	public List<DetalleFormato> getTiposFormato() {
		LOG.info("getTiposFormato");
		return commonService.getAllFormatTypes();
	}

	@GetMapping("/tipos_formula")
	public List<TipoFormula> getTiposFormula() {
		LOG.info("getTiposFormula");
		return commonService.getAllFormulaTypes();
	}

	@GetMapping("/tipos_plantilla")
	public List<TipoPlantilla> getTiposPlantilla() {
		LOG.info("getTiposPlantilla");
		return commonService.getAllTemplateTypes();
	}

	@GetMapping("/tipos_salida")
	public List<TipoSalida> getTiposSalida() {
		LOG.info("getTiposSalida");
		return commonService.getAllOutputTypes();
	}

	@GetMapping("/procesos")
	public ResponseEntity<List<ProcesoOutput>> getProcesos(
		@RequestParam(name = "idSocio", required = false) Long idSocio, @RequestParam(name = "idEvento", required = false) Long idEvento) {
		LOG.info("getProcesos");
		try {
			List<ProcesoOutput> lista = this.commonService.getProcesos(idSocio, idEvento);
			return new ResponseEntity<>(lista, HttpStatus.OK);
		} catch (Exception e) {
			LOG.error("Error al obtener la lista de procesos", e);
			return ResponseEntity.notFound().build();
		}
	}

	@ExceptionHandler({ Exception.class })
	public ResponseEntity<String> exceptionHandler(Exception ex) {
		return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
}
