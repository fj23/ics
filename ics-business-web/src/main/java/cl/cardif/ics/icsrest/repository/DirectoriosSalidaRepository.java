package cl.cardif.ics.icsrest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

import cl.cardif.ics.domain.entity.common.IcsDirectoriosSalida;

@Repository
public interface DirectoriosSalidaRepository
    extends JpaRepository<IcsDirectoriosSalida, Long>, 
		QueryDslPredicateExecutor<IcsDirectoriosSalida> {
	
}
