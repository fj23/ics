package cl.cardif.ics.icsrest.domain.dto.input;

public abstract class BaseModel {
	protected Long id;
	protected String nombre;
	protected String vigencia;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getVigencia() {
		return vigencia;
	}

	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}

	@Override
	public String toString() {
		return "BaseModel [id=" + id + ", nombre=" + nombre + ", vigencia=" + vigencia + "]";
	}

}
