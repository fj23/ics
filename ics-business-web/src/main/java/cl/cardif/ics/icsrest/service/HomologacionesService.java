package cl.cardif.ics.icsrest.service;

import cl.cardif.ics.icsrest.domain.dto.output.TipoHomologacionOutput;
import cl.cardif.ics.icsrest.domain.pojo.Homologacion;
import cl.cardif.ics.icsrest.util.OutputListWrapper;
import cl.cardif.ics.icsrest.util.Permisos;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.List;
import java.util.Map;

public interface HomologacionesService {

	@PreAuthorize(value = "hasAnyAuthority("
	            + Permisos.Mant_Homo_Alta + ","
	            + Permisos.Mant_Homo_Canc + ","
	            + Permisos.Mant_Homo_Reca + ","
	            + Permisos.Mant_Homo_Prosp + ","
	            + Permisos.Mant_Homo_Intera + ","
	            + Permisos.Mant_Homo_Prosp + ")")
	void activarHomologacion(Homologacion detalle);
	
	@PreAuthorize(value = "hasAnyAuthority("
	            + Permisos.Mant_Homo_Alta + ","
	            + Permisos.Mant_Homo_Canc + ","
	            + Permisos.Mant_Homo_Reca + ","
	            + Permisos.Mant_Homo_Prosp + ","
	            + Permisos.Mant_Homo_Intera + ","
	            + Permisos.Mant_Homo_Prosp + ")")
	void actualizarHomologacion(Homologacion detalle);
	
	@PreAuthorize(value = "hasAnyAuthority("
	            + Permisos.Mant_Homo_Alta + ","
	            + Permisos.Mant_Homo_Canc + ","
	            + Permisos.Mant_Homo_Reca + ","
	            + Permisos.Mant_Homo_Prosp + ","
	            + Permisos.Mant_Homo_Intera + ","
	            + Permisos.Mant_Homo_Prosp + ")")
	Long almacenarHomologacion(Homologacion detalle, boolean verificacionAmbosLados);
	
	@PreAuthorize(value = "hasAnyAuthority("
	            + Permisos.Mant_Homo_Alta + ","
	            + Permisos.Mant_Homo_Canc + ","
	            + Permisos.Mant_Homo_Reca + ")")
	void desactivarHomologacion(Homologacion detalle);
	
	@PreAuthorize(value = "hasAnyAuthority("
	            + Permisos.Cslt_Homo + ","
	            + Permisos.Mant_Homo_Alta + ","
	            + Permisos.Mant_Homo_Canc + ","
	            + Permisos.Mant_Homo_Reca + ","
	            + Permisos.Mant_Homo_Prosp + ","
	            + Permisos.Mant_Homo_Intera + ","
	            + Permisos.Mant_Homo_Prosp + ")")
	List<String> getAllDistinctConceptosHomologacion();
	
	@PreAuthorize(value = "hasAnyAuthority("
	            + Permisos.Cslt_Homo + ","
	            + Permisos.Mant_Homo_Alta + ","
	            + Permisos.Mant_Homo_Canc + ","
	            + Permisos.Mant_Homo_Reca + ","
	            + Permisos.Mant_Homo_Prosp + ","
	            + Permisos.Mant_Homo_Intera + ","
	            + Permisos.Mant_Homo_Prosp + ")")
	OutputListWrapper<Homologacion> getHomologaciones(
	    int page, int size, String sortColumn, String sortOrder, Map<String, String> filters);
	
	@PreAuthorize(value = "hasAnyAuthority("
	            + Permisos.Cslt_Homo + ","
	            + Permisos.Mant_Homo_Alta + ","
	            + Permisos.Mant_Homo_Canc + ","
	            + Permisos.Mant_Homo_Reca + ","
	    	            + Permisos.Mant_Homo_Prosp + ","
	    	            + Permisos.Mant_Homo_Intera + ","
	    	            + Permisos.Mant_Homo_Prosp + ")")
	List<TipoHomologacionOutput> getTiposHomologaciones(int socioId, int coreId);
}
