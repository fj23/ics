package cl.cardif.ics.icsrest.service;

import cl.cardif.ics.icsrest.domain.dto.input.ProcesoInput;
import cl.cardif.ics.icsrest.domain.dto.output.ProcesoOutput;
import cl.cardif.ics.icsrest.domain.pojo.CallCenter;
import cl.cardif.ics.icsrest.domain.pojo.DirectorioSalida;
import cl.cardif.ics.icsrest.domain.pojo.TipoArchivo;
import cl.cardif.ics.icsrest.util.OutputListWrapper;
import cl.cardif.ics.icsrest.util.Permisos;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.List;
import java.util.Map;

public interface ProcesoService {

  @PreAuthorize(value = "hasAnyAuthority("
              + Permisos.Admi_PrCa_Alta + ","
              + Permisos.Admi_PrCa_Canc + ","
              + Permisos.Admi_PrCa_Reca + ","
              + Permisos.Admi_PrCa_Prosp + ","
              + Permisos.Admi_PrCa_Intera + ","
              + Permisos.Admi_PrCa_Pago + ")")
  void activarProceso(long idProceso);

  @PreAuthorize(value = "hasAnyAuthority("
              + Permisos.Admi_PrCa_Alta + ","
              + Permisos.Admi_PrCa_Canc + ","
              + Permisos.Admi_PrCa_Reca + ","
              + Permisos.Admi_PrCa_Prosp + ","
              + Permisos.Admi_PrCa_Intera + ","
              + Permisos.Admi_PrCa_Pago + ")")
  void almacenarProceso(ProcesoInput input, boolean isUpdate);

  @PreAuthorize(value = "hasAnyAuthority("
              + Permisos.Admi_PrCa_Alta + ","
              + Permisos.Admi_PrCa_Canc + ","
              + Permisos.Admi_PrCa_Reca + ","
              + Permisos.Admi_PrCa_Prosp + ","
              + Permisos.Admi_PrCa_Intera + ","
              + Permisos.Admi_PrCa_Pago + ")")
  void desactivarProceso(long idProceso);

  @PreAuthorize(value = "hasAnyAuthority("
              + Permisos.Cslt_PrCa + ","
              + Permisos.Admi_PrCa_Alta + ","
              + Permisos.Admi_PrCa_Canc + ","
              + Permisos.Admi_PrCa_Reca + ","
              + Permisos.Admi_PrCa_Prosp + ","
              + Permisos.Admi_PrCa_Intera + ","
              + Permisos.Admi_PrCa_Pago + ","
              + Permisos.Term_PrCa_Alta + ","
              + Permisos.Term_PrCa_Canc + ","
              + Permisos.Term_PrCa_Reca + ","
              + Permisos.Term_PrCa_Prosp + ","
              + Permisos.Term_PrCa_Intera + ","
              + Permisos.Term_PrCa_Pago + ")")
  List<ProcesoOutput> getAllVigentes(Long idSocio);

  @PreAuthorize(value = "hasAnyAuthority("
              + Permisos.Cslt_PrCa + ","
              + Permisos.Admi_PrCa_Alta + ","
              + Permisos.Admi_PrCa_Canc + ","
              + Permisos.Admi_PrCa_Reca + ","
              + Permisos.Admi_PrCa_Prosp + ","
              + Permisos.Admi_PrCa_Intera + ","
              + Permisos.Admi_PrCa_Pago + ","
              + Permisos.Term_PrCa_Alta + ","
              + Permisos.Term_PrCa_Canc + ","
              + Permisos.Term_PrCa_Reca + ","
              + Permisos.Term_PrCa_Prosp + ","
              + Permisos.Term_PrCa_Intera + ","
              + Permisos.Term_PrCa_Pago + ")")
  ProcesoOutput getProceso(long id);

  @PreAuthorize(value = "hasAnyAuthority("
              + Permisos.Cslt_PrCa + ","
              + Permisos.Admi_PrCa_Alta + ","
              + Permisos.Admi_PrCa_Canc + ","
              + Permisos.Admi_PrCa_Reca + ","
              + Permisos.Admi_PrCa_Prosp + ","
              + Permisos.Admi_PrCa_Intera + ","
              + Permisos.Admi_PrCa_Pago + ","
              + Permisos.Term_PrCa_Alta + ","
              + Permisos.Term_PrCa_Canc + ","
              + Permisos.Term_PrCa_Reca + ","
              + Permisos.Term_PrCa_Prosp + ","
              + Permisos.Term_PrCa_Intera + ","
              + Permisos.Term_PrCa_Pago + ")")
  OutputListWrapper<ProcesoOutput> getProcesos(
      int page, int size, String sortColumn, String sortOrder, Map<String, String> filters);

  @PreAuthorize(value = "hasAnyAuthority("
              + Permisos.Cslt_PrCa + ","
              + Permisos.Admi_PrCa_Alta + ","
              + Permisos.Admi_PrCa_Canc + ","
              + Permisos.Admi_PrCa_Reca + ","
              + Permisos.Admi_PrCa_Prosp + ","
              + Permisos.Admi_PrCa_Intera + ","
              + Permisos.Admi_PrCa_Pago + ","
              + Permisos.Term_PrCa_Alta + ","
              + Permisos.Term_PrCa_Canc + ","
              + Permisos.Term_PrCa_Reca + ","
              + Permisos.Term_PrCa_Prosp + ","
              + Permisos.Term_PrCa_Intera + ","
              + Permisos.Term_PrCa_Pago + ")")
  List<String> getNombresAll();

  @PreAuthorize(value = "hasAnyAuthority("
              + Permisos.Cslt_PrCa + ","
              + Permisos.Admi_PrCa_Alta + ","
              + Permisos.Admi_PrCa_Canc + ","
              + Permisos.Admi_PrCa_Reca + ","
              + Permisos.Admi_PrCa_Prosp + ","
              + Permisos.Admi_PrCa_Intera + ","
              + Permisos.Admi_PrCa_Pago + ","
              + Permisos.Term_PrCa_Alta + ","
              + Permisos.Term_PrCa_Canc + ","
              + Permisos.Term_PrCa_Reca + ","
              + Permisos.Term_PrCa_Prosp + ","
              + Permisos.Term_PrCa_Intera + ","
              + Permisos.Term_PrCa_Pago + ")")
  List<CallCenter> getCallCenters(Long idSocio, Long idEvento);

  @PreAuthorize(value = "hasAnyAuthority("
              + Permisos.Cslt_PrCa + ","
              + Permisos.Admi_PrCa_Alta + ","
              + Permisos.Admi_PrCa_Canc + ","
              + Permisos.Admi_PrCa_Reca + ","
              + Permisos.Admi_PrCa_Prosp + ","
              + Permisos.Admi_PrCa_Intera + ","
              + Permisos.Admi_PrCa_Pago + ","
              + Permisos.Term_PrCa_Alta + ","
              + Permisos.Term_PrCa_Canc + ","
              + Permisos.Term_PrCa_Reca + ","
              + Permisos.Term_PrCa_Prosp + ","
              + Permisos.Term_PrCa_Intera + ","
              + Permisos.Term_PrCa_Pago + ")")
  List<TipoArchivo> getTiposArchivo();

  @PreAuthorize(value = "hasAnyAuthority("
              + Permisos.Cslt_PrCa + ","
              + Permisos.Admi_PrCa_Alta + ","
              + Permisos.Admi_PrCa_Canc + ","
              + Permisos.Admi_PrCa_Reca + ","
              + Permisos.Admi_PrCa_Prosp + ","
              + Permisos.Admi_PrCa_Intera + ","
              + Permisos.Admi_PrCa_Pago + ","
              + Permisos.Term_PrCa_Alta + ","
              + Permisos.Term_PrCa_Canc + ","
              + Permisos.Term_PrCa_Reca + ","
              + Permisos.Term_PrCa_Prosp + ","
              + Permisos.Term_PrCa_Intera + ","
              + Permisos.Term_PrCa_Pago + ")")
  List<DirectorioSalida> getDirectoriosError();
  
  
}
