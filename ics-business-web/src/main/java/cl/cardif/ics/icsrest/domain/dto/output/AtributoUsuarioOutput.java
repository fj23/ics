package cl.cardif.ics.icsrest.domain.dto.output;

public class AtributoUsuarioOutput {
	private Long idAtributoUsuario;
	private String tipoPersona;
	private String nombreColumna;
	private String flgCardinalidad;

	private Long idAtributoNegocio;
	private String nombreColumnaNegocio;
	private String nombreTablaDestino;
	private Long cardinalidadMaxima;

	public AtributoUsuarioOutput() {
		super();
	}

	public String getFlgCardinalidad() {
		return flgCardinalidad;
	}

	public void setFlgCardinalidad(String flgCardinalidad) {
		this.flgCardinalidad = flgCardinalidad;
	}

	public Long getIdAtributoNegocio() {
		return idAtributoNegocio;
	}

	public void setIdAtributoNegocio(Long idAtributoNegocio) {
		this.idAtributoNegocio = idAtributoNegocio;
	}

	public Long getIdAtributoUsuario() {
		return idAtributoUsuario;
	}

	public void setIdAtributoUsuario(Long idAtributoUsuario) {
		this.idAtributoUsuario = idAtributoUsuario;
	}

	public String getNombreColumna() {
		return nombreColumna;
	}

	public void setNombreColumna(String nombreColumna) {
		this.nombreColumna = nombreColumna;
	}

	public String getNombreColumnaNegocio() {
		return nombreColumnaNegocio;
	}

	public void setNombreColumnaNegocio(String nombreColumnaNegocio) {
		this.nombreColumnaNegocio = nombreColumnaNegocio;
	}

	public String getTipoPersona() {
		return tipoPersona;
	}

	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	public String getNombreTablaDestino() {
		return nombreTablaDestino;
	}

	public void setNombreTablaDestino(String nombreTablaDestino) {
		this.nombreTablaDestino = nombreTablaDestino;
	}

	public Long getCardinalidadMaxima() {
		return cardinalidadMaxima;
	}

	public void setCardinalidadMaxima(Long cardinalidadMaxima) {
		this.cardinalidadMaxima = cardinalidadMaxima;
	}

	@Override
	public String toString() {
		return "AtributoUsuarioOutput [idAtributoUsuario=" + idAtributoUsuario + ", tipoPersona=" + tipoPersona
				+ ", nombreColumna=" + nombreColumna + ", flgCardinalidad=" + flgCardinalidad + ", idAtributoNegocio="
				+ idAtributoNegocio + ", nombreColumnaNegocio=" + nombreColumnaNegocio + ", nombreTablaDestino="
				+ nombreTablaDestino + "]";
	}
	
	
}
