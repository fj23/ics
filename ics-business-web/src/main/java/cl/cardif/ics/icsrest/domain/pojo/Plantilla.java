package cl.cardif.ics.icsrest.domain.pojo;

public class Plantilla {
	private Long id;
	private String nombre;
	private Core core;
	private Evento evento;
	private Socio socio;
	private TipoSalida tipoSalida;
	private TipoPlantilla tipo;
	private String observaciones;
	private String plantillaMultiarchivo;
	private Long version;
	private String codUsuario;
	private String fechaCreacion;
	private String vigencia;

	public Plantilla() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Core getCore() {
		return core;
	}

	public void setCore(Core core) {
		this.core = core;
	}

	public Evento getEvento() {
		return evento;
	}

	public void setEvento(Evento evento) {
		this.evento = evento;
	}

	public Socio getSocio() {
		return socio;
	}

	public void setSocio(Socio socio) {
		this.socio = socio;
	}

	public TipoSalida getTipoSalida() {
		return tipoSalida;
	}

	public void setTipoSalida(TipoSalida tipoSalida) {
		this.tipoSalida = tipoSalida;
	}

	public TipoPlantilla getTipo() {
		return tipo;
	}

	public void setTipo(TipoPlantilla tipo) {
		this.tipo = tipo;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getPlantillaMultiarchivo() {
		return plantillaMultiarchivo;
	}

	public void setPlantillaMultiarchivo(String plantillaMultiarchivo) {
		this.plantillaMultiarchivo = plantillaMultiarchivo;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public String getCodUsuario() {
		return codUsuario;
	}

	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}

	public String getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(String fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getVigencia() {
		return vigencia;
	}

	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}

}
