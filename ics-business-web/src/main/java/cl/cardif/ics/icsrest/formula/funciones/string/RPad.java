package cl.cardif.ics.icsrest.formula.funciones.string;

import java.util.ArrayList;
import java.util.Map;

import cl.cardif.ics.icsrest.formula.FormulaParent;
import cl.cardif.ics.icsrest.formula.IFormulaFactor;
import cl.cardif.ics.icsrest.formula.IStringFactor;
import cl.cardif.ics.icsrest.formula.operadores.NumberLiteral;
import cl.cardif.ics.icsrest.util.FormulaOrigenKey;

public class RPad extends FormulaParent implements IStringFactor {
	public RPad() {
		hijos = new ArrayList<>();
	}

	@Override
	public int getMaximumChildrenCount() {
		return 3;
	}

	@Override
	public String toSafeSQL(Map<FormulaOrigenKey, Integer> tableAliases) {
		StringBuilder sb = new StringBuilder()
				.append("RPAD")
				.append("(");
		
		IFormulaFactor characters = getHijo(0);
		if (characters instanceof NumberLiteral) {
			sb.append("'").append(characters.toSafeSQL(tableAliases)).append("'");
		} else {
			sb.append(characters.toSafeSQL(tableAliases));
		}
		
		sb.append(",")
			.append(getHijo(1).toSafeSQL(tableAliases)).append(",")
			.append(getHijo(2).toSafeSQL(tableAliases))
			.append(")");

		return sb.toString();
	}

	@Override
	public String toSQL(Map<FormulaOrigenKey, Integer> tableAliases) {
		StringBuilder sb = new StringBuilder()
				.append("RPAD")
				.append("(");
		
		IFormulaFactor characters = getHijo(0);
		if (characters instanceof NumberLiteral) {
			sb.append("'").append(characters.toSQL(tableAliases)).append("'");
		} else {
			sb.append(characters.toSQL(tableAliases));
		}
		
		sb.append(",")
			.append(getHijo(1).toSQL(tableAliases)).append(",")
			.append(getHijo(2).toSQL(tableAliases))
			.append(")");

		return sb.toString();
	}
}
