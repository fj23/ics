package cl.cardif.ics.icsrest.util.converters.toentity;

import cl.cardif.ics.domain.entity.plantillas.IcsPlantilla;
import cl.cardif.ics.domain.entity.plantillas.QIcsPlantilla;
import cl.cardif.ics.domain.entity.procesos.IcsDetalleProceso;
import cl.cardif.ics.icsrest.domain.dto.input.DetalleProcesoInput;
import cl.cardif.ics.icsrest.exceptions.icsplantilla.IcsPlantillaNotFoundException;
import cl.cardif.ics.icsrest.repository.PlantillaRepository;
import com.querydsl.core.BooleanBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;

@Component
public class IcsDetalleProcesoFromInputConverter
    implements Converter<DetalleProcesoInput, IcsDetalleProceso> {

  static final Logger LOG = LoggerFactory.getLogger(IcsDetalleProcesoFromInputConverter.class);

  @Autowired private PlantillaRepository plantillaRepository;

  @Override
  public IcsDetalleProceso convert(DetalleProcesoInput pojo) {

    IcsDetalleProceso detalleEntity = new IcsDetalleProceso();

    LOG.info("Convertir DetalleProcesoInput a IcsDetalleProceso");

    detalleEntity.setIdDetalleProceso(pojo.getId());

    LOG.info("ID DetalleProceso : " + pojo.getId());

    IcsPlantilla plantilla = null;
    if (pojo.getIdPlantilla() > 0) {
      BooleanBuilder booleanBuilderTipoDato = new BooleanBuilder();
      QIcsPlantilla qIcsPlantilla = QIcsPlantilla.icsPlantilla;

      booleanBuilderTipoDato.and(qIcsPlantilla.idPlantilla.eq(pojo.getIdPlantilla()));

      try {
        plantilla = this.plantillaRepository.findOne(booleanBuilderTipoDato);
      } catch (EntityNotFoundException e) {
        throw new IcsPlantillaNotFoundException("Plantila no encontrada", e);
      }

      LOG.info("Obtiene plantilla : " + plantilla.getIdPlantilla());
      //detalleEntity.setIdPlantilla(plantilla.getIdPlantilla());
      detalleEntity.setIcsPlantilla(plantilla);

    } else {
      plantilla = new IcsPlantilla();
      //detalleEntity.setIdPlantilla(plantilla.getIdPlantilla());
      detalleEntity.setIcsPlantilla(plantilla);
    }

    return detalleEntity;
  }
}
