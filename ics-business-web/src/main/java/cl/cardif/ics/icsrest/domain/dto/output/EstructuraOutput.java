package cl.cardif.ics.icsrest.domain.dto.output;

import cl.cardif.ics.icsrest.domain.dto.input.EstructuraAnetoSimpleInput;

import java.util.ArrayList;
import java.util.List;

public class EstructuraOutput extends EstructuraAnetoSimpleInput {

	private List<DetalleEstructuraOutput> columnas;
	private String flgSatelite;
	private String vigencia;
	private int largo;

	public EstructuraOutput() {
		super();
	}

	public List<DetalleEstructuraOutput> getColumnas() {
		if (this.columnas != null) {
			return new ArrayList<>(this.columnas);
		} else {
			return new ArrayList<>();
		}
	}

	public void setColumnas(List<DetalleEstructuraOutput> columnas) {
		if (columnas != null) {
			this.columnas = new ArrayList<>(columnas);
		}
	}

	public String getFlgSatelite() {
		return flgSatelite;
	}

	public void setFlgSatelite(String flgSatelite) {
		this.flgSatelite = flgSatelite;
	}

	public String getVigencia() {
		return vigencia;
	}

	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}

	public int getLargo() {
		return largo;
	}

	public void setLargo(int largo) {
		this.largo = largo;
	}

}
