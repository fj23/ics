package cl.cardif.ics.icsrest.domain.pojo;

import java.util.ArrayList;
import java.util.List;

public class OutputFile {

	private List<String> data;
	private String archiveName;
	private String extension;
	private List<String> sheet;
	private List<String> dataType;
	private String directory;
	private String outType;

	public OutputFile() {
		// Constructor
	}

	public List<String> getData() {
		return new ArrayList<>(data);
	}

	public void setData(List<String> data) {
		List<String> newData = new ArrayList<>(data);
		this.data = newData;
	}

	public String getArchiveName() {
		return archiveName;
	}

	public void setArchiveName(String archiveName) {
		this.archiveName = archiveName;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public List<String> getSheet() {
		return new ArrayList<>(sheet);
	}

	public void setSheet(List<String> sheet) {
		List<String> newSheet = new ArrayList<>(sheet);
		this.sheet = newSheet;
	}

	public List<String> getDataType() {
		return new ArrayList<>(dataType);
	}

	public void setDataType(List<String> dataType) {
		List<String> newDataType = new ArrayList<>(dataType);
		this.dataType = newDataType;
	}

	public String getDirectory() {
		return directory;
	}

	public void setDirectory(String directory) {
		this.directory = directory;
	}

	public String getOutType() {
		return outType;
	}

	public void setOutType(String outType) {
		this.outType = outType;
	}
}
