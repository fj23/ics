package cl.cardif.ics.icsrest.message.response;

import cl.cardif.ics.icsrest.domain.pojo.DetalleRol;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AuthLoginResponse {
	private String username;
	private String password;
	private String token;
	private List<String> roles = new ArrayList<>();
	private Map<Long, DetalleRol> detallesRolMap;
	private Boolean result;

	public AuthLoginResponse() {
		this.result = false;
	}

	public AuthLoginResponse(String username, String password, String token, Boolean result) {
		this.username = username;
		this.password = password;
		this.token = token;
		this.result = result;
	}

	public AuthLoginResponse(Boolean result) {
		this.result = result;
	}

	public Map<Long, DetalleRol> getDetallesRolMap() {
		return detallesRolMap;
	}

	public void setDetallesRolMap(Map<Long, DetalleRol> detallesRolMap) {
		this.detallesRolMap = detallesRolMap;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Boolean getResult() {
		return result;
	}

	public void setResult(Boolean result) {
		this.result = result;
	}

	public List<String> getRoles() {
		if (this.roles != null) {
			return new ArrayList<>(this.roles);
		} else {
			return new ArrayList<>();
		}
	}

	public void setRoles(List<String> roles) {
		if (roles != null) {
			this.roles = new ArrayList<>(roles);
		}
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public String toString() {
		return "AuthLoginResponse [username=" + username + ", password=" + password + ", token=" + token + ", roles="
				+ roles + ", detallesRolMap=" + detallesRolMap + ", result=" + result + "]";
	}
}
