package cl.cardif.ics.icsrest.domain.dto.output;

public abstract class AtributosIntg {

  protected String nombreTablaDestino;
  protected String flgVisible;
  protected String traduccion;

  public String getFlgVisible() {
    return flgVisible;
  }

  public void setFlgVisible(String flgVisible) {
    this.flgVisible = flgVisible;
  }

  public String getNombreTablaDestino() {
    return this.nombreTablaDestino;
  }

  public void setNombreTablaDestino(String nombreTablaDestino) {
    this.nombreTablaDestino = nombreTablaDestino;
  }

  public String getTraduccion() {
    return traduccion;
  }

  public void setTraduccion(String traduccion) {
    this.traduccion = traduccion;
  }
}
