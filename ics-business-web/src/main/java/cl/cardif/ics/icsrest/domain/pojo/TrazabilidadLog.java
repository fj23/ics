package cl.cardif.ics.icsrest.domain.pojo;

import java.io.Serializable;

public class TrazabilidadLog implements Serializable {
	private static final long serialVersionUID = -8353533271830074205L;
	
	private byte[] archivoOriginal;
	private String extension;
	private String nombreArchivo;

	public TrazabilidadLog(byte[] archivoOriginal, String extension, String nombreArchivo) {
		this.setArchivoOriginal(archivoOriginal);
		this.extension = extension;
		this.nombreArchivo = nombreArchivo;
	}

	public byte[] getArchivoOriginal() {
		return archivoOriginal != null ? archivoOriginal.clone() : null;
	}

	public void setArchivoOriginal(byte[] archivoOriginal) {
		this.archivoOriginal = archivoOriginal != null ? archivoOriginal.clone() : null;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public String getNombreArchivo() {
		return nombreArchivo;
	}

	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

}
