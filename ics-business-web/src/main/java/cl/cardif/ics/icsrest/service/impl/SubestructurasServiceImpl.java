package cl.cardif.ics.icsrest.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityNotFoundException;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Service;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.OrderSpecifier;

import cl.cardif.ics.domain.entity.common.IcsTipoDato;
import cl.cardif.ics.domain.entity.common.QIcsTipoDato;
import cl.cardif.ics.domain.entity.estructuras.IcsDetalleSubestructura;
import cl.cardif.ics.domain.entity.estructuras.IcsSubestructura;
import cl.cardif.ics.domain.entity.estructuras.QIcsDetalleSubestructura;
import cl.cardif.ics.domain.entity.estructuras.QIcsSubestructura;
import cl.cardif.ics.icsrest.domain.dto.input.ColumnaMetadatosModel;
import cl.cardif.ics.icsrest.domain.dto.input.EstructuraAnetoModel;
import cl.cardif.ics.icsrest.domain.dto.input.SubEstructuraInput;
import cl.cardif.ics.icsrest.domain.dto.output.DetalleSubEstructuraOutput;
import cl.cardif.ics.icsrest.domain.dto.output.RequestResultOutput;
import cl.cardif.ics.icsrest.domain.dto.output.SubEstructuraOutput;
import cl.cardif.ics.icsrest.domain.dto.output.SubEstructuraSimpleOutput;
import cl.cardif.ics.icsrest.exceptions.icsestructura.IcsEstructuraNotFoundException;
import cl.cardif.ics.icsrest.exceptions.icssubestructura.IcsSubestructuraNotFoundException;
import cl.cardif.ics.icsrest.exceptions.icssubestructura.IcsSubestructuraServiceException;
import cl.cardif.ics.icsrest.exceptions.icstipodato.IcsTipoDatoNotFoundException;
import cl.cardif.ics.icsrest.repository.DetallesSubestructurasRepository;
import cl.cardif.ics.icsrest.repository.SubestructurasRepository;
import cl.cardif.ics.icsrest.repository.TiposDatosRepository;
import cl.cardif.ics.icsrest.service.SubestructurasService;
import cl.cardif.ics.icsrest.util.OutputListWrapper;

@Service
public class SubestructurasServiceImpl implements SubestructurasService {
	private static final Logger LOG = LoggerFactory.getLogger(SubestructurasServiceImpl.class);

	private static final String CODIGO_SUBESCTRUCTURA = "codigoSubestructura";
	private static final String NOMBRE_SUBESCTRUCTURA = "descripcionSubestructura";

	private JdbcTemplate jdbcTemplate;

	@Autowired
	private TiposDatosRepository tiposDatosRepo;
	@Autowired
	private SubestructurasRepository subestructurasRepository;
	@Autowired
	private DetallesSubestructurasRepository detalleSubestructurasRepository;
	@Autowired
	private ConversionService conversionService;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	private IcsDetalleSubestructura convertirColumnaMetadatosAIcsDetalleSubestructura(
			ColumnaMetadatosModel columnaMetadatos) {
		IcsDetalleSubestructura detalleEstructura = conversionService.convert(columnaMetadatos,
				IcsDetalleSubestructura.class);

		Long idTipoDato = columnaMetadatos.getTipoDato();

		BooleanBuilder bb = new BooleanBuilder().and(QIcsTipoDato.icsTipoDato.idTipoDato.eq(idTipoDato));

		try {
			IcsTipoDato tipoEntity = tiposDatosRepo.findOne(bb);
			detalleEstructura.setIcsTipoDato(tipoEntity);
			detalleEstructura.setTipoDato(tipoEntity.getNombreTipoDato().substring(0, 1));
		} catch (EntityNotFoundException e) {
			throw new IcsTipoDatoNotFoundException("El tipo de dato de la columna no pudo ser encontrado", e);
		}
		return detalleEstructura;
	}

	@Override
	public void almacenarSubestructura(EstructuraAnetoModel input) {
		final String method = "almacenarSubestructura";
		LOG.info(method);

		IcsSubestructura entity = conversionService.convert(input, IcsSubestructura.class);
		entity.setVigenciaSubestructura("Y");

		entity = subestructurasRepository.save(entity);

		final String log1 = method + " - Insercion con ID " + entity.getIdSubestructura();
		LOG.info(log1);

		for (ColumnaMetadatosModel columnaMetadatos : input.getColumnas()) {
			IcsDetalleSubestructura detalleEntity = this
					.convertirColumnaMetadatosAIcsDetalleSubestructura(columnaMetadatos);
			detalleEntity.setIcsSubestructura(entity);
			detalleSubestructurasRepository.save(detalleEntity);
		}

		detalleSubestructurasRepository.flush();
	}

	@Override
	public void actualizarSubestructura(EstructuraAnetoModel input) {
		LOG.info("actualizarSubestructura");

		IcsSubestructura entity = null;
		Long idSubestructura = input.getId();
		try {
			entity = subestructurasRepository.findOne(idSubestructura);
		} catch (EntityNotFoundException e) {
			throw new IcsEstructuraNotFoundException("La subestructura a actualizar no existe", e);
		}

		entity.setCodigoSubestructura(input.getCodigo());
		entity.setDescripcionSubestructura(input.getDescripcion());

		BooleanBuilder bb = new BooleanBuilder().and(
				QIcsDetalleSubestructura.icsDetalleSubestructura.icsSubestructura.idSubestructura.eq(idSubestructura));

		detalleSubestructurasRepository.delete(detalleSubestructurasRepository.findAll(bb));

		for (ColumnaMetadatosModel columnaMetadatos : input.getColumnas()) {
			IcsDetalleSubestructura detalleEntity = this
					.convertirColumnaMetadatosAIcsDetalleSubestructura(columnaMetadatos);
			detalleEntity.setIcsSubestructura(entity);
			detalleSubestructurasRepository.save(detalleEntity);
		}

		subestructurasRepository.saveAndFlush(entity);
	}

	private Sort buildSort(String sortColumn, String sortOrder) {

		Sort.Direction dir;
		String entitySortField;

		switch (sortColumn) {
		case "codigo":
			entitySortField = CODIGO_SUBESCTRUCTURA;
			break;
		case "nombre":
			entitySortField = NOMBRE_SUBESCTRUCTURA;
			break;
		default:
			return null;
		}

		if ("desc".equalsIgnoreCase(sortOrder)) {
			dir = Sort.Direction.DESC;
		} else {
			dir = Sort.Direction.ASC;
		}

		return new Sort(dir, entitySortField);
	}

	@Override
	public void activarSubestructura(long idSubEstructura) {

		LOG.info("Activar Subestructura " + idSubEstructura);
		try {
			IcsSubestructura entity = this.subestructurasRepository.getOne(idSubEstructura);
			entity.setVigenciaSubestructura("Y");
			this.subestructurasRepository.saveAndFlush(entity);
		} catch (EntityNotFoundException e) {
			throw new IcsSubestructuraNotFoundException("Subestructura", e);
		}

	}

	@Override
	public void desactivarSubestructura(long idSubEstructura) {
		LOG.info("Desactivar detalle de Subestructura " + idSubEstructura);

		IcsSubestructura entity = null;
		try {
			entity = this.subestructurasRepository.getOne(idSubEstructura);
		} catch (EntityNotFoundException e) {
			throw new IcsSubestructuraNotFoundException("Subestructura", e);
		}

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("PN_IDESTRUCTURA", idSubEstructura);
		map.put("PN_TIPOESTRUCTURA", 0);
		LOG.info("\n\nLlamada a PCK_UTIL_APP.PRC_VALIDAR_ESTRUCTURAS \n" + idSubEstructura + "\n");

		RequestResultOutput result = null;
		SimpleJdbcCall call = new SimpleJdbcCall(jdbcTemplate).withCatalogName("PCK_UTIL_APP")
				.withProcedureName("PRC_VALIDAR_ESTRUCTURAS")
				.declareParameters(new SqlParameter("PN_IDESTRUCTURA", oracle.jdbc.OracleTypes.NUMBER),
						new SqlParameter("PN_TIPOESTRUCTURA", oracle.jdbc.OracleTypes.NUMBER));

		Map<String, Object> execute = call.execute(new MapSqlParameterSource(map));
		result = new RequestResultOutput(execute);

		if (result.getCode() == 1) {
			throw new IcsSubestructuraServiceException(result.getMessage());
		}

		entity.setVigenciaSubestructura("N");
		this.subestructurasRepository.saveAndFlush(entity);
	}

	@Override
	public SubEstructuraOutput getSubEstructura(long idSubestructura) {
		LOG.info("getSubEstructura");

		IcsSubestructura entity = null;
		try {
			entity = this.subestructurasRepository.findOne(idSubestructura);
		} catch (EntityNotFoundException e) {
			throw new IcsSubestructuraNotFoundException("Subestructura no encontrada", e);
		}

		if (entity != null) {

			SubEstructuraOutput transformada = conversionService.convert(entity, SubEstructuraOutput.class);

			BooleanBuilder bb = new BooleanBuilder()
					.and(QIcsDetalleSubestructura.icsDetalleSubestructura.icsSubestructura.idSubestructura
							.eq(idSubestructura));

			OrderSpecifier<BigDecimal> sortOrder = QIcsDetalleSubestructura.icsDetalleSubestructura.orden.asc();
			Iterable<IcsDetalleSubestructura> iteraDetalles = detalleSubestructurasRepository.findAll(bb, sortOrder);

			LOG.debug("getSubEstructura - iteraDetalles");
			List<DetalleSubEstructuraOutput> detalles = new ArrayList<>();
			int i = 0;
			for (IcsDetalleSubestructura icsDetalleSubestr : iteraDetalles) {
				i++;
				final String logDebug = "getSubEstructura - iteraDetalles " + i;
				LOG.debug(logDebug);
				DetalleSubEstructuraOutput detalle = conversionService.convert(icsDetalleSubestr,
						DetalleSubEstructuraOutput.class);
				detalles.add(detalle);
			}
			LOG.debug("getSubEstructura - iteraDetalles OK");

			transformada.setColumnas(detalles);

			return transformada;
		}

		return null;
	}

	@Override
	public OutputListWrapper<SubEstructuraOutput> getSubestructuras() {
		LOG.info("Obteniendo estructuras");

		List<IcsSubestructura> subestructuras = subestructurasRepository.findAll();
		long countSubEstructuras = subestructurasRepository.count();

		LOG.info("Subestructuras obtenidas : " + subestructuras.size());

		TypeDescriptor sourceType = TypeDescriptor.collection(List.class,
				TypeDescriptor.valueOf(IcsSubestructura.class));
		TypeDescriptor targetType = TypeDescriptor.collection(List.class,
				TypeDescriptor.valueOf(SubEstructuraOutput.class));

		@SuppressWarnings("unchecked")
		List<SubEstructuraOutput> lista = (List<SubEstructuraOutput>) conversionService.convert(subestructuras,
				sourceType, targetType);

		return new OutputListWrapper<SubEstructuraOutput>(lista, (int) countSubEstructuras, "");
	}

	@Override
	public OutputListWrapper<SubEstructuraOutput> getSubestructuras(int page, int size, String sortColumn,
			String sortOrder, Map<String, String> filters) {

		Pageable paginador;

		if ("".equals(sortColumn)) {
			paginador = new PageRequest(page, size);
		} else {
			Sort sorter = buildSort(sortColumn, sortOrder);
			paginador = new PageRequest(page, size, sorter);
		}

		List<IcsSubestructura> estructuras;
		long countEstructuras;
		if (filters != null) {
			BooleanBuilder queryDslFilters = generateQueryFilter(filters);

			countEstructuras = subestructurasRepository.count(queryDslFilters);
			Page<IcsSubestructura> pagina = subestructurasRepository.findAll(queryDslFilters, paginador);
			estructuras = pagina.getContent();
		} else {
			countEstructuras = subestructurasRepository.count();
			Page<IcsSubestructura> pagina = subestructurasRepository.findAll(paginador);
			estructuras = pagina.getContent();
		}

		TypeDescriptor sourceType = TypeDescriptor.collection(List.class,
				TypeDescriptor.valueOf(IcsSubestructura.class));
		TypeDescriptor targetType = TypeDescriptor.collection(List.class,
				TypeDescriptor.valueOf(SubEstructuraOutput.class));

		@SuppressWarnings("unchecked")
		List<SubEstructuraOutput> lista = (List<SubEstructuraOutput>) conversionService.convert(estructuras, sourceType,
				targetType);

		return new OutputListWrapper<SubEstructuraOutput>(lista, (int) countEstructuras, "");
	}

	private BooleanBuilder generateQueryFilter(Map<String, String> filters) {
		BooleanBuilder bb = new BooleanBuilder();

		LOG.info("Generando filtros de consulta.");
		QIcsSubestructura queryDslRef = QIcsSubestructura.icsSubestructura;

		if (filters.containsKey("nombre") && !filters.get("nombre").trim().isEmpty()) {
			bb.and(queryDslRef.descripcionSubestructura.upper().eq(filters.get("nombre").toUpperCase()));
			LOG.info("Consultando por nombre.");
		}

		if (filters.containsKey("codigo") && !filters.get("codigo").trim().isEmpty()) {
			bb.and(queryDslRef.codigoSubestructura.upper().eq(filters.get("codigo").toUpperCase()));
			LOG.info("Consultando por codigo.");
		}

		return bb;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SubEstructuraSimpleOutput> getSubestructurasSimple() {
		List<IcsSubestructura> estructuras = subestructurasRepository.findAll();

		LOG.info("Estructuras obtenidas : " + estructuras.size());

		TypeDescriptor sourceType = TypeDescriptor.collection(List.class,
				TypeDescriptor.valueOf(IcsSubestructura.class));
		TypeDescriptor targetType = TypeDescriptor.collection(List.class,
				TypeDescriptor.valueOf(SubEstructuraSimpleOutput.class));

		return (List<SubEstructuraSimpleOutput>) conversionService.convert(estructuras, sourceType, targetType);
	}

	@Override
	public boolean existeSubestructura(SubEstructuraInput subestructura) {
		String codigo = subestructura.getCodigo();
		String descripcion = subestructura.getDescripcion();

		QIcsSubestructura qIcsSub = QIcsSubestructura.icsSubestructura;
		// si se encuentra coincidencia de cualqueir de ambos datos
		BooleanBuilder bb = new BooleanBuilder().andAnyOf(qIcsSub.codigoSubestructura.eq(codigo),
				qIcsSub.descripcionSubestructura.eq(descripcion));
		long count = subestructurasRepository.count(bb);
		return (count > 0);
	}
}
