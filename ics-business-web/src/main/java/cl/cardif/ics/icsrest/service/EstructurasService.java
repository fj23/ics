package cl.cardif.ics.icsrest.service;

import cl.cardif.ics.icsrest.domain.dto.input.EstructuraAnetoModel;
import cl.cardif.ics.icsrest.domain.dto.input.EstructuraInput;
import cl.cardif.ics.icsrest.domain.dto.output.EstructuraOutput;
import cl.cardif.ics.icsrest.domain.dto.output.EstructuraSimpleOutput;
import cl.cardif.ics.icsrest.util.OutputListWrapper;
import cl.cardif.ics.icsrest.util.Permisos;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.List;
import java.util.Map;

public interface EstructurasService {

	@PreAuthorize(value = "hasAnyAuthority("
	            + Permisos.Mant_Estr_Alta + ","
	            + Permisos.Mant_Estr_Canc + ","
	            + Permisos.Mant_Estr_Reca + ","
	            + Permisos.Mant_Estr_Prosp + ","
	            + Permisos.Mant_Estr_Intera + ","
	            + Permisos.Mant_Estr_Pago + ")")
	void activarEstructura(long idEstructura);
	
	@PreAuthorize(value = "hasAnyAuthority("
	            + Permisos.Mant_Estr_Alta + ","
	            + Permisos.Mant_Estr_Canc + ","
	            + Permisos.Mant_Estr_Reca + ","
	            + Permisos.Mant_Estr_Prosp + ","
	            + Permisos.Mant_Estr_Intera + ","
	            + Permisos.Mant_Estr_Pago + ")")
	void actualizarEstructura(EstructuraAnetoModel detalle);
	
	@PreAuthorize(value = "hasAnyAuthority("
	            + Permisos.Mant_Estr_Alta + ","
	            + Permisos.Mant_Estr_Canc + ","
	            + Permisos.Mant_Estr_Reca + ","
	            + Permisos.Mant_Estr_Prosp + ","
	            + Permisos.Mant_Estr_Intera + ","
	            + Permisos.Mant_Estr_Pago + ")")
	void almacenarEstructura(EstructuraAnetoModel detalle);
	
	@PreAuthorize(value = "hasAnyAuthority("
	            + Permisos.Mant_Estr_Alta + ","
	            + Permisos.Mant_Estr_Canc + ","
	            + Permisos.Mant_Estr_Reca + ","
	            + Permisos.Mant_Estr_Prosp + ","
	            + Permisos.Mant_Estr_Intera + ","
	            + Permisos.Mant_Estr_Pago + ")")
	void desactivarEstructura(long idEstructura);
	
	@PreAuthorize(value = "hasAnyAuthority("
	            + Permisos.Cslt_Estr + ","
	            + Permisos.Mant_Estr_Alta + ","
	            + Permisos.Mant_Estr_Canc + ","
	            + Permisos.Mant_Estr_Reca + ","
	            + Permisos.Mant_Estr_Prosp + ","
	            + Permisos.Mant_Estr_Intera + ","
	            + Permisos.Mant_Estr_Pago + ")")
	EstructuraOutput getEstructura(long id);
	
	@PreAuthorize(value = "hasAnyAuthority("
	            + Permisos.Cslt_Estr + ","
	            + Permisos.Mant_Estr_Alta + ","
	            + Permisos.Mant_Estr_Canc + ","
	            + Permisos.Mant_Estr_Reca + ","
	            + Permisos.Mant_Estr_Prosp + ","
	            + Permisos.Mant_Estr_Intera + ","
	            + Permisos.Mant_Estr_Pago + ")")
	OutputListWrapper<EstructuraOutput> getEstructuras(
	    int page, int size, String sortColumn, String sortOrder, Map<String, String> filters);
	
	@PreAuthorize(value = "hasAnyAuthority("
	            + Permisos.Cslt_Estr + ","
	            + Permisos.Mant_Estr_Alta + ","
	            + Permisos.Mant_Estr_Canc + ","
	            + Permisos.Mant_Estr_Reca + ","
	            + Permisos.Mant_Estr_Prosp + ","
	            + Permisos.Mant_Estr_Intera + ","
	            + Permisos.Mant_Estr_Pago + ")")
	OutputListWrapper<EstructuraOutput> getEstructurasAll();
	
	@PreAuthorize(value = "hasAnyAuthority("
	            + Permisos.Cslt_Estr + ","
	            + Permisos.Mant_Estr_Alta + ","
	            + Permisos.Mant_Estr_Canc + ","
	            + Permisos.Mant_Estr_Reca + ","
	            + Permisos.Mant_Estr_Prosp + ","
	            + Permisos.Mant_Estr_Intera + ","
	            + Permisos.Mant_Estr_Pago + ")")
	List<EstructuraSimpleOutput> getEstructurasSimples();
	
	@PreAuthorize(value = "hasAnyAuthority("
	            + Permisos.Cslt_Estr + ","
	            + Permisos.Mant_Estr_Alta + ","
	            + Permisos.Mant_Estr_Canc + ","
	            + Permisos.Mant_Estr_Reca + ","
	            + Permisos.Mant_Estr_Prosp + ","
	            + Permisos.Mant_Estr_Intera + ","
	            + Permisos.Mant_Estr_Pago + ")")
	boolean existeEstructura(EstructuraInput estructura);
}
