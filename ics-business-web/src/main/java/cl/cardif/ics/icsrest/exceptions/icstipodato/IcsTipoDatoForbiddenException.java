package cl.cardif.ics.icsrest.exceptions.icstipodato;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class IcsTipoDatoForbiddenException extends RuntimeException {

  private static final long serialVersionUID = 3397992475249736227L;

  public IcsTipoDatoForbiddenException(String exception) {
    super(exception);
  }

  public IcsTipoDatoForbiddenException(String exception, Throwable t) {
    super(exception, t);
  }
}
