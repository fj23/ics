package cl.cardif.ics.icsrest.domain.dto.output;

public class AtributoSalidaEstrucOutput {
	private Long idAtributoEst;
	private String nombreColumna;
	
	public AtributoSalidaEstrucOutput() {
		super();
	}

	public Long getIdAtributoEst() {
		return idAtributoEst;
	}

	public void setIdAtributoEst(Long idAtributoEst) {
		this.idAtributoEst = idAtributoEst;
	}

	public String getNombreColumna() {
		return nombreColumna;
	}

	public void setNombreColumna(String nombreColumna) {
		this.nombreColumna = nombreColumna;
	}
}
