package cl.cardif.ics.icsrest.service;

import cl.cardif.ics.icsrest.message.response.AuthLoginResponse;

@FunctionalInterface
public interface AuthService {
  String signin(AuthLoginResponse authLoginResponse);
}
