package cl.cardif.ics.icsrest.domain.dto.input;

public abstract class ColumnaInput {
  protected Long id;
  protected Long orden;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getOrden() {
    return orden;
  }

  public void setOrden(Long orden) {
    this.orden = orden;
  }
}
