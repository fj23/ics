package cl.cardif.ics.icsrest.util.converters.topojo;

import cl.cardif.ics.domain.entity.common.IcsTipoExtension;
import cl.cardif.ics.icsrest.domain.pojo.TipoExtension;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class TipoExtensionConverter implements Converter<IcsTipoExtension, TipoExtension> {

  @Override
  public TipoExtension convert(IcsTipoExtension entity) {
    TipoExtension pojo = new TipoExtension();
    pojo.setId(entity.getIdExtension());
    pojo.setNombre(entity.getNombreExtension());
    pojo.setFormato(entity.getFormatoExtension());
    pojo.setVigencia(entity.getVigencia());
    return pojo;
  }
}
