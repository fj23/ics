package cl.cardif.ics.icsrest.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import cl.cardif.ics.icsrest.domain.dto.input.PlantillaEntradaInput;
import cl.cardif.ics.icsrest.domain.dto.output.PlantillaOutput;
import cl.cardif.ics.icsrest.domain.dto.output.RequestResultOutput;
import cl.cardif.ics.icsrest.domain.pojo.TipoDato;
import cl.cardif.ics.icsrest.exceptions.icsplantilla.IcsPlantillaServiceException;
import cl.cardif.ics.icsrest.service.PlantillaEntradaService;

@RestController
@RequestMapping("/api/plantillas/entrada")
public class PlantillasEntradaController {
	private static final Logger LOG = LoggerFactory.getLogger(PlantillasEntradaController.class);

	@Autowired private PlantillaEntradaService plantillaService;

	@PostMapping("/nueva")
	public ResponseEntity<RequestResultOutput> almacenarPlantillaEntrada(@RequestBody PlantillaEntradaInput input) {
		LOG.info("almacenarPlantillaEntrada");
		try {
			RequestResultOutput result = plantillaService.addOrUpdatePlantillaEntrada(input);
			return new ResponseEntity<>(result, HttpStatus.CREATED);
		} catch (IcsPlantillaServiceException e) {
			LOG.error("almacenarPlantillaEntrada - La plantilla no pudo ser almacenada", e);
			return ResponseEntity.badRequest().build();
		}
	}

	@PostMapping("/actualizar")
	public ResponseEntity<RequestResultOutput> actualizarPlantillaEntrada(@RequestBody PlantillaEntradaInput input) {
		LOG.info("actualizarPlantillaEntrada");
		try {
			RequestResultOutput result = plantillaService.addOrUpdatePlantillaEntrada(input);
			return new ResponseEntity<>(result, HttpStatus.CREATED);
		} catch (IcsPlantillaServiceException e) {
			LOG.error("actualizarPlantillaEntrada - La plantilla no pudo ser actualizada", e);
			return ResponseEntity.badRequest().build();
		}
	}

	@GetMapping("/vigentes")
	@ResponseStatus(HttpStatus.OK)
	public Collection<PlantillaOutput> getVigentes(@RequestParam Map<String, String> requestParams) {
		LOG.info("getVigentes");

		if (requestParams.containsKey("socio") && requestParams.containsKey("evento")) {
			Long idSocio = Long.valueOf(requestParams.get("socio"));
			Long idEvento = Long.valueOf(requestParams.get("evento"));
			return plantillaService.getVigentes(idSocio, idEvento);
		} else {
			return new ArrayList<>();
		}
	}

	@GetMapping("/tipos_datos")
	@ResponseStatus(HttpStatus.OK)
	public Collection<TipoDato> getTiposDatos() {
		LOG.info("getTiposDatos");
		return plantillaService.getColumnDataTypes();
	}
}
