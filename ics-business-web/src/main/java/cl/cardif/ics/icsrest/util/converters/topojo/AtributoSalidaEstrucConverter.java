package cl.cardif.ics.icsrest.util.converters.topojo;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import cl.cardif.ics.domain.entity.plantillas.IcsAtributoSalidaEstruc;
import cl.cardif.ics.icsrest.domain.dto.output.AtributoSalidaEstrucOutput;

@Component
public class AtributoSalidaEstrucConverter implements Converter<IcsAtributoSalidaEstruc, AtributoSalidaEstrucOutput> {

	@Override
	public AtributoSalidaEstrucOutput convert(IcsAtributoSalidaEstruc entity) {
		AtributoSalidaEstrucOutput pojo = new AtributoSalidaEstrucOutput();

		pojo.setIdAtributoEst(entity.getIdAtributoEst());
		pojo.setNombreColumna(entity.getNombreColumna());

		return pojo;
	}
}
