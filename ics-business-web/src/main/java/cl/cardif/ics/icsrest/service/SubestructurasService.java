package cl.cardif.ics.icsrest.service;

import java.util.List;
import java.util.Map;

import org.springframework.security.access.prepost.PreAuthorize;

import cl.cardif.ics.icsrest.domain.dto.input.EstructuraAnetoModel;
import cl.cardif.ics.icsrest.domain.dto.input.SubEstructuraInput;
import cl.cardif.ics.icsrest.domain.dto.output.SubEstructuraOutput;
import cl.cardif.ics.icsrest.domain.dto.output.SubEstructuraSimpleOutput;
import cl.cardif.ics.icsrest.util.OutputListWrapper;
import cl.cardif.ics.icsrest.util.Permisos;

public interface SubestructurasService {

	@PreAuthorize(value = "hasAnyAuthority(" 
			+ Permisos.Mant_Estr_Alta + "," 
			+ Permisos.Mant_Estr_Canc + ","
			+ Permisos.Mant_Estr_Reca + ","
			+ Permisos.Mant_Estr_Prosp + ","
			+ Permisos.Mant_Estr_Intera + ","
			+ Permisos.Mant_Estr_Pago + ")")
	void activarSubestructura(long idSubEstructura);

	@PreAuthorize(value = "hasAnyAuthority(" 
			+ Permisos.Mant_Estr_Alta + "," 
			+ Permisos.Mant_Estr_Canc + ","
			+ Permisos.Mant_Estr_Reca + ","
			+ Permisos.Mant_Estr_Prosp + ","
			+ Permisos.Mant_Estr_Intera + ","
			+ Permisos.Mant_Estr_Pago + ")")
	void actualizarSubestructura(EstructuraAnetoModel detalle);

	@PreAuthorize(value = "hasAnyAuthority(" 
			+ Permisos.Mant_Estr_Alta + "," 
			+ Permisos.Mant_Estr_Canc + ","
			+ Permisos.Mant_Estr_Reca + ","
			+ Permisos.Mant_Estr_Prosp + ","
			+ Permisos.Mant_Estr_Intera + ","
			+ Permisos.Mant_Estr_Pago + ")")
	void almacenarSubestructura(EstructuraAnetoModel input);

	@PreAuthorize(value = "hasAnyAuthority(" 
			+ Permisos.Mant_Estr_Alta + "," 
			+ Permisos.Mant_Estr_Canc + ","
			+ Permisos.Mant_Estr_Reca + ","
			+ Permisos.Mant_Estr_Prosp + ","
			+ Permisos.Mant_Estr_Intera + ","
			+ Permisos.Mant_Estr_Pago + ")")
	void desactivarSubestructura(long idSubEstructura);

	@PreAuthorize(value = "hasAnyAuthority(" 
			+ Permisos.Cslt_Estr + "," 
			+ Permisos.Mant_Estr_Alta + ","
			+ Permisos.Mant_Estr_Canc + "," 
			+ Permisos.Mant_Estr_Reca + ","
			+ Permisos.Mant_Estr_Prosp + ","
			+ Permisos.Mant_Estr_Intera + ","
			+ Permisos.Mant_Estr_Pago + ")")
	SubEstructuraOutput getSubEstructura(long id);

	@PreAuthorize(value = "hasAnyAuthority(" 
			+ Permisos.Cslt_Estr + "," 
			+ Permisos.Mant_Estr_Alta + ","
			+ Permisos.Mant_Estr_Canc + "," 
			+ Permisos.Mant_Estr_Reca + ","
			+ Permisos.Mant_Estr_Prosp + ","
			+ Permisos.Mant_Estr_Intera + ","
			+ Permisos.Mant_Estr_Pago + ")")
	OutputListWrapper<SubEstructuraOutput> getSubestructuras();

	@PreAuthorize(value = "hasAnyAuthority(" 
			+ Permisos.Cslt_Estr + "," 
			+ Permisos.Mant_Estr_Alta + ","
			+ Permisos.Mant_Estr_Canc + "," 
			+ Permisos.Mant_Estr_Reca + ","
			+ Permisos.Mant_Estr_Prosp + ","
			+ Permisos.Mant_Estr_Intera + ","
			+ Permisos.Mant_Estr_Pago + ")")
	OutputListWrapper<SubEstructuraOutput> getSubestructuras(int page, int size, String sortColumn, String sortOrder,
			Map<String, String> filters);

  @PreAuthorize(value = "hasAnyAuthority("
            + Permisos.Cslt_Estr + ","
            + Permisos.Mant_Estr_Alta + ","
            + Permisos.Mant_Estr_Canc + ","
            + Permisos.Mant_Estr_Reca + ","
  			+ Permisos.Mant_Estr_Prosp + ","
			+ Permisos.Mant_Estr_Intera + ","
			+ Permisos.Mant_Estr_Pago + ")")
  List<SubEstructuraSimpleOutput> getSubestructurasSimple();
  

  @PreAuthorize(value = "hasAnyAuthority("
            + Permisos.Cslt_Estr + ","
            + Permisos.Mant_Estr_Alta + ","
            + Permisos.Mant_Estr_Canc + ","
            + Permisos.Mant_Estr_Reca + ","
  			+ Permisos.Mant_Estr_Prosp + ","
			+ Permisos.Mant_Estr_Intera + ","
			+ Permisos.Mant_Estr_Pago + ")")
  boolean existeSubestructura(SubEstructuraInput subestructura);
}
