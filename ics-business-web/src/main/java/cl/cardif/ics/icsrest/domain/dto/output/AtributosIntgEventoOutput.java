package cl.cardif.ics.icsrest.domain.dto.output;

public class AtributosIntgEventoOutput extends AtributosIntg {

	private long idIntegracionEvento;
	private long idEvento;
	private String nombreColumnaIntegracion;

	public AtributosIntgEventoOutput() {
		super();
	}

	public long getIdEvento() {
		return this.idEvento;
	}

	public void setIdEvento(long idEvento) {
		this.idEvento = idEvento;
	}

	public long getIdIntegracionEvento() {
		return this.idIntegracionEvento;
	}

	public void setIdIntegracionEvento(long idIntegracionEvento) {
		this.idIntegracionEvento = idIntegracionEvento;
	}

	public String getNombreColumnaIntegracion() {
		return this.nombreColumnaIntegracion;
	}

	public void setNombreColumnaIntegracion(String nombreColumnaIntegracion) {
		this.nombreColumnaIntegracion = nombreColumnaIntegracion;
	}
}
