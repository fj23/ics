package cl.cardif.ics.icsrest.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.OrderSpecifier;

import cl.cardif.ics.domain.entity.common.IcsTipoPersona;
import cl.cardif.ics.domain.entity.plantillas.IcsAtributoEnriquecimiento;
import cl.cardif.ics.domain.entity.plantillas.IcsAtributoNegocio;
import cl.cardif.ics.domain.entity.plantillas.IcsAtributoUserIntg;
import cl.cardif.ics.domain.entity.plantillas.IcsAtributoUsuario;
import cl.cardif.ics.domain.entity.plantillas.IcsAtributosIntg;
import cl.cardif.ics.domain.entity.plantillas.QIcsAtributoEnriquecimiento;
import cl.cardif.ics.domain.entity.plantillas.QIcsAtributoUserIntg;
import cl.cardif.ics.domain.entity.plantillas.QIcsAtributoUsuario;
import cl.cardif.ics.icsrest.domain.dto.output.AtributoEnriquecimientoOutput;
import cl.cardif.ics.icsrest.domain.dto.output.AtributoNegocioOutput;
import cl.cardif.ics.icsrest.domain.dto.output.AtributoUserIntgOutput;
import cl.cardif.ics.icsrest.domain.dto.output.AtributoUsuarioOutput;
import cl.cardif.ics.icsrest.domain.dto.output.integracion.AtributosIntgOutput;
import cl.cardif.ics.icsrest.repository.AtributoUserIntegracionRepository;
import cl.cardif.ics.icsrest.repository.AtributosEnriquecimientoRepository;
import cl.cardif.ics.icsrest.repository.AtributosIntegracionRepository;
import cl.cardif.ics.icsrest.repository.AtributosNegocioRepository;
import cl.cardif.ics.icsrest.repository.AtributosUsuarioRepository;
import cl.cardif.ics.icsrest.repository.TipoPersonaRepository;
import cl.cardif.ics.icsrest.service.AtributosService;

@Service
public class AtributosServiceImpl implements AtributosService {
	private static final Logger LOG = LoggerFactory.getLogger(AtributosServiceImpl.class);
	
	@Autowired private ConversionService conversionService;

	@Autowired private AtributosEnriquecimientoRepository enriquecimientoRepository;
	@Autowired private AtributosNegocioRepository negocioRepository;
	@Autowired private AtributosIntegracionRepository integracionRepository;
	@Autowired private AtributoUserIntegracionRepository userRepository;
	@Autowired private AtributosUsuarioRepository usuarioRepository;
	@Autowired private TipoPersonaRepository tipoPersonaRepository;

	@Override 
	public List<AtributoEnriquecimientoOutput> obtenerAtributosEnriquecimientoPorCore(Long idCoreL) {
		LOG.info("getAllAtributosEnriquecimiento");
		
		Iterable<IcsAtributoEnriquecimiento> iteraAtributos;
		
		if (idCoreL != null && idCoreL != 0) {
			BigDecimal idCore = BigDecimal.valueOf(idCoreL);
			BooleanBuilder bb = new BooleanBuilder()
					.and(QIcsAtributoEnriquecimiento.icsAtributoEnriquecimiento.idCore.eq(idCore));
			iteraAtributos = this.enriquecimientoRepository.findAll(bb);
		} else {
			iteraAtributos = this.enriquecimientoRepository.findAll();
		}

		List<AtributoEnriquecimientoOutput> atributos = new ArrayList<>();
		for (IcsAtributoEnriquecimiento icsAtributoEnriquecimiento : iteraAtributos) {
			AtributoEnriquecimientoOutput atributoEnriquecimiento = conversionService.convert(icsAtributoEnriquecimiento, AtributoEnriquecimientoOutput.class);
			atributos.add(atributoEnriquecimiento);
		}

		return atributos;
	}

	@Override
	public List<AtributosIntgOutput> obtenerAtributosIntegracion() {
		LOG.info("getAllAtributosIntegracion");
		
		Iterable<IcsAtributosIntg> iteraAtributos = integracionRepository.findAll();

		List<AtributosIntgOutput> atributos = new ArrayList<>();
		for (IcsAtributosIntg icsAtributoIntegracion : iteraAtributos) {
			AtributosIntgOutput atributoIntegracion = conversionService.convert(icsAtributoIntegracion, AtributosIntgOutput.class);
			atributos.add(atributoIntegracion);
		}

		return atributos;
	}

	@Override
	public List<AtributoUserIntgOutput> obtenerAtributosDestinoIntegracionPorEvento(Long idEvent) {
		LOG.info("getAtributosDestinoIntegracionPorEvento");

		BooleanBuilder bb = new BooleanBuilder()
					.and(QIcsAtributoUserIntg.icsAtributoUserIntg.icsAtributoIntegracionEvento.icsEvento.idEvento.eq(idEvent));

		OrderSpecifier<String> orden = QIcsAtributoUserIntg.icsAtributoUserIntg.nombreColumna.asc();

		Iterable<IcsAtributoUserIntg> iteraAtributos = this.userRepository.findAll(bb, orden);

		List<AtributoUserIntgOutput> atributos = new ArrayList<>();
		for (IcsAtributoUserIntg icsAtributoIntegracion : iteraAtributos) {
			AtributoUserIntgOutput atributoIntegracion = conversionService.convert(icsAtributoIntegracion, AtributoUserIntgOutput.class);
			atributos.add(atributoIntegracion);
		}
		
		return atributos;
	}

	@Override
	public List<AtributoNegocioOutput> obtenerAtributosNegocio() {
		LOG.info("obtenerAtributosNegocio");
		
		Iterable<IcsAtributoNegocio> iteraAtributos = negocioRepository.findAll();

		List<AtributoNegocioOutput> atributos = new ArrayList<>();
		for (IcsAtributoNegocio icsAtributoIntegracion : iteraAtributos) {
			AtributoNegocioOutput atributoIntegracion = conversionService.convert(icsAtributoIntegracion, AtributoNegocioOutput.class);
			atributos.add(atributoIntegracion);
		}
		
		return atributos;
	}

	@Override
	public IcsAtributoEnriquecimiento obtenerAtributoEnriquecimientoPorId(Long idAtributoEnriquecimiento) {
		LOG.info("obtenerAtributoEnriquecimientoPorId");

		try {
			return this.enriquecimientoRepository.getOne(idAtributoEnriquecimiento);
		} catch (Exception e) {
			LOG.error("No se pudo obtener el atributo", e);
			return null;
		}
	}

	@Override
	public IcsAtributoUsuario obtenerAtributoUsuarioPorId(Long idAtributoUsuario) {
		LOG.info("obtenerAtributoUsuarioPorId");

		try {
			return usuarioRepository.findOne(idAtributoUsuario);
		} catch (Exception e) {
			LOG.error("No se pudo obtener el atributo", e);
			return null;
		}
	}

	private Map<String, Long> mapaTipoPersonaCardinalidad() {
		Iterable<IcsTipoPersona> iteraTiposPersona = tipoPersonaRepository.findAll();
		Map<String, Long> tipoPersonaCardinalidadMap = new HashMap<>();
		for (IcsTipoPersona icsTipoPersona : iteraTiposPersona) {
			tipoPersonaCardinalidadMap.put(icsTipoPersona.getTipoPersona(), icsTipoPersona.getCardinalidadMaxima());
		}
		return tipoPersonaCardinalidadMap;
	}

	@Override
	public List<AtributoUsuarioOutput> obtenerAtributosUsuarioPorEvento(Long idEvento) {
		LOG.info("obtenerAtributosUsuarioPorEvento");
		
		Map<String, Long> tipoPersonaCardinalidadMap = this.mapaTipoPersonaCardinalidad();
		LOG.debug("obtenerAtributosUsuarioPorEvento/tipoPersonaCardinalidadMap={}",tipoPersonaCardinalidadMap);
		
		BooleanBuilder atributosUsuarioParaEvento = new BooleanBuilder()
					.and(QIcsAtributoUsuario.icsAtributoUsuario.icsAtributoNegocio.idEvento.eq(idEvento));
		
		OrderSpecifier<String> ordenarPorNombreAscendente = QIcsAtributoUsuario.icsAtributoUsuario.nombreColumna.asc();

		Iterable<IcsAtributoUsuario> iteraAtributos = usuarioRepository.findAll(atributosUsuarioParaEvento, ordenarPorNombreAscendente);

		List<AtributoUsuarioOutput> atributos = new ArrayList<>();
		for (IcsAtributoUsuario icsAtributoUsuario : iteraAtributos) {
			AtributoUsuarioOutput atributoUsuario = conversionService.convert(icsAtributoUsuario, AtributoUsuarioOutput.class);
			Long cardinalidad = tipoPersonaCardinalidadMap.get(icsAtributoUsuario.getTipoPersona());
			atributoUsuario.setCardinalidadMaxima(cardinalidad);
			atributos.add(atributoUsuario);
		}

		return atributos;
	}
}
