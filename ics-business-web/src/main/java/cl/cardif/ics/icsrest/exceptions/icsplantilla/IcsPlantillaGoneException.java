package cl.cardif.ics.icsrest.exceptions.icsplantilla;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.GONE)
public class IcsPlantillaGoneException extends RuntimeException {

  private static final long serialVersionUID = -7209750264039464453L;

  public IcsPlantillaGoneException(String exception) {
    super(exception);
  }

  public IcsPlantillaGoneException(String exception, Throwable t) {
    super(exception);
  }
}
