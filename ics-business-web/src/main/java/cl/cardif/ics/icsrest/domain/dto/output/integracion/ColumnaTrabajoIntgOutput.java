package cl.cardif.ics.icsrest.domain.dto.output.integracion;

public class ColumnaTrabajoIntgOutput {

  private Long id;
  private Long idPlantilla;
  private String nombre;
  private Long orden;
  private String formulaSql;
  private String formulaJson;
  private Long columnaOrigen;
  private Long columnaEnriquecimiento;
  private String esObligatorio;
  private String formato;
  private String formula;
  private String homologacion;
  private AtributosIntgOutput atributoIntegracion;
  private Long ordenColumnaArchivo;

  public ColumnaTrabajoIntgOutput() {
    super();
  }

  public AtributosIntgOutput getAtributoIntegracion() {
    return atributoIntegracion;
  }

  public void setAtributoIntegracion(AtributosIntgOutput atributoIntegracion) {
    this.atributoIntegracion = atributoIntegracion;
  }

  public Long getColumnaEnriquecimiento() {
    return columnaEnriquecimiento;
  }

  public void setColumnaEnriquecimiento(Long columnaEnriquecimiento) {
    this.columnaEnriquecimiento = columnaEnriquecimiento;
  }

  public Long getColumnaOrigen() {
    return columnaOrigen;
  }

  public void setColumnaOrigen(Long columnaOrigen) {
    this.columnaOrigen = columnaOrigen;
  }

  public String getEsObligatorio() {
    return esObligatorio;
  }

  public void setEsObligatorio(String esObligatorio) {
    this.esObligatorio = esObligatorio;
  }

  public String getFormato() {
    return formato;
  }

  public void setFormato(String formato) {
    this.formato = formato;
  }

  public String getFormula() {
    return formula;
  }

  public void setFormula(String formula) {
    this.formula = formula;
  }

  public String getFormulaJson() {
    return formulaJson;
  }

  public void setFormulaJson(String formulaJson) {
    this.formulaJson = formulaJson;
  }

  public String getFormulaSql() {
    return formulaSql;
  }

  public void setFormulaSql(String formulaSql) {
    this.formulaSql = formulaSql;
  }

  public String getHomologacion() {
    return homologacion;
  }

  public void setHomologacion(String homologacion) {
    this.homologacion = homologacion;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getIdPlantilla() {
    return idPlantilla;
  }

  public void setIdPlantilla(Long idPlantilla) {
    this.idPlantilla = idPlantilla;
  }

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public Long getOrden() {
    return orden;
  }

  public void setOrden(Long orden) {
    this.orden = orden;
  }

  public Long getOrdenColumnaArchivo() {
    return ordenColumnaArchivo;
  }

  public void setOrdenColumnaArchivo(Long ordenColumnaArchivo) {
    this.ordenColumnaArchivo = ordenColumnaArchivo;
  }

@Override
public String toString() {
	return "AtributosPlantillaIntgOutput [id=" + id + ", idPlantilla=" + idPlantilla + ", nombre=" + nombre + ", orden="
			+ orden + ", formulaSql=" + formulaSql + ", formulaJson=" + formulaJson + ", columnaOrigen=" + columnaOrigen
			+ ", columnaEnriquecimiento=" + columnaEnriquecimiento + ", esObligatorio=" + esObligatorio + ", formato="
			+ formato + ", formula=" + formula + ", homologacion=" + homologacion + ", atributoIntegracion="
			+ atributoIntegracion + ", ordenColumnaArchivo=" + ordenColumnaArchivo + "]";
}
  
  
}
