package cl.cardif.ics.icsrest.domain.dto.input;

public class BaseDetalleColumnaModel extends BaseColumnaModel {

  protected Long largo;
  protected Long orden;
  protected String vigencia;

  public Long getLargo() {
    return largo;
  }

  public void setLargo(Long largo) {
    this.largo = largo;
  }

  public Long getOrden() {
    return orden;
  }

  public void setOrden(Long orden) {
    this.orden = orden;
  }

  public String getVigencia() {
    return vigencia;
  }

  public void setVigencia(String vigencia) {
    this.vigencia = vigencia;
  }
}
