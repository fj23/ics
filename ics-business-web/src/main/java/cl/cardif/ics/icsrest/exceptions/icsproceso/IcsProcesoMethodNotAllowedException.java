package cl.cardif.ics.icsrest.exceptions.icsproceso;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
public class IcsProcesoMethodNotAllowedException extends RuntimeException {

  private static final long serialVersionUID = -6814524510236406273L;

  public IcsProcesoMethodNotAllowedException(String exception) {
    super(exception);
  }

  public IcsProcesoMethodNotAllowedException(String exception, Throwable t) {
    super(exception, t);
  }
}
