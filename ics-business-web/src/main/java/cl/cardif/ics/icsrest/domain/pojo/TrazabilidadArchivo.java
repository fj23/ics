package cl.cardif.ics.icsrest.domain.pojo;

import java.io.Serializable;
import java.util.Date;

public class TrazabilidadArchivo implements Serializable {

	private static final long serialVersionUID = 7225800692879596155L;

	private Long id;
	private Long idProceso;
	private String idExterno;
	private String nombreSocio;
	private String nombreEvento;
	private String nombreArchivo;
	private String estadoTransaccion;
	private Integer lineasOriginal;
	private Date fechaInicio;
	private Date fechaFin;
	private Integer lineasIcsOk;
	private Integer lineasCoreOk;
	private Integer lineasIcsError;
	private Integer lineasCoreError;
	private boolean flgConsolidar;
	private String tipoTransaccion;
	private boolean flgSalida;

	public TrazabilidadArchivo() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdProceso() {
		return idProceso;
	}

	public void setIdProceso(Long idProceso) {
		this.idProceso = idProceso;
	}

	public String getIdExterno() {
		return idExterno;
	}

	public void setIdExterno(String idExterno) {
		this.idExterno = idExterno;
	}

	public String getNombreSocio() {
		return nombreSocio;
	}

	public void setNombreSocio(String nombreSocio) {
		this.nombreSocio = nombreSocio;
	}

	public String getNombreEvento() {
		return nombreEvento;
	}

	public void setNombreEvento(String nombreEvento) {
		this.nombreEvento = nombreEvento;
	}

	public String getNombreArchivo() {
		return nombreArchivo;
	}

	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	public String getEstadoTransaccion() {
		return estadoTransaccion;
	}

	public void setEstadoTransaccion(String estadoTransaccion) {
		this.estadoTransaccion = estadoTransaccion;
	}

	public Integer getLineasOriginal() {
		return lineasOriginal;
	}

	public void setLineasOriginal(Integer lineasOriginal) {
		this.lineasOriginal = lineasOriginal;
	}

	public Date getFechaInicio() {
		if (this.fechaInicio != null) {
			return (Date)this.fechaInicio.clone();
		} else {
			return null;
		}
	}

	public void setFechaInicio(Date fechaInicio) {
		if (fechaInicio != null) {
			this.fechaInicio = (Date)fechaInicio.clone();
		}
	}

	public Date getFechaFin() {
		if (this.fechaFin != null) {
			return (Date)this.fechaFin.clone();
		} else {
			return null;
		}
	}

	public void setFechaFin(Date fechaFin) {
		if (fechaFin != null) {
			this.fechaFin = (Date)fechaFin.clone();
		}
	}

	public Integer getLineasIcsOk() {
		return lineasIcsOk;
	}

	public void setLineasIcsOk(Integer lineasIcsOk) {
		this.lineasIcsOk = lineasIcsOk;
	}

	public Integer getLineasCoreOk() {
		return lineasCoreOk;
	}

	public void setLineasCoreOk(Integer lineasCoreOk) {
		this.lineasCoreOk = lineasCoreOk;
	}

	public Integer getLineasIcsError() {
		return lineasIcsError;
	}

	public void setLineasIcsError(Integer lineasIcsError) {
		this.lineasIcsError = lineasIcsError;
	}

	public Integer getLineasCoreError() {
		return lineasCoreError;
	}

	public void setLineasCoreError(Integer lineasCoreError) {
		this.lineasCoreError = lineasCoreError;
	}

	public boolean isFlgConsolidar() {
		return flgConsolidar;
	}

	public void setFlgConsolidar(boolean flgConsolidar) {
		this.flgConsolidar = flgConsolidar;
	}

	public String getTipoTransaccion() {
		return tipoTransaccion;
	}

	public void setTipoTransaccion(String tipoTransaccion) {
		this.tipoTransaccion = tipoTransaccion;
	}

	public boolean getFlgSalida() {
		return flgSalida;
	}

	public void setFlgSalida(boolean flgSalida) {
		this.flgSalida = flgSalida;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((estadoTransaccion == null) ? 0 : estadoTransaccion.hashCode());
		result = prime * result + ((fechaFin == null) ? 0 : fechaFin.hashCode());
		result = prime * result + ((fechaInicio == null) ? 0 : fechaInicio.hashCode());
		result = prime * result + (flgConsolidar ? 1231 : 1237);
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((idExterno == null) ? 0 : idExterno.hashCode());
		result = prime * result + ((idProceso == null) ? 0 : idProceso.hashCode());
		result = prime * result + ((lineasCoreError == null) ? 0 : lineasCoreError.hashCode());
		result = prime * result + ((lineasCoreOk == null) ? 0 : lineasCoreOk.hashCode());
		result = prime * result + ((lineasIcsError == null) ? 0 : lineasIcsError.hashCode());
		result = prime * result + ((lineasIcsOk == null) ? 0 : lineasIcsOk.hashCode());
		result = prime * result + ((lineasOriginal == null) ? 0 : lineasOriginal.hashCode());
		result = prime * result + ((nombreArchivo == null) ? 0 : nombreArchivo.hashCode());
		result = prime * result + ((nombreEvento == null) ? 0 : nombreEvento.hashCode());
		result = prime * result + ((nombreSocio == null) ? 0 : nombreSocio.hashCode());
		result = prime * result + ((tipoTransaccion == null) ? 0 : tipoTransaccion.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrazabilidadArchivo other = (TrazabilidadArchivo) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrazabilidadArchivo [id=" + id + ", idProceso=" + idProceso + ", idExterno=" + idExterno
				+ ", nombreSocio=" + nombreSocio + ", nombreEvento=" + nombreEvento + ", nombreArchivo=" + nombreArchivo
				+ ", estadoTransaccion=" + estadoTransaccion + ", lineasOriginal=" + lineasOriginal + ", fechaInicio="
				+ fechaInicio + ", fechaFin=" + fechaFin + ", lineasIcsOk=" + lineasIcsOk + ", lineasCoreOk="
				+ lineasCoreOk + ", lineasIcsError=" + lineasIcsError + ", lineasCoreError=" + lineasCoreError
				+ ", flgConsolidar=" + flgConsolidar + ", tipoTransaccion=" + tipoTransaccion + "]";
	}

}
