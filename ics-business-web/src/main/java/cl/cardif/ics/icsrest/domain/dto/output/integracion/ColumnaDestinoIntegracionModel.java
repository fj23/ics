package cl.cardif.ics.icsrest.domain.dto.output.integracion;

import cl.cardif.ics.icsrest.domain.dto.input.ColumnaDestinoIntegracion;

public class ColumnaDestinoIntegracionModel extends ColumnaDestinoIntegracion {
  private String flgVisible;
  private String traduccion;

  public ColumnaDestinoIntegracionModel() {
    super();
  }

  public String getFlgVisible() {
    return flgVisible;
  }

  public void setFlgVisible(String flgVisible) {
    this.flgVisible = flgVisible;
  }

  public String getTraduccion() {
    return traduccion;
  }

  public void setTraduccion(String traduccion) {
    this.traduccion = traduccion;
  }
}
