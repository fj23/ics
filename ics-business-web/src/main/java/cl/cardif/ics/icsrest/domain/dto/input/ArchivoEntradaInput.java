package cl.cardif.ics.icsrest.domain.dto.input;

import java.util.ArrayList;
import java.util.List;

public class ArchivoEntradaInput extends BaseModel {

	private boolean noRealizarTransformaciones;
	private String delimitadorColumnas;
	private int numeroHoja;
	private int saltarRegistrosIniciales;
	private int saltarRegistrosFinales;
	private List<ColumnaMetadatosInput> columnas;
	private List<Integer> estructuras;

	public ArchivoEntradaInput() {
		super();
	}

	public List<ColumnaMetadatosInput> getColumnas() {
		if (columnas != null) {
			return new ArrayList<>(columnas);
		}
		else {
			return new ArrayList<>();
		}
	}

	public void setColumnas(List<ColumnaMetadatosInput> columnas) {
		if (columnas != null) {
			this.columnas = new ArrayList<>(columnas);
		}
	}

	public String getDelimitadorColumnas() {
		return delimitadorColumnas;
	}

	public void setDelimitadorColumnas(String delimitadorColumnas) {
		this.delimitadorColumnas = delimitadorColumnas;
	}

	public List<Integer> getEstructuras() {
		if (estructuras != null) {
			return new ArrayList<>(estructuras);
		}
		else {
			return new ArrayList<>();
		}
	}

	public void setEstructuras(List<Integer> estructuras) {
		if (estructuras != null) {
			this.estructuras = new ArrayList<>(estructuras);
		}
	}

	public int getNumeroHoja() {
		return numeroHoja;
	}

	public void setNumeroHoja(int numeroHoja) {
		this.numeroHoja = numeroHoja;
	}

	public int getSaltarRegistrosFinales() {
		return saltarRegistrosFinales;
	}

	public void setSaltarRegistrosFinales(int saltarRegistrosFinales) {
		this.saltarRegistrosFinales = saltarRegistrosFinales;
	}

	public int getSaltarRegistrosIniciales() {
		return saltarRegistrosIniciales;
	}

	public void setSaltarRegistrosIniciales(int saltarRegistrosIniciales) {
		this.saltarRegistrosIniciales = saltarRegistrosIniciales;
	}

	public boolean isNoRealizarTransformaciones() {
		return noRealizarTransformaciones;
	}

	public void setNoRealizarTransformaciones(boolean noRealizarTransformaciones) {
		this.noRealizarTransformaciones = noRealizarTransformaciones;
	}
}
