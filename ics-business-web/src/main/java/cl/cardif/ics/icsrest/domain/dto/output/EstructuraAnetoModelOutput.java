package cl.cardif.ics.icsrest.domain.dto.output;

import cl.cardif.ics.icsrest.domain.dto.input.EstructuraAnetoSimpleInput;

public class EstructuraAnetoModelOutput extends EstructuraAnetoSimpleInput {

  private String vigencia;

  public EstructuraAnetoModelOutput() {
    super();
  }

  public EstructuraAnetoModelOutput(long id) {
    super();
    this.id = id;
  }

  public String getVigencia() {
    return vigencia;
  }

  public void setVigencia(String vigencia) {
    this.vigencia = vigencia;
  }
}
