package cl.cardif.ics.icsrest.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
public class ConfigProperties {

	@Autowired private Environment env;

	public Integer getValidationIntervalTime() {
		return this.env.getProperty("INTERVAL_VALIDATION_TIME", Integer.class);
	}

	public Integer getValidationAttemptLimit() {
		return this.env.getProperty("VALIDATION_ATTEMPT_LIMIT", Integer.class);
	}
}
