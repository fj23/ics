package cl.cardif.ics.icsrest.domain.dto.output;

import java.util.Date;

public class TipoRolBase {
  protected Long idRol;
  protected String nombreRol;
  protected Date fechaCreacion;
  protected String vigencia;

  public Date getFechaCreacion() {
    return fechaCreacion;
  }

  public void setFechaCreacion(Date fechaCreacion) {
    this.fechaCreacion = fechaCreacion;
  }

  public Long getIdRol() {
    return idRol;
  }

  public void setIdRol(Long idRol) {
    this.idRol = idRol;
  }

  public String getNombreRol() {
    return nombreRol;
  }

  public void setNombreRol(String nombreRol) {
    this.nombreRol = nombreRol;
  }

  public String getVigencia() {
    return vigencia;
  }

  public void setVigencia(String vigencia) {
    this.vigencia = vigencia;
  }

@Override
public String toString() {
	return "TipoRolBase [idRol=" + idRol + ", nombreRol=" + nombreRol + ", fechaCreacion=" + fechaCreacion
			+ ", vigencia=" + vigencia + "]";
}
}
