package cl.cardif.ics.icsrest.formula.funciones.custom;

import java.util.ArrayList;
import java.util.Map;

import cl.cardif.ics.icsrest.formula.FormulaParent;
import cl.cardif.ics.icsrest.formula.IStringFactor;
import cl.cardif.ics.icsrest.util.FormulaOrigenKey;

/*
    FUNCTION fnc_validarut (
        in_rut IN   VARCHAR
    ) RETURN VARCHAR2;
*/
public class FncValidaRut extends FormulaParent implements IStringFactor {
	public FncValidaRut() {
		hijos = new ArrayList<>();
	}

	@Override
	public int getMaximumChildrenCount() {
		return 1;
	}

	@Override
	public String toSafeSQL(Map<FormulaOrigenKey, Integer> tableAliases) {
		StringBuilder sb = new StringBuilder()
			.append("PCK_UTIL_ICS.FNC_VALIDARUT")
			.append("(")
			.append(getHijo(0).toSafeSQL(tableAliases))
			.append(")");

		return "(" + sb.toString() + ")";
	}

	@Override
	public String toSQL(Map<FormulaOrigenKey, Integer> tableAliases) {
		StringBuilder sb = new StringBuilder()
			.append("PCK_UTIL_ICS.FNC_VALIDARUT")
			.append("(")
			.append(getHijo(0).toSQL(tableAliases))
			.append(")");

		return "(" + sb.toString() + ")";
	}
}
