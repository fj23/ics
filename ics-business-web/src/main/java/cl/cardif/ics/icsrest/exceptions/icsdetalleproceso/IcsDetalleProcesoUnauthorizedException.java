package cl.cardif.ics.icsrest.exceptions.icsdetalleproceso;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class IcsDetalleProcesoUnauthorizedException extends RuntimeException {

  private static final long serialVersionUID = 3815682526998866691L;

  public IcsDetalleProcesoUnauthorizedException(String exception) {
    super(exception);
  }

  public IcsDetalleProcesoUnauthorizedException(String exception, Throwable t) {
    super(exception, t);
  }
}
