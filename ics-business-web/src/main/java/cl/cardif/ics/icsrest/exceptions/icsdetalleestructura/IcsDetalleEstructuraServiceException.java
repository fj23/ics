package cl.cardif.ics.icsrest.exceptions.icsdetalleestructura;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.io.Serializable;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class IcsDetalleEstructuraServiceException extends RuntimeException implements Serializable {

  private static final long serialVersionUID = -7033943522744572117L;

  public IcsDetalleEstructuraServiceException(String exception) {
    super(exception);
  }
}
