package cl.cardif.ics.icsrest.domain.dto.input;

public abstract class Proceso extends BaseModel {
	protected String correoSalida;
	protected String correoEmail;
	private String correoHitos;

	public String getCorreoEmail() {
		return correoEmail;
	}

	public void setCorreoEmail(String correoEmail) {
		this.correoEmail = correoEmail;
	}

	public String getCorreoHitos() {
		return correoHitos;
	}

	public void setCorreoHitos(String correoHitos) {
		this.correoHitos = correoHitos;
	}

	public String getCorreoSalida() {
		return correoSalida;
	}

	public void setCorreoSalida(String correoSalida) {
		this.correoSalida = correoSalida;
	}

	@Override
	public String toString() {
		return "Proceso [correoSalida=" + correoSalida + ", correoEmail=" + correoEmail + ", correoHitos=" + correoHitos + ", " + super.toString() + "]";
	}

	
}
