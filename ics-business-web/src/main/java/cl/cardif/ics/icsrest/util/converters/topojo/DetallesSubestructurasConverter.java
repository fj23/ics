package cl.cardif.ics.icsrest.util.converters.topojo;

import cl.cardif.ics.domain.entity.common.IcsTipoDato;
import cl.cardif.ics.domain.entity.estructuras.IcsDetalleSubestructura;
import cl.cardif.ics.icsrest.domain.dto.input.TipoDatoModel;
import cl.cardif.ics.icsrest.domain.dto.output.DetalleSubEstructuraOutput;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;

@Component
public class DetallesSubestructurasConverter
    implements Converter<IcsDetalleSubestructura, DetalleSubEstructuraOutput> {

  private static final Logger LOG = LoggerFactory.getLogger(DetallesSubestructurasConverter.class);

  @Override
  public DetalleSubEstructuraOutput convert(IcsDetalleSubestructura icsEstructura) {

	  DetalleSubEstructuraOutput detalle = new DetalleSubEstructuraOutput();
    detalle.setId(icsEstructura.getIdDetalleSubestructura());
    detalle.setCodigo(icsEstructura.getCodigoDetalleSubestructura());
    detalle.setDescripcion(icsEstructura.getDescripcionDetalleSubestructura());
    detalle.setLargo(icsEstructura.getLargo().longValue());
    detalle.setOrden(icsEstructura.getOrden().longValue());
    detalle.setIdSubestructuraPadre(icsEstructura.getIcsSubestructura().getIdSubestructura());
    detalle.setVigencia(icsEstructura.getVigenciaDetalleSubestructura());

    try {
      if (icsEstructura.getIcsTipoDato() != null) {
        IcsTipoDato icsTipoDato = icsEstructura.getIcsTipoDato();
        TipoDatoModel tipoDato = new TipoDatoModel();
        tipoDato.setId(icsTipoDato.getIdTipoDato());
        tipoDato.setNombre(icsTipoDato.getNombreTipoDato());

        detalle.setTipoDato(tipoDato);
      }
    } catch (EntityNotFoundException e) {
      LOG.error("El tipo de dato no se pudo convertir", e);
    }

    return detalle;
  }
}
