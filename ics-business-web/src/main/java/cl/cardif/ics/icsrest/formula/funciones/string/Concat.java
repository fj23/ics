package cl.cardif.ics.icsrest.formula.funciones.string;

import java.util.ArrayList;
import java.util.Map;

import cl.cardif.ics.icsrest.formula.FormulaParent;
import cl.cardif.ics.icsrest.formula.IFormulaFactor;
import cl.cardif.ics.icsrest.formula.IStringFactor;
import cl.cardif.ics.icsrest.formula.operadores.NumberLiteral;
import cl.cardif.ics.icsrest.util.FormulaOrigenKey;

public class Concat extends FormulaParent implements IStringFactor {
	public Concat() {
		hijos = new ArrayList<>();
	}
	
	@Override
	public int getMaximumChildrenCount() {
		return Integer.MAX_VALUE;
	}

	@Override
	public String toSafeSQL(Map<FormulaOrigenKey, Integer> tableAliases) {
		StringBuilder sb = new StringBuilder();

		for (IFormulaFactor operator : hijos) {
			
			if (sb.length() > 0) {
				sb.append(" || ");
			}

			if (operator instanceof NumberLiteral) {
				sb.append("'").append(operator.toSafeSQL(tableAliases)).append("'");
			} else {
				sb.append(operator.toSafeSQL(tableAliases));
			}
		}

		return sb.toString();
	}

	@Override
	public String toSQL(Map<FormulaOrigenKey, Integer> tableAliases) {
		StringBuilder sb = new StringBuilder();

		for (IFormulaFactor operator : hijos) {
			
			if (sb.length() > 0) {
				sb.append(" || ");
			}

			if (operator instanceof NumberLiteral) {
				sb.append("'").append(operator.toSQL(tableAliases)).append("'");
			} else {
				sb.append(operator.toSQL(tableAliases));
			}
		}

		return sb.toString();
	}
}
