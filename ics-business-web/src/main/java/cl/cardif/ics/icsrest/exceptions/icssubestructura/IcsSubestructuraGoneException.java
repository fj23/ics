package cl.cardif.ics.icsrest.exceptions.icssubestructura;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.GONE)
public class IcsSubestructuraGoneException extends RuntimeException {

  private static final long serialVersionUID = -7209750264039464453L;

  public IcsSubestructuraGoneException(String exception) {
    super(exception);
  }

  public IcsSubestructuraGoneException(String exception, Throwable t) {
    super(exception, t);
  }
}
