package cl.cardif.ics.icsrest.domain.dto.output;

import cl.cardif.ics.icsrest.domain.dto.input.ColumnaMetadatos;

public class ColumnaMetadatosModelOutput extends ColumnaMetadatos {
  private Long subEstructura;

  public ColumnaMetadatosModelOutput() {
    super();
  }

  public ColumnaMetadatosModelOutput(Long id) {
    super();
    this.id = id;
  }

  @Override
  public Long getSubEstructura() {
    return subEstructura;
  }

  @Override
  public void setSubEstructura(Long subEstructura) {
    this.subEstructura = subEstructura;
  }
}
