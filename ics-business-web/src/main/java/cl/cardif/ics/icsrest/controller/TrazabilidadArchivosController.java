package cl.cardif.ics.icsrest.controller;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cl.cardif.ics.domain.entity.common.IcsParametro;
import cl.cardif.ics.domain.entity.procesos.IcsJobs;
import cl.cardif.ics.icsgeneratefile.entity.FilesInformation;
import cl.cardif.ics.icsrest.domain.pojo.DetalleTransaccion;
import cl.cardif.ics.icsrest.domain.pojo.TrazabilidadArchivo;
import cl.cardif.ics.icsrest.domain.pojo.TrazabilidadLog;
import cl.cardif.ics.icsrest.domain.pojo.TrazabilidadRespaldo;
import cl.cardif.ics.icsrest.exceptions.icstrazabilidad.IcsTrazabilidadServiceException;
import cl.cardif.ics.icsrest.service.TrazabilidadArchivoService;
import cl.cardif.ics.icsrest.util.Constants;
import cl.cardif.ics.icsrest.util.OutputListWrapper;

@RestController
@RequestMapping("/api/trazabilidad/archivos")
public class TrazabilidadArchivosController {
	private static final Logger LOG = LoggerFactory.getLogger(TrazabilidadArchivosController.class);

	@Autowired private TrazabilidadArchivoService trazaService;

	@GetMapping("/page/{page}")
	public ResponseEntity<OutputListWrapper<TrazabilidadArchivo>> getTrazabilidadArchivos(@PathVariable int page) {
		return this.getTrazabilidadArchivos(page, Constants.DEFAULT_PAGE_SIZE, "", "", null);
	}

	@GetMapping("/page/{page}/size/{size}")
	public ResponseEntity<OutputListWrapper<TrazabilidadArchivo>> getTrazabilidadArchivos(@PathVariable int page, @PathVariable int size) {
		return this.getTrazabilidadArchivos(page, size, "", "", null);
	}

	@GetMapping("/filters")
	public ResponseEntity<OutputListWrapper<TrazabilidadArchivo>> getTrazabilidadArchivos(@RequestParam Map<String, String> filters) {
		return this.getTrazabilidadArchivos(Constants.DEFAULT_PAGE, Constants.DEFAULT_PAGE_SIZE, "", "", filters);
	}

	@GetMapping("/page/{page}/size/{size}/filters")
	public ResponseEntity<OutputListWrapper<TrazabilidadArchivo>> getTrazabilidadArchivos(
		@RequestParam Map<String, String> filters, @PathVariable int page, @PathVariable int size) {
		return this.getTrazabilidadArchivos(page, size, "", "", filters);
	}

	@GetMapping("/page/{page}/size/{size}/sort/{sortColumn}/{sortOrder}/filters")
	public ResponseEntity<OutputListWrapper<TrazabilidadArchivo>> getTrazabilidadArchivos(
		@PathVariable int page, @PathVariable int size, @PathVariable String sortColumn, @PathVariable String sortOrder,
		@RequestParam Map<String, String> filters) {
		LOG.info("getTrazabilidadArchivos");
		OutputListWrapper<TrazabilidadArchivo> trazabilidad = trazaService.getTrazabilidadArchivos(page, size, sortColumn, sortOrder, filters);
		return ResponseEntity.ok(trazabilidad);
	}

	@GetMapping("/get/{id}")
	public ResponseEntity<TrazabilidadArchivo> getTrazabilidadArchivo(@PathVariable Long id) {
		LOG.info("getTrazabilidadArchivo");
		TrazabilidadArchivo trazabilidad = trazaService.getTrazabilidadArchivo(id);
		return ResponseEntity.ok(trazabilidad);
	}

	@GetMapping("/detalle/{id}")
	public ResponseEntity<List<DetalleTransaccion>> getDetalleTransaccion(@PathVariable Long id) {
		LOG.info("getDetalleTransaccion");
		List<DetalleTransaccion> detallesTransaccion = trazaService.getDetalleTransaccion(id);
		return ResponseEntity.ok(detallesTransaccion);
	}

	@GetMapping("/exportar")
	public ResponseEntity<List<TrazabilidadArchivo>> exportTrazabilidadArchivos(@RequestParam Map<String, String> filters) {
		LOG.info("exportTrazabilidadArchivos");
		List<TrazabilidadArchivo> trazabilidad = trazaService.exportTrazabilidadArchivos(filters);
		return ResponseEntity.ok(trazabilidad);
	}

	@GetMapping("/parametros")
	public ResponseEntity<List<IcsParametro>> getParametrosTrazabilidad() {
		LOG.debug("getParametrosTrazabilidad");
		List<IcsParametro> parametros = trazaService.getParametrosTrazabilidad();
		return ResponseEntity.ok(parametros);
	}

	@GetMapping(path = "/descargar/original/{id}")
	public ResponseEntity<byte[]> descargarOriginal(@PathVariable Long id) {
		LOG.info("descargarOriginal");
		TrazabilidadRespaldo archivo = trazaService.downloadOriginal(id);
		HttpHeaders headers = trazaService.getHttpHeadersForFile(archivo.getNombreArchivo(), archivo.getExtension());
		return ResponseEntity.accepted().headers(headers).body(archivo.getArchivoOriginal());
	}

	@GetMapping(path = "/descargar/respuesta_socio/{id}")
	public ResponseEntity<byte[]> descargarRespuestaSocio(@PathVariable Long id) {
		LOG.info("descargarRespuestaSocio");

		FilesInformation outputFile = trazaService.getFileInformation(id);

		if (outputFile == null) {
			throw new IcsTrazabilidadServiceException("No existe el archivo de respuesta");
		} else {
			trazaService.deleteGeneratedFile(id, 0L);
			LOG.debug("Archivo id transaccion {} eliminado luego de generar archivo", id);

			HttpHeaders headers = trazaService.getHttpHeadersForFile(outputFile.getNameFile(), outputFile.getExtensionFile());
			byte[] fileBinaryData = trazaService.generateFile(String.valueOf(id), outputFile);

			return ResponseEntity.accepted().headers(headers).body(fileBinaryData);
		}
	}

	@GetMapping(path = "/descargar/log/{id}", produces = MediaType.TEXT_PLAIN_VALUE)
	public ResponseEntity<byte[]> descargarLog(@PathVariable Long id) {
		LOG.info("descargarLog");
		TrazabilidadLog archivo = trazaService.downloadLog(id);
		HttpHeaders headers = trazaService.getHttpHeadersForFile(archivo.getNombreArchivo(), archivo.getExtension());
		return ResponseEntity.accepted().headers(headers).body(archivo.getArchivoOriginal());
	}

	@GetMapping(path = "/consultarConsolidacion")
	public ResponseEntity<IcsJobs> consolidar() {
		LOG.info("consolidar");
		IcsJobs job = trazaService.consolidar();
		return ResponseEntity.ok(job);
	}

	@GetMapping(path = "/finalizar_proceso/{id}")
	public ResponseEntity<Void> finalizarProceso(@PathVariable Long id) {
		LOG.info("finalizarProceso");
		trazaService.finalizarProceso(id);
		return ResponseEntity.ok().build();

	}

	@ExceptionHandler({ IcsTrazabilidadServiceException.class })
	public ResponseEntity<String> handlerIcsTrazabilidadServiceException(IcsTrazabilidadServiceException ex) {
		MultiValueMap<String, String> args = new LinkedMultiValueMap<>();
		args.add("Exception-Message", ex.getMessage());
		return new ResponseEntity<>(ex.getMessage(), args, HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
