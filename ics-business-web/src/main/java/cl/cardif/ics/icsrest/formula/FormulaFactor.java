package cl.cardif.ics.icsrest.formula;

public abstract class FormulaFactor implements IFormulaFactor {
	@Override
	public String toJSON() {
		StringBuilder sb = new StringBuilder()
				.append("{")
					.append("\"tipo\":\"").append(getClass().getSimpleName()).append("\"")
				.append("}");

		return sb.toString();
	}
}
