package cl.cardif.ics.icsrest.util.converters.toentity;

import cl.cardif.ics.domain.entity.estructuras.IcsEstructura;
import cl.cardif.ics.icsrest.domain.dto.input.EstructuraInput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class IcsEstructuraFromInputConverter implements Converter<EstructuraInput, IcsEstructura> {

  static final Logger LOG = LoggerFactory.getLogger(IcsEstructuraFromInputConverter.class);

  @Override
  public IcsEstructura convert(EstructuraInput input) {
    LOG.info("convert EstructuraInput to ICSEstructura");
    IcsEstructura entity = new IcsEstructura();

    entity.setIdEstructura(input.getId());
    entity.setCodigoEstructura(input.getCodigo());
    entity.setDescripcionEstructura(input.getDescripcion());
    entity.setCardinalidadMaximaEstructura(BigDecimal.valueOf(input.getCardMax()));
    entity.setCardinalidadMinimaEstructura(BigDecimal.valueOf(input.getCardMin()));

    return entity;
  }
}
