package cl.cardif.ics.icsrest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cl.cardif.ics.domain.entity.carga.IcsResumenImportacion;

@Repository
public interface ResumenImportacionRepository 
	extends JpaRepository<IcsResumenImportacion, IcsResumenImportacion>,
		QueryDslPredicateExecutor<IcsResumenImportacion> {

	@Query("SELECT COUNT(*) FROM IcsResumenImportacion ri WHERE ri.id.idTransaccion >= :idTransaccion")
	int count(@Param("idTransaccion") Long idTransaccion);
	
}
