package cl.cardif.ics.icsrest.domain.dto.output;

import cl.cardif.ics.icsrest.domain.dto.input.BaseDetalleColumnaModel;
import cl.cardif.ics.icsrest.domain.dto.input.TipoDatoModel;

public class DetalleSubEstructuraOutput extends BaseDetalleColumnaModel {
	private TipoDatoModel tipoDato;
	private long idSubestructuraPadre;

	public DetalleSubEstructuraOutput() {
		super();
	}

	public long getIdSubestructuraPadre() {
		return idSubestructuraPadre;
	}

	public void setIdSubestructuraPadre(long idSubestructuraPadre) {
		this.idSubestructuraPadre = idSubestructuraPadre;
	}

	public TipoDatoModel getTipoDato() {
		return tipoDato;
	}

	public void setTipoDato(TipoDatoModel tipoDato) {
		this.tipoDato = tipoDato;
	}
}
