package cl.cardif.ics.icsrest.domain.dto.output;

public class TipoFormulaOutput {

  private Long idTipoFormula;
  private String estructuraFormula;
  private String nombreFormula;
  private String tipoDatoResultante;
  private String vigenciaFormula;
  private Long orden;

  public TipoFormulaOutput() {
    super();
  }

  public TipoFormulaOutput(
      Long idTipoFormula,
      String estructuraFormula,
      String nombreFormula,
      String tipoDatoResultante,
      String vigenciaFormula) {
    super();
    this.idTipoFormula = idTipoFormula;
    this.estructuraFormula = estructuraFormula;
    this.nombreFormula = nombreFormula;
    this.tipoDatoResultante = tipoDatoResultante;
    this.vigenciaFormula = vigenciaFormula;
  }

  public String getEstructuraFormula() {
    return estructuraFormula;
  }

  public void setEstructuraFormula(String estructuraFormula) {
    this.estructuraFormula = estructuraFormula;
  }

  public Long getIdTipoFormula() {
    return idTipoFormula;
  }

  public void setIdTipoFormula(Long idTipoFormula) {
    this.idTipoFormula = idTipoFormula;
  }

  public String getNombreFormula() {
    return nombreFormula;
  }

  public void setNombreFormula(String nombreFormula) {
    this.nombreFormula = nombreFormula;
  }

  public Long getOrden() {
    return orden;
  }

  public void setOrden(Long orden) {
    this.orden = orden;
  }

  public String getTipoDatoResultante() {
    return tipoDatoResultante;
  }

  public void setTipoDatoResultante(String tipoDatoResultante) {
    this.tipoDatoResultante = tipoDatoResultante;
  }

  public String getVigenciaFormula() {
    return vigenciaFormula;
  }

  public void setVigenciaFormula(String vigenciaFormula) {
    this.vigenciaFormula = vigenciaFormula;
  }
}
