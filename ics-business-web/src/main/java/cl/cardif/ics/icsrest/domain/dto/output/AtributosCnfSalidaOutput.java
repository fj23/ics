package cl.cardif.ics.icsrest.domain.dto.output;

import cl.cardif.ics.icsrest.formula.IFormulaFactor;

public class AtributosCnfSalidaOutput {

	private Long idAtributoAnulacion;
	private Long idDetalleEstructura;
	private Long idDetalleSubEstructura;
	private IFormulaFactor formula;
	private Long idHomologacion;
	private Long idFormato;
	private Long atributoEstructura;
	private Long valor;

	public AtributosCnfSalidaOutput() {
		super();
	}

	public Long getIdAtributoAnulacion() {
		return idAtributoAnulacion;
	}

	public void setIdAtributoAnulacion(Long idAtributoAnulacion) {
		this.idAtributoAnulacion = idAtributoAnulacion;
	}

	public Long getIdDetalleEstructura() {
		return idDetalleEstructura;
	}

	public void setIdDetalleEstructura(Long idDetalleEstructura) {
		this.idDetalleEstructura = idDetalleEstructura;
	}

	public Long getIdDetalleSubEstructura() {
		return idDetalleSubEstructura;
	}

	public void setIdDetalleSubEstructura(Long idDetalleSubEstructura) {
		this.idDetalleSubEstructura = idDetalleSubEstructura;
	}

	public IFormulaFactor getFormula() {
		return formula;
	}

	public void setFormula(IFormulaFactor formula) {
		this.formula = formula;
	}

	public Long getIdHomologacion() {
		return idHomologacion;
	}

	public void setIdHomologacion(Long idHomologacion) {
		this.idHomologacion = idHomologacion;
	}

	public Long getIdFormato() {
		return idFormato;
	}

	public void setIdFormato(Long idFormato) {
		this.idFormato = idFormato;
	}

	public Long getAtributoEstructura() {
		return atributoEstructura;
	}

	public void setAtributoEstructura(Long atributoEstructura) {
		this.atributoEstructura = atributoEstructura;
	}

	public Long getValor() {
		return valor;
	}

	public void setValor(Long valor) {
		this.valor = valor;
	}

}
