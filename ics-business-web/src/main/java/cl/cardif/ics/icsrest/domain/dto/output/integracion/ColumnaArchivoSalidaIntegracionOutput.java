package cl.cardif.ics.icsrest.domain.dto.output.integracion;

public class ColumnaArchivoSalidaIntegracionOutput {

	private Long id;
	private Long orden;
	private Long idColumnaOrigen;
	private Long posicionInicial;
	private Long posicionFinal;
	private Long idColumnaDestino;
	private Boolean separarArchivo;
	private Boolean incluir;

	private Long idIntegracion;

	public ColumnaArchivoSalidaIntegracionOutput() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdColumnaDestino() {
		return idColumnaDestino;
	}

	public void setIdColumnaDestino(Long idColumnaDestino) {
		this.idColumnaDestino = idColumnaDestino;
	}

	public Long getIdColumnaOrigen() {
		return idColumnaOrigen;
	}

	public void setIdColumnaOrigen(Long idColumnaOrigen) {
		this.idColumnaOrigen = idColumnaOrigen;
	}

	public Long getOrden() {
		return orden;
	}

	public void setOrden(Long orden) {
		this.orden = orden;
	}

	public Long getPosicionFinal() {
		return posicionFinal;
	}

	public void setPosicionFinal(Long posicionFinal) {
		this.posicionFinal = posicionFinal;
	}

	public Long getPosicionInicial() {
		return posicionInicial;
	}

	public void setPosicionInicial(Long posicionInicial) {
		this.posicionInicial = posicionInicial;
	}

	public Boolean getSepararArchivo() {
		return separarArchivo;
	}

	public void setSepararArchivo(Boolean separarArchivo) {
		this.separarArchivo = separarArchivo;
	}

	public Long getIdIntegracion() {
		return idIntegracion;
	}

	public void setIdIntegracion(Long idIntegracion) {
		this.idIntegracion = idIntegracion;
	}

	public Boolean getIncluir() {
		return incluir;
	}

	public void setIncluir(Boolean incluir) {
		this.incluir = incluir;
	}

	@Override
	public String toString() {
		return "ColumnaArchivoSalidaIntegracionOutput [id=" + id + ", orden=" + orden + ", idIntegracion="
				+ idIntegracion + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((idColumnaDestino == null) ? 0 : idColumnaDestino.hashCode());
		result = prime * result + ((idColumnaOrigen == null) ? 0 : idColumnaOrigen.hashCode());
		result = prime * result + ((idIntegracion == null) ? 0 : idIntegracion.hashCode());
		result = prime * result + ((incluir == null) ? 0 : incluir.hashCode());
		result = prime * result + ((orden == null) ? 0 : orden.hashCode());
		result = prime * result + ((posicionFinal == null) ? 0 : posicionFinal.hashCode());
		result = prime * result + ((posicionInicial == null) ? 0 : posicionInicial.hashCode());
		result = prime * result + ((separarArchivo == null) ? 0 : separarArchivo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ColumnaArchivoSalidaIntegracionOutput other = (ColumnaArchivoSalidaIntegracionOutput) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (idColumnaDestino == null) {
			if (other.idColumnaDestino != null)
				return false;
		} else if (!idColumnaDestino.equals(other.idColumnaDestino))
			return false;
		if (idColumnaOrigen == null) {
			if (other.idColumnaOrigen != null)
				return false;
		} else if (!idColumnaOrigen.equals(other.idColumnaOrigen))
			return false;
		if (idIntegracion == null) {
			if (other.idIntegracion != null)
				return false;
		} else if (!idIntegracion.equals(other.idIntegracion))
			return false;
		if (incluir == null) {
			if (other.incluir != null)
				return false;
		} else if (!incluir.equals(other.incluir))
			return false;
		if (orden == null) {
			if (other.orden != null)
				return false;
		} else if (!orden.equals(other.orden))
			return false;
		if (posicionFinal == null) {
			if (other.posicionFinal != null)
				return false;
		} else if (!posicionFinal.equals(other.posicionFinal))
			return false;
		if (posicionInicial == null) {
			if (other.posicionInicial != null)
				return false;
		} else if (!posicionInicial.equals(other.posicionInicial))
			return false;
		if (separarArchivo == null) {
			if (other.separarArchivo != null)
				return false;
		} else if (!separarArchivo.equals(other.separarArchivo))
			return false;
		return true;
	}

	
}
