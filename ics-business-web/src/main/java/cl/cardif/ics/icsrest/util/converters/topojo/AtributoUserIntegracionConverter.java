package cl.cardif.ics.icsrest.util.converters.topojo;

import cl.cardif.ics.domain.entity.plantillas.IcsAtributoUserIntg;
import cl.cardif.ics.icsrest.domain.dto.output.AtributoUserIntgOutput;
import cl.cardif.ics.icsrest.domain.dto.output.AtributosIntgEventoOutput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component
public class AtributoUserIntegracionConverter
    implements Converter<IcsAtributoUserIntg, AtributoUserIntgOutput> {
  private static final Logger LOG = LoggerFactory.getLogger(AtributoUserIntegracionConverter.class);

  @Override
  public AtributoUserIntgOutput convert(IcsAtributoUserIntg entity) {
    AtributoUserIntgOutput pojo = new AtributoUserIntgOutput();
    pojo.setIdAtributoUser(entity.getIdAtributoUser());
    pojo.setIdAtributoIntegracionEvento(
        entity.getIcsAtributoIntegracionEvento().getIdIntegracionEvento());
    pojo.setNombreColumna(entity.getNombreColumna());
    pojo.setTipoPersona(entity.getTipoPersona());
    pojo.setFlgCardinalidad(entity.getFlgCardinalidad());

    try {
      if (entity.getIcsAtributoIntegracionEvento() != null) {
        AtributoIntegracionEventConverter convert = new AtributoIntegracionEventConverter();
        AtributosIntgEventoOutput atributoIntgEvento =
            convert.convert(entity.getIcsAtributoIntegracionEvento());
        pojo.setAtributosIntgEvento(atributoIntgEvento);

        pojo.setIdAtributoIntegracionEvento(atributoIntgEvento.getIdIntegracionEvento());
        pojo.setNombreColumnaIntegracion(atributoIntgEvento.getNombreColumnaIntegracion());
      }
    } catch (EntityNotFoundException e) {
      LOG.info("IcsAtributoUserIntg", e);
    }

    return pojo;
  }

  public List<AtributoUserIntgOutput> convertList(Collection<IcsAtributoUserIntg> entities) {
    List<AtributoUserIntgOutput> pojoList = new ArrayList<>();

    for (IcsAtributoUserIntg entity : entities) {
      pojoList.add(this.convert(entity));
    }

    return pojoList;
  }
}
