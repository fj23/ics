package cl.cardif.ics.icsrest.repository;

import cl.cardif.ics.domain.entity.plantillas.IcsAtributos;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface AtributosPlantillaRepository
    extends JpaRepository<IcsAtributos, Long>, 
    	QueryDslPredicateExecutor<IcsAtributos> {
	
}
