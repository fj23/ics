package cl.cardif.ics.icsrest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

import cl.cardif.ics.domain.entity.estructuras.IcsDetalleSubestructura;

@Repository
public interface DetallesSubestructurasRepository
    extends JpaRepository<IcsDetalleSubestructura, Long>,
        QueryDslPredicateExecutor<IcsDetalleSubestructura> {
	
}
