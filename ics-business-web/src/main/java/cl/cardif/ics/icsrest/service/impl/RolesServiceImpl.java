package cl.cardif.ics.icsrest.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import com.querydsl.core.BooleanBuilder;

import cl.cardif.ics.domain.entity.perfilamiento.IcsTipoRol;
import cl.cardif.ics.domain.entity.perfilamiento.QIcsTipoRol;
import cl.cardif.ics.icsrest.domain.pojo.TipoRol;
import cl.cardif.ics.icsrest.repository.RolesRepository;
import cl.cardif.ics.icsrest.service.RolesService;

@Service
public class RolesServiceImpl implements RolesService {
	private static final Logger LOG = LoggerFactory.getLogger(RolesServiceImpl.class);

	@Autowired private ConversionService conversionService;
	@Autowired private RolesRepository rolesRepository;

	@Override
	public List<TipoRol> findRolesByName(List<String> roles) {
		LOG.info("findRolesByName");
		QIcsTipoRol qIcsRol = QIcsTipoRol.icsTipoRol;

		BooleanBuilder booleanBuilder = new BooleanBuilder()
				.and(qIcsRol.nombreRol.in(roles));

		Iterable<IcsTipoRol> iteraRoles = this.rolesRepository.findAll(booleanBuilder);
		
		List<TipoRol> lista = new ArrayList<>();
		for (IcsTipoRol icsTipoRol : iteraRoles) {
			TipoRol tipoRol = conversionService.convert(icsTipoRol, TipoRol.class);
			lista.add(tipoRol);
		} 

		return lista;
	}
}
