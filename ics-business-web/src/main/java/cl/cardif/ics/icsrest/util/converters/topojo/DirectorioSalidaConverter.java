package cl.cardif.ics.icsrest.util.converters.topojo;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import cl.cardif.ics.domain.entity.common.IcsDirectoriosSalida;
import cl.cardif.ics.icsrest.domain.pojo.DirectorioSalida;

@Component
public class DirectorioSalidaConverter implements Converter<IcsDirectoriosSalida, DirectorioSalida> {

	@Override
	public DirectorioSalida convert(IcsDirectoriosSalida source) {
		DirectorioSalida target = new DirectorioSalida();
		target.setId(source.getIdDirectorioSalida());
		target.setNombre(source.getDescripcionDirectorio());
		target.setVigencia(source.getFlag());
		return target;
	}
}
