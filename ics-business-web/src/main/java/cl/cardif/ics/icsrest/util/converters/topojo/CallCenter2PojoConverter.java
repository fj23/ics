package cl.cardif.ics.icsrest.util.converters.topojo;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import cl.cardif.ics.domain.entity.common.IcsCallCenter;
import cl.cardif.ics.icsrest.domain.pojo.CallCenter;

@Component
public class CallCenter2PojoConverter implements Converter<IcsCallCenter, CallCenter> {

	@Override
	public CallCenter convert(IcsCallCenter source) {
		CallCenter callCenter = new CallCenter();
		callCenter.setId(source.getIdCallCenter());
		callCenter.setNombre(source.getNombreCallCenter());
		callCenter.setVigencia(source.getFlgActivo());
		return callCenter;
	}

}
