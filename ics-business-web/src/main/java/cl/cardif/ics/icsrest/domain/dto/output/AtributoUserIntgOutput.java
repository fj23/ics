package cl.cardif.ics.icsrest.domain.dto.output;

public class AtributoUserIntgOutput {

	private Long idAtributoUser;
	private String tipoPersona;
	private String nombreColumna;
	private String flgCardinalidad;

	private AtributosIntgEventoOutput atributosIntgEvento;

	private Long idAtributoIntegracionEvento;
	private String nombreColumnaIntegracion;

	public AtributoUserIntgOutput() {
		super();
	}

	public Long getIdAtributoUser() {
		return idAtributoUser;
	}

	public void setIdAtributoUser(Long idAtributoUsuario) {
		this.idAtributoUser = idAtributoUsuario;
	}

	public String getTipoPersona() {
		return tipoPersona;
	}

	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	public String getNombreColumna() {
		return nombreColumna;
	}

	public void setNombreColumna(String nombreColumna) {
		this.nombreColumna = nombreColumna;
	}

	public String getFlgCardinalidad() {
		return flgCardinalidad;
	}

	public void setFlgCardinalidad(String flgCardinalidad) {
		this.flgCardinalidad = flgCardinalidad;
	}

	public AtributosIntgEventoOutput getAtributosIntgEvento() {
		return atributosIntgEvento;
	}

	public void setAtributosIntgEvento(AtributosIntgEventoOutput atributosIntgEvento) {
		this.atributosIntgEvento = atributosIntgEvento;
	}

	public Long getIdAtributoIntegracionEvento() {
		return idAtributoIntegracionEvento;
	}

	public void setIdAtributoIntegracionEvento(Long idAtributoIntegracionEvento) {
		this.idAtributoIntegracionEvento = idAtributoIntegracionEvento;
	}

	public String getNombreColumnaIntegracion() {
		return nombreColumnaIntegracion;
	}

	public void setNombreColumnaIntegracion(String nombreColumnaIntegracion) {
		this.nombreColumnaIntegracion = nombreColumnaIntegracion;
	}

	@Override
	public String toString() {
		return "AtributoUserIntgOutput [idAtributoUsuario=" + idAtributoUser + ", tipoPersona=" + tipoPersona
				+ ", nombreColumna=" + nombreColumna + ", flgCardinalidad=" + flgCardinalidad + ", atributosIntgEvento="
				+ atributosIntgEvento + ", idAtributoIntegracionEvento=" + idAtributoIntegracionEvento
				+ ", nombreColumnaIntegracion=" + nombreColumnaIntegracion + "]";
	}

}
