package cl.cardif.ics.icsrest.repository;

import cl.cardif.ics.domain.entity.common.IcsTipoSalida;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface TiposSalidaRepository
    extends JpaRepository<IcsTipoSalida, Long>, 
		QueryDslPredicateExecutor<IcsTipoSalida> {
	
}
