package cl.cardif.ics.icsrest.exceptions.icsplantilla;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class IcsPlantillaNotFoundException extends RuntimeException {

  private static final long serialVersionUID = -1527596879157446910L;

  public IcsPlantillaNotFoundException(String exception) {
    super(exception);
  }

  public IcsPlantillaNotFoundException(String exception, Throwable t) {
    super(exception, t);
  }
}
