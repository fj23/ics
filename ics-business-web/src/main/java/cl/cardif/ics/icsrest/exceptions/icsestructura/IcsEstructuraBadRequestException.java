package cl.cardif.ics.icsrest.exceptions.icsestructura;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class IcsEstructuraBadRequestException extends RuntimeException {

  private static final long serialVersionUID = -6459654653826812166L;

  public IcsEstructuraBadRequestException(String exception) {
    super(exception);
  }

  public IcsEstructuraBadRequestException(String exception, Throwable t) {
    super(exception, t);
  }
}
