package cl.cardif.ics.icsrest.util.converters.topojo;

import cl.cardif.ics.domain.entity.common.IcsTipoDato;
import cl.cardif.ics.icsrest.domain.pojo.TipoDato;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class TipoDatoConverter implements Converter<IcsTipoDato, TipoDato> {

  @Override
  public TipoDato convert(IcsTipoDato icsTipoDato) {
    TipoDato tipoDato = new TipoDato();
    tipoDato.setId(icsTipoDato.getIdTipoDato());
    tipoDato.setNombre(icsTipoDato.getNombreTipoDato());
    return tipoDato;
  }
}
