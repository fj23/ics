package cl.cardif.ics.icsrest.formula.funciones.uf;

import java.util.Map;

import cl.cardif.ics.icsrest.formula.FormulaFactor;
import cl.cardif.ics.icsrest.formula.IMathFactor;
import cl.cardif.ics.icsrest.util.FormulaOrigenKey;

public class ValorUFActual extends FormulaFactor implements IMathFactor {
	public ValorUFActual() {
		super();
	}

	@Override
	public String toSafeSQL(Map<FormulaOrigenKey, Integer> tableAliases) {
		return "1";
	}

	@Override
	public String toSQL(Map<FormulaOrigenKey, Integer> tableAliases) {
		return "PCK_UTIL_ICS.FNC_VALORUF_ACTUAL()";
	}
}
