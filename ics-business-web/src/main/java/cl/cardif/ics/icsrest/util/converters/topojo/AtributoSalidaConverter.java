package cl.cardif.ics.icsrest.util.converters.topojo;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import cl.cardif.ics.domain.entity.plantillas.IcsAtributos;
import cl.cardif.ics.domain.entity.plantillas.IcsAtributosSalida;
import cl.cardif.ics.icsrest.domain.dto.input.ColumnaMetadatosSalidaModel;
import cl.cardif.ics.icsrest.domain.dto.output.AtributoUsuarioOutput;
import cl.cardif.ics.icsrest.domain.dto.output.AtributosSalidaOutput;

@Component
public class AtributoSalidaConverter implements Converter<IcsAtributosSalida, AtributosSalidaOutput> {

	@Override
	public AtributosSalidaOutput convert(IcsAtributosSalida entity) {
		AtributosSalidaOutput pojo = new AtributosSalidaOutput();

		AtributoUsuarioOutput atributoUsuario = new AtributoUsuarioOutput();
		if (entity.getIcsAtributoUsuario() != null) {
			atributoUsuario.setIdAtributoUsuario(entity.getIcsAtributoUsuario().getIdAtributoUsuario());
			atributoUsuario.setNombreColumna(entity.getIcsAtributoUsuario().getNombreColumna());

			pojo.setColumnaOrigenEM(atributoUsuario);
		}

		IcsAtributos icsAtributos = entity.getIcsAtributos();
		if (icsAtributos != null) {
			ColumnaMetadatosSalidaModel columnaSalida = new ColumnaMetadatosSalidaModel();
			
			columnaSalida.setId(icsAtributos.getIdAtributo());
			if (entity.getIcsAtributoUsuario() != null)
				columnaSalida.setNombre(entity.getIcsAtributoUsuario().getNombreColumna());
			columnaSalida.setOrden(icsAtributos.getNumeroColumna());
			columnaSalida.setFlgAgrupacion(icsAtributos.getFlgAgrupacion());

			if (icsAtributos.getPosicionInicial() != null) {
				columnaSalida.setPosicionInicial(icsAtributos.getPosicionInicial());
			} else {
				columnaSalida.setPosicionInicial(0L);
			}

			if (icsAtributos.getPosicionFinal() != null) {
				columnaSalida.setPosicionFinal(icsAtributos.getPosicionFinal());
			} else {
				columnaSalida.setPosicionFinal(0L);
			}

			pojo.setAtributo(columnaSalida);
		}

		return pojo;
	}
}
