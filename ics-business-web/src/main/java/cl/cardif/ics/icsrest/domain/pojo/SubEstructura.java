package cl.cardif.ics.icsrest.domain.pojo;

import cl.cardif.ics.icsrest.domain.dto.input.BaseModel;

import java.util.ArrayList;
import java.util.List;

public class SubEstructura extends BaseModel {

	private String descripcion;
	private String codigo;
	private List<DetalleSubEstructura> detalles;
	private Integer largo;

	public SubEstructura() {
		super();
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<DetalleSubEstructura> getDetalles() {
		if (this.detalles != null) {
			return new ArrayList<>(this.detalles);
		} else {
			return new ArrayList<>();
		}
	}

	public void setDetalles(List<DetalleSubEstructura> detalles) {
		if (detalles != null) {
			this.detalles = new ArrayList<>(detalles);
		}
	}

	public Integer getLargo() {
		return largo;
	}

	public void setLargo(Integer largo) {
		this.largo = largo;
	}

}
