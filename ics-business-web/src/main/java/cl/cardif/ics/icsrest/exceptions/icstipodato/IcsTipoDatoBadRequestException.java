package cl.cardif.ics.icsrest.exceptions.icstipodato;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class IcsTipoDatoBadRequestException extends RuntimeException {

  private static final long serialVersionUID = -6459654653826812166L;

  public IcsTipoDatoBadRequestException(String exception) {
    super(exception);
  }

  public IcsTipoDatoBadRequestException(String exception, Throwable t) {
    super(exception, t);
  }
}
