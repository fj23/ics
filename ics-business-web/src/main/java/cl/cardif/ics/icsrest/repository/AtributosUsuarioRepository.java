package cl.cardif.ics.icsrest.repository;

import cl.cardif.ics.domain.entity.plantillas.IcsAtributoUsuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface AtributosUsuarioRepository
    extends JpaRepository<IcsAtributoUsuario, Long>,
        QueryDslPredicateExecutor<IcsAtributoUsuario> {
	
}
