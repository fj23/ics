package cl.cardif.ics.icsrest.service;

import java.util.List;

import org.springframework.security.access.prepost.PostFilter;

import cl.cardif.ics.domain.entity.procesos.IcsProceso;
import cl.cardif.ics.icsrest.domain.dto.output.ProcesoOutput;
import cl.cardif.ics.icsrest.domain.pojo.Core;
import cl.cardif.ics.icsrest.domain.pojo.DetalleFormato;
import cl.cardif.ics.icsrest.domain.pojo.Evento;
import cl.cardif.ics.icsrest.domain.pojo.Socio;
import cl.cardif.ics.icsrest.domain.pojo.TipoDato;
import cl.cardif.ics.icsrest.domain.pojo.TipoExtension;
import cl.cardif.ics.icsrest.domain.pojo.TipoFormula;
import cl.cardif.ics.icsrest.domain.pojo.TipoPlantilla;
import cl.cardif.ics.icsrest.domain.pojo.TipoSalida;

/**
 * Se encarga de consultar la BD y transformar las entidades obtenidas a
 * POJO/DTO dentro de listas
 */
public interface CommonService {
	/**
	 * Obtiene el listado de todos los cores de Cardif.
	 * @return List&lt;{@link Core}&gt;
	 * @see CommonService
	 */
	List<Core> getAllCores();

	/**
	 * Obtiene el listado de todos tipos de datos.
	 * @return List&lt;{@link TipoDato}&gt;
	 * @see CommonService
	 */
	List<TipoDato> getAllDataTypes();

	/**
	 * Retorna los eventos en la base de datos despues de ser filtrados.
	 * @return List&lt;{@link Evento}&gt;
	 * @see CommonService
	 */
	@PostFilter("hasAnyAuthority('EVENT_'+filterObject.id)")
	List<Evento> getAllEvents();

	/**
	 * Obtiene el listado de todos tipos de extensión.
	 * @return List&lt;{@link TipoExtension}&gt;
	 * @see CommonService
	 */
	List<TipoExtension> getAllExtensionTypes();

	/**
	 * Obtiene el listado de todos tipos de formato.
	 * @return List&lt;{@link DetalleFormato}&gt;
	 * @see CommonService
	 */
	List<DetalleFormato> getAllFormatTypes();

	/**
	 * Obtiene el listado de todos tipos de formula.
	 * @return List&lt;{@link TipoFormula}&gt;
	 * @see CommonService
	 */
	List<TipoFormula> getAllFormulaTypes();

	/**
	 * Obtiene el listado de todos tipos de salida.
	 * @return List&lt;{@link TipoSalida}&gt;
	 * @see CommonService
	 */
	List<TipoSalida> getAllOutputTypes();

	/**
	 * Obtiene el listado de todos los socios.
	 * @return List&lt;{@link Socio}&gt;
	 * @see CommonService
	 */
	List<Socio> getAllPartners();

	/**
	 * Obtiene todos los tiempos de plantillas.
	 * @return List&lt;{@link TipoPlantilla}&gt;
	 * @see CommonService
	 */
	List<TipoPlantilla> getAllTemplateTypes();

	/**
	 * Obtiene los procesos, los parametros idSocio e idEvento son opcionales,
	 * si el idSocio no es especificado obtiene los procesos de todos los socios,
	 * si el idEvento no es especificado obtiene los procesos que solo el usuario 
	 * tiene autorizado obtener según sus permisos de eventos, estos se obtienen desde
	 * {@link org.springframework.security.core.context.SecurityContextHolder} 
	 * utilizando la utilidad getEventsAuthorized() en {@link cl.cardif.ics.icsrest.util.Util}.
	 * @param idSocio Filtra por el identificador del socio (opcional)
	 * @param idEvento Filtra por el identificador del evento (opcional)
	 * @return List&lt;{@link IcsProceso}&gt; Lista de Procesos
	 * @see CommonService
	 */
	List<ProcesoOutput> getProcesos(Long idSocio, Long idEvento);
}
