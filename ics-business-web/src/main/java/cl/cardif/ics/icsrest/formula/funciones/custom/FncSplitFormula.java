package cl.cardif.ics.icsrest.formula.funciones.custom;

import java.util.ArrayList;
import java.util.Map;

import cl.cardif.ics.icsrest.formula.FormulaParent;
import cl.cardif.ics.icsrest.formula.IStringFactor;
import cl.cardif.ics.icsrest.util.FormulaOrigenKey;

/*
    FUNCTION fnc_substr_formula (
	    pv_valorentrada IN VARCHAR2,
	    pv_delimitador  IN VARCHAR2,
	    pn_posicion     IN NUMBER
    ) RETURN VARCHAR2;
*/
public class FncSplitFormula extends FormulaParent implements IStringFactor {
	public FncSplitFormula() {
		hijos = new ArrayList<>();
	}

	@Override
	public int getMaximumChildrenCount() {
		return 3;
	}

	@Override
	public String toSafeSQL(Map<FormulaOrigenKey, Integer> tableAliases) {
		StringBuilder sb = new StringBuilder()
			.append("PCK_UTIL_APP.FNC_SUBSTR_FORMULA")
			.append("(")
			.append(getHijo(0).toSafeSQL(tableAliases)).append(", ")
			.append(getHijo(1).toSafeSQL(tableAliases)).append(", ")
			.append(getHijo(2).toSafeSQL(tableAliases)).append(", ")
			.append(")");

		return sb.toString();
	}

	@Override
	public String toSQL(Map<FormulaOrigenKey, Integer> tableAliases) {
		StringBuilder sb = new StringBuilder()
			.append("PCK_UTIL_APP.FNC_SUBSTR_FORMULA")
			.append("(")
			.append(getHijo(0).toSQL(tableAliases)).append(", ")
			.append(getHijo(1).toSQL(tableAliases)).append(", ")
			.append(getHijo(2).toSQL(tableAliases)).append(", ")
			.append(")");

		return sb.toString();
	}
}
