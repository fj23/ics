package cl.cardif.ics.icsrest.util.converters.topojo;

import cl.cardif.ics.domain.entity.plantillas.IcsAtributosIntgEvento;
import cl.cardif.ics.icsrest.domain.dto.output.AtributosIntgEventoOutput;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class AtributoIntegracionEventConverter
    implements Converter<IcsAtributosIntgEvento, AtributosIntgEventoOutput> {

  @Override
  public AtributosIntgEventoOutput convert(IcsAtributosIntgEvento entity) {
    AtributosIntgEventoOutput pojo = new AtributosIntgEventoOutput();
    pojo.setIdIntegracionEvento(entity.getIdIntegracionEvento());
    pojo.setIdEvento(entity.getIcsEvento().getIdEvento());
    pojo.setNombreTablaDestino(entity.getNombreTablaDestino());
    pojo.setNombreColumnaIntegracion(entity.getNombreColumnaIntegracion());
    pojo.setFlgVisible(entity.getFlgVisible());
    pojo.setTraduccion(entity.getTraduccion());
    return pojo;
  }
}
