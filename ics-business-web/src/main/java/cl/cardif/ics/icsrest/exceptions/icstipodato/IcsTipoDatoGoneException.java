package cl.cardif.ics.icsrest.exceptions.icstipodato;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.GONE)
public class IcsTipoDatoGoneException extends RuntimeException {

  private static final long serialVersionUID = -7209750264039464453L;

  public IcsTipoDatoGoneException(String exception) {
    super(exception);
  }

  public IcsTipoDatoGoneException(String exception, Throwable t) {
    super(exception, t);
  }
}
