package cl.cardif.ics.icsrest.domain.dto.output;

public class DetalleFormatoOutput {
  private long idFormato;
  private TipoFormatoOutput tipoFormato;
  private String formulaFormato;
  private String tipoDato;
  private String formato;
  private String nombreDetalleFormato;

  public DetalleFormatoOutput() {
    super();
  }

  public String getFormato() {
    return formato;
  }

  public void setFormato(String formato) {
    this.formato = formato;
  }

  public String getFormulaFormato() {
    return formulaFormato;
  }

  public void setFormulaFormato(String formulaFormato) {
    this.formulaFormato = formulaFormato;
  }

  public long getIdFormato() {
    return idFormato;
  }

  public void setIdFormato(long idFormato) {
    this.idFormato = idFormato;
  }

  public String getNombreDetalleFormato() {
    return nombreDetalleFormato;
  }

  public void setNombreDetalleFormato(String nombreDetalleFormato) {
    this.nombreDetalleFormato = nombreDetalleFormato;
  }

  public String getTipoDato() {
    return tipoDato;
  }

  public void setTipoDato(String tipoDato) {
    this.tipoDato = tipoDato;
  }

  public TipoFormatoOutput getTipoFormato() {
    return tipoFormato;
  }

  public void setTipoFormato(TipoFormatoOutput tipoFormato) {
    this.tipoFormato = tipoFormato;
  }
}
