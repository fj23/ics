package cl.cardif.ics.icsrest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

import cl.cardif.ics.domain.entity.procesos.VwResumenTrazabilidad;

@Repository
public interface VwResumenTranzabilidadRepository 
	extends JpaRepository<VwResumenTrazabilidad, Long>, 
		QueryDslPredicateExecutor<VwResumenTrazabilidad> {

}
