package cl.cardif.ics.icsrest.domain.pojo;

import cl.cardif.ics.icsrest.domain.dto.input.EstructuraAnetoSimpleInput;

import java.util.ArrayList;
import java.util.List;

public class Estructura extends EstructuraAnetoSimpleInput {

	private List<DetalleEstructura> columnas;
	private String flgSatelite;
	private String vigencia;
	private Integer largo;

	public Estructura() {
		super();
	}

	public List<DetalleEstructura> getColumnas() {
		if (this.columnas != null) {
			return new ArrayList<>(this.columnas);
		} else {
			return new ArrayList<>();
		}
	}

	public void setColumnas(List<DetalleEstructura> columnas) {
		if (columnas != null) {
			this.columnas = new ArrayList<>(columnas);
		}
	}

	public String getFlgSatelite() {
		return flgSatelite;
	}

	public void setFlgSatelite(String flgSatelite) {
		this.flgSatelite = flgSatelite;
	}

	public String getVigencia() {
		return vigencia;
	}

	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}

	public Integer getLargo() {
		return largo;
	}

	public void setLargo(Integer largo) {
		this.largo = largo;
	}

}
