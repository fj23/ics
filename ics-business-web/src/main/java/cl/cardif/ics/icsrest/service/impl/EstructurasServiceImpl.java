package cl.cardif.ics.icsrest.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityNotFoundException;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Service;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.OrderSpecifier;

import cl.cardif.ics.domain.entity.common.IcsTipoDato;
import cl.cardif.ics.domain.entity.estructuras.IcsDetalleEstructura;
import cl.cardif.ics.domain.entity.estructuras.IcsEstructura;
import cl.cardif.ics.domain.entity.estructuras.IcsSubestructura;
import cl.cardif.ics.domain.entity.estructuras.QIcsDetalleEstructura;
import cl.cardif.ics.domain.entity.estructuras.QIcsEstructura;
import cl.cardif.ics.icsrest.domain.dto.input.ColumnaMetadatosModel;
import cl.cardif.ics.icsrest.domain.dto.input.EstructuraAnetoModel;
import cl.cardif.ics.icsrest.domain.dto.input.EstructuraInput;
import cl.cardif.ics.icsrest.domain.dto.output.DetalleEstructuraOutput;
import cl.cardif.ics.icsrest.domain.dto.output.EstructuraOutput;
import cl.cardif.ics.icsrest.domain.dto.output.EstructuraSimpleOutput;
import cl.cardif.ics.icsrest.domain.dto.output.RequestResultOutput;
import cl.cardif.ics.icsrest.exceptions.icsestructura.IcsEstructuraNotFoundException;
import cl.cardif.ics.icsrest.exceptions.icsestructura.IcsEstructuraServiceException;
import cl.cardif.ics.icsrest.exceptions.icssubestructura.IcsSubestructuraNotFoundException;
import cl.cardif.ics.icsrest.exceptions.icstipodato.IcsTipoDatoNotFoundException;
import cl.cardif.ics.icsrest.repository.DetallesEstructurasRepository;
import cl.cardif.ics.icsrest.repository.EstructurasRepository;
import cl.cardif.ics.icsrest.repository.SubestructurasRepository;
import cl.cardif.ics.icsrest.repository.TiposDatosRepository;
import cl.cardif.ics.icsrest.service.EstructurasService;
import cl.cardif.ics.icsrest.util.OutputListWrapper;

@Service
public class EstructurasServiceImpl implements EstructurasService {
	private static final Logger LOG = LoggerFactory.getLogger(EstructurasServiceImpl.class);

	private static final String CODIGO_ESTRUCTURA = "codigoEstructura";
	private static final String NOMBRE_ESTRUCTURA = "descripcionEstructura";

	@Autowired private TiposDatosRepository tiposDatosRepo;
	@Autowired private EstructurasRepository estructurasRepository;
	@Autowired private SubestructurasRepository subestructurasRepository;
	@Autowired private DetallesEstructurasRepository detalleEstructurasRepository;
	@Autowired private ConversionService conversionService;

	private JdbcTemplate jdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	private IcsDetalleEstructura convertirColumnaMetadatosAIcsDetalleEstructura(
			ColumnaMetadatosModel columnaMetadatos) {
		IcsDetalleEstructura icsDetalleEstructura = conversionService.convert(columnaMetadatos, IcsDetalleEstructura.class);

		Long idTipoDato = columnaMetadatos.getTipoDato();
		try {
			IcsTipoDato tipoEntity = tiposDatosRepo.findOne(idTipoDato);
			icsDetalleEstructura.setIcsTipoDato(tipoEntity);
			icsDetalleEstructura.setTipoDato(tipoEntity.getNombreTipoDato().substring(0,1));
		} catch (EntityNotFoundException e) {
			throw new IcsTipoDatoNotFoundException("El tipo de dato de la columna no pudo ser encontrado", e);
		}
		
		Long idSubestructura = columnaMetadatos.getSubEstructura();
		if (idSubestructura != null && idSubestructura > 0) {

			try {
				IcsSubestructura subestructura = subestructurasRepository.findOne(idSubestructura);
				icsDetalleEstructura.setIcsSubestructura(subestructura);
			} catch (EntityNotFoundException e) {
				throw new IcsSubestructuraNotFoundException("La subestructura de la columna no pudo ser encontrada", e);
			}
		}
		return icsDetalleEstructura;
	}

	@Override
	public void almacenarEstructura(EstructuraAnetoModel input) {
		LOG.info("almacenarEstructura");
		
		IcsEstructura entity = conversionService.convert(input, IcsEstructura.class);
		entity.setVigenciaEstructura("Y");
		entity.setFlgSatelite("Y");

		entity = estructurasRepository.saveAndFlush(entity);
		
		for (ColumnaMetadatosModel columnaMetadatos : input.getColumnas()) {
			IcsDetalleEstructura detalleEstructura = this.convertirColumnaMetadatosAIcsDetalleEstructura(columnaMetadatos);
			detalleEstructura.setIcsEstructura(entity);
			detalleEstructurasRepository.save(detalleEstructura);
		}
		detalleEstructurasRepository.flush();
	}

	@Override
	public void actualizarEstructura(EstructuraAnetoModel estructura) {
		LOG.info("actualizarEstructura");

		IcsEstructura entity = null;
		Long idEstructura = estructura.getId();
		try {
			entity = estructurasRepository.getOne(idEstructura);
		} catch (EntityNotFoundException e) {
			throw new IcsEstructuraNotFoundException("La estructura a actualizar no existe", e);
		}

		entity.setDescripcionEstructura(estructura.getDescripcion());
		entity.setCardinalidadMaximaEstructura(BigDecimal.valueOf(estructura.getCardMax()));
		entity.setCardinalidadMinimaEstructura(BigDecimal.valueOf(estructura.getCardMin()));
		entity.setCodigoEstructura(estructura.getCodigo());
		
		BooleanBuilder bb = new BooleanBuilder()
				.and(QIcsDetalleEstructura.icsDetalleEstructura.icsEstructura.idEstructura.eq(idEstructura));
		
		detalleEstructurasRepository.delete(
			detalleEstructurasRepository.findAll(bb)
		);

		for (ColumnaMetadatosModel columnaMetadatos : estructura.getColumnas()) {
			IcsDetalleEstructura detalleEntity = this.convertirColumnaMetadatosAIcsDetalleEstructura(columnaMetadatos);
			detalleEntity.setIcsEstructura(entity);
			detalleEstructurasRepository.save(detalleEntity);
		}
		
		estructurasRepository.saveAndFlush(entity);
	}

	private Sort buildSort(String sortColumn, String sortOrder) {

		Sort.Direction dir;
		String entitySortField;

		switch (sortColumn) {
			case "codigo":
				entitySortField = CODIGO_ESTRUCTURA;
				break;
			case "nombre":
				entitySortField = NOMBRE_ESTRUCTURA;
				break;
			default:
				return null;
		}

		if ("desc".equalsIgnoreCase(sortOrder)) {
			dir = Sort.Direction.DESC;
		} else {
			dir = Sort.Direction.ASC;
		}

		return new Sort(dir, entitySortField);
	}

	@Override
	public void activarEstructura(long idEstructura) {
		LOG.info("activarEstructura");

		IcsEstructura entity = null;
		try {
			entity = estructurasRepository.findOne(idEstructura);
		} catch (EntityNotFoundException e) {
			throw new IcsEstructuraNotFoundException("IcsEstructura ID: [" + idEstructura + "]", e);
		}

		entity.setVigenciaEstructura("Y");
		estructurasRepository.saveAndFlush(entity);
	}

	@Override
	public void desactivarEstructura(long idEstructura) {
		LOG.info("desactivarEstructura");

		IcsEstructura entity = null;
		try {
			entity = estructurasRepository.findOne(idEstructura);
		} catch (EntityNotFoundException e) {
			throw new IcsEstructuraNotFoundException("IcsEstructura ID: [" + idEstructura + "]", e);
		}

		Map<String, Object> map = new HashMap<>();
		map.put("PN_IDESTRUCTURA", idEstructura);
		map.put("PN_TIPOESTRUCTURA", 1);

		RequestResultOutput result = null;
		SimpleJdbcCall call = new SimpleJdbcCall(jdbcTemplate).withCatalogName("PCK_UTIL_APP")
				.withProcedureName("PRC_VALIDAR_ESTRUCTURAS")
				.declareParameters(new SqlParameter("PN_IDESTRUCTURA", oracle.jdbc.OracleTypes.NUMBER),
						new SqlParameter("PN_TIPOESTRUCTURA", oracle.jdbc.OracleTypes.NUMBER));

		Map<String, Object> execute = call.execute(new MapSqlParameterSource(map));
		result = new RequestResultOutput(execute);

		if (result.getCode() == 1) {
			throw new IcsEstructuraServiceException(result.getMessage());
		}

		entity.setVigenciaEstructura("N");
		estructurasRepository.saveAndFlush(entity);
	}

	@Override
	public EstructuraOutput getEstructura(long idEstructura) {
		LOG.info("getEstructura");

		IcsEstructura entity = null;
		try {
			entity = estructurasRepository.findOne(idEstructura);
		} catch (EntityNotFoundException e) {
			throw new IcsEstructuraNotFoundException("Estructura no encontrada", e);
		}

		if (entity != null) {
			QIcsDetalleEstructura qIcsDetalleEstructura = QIcsDetalleEstructura.icsDetalleEstructura;
			
			EstructuraOutput transformada = conversionService.convert(entity, EstructuraOutput.class);

			BooleanBuilder detallesDeEstructura = new BooleanBuilder()
					.and(qIcsDetalleEstructura.icsEstructura.idEstructura.eq(idEstructura));
			
			OrderSpecifier<BigDecimal> ordenAscendente = qIcsDetalleEstructura.orden.asc();
			
			Iterable<IcsDetalleEstructura> iteraDetalles = detalleEstructurasRepository.findAll(detallesDeEstructura, ordenAscendente);
			
			List<DetalleEstructuraOutput> detalles = new ArrayList<>();
			for (IcsDetalleEstructura icsDetalle : iteraDetalles) {
				DetalleEstructuraOutput detalle = conversionService.convert(icsDetalle, DetalleEstructuraOutput.class);
				detalles.add(detalle);
			}
			transformada.setColumnas(detalles);

			return transformada;
		}

		return null;
	}

	@Override
	public OutputListWrapper<EstructuraOutput> getEstructuras(int page, int size, String sortColumn, String sortOrder,
			Map<String, String> filters) {

		Pageable paginator;

		if (sortColumn.isEmpty()) {
			paginator = new PageRequest(page, size);
		} else {
			Sort sorter = buildSort(sortColumn, sortOrder);
			paginator = new PageRequest(page, size, sorter);
		}

		Iterable<IcsEstructura> pagina;
		long countEstructuras;
		if (filters != null) {
			BooleanBuilder queryDslFilters = this.generateQueryFilter(filters);
			countEstructuras = estructurasRepository.count(queryDslFilters);
			pagina = estructurasRepository.findAll(queryDslFilters, paginator);
		} else {
			countEstructuras = estructurasRepository.count();
			pagina = estructurasRepository.findAll(paginator);
		}
		
		List<EstructuraOutput> lista = new ArrayList<>();
		for (IcsEstructura icsEstructura : pagina) {
			EstructuraOutput estructura = conversionService.convert(icsEstructura, EstructuraOutput.class);
			lista.add(estructura);
		}

		return new OutputListWrapper<>(lista, (int) countEstructuras, "");
	}

	private BooleanBuilder generateQueryFilter(Map<String, String> filters) {
		LOG.info("generateQueryFilter");
		
		String filterKeyNombre = "nombre";
		String filterKeyCodigo = "codigo";
		
		QIcsEstructura queryDslRef = QIcsEstructura.icsEstructura;

		BooleanBuilder bb = new BooleanBuilder();
		if (filters.containsKey(filterKeyNombre) && !filters.get(filterKeyNombre).trim().isEmpty()) {
			bb.and(queryDslRef.descripcionEstructura.equalsIgnoreCase(filters.get(filterKeyNombre)));
		}
		if (filters.containsKey(filterKeyCodigo) && !filters.get(filterKeyCodigo).trim().isEmpty()) {
			bb.and(queryDslRef.codigoEstructura.equalsIgnoreCase(filters.get(filterKeyCodigo)));
		}

		return bb;
	}

	@Override
	public OutputListWrapper<EstructuraOutput> getEstructurasAll() {
		List<IcsEstructura> iteraEstructuras = estructurasRepository.findAll();
		long countEstructuras = estructurasRepository.count();

		List<EstructuraOutput> estructuras = new ArrayList<>();
		for (IcsEstructura icsEstructura : iteraEstructuras) {
			EstructuraOutput estructura = conversionService.convert(icsEstructura, EstructuraOutput.class);
			estructuras.add(estructura);
		}

		return new OutputListWrapper<>(estructuras, (int) countEstructuras, "");
	}

	@Override
	public List<EstructuraSimpleOutput> getEstructurasSimples() {

		Iterable<IcsEstructura> iteraEstructuras = estructurasRepository.findAll();
		
		List<EstructuraSimpleOutput> estructuras = new ArrayList<>();
		for (IcsEstructura icsEstructura : iteraEstructuras) {
			EstructuraSimpleOutput estructura = conversionService.convert(icsEstructura, EstructuraSimpleOutput.class);
			estructuras.add(estructura);
		}

		return estructuras;
	}


	@Override
	public boolean existeEstructura(EstructuraInput estructura) {
		String codigo = estructura.getCodigo();
		String descripcion = estructura.getDescripcion();
		
		QIcsEstructura qIcsSub = QIcsEstructura.icsEstructura;
		//si se encuentra coincidencia de cualqueir de ambos datos
		BooleanBuilder bb = new BooleanBuilder()
				.andAnyOf(
					qIcsSub.codigoEstructura.eq(codigo), 
					qIcsSub.descripcionEstructura.eq(descripcion)
				);
		long count = estructurasRepository.count(bb);
		return (count > 0);
	}
}
