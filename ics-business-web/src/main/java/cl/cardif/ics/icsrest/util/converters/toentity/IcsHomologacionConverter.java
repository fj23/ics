package cl.cardif.ics.icsrest.util.converters.toentity;

import cl.cardif.ics.domain.entity.homologaciones.IcsDetalleHomologacion;
import cl.cardif.ics.domain.entity.homologaciones.QIcsDetalleHomologacion;
import cl.cardif.ics.icsrest.domain.pojo.Homologacion;
import cl.cardif.ics.icsrest.repository.DetallesHomologacionesRepository;
import com.querydsl.core.BooleanBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;

@Component
public class IcsHomologacionConverter implements Converter<Homologacion, IcsDetalleHomologacion> {

  static final Logger LOG = LoggerFactory.getLogger(IcsHomologacionConverter.class);

  @Autowired private DetallesHomologacionesRepository detalleHomologacionesRepository;

  @Override
  public IcsDetalleHomologacion convert(Homologacion pojo) {

    LOG.info("Conversion Detalle Homologacion");
    BooleanBuilder booleanBuilder = new BooleanBuilder();
    QIcsDetalleHomologacion qIcsDetalleHomologacion =
        QIcsDetalleHomologacion.icsDetalleHomologacion;

    booleanBuilder.and(qIcsDetalleHomologacion.idHomologacion.eq(pojo.getId()));

    IcsDetalleHomologacion entity = null;
    try {
      entity = this.detalleHomologacionesRepository.findOne(booleanBuilder);
    } catch (EntityNotFoundException e) {
      throw e;
    }

    if (entity != null
        && ((entity.getIcsTipoHomologacion().getIdDeTipoHomologacion() == pojo.getIdTipo())
            || pojo.getIdTipo() == 0)) {
       entity.setValorSocio(pojo.getValor1());
      entity.setValorCore(pojo.getValor2());
      return entity;
    }

    return null;
  }
}
