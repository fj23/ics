package cl.cardif.ics.icsrest.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

import cl.cardif.ics.domain.entity.homologaciones.IcsTipoHomologacion;

@Repository
public interface TiposHomologacionRepository 
	extends JpaRepository<IcsTipoHomologacion, Long>,
		QueryDslPredicateExecutor<IcsTipoHomologacion> {

	@Query("SELECT distinct(i.nombreHomologacion) as nombre FROM IcsTipoHomologacion i order by nombre asc")
	List<String> findDistinctConceptos();
}
