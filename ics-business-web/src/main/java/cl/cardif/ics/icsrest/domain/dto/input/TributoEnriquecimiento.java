package cl.cardif.ics.icsrest.domain.dto.input;

public abstract class TributoEnriquecimiento {
  protected Long idAtributoEnriquecimiento;
  protected Long idCore;
  protected String nombreColumna;
  protected String nombreTabla;

  public Long getIdAtributoEnriquecimiento() {
    return idAtributoEnriquecimiento;
  }

  public void setIdAtributoEnriquecimiento(Long idAtributoEnriquecimiento) {
    this.idAtributoEnriquecimiento = idAtributoEnriquecimiento;
  }

  public Long getIdCore() {
    return idCore;
  }

  public void setIdCore(Long idCore) {
    this.idCore = idCore;
  }

  public String getNombreColumna() {
    return nombreColumna;
  }

  public void setNombreColumna(String nombreColumna) {
    this.nombreColumna = nombreColumna;
  }

  public String getNombreTabla() {
    return nombreTabla;
  }

  public void setNombreTabla(String nombreTabla) {
    this.nombreTabla = nombreTabla;
  }
}
