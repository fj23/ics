package cl.cardif.ics.icsrest.domain.dto.input;

public class ColumnaArchivoSalidaIntegracionInput extends ColumnaInput {

	private Long posicionInicial;
	private Long posicionFinal;
	private Boolean separarArchivo;
	private Boolean incluirEnArchivo;
	private String flgVisible;
	private Long indiceMapeoOrigen;

	public ColumnaArchivoSalidaIntegracionInput() {
		super();
	}

	public Long getPosicionInicial() {
		return posicionInicial;
	}

	public void setPosicionInicial(Long posicionInicial) {
		this.posicionInicial = posicionInicial;
	}

	public Long getPosicionFinal() {
		return posicionFinal;
	}

	public void setPosicionFinal(Long posicionFinal) {
		this.posicionFinal = posicionFinal;
	}

	public Boolean getSepararArchivo() {
		return separarArchivo;
	}

	public void setSepararArchivo(Boolean separarArchivo) {
		this.separarArchivo = separarArchivo;
	}

	public Boolean getIncluirEnArchivo() {
		return incluirEnArchivo;
	}

	public void setIncluirEnArchivo(Boolean incluirEnArchivo) {
		this.incluirEnArchivo = incluirEnArchivo;
	}

	public String getFlgVisible() {
		return flgVisible;
	}

	public void setFlgVisible(String flgVisible) {
		this.flgVisible = flgVisible;
	}

	public Long getIndiceMapeoOrigen() {
		return indiceMapeoOrigen;
	}

	public void setIndiceMapeoOrigen(Long indiceMapeoOrigen) {
		this.indiceMapeoOrigen = indiceMapeoOrigen;
	}

	@Override
	public String toString() {
		return "ColumnaArchivoSalidaIntegracionInput [posicionInicial=" + posicionInicial + ", posicionFinal="
				+ posicionFinal + ", separarArchivo=" + separarArchivo + ", incluirEnArchivo=" + incluirEnArchivo
				+ ", flgVisible=" + flgVisible + "]";
	}


	
}
