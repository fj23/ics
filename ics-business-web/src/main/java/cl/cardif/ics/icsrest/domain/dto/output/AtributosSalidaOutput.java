package cl.cardif.ics.icsrest.domain.dto.output;

import cl.cardif.ics.icsrest.domain.dto.input.ColumnaMetadatosSalidaModel;
import cl.cardif.ics.icsrest.domain.pojo.Archivo;

public class AtributosSalidaOutput {
	private Archivo archivoEM;
	private AtributoUsuarioOutput columnaOrigenEM;
	private ColumnaMetadatosSalidaModel atributo;

	public AtributosSalidaOutput() {
		super();
	}

	public Archivo getArchivoEM() {
		return archivoEM;
	}

	public void setArchivoEM(Archivo archivoEM) {
		this.archivoEM = archivoEM;
	}

	public AtributoUsuarioOutput getColumnaOrigenEM() {
		return columnaOrigenEM;
	}

	public void setColumnaOrigenEM(AtributoUsuarioOutput columnaOrigenEM) {
		this.columnaOrigenEM = columnaOrigenEM;
	}

	public ColumnaMetadatosSalidaModel getAtributo() {
		return atributo;
	}

	public void setAtributo(ColumnaMetadatosSalidaModel atributo) {
		this.atributo = atributo;
	}
}
