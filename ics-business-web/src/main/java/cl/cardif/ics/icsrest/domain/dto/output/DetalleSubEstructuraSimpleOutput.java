package cl.cardif.ics.icsrest.domain.dto.output;

public class DetalleSubEstructuraSimpleOutput {

	private long id;
	private String descripcion;

	public DetalleSubEstructuraSimpleOutput() {
		super();
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
}
