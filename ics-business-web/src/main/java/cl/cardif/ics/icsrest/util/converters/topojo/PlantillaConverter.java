package cl.cardif.ics.icsrest.util.converters.topojo;

import java.text.SimpleDateFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import cl.cardif.ics.domain.entity.plantillas.IcsPlantilla;
import cl.cardif.ics.icsrest.domain.pojo.Plantilla;
import cl.cardif.ics.icsrest.util.Constants;

@Component
public class PlantillaConverter implements Converter<IcsPlantilla, Plantilla> {
	private static final Logger LOG = LoggerFactory.getLogger(PlantillaConverter.class);
	
	@Override
	public Plantilla convert(IcsPlantilla entity) {
		LOG.info("convert");

		Plantilla convertida = new Plantilla();
		convertida.setId(entity.getIdPlantilla());
		convertida.setNombre(entity.getNombrePlantilla());
		convertida.setPlantillaMultiarchivo(entity.getPlantillaMultiarchivo());
		convertida.setVersion(entity.getVersionPlantilla());
		convertida.setVigencia(entity.getVigencia());
		convertida.setObservaciones(entity.getObservacionPlantilla());
		
		SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_DEFAULT_FRONTEND_FORMAT);
		String fechaCreacion = sdf.format(entity.getFechaCreacion());
		convertida.setFechaCreacion(fechaCreacion);

		return convertida;
	}
}
