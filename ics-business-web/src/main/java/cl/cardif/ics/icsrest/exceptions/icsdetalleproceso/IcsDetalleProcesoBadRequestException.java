package cl.cardif.ics.icsrest.exceptions.icsdetalleproceso;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class IcsDetalleProcesoBadRequestException extends RuntimeException {

  private static final long serialVersionUID = -6459654653826812166L;

  public IcsDetalleProcesoBadRequestException(String exception) {
    super(exception);
  }

  public IcsDetalleProcesoBadRequestException(String exception, Throwable t) {
    super(exception, t);
  }
}
