package cl.cardif.ics.icsrest.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cl.cardif.ics.icsrest.domain.dto.input.BaseColumnaModel;
import cl.cardif.ics.icsrest.domain.dto.input.EstructuraAnetoModel;
import cl.cardif.ics.icsrest.domain.dto.input.EstructuraInput;
import cl.cardif.ics.icsrest.domain.dto.output.EstructuraOutput;
import cl.cardif.ics.icsrest.domain.dto.output.EstructuraSimpleOutput;
import cl.cardif.ics.icsrest.domain.dto.output.SubEstructuraSimpleOutput;
import cl.cardif.ics.icsrest.exceptions.icsestructura.IcsEstructuraNotFoundException;
import cl.cardif.ics.icsrest.exceptions.icssubestructura.IcsSubestructuraNotFoundException;
import cl.cardif.ics.icsrest.service.EstructurasService;
import cl.cardif.ics.icsrest.service.SubestructurasService;
import cl.cardif.ics.icsrest.util.Constants;
import cl.cardif.ics.icsrest.util.OutputListWrapper;

@RestController
@RequestMapping("/api/estructuras")
public class EstructurasController {
	private static final Logger LOG = LoggerFactory.getLogger(EstructurasController.class);
	
	@Autowired EstructurasService estructurasService;
	@Autowired SubestructurasService subEstructurasService;

	@PostMapping("/nueva")
	public ResponseEntity<Void> almacenarEstructura(@RequestBody EstructuraAnetoModel estructura) {
		LOG.info("almacenarEstructura");
		estructurasService.almacenarEstructura(estructura);
		return ResponseEntity.ok().build();
	}

	@PutMapping("/actualizar")
	public ResponseEntity<Void> actualizarEstructura(@RequestBody EstructuraAnetoModel estructura) {
		LOG.info("actualizarEstructura");
		estructurasService.actualizarEstructura(estructura);
		return ResponseEntity.ok().build();
	}

	@GetMapping("/activar/{idEstructura}")
	public ResponseEntity<Void> activarEstructura(@PathVariable long idEstructura) {
		LOG.info("activarEstructura");
		estructurasService.activarEstructura(idEstructura);
		return ResponseEntity.ok().build();
	}

	@GetMapping("/desactivar/{idEstructura}")
	public ResponseEntity<Void> desactivarEstructura(@PathVariable long idEstructura) {
		LOG.info("desactivarEstructura");
		estructurasService.desactivarEstructura(idEstructura);
		return ResponseEntity.noContent().build();
	}

	@GetMapping("/get/{id}")
	public ResponseEntity<EstructuraOutput> getEstructura(@PathVariable long id) {
		LOG.info("getEstructura");
		EstructuraOutput entity = estructurasService.getEstructura(id);
		return ResponseEntity.ok(entity);
	}

	@GetMapping("/")
	public OutputListWrapper<EstructuraOutput> getEstructuras() {
		return this.getEstructuras(Constants.DEFAULT_PAGE, Constants.DEFAULT_PAGE_SIZE, null);
	}

	@GetMapping("/page/{page}")
	public OutputListWrapper<EstructuraOutput> getEstructuras(@PathVariable int page) {
		return this.getEstructuras(page, Constants.DEFAULT_PAGE_SIZE, null);
	}

	@GetMapping("/page/{page}/size/{size}")
	public OutputListWrapper<EstructuraOutput> getEstructuras(@PathVariable int page, @PathVariable int size) {
		return this.getEstructuras(page, size, null);
	}

	@GetMapping("/filters")
	public OutputListWrapper<EstructuraOutput> getEstructuras(@RequestParam Map<String, String> filters) {
		return this.getEstructuras(Constants.DEFAULT_PAGE, Constants.DEFAULT_PAGE_SIZE, filters);
	}

	@GetMapping("/page/{page}/size/{size}/filters")
	public OutputListWrapper<EstructuraOutput> getEstructuras(
		@PathVariable int page, @PathVariable int size, @RequestParam Map<String, String> filters) {
		return estructurasService.getEstructuras(page, size, "", "", filters);
	}

	@GetMapping("/page/{page}/size/{size}/sort/{sortColumn}/{sortOrder}/filters")
	public ResponseEntity<OutputListWrapper<EstructuraOutput>> getEstructuras(
		@PathVariable int page, @PathVariable int size, @PathVariable String sortColumn, @PathVariable String sortOrder,
		@RequestParam Map<String, String> filters) {
		LOG.info("getEstructuras");
		OutputListWrapper<EstructuraOutput> estructuras = estructurasService.getEstructuras(page, size, sortColumn, sortOrder, filters);
		return ResponseEntity.ok(estructuras);
	}

	@GetMapping("/all")
	public OutputListWrapper<BaseColumnaModel> getEstructurasAll() {
		LOG.info("getEstructurasAll");
		List<BaseColumnaModel> lista = new ArrayList<>();

		try {
			OutputListWrapper<EstructuraOutput> estructuras = estructurasService.getEstructurasAll();
			lista.addAll(estructuras.getItems());
		} catch (Exception exc) {
			LOG.error("Error al añadir estructuras al listado.", exc);
		}

		try {
			List<SubEstructuraSimpleOutput> subestructuras = subEstructurasService.getSubestructurasSimple();
			lista.addAll(subestructuras);
		} catch (Exception exc) {
			LOG.error("Error al añadir subestructuras al listado.", exc);
		}

		return new OutputListWrapper<>(lista, "");
	}

	@PutMapping("/exists")
	public boolean existeEstructura(@RequestBody EstructuraInput estructura) {
		LOG.info("existeEstructura");
		return estructurasService.existeEstructura(estructura);
	}

	@GetMapping("/simple")
	public ResponseEntity<Collection<EstructuraSimpleOutput>> getEstructurasSimple() {
		LOG.info("getEstructurasSimple");
		Collection<EstructuraSimpleOutput> lista = estructurasService.getEstructurasSimples();
		return ResponseEntity.ok(lista);
	}

	@ExceptionHandler({ IcsEstructuraNotFoundException.class })
	public ResponseEntity<String> exceptionHandler(IcsEstructuraNotFoundException e) {
		return ResponseEntity.notFound().build();
	}

	@ExceptionHandler({ IcsSubestructuraNotFoundException.class })
	public ResponseEntity<String> exceptionHandler(IcsSubestructuraNotFoundException e) {
		return ResponseEntity.notFound().build();
	}

	@ExceptionHandler({ Exception.class })
	public ResponseEntity<String> exceptionHandler(Exception e) {
		return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
