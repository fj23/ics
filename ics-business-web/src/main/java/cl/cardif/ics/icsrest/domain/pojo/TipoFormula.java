package cl.cardif.ics.icsrest.domain.pojo;

import cl.cardif.ics.icsrest.domain.dto.input.BaseModel;

public class TipoFormula extends BaseModel {

	private String estructura;
	private String tipoDatoResultante;

	public TipoFormula() {
		super();
	}

	public String getEstructura() {
		return this.estructura;
	}

	public void setEstructura(String estructura) {
		this.estructura = estructura;
	}

	public String getTipoDatoResultante() {
		return this.tipoDatoResultante;
	}

	public void setTipoDatoResultante(String tipoDatoResultante) {
		this.tipoDatoResultante = tipoDatoResultante;
	}
}
