package cl.cardif.ics.icsrest.domain.dto.output;

import java.util.ArrayList;
import java.util.List;

import cl.cardif.ics.icsrest.domain.dto.input.ColumnaMetadatosSalidaModel;
import cl.cardif.ics.icsrest.domain.dto.input.MapeoEntradaInput;
import cl.cardif.ics.icsrest.domain.dto.input.MapeoIntegracionInput;
import cl.cardif.ics.icsrest.domain.dto.output.integracion.ColumnaArchivoSalidaIntegracionOutput;
import cl.cardif.ics.icsrest.domain.dto.output.integracion.ColumnaTrabajoIntgOutput;
import cl.cardif.ics.icsrest.domain.pojo.Core;
import cl.cardif.ics.icsrest.domain.pojo.Evento;
import cl.cardif.ics.icsrest.domain.pojo.Socio;
import cl.cardif.ics.icsrest.domain.pojo.TipoPlantilla;
import cl.cardif.ics.icsrest.domain.pojo.TipoSalida;

public class PlantillaOutput {
	private Long id;
	private String nombre;
	private String vigencia;
	private Core core;
	private Evento evento;
	private Socio socio;
	private Socio socioDestino;
	private TipoSalida tipoSalida;
	private String observaciones;
	private String plantillaMultiarchivo;
	private Long version;
	private String codUsuario;
	private TipoPlantilla tipo;
	private String fechaCreacion;
	private List<MapeoEntradaInput> columnasDestino;
	private List<ColumnaTrabajoIntgOutput> columnasTrabajo;
	private List<MapeoIntegracionInput> mapeosIntegracion;
	private List<ColumnaArchivoSalidaIntegracionOutput> columnasArchivoSalida;
	private List<ColumnaMetadatosSalidaModel> columnas;
	private List<EstructuraAnetoModelOutput> estructuras;
	private Boolean incluirEncabezados;

	public PlantillaOutput() {
		super();
	}

	public PlantillaOutput(long id) {
		super();
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getVigencia() {
		return vigencia;
	}

	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}

	public Core getCore() {
		return core;
	}

	public void setCore(Core core) {
		this.core = core;
	}

	public Evento getEvento() {
		return evento;
	}

	public void setEvento(Evento evento) {
		this.evento = evento;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getPlantillaMultiarchivo() {
		return plantillaMultiarchivo;
	}

	public void setPlantillaMultiarchivo(String plantillaMultiarchivo) {
		this.plantillaMultiarchivo = plantillaMultiarchivo;
	}

	public Socio getSocio() {
		return socio;
	}

	public void setSocio(Socio socio) {
		this.socio = socio;
	}

	public Socio getSocioDestino() {
		return socioDestino;
	}

	public void setSocioDestino(Socio socioDestino) {
		this.socioDestino = socioDestino;
	}

	public TipoSalida getTipoSalida() {
		return tipoSalida;
	}

	public void setTipoSalida(TipoSalida tipoSalida) {
		this.tipoSalida = tipoSalida;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public String getCodUsuario() {
		return codUsuario;
	}

	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}

	public List<ColumnaMetadatosSalidaModel> getColumnas() {
		if (this.columnas != null) {
			return new ArrayList<>(this.columnas);
		} else {
			return new ArrayList<>();
		}
	}

	public void setColumnas(List<ColumnaMetadatosSalidaModel> columnas) {
		if (columnas != null) {
			this.columnas = new ArrayList<>(columnas);
		}
	}

	public List<ColumnaArchivoSalidaIntegracionOutput> getColumnasArchivoSalida() {
		if (this.columnasArchivoSalida != null) {
			return new ArrayList<>(this.columnasArchivoSalida);
		} else {
			return new ArrayList<>();
		}
	}

	public void setColumnasArchivoSalida(List<ColumnaArchivoSalidaIntegracionOutput> columnasArchivoSalida) {
		if (columnasArchivoSalida != null) {
			this.columnasArchivoSalida = new ArrayList<>(columnasArchivoSalida);
		}
	}

	public List<MapeoEntradaInput> getColumnasDestino() {
		if (this.columnasDestino != null) {
			return new ArrayList<>(this.columnasDestino);
		} else {
			return new ArrayList<>();
		}
	}

	public void setColumnasDestino(List<MapeoEntradaInput> columnasDestino) {
		if (columnasDestino != null) {
			this.columnasDestino = new ArrayList<>(columnasDestino);
		}
	}

	public List<ColumnaTrabajoIntgOutput> getColumnasTrabajo() {
		if (this.columnasTrabajo != null) {
			return new ArrayList<>(this.columnasTrabajo);
		} else {
			return new ArrayList<>();
		}
	}

	public void setColumnasTrabajo(List<ColumnaTrabajoIntgOutput> columnasTrabajo) {
		if (columnasTrabajo != null) {
			this.columnasTrabajo = new ArrayList<>(columnasTrabajo);
		}
	}

	public List<EstructuraAnetoModelOutput> getEstructuras() {
		if (this.estructuras != null) {
			return new ArrayList<>(this.estructuras);
		} else {
			return new ArrayList<>();
		}
	}

	public void setEstructuras(List<EstructuraAnetoModelOutput> estructuras) {
		if (estructuras != null) {
			this.estructuras = new ArrayList<>(estructuras);
		}
	}

	public String getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(String fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public List<MapeoIntegracionInput> getMapeosIntegracion() {
		if (this.mapeosIntegracion != null) {
			return new ArrayList<>(this.mapeosIntegracion);
		}
		else {
			return new ArrayList<>();
		}
	}

	public void setMapeosIntegracion(List<MapeoIntegracionInput> mapeosIntegracion) {
		if (mapeosIntegracion != null) {
			this.mapeosIntegracion = new ArrayList<>(mapeosIntegracion);
		}
	}

	public TipoPlantilla getTipo() {
		return tipo;
	}

	public void setTipo(TipoPlantilla tipo) {
		this.tipo = tipo;
	}

	public Boolean getIncluirEncabezados() {
		return incluirEncabezados;
	}

	public void setIncluirEncabezados(Boolean incluirEncabezados) {
		this.incluirEncabezados = incluirEncabezados;
	}

	@Override
	public String toString() {
		return "PlantillaOutput [id=" + id + ", nombre=" + nombre + ", vigencia=" + vigencia + ", core=" + core + ", evento=" + evento
			+ ", socio=" + socio + ", socioDestino=" + socioDestino + ", tipoSalida=" + tipoSalida + ", observaciones=" + observaciones
			+ ", plantillaMultiarchivo=" + plantillaMultiarchivo + ", version=" + version + ", codUsuario=" + codUsuario + ", tipo=" + tipo
			+ ", fechaCreacion=" + fechaCreacion + ", columnasDestino=" + columnasDestino + ", columnasTrabajo=" + columnasTrabajo
			+ ", mapeosIntegracion=" + mapeosIntegracion + ", columnasArchivoSalida=" + columnasArchivoSalida + ", columnas=" + columnas
			+ ", estructuras=" + estructuras + ", incluirEncabezados=" + incluirEncabezados + "]";
	}
}
