package cl.cardif.ics.icsrest.domain.pojo;

public class TipoArchivo {

  private long id;
  private String nombre;
  private String vigencia;

  public TipoArchivo() {
    // Constructor JavaBean
  }

  public long getId() {
    return this.id;
  }

  public void setId(long idTipoDato) {
    this.id = idTipoDato;
  }

  public String getNombre() {
    return this.nombre;
  }

  public void setNombre(String nombreTipoDato) {
    this.nombre = nombreTipoDato;
  }

	public String getVigencia() {
		return vigencia;
	}
	
	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}
}
