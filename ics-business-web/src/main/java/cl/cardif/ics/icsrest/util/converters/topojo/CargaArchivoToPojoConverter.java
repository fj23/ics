package cl.cardif.ics.icsrest.util.converters.topojo;

import cl.cardif.ics.domain.entity.carga.IcsCargaArchivo;
import cl.cardif.ics.icsrest.domain.dto.input.CargaArchivoInput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class CargaArchivoToPojoConverter implements Converter<IcsCargaArchivo, CargaArchivoInput> {
  static final Logger LOG = LoggerFactory.getLogger(CargaArchivoToPojoConverter.class);

  @Override
  public CargaArchivoInput convert(IcsCargaArchivo entity) {

    LOG.info("CARGA ARCHIVO A CONVERTIR:\n\n" + entity.toString());

    CargaArchivoInput cargaArchivo = new CargaArchivoInput();

    cargaArchivo.setId(entity.getIdCarga());
    cargaArchivo.setNombre(entity.getNombreCarga());
    cargaArchivo.setFechaRecepcion(entity.getFechaRecepcionCarga());
    cargaArchivo.setCodigo(entity.getCodigoCarga());
    cargaArchivo.setObservacion(entity.getObservacionCarga());
    cargaArchivo.setNombreArchivo(entity.getNombreArchivo());
    cargaArchivo.setExtensionArchivo(entity.getExtensionArchivo());
    cargaArchivo.setIdProceso(entity.getIdProceso());

    return cargaArchivo;
  }
}
