package cl.cardif.ics.icsrest.formula.interfaces;

import java.util.Collection;

public interface IFormulaParent extends IFormulaFactor {
	int maxHijos = Integer.MAX_VALUE;

	String childrenToJSON();

	Collection<? extends IFormulaFactor> getHijos();

	void setHijos(Collection<? extends IFormulaFactor> hijos);
}
