package cl.cardif.ics.icsrest.exceptions.icsdetallehomologacion;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class IcsDetalleHomologacionForbiddenException extends RuntimeException {

  private static final long serialVersionUID = 3397992475249736227L;

  public IcsDetalleHomologacionForbiddenException(String exception) {
    super(exception);
  }

  public IcsDetalleHomologacionForbiddenException(String exception, Throwable t) {
    super(exception, t);
  }
}
