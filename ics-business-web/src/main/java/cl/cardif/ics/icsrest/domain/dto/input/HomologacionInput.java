package cl.cardif.ics.icsrest.domain.dto.input;

public class HomologacionInput {

  private long idHomologacion;
  private long idTipo;
  private long socio1;
  private long socio2;
  private long core1;
  private long core2;
  private String nombreHomologacion;
  private String vigenciaHomologacion;

  public HomologacionInput() {
    // Constructor JavaBean
  }

  public long getCore1() {
    return core1;
  }

  public void setCore1(long idCore1) {
    this.core1 = idCore1;
  }

  public long getCore2() {
    return core2;
  }

  public void setCore2(long idCore2) {
    this.core2 = idCore2;
  }

  public long getIdHomologacion() {
    return idHomologacion;
  }

  public void setIdHomologacion(long idHomologacion) {
    this.idHomologacion = idHomologacion;
  }

  public long getIdTipo() {
    return idTipo;
  }

  public void setIdTipo(long idTipo) {
    this.idTipo = idTipo;
  }

  public long getIdTipoHomologacion() {
    return idHomologacion;
  }

  public void setIdTipoHomologacion(long idTipoHomologacion) {
    this.idHomologacion = idTipoHomologacion;
  }

  public String getNombreHomologacion() {
    return nombreHomologacion;
  }

  public void setNombreHomologacion(String nombreHomologacion) {
    this.nombreHomologacion = nombreHomologacion;
  }

  public long getSocio1() {
    return socio1;
  }

  public void setSocio1(long idSocio1) {
    this.socio1 = idSocio1;
  }

  public long getSocio2() {
    return socio2;
  }

  public void setSocio2(long idSocio2) {
    this.socio2 = idSocio2;
  }

  public String getVigenciaHomologacion() {
    return vigenciaHomologacion;
  }

  public void setVigenciaHomologacion(String vigenciaHomologacion) {
    this.vigenciaHomologacion = vigenciaHomologacion;
  }
}
