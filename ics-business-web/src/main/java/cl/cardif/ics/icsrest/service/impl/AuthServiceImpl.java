package cl.cardif.ics.icsrest.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.cardif.ics.icsrest.message.response.AuthLoginResponse;
import cl.cardif.ics.icsrest.security.jwt.JwtTokenProvider;
import cl.cardif.ics.icsrest.service.AuthService;
import cl.cardif.ics.icsrest.util.RolesUtil;

@Service
public class AuthServiceImpl implements AuthService {
	private static final Logger LOG = LoggerFactory.getLogger(AuthServiceImpl.class);
	RolesUtil rolesUtil = new RolesUtil();
	
	@Autowired private JwtTokenProvider jwtTokenProvider;

	@Override
	public String signin(AuthLoginResponse authLoginResponse) {
		String token = this.jwtTokenProvider.createToken(authLoginResponse);
		LOG.debug("Se genera para usuario {} token {}", authLoginResponse.getUsername(), token);
		return token;
	}
}
