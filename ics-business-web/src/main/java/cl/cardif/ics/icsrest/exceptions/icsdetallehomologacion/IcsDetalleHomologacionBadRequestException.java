package cl.cardif.ics.icsrest.exceptions.icsdetallehomologacion;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class IcsDetalleHomologacionBadRequestException extends RuntimeException {

  private static final long serialVersionUID = -6459654653826812166L;

  public IcsDetalleHomologacionBadRequestException(String exception) {
    super(exception);
  }

  public IcsDetalleHomologacionBadRequestException(String exception, Throwable t) {
    super(exception, t);
  }
}
