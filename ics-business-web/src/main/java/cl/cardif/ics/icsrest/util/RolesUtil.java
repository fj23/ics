package cl.cardif.ics.icsrest.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import cl.cardif.ics.icsrest.domain.pojo.DetalleRol;
import cl.cardif.ics.icsrest.domain.pojo.TipoRol;

@Component
public class RolesUtil {
	static final Logger LOG = LoggerFactory.getLogger(RolesUtil.class);

	public Map<Long, DetalleRol> createRichDetalleRolMapFromTipoRolList(List<TipoRol> roles) {
		Map<Long, DetalleRol> map = new HashMap<Long, DetalleRol>();
		LOG.info("INICIO createRichDetalleRolMapFromTipoRolList");
		for (TipoRol rol : roles) {
			for (Entry<Long, DetalleRol> entry : rol.getDetalles().entrySet()) {
				map.put(entry.getValue().getCodigo(), entry.getValue());
			}
		}
		LOG.info("FINAL createRichDetalleRolMapFromTipoRolList");
		return map;
	}

	@SuppressWarnings("unchecked")
	public boolean hasRole(String role) {
		Collection<GrantedAuthority> authorities = (Collection<GrantedAuthority>) SecurityContextHolder.getContext()
				.getAuthentication().getAuthorities();
		boolean hasRole = false;
		for (GrantedAuthority authority : authorities) {
			hasRole = authority.getAuthority().equals(role);
			if (hasRole) {
				break;
			}
		}
		return hasRole;
	}
	
}
