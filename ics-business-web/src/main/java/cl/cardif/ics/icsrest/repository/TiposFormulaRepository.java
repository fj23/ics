package cl.cardif.ics.icsrest.repository;

import cl.cardif.ics.domain.entity.common.IcsTipoFormula;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface TiposFormulaRepository
    extends JpaRepository<IcsTipoFormula, Long>, 
    	QueryDslPredicateExecutor<IcsTipoFormula> {
	
}
