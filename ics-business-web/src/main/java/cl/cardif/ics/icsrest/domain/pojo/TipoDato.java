package cl.cardif.ics.icsrest.domain.pojo;

public class TipoDato {

  private long id;
  private String nombre;

  public TipoDato() {
    // Constructor JavaBean
  }

  public long getId() {
    return this.id;
  }

  public void setId(long idTipoDato) {
    this.id = idTipoDato;
  }

  public String getNombre() {
    return this.nombre;
  }

  public void setNombre(String nombreTipoDato) {
    this.nombre = nombreTipoDato;
  }
}
