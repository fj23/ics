package cl.cardif.ics.icsrest.exceptions.icssubestructura;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class IcsSubestructuraForbiddenException extends RuntimeException {

  private static final long serialVersionUID = 3397992475249736227L;

  public IcsSubestructuraForbiddenException(String exception) {
    super(exception);
  }

  public IcsSubestructuraForbiddenException(String exception, Throwable t) {
    super(exception, t);
  }
}
