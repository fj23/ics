package cl.cardif.ics.icsrest.util.converters.topojo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionException;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import cl.cardif.ics.domain.entity.common.IcsTipoFormula;
import cl.cardif.ics.domain.entity.estructuras.IcsDetalleSubestructura;
import cl.cardif.ics.domain.entity.formatos.IcsDetalleFormato;
import cl.cardif.ics.domain.entity.homologaciones.IcsTipoHomologacion;
import cl.cardif.ics.domain.entity.plantillas.IcsAtributoCnfSalida;
import cl.cardif.ics.domain.entity.plantillas.IcsAtributoSalidaEstruc;
import cl.cardif.ics.icsrest.domain.dto.input.DetalleEstructuraPlantillaSalidaInput;
import cl.cardif.ics.icsrest.formula.IFormulaFactor;

@Component
public class DetalleEstructuraPlantillaSalidaInputConverter
		implements Converter<IcsAtributoCnfSalida, DetalleEstructuraPlantillaSalidaInput> {
	static final Logger LOG = LoggerFactory.getLogger(DetalleEstructuraPlantillaSalidaInputConverter.class);

	@Autowired
	ConversionService conversionService;

	@Override
	public DetalleEstructuraPlantillaSalidaInput convert(IcsAtributoCnfSalida input) {
		DetalleEstructuraPlantillaSalidaInput detalle = new DetalleEstructuraPlantillaSalidaInput();
		detalle.setId(input.getIdAtributoAnulacion());
		detalle.setIdDetalleEstructura(input.getIcsDetalleEstructura().getIdDetalleEstructura());

		IcsAtributoSalidaEstruc atrEstruc = input.getIcsAtributoSalidaEstruc();
		String valor = input.getValor();
		if (atrEstruc != null ^ valor != null) { // uno solo de ambos
			if (valor != null) {
				detalle.setValorFijo(valor);
			} else {
				detalle.setIdAtributoDiccionario(atrEstruc.getIdAtributoEst());
			}
		} else {
			throw new ConversionException("El mapeo no posee un origen unico") {

				private static final long serialVersionUID = -2328573483396781177L;
			};
		}

		IcsDetalleSubestructura dtlSubestruc = input.getIcsDetalleSubestructura();
		if (dtlSubestruc != null) {
			detalle.setIdDetalleEstructura(dtlSubestruc.getIdDetalleSubestructura());
		}

		IcsDetalleFormato tpFormato = input.getIcsDetalleFormato();
		if (tpFormato != null) {
			detalle.setIdTipoFormato(input.getIcsDetalleFormato().getIdDetalleFormato());
		}

		IcsTipoHomologacion tpHomologacion = input.getIcsTipoHomologacion();
		if (tpHomologacion != null) {
			detalle.setIdTipoHomologacion(tpHomologacion.getIdDeTipoHomologacion());
		}

		IcsTipoFormula tipoFormula = input.getIcsTipoFormula();
		if (tipoFormula != null) {
			IFormulaFactor formula = conversionService.convert(tipoFormula.getEstructuraFormula(),
					IFormulaFactor.class);
			detalle.setFormula(formula);
		}

		return detalle;
	}
}
