package cl.cardif.ics.icsrest.controller;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cl.cardif.ics.icsrest.domain.dto.input.EstructuraAnetoModel;
import cl.cardif.ics.icsrest.domain.dto.input.SubEstructuraInput;
import cl.cardif.ics.icsrest.domain.dto.output.SubEstructuraOutput;
import cl.cardif.ics.icsrest.domain.dto.output.SubEstructuraSimpleOutput;
import cl.cardif.ics.icsrest.exceptions.icssubestructura.IcsSubestructuraNotFoundException;
import cl.cardif.ics.icsrest.exceptions.icssubestructura.IcsSubestructuraServiceException;
import cl.cardif.ics.icsrest.service.SubestructurasService;
import cl.cardif.ics.icsrest.util.Constants;
import cl.cardif.ics.icsrest.util.OutputListWrapper;

@RestController
@RequestMapping("/api/subestructuras")
public class SubestructurasController {

	static final Logger LOG = LoggerFactory.getLogger(SubestructurasController.class);

	@Autowired SubestructurasService subEstructurasService;

	@GetMapping("/activar/{idSubEstructura}")
	public ResponseEntity<Void> activarSubestructura(@PathVariable long idSubEstructura) {
		LOG.info("activarSubestructura");
		subEstructurasService.activarSubestructura(idSubEstructura);
		return ResponseEntity.ok().build();
	}

	@PutMapping("/actualizar")
	public ResponseEntity<Void> actualizarSubestructura(@RequestBody EstructuraAnetoModel subestructura) {
		LOG.info("actualizarSubestructura");
		subEstructurasService.actualizarSubestructura(subestructura);
		return ResponseEntity.ok().build();
	}

	@PostMapping("/nueva")
	public ResponseEntity<Void> almacenarSubestructura(@RequestBody EstructuraAnetoModel subestructura) {
		LOG.info("almacenarSubestructura");
		subEstructurasService.almacenarSubestructura(subestructura);
		return ResponseEntity.ok().build();
	}

	@GetMapping("/desactivar/{idSubEstructura}")
	public ResponseEntity<Void> desactivarEstructura(@PathVariable long idSubEstructura) throws IcsSubestructuraServiceException {
		LOG.info("desactivarEstructura");
		subEstructurasService.desactivarSubestructura(idSubEstructura);
		return ResponseEntity.noContent().build();
	}

	@GetMapping("/get/{id}")
	public SubEstructuraOutput getSubEstructura(@PathVariable long id) {
		LOG.info("getSubEstructura");
		return subEstructurasService.getSubEstructura(id);
	}

	@GetMapping("/")
	public OutputListWrapper<SubEstructuraOutput> getSubestructuras() {
		return this.getSubestructuras(Constants.DEFAULT_PAGE, Constants.DEFAULT_PAGE_SIZE, "", "", null);
	}

	@GetMapping("/page/{page}")
	public OutputListWrapper<SubEstructuraOutput> getSubestructuras(@PathVariable int page) {
		return this.getSubestructuras(page, Constants.DEFAULT_PAGE_SIZE, "", "", null);
	}

	@GetMapping("/page/{page}/size/{size}")
	public OutputListWrapper<SubEstructuraOutput> getSubestructuras(@PathVariable int page, @PathVariable int size) {
		return this.getSubestructuras(page, size, "", "", null);
	}

	@GetMapping("/filters")
	public OutputListWrapper<SubEstructuraOutput> getSubestructuras(@RequestParam Map<String, String> filters) {
		LOG.info("getSubestructuras");
		return this.getSubestructuras(Constants.DEFAULT_PAGE, Constants.DEFAULT_PAGE_SIZE, "", "", filters);
	}

	@GetMapping("/page/{page}/size/{size}/filters")
	public OutputListWrapper<SubEstructuraOutput> getSubestructuras(
		@PathVariable int page, @PathVariable int size, @RequestParam Map<String, String> filters) {
		return this.getSubestructuras(page, size, "", "", filters);
	}

	@GetMapping("/page/{page}/size/{size}/sort/{sortColumn}/{sortOrder}/filters")
	public OutputListWrapper<SubEstructuraOutput> getSubestructuras(
		@PathVariable int page, @PathVariable int size, @PathVariable String sortColumn, @PathVariable String sortOrder,
		@RequestParam Map<String, String> filters) {
		LOG.info("getSubestructuras");
		return subEstructurasService.getSubestructuras(page, size, sortColumn, sortOrder, filters);
	}

	@GetMapping("/simple")
	public List<SubEstructuraSimpleOutput> getSubestructurasSimple() {
		LOG.info("getSubestructurasSimple");
		return subEstructurasService.getSubestructurasSimple();
	}

	@PutMapping("/exists")
	public boolean existeSubEstructura(@RequestBody SubEstructuraInput subestructura) {
		return subEstructurasService.existeSubestructura(subestructura);
	}

	@ExceptionHandler({ IcsSubestructuraServiceException.class })
	public ResponseEntity<String> handleServiceException(IcsSubestructuraServiceException ex) {
		return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler({ IcsSubestructuraNotFoundException.class })
	public ResponseEntity<String> handleServiceException(IcsSubestructuraNotFoundException ex) {
		return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
