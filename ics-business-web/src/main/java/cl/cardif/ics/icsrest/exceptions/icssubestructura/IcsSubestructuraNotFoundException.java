package cl.cardif.ics.icsrest.exceptions.icssubestructura;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class IcsSubestructuraNotFoundException extends RuntimeException {

  private static final long serialVersionUID = -1527596879157446910L;

  public IcsSubestructuraNotFoundException(String exception) {
    super(exception);
  }

  public IcsSubestructuraNotFoundException(String exception, Throwable t) {
    super(exception, t);
  }
}
