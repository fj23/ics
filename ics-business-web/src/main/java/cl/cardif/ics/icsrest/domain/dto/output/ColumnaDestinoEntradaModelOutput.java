package cl.cardif.ics.icsrest.domain.dto.output;

public class ColumnaDestinoEntradaModelOutput {

  private long idAtributoNegocio;
  private String nombreColumnaNegocio;

  public ColumnaDestinoEntradaModelOutput() {
    super();
  }

  public ColumnaDestinoEntradaModelOutput(long idAtributoNegocio) {
    super();
    this.idAtributoNegocio = idAtributoNegocio;
  }

  public ColumnaDestinoEntradaModelOutput(long idAtributoNegocio, String nombreColumnaNegocio) {
    super();
    this.idAtributoNegocio = idAtributoNegocio;
    this.nombreColumnaNegocio = nombreColumnaNegocio;
  }

  public long getIdAtributoNegocio() {
    return idAtributoNegocio;
  }

  public void setIdAtributoNegocio(long idAtributoNegocio) {
    this.idAtributoNegocio = idAtributoNegocio;
  }

  public String getNombreColumnaNegocio() {
    return nombreColumnaNegocio;
  }

  public void setNombreColumnaNegocio(String nombreColumnaNegocio) {
    this.nombreColumnaNegocio = nombreColumnaNegocio;
  }
}
