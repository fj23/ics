package cl.cardif.ics.icsrest.formula.operadores.math;

import java.util.ArrayList;
import java.util.Map;

import cl.cardif.ics.icsrest.formula.FormulaParent;
import cl.cardif.ics.icsrest.formula.IFormulaFactor;
import cl.cardif.ics.icsrest.formula.IMathFactor;
import cl.cardif.ics.icsrest.util.FormulaOrigenKey;

public class Sum extends FormulaParent implements IMathFactor {
	public Sum() {
		hijos = new ArrayList<>();
	}
	
	@Override
	public int getMaximumChildrenCount() {
		return Integer.MAX_VALUE;
	}

	@Override
	public String toSafeSQL(Map<FormulaOrigenKey, Integer> tableAliases) {
		StringBuilder sb = new StringBuilder();

		for (IFormulaFactor child : hijos) {
			if (sb.length() > 0) {
				sb.append(" + ");
			}
			
			sb.append("(").append(child.toSafeSQL(tableAliases)).append(")");
		}

		return sb.toString();
	}

	@Override
	public String toSQL(Map<FormulaOrigenKey, Integer> tableAliases) {
		StringBuilder sb = new StringBuilder();

		for (IFormulaFactor child : hijos) {
			if (sb.length() > 0) {
				sb.append(" + ");
			}
			
			sb.append("(").append(child.toSQL(tableAliases)).append(")");
		}

		return sb.toString();
	}
}
