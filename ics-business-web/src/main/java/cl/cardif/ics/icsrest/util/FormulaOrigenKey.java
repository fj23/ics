package cl.cardif.ics.icsrest.util;

public class FormulaOrigenKey {

	private Long idAtributoUsuario;
	private Long idAtributoNegocio;
	private String tablaOrigen;
	private String codigoTipoPersona;
	private String tipoPersona;

	public FormulaOrigenKey() {
		super();
	}

	public FormulaOrigenKey(Long idAtributoNegocio, String tipoPersona) {
		this.idAtributoNegocio = idAtributoNegocio;
		this.tipoPersona = tipoPersona;
	}

	public FormulaOrigenKey(String tablaOrigen, String tipoPersona) {
		this.tablaOrigen = tablaOrigen;
		this.tipoPersona = tipoPersona;
	}

	public FormulaOrigenKey(Long idAtributoNegocio, String tablaOrigen, String tipoPersona) {
		this.idAtributoNegocio = idAtributoNegocio;
		this.tablaOrigen = tablaOrigen;
		this.tipoPersona = tipoPersona;
	}

	public FormulaOrigenKey(Long idAtributoUsuario, Long idAtributoNegocio, String tablaOrigen,
			String tipoPersona) {
		this.idAtributoUsuario = idAtributoUsuario;
		this.idAtributoNegocio = idAtributoNegocio;
		this.tablaOrigen = tablaOrigen;
		this.tipoPersona = tipoPersona;
	}

	public Long getIdAtributoUsuario() {
		return idAtributoUsuario;
	}

	public void setIdAtributoUsuario(Long idAtributoUsuario) {
		this.idAtributoUsuario = idAtributoUsuario;
	}

	public Long getIdAtributoNegocio() {
		return idAtributoNegocio;
	}

	public void setIdAtributoNegocio(Long idAtributoNegocio) {
		this.idAtributoNegocio = idAtributoNegocio;
	}

	public String getTablaOrigen() {
		return tablaOrigen;
	}

	public void setTablaOrigen(String tablaOrigen) {
		this.tablaOrigen = tablaOrigen;
	}

	public String getCodigoTipoPersona() {
		return codigoTipoPersona;
	}

	public void setCodigoTipoPersona(String codigoTipoPersona) {
		this.codigoTipoPersona = codigoTipoPersona;
	}
	
	public String getTipoPersona() {
		return tipoPersona;
	}

	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}
	
	
	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
//		if (idAtributoNegocio != null) {
//			result = prime * result + idAtributoNegocio.hashCode();
//		}
//		else 
			if (tablaOrigen != null) {
			result = prime * result + tablaOrigen.hashCode();
		}
		result = prime * result + ((tipoPersona == null) ? 0 : tipoPersona.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object o) {
		if (o != null && this.getClass() == o.getClass()) {
			FormulaOrigenKey other = (FormulaOrigenKey)o;
			String otherTipoPersona = other.getTipoPersona();
			
			if (otherTipoPersona != null && this.tipoPersona != null) {
				String otherTablaNegocio = other.getTablaOrigen();
				
				if (otherTablaNegocio != null && this.tablaOrigen != null) {
					return this.tablaOrigen.equals(otherTablaNegocio) && 
							this.tipoPersona.equals(otherTipoPersona);
				}
			}
		}
		return false;
	}

	@Override
	public String toString() {
		return "FormulaOrigenKey [idAtributoNegocio=" + idAtributoNegocio + ", tablaOrigen=" + tablaOrigen
				+ ", tipoPersona=" + tipoPersona + "]";
	}
}
