package cl.cardif.ics.icsrest.controller;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import cl.cardif.ics.icsrest.domain.dto.input.PlantillaInput;
import cl.cardif.ics.icsrest.domain.dto.output.AtributoEnriquecimientoOutput;
import cl.cardif.ics.icsrest.domain.dto.output.AtributoNegocioOutput;
import cl.cardif.ics.icsrest.domain.dto.output.AtributoUsuarioOutput;
import cl.cardif.ics.icsrest.domain.dto.output.PlantillaOutput;
import cl.cardif.ics.icsrest.domain.dto.output.integracion.AtributosIntgOutput;
import cl.cardif.ics.icsrest.domain.pojo.TipoExtension;
import cl.cardif.ics.icsrest.exceptions.icsplantilla.IcsPlantillaNotFoundException;
import cl.cardif.ics.icsrest.exceptions.icsplantilla.IcsPlantillaServiceException;
import cl.cardif.ics.icsrest.service.AtributosService;
import cl.cardif.ics.icsrest.service.PlantillaEntradaService;
import cl.cardif.ics.icsrest.service.PlantillaIntegracionService;
import cl.cardif.ics.icsrest.service.PlantillaSalidaService;
import cl.cardif.ics.icsrest.service.PlantillasService;
import cl.cardif.ics.icsrest.util.Constants;
import cl.cardif.ics.icsrest.util.OutputListWrapper;

@RestController
@RequestMapping("/api/plantillas")
public class PlantillasController {
	private static final Logger LOG = LoggerFactory.getLogger(PlantillasController.class);

	private static final String MSJ_PLANTILLA_NO_ENCONTRADA = "Plantilla no encontrada";

	@Autowired AtributosService atributosService;
	@Autowired PlantillasService plantillaService;
	@Autowired PlantillaEntradaService plantillaEntradaService;
	@Autowired PlantillaIntegracionService plantillaIntegracionService;
	@Autowired PlantillaSalidaService plantillaSalidaService;

	@GetMapping("/activar/{idPlantilla}/{sinConfirmacion}")
	public ResponseEntity<Void> activarPlantilla(@PathVariable Long idPlantilla, @PathVariable Boolean sinConfirmacion) {
		LOG.info("activarPlantilla");

		if (!sinConfirmacion) {
			boolean puedeActivar = plantillaService.puedeActivar(idPlantilla);
			if (!puedeActivar) {
				return ResponseEntity.accepted().build();
			}
		}
		
		try {
			plantillaService.activar(idPlantilla);
			return ResponseEntity.ok().build();
		} catch (Exception e) {
			LOG.error("La plantilla no pudo ser activada", e);
			return ResponseEntity.notFound().build();
		}

	}

	@GetMapping("/desactivar/{idPlantilla}/{sinConfirmacion}")
	public ResponseEntity<Void> desactivarPlantilla(@PathVariable Long idPlantilla, @PathVariable Boolean sinConfirmacion) {
		LOG.info("desactivarPlantilla");

		boolean puedeDesactivar;
		if (sinConfirmacion) {
			puedeDesactivar = true;
		} else {
			try {
				puedeDesactivar = plantillaService.puedeDesactivar(idPlantilla);
				if (!puedeDesactivar) {
					return ResponseEntity.ok().build();
				}
			} catch (IcsPlantillaNotFoundException e) {
				puedeDesactivar = false;
				LOG.warn(MSJ_PLANTILLA_NO_ENCONTRADA, e);
			}
		}

		if (puedeDesactivar) {
			try {
				plantillaService.desactivar(idPlantilla);
				return ResponseEntity.ok().build();
			} catch (Exception e) {
				LOG.error("La plantilla no pudo ser desactivada", e);
			}
		}

		return ResponseEntity.notFound().build();

	}

	@GetMapping("/")
	public OutputListWrapper<PlantillaOutput> obtenerPagina() {
		return this.obtenerPagina(null, null, null, null, null);
	}

	@GetMapping("/filters")
	public OutputListWrapper<PlantillaOutput> obtenerPagina(@RequestParam Map<String, String> filtrosQueryString) {
		return this.obtenerPagina(null, null, null, null, filtrosQueryString);
	}

	@GetMapping("/page/{indice}")
	public OutputListWrapper<PlantillaOutput> obtenerPagina(@PathVariable Integer indice) {
		return this.obtenerPagina(indice, null, null, null, null);
	}

	@GetMapping("/page/{indice}/size/{registros}")
	public OutputListWrapper<PlantillaOutput> obtenerPagina(@PathVariable Integer indice, @PathVariable Integer registros) {
		return this.obtenerPagina(indice, registros, null, null, null);
	}

	@GetMapping("/page/{indice}/size/{registros}/filters")
	public OutputListWrapper<PlantillaOutput> obtenerPagina(
		@PathVariable Integer indice, @PathVariable Integer registros, @RequestParam Map<String, String> filtrosQueryString) {
		return this.obtenerPagina(indice, registros, null, null, filtrosQueryString);
	}

	@GetMapping("/page/{indice}/size/{registros}/sort/{columnaOrden}/{orden}/filters")
	public OutputListWrapper<PlantillaOutput> obtenerPagina(
		@PathVariable Integer indice, @PathVariable Integer registros, @PathVariable String columnaOrden, @PathVariable String orden,
		@RequestParam Map<String, String> filtrosQueryString) {
		LOG.info("obtenerPagina");

		if (indice == null) {
			indice = Constants.DEFAULT_PAGE;
		}

		if (registros == null) {
			registros = Constants.DEFAULT_PAGE_SIZE;
		}

		if (orden == null || orden.isEmpty() || !(orden.equals("asc") || orden.equals("desc"))) {
			orden = "asc";
		}

		return plantillaService.obtenerPlantillasOutput(indice, registros, columnaOrden, orden, filtrosQueryString);
	}

	@GetMapping("/nombresAll")
	public ResponseEntity<Collection<String>> obtenerNombresPlantillas(
		@RequestParam(name = "idSocio", required = false) Long idSocio, @RequestParam(name = "idEvento", required = false) Long idEvento) {
		LOG.info("obtenerNombresPlantillas");

		try {
			Collection<String> entities = plantillaService.obtenerNombresDeTodasLasPlantillas(idSocio, idEvento);
			return ResponseEntity.ok(entities);
		} catch (IcsPlantillaServiceException e) {
			LOG.error("No se pudieron obtener todos los nombres de plantillas", e);
			return ResponseEntity.notFound().build();
		}
	}

	@GetMapping("/tipos_salida_estandar")
	public List<TipoExtension> obtenerTiposExtensionEstandar() {
		LOG.info("obtenerTiposExtensionEstandar");
		return plantillaService.obtenerTiposExtensionEstandar();
	}

	@PostMapping("/version")
	public ResponseEntity<Long> obtenerUltimaVersion(@RequestBody PlantillaInput plantilla) {
		LOG.info("obtenerUltimaVersion");

		Long idPlantilla;
		try {
			idPlantilla = plantillaService.obtenerNumeroVersionVigente(plantilla.getNombre(), plantilla.getSocio(), plantilla.getEvento(),
					plantilla.getTipo());
		} catch (IcsPlantillaNotFoundException e) {
			LOG.info("obtenerUltimaVersion - La plantilla no existe", e);
			idPlantilla = 0L;
		}

		return ResponseEntity.ok(idPlantilla);
	}

	@GetMapping("/atributos_enriquecimiento/{idCore}")
	@ResponseStatus(HttpStatus.OK)
	public Collection<AtributoEnriquecimientoOutput> obtenerAtributosEnriquecimiento(@PathVariable Long idCore) {
		LOG.info("obtenerAtributosEnriquecimiento");
		return atributosService.obtenerAtributosEnriquecimientoPorCore(idCore);
	}

	@GetMapping("/atributos_integracion")
	@ResponseStatus(HttpStatus.OK)
	public Collection<AtributosIntgOutput> obtenerAtributosIntegracion() {
		LOG.info("obtenerAtributosIntegracion");
		return atributosService.obtenerAtributosIntegracion();
	}

	@GetMapping("/atributos_negocio")
	@ResponseStatus(HttpStatus.OK)
	public Collection<AtributoNegocioOutput> obtenerAtributosNegocio() {
		LOG.info("obtenerAtributosNegocio");
		return atributosService.obtenerAtributosNegocio();
	}

	@GetMapping("/columnas_destino_disponibles_usuario/{idEvento}")
	@ResponseStatus(HttpStatus.OK)
	public Collection<AtributoUsuarioOutput> obtenerAtributosUsuarioEvento(@PathVariable long idEvento) {
		LOG.info("obtenerColumnasUsuarios");
		return atributosService.obtenerAtributosUsuarioPorEvento(idEvento);
	}

	@GetMapping("/ver/{idPlantilla}")
	public ResponseEntity<PlantillaOutput> obtenerPlantilla(@PathVariable long idPlantilla) {
		LOG.info("obtenerPlantilla");
		PlantillaOutput plantilla = null;
		try {
			plantilla = plantillaService.cargarPlantilla(idPlantilla);
		} catch (IcsPlantillaNotFoundException e) {
			final String logError = "obtenerPlantilla - " + MSJ_PLANTILLA_NO_ENCONTRADA;
			LOG.error(logError, e);
			return ResponseEntity.notFound().build();
		}

		Long idTipoPlantilla = plantilla.getTipo().getId();
		if (idTipoPlantilla == Constants.ID_TIPO_PLANTILLA_ENTRADA) {
			plantilla = plantillaEntradaService.viewTemplateDetalle(plantilla);
		} else if (idTipoPlantilla == Constants.ID_TIPO_PLANTILLA_INTEGRACION) {
			plantilla = plantillaIntegracionService.viewTemplateDetalle(plantilla);
		} else if (idTipoPlantilla == Constants.ID_TIPO_PLANTILLA_SALIDA) {
			plantilla = plantillaSalidaService.viewTemplateDetalle(plantilla);
		}

		return ResponseEntity.ok(plantilla);
	}

	@ExceptionHandler({ Exception.class })
	public ResponseEntity<String> handleException(Exception e) {
		LOG.error("handleException",e);
		return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
