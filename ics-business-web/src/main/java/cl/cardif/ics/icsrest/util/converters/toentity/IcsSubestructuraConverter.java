package cl.cardif.ics.icsrest.util.converters.toentity;

import cl.cardif.ics.domain.entity.estructuras.IcsSubestructura;
import cl.cardif.ics.icsrest.domain.pojo.SubEstructura;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class IcsSubestructuraConverter implements Converter<SubEstructura, IcsSubestructura> {

  static final Logger LOG = LoggerFactory.getLogger(IcsSubestructuraConverter.class);

  @Override
  public IcsSubestructura convert(SubEstructura pojo) {

    IcsSubestructura entity = new IcsSubestructura();

    entity.setCodigoSubestructura(pojo.getCodigo());
    entity.setDescripcionSubestructura(pojo.getDescripcion());
    entity.setVigenciaSubestructura(pojo.getVigencia());

    return entity;
  }
}
