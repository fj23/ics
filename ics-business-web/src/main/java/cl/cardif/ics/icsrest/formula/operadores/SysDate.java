package cl.cardif.ics.icsrest.formula.operadores;

import java.util.Map;

import cl.cardif.ics.icsrest.formula.FormulaFactor;
import cl.cardif.ics.icsrest.formula.IStringFactor;
import cl.cardif.ics.icsrest.util.FormulaOrigenKey;

public class SysDate extends FormulaFactor implements IStringFactor {
	protected static String tipo = "SysDate";

	public SysDate() {
		super();
	}

	@Override
	public String toSafeSQL(Map<FormulaOrigenKey, Integer> tableAliases) {
		return "SYSDATE";
	}

	@Override
	public String toSQL(Map<FormulaOrigenKey, Integer> tableAliases) {
		return "SYSDATE";
	}
}
