package cl.cardif.ics.icsrest.exceptions.icsdetallehomologacion;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class IcsDetalleHomologacionUnauthorizedException extends RuntimeException {

  private static final long serialVersionUID = 3815682526998866691L;

  public IcsDetalleHomologacionUnauthorizedException(String exception) {
    super(exception);
  }

  public IcsDetalleHomologacionUnauthorizedException(String exception, Throwable t) {
    super(exception, t);
  }
}
