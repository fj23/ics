package cl.cardif.ics.icsrest.util;

/**
 * Contiene el valor asociado a cada permiso específico del perfilamiento, para usar @PreAuthorize
 * con hasAnyAuthority() en los Servicios a los que apuntan los @RestController
 *
 * @author Alvaro San Martin
 */
public final class Permisos {	

  // ======================================================
  // ==================== PLANTILLAS ======================
  // ======================================================
  /** Consultar PLANTILLAS */
  public static final String Cslt_Plan = "'29'";
  /** Mantener PLANTILLAS Altas */
  public static final String Mant_Plan_Alta = "'1'";
  /** Mantener PLANTILLAS Recaudacion */
  public static final String Mant_Plan_Reca = "'2'";
  /** Mantener PLANTILLAS Cancelacion */
  public static final String Mant_Plan_Canc = "'3'";
  /** Mantener PLANTILLAS Prospecto */
  public static final String Mant_Plan_Prosp = "'33'";
  /** Mantener PLANTILLAS Interacciones */
  public static final String Mant_Plan_Intera = "'34'";
  /** Mantener PLANTILLAS Medios de Pago */
  public static final String Mant_Plan_Pago = "'35'";
  
  // =======================================================
  // ==================== PROCESOS DE CARGA ================
  // =======================================================
  /** Consultar PROCESOS */
  public static final String Cslt_PrCa = "'30'";
  /** Mantener PROCESOS Altas */
  public static final String Admi_PrCa_Alta = "'4'";
  /** Mantener PROCESOS Recaudacion */
  public static final String Admi_PrCa_Reca = "'5'";
  /** Mantener PROCESOS Cancelacion */
  public static final String Admi_PrCa_Canc = "'6'";
  /** Terminar PROCESOS Altas */
  public static final String Term_PrCa_Alta = "'25'";
  /** Terimnar PROCESOS Recaudacion */
  public static final String Term_PrCa_Reca = "'26'";
  /** Terminar PROCESOS Cancelacion */
  public static final String Term_PrCa_Canc = "'27'";
  /** Mantener PROCESOS Prospecto */
  public static final String Admi_PrCa_Prosp = "'42'";
  /** Mantener PROCESOS Interacciones */
  public static final String Admi_PrCa_Intera = "'43'";
  /** Mantener PROCESOS Medios de Pago */
  public static final String Admi_PrCa_Pago = "'44'";
  /** Terminar PROCESOS Prospecto */
  public static final String Term_PrCa_Prosp = "'48'";
  /** Terminar PROCESOS Interacciones */
  public static final String Term_PrCa_Intera = "'49'";
  /** Terminar PROCESOS Medios de Pago */
  public static final String Term_PrCa_Pago = "'50'";
  
  // =======================================================
  // ==================== ESTRUCTURAS ======================
  // =======================================================
  /** Consultar ESTRUCTURAS */
  public static final String Cslt_Estr = "'31'";
  /** Mantener ESTRUCTURAS Aneto Altas */
  public static final String Mant_Estr_Alta = "'7'";
  /** Mantener ESTRUCTURAS Recaudacion */
  public static final String Mant_Estr_Reca = "'8'";
  /** Mantener ESTRUCTURAS Cancelacion */
  public static final String Mant_Estr_Canc = "'9'";
  /** Mantener ESTRUCTURAS Prospecto */
  public static final String Mant_Estr_Prosp = "'36'";
  /** Mantener ESTRUCTURAS Interacciones */
  public static final String Mant_Estr_Intera = "'37'";
  /** Mantener ESTRUCTURAS Medios de Pago */
  public static final String Mant_Estr_Pago = "'38'";
  
  // ==========================================================
  // ==================== HOMOLOGACIONES ======================
  // ==========================================================
  /** Consultar HOMOLOGACIONES */
  public static final String Cslt_Homo = "'32'";
  /** Mantener HOMOLOGACIONES Altas */
  public static final String Mant_Homo_Alta = "'10'";
  /** Mantener HOMOLOGACIONES Recaudacion */
  public static final String Mant_Homo_Reca = "'11'";
  /** Mantener HOMOLOGACIONES Cancelacion */
  public static final String Mant_Homo_Canc = "'12'";
  /** Mantener HOMOLOGACIONES Prospecto */
  public static final String Mant_Homo_Prosp = "'39'";
  /** Mantener HOMOLOGACIONES Interacciones */
  public static final String Mant_Homo_Intera = "'40'";
  /** Mantener HOMOLOGACIONES Medios de Pago */
  public static final String Mant_Homo_Pago = "'41'";
  
  // ==============================================================
  // ==================== CARGAS DE ARCHIVOS ======================
  // ==============================================================
  /** CARGA ARCHIVO Altas */
  public static final String Carga_Arch_Alta = "'16'";
  /** CARGA ARCHIVO Recaudacion */
  public static final String Carga_Arch_Reca = "'17'";
  /** CARGA ARCHIVO Cancelacion */
  public static final String Carga_Arch_Canc = "'18'";
  /** CARGA ARCHIVO Prospecto */
  public static final String Carga_Arch_Prosp = "'45'";
  /** CARGA ARCHIVO Interacciones */
  public static final String Carga_Arch_Intera = "'46'";
  /** CARGA ARCHIVO Medios de Pago */
  public static final String Carga_Arch_Pago = "'47'";

  private Permisos() {}
}
