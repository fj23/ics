package cl.cardif.ics.icsrest.domain.pojo;

public class InputRow {

	private String data;
	private String name;
	private String extension;
	private String directory;
	private String sheet;
	private String dataType;

	public InputRow(final String data, final String name, final String extension, final String directory,
			final String sheet, final String dataType) {
		this.data = data;
		this.name = name;
		this.extension = extension;
		this.directory = directory;
		this.sheet = sheet;
		this.dataType = dataType;
	}

	public String getData() {
		return data;
	}

	public String getName() {
		return name;
	}

	public String getExtension() {
		return extension;
	}

	public String getDirectory() {
		return directory;
	}

	public String getSheet() {
		return sheet;
	}

	public String getDataType() {
		return dataType;
	}
}
