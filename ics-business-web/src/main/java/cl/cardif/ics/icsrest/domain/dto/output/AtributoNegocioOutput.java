package cl.cardif.ics.icsrest.domain.dto.output;

public class AtributoNegocioOutput {

	private Long idAtributoNegocio;

	private Long idEvento;

	private String nombreColumnaNegocio;

	private String nombreTablaDestino;

	public AtributoNegocioOutput() {
		super();
	}

	public long getIdAtributoNegocio() {
		return idAtributoNegocio;
	}

	public void setIdAtributoNegocio(long idAtributoNegocio) {
		this.idAtributoNegocio = idAtributoNegocio;
	}

	public Long getIdEvento() {
		return idEvento;
	}

	public void setIdEvento(Long idEvento) {
		this.idEvento = idEvento;
	}

	public String getNombreColumnaNegocio() {
		return nombreColumnaNegocio;
	}

	public void setNombreColumnaNegocio(String nombreColumnaNegocio) {
		this.nombreColumnaNegocio = nombreColumnaNegocio;
	}

	public String getNombreTablaDestino() {
		return nombreTablaDestino;
	}

	public void setNombreTablaDestino(String nombreTablaDestino) {
		this.nombreTablaDestino = nombreTablaDestino;
	}

	@Override
	public String toString() {
		return "AtributoNegocioOutput [idAtributoNegocio=" + idAtributoNegocio + ", idEvento=" + idEvento
				+ ", nombreColumnaNegocio=" + nombreColumnaNegocio + ", nombreTablaDestino=" + nombreTablaDestino + "]";
	}
}
