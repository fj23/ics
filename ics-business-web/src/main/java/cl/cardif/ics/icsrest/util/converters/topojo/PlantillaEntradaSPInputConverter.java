package cl.cardif.ics.icsrest.util.converters.topojo;

import cl.cardif.ics.icsrest.domain.dto.input.PlantillaEntradaInput;
import cl.cardif.ics.icsrest.domain.pojo.PlantillaEntradaSPInput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class PlantillaEntradaSPInputConverter
    implements Converter<PlantillaEntradaInput, PlantillaEntradaSPInput> {

  static final Logger LOG = LoggerFactory.getLogger(TipoSalidaConverter.class);

  @Override
  public PlantillaEntradaSPInput convert(PlantillaEntradaInput frontInput) {

    PlantillaEntradaSPInput spInput = new PlantillaEntradaSPInput();

    // Datos Plantilla
    spInput.setNombrePlantilla(frontInput.getNombre());
    spInput.setVersionPlantilla(Long.valueOf(frontInput.getVersion()));
    spInput.setIdTipoPlantilla(frontInput.getTipo());
    spInput.setIdSocio(frontInput.getSocio());
    spInput.setIdEvento(frontInput.getEvento());
    spInput.setIdCore(frontInput.getCore());
    spInput.setObservaciones(frontInput.getObservaciones());
    spInput.setIdTipoExtension(frontInput.getTipoExtension());

    return spInput;
  }
}
