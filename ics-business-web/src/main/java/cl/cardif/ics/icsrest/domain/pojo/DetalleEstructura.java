package cl.cardif.ics.icsrest.domain.pojo;

import cl.cardif.ics.icsrest.domain.dto.input.BaseDetalleColumnaModel;

public class DetalleEstructura extends BaseDetalleColumnaModel {
	protected TipoDato tipoDato;
	protected Long idEstructuraPadre;
	protected Long idSubEstructura;

	public DetalleEstructura() {
		super();
	}

	public Long getIdEstructuraPadre() {
		return idEstructuraPadre;
	}

	public void setIdEstructuraPadre(Long idEstructuraPadre) {
		this.idEstructuraPadre = idEstructuraPadre;
	}

	public Long getIdSubEstructura() {
		return idSubEstructura;
	}

	public void setIdSubEstructura(Long idSubEstructura) {
		this.idSubEstructura = idSubEstructura;
	}

	public TipoDato getTipoDato() {
		return tipoDato;
	}

	public void setTipoDato(TipoDato tipoDato) {
		this.tipoDato = tipoDato;
	}
}
