package cl.cardif.ics.icsrest.domain.dto.input;

public class EstructuraAnetoModelInput extends EstructuraAnetoSimpleInput {

  private String vigencia;

  public EstructuraAnetoModelInput() {
    super();
  }

  public EstructuraAnetoModelInput(long id) {
    super();
    this.id = id;
  }

  public String getVigencia() {
    return vigencia;
  }

  public void setVigencia(String vigencia) {
    this.vigencia = vigencia;
  }
}
