package cl.cardif.ics.icsrest.domain.dto.input;

import java.util.ArrayList;
import java.util.List;

public class PlantillaEntradaInput extends PlantillaInput {
	private List<ArchivoEntradaInput> archivos;
	private List<MapeoEntradaInput> columnasDestino;
	private List<UnionArchivosInputModel> uniones;

	public PlantillaEntradaInput() {
		super();
	}

	public List<ArchivoEntradaInput> getArchivos() {
		if (archivos != null) {
			return new ArrayList<>(archivos);
		}
		else {
			return new ArrayList<>();
		}
	}

	public void setArchivos(List<ArchivoEntradaInput> archivos) {
		if (archivos != null) {
			this.archivos = new ArrayList<>(archivos);
		}
	}

	public List<MapeoEntradaInput> getColumnasDestino() {
		if (columnasDestino != null) {
			return new ArrayList<>(columnasDestino);
		}
		else {
			return new ArrayList<>();
		}
	}

	public void setColumnasDestino(List<MapeoEntradaInput> columnasDestino) {
		if (columnasDestino != null) {
			this.columnasDestino = new ArrayList<>(columnasDestino);
		}
	}

	public List<UnionArchivosInputModel> getUniones() {
		if (uniones != null) {
			return new ArrayList<>(uniones);
		}
		return new ArrayList<>();
	}

	public void setUniones(List<UnionArchivosInputModel> uniones) {
		if (uniones != null) {
			this.uniones = new ArrayList<>(uniones);
		}
	}	

}
