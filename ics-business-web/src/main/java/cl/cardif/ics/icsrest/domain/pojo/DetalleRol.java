package cl.cardif.ics.icsrest.domain.pojo;

import java.io.Serializable;
import java.sql.Date;

public class DetalleRol implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long idDetalle;
	private String IcsTipoRol;
	private Long codigo;
	private Date fechaCreacion;
	private String vigencia;

	public DetalleRol() {
		super();
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public Date getFechaCreacion() {
		return (Date) fechaCreacion.clone();
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = (Date) fechaCreacion.clone();
	}

	public String getIcsTipoRol() {
		return IcsTipoRol;
	}

	public void setIcsTipoRol(String icsTipoRol) {
		IcsTipoRol = icsTipoRol;
	}

	public Long getIdDetalle() {
		return idDetalle;
	}

	public void setIdDetalle(Long idDetalle) {
		this.idDetalle = idDetalle;
	}

	public String getVigencia() {
		return vigencia;
	}

	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}

	@Override
	public String toString() {
		return "IcsDetalleRol [idDetalle=" + idDetalle + ", IcsTipoRol=" + IcsTipoRol + ", codigo=" + codigo
				+ ", fechaCreacion=" + fechaCreacion + ", vigencia=" + vigencia + "]";
	}
}
