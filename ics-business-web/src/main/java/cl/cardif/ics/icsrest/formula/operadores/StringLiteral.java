package cl.cardif.ics.icsrest.formula.operadores;

import java.util.Map;

import cl.cardif.ics.icsrest.formula.FormulaFactor;
import cl.cardif.ics.icsrest.formula.IStringFactor;
import cl.cardif.ics.icsrest.util.FormulaOrigenKey;

public class StringLiteral extends FormulaFactor implements IStringFactor {
	protected static String tipo = "StringLiteral";
	private String valor = null;

	public StringLiteral() {
		super();
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	@Override
	public String toSafeSQL(Map<FormulaOrigenKey, Integer> tableAliases) {
		if (valor == null) {
			return "''";
		} else {
			int indexOfSingleQuote = valor.indexOf('\'', 0);

			if (indexOfSingleQuote != -1) {
				valor = valor.replaceAll("(')", "''");
			}

			return "('" + valor + "')";
		}
	}

	@Override
	public String toSQL(Map<FormulaOrigenKey, Integer> tableAliases) {
		return this.toSafeSQL(tableAliases);
	}

	@Override
	public String toJSON() {
		StringBuilder sb = new StringBuilder()
			.append("{")
				.append("\"tipo\":\"StringLiteral\"");

		if (valor != null) {
			
			int indexOfDoubleQuote = valor.indexOf('\"', 0);
			if (indexOfDoubleQuote != -1) {
				valor = valor.replaceAll("(\")", "\\\"");
			}
			
			sb.append(",")
				.append("\"valor\":\"").append(valor).append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

}
