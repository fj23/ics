package cl.cardif.ics.icsrest.domain.dto.input;

public class PlantillaInput extends BaseModel {
	protected String version;
	protected String observaciones;
	protected Long tipo;
	protected Long socio;
	protected Long evento;
	protected Long core;
	protected String fechaCreacion;
	protected Long tipoExtension;
	protected String codUsuario;

	public Long getCore() {
		return core;
	}

	public void setCore(Long core) {
		this.core = core;
	}

	public Long getEvento() {
		return evento;
	}

	public void setEvento(Long evento) {
		this.evento = evento;
	}

	public String getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(String fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public Long getSocio() {
		return socio;
	}

	public void setSocio(Long socio) {
		this.socio = socio;
	}

	public Long getTipo() {
		return tipo;
	}

	public void setTipo(Long tipo) {
		this.tipo = tipo;
	}

	public Long getTipoExtension() {
		return tipoExtension;
	}

	public void setTipoExtension(Long tipoExtension) {
		this.tipoExtension = tipoExtension;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getCodUsuario() {
		return codUsuario;
	}

	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}

}
