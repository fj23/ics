package cl.cardif.ics.icsrest.domain.pojo;

import cl.cardif.ics.icsrest.domain.dto.input.BaseModel;

public class TipoExtension extends BaseModel {

	private String formato;

	public TipoExtension() {
		super();
	}

	public String getFormato() {
		return this.formato;
	}

	public void setFormato(String formato) {
		this.formato = formato;
	}
}
