package cl.cardif.ics.icsrest.util;

import java.util.Collection;
import java.util.HashSet;

import javax.persistence.Column;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import cl.cardif.ics.icsrest.controller.HomologacionesController;

public final class Util {

	static final Logger LOG = LoggerFactory.getLogger(HomologacionesController.class);

	public static String getMappedColumnNameFromEntityFieldName(@SuppressWarnings("rawtypes") Class c,
			String classFieldName) {
		try {
			Column annotation = c.getField(classFieldName).getAnnotation(Column.class);
			return annotation.name();
		} catch (NoSuchFieldException e) {
			LOG.info("NoSuchFieldException", e);
		}
		return null;
	}

	public static HashSet<Long> getEventsAuthorized() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
		HashSet<Long> where = new HashSet<>();
		for (GrantedAuthority grantedAuthority : authorities) {
			if (grantedAuthority.getAuthority().contains("EVENT_")) {
				String aux = grantedAuthority.getAuthority();
				where.add(Long.parseLong(aux.substring(6)));
			}
		}
		return where;
	}
}
