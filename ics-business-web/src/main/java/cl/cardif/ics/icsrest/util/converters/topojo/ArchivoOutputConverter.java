package cl.cardif.ics.icsrest.util.converters.topojo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import cl.cardif.ics.domain.entity.plantillas.IcsArchivo;
import cl.cardif.ics.icsrest.domain.pojo.Archivo;

@Component
public class ArchivoOutputConverter implements Converter<IcsArchivo, Archivo> {
	static final Logger LOG = LoggerFactory.getLogger(ArchivoOutputConverter.class);

	@Override
	public Archivo convert(IcsArchivo entity) {
		Archivo archivo = new Archivo();
		archivo.setIdArchivo(entity.getIdArchivo());
		archivo.setDelimitador(entity.getDelimitador());
		archivo.setLineaFinal(entity.getLineaFinal());
		archivo.setLineaInicial(entity.getLineaInicial());
		archivo.setNombreArchivo(entity.getNombreArchivo());
		archivo.setTipoArchivo(entity.getTipoArchivo());
		archivo.setVigenciaArchivo(entity.getVigenciaArchivo());
		archivo.setNumeroHoja(entity.getNumeroHoja());
		archivo.setFlgSinTransformacion(entity.getFlgSinTransformacion());
		
		return archivo;
	}
}
