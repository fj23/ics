package cl.cardif.ics.icsrest.util.converters.topojo;

import cl.cardif.ics.domain.entity.common.IcsCore;
import cl.cardif.ics.icsrest.domain.pojo.Core;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class CoreConverter implements Converter<IcsCore, Core> {

  @Override
  public Core convert(IcsCore icsCore) {
    Core core = new Core();
    core.setId(icsCore.getIdCore());
    core.setNombre(icsCore.getNombreCore());
    core.setFlgVisible(icsCore.getFlgVisible());
    core.setVigencia(icsCore.getVigenciaCore());
    return core;
  }
}
