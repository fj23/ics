package cl.cardif.ics.icsrest.util.converters.topojo;

import cl.cardif.ics.domain.entity.estructuras.IcsDetalleSubestructura;
import cl.cardif.ics.icsrest.domain.dto.output.DetalleSubEstructuraSimpleOutput;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class DetallesSubestructurasSimpleConverter
    implements Converter<IcsDetalleSubestructura, DetalleSubEstructuraSimpleOutput> {

  @Override
  public DetalleSubEstructuraSimpleOutput convert(IcsDetalleSubestructura icsDetalle) {

    DetalleSubEstructuraSimpleOutput detalle = new DetalleSubEstructuraSimpleOutput();
    detalle.setId(icsDetalle.getIdDetalleSubestructura());
    detalle.setDescripcion(icsDetalle.getDescripcionDetalleSubestructura());
    return detalle;
  }
}
