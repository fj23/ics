package cl.cardif.ics.icsrest.service;

import cl.cardif.ics.icsrest.domain.pojo.TipoRol;

import java.util.List;

@FunctionalInterface
public interface RolesService {

  List<TipoRol> findRolesByName(List<String> roles);
}
