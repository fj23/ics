package cl.cardif.ics.icsrest.exceptions.icscargaarchivo;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class IcsCargaArchivoNotFoundException extends RuntimeException {

  private static final long serialVersionUID = -1527596879157446910L;

  public IcsCargaArchivoNotFoundException(String exception) {
    super(exception);
  }

  public IcsCargaArchivoNotFoundException(String exception, Throwable t) {
    super(exception, t);
  }
}
