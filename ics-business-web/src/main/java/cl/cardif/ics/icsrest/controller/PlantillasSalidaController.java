package cl.cardif.ics.icsrest.controller;

import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import cl.cardif.ics.icsrest.domain.dto.input.EstructuraAnetoPlantillaSalidaModel;
import cl.cardif.ics.icsrest.domain.dto.input.PlantillaSalidaInput;
import cl.cardif.ics.icsrest.domain.dto.output.AtributoSalidaEstrucOutput;
import cl.cardif.ics.icsrest.domain.dto.output.RequestResultOutput;
import cl.cardif.ics.icsrest.domain.pojo.Core;
import cl.cardif.ics.icsrest.domain.pojo.DirectorioSalida;
import cl.cardif.ics.icsrest.exceptions.icsplantilla.IcsPlantillaServiceException;
import cl.cardif.ics.icsrest.service.AtributosService;
import cl.cardif.ics.icsrest.service.PlantillaSalidaService;

@RestController
@RequestMapping("/api/plantillas/salida")
public class PlantillasSalidaController {
	private static final Logger LOG = LoggerFactory.getLogger(PlantillasSalidaController.class);

	@Autowired PlantillaSalidaService plantillaService;
	@Autowired AtributosService atributosService;

	@PostMapping("/actualizar")
	public ResponseEntity<RequestResultOutput> actualizarPlantilla(@RequestBody PlantillaSalidaInput input) {
		LOG.info("actualizarPlantilla");
		try {
			RequestResultOutput result = plantillaService.addOrUpdatePlantillaSalida(input, true);
			return new ResponseEntity<>(result, HttpStatus.CREATED);
		} catch (IcsPlantillaServiceException e) {
			LOG.info("Plantilla no encontrada : ", e);
			return ResponseEntity.badRequest().build();
		}
	}

	@PostMapping("/nueva")
	public ResponseEntity<RequestResultOutput> almacenarPlantilla(@RequestBody PlantillaSalidaInput input) {
		LOG.info("almacenarPlantilla");
		try {
			RequestResultOutput result = plantillaService.addOrUpdatePlantillaSalida(input, false);
			return new ResponseEntity<>(result, HttpStatus.CREATED);
		} catch (IcsPlantillaServiceException e) {
			LOG.info("Plantilla no encontrada : ", e);
			return ResponseEntity.badRequest().build();
		}
	}

	@GetMapping("/estructuras")
	@ResponseStatus(HttpStatus.OK)
	public Collection<EstructuraAnetoPlantillaSalidaModel> obtenerEstructurasPlantillaSalida(
		@RequestParam("idPlantilla") Long idPlantilla) {
		LOG.info("obtenerEstructurasPlantillaSalida");

		Collection<EstructuraAnetoPlantillaSalidaModel> lista;
		lista = plantillaService.obtenerEstructurasPlantillaSalida(idPlantilla);
		return lista;
	}

	@GetMapping("/atributos_estructuras")
	@ResponseStatus(HttpStatus.OK)
	public Collection<AtributoSalidaEstrucOutput> obtenerAtributosOrigenEstructuras() {
		LOG.info("obtenerAtributosOrigenEstructuras");

		Collection<AtributoSalidaEstrucOutput> lista;
		lista = plantillaService.consultarAtributosOrigenEstructuras();
		return lista;
	}

	@GetMapping("/cores_estructuras")
	@ResponseStatus(HttpStatus.OK)
	public Collection<Core> obtenerCoresHomologacionesEstructuras() {
		LOG.info("obtenerCoresHomologacionesEstructuras");
		return plantillaService.consultarCoresHomologacionesEstructuras();
	}

	@GetMapping("/directorios")
	@ResponseStatus(HttpStatus.OK)
	public List<DirectorioSalida> obtenerDirectoriosSalida() {
		LOG.info("obtenerDirectoriosSalida");
		return plantillaService.consultarDirectoriosSalida();
	}

}
