package cl.cardif.ics.icsrest.domain.dto.input;

public abstract class EstructuraAnetoSimpleInput extends BaseColumnaModel {
  protected Long cardMin;
  protected Long cardMax;

  public long getCardMax() {
    return cardMax;
  }

  public void setCardMax(Long cardMax) {
    this.cardMax = cardMax;
  }

  public long getCardMin() {
    return cardMin;
  }

  public void setCardMin(Long cardMin) {
    this.cardMin = cardMin;
  }
}
