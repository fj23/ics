package cl.cardif.ics.icsrest.domain.dto.output.integracion;

import cl.cardif.ics.icsrest.domain.dto.output.*;

public class AtributosIntgOutput {

	private Long idAtributoIntegracion;
	private String flgObligatorio;
	private Long idAtributoEnriquecimiento;
	private Long idAtributoNegocio;
	private Long idIntegracionEvento;
	private Long idPlantilla;
	private Long idTipoFormula;

	private Long idFormato;
	private Integer ordenFormato;
	private Long idTipoHomologacion;
	private Integer ordenHomologacion;

	private AtributoUsuarioOutput columnaOrigen;
	private AtributoEnriquecimientoOutput columnaEnriquecimiento;

	private TipoFormulaOutput formula;

	private Long idAtributoUsuario;
	private Long idAtributoUser;

	public AtributosIntgOutput() {
		super();
	}

	public AtributoEnriquecimientoOutput getColumnaEnriquecimiento() {
		return columnaEnriquecimiento;
	}

	public void setColumnaEnriquecimiento(AtributoEnriquecimientoOutput columnaEnriquecimiento) {
		this.columnaEnriquecimiento = columnaEnriquecimiento;
	}

	public AtributoUsuarioOutput getColumnaOrigen() {
		return columnaOrigen;
	}

	public void setColumnaOrigen(AtributoUsuarioOutput columnaOrigen) {
		this.columnaOrigen = columnaOrigen;
	}

	public String getFlgObligatorio() {
		return flgObligatorio;
	}

	public void setFlgObligatorio(String flgObligatorio) {
		this.flgObligatorio = flgObligatorio;
	}

	public TipoFormulaOutput getFormula() {
		return formula;
	}

	public void setFormula(TipoFormulaOutput formula) {
		this.formula = formula;
	}

	public Long getIdAtributoEnriquecimiento() {
		return idAtributoEnriquecimiento;
	}

	public void setIdAtributoEnriquecimiento(Long idAtributoEnriquecimiento) {
		this.idAtributoEnriquecimiento = idAtributoEnriquecimiento;
	}

	public Long getIdAtributoIntegracion() {
		return idAtributoIntegracion;
	}

	public void setIdAtributoIntegracion(Long idAtributoIntegracion) {
		this.idAtributoIntegracion = idAtributoIntegracion;
	}

	public Long getIdAtributoNegocio() {
		return idAtributoNegocio;
	}

	public void setIdAtributoNegocio(Long idAtributoNegocio) {
		this.idAtributoNegocio = idAtributoNegocio;
	}

	public Long getIdFormato() {
		return idFormato;
	}

	public void setIdFormato(Long idDeFormato) {
		this.idFormato = idDeFormato;
	}

	public Long getIdTipoHomologacion() {
		return idTipoHomologacion;
	}

	public void setIdTipoHomologacion(Long idTipoHomologacion) {
		this.idTipoHomologacion = idTipoHomologacion;
	}

	public Long getIdIntegracionEvento() {
		return idIntegracionEvento;
	}

	public void setIdIntegracionEvento(Long idIntegracionEvento) {
		this.idIntegracionEvento = idIntegracionEvento;
	}

	public Long getIdPlantilla() {
		return idPlantilla;
	}

	public void setIdPlantilla(Long idPlantilla) {
		this.idPlantilla = idPlantilla;
	}

	public Long getIdTipoFormula() {
		return idTipoFormula;
	}

	public void setIdTipoFormula(Long idTipoFormula) {
		this.idTipoFormula = idTipoFormula;
	}

	public Integer getOrdenFormato() {
		return ordenFormato;
	}

	public void setOrdenFormato(Integer ordenFormato) {
		this.ordenFormato = ordenFormato;
	}

	public Integer getOrdenHomologacion() {
		return ordenHomologacion;
	}

	public void setOrdenHomologacion(Integer ordenHomologacion) {
		this.ordenHomologacion = ordenHomologacion;
	}

	public Long getIdAtributoUsuario() {
		return idAtributoUsuario;
	}

	public void setIdAtributoUsuario(Long idAtributoUsuario) {
		this.idAtributoUsuario = idAtributoUsuario;
	}

	public Long getIdAtributoUser() {
		return idAtributoUser;
	}

	public void setIdAtributoUser(Long idAtributoUser) {
		this.idAtributoUser = idAtributoUser;
	}
}
