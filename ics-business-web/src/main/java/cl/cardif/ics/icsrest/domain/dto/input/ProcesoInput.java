package cl.cardif.ics.icsrest.domain.dto.input;

import java.util.ArrayList;
import java.util.List;

import cl.cardif.ics.icsrest.domain.dto.output.PlantillaOutput;

public class ProcesoInput extends Proceso {
	private List<DetalleProcesoInput> detalleProceso;
	private PlantillaOutput plantillaEntradaSocio;
	private PlantillaOutput plantillaIntegracion;
	private PlantillaOutput plantillaSalidaSocio;
	private boolean esSalidaAneto;
	private Long callCenter;
	private Long tipoArchivo;
	private Long directorioSalidaError;

	public ProcesoInput() {
		super();
	}

	public List<DetalleProcesoInput> getDetalleProceso() {
		if (this.detalleProceso != null) {
			return new ArrayList<>(this.detalleProceso);
		} else {
			return new ArrayList<>();
		}
	}

	public void setDetalleProceso(List<DetalleProcesoInput> detalleProceso) {
		if (detalleProceso != null) {
			this.detalleProceso = new ArrayList<>(detalleProceso);
		}
	}

	public PlantillaOutput getPlantillaEntradaSocio() {
		return plantillaEntradaSocio;
	}

	public void setPlantillaEntradaSocio(PlantillaOutput plantillaEntradaSocio) {
		this.plantillaEntradaSocio = plantillaEntradaSocio;
	}

	public PlantillaOutput getPlantillaIntegracion() {
		return plantillaIntegracion;
	}

	public void setPlantillaIntegracion(PlantillaOutput plantillaIntegracion) {
		this.plantillaIntegracion = plantillaIntegracion;
	}

	public PlantillaOutput getPlantillaSalidaSocio() {
		return plantillaSalidaSocio;
	}

	public void setPlantillaSalidaSocio(PlantillaOutput plantillaSalidaSocio) {
		this.plantillaSalidaSocio = plantillaSalidaSocio;
	}

	public boolean isSalidaAneto() {
		return esSalidaAneto;
	}

	public void setEsSalidaAneto(boolean esSalidaAneto) {
		this.esSalidaAneto = esSalidaAneto;
	}

	public Long getCallCenter() {
		return callCenter;
	}

	public void setCallCenter(Long callCenter) {
		this.callCenter = callCenter;
	}

	public Long getTipoArchivo() {
		return tipoArchivo;
	}

	public void setTipoArchivo(Long tipoArchivo) {
		this.tipoArchivo = tipoArchivo;
	}

	public Long getDirectorioSalidaError() {
		return directorioSalidaError;
	}

	public void setDirectorioSalida(Long directorioSalida) {
		this.directorioSalidaError = directorioSalida;
	}

	@Override
	public String toString() {
		return "ProcesoInput [detalleProceso=" + detalleProceso + ", plantillaEntradaSocio=" + plantillaEntradaSocio
				+ ", plantillaIntegracion=" + plantillaIntegracion + ", plantillaSalidaSocio=" + plantillaSalidaSocio
				+ ", esSalidaAneto=" + esSalidaAneto + ", callCenter=" + callCenter + ", tipoArchivo=" + tipoArchivo
				+ ", directorioSalidaError=" + directorioSalidaError + ", toString()=" + super.toString() + "]";
	}
	
}
