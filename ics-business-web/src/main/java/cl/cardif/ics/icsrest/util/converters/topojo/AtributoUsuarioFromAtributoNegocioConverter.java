package cl.cardif.ics.icsrest.util.converters.topojo;

import cl.cardif.ics.domain.entity.plantillas.IcsAtributoNegocio;
import cl.cardif.ics.icsrest.domain.dto.output.AtributoNegocioOutput;
import cl.cardif.ics.icsrest.domain.dto.output.AtributoUsuarioOutput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component
public class AtributoUsuarioFromAtributoNegocioConverter
    implements Converter<IcsAtributoNegocio, AtributoUsuarioOutput> {

  private static final Logger LOG =
      LoggerFactory.getLogger(AtributoUsuarioFromAtributoNegocioConverter.class);

  @Override
  public AtributoUsuarioOutput convert(IcsAtributoNegocio entity) {
    AtributoUsuarioOutput pojo = new AtributoUsuarioOutput();

    pojo.setIdAtributoNegocio(entity.getIdAtributoNegocio());

    LOG.info("Evento obtenido en ics atributo : " + entity.getIcsEvento().getIdEvento());

    pojo.setIdAtributoUsuario(null);
    pojo.setNombreColumna(null);
    pojo.setTipoPersona(null);
    pojo.setFlgCardinalidad(null);

    AtributoNegocioConverter convert = new AtributoNegocioConverter();
    AtributoNegocioOutput atributoNegocio = convert.convert(entity);

    pojo.setIdAtributoNegocio(atributoNegocio.getIdAtributoNegocio());
    pojo.setNombreColumnaNegocio(atributoNegocio.getNombreColumnaNegocio());
    pojo.setNombreTablaDestino(atributoNegocio.getNombreTablaDestino());

    return pojo;
  }

  public List<AtributoUsuarioOutput> convertList(Collection<IcsAtributoNegocio> entities) {
    List<AtributoUsuarioOutput> pojoList = new ArrayList<>();

    for (IcsAtributoNegocio entity : entities) {
      pojoList.add(this.convert(entity));
    }

    return pojoList;
  }
}
