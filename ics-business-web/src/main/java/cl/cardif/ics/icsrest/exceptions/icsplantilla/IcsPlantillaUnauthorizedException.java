package cl.cardif.ics.icsrest.exceptions.icsplantilla;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class IcsPlantillaUnauthorizedException extends RuntimeException {

  private static final long serialVersionUID = 3815682526998866691L;

  public IcsPlantillaUnauthorizedException(String exception) {
    super(exception);
  }

  public IcsPlantillaUnauthorizedException(String exception, Throwable t) {
    super(exception, t);
  }
}
