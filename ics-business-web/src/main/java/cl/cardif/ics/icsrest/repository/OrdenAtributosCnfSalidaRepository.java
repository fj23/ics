package cl.cardif.ics.icsrest.repository;

import cl.cardif.ics.domain.entity.plantillas.IcsOrdenTrabajosSalida;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface OrdenAtributosCnfSalidaRepository
    extends JpaRepository<IcsOrdenTrabajosSalida, Long>, 
    	QueryDslPredicateExecutor<IcsOrdenTrabajosSalida> {
	
}
