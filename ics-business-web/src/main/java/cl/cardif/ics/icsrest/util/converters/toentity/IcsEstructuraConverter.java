package cl.cardif.ics.icsrest.util.converters.toentity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import cl.cardif.ics.domain.entity.estructuras.IcsDetalleEstructura;
import cl.cardif.ics.domain.entity.estructuras.IcsEstructura;
import cl.cardif.ics.icsrest.domain.pojo.Estructura;

@Component
public class IcsEstructuraConverter implements Converter<Estructura, IcsEstructura> {

  static final Logger LOG = LoggerFactory.getLogger(IcsEstructuraConverter.class);

  @Override
  public IcsEstructura convert(Estructura input) {
    LOG.info("convert Estructura to ICSEstructura");

    IcsEstructura entity = new IcsEstructura();
    entity.setCodigoEstructura(input.getCodigo());
    entity.setDescripcionEstructura(input.getDescripcion());
    entity.setCardinalidadMaximaEstructura(BigDecimal.valueOf(input.getCardMax()));
    entity.setCardinalidadMinimaEstructura(BigDecimal.valueOf(input.getCardMin()));

    List<IcsDetalleEstructura> detalles = new ArrayList<>();
    entity.setIcsDetalleEstructuras(detalles);

    return entity;
  }
}
