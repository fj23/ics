package cl.cardif.ics.icsrest.util;

import cl.cardif.ics.icsrest.domain.dto.input.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;
import java.util.List;

public class PlantillaUtil {

	static final Logger LOG = LoggerFactory.getLogger(PlantillaUtil.class);

	/**
	 * Crea un String de los enteros separados por ','
	 *
	 * @param estructuras Enteros a ordenar
	 * @return String con los enteros separados por ','
	 */
	public String generarStringIdsGrupoEstructuras(List<Integer> estructuras) {
		StringBuilder salida = new StringBuilder();
		if (null != estructuras) {
			for (Integer estructura : estructuras) {
				salida.append(estructura.toString()).append(";");
			}

			if (salida.length() != 0) {
				return salida.deleteCharAt(salida.lastIndexOf(";")).toString();
			} else {
				LOG.warn("Al generar la lista de estructuras no se encontró ';'.\nNo existen estructuras.");
				return null;
			} 
		} else {
			return null;
		}
	}

	public MapeoIntegracionInput getMappingColumn(List<MapeoIntegracionInput> lista, int id) {
		for (MapeoIntegracionInput mappedColumn : lista) {
			if (mappedColumn.getColumnaOrigen() == id) {
				return mappedColumn;
			}
		}
		return null;
	}

	public MapeoIntegracionInput getMappingColumn(List<MapeoIntegracionInput> lista,
			ColumnaTrabajoIntegracionInput columna) {
		for (MapeoIntegracionInput mappedColumn : lista) {
			if (mappedColumn.getColumnaOrigen().equals(columna.getColumnaOrigen())
					|| mappedColumn.getColumnaOrigen().equals(columna.getOrden())) {
				return mappedColumn;
			}
		}
		return null;
	}

	public String safeLongToStringParser(Long value) {
		if (null != value && value != 0) {
			return String.valueOf(value);
		}
		return "0";
	}

	public String safeIntToStringParser(Integer value) {
		if (null != value && value != 0) {
			return String.valueOf(value);
		}
		return "0";
	}
	
	public String concatenarStringsConDelimitador(List<String> strings, String delimitador) {
		String stringFinal;
		//concatena los datos de los mapeos
		StringBuilder sb = new StringBuilder();
		for (Iterator<String> it = strings.iterator(); it.hasNext();) {
			String str = it.next();
			if (str != null) {
				sb = sb.append(str).append(delimitador);
			}
		}
		sb.deleteCharAt(sb.lastIndexOf(delimitador));
		
		stringFinal = sb.toString();
		return stringFinal;
	}
}
