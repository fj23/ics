package cl.cardif.ics.icsrest.exceptions.icssubestructura;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
public class IcsSubestructuraMethodNotAllowedException extends RuntimeException {

  private static final long serialVersionUID = -6814524510236406273L;

  public IcsSubestructuraMethodNotAllowedException(String exception) {
    super(exception);
  }

  public IcsSubestructuraMethodNotAllowedException(String exception, Throwable t) {
    super(exception, t);
  }
}
