package cl.cardif.ics.icsrest.util.converters.topojo;

import javax.persistence.EntityNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import cl.cardif.ics.domain.entity.common.IcsTipoFormula;
import cl.cardif.ics.domain.entity.formatos.IcsDetalleFormato;
import cl.cardif.ics.domain.entity.homologaciones.IcsTipoHomologacion;
import cl.cardif.ics.domain.entity.plantillas.IcsAtributoUserIntg;
import cl.cardif.ics.domain.entity.plantillas.IcsAtributoUsuario;
import cl.cardif.ics.domain.entity.plantillas.IcsAtributosIntg;
import cl.cardif.ics.domain.entity.plantillas.IcsAtributosIntgEvento;
import cl.cardif.ics.icsrest.domain.dto.output.integracion.AtributosIntgOutput;

@Component
public class AtributoIntegracionConverter implements Converter<IcsAtributosIntg, AtributosIntgOutput> {
	private static final Logger LOG = LoggerFactory.getLogger(AtributoIntegracionConverter.class);

	@Override
	public AtributosIntgOutput convert(IcsAtributosIntg entity) {
		AtributosIntgOutput pojo = new AtributosIntgOutput();
		pojo.setIdAtributoIntegracion(entity.getIdAtributoIntegracion());
		pojo.setIdPlantilla(entity.getIcsPlantilla().getIdPlantilla());
		pojo.setFlgObligatorio(entity.getFlgObligatorio());

		IcsAtributoUsuario icsAtributoUsuario = entity.getIcsAtributoUsuario();
		if (icsAtributoUsuario != null) {
			pojo.setIdAtributoUsuario(icsAtributoUsuario.getIdAtributoUsuario());
			pojo.setIdAtributoNegocio(icsAtributoUsuario.getIcsAtributoNegocio().getIdAtributoNegocio());
		}

		IcsAtributoUserIntg icsAtributoUserIntg = entity.getIcsAtributoUserIntg();
		if (icsAtributoUserIntg != null) {
			pojo.setIdAtributoUser(icsAtributoUserIntg.getIdAtributoUser());
		}

		IcsAtributosIntgEvento icsAtributosIntgEvento = entity.getIcsAtributosIntgEvento();
		if (icsAtributosIntgEvento != null) {
			pojo.setIdIntegracionEvento(icsAtributosIntgEvento.getIdIntegracionEvento());
		}

		try {
			IcsTipoFormula icsTipoFormula = entity.getIcsTipoFormula();
			if (icsTipoFormula != null) {
				pojo.setIdTipoFormula(icsTipoFormula.getIdTipoFormula());
			}
		} catch (EntityNotFoundException e) {
			LOG.warn("No hay formula asociada", e);
		}

		try {
			IcsDetalleFormato icsDetalleFormato = entity.getIcsDetalleFormato();
			if (icsDetalleFormato != null) {
				pojo.setIdFormato(icsDetalleFormato.getIdDetalleFormato());
			}
		} catch (EntityNotFoundException e) {
			LOG.warn("No hay formato asociado", e);
		}

		try {
			IcsTipoHomologacion icsTipoHomologacion = entity.getIcsTipoHomologacion();
			if (icsTipoHomologacion != null) {
				pojo.setIdTipoHomologacion(icsTipoHomologacion.getIdDeTipoHomologacion());
			}
		} catch (EntityNotFoundException e) {
			LOG.warn("No hay homologacion asociado", e);
		}

		return pojo;
	}
}
