package cl.cardif.ics.icsrest.exceptions.icsestructura;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.GONE)
public class IcsEstructuraGoneException extends RuntimeException {

  private static final long serialVersionUID = -7209750264039464453L;

  public IcsEstructuraGoneException(String exception) {
    super(exception);
  }

  public IcsEstructuraGoneException(String exception, Throwable t) {
    super(exception, t);
  }
}
