package cl.cardif.ics.icsrest.formula.funciones;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import cl.cardif.ics.icsrest.formula.interfaces.IFormulaFactor;
import cl.cardif.ics.icsrest.formula.interfaces.IFormulaParent;
import cl.cardif.ics.icsrest.formula.interfaces.IMathFactor;
import cl.cardif.ics.icsrest.formula.interfaces.IStringFactor;
import cl.cardif.ics.icsrest.util.FormulaOrigenKey;

public class Max implements IFormulaParent, IMathFactor, IStringFactor {
	public final static String tipo = "Max";
	public final static int maxHijos = 1;
	private List<IFormulaFactor> hijos;

	public Max() {
		this.hijos = new ArrayList<IFormulaFactor>();
	}

	@Override
	public Collection<? extends IFormulaFactor> getHijos() {
		return new ArrayList<>(hijos);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void setHijos(Collection<? extends IFormulaFactor> hijos) {
		this.hijos = (List<IFormulaFactor>) hijos;
	}

	@Override
	public String toSafeSQL(Map<FormulaOrigenKey, Integer> tableAliases) {
		String sql = "MAX";

		sql += "(";
		sql += this.hijos.get(0).toSafeSQL(tableAliases);
		sql += ")";

		return sql;
	}

	@Override
	public String toSQL(Map<FormulaOrigenKey, Integer> tableAliases) {
		String sql = "MAX";

		sql += "(";
		sql += this.hijos.get(0).toSQL(tableAliases);
		sql += ")";

		return sql;
	}

	@Override
	public String childrenToJSON() {
		String json = "";

		if (this.hijos != null && this.hijos.size() > 0) {
			String values = "\"hijos\":[";

			for (int i = 0; i < this.hijos.size(); i++) {

				IFormulaFactor operator = this.hijos.get(i);
				values += operator.toJSON();

				if (i + 1 == Max.maxHijos) {
					break;
				} else if (i + 1 < this.hijos.size()) {
					values += ",";
				}
			}

			values += "]";

			json += values;
		}

		return json;
	}

	@Override
	public String toJSON() {
		String json = "\"tipo\":\"" + Max.tipo + "\"";

		if (!this.hijos.isEmpty()) {
			json += ",";
			json += this.childrenToJSON();
		}

		return "{" + json + "}";
	}
}
