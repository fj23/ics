package cl.cardif.ics.icsrest.exceptions.icsproceso;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class IcsProcesoUnauthorizedException extends RuntimeException {

  private static final long serialVersionUID = 3815682526998866691L;

  public IcsProcesoUnauthorizedException(String exception) {
    super(exception);
  }

  public IcsProcesoUnauthorizedException(String exception, Throwable t) {
    super(exception, t);
  }
}
