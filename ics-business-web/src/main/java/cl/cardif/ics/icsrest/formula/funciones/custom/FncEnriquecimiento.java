package cl.cardif.ics.icsrest.formula.funciones.custom;

import java.util.ArrayList;
import java.util.Map;

import cl.cardif.ics.icsrest.formula.FormulaParent;
import cl.cardif.ics.icsrest.formula.IFormulaFactor;
import cl.cardif.ics.icsrest.formula.IStringFactor;
import cl.cardif.ics.icsrest.util.FormulaOrigenKey;

/*
    FUNCTION fnc_enriquecimiento (
        in_valor VARCHAR2,
        in_id_enriquecimiento VARCHAR2
    ) RETURN VARCHAR2;
*/
public class FncEnriquecimiento extends FormulaParent implements IStringFactor {
	private Integer atrEnriquecimientoId;
	private Integer socioId;

	public FncEnriquecimiento() {
		hijos = new ArrayList<>();
	}

	@Override
	public int getMaximumChildrenCount() {
		return 3;
	}

	public Integer getAtrEnriquecimientoId() {
		return atrEnriquecimientoId;
	}

	public void setAtrEnriquecimientoId(Integer idEnriquecimiento) {
		this.atrEnriquecimientoId = idEnriquecimiento;
	}

	public Integer getSocioId() {
		return socioId;
	}

	public void setSocioId(Integer socioId) {
		this.socioId = socioId;
	}

	@Override
	public String toSafeSQL(Map<FormulaOrigenKey, Integer> tableAliases) {
		return "1";
	}

	@Override
	public String toSQL(Map<FormulaOrigenKey, Integer> tableAliases) {
		StringBuilder sb = new StringBuilder()
			.append("PCK_UTIL_ICS.FNC_ENRIQUECIMIENTO_WEB")
			.append("(")
			.append(this.socioId.toString()).append(", ")
			.append(getHijo(0).toSQL(tableAliases)).append(", ");
		
		IFormulaFactor poliza = getHijo(1);
		IFormulaFactor rut = getHijo(2);
		
		if (poliza != null) {
			sb.append(poliza.toSQL(tableAliases));
		} else {
			sb.append("null");
		}
		sb.append(", ");
		
		if (rut != null) {
			sb.append(rut.toSQL(tableAliases));
		} else {
			sb.append("null");
		}
		sb.append(", ");
		
		
		sb.append(atrEnriquecimientoId)
			.append(")");

		return sb.toString();
	}

	@Override
	public String toJSON() {
		StringBuilder sb = new StringBuilder()
				.append("{")
				.append("\"tipo\":\"FncEnriquecimiento\"");

		if (!hijos.isEmpty()) {
			sb.append(",")
				.append(this.childrenToJSON()).append(",")
				.append("\"atrEnriquecimientoId\":").append(atrEnriquecimientoId).append(",")
				.append("\"socioId\":").append(socioId);
		}
		
		sb.append("}");

		return sb.toString();
	}
}
