package cl.cardif.ics.icsrest.exceptions.icsdetallesubestructura;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class IcsDetalleSubestructuraUnauthorizedException extends RuntimeException {

  private static final long serialVersionUID = 3815682526998866691L;

  public IcsDetalleSubestructuraUnauthorizedException(String exception) {
    super(exception);
  }
}
