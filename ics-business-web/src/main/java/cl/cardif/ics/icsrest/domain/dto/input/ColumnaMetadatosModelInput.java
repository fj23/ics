package cl.cardif.ics.icsrest.domain.dto.input;

public class ColumnaMetadatosModelInput extends ColumnaMetadatos {
  public ColumnaMetadatosModelInput() {
    super();
  }

  public ColumnaMetadatosModelInput(long id) {
    super();
    this.id = id;
  }
}
