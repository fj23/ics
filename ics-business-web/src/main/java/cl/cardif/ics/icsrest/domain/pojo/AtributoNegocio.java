package cl.cardif.ics.icsrest.domain.pojo;

public class AtributoNegocio {

  private long idAtributoNegocio;
  private long idEvento;
  private String nombreColumnaNegocio;
  private String nombreTablaDestino;

  public AtributoNegocio() {
    // Constructor JavaBean
  }

  public long getIdAtributoNegocio() {
    return idAtributoNegocio;
  }

  public void setIdAtributoNegocio(long idAtributoNegocio) {
    this.idAtributoNegocio = idAtributoNegocio;
  }

  public long getIdEvento() {
    return idEvento;
  }

  public void setIdEvento(long idEvento) {
    this.idEvento = idEvento;
  }

  public String getNombreColumnaNegocio() {
    return nombreColumnaNegocio;
  }

  public void setNombreColumnaNegocio(String nombreColumnaNegocio) {
    this.nombreColumnaNegocio = nombreColumnaNegocio;
  }

  public String getNombreTablaDestino() {
    return nombreTablaDestino;
  }

  public void setNombreTablaDestino(String nombreTablaDestino) {
    this.nombreTablaDestino = nombreTablaDestino;
  }
}
