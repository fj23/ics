package cl.cardif.ics.icsrest.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityNotFoundException;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Service;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.OrderSpecifier;

import cl.cardif.ics.domain.entity.common.IcsCore;
import cl.cardif.ics.domain.entity.common.IcsSocio;
import cl.cardif.ics.domain.entity.homologaciones.IcsDetalleHomologacion;
import cl.cardif.ics.domain.entity.homologaciones.IcsTipoHomologacion;
import cl.cardif.ics.domain.entity.homologaciones.QIcsDetalleHomologacion;
import cl.cardif.ics.domain.entity.homologaciones.QIcsTipoHomologacion;
import cl.cardif.ics.icsrest.domain.dto.output.TipoHomologacionOutput;
import cl.cardif.ics.icsrest.domain.pojo.Homologacion;
import cl.cardif.ics.icsrest.exceptions.icsdetallehomologacion.IcsDetalleHomologacionNotFoundException;
import cl.cardif.ics.icsrest.repository.DetallesHomologacionesRepository;
import cl.cardif.ics.icsrest.repository.TiposHomologacionRepository;
import cl.cardif.ics.icsrest.service.HomologacionesService;
import cl.cardif.ics.icsrest.util.Constants;
import cl.cardif.ics.icsrest.util.OutputListWrapper;

@Service
public class HomologacionesServiceImpl implements HomologacionesService {
	private static final Logger LOG = LoggerFactory.getLogger(HomologacionesServiceImpl.class);

	private static final String NOMBRE_SOCIO1 = "icsTipoHomologacion.icsSocio1.nombreSocio";
	private static final String NOMBRE_SOCIO2 = "icsTipoHomologacion.icsSocio2.nombreSocio";
	private static final String NOMBRE_CORE1 = "icsTipoHomologacion.icsCore1.nombreCore";
	private static final String NOMBRE_CORE2 = "icsTipoHomologacion.icsCore2.nombreCore";
	private static final String NOMBRE_HOMOLOGACION = "icsTipoHomologacion.nombreHomologacion";

	private JdbcTemplate jdbcTemplate;

	@Autowired
	private TiposHomologacionRepository tipoHomologacionesRepository;
	@Autowired
	private DetallesHomologacionesRepository detalleHomologacionesRepository;
	@Autowired
	private ConversionService conversionService;

	@Override
	public void activarHomologacion(Homologacion detalle) {

		IcsDetalleHomologacion entity = null;
		try {
			entity = this.detalleHomologacionesRepository.getOne(detalle.getId());
			entity.setVigenciaHomologacion("Y");
			detalleHomologacionesRepository.saveAndFlush(entity);
		} catch (EntityNotFoundException e) {
			throw new IcsDetalleHomologacionNotFoundException("Detalle homologacion no encontrada", e);
		}
	}

	@Override
	public void actualizarHomologacion(Homologacion detalle) {

		TypeDescriptor sourceType = TypeDescriptor.valueOf(Homologacion.class);
		TypeDescriptor targetType = TypeDescriptor.valueOf(IcsDetalleHomologacion.class);
		IcsDetalleHomologacion entity = (IcsDetalleHomologacion) conversionService.convert(detalle, sourceType,
				targetType);

		try {
			detalleHomologacionesRepository.saveAndFlush(entity);
		} catch (Exception e) {
			throw new IcsDetalleHomologacionNotFoundException("La homologación no se pudo guardar", e);
		}
	}

	@Override
	public Long almacenarHomologacion(Homologacion detalle, boolean verificacionAmbosLados) {

		Long idTipo = this.existeTipoHomologacion(detalle, verificacionAmbosLados);
		if (idTipo == 0L) {
			idTipo = this.almacenarNuevoTipo(detalle);
		} else {
			detalle.setIdTipo(idTipo);

			List<IcsDetalleHomologacion> detallesFiltrados = null;
			try {
				detallesFiltrados = this.getDetallesHomologacion(detalle);
			} catch (Exception e) {
				LOG.error("Hubo un error al consultar la homologación", e);
			}

			if (detallesFiltrados == null || !detallesFiltrados.isEmpty()) {
				return null;
			}
		}

		try {
			detalle.setIdTipo(idTipo);
			IcsDetalleHomologacion detalleEntity = this.almacenarNuevoDetalle(detalle);
			if (detalleEntity != null) {
				return detalleEntity.getIdHomologacion();
			} else {
				return 0L;
			}
		} catch (Exception e) {
			tipoHomologacionesRepository.delete(idTipo);
			throw new IcsDetalleHomologacionNotFoundException("Hay problemas al almacenar la homologación", e);
		}
	}

	private IcsDetalleHomologacion almacenarNuevoDetalle(Homologacion detalle) {

		try {
			IcsDetalleHomologacion detalleEntity = new IcsDetalleHomologacion();
			IcsTipoHomologacion tipoEntity = new IcsTipoHomologacion();
			tipoEntity.setIdDeTipoHomologacion(detalle.getIdTipo());

			detalleEntity.setIcsTipoHomologacion(tipoEntity);
			detalleEntity.setValorSocio(this.treatValues(detalle.getValor1()));
			detalleEntity.setValorCore(this.treatValues(detalle.getValor2()));
			detalleEntity.setVigenciaHomologacion("Y");
			detalleEntity = detalleHomologacionesRepository.save(detalleEntity);

			return detalleEntity;
		} catch (Exception e) {
			LOG.error("Excepción en almacenamiento de nuevo detalle de homologación", e);
			return null;
		}

	}

	private String treatValues(String value) {
		char lf = 0x0A;
		char cr = 0x0D;
		if (value != null) {
			value = value.trim().replace(lf, ' ').replace(cr, ' ').trim();
		}
		return value;
	}

	private long almacenarNuevoTipo(Homologacion detalle) {

		IcsTipoHomologacion entity = this.buildIcsTipoHomologacion(detalle);

		try {
			tipoHomologacionesRepository.save(entity);
			return entity.getIdDeTipoHomologacion();
		} catch (Exception e) {
			LOG.error("Excepción en almacenamiento de nuevo tipo de homologación", e);
			return 0L;
		}
	}

	/**
	 * Construye un tipo de homologación (JPA Entity) a partir de una Homologacion
	 * (POJO)
	 *
	 * @param detalle
	 *            Homologación base.
	 * @return IcsTipoHomologacion
	 */
	private IcsTipoHomologacion buildIcsTipoHomologacion(Homologacion detalle) {
		IcsTipoHomologacion entity = new IcsTipoHomologacion();

		IcsSocio socioTmp = new IcsSocio();
		socioTmp.setIdSocio(detalle.getSocio1().getId());
		socioTmp.setNombreSocio(detalle.getSocio1().getNombre());
		socioTmp.setVigenciaSocio(detalle.getSocio1().getVigencia());
		entity.setIcsSocio1(socioTmp);

		socioTmp = new IcsSocio();
		socioTmp.setIdSocio(detalle.getSocio2().getId());
		socioTmp.setNombreSocio(detalle.getSocio2().getNombre());
		socioTmp.setVigenciaSocio(detalle.getSocio2().getVigencia());
		entity.setIcsSocio2(socioTmp);

		IcsCore coreTmp = new IcsCore();
		coreTmp.setIdCore(detalle.getCore1().getId());
		coreTmp.setNombreCore(detalle.getCore1().getNombre());
		coreTmp.setVigenciaCore(detalle.getCore1().getVigencia());
		coreTmp.setFlgVisible(detalle.getCore1().getFlgVisible());
		entity.setIcsCore1(coreTmp);

		coreTmp = new IcsCore();
		coreTmp.setIdCore(detalle.getCore2().getId());
		coreTmp.setNombreCore(detalle.getCore2().getNombre());
		coreTmp.setVigenciaCore(detalle.getCore2().getVigencia());
		coreTmp.setFlgVisible(detalle.getCore2().getFlgVisible());
		entity.setIcsCore2(coreTmp);

		entity.setNombreHomologacion(detalle.getConcepto());
		entity.setVigenciaTipoHomologacion("Y");
		return entity;
	}

	private Sort buildSort(String sortColumn, String sortOrder) {

		Sort.Direction dir;
		String entitySortField;

		switch (sortColumn) {
		case "socio1":
			entitySortField = NOMBRE_SOCIO1;
			break;
		case "socio2":
			entitySortField = NOMBRE_SOCIO2;
			break;
		case "core1":
			entitySortField = NOMBRE_CORE1;
			break;
		case "core2":
			entitySortField = NOMBRE_CORE2;
			break;
		case "concepto":
			entitySortField = NOMBRE_HOMOLOGACION;
			break;
		default:
			return null;
		}

		if ("desc".equalsIgnoreCase(sortOrder)) {
			dir = Sort.Direction.DESC;
		} else {
			dir = Sort.Direction.ASC;
		}

		return new Sort(dir, entitySortField);
	}

	public BigDecimal callSpTipoCreado(Homologacion homologacion, boolean verificacionAmbosLados) {

		SimpleJdbcCall call = new SimpleJdbcCall(jdbcTemplate).withCatalogName(Constants.PACKAGE_UTIL_ICS)
				.withProcedureName(Constants.PROCEDURE_VALID_HOMO_TYPE).declareParameters(
						new SqlParameter(Constants.PV_VALID_HOMO_TYPE_SISTEMA1, oracle.jdbc.OracleTypes.NUMBER),
						new SqlParameter(Constants.PV_VALID_HOMO_TYPE_SISTEMA2, oracle.jdbc.OracleTypes.NUMBER),
						new SqlParameter(Constants.PV_VALID_HOMO_TYPE_SOCIO1, oracle.jdbc.OracleTypes.NUMBER),
						new SqlParameter(Constants.PV_VALID_HOMO_TYPE_SOCIO1, oracle.jdbc.OracleTypes.NUMBER),
						new SqlParameter(Constants.PV_VALID_HOMO_TYPE_NOMBRE_TIPO, oracle.jdbc.OracleTypes.VARCHAR),
						new SqlParameter(Constants.PV_VALID_HOMO_TYPE_TIPO_VALIDACION, oracle.jdbc.OracleTypes.NUMBER),
						new SqlOutParameter(Constants.PV_VALID_HOMO_TYPE_OUT_ID_TIPO, oracle.jdbc.OracleTypes.NUMBER));

		Map<String, Object> map = new HashMap<>();

		map.put(Constants.PV_VALID_HOMO_TYPE_SISTEMA1, homologacion.getCore1().getId());
		map.put(Constants.PV_VALID_HOMO_TYPE_SISTEMA2, homologacion.getCore2().getId());
		map.put(Constants.PV_VALID_HOMO_TYPE_SOCIO1, homologacion.getSocio1().getId());
		map.put(Constants.PV_VALID_HOMO_TYPE_SOCIO2, homologacion.getSocio2().getId());
		map.put(Constants.PV_VALID_HOMO_TYPE_NOMBRE_TIPO, homologacion.getConcepto());
		map.put(Constants.PV_VALID_HOMO_TYPE_TIPO_VALIDACION, verificacionAmbosLados ? 1 : 0);
		map.put(Constants.PV_VALID_HOMO_TYPE_OUT_ID_TIPO, 0);

		try {
			Map<String, Object> execute = call.execute(new MapSqlParameterSource(map));
			return (BigDecimal) execute.get(Constants.PV_VALID_HOMO_TYPE_OUT_ID_TIPO);
		} catch (Exception e) {
			return BigDecimal.valueOf(0);
		}
	}

	@Override
	public void desactivarHomologacion(Homologacion detalle) {

		IcsDetalleHomologacion entity = null;
		try {
			entity = this.detalleHomologacionesRepository.getOne(detalle.getId());
		} catch (EntityNotFoundException e) {
			throw new IcsDetalleHomologacionNotFoundException("Detalle homologacion no encontrada", e);
		}

		entity.setVigenciaHomologacion("N");
		detalleHomologacionesRepository.saveAndFlush(entity);
	}

	/**
	 * Llama a un Stored Procedure que valida si existe el tipo de la homologación
	 * base.
	 * 
	 * @param homologacion
	 *            Homologación base.
	 * @param verificacionAmbosLados
	 *            Parametro que indica si se debe validar el tipo en ambos lados.
	 * @return Long Resultado de la ejecución del SP.
	 */
	public Long existeTipoHomologacion(Homologacion homologacion, boolean verificacionAmbosLados) {

		try {
			BigDecimal result = this.callSpTipoCreado(homologacion, verificacionAmbosLados);
			return result.longValue();
		} catch (Exception e) {
			LOG.error("Error al verificar la existencia de un tipo de homologacion", e);
			return 0L;
		}
	}

	@Override
	public List<String> getAllDistinctConceptosHomologacion() {

		return tipoHomologacionesRepository.findDistinctConceptos();
	}

	private BooleanBuilder generateQueryFilter(Map<String, String> filtroMap) {
		QIcsDetalleHomologacion qIcsDetalleHomologacion = QIcsDetalleHomologacion.icsDetalleHomologacion;
		QIcsTipoHomologacion qIcsTipoHomologacion = qIcsDetalleHomologacion.icsTipoHomologacion;

		String filterKeyConcepto = "concepto";
		String filterKeySistema1 = "sistema1";
		String filterKeySistema2 = "sistema2";
		String filterKeySocio1 = "socio1";
		String filterKeySocio2 = "socio2";
		String filterKeyValorSocio = "valorSocio";
		String filterKeyValorCore = "valorCore";
		String filterKeyVigencia = "vigencia";

		BooleanBuilder bb = new BooleanBuilder();

		if (filtroMap.containsKey(filterKeyConcepto)) {
			String nombreHomologacion = filtroMap.get(filterKeyConcepto);
			if (nombreHomologacion != null && !nombreHomologacion.trim().isEmpty()) {
				bb.and(qIcsTipoHomologacion.nombreHomologacion.eq(nombreHomologacion));
			}
		}

		if (filtroMap.containsKey(filterKeySistema1) && !filtroMap.get(filterKeySistema1).isEmpty()) {
			try {
				Long idCore1 = Long.valueOf(filtroMap.get(filterKeySistema1));
				bb.and(qIcsTipoHomologacion.icsCore1.idCore.eq(idCore1));
			} catch (NumberFormatException e) {
				LOG.warn("Error en el filtro de sistema1", e);
			}
		}

		if (filtroMap.containsKey(filterKeySistema2) && !filtroMap.get(filterKeySistema2).isEmpty()) {
			try {
				Long idCore2 = Long.valueOf(filtroMap.get(filterKeySistema2));
				bb.and(qIcsTipoHomologacion.icsCore2.idCore.eq(idCore2));
			} catch (NumberFormatException e) {
				LOG.warn("Error en el filtro de sistema2", e);
			}
		}

		if (filtroMap.containsKey(filterKeySocio1)) {
			try {
				Long idSocio1 = Long.valueOf(filtroMap.get(filterKeySocio1));
				bb.and(qIcsTipoHomologacion.icsSocio1.idSocio.eq(idSocio1));
			} catch (NumberFormatException e) {
				LOG.warn("Error en el filtro de socio1", e);
			}
		}

		if (filtroMap.containsKey(filterKeySocio2)) {
			try {
				Long idSocio2 = Long.valueOf(filtroMap.get(filterKeySocio2));
				bb.and(qIcsTipoHomologacion.icsSocio2.idSocio.eq(idSocio2));
			} catch (NumberFormatException e) {
				LOG.warn("Error en el filtro de socio2", e);
			}
		}

		if (filtroMap.containsKey(filterKeyValorCore) && !filtroMap.get(filterKeyValorCore).isEmpty()) {
			bb.and(qIcsDetalleHomologacion.valorCore.eq(filtroMap.get(filterKeyValorCore)));
		}

		if (filtroMap.containsKey(filterKeyValorSocio) && !filtroMap.get(filterKeyValorSocio).isEmpty()) {
			bb.and(qIcsDetalleHomologacion.valorSocio.eq(filtroMap.get(filterKeyValorSocio)));
		}

		if (filtroMap.containsKey(filterKeyVigencia) && !filtroMap.get(filterKeyVigencia).isEmpty()) {
			bb.and(qIcsDetalleHomologacion.vigenciaHomologacion.eq(filtroMap.get(filterKeyVigencia)));
		}
		return bb;
	}

	@Override
	public OutputListWrapper<Homologacion> getHomologaciones(int page, int size, String sortColumn, String sortOrder,
			Map<String, String> filtroMap) {

		Pageable paginador;

		if (sortColumn.isEmpty()) {
			paginador = new PageRequest(page, size);
		} else {
			Sort sorter = buildSort(sortColumn, sortOrder);
			paginador = new PageRequest(page, size, sorter);
		}

		BooleanBuilder bb = generateQueryFilter(filtroMap);

		long countHomologaciones = detalleHomologacionesRepository.count(bb);
		Iterable<IcsDetalleHomologacion> pageHomologaciones = detalleHomologacionesRepository.findAll(bb, paginador);

		List<Homologacion> lista = new ArrayList<>();
		for (IcsDetalleHomologacion icsDetalleHomologacion : pageHomologaciones) {
			Homologacion homologacion = conversionService.convert(icsDetalleHomologacion, Homologacion.class);
			lista.add(homologacion);
		}

		return new OutputListWrapper<>(lista, (int) countHomologaciones, "");
	}

	@Override
	public List<TipoHomologacionOutput> getTiposHomologaciones(int socioId, int coreId) {

		BooleanBuilder bb = new BooleanBuilder();
		if (socioId != 0) {
			bb.and(QIcsTipoHomologacion.icsTipoHomologacion.icsSocio1.idSocio.eq((long) socioId));
		}
		if (coreId != 0) {
			bb.and(QIcsTipoHomologacion.icsTipoHomologacion.icsCore2.idCore.eq((long) coreId));
		}

		OrderSpecifier<String> orderByNombre = QIcsTipoHomologacion.icsTipoHomologacion.nombreHomologacion.asc();

		Iterable<IcsTipoHomologacion> iteraHomologaciones = tipoHomologacionesRepository.findAll(bb, orderByNombre);

		List<TipoHomologacionOutput> tiposHomologaciones = new ArrayList<>();
		for (IcsTipoHomologacion icsTipoHomologacion : iteraHomologaciones) {
			TipoHomologacionOutput tipoHomologacion = conversionService.convert(icsTipoHomologacion,
					TipoHomologacionOutput.class);
			tiposHomologaciones.add(tipoHomologacion);
		}

		return tiposHomologaciones;
	}

	@Autowired
	public void setDataSource(DataSource dataSource) {

		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	/**
	 * Obtiene las homologaciones que tengan el mismo tipo y valor 1 (valor socio)
	 * que la ingresada
	 * 
	 * @param homologacion
	 * @return
	 */
	private List<IcsDetalleHomologacion> getDetallesHomologacion(Homologacion homologacion) {
		QIcsDetalleHomologacion qIcsDetalle = QIcsDetalleHomologacion.icsDetalleHomologacion;
		QIcsTipoHomologacion qIcsTipo = qIcsDetalle.icsTipoHomologacion;

		BooleanBuilder bb = new BooleanBuilder().and(qIcsDetalle.valorSocio.eq(homologacion.getValor1()));

		if (homologacion.getIdTipo() != 0L) {
			bb.and(qIcsTipo.idDeTipoHomologacion.eq(homologacion.getIdTipo()));
		} else {
			bb.and(qIcsTipo.nombreHomologacion.eq(homologacion.getConcepto()))
					.and(qIcsTipo.icsCore1.idCore.eq(homologacion.getCore1().getId()))
					.and(qIcsTipo.icsCore2.idCore.eq(homologacion.getCore2().getId()))
					.and(qIcsTipo.icsSocio1.idSocio.eq(homologacion.getSocio1().getId()))
					.and(qIcsTipo.icsSocio2.idSocio.eq(homologacion.getSocio2().getId()));
		}

		Iterable<IcsDetalleHomologacion> iterable = detalleHomologacionesRepository.findAll(bb);

		List<IcsDetalleHomologacion> entities = new ArrayList<>();
		for (IcsDetalleHomologacion icsDetalleHomologacion : iterable) {
			entities.add(icsDetalleHomologacion);
		}

		return entities;
	}
}
