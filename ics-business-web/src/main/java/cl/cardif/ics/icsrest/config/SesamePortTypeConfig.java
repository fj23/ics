package cl.cardif.ics.icsrest.config;

import javax.annotation.PostConstruct;
import javax.xml.ws.BindingProvider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import cl.cardif.service.ebsc.authentication.v1.AuthenticationPortType;
import cl.cardif.service.ebsc.authentication.v1.AuthenticationService;
import cl.cardif.service.ebsc.authorization.v1.AuthorizationPortType;
import cl.cardif.service.ebsc.authorization.v1.AuthorizationService;

@Configuration
public class SesamePortTypeConfig {
	private static final Logger LOG = LoggerFactory.getLogger(SesamePortTypeConfig.class);

	@Autowired
	private Environment environment;

	@PostConstruct
	public void printInfo() {
		LOG.info("printInfo: URL de autenticacion: {}", this.environment.getProperty("ESB_ADDRESS_AUTHENTICATION"));
		LOG.info("printInfo: URL de autorizacion: {}", this.environment.getProperty("ESB_ADDRESS_AUTHORIZATION"));
	}

	@Bean
	public AuthenticationPortType authenticationPortType() {
		LOG.info("authenticationPortType");

		try {
			AuthenticationService service = new AuthenticationService();

			final String serviceDebugLog = "authenticationPortType - Service Name: " + service.getServiceName();
			LOG.debug(serviceDebugLog);

			AuthenticationPortType port = service.getAuthenticationPort();
			BindingProvider bp = (BindingProvider) port;

			bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
					environment.getProperty("ESB_ADDRESS_AUTHENTICATION"));
			return port;

		} catch (Exception e) {
			LOG.error("Error en servicio de autenticacion", e);
			return null;
		}
	}

	@Bean
	public AuthorizationPortType authorizationPortType() {
		LOG.info("authorizationPortType");

		try {

			AuthorizationService service = new AuthorizationService();

			final String serviceDebugLog = "authorizationPortType - Service Name: " + service.getServiceName();
			LOG.debug(serviceDebugLog);

			AuthorizationPortType port = service.getAuthorizationPort();
			BindingProvider bp = (BindingProvider) port;

			bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
					environment.getProperty("ESB_ADDRESS_AUTHORIZATION"));
			return port;

		} catch (Exception e) {
			LOG.error("Error en servicio de autorizacion", e);
			return null;
		}
	}

}
