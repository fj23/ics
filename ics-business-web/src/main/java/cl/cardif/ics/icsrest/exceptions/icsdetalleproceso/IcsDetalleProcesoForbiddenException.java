package cl.cardif.ics.icsrest.exceptions.icsdetalleproceso;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class IcsDetalleProcesoForbiddenException extends RuntimeException {

  private static final long serialVersionUID = 3397992475249736227L;

  public IcsDetalleProcesoForbiddenException(String exception) {
    super(exception);
  }

  public IcsDetalleProcesoForbiddenException(String exception, Throwable t) {
    super(exception, t);
  }
}
