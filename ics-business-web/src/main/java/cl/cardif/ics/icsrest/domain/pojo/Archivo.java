package cl.cardif.ics.icsrest.domain.pojo;

import cl.cardif.ics.icsrest.domain.dto.input.ColumnaMetadatosSalidaModel;
import cl.cardif.ics.icsrest.domain.dto.output.AtributosSalidaOutput;
import cl.cardif.ics.icsrest.domain.dto.output.EstructuraOutput;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Archivo {

	private long idArchivo;
	private String delimitador;
	private String formulaNombre;
	private TipoExtension tipoExtension;
	private EstructurasAgrupada idGrupoEstructuras;
	private BigDecimal lineaFinal;
	private BigDecimal lineaInicial;
	private String nombreArchivo;
	private String tipoArchivo;
	private String vigenciaArchivo;
	private Long numeroHoja;
	private List<ColumnaMetadatosSalidaModel> columnas;
	private List<EstructuraOutput> estructurasOutput;
	private List<AtributosSalidaOutput> columnasSalida;
	private String flgSinTransformacion;
	private String valorFiltrado;

	public Archivo() {
		super();
	}

	public List<ColumnaMetadatosSalidaModel> getColumnas() {
		if (columnas != null) {
			return new ArrayList<>(columnas);
		}
		else {
			return new ArrayList<>();
		}
	}

	public void setColumnas(List<ColumnaMetadatosSalidaModel> columnas) {
		if (columnas != null) {
			this.columnas = new ArrayList<>(columnas);
		}
	}

	public List<AtributosSalidaOutput> getColumnasSalida() {
		if (columnasSalida != null) {
			return new ArrayList<>(columnasSalida);
		}
		else {
			return new ArrayList<>();
		}
	}

	public void setColumnasSalida(List<AtributosSalidaOutput> columnasSalida) {
		if (columnasSalida != null) {
			this.columnasSalida = new ArrayList<>(columnasSalida);
		}
	}

	public String getDelimitador() {
		return delimitador;
	}

	public void setDelimitador(String delimitador) {
		this.delimitador = delimitador;
	}

	public List<EstructuraOutput> getEstructurasOutput() {
		if (estructurasOutput != null) {
			return new ArrayList<>(estructurasOutput);
		}
		else {
			return new ArrayList<>();
		}
	}

	public void setEstructurasOutput(List<EstructuraOutput> estructurasOutput) {
		this.estructurasOutput = new ArrayList<>(estructurasOutput);
	}

	public String getFormulaNombre() {
		return formulaNombre;
	}

	public void setFormulaNombre(String formulaNombre) {
		this.formulaNombre = formulaNombre;
	}

	public long getIdArchivo() {
		return idArchivo;
	}

	public void setIdArchivo(long idArchivo) {
		this.idArchivo = idArchivo;
	}

	public EstructurasAgrupada getIdGrupoEstructuras() {
		return idGrupoEstructuras;
	}

	public void setIdGrupoEstructuras(EstructurasAgrupada idGrupoEstructuras) {
		this.idGrupoEstructuras = idGrupoEstructuras;
	}

	public BigDecimal getLineaFinal() {
		return lineaFinal;
	}

	public void setLineaFinal(BigDecimal lineaFinal) {
		this.lineaFinal = lineaFinal;
	}

	public BigDecimal getLineaInicial() {
		return lineaInicial;
	}

	public void setLineaInicial(BigDecimal lineaInicial) {
		this.lineaInicial = lineaInicial;
	}

	public String getNombreArchivo() {
		return nombreArchivo;
	}

	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	public Long getNumeroHoja() {
		return numeroHoja;
	}

	public void setNumeroHoja(Long numeroHoja) {
		this.numeroHoja = numeroHoja;
	}

	public String getTipoArchivo() {
		return tipoArchivo;
	}

	public void setTipoArchivo(String tipoArchivo) {
		this.tipoArchivo = tipoArchivo;
	}

	public TipoExtension getTipoExtension() {
		return tipoExtension;
	}

	public void setTipoExtension(TipoExtension idExtension) {
		this.tipoExtension = idExtension;
	}

	public String getVigenciaArchivo() {
		return vigenciaArchivo;
	}

	public void setVigenciaArchivo(String vigenciaArchivo) {
		this.vigenciaArchivo = vigenciaArchivo;
	}

	public String getFlgSinTransformacion() {
		return flgSinTransformacion;
	}

	public void setFlgSinTransformacion(String flgSinTransformacion) {
		this.flgSinTransformacion = flgSinTransformacion;
	}

	public String getValorFiltrado() {
		return valorFiltrado;
	}

	public void setValorFiltrado(String valorFiltrado) {
		this.valorFiltrado = valorFiltrado;
	}
}
