package cl.cardif.ics.icsrest.util.converters.topojo;

import cl.cardif.ics.domain.entity.common.IcsTipoDato;
import cl.cardif.ics.domain.entity.estructuras.IcsDetalleEstructura;
import cl.cardif.ics.domain.entity.estructuras.IcsEstructura;
import cl.cardif.ics.icsrest.domain.dto.output.DetalleEstructuraOutput;
import cl.cardif.ics.icsrest.domain.dto.output.EstructuraOutput;
import cl.cardif.ics.icsrest.domain.pojo.TipoDato;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

@Component
public class EstructurasConverter implements Converter<IcsEstructura, EstructuraOutput> {

  static final Logger LOG = LoggerFactory.getLogger(EstructurasConverter.class);

  @Override
  public EstructuraOutput convert(IcsEstructura icsEstructura) {
    LOG.debug("Convert icsestructra: " + icsEstructura.getIdEstructura());

    EstructuraOutput estructura = new EstructuraOutput();
    estructura.setId(icsEstructura.getIdEstructura());
    estructura.setDescripcion(icsEstructura.getDescripcionEstructura());
    estructura.setCodigo(icsEstructura.getCodigoEstructura());
    estructura.setCardMax(icsEstructura.getCardinalidadMaximaEstructura().longValue());
    estructura.setCardMin(icsEstructura.getCardinalidadMinimaEstructura().longValue());
    estructura.setFlgSatelite(icsEstructura.getFlgSatelite());
    estructura.setVigencia(icsEstructura.getVigenciaEstructura());

    List<DetalleEstructuraOutput> detalles = new ArrayList<>();
    
    int largo = 0;

    for (int i = 0; i < icsEstructura.getIcsDetalleEstructuras().size(); i++) {
      IcsDetalleEstructura entity = icsEstructura.getIcsDetalleEstructuras().get(i);
      DetalleEstructuraOutput detalle = new DetalleEstructuraOutput();
      detalle.setId(entity.getIdDetalleEstructura());
      detalle.setDescripcion(entity.getDescripcionDetalleEstructura());
      detalle.setCodigo(entity.getCodigoEstructura());
      detalle.setIdEstructuraPadre(entity.getIcsEstructura().getIdEstructura());

      try {
        if (entity.getIcsTipoDato() != null) {
          IcsTipoDato icsTipoDato = entity.getIcsTipoDato();
          TipoDato tipoDato = new TipoDato();
          tipoDato.setId(icsTipoDato.getIdTipoDato());
          tipoDato.setNombre(icsTipoDato.getNombreTipoDato());

          detalle.setTipoDato(tipoDato);
        }
      } catch (EntityNotFoundException e) {
        LOG.info("IcsTipoDato", e);
      }

      detalle.setLargo(entity.getLargo().longValue());
      detalle.setOrden(entity.getOrden().longValue());
      detalle.setVigencia(entity.getVigenciaDetalleEstructura());
      detalles.add(detalle);
      
      largo += entity.getLargo().longValue();
    }

    estructura.setLargo(largo);
    estructura.setColumnas(detalles);

    return estructura;
  }
}
