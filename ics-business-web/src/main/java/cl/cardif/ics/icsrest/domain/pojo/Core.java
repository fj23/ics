package cl.cardif.ics.icsrest.domain.pojo;

import cl.cardif.ics.icsrest.domain.dto.input.BaseModel;

public class Core extends BaseModel {

  private String flgVisible;

  public Core() {
    // Constructor JavaBean
  }

  public String getFlgVisible() {
    return flgVisible;
  }

  public void setFlgVisible(String flgVisible) {
    this.flgVisible = flgVisible;
  }
}
