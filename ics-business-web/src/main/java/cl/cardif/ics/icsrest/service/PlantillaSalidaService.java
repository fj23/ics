package cl.cardif.ics.icsrest.service;

import java.util.Collection;
import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;

import cl.cardif.ics.icsrest.domain.dto.input.EstructuraAnetoPlantillaSalidaModel;
import cl.cardif.ics.icsrest.domain.dto.input.PlantillaSalidaInput;
import cl.cardif.ics.icsrest.domain.dto.output.AtributoSalidaEstrucOutput;
import cl.cardif.ics.icsrest.domain.dto.output.PlantillaOutput;
import cl.cardif.ics.icsrest.domain.dto.output.PlantillaSalidaOutput;
import cl.cardif.ics.icsrest.domain.dto.output.RequestResultOutput;
import cl.cardif.ics.icsrest.domain.pojo.Core;
import cl.cardif.ics.icsrest.domain.pojo.DirectorioSalida;
import cl.cardif.ics.icsrest.util.Permisos;

public interface PlantillaSalidaService {

	@PreAuthorize(value = "hasAnyAuthority("
	        + Permisos.Mant_Plan_Alta + ","
	        + Permisos.Mant_Plan_Canc + ","
	        + Permisos.Mant_Plan_Reca + ","
            + Permisos.Mant_Plan_Prosp + ","
            + Permisos.Mant_Plan_Intera + ","
            + Permisos.Mant_Plan_Pago + ")")
	RequestResultOutput addOrUpdatePlantillaSalida(PlantillaSalidaInput plantilla, boolean isUpdate);

	@PreAuthorize(value = "hasAnyAuthority("
	        + Permisos.Cslt_Plan + ","
	        + Permisos.Mant_Plan_Alta + ","
	        + Permisos.Mant_Plan_Canc + ","
	        + Permisos.Mant_Plan_Reca + ","
            + Permisos.Mant_Plan_Prosp + ","
            + Permisos.Mant_Plan_Intera + ","
            + Permisos.Mant_Plan_Pago + ")")
	PlantillaSalidaOutput viewTemplateDetalle(PlantillaOutput plantilla);

	@PreAuthorize(value = "hasAnyAuthority("
	        + Permisos.Cslt_Plan + ","
	        + Permisos.Mant_Plan_Alta + ","
	        + Permisos.Mant_Plan_Canc + ","
	        + Permisos.Mant_Plan_Reca + ","
            + Permisos.Mant_Plan_Prosp + ","
            + Permisos.Mant_Plan_Intera + ","
            + Permisos.Mant_Plan_Pago + ")")
	Collection<AtributoSalidaEstrucOutput> consultarAtributosOrigenEstructuras();

	@PreAuthorize(value = "hasAnyAuthority("
	        + Permisos.Mant_Plan_Alta + ","
	        + Permisos.Mant_Plan_Canc + ","
	        + Permisos.Mant_Plan_Reca + ","
            + Permisos.Mant_Plan_Prosp + ","
            + Permisos.Mant_Plan_Intera + ","
            + Permisos.Mant_Plan_Pago + ")")
	Collection<Core> consultarCoresHomologacionesEstructuras();

	@PreAuthorize(value = "hasAnyAuthority("
	        + Permisos.Cslt_Plan + ","
	        + Permisos.Mant_Plan_Alta + ","
	        + Permisos.Mant_Plan_Canc + ","
	        + Permisos.Mant_Plan_Reca + ","
            + Permisos.Mant_Plan_Prosp + ","
            + Permisos.Mant_Plan_Intera + ","
            + Permisos.Mant_Plan_Pago + ")")
	List<EstructuraAnetoPlantillaSalidaModel> obtenerEstructurasPlantillaSalida(Long idPlantilla);

	@PreAuthorize(value = "hasAnyAuthority("
	        + Permisos.Cslt_Plan + ","
	        + Permisos.Mant_Plan_Alta + ","
	        + Permisos.Mant_Plan_Canc + ","
	        + Permisos.Mant_Plan_Reca + ","
            + Permisos.Mant_Plan_Prosp + ","
            + Permisos.Mant_Plan_Intera + ","
            + Permisos.Mant_Plan_Pago + ")")
	List<DirectorioSalida> consultarDirectoriosSalida();
}
