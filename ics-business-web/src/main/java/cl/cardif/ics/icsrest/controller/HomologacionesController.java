package cl.cardif.ics.icsrest.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import cl.cardif.ics.icsrest.domain.dto.output.TipoHomologacionOutput;
import cl.cardif.ics.icsrest.domain.pojo.Homologacion;
import cl.cardif.ics.icsrest.exceptions.icsdetallehomologacion.IcsDetalleHomologacionMethodNotAllowedException;
import cl.cardif.ics.icsrest.exceptions.icsdetallehomologacion.IcsDetalleHomologacionNotFoundException;
import cl.cardif.ics.icsrest.service.HomologacionesService;
import cl.cardif.ics.icsrest.util.Constants;
import cl.cardif.ics.icsrest.util.OutputListWrapper;

@RestController
@RequestMapping("/api/homologaciones")
public class HomologacionesController {
	private static final Logger LOG = LoggerFactory.getLogger(HomologacionesController.class);
	@Autowired HomologacionesService homologacionesService;

	@PostMapping("/activar")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void activarDetalleHomologacion(@RequestBody Homologacion homologacion) {
		LOG.info("activarDetalleHomologacion");
		homologacionesService.activarHomologacion(homologacion);
	}

	@PutMapping("/actualizar")
	public ResponseEntity<Void> actualizarHomologacion(@RequestBody Homologacion homologacion) {
		LOG.info("actualizarHomologacion");
		boolean error = false;

		try {
			homologacionesService.actualizarHomologacion(homologacion);
		} catch (IcsDetalleHomologacionNotFoundException e) {
			LOG.error("Error al actualizar homologacion", e);
			error = true;
		}

		if (error) {
			return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
		}

		return ResponseEntity.ok().build();
	}

	@PostMapping("/nuevo_grupo")
	@ResponseStatus(HttpStatus.CREATED)
	public List<String> almacenarGrupoHomologaciones(@RequestBody List<Homologacion> lista) {
		LOG.info("almacenarGrupoHomologaciones");

		List<String> respuestas = new ArrayList<>();
		if (lista != null && !lista.isEmpty()) {
			for (Homologacion homologacion : lista) {
				Long respAlmacenar = homologacionesService.almacenarHomologacion(homologacion, false);
				if (respAlmacenar == null) {
					respuestas.add("El valor de entrada " + homologacion.getValor1() + " ya existe para este grupo de homologaciones.");
				}
			}
		}
		return respuestas;
	}

	@PostMapping("/nueva")
	public ResponseEntity<Void> almacenarHomologacion(@RequestBody Homologacion homologacion) {
		LOG.info("almacenarHomologacion");

		int error = 0;
		try {
			Long idCreada = homologacionesService.almacenarHomologacion(homologacion, true);
			if (idCreada == null || idCreada == 0) {
				error = -2;
			}
		} catch (IcsDetalleHomologacionNotFoundException e) {
			LOG.error("IcsDetalleHomologacionNotFoundException", e);
			error = -1;
		} catch (IcsDetalleHomologacionMethodNotAllowedException e) {
			LOG.error("IcsDetalleHomologacionMethodNotAllowedException", e);
			error = -2;
		}

		if (error == -1) {
			return ResponseEntity.notFound().build();
		} else if (error == -2) {
			return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
		}

		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@PostMapping("/desactivar")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void desactivarDetalleHomologacion(@RequestBody Homologacion homologacion) {
		LOG.info("desactivarDetalleHomologacion");
		homologacionesService.desactivarHomologacion(homologacion);
	}

	@GetMapping("/conceptos")
	public List<String> getConceptos() {
		LOG.info("getConceptos");
		return homologacionesService.getAllDistinctConceptosHomologacion();
	}

	@GetMapping("/")
	public ResponseEntity<OutputListWrapper<Homologacion>> getHomologaciones() {
		LOG.info("getHomologaciones");
		try {
			OutputListWrapper<Homologacion> lista =
				homologacionesService.getHomologaciones(Constants.DEFAULT_PAGE, Constants.DEFAULT_PAGE_SIZE, "", "", null);
			return ResponseEntity.ok(lista);
		} catch (IcsDetalleHomologacionNotFoundException e) {
			LOG.error("Homologacion no encontrada", e);
			return ResponseEntity.notFound().build();
		}
	}

	@GetMapping("/page/{page}")
	public OutputListWrapper<Homologacion> getHomologaciones(@PathVariable int page) {
		return this.getHomologaciones(page, Constants.DEFAULT_PAGE_SIZE, "", "", null);
	}

	@GetMapping("/page/{page}/size/{size}")
	public OutputListWrapper<Homologacion> getHomologaciones(@PathVariable int page, @PathVariable int size) {
		return this.getHomologaciones(page, size, "", "", null);
	}

	@GetMapping("/filters")
	public OutputListWrapper<Homologacion> getHomologaciones(@RequestParam Map<String, String> filters) {
		return this.getHomologaciones(Constants.DEFAULT_PAGE, Constants.DEFAULT_PAGE_SIZE, "", "", filters);
	}

	@GetMapping("/page/{page}/size/{size}/filters")
	public OutputListWrapper<Homologacion> getHomologaciones(
		@RequestParam Map<String, String> filters, @PathVariable int page, @PathVariable int size) {
		return this.getHomologaciones(page, size, "", "", filters);
	}

	@GetMapping("/page/{page}/size/{size}/sort/{sortColumn}/{sortOrder}/filters")
	public OutputListWrapper<Homologacion> getHomologaciones(
		@PathVariable int page, @PathVariable int size, @PathVariable String sortColumn, @PathVariable String sortOrder,
		@RequestParam Map<String, String> filters) {
		LOG.info("getHomologaciones");
		return homologacionesService.getHomologaciones(page, size, sortColumn, sortOrder, filters);
	}

	@GetMapping("/tipos/{socioId}/{coreId}")
	public List<TipoHomologacionOutput> getTiposHomologaciones(@PathVariable int socioId, @PathVariable int coreId) {
		LOG.info("getTiposHomologaciones");
		return homologacionesService.getTiposHomologaciones(socioId, coreId);
	}
}
