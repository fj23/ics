package cl.cardif.ics.icsrest.util.converters.topojo;

import cl.cardif.ics.domain.entity.plantillas.IcsAtributoEnriquecimiento;
import cl.cardif.ics.icsrest.domain.dto.output.AtributoEnriquecimientoOutput;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class AtributoEnriquecimientoConverter
    implements Converter<IcsAtributoEnriquecimiento, AtributoEnriquecimientoOutput> {

  @Override
  public AtributoEnriquecimientoOutput convert(IcsAtributoEnriquecimiento entity) {
    AtributoEnriquecimientoOutput pojo = new AtributoEnriquecimientoOutput();
    pojo.setIdAtributoEnriquecimiento(entity.getIdAtributoEnriquecimiento());
    pojo.setIdCore(entity.getIdCore().longValue());
    pojo.setNombreTabla(entity.getNombreTabla());
    pojo.setNombreColumna(entity.getNombreColumna());
    return pojo;
  }
}
