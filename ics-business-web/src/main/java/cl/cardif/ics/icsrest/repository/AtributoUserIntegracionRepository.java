package cl.cardif.ics.icsrest.repository;

import cl.cardif.ics.domain.entity.plantillas.IcsAtributoUserIntg;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface AtributoUserIntegracionRepository
    extends JpaRepository<IcsAtributoUserIntg, Long>,
        QueryDslPredicateExecutor<IcsAtributoUserIntg> {
	
}
