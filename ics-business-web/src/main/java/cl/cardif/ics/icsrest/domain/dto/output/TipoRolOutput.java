package cl.cardif.ics.icsrest.domain.dto.output;

import cl.cardif.ics.icsrest.domain.pojo.DetalleRol;

import java.util.Map;

public class TipoRolOutput extends TipoRolBase {

	private Map<String, DetalleRol> detalles;

	public TipoRolOutput() {
		super();
	}

	public Map<String, DetalleRol> getDetalles() {
		return detalles;
	}

	public void setDetalles(Map<String, DetalleRol> detalles) {
		this.detalles = detalles;
	}
}
