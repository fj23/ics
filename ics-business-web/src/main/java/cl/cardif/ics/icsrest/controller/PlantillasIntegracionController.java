package cl.cardif.ics.icsrest.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import cl.cardif.ics.icsrest.domain.dto.input.PlantillaIntegracionInput;
import cl.cardif.ics.icsrest.domain.dto.output.AtributoUserIntgOutput;
import cl.cardif.ics.icsrest.domain.dto.output.AtributoUsuarioOutput;
import cl.cardif.ics.icsrest.domain.dto.output.RequestResultOutput;
import cl.cardif.ics.icsrest.exceptions.icsplantilla.IcsPlantillaServiceException;
import cl.cardif.ics.icsrest.service.AtributosService;
import cl.cardif.ics.icsrest.service.PlantillaIntegracionService;

@RestController
@RequestMapping("/api/plantillas/integracion")
public class PlantillasIntegracionController {
	private static final Logger LOG = LoggerFactory.getLogger(PlantillasIntegracionController.class);

	@Autowired PlantillaIntegracionService plantillaIntegracionService;
	@Autowired AtributosService atributosService;

	@PostMapping("/actualizar")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<RequestResultOutput> actualizarPlantilla(@RequestBody PlantillaIntegracionInput input) {
		LOG.info("actualizarPlantilla");
		try {
			RequestResultOutput result = plantillaIntegracionService.addOrUpdatePlantillaIntegracion(input);
			return new ResponseEntity<>(result, HttpStatus.OK);
		} catch (IcsPlantillaServiceException e) {
			LOG.error("No se pudo actualizar la plantilla", e);
			return ResponseEntity.badRequest().build();
		}
	}

	@PostMapping("/nueva")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<RequestResultOutput> almacenarPlantilla(@RequestBody PlantillaIntegracionInput input) {
		LOG.info("almacenarPlantilla");
		try {
			RequestResultOutput result = plantillaIntegracionService.addOrUpdatePlantillaIntegracion(input);
			return new ResponseEntity<>(result, HttpStatus.OK);
		} catch (IcsPlantillaServiceException e) {
			LOG.error("No se pudo crear la plantilla", e);
			return ResponseEntity.badRequest().build();
		}
	}

	/**
	 * Intenta validar un string JSON como objeto formula, y valida que su consulta
	 * SQL derivada sea correcta.
	 *
	 * @param input
	 *            String JSON.
	 * @return boolean Respuesta indicando si es valida.
	 */
	@PostMapping("/verificar_formula")
	@ResponseStatus(HttpStatus.OK)
	public boolean verificarFormula(@RequestBody String input) {
		LOG.info("verificarFormula");
		return plantillaIntegracionService.testFormulaIntegracion(input);
	}

	@GetMapping("/atributos_destino/{idEvento}")
	@ResponseStatus(HttpStatus.OK)
	public List<AtributoUserIntgOutput> obtenerAtributosUserParaEvento(@PathVariable long idEvento) {
		LOG.info("obtenerAtributosUserParaEvento");
		return atributosService.obtenerAtributosDestinoIntegracionPorEvento(idEvento);
	}

	@GetMapping("/atributos_origen_plantilla_entrada/{idPlantilla}")
	@ResponseStatus(HttpStatus.OK)
	public List<AtributoUsuarioOutput> obtenerAtributosUsuarioDesdePlantillaEntrada(@PathVariable long idPlantilla) {
		LOG.info("obtenerAtributosUsuarioDesdePlantillaEntrada");
		return plantillaIntegracionService.getAtributosUsuarioFromPlantillaEntradaMapeos(idPlantilla);
	}
}
