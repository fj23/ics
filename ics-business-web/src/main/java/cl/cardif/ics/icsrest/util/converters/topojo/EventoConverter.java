package cl.cardif.ics.icsrest.util.converters.topojo;

import cl.cardif.ics.domain.entity.common.IcsEvento;
import cl.cardif.ics.icsrest.domain.pojo.Evento;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class EventoConverter implements Converter<IcsEvento, Evento> {

  @Override
  public Evento convert(IcsEvento icsEvento) {
    Evento evento = new Evento();
    evento.setId(icsEvento.getIdEvento());
    evento.setNombre(icsEvento.getNombreEvento());
    evento.setVigencia(icsEvento.getVigenciaEvento());
    return evento;
  }
}
