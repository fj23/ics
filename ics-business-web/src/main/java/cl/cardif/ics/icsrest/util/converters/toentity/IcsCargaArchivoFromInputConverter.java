package cl.cardif.ics.icsrest.util.converters.toentity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import cl.cardif.ics.domain.entity.carga.IcsCargaArchivo;
import cl.cardif.ics.icsrest.domain.dto.input.CargaArchivoInput;

@Component
public class IcsCargaArchivoFromInputConverter implements Converter<CargaArchivoInput, IcsCargaArchivo> {
	static final Logger LOG = LoggerFactory.getLogger(IcsCargaArchivoFromInputConverter.class);

	@Override
	public IcsCargaArchivo convert(CargaArchivoInput input) {

		return null;
	}
}
