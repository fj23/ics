package cl.cardif.ics.icsrest.domain.dto.output;

public class DetalleProcesoOutput {
  private long id;
  private long idProceso;
  private PlantillaOutput plantilla;

  public DetalleProcesoOutput() {
    super();
  }

  public DetalleProcesoOutput(long id, long idProceso, PlantillaOutput plantilla) {
    super();
    this.id = id;
    this.idProceso = idProceso;
    this.plantilla = plantilla;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public long getIdProceso() {
    return idProceso;
  }

  public void setIdProceso(long idProceso) {
    this.idProceso = idProceso;
  }

  public PlantillaOutput getPlantilla() {
    return plantilla;
  }

  public void setPlantilla(PlantillaOutput plantilla) {
    this.plantilla = plantilla;
  }
}
