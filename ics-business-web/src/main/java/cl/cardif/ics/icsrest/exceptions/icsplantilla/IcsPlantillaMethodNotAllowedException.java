package cl.cardif.ics.icsrest.exceptions.icsplantilla;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
public class IcsPlantillaMethodNotAllowedException extends RuntimeException {

  private static final long serialVersionUID = -6814524510236406273L;

  public IcsPlantillaMethodNotAllowedException(String exception) {
    super(exception);
  }

  public IcsPlantillaMethodNotAllowedException(String exception, Throwable t) {
    super(exception, t);
  }
}
