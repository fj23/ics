package cl.cardif.ics.icsrest.exceptions.icscargaarchivo;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
public class IcsCargaArchivoMethodNotAllowedException extends RuntimeException {

  private static final long serialVersionUID = -6814524510236406273L;

  public IcsCargaArchivoMethodNotAllowedException(String exception) {
    super(exception);
  }

  public IcsCargaArchivoMethodNotAllowedException(String exception, Throwable t) {
    super(exception, t);
  }
}
