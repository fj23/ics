package cl.cardif.ics.icsrest.util.converters.topojo;

import cl.cardif.ics.domain.entity.common.IcsSocio;
import cl.cardif.ics.icsrest.domain.pojo.Socio;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class SocioConverter implements Converter<IcsSocio, Socio> {

	@Override
	public Socio convert(IcsSocio icsSocio) {
		Socio socio = new Socio();
		socio.setId(icsSocio.getIdSocio());
		socio.setNombre(icsSocio.getNombreSocio());
		socio.setVigencia(icsSocio.getVigenciaSocio());
		return socio;
	}
}
