package cl.cardif.ics.icsrest.domain.dto.input;

import cl.cardif.ics.icsrest.domain.pojo.DetalleEstructura;
import cl.cardif.ics.icsrest.formula.IFormulaFactor;

public class DetalleEstructuraPlantillaSalidaInput extends DetalleEstructura {
	
	private Long idDetalleEstructura;
	private Long idEstructura;
	private EstructuraAnetoPlantillaSalidaModel subestructura;
	private String codigoColumna;
	private String descripcionColumna;
	private Long idAtributoDiccionario;
	private String valorFijo;
	private Long idTipoHomologacion;
	private Long idTipoFormato;
	private IFormulaFactor formula;
	private Integer ordenHomologacion;
	private Integer ordenFormato;
	private Integer ordenFormula;
	
	public DetalleEstructuraPlantillaSalidaInput() {
		super();
	}

	public Long getIdDetalleEstructura() {
		return idDetalleEstructura;
	}

	public void setIdDetalleEstructura(Long idDetalleEstructura) {
		this.idDetalleEstructura = idDetalleEstructura;
	}

	public Long getIdEstructura() {
		return idEstructura;
	}

	public void setIdEstructura(Long idEstructura) {
		this.idEstructura = idEstructura;
	}

	public String getCodigoColumna() {
		return codigoColumna;
	}

	public void setCodigoColumna(String codigoColumna) {
		this.codigoColumna = codigoColumna;
	}

	public String getDescripcionColumna() {
		return descripcionColumna;
	}

	public void setDescripcionColumna(String descripcionColumna) {
		this.descripcionColumna = descripcionColumna;
	}

	public Long getIdAtributoDiccionario() {
		return idAtributoDiccionario;
	}

	public void setIdAtributoDiccionario(Long iAtributoDiccionario) {
		this.idAtributoDiccionario = iAtributoDiccionario;
	}

	public String getValorFijo() {
		return valorFijo;
	}

	public void setValorFijo(String valorFijo) {
		this.valorFijo = valorFijo;
	}

	public Long getIdTipoHomologacion() {
		return idTipoHomologacion;
	}

	public void setIdTipoHomologacion(Long idTipoHomologacion) {
		this.idTipoHomologacion = idTipoHomologacion;
	}

	public Long getIdTipoFormato() {
		return idTipoFormato;
	}

	public void setIdTipoFormato(Long idTipoFormato) {
		this.idTipoFormato = idTipoFormato;
	}

	public IFormulaFactor getFormula() {
		return formula;
	}

	public void setFormula(IFormulaFactor formula) {
		this.formula = formula;
	}

	public Integer getOrdenHomologacion() {
		return ordenHomologacion;
	}

	public void setOrdenHomologacion(Integer ordenHomologacion) {
		this.ordenHomologacion = ordenHomologacion;
	}

	public Integer getOrdenFormato() {
		return ordenFormato;
	}

	public void setOrdenFormato(Integer ordenFormato) {
		this.ordenFormato = ordenFormato;
	}

	public Integer getOrdenFormula() {
		return ordenFormula;
	}

	public void setOrdenFormula(Integer ordenFormula) {
		this.ordenFormula = ordenFormula;
	}

	public EstructuraAnetoPlantillaSalidaModel getSubestructura() {
		return subestructura;
	}

	public void setSubestructura(EstructuraAnetoPlantillaSalidaModel subestructura) {
		this.subestructura = subestructura;
	}
	
}
