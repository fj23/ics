package cl.cardif.ics.icsrest.util.converters.topojo;

import cl.cardif.ics.domain.entity.common.IcsTipoDato;
import cl.cardif.ics.domain.entity.estructuras.IcsDetalleEstructura;
import cl.cardif.ics.domain.entity.estructuras.IcsSubestructura;
import cl.cardif.ics.icsrest.domain.dto.output.DetalleEstructuraOutput;
import cl.cardif.ics.icsrest.domain.pojo.TipoDato;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;

@Component
public class DetallesEstructurasConverter 
	implements Converter<IcsDetalleEstructura, DetalleEstructuraOutput> {
	private static final Logger LOG = LoggerFactory.getLogger(DetallesEstructurasConverter.class);

	@Override
	public DetalleEstructuraOutput convert(IcsDetalleEstructura icsEstructura) {

		DetalleEstructuraOutput detalle = new DetalleEstructuraOutput();
		detalle.setId(icsEstructura.getIdDetalleEstructura());
		detalle.setCodigo(icsEstructura.getCodigoEstructura());
		detalle.setDescripcion(icsEstructura.getDescripcionDetalleEstructura());
		detalle.setLargo(icsEstructura.getLargo().longValue());
		detalle.setOrden(icsEstructura.getOrden().longValue());
		detalle.setVigencia(icsEstructura.getVigenciaDetalleEstructura());
		detalle.setIdEstructuraPadre(icsEstructura.getIcsEstructura().getIdEstructura());

		IcsSubestructura icsSubestr = icsEstructura.getIcsSubestructura();
		if (icsSubestr != null) {
			LOG.info("La columna posee una subestructura");
			detalle.setIdSubEstructura(icsSubestr.getIdSubestructura());
		}

		try {
			IcsTipoDato icsTipoDato = icsEstructura.getIcsTipoDato();
			if (icsTipoDato != null) {
				TipoDato tipoDato = new TipoDato();
				tipoDato.setId(icsTipoDato.getIdTipoDato());
				tipoDato.setNombre(icsTipoDato.getNombreTipoDato());

				detalle.setTipoDato(tipoDato);
			}
		} catch (EntityNotFoundException e) {
			LOG.error("El tipo de dato de la columna no pudo ser encontrado", e);
		}

		return detalle;
	}
}
