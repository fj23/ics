package cl.cardif.ics.icsrest.util.converters.topojo;

import cl.cardif.ics.domain.entity.plantillas.IcsAtributoNegocio;
import cl.cardif.ics.icsrest.domain.dto.output.AtributoNegocioOutput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class AtributoNegocioConverter
    implements Converter<IcsAtributoNegocio, AtributoNegocioOutput> {

  static final Logger LOG = LoggerFactory.getLogger(AtributoNegocioConverter.class);

  @Override
  public AtributoNegocioOutput convert(IcsAtributoNegocio entity) {
    AtributoNegocioOutput pojo = new AtributoNegocioOutput();
    pojo.setIdAtributoNegocio(entity.getIdAtributoNegocio());

    LOG.debug("Evento obtenido en ics atributo : " + entity.getIcsEvento().getIdEvento());

    pojo.setIdEvento(entity.getIcsEvento().getIdEvento());
    pojo.setNombreColumnaNegocio(entity.getNombreColumnaNegocio());
    pojo.setNombreTablaDestino(entity.getNombreTablaDestino());
    return pojo;
  }


}
