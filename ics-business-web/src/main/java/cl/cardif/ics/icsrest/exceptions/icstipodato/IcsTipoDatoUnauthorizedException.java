package cl.cardif.ics.icsrest.exceptions.icstipodato;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class IcsTipoDatoUnauthorizedException extends RuntimeException {

  private static final long serialVersionUID = 3815682526998866691L;

  public IcsTipoDatoUnauthorizedException(String exception) {
    super(exception);
  }

  public IcsTipoDatoUnauthorizedException(String exception, Throwable t) {
    super(exception, t);
  }
}
