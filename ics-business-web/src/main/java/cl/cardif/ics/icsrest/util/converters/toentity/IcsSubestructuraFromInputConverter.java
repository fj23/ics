package cl.cardif.ics.icsrest.util.converters.toentity;

import cl.cardif.ics.domain.entity.estructuras.IcsSubestructura;
import cl.cardif.ics.icsrest.domain.dto.input.SubEstructuraInput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class IcsSubestructuraFromInputConverter
    implements Converter<SubEstructuraInput, IcsSubestructura> {

  static final Logger LOG = LoggerFactory.getLogger(IcsSubestructuraFromInputConverter.class);

  @Override
  public IcsSubestructura convert(SubEstructuraInput pojo) {

    IcsSubestructura entity = new IcsSubestructura();

    entity.setCodigoSubestructura(pojo.getCodigo());
    entity.setDescripcionSubestructura(pojo.getDescripcion());
    return entity;
  }
}
