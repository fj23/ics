package cl.cardif.ics.icsrest.exceptions.icsproceso;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class IcsProcesoBadRequestException extends RuntimeException {

  private static final long serialVersionUID = -6459654653826812166L;

  public IcsProcesoBadRequestException(String exception) {
    super(exception);
  }

  public IcsProcesoBadRequestException(String exception, Throwable t) {
    super(exception, t);
  }
}
