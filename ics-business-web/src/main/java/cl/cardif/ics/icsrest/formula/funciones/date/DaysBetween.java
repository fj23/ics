package cl.cardif.ics.icsrest.formula.funciones.date;

import java.util.ArrayList;
import java.util.Map;

import cl.cardif.ics.icsrest.formula.FormulaParent;
import cl.cardif.ics.icsrest.formula.IFormulaFactor;
import cl.cardif.ics.icsrest.formula.IMathFactor;
import cl.cardif.ics.icsrest.formula.operadores.SysDate;
import cl.cardif.ics.icsrest.util.FormulaOrigenKey;

public class DaysBetween extends FormulaParent implements IMathFactor {
	public DaysBetween() {
		hijos = new ArrayList<>();
	}

	@Override
	public int getMaximumChildrenCount() {
		return 2;
	}

	@Override
	public String toSafeSQL(Map<FormulaOrigenKey, Integer> tableAliases) {
		return "(SYSDATE - SYSDATE)";
	}

	@Override
	public String toSQL(Map<FormulaOrigenKey, Integer> tableAliases) {
		StringBuilder sb = new StringBuilder();
		
		IFormulaFactor dateFrom = getHijo(0);
		IFormulaFactor dateTo = getHijo(1);

		if (dateFrom instanceof SysDate) {
			sb.append(dateFrom.toSQL(tableAliases));
		} else {
			sb.append("TO_DATE")
				.append("(")
				.append(dateFrom.toSQL(tableAliases)).append(",")
				.append("'dd-mm-yyyy'")
				.append(")");
		}
		
		sb.append(" - ");

		
		if (dateTo instanceof SysDate) {
			sb.append(dateTo.toSQL(tableAliases));
		} else {
			sb.append("TO_DATE")
				.append("(")
				.append(dateTo.toSQL(tableAliases)).append(",")
				.append("'dd-mm-yyyy'")
				.append(")");
		}

		return "(" + sb.toString() + ")";
	}
}
