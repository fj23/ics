package cl.cardif.ics.icsrest.domain.pojo;

public class TipoAgrupacion {

  private long idAgrupacion;
  private String nombreAgrupacion;
  private String vigenciaAgrupacion;

  public TipoAgrupacion() {
    // Constructor JavaBean
  }

  public long getIdAgrupacion() {
    return this.idAgrupacion;
  }

  public void setIdAgrupacion(long idAgrupacion) {
    this.idAgrupacion = idAgrupacion;
  }

  public String getNombreAgrupacion() {
    return this.nombreAgrupacion;
  }

  public void setNombreAgrupacion(String nombreAgrupacion) {
    this.nombreAgrupacion = nombreAgrupacion;
  }

  public String getVigenciaAgrupacion() {
    return this.vigenciaAgrupacion;
  }

  public void setVigenciaAgrupacion(String vigenciaAgrupacion) {
    this.vigenciaAgrupacion = vigenciaAgrupacion;
  }
}
