package cl.cardif.ics.icsrest.exceptions.icstipodato;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class IcsTipoDatoNotFoundException extends RuntimeException {

  private static final long serialVersionUID = -1527596879157446910L;

  public IcsTipoDatoNotFoundException(String exception) {
    super(exception);
  }

  public IcsTipoDatoNotFoundException(String exception, Throwable t) {
    super(exception, t);
  }
}
