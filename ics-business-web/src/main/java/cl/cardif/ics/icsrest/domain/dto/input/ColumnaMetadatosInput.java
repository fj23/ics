package cl.cardif.ics.icsrest.domain.dto.input;

import java.util.ArrayList;
import java.util.List;

public class ColumnaMetadatosInput extends ColumnaMetadatos {
	private String valorParaFiltrar;
	private List<ColumnaMetadatosModelInput> columnas;

	public ColumnaMetadatosInput() {
		super();
	}

	public List<ColumnaMetadatosModelInput> getColumnas() {
		return new ArrayList<>(columnas);
	}

	public void setColumnas(List<ColumnaMetadatosModelInput> columnas) {
		this.columnas = new ArrayList<>(columnas);
	}

	public String getValorParaFiltrar() {
		return valorParaFiltrar;
	}

	public void setValorParaFiltrar(String valorParaFiltrar) {
		this.valorParaFiltrar = valorParaFiltrar;
	}
}
