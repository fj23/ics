package cl.cardif.ics.icsrest.exceptions.icsplantilla;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class IcsPlantillaForbiddenException extends RuntimeException {

  private static final long serialVersionUID = 3397992475249736227L;

  public IcsPlantillaForbiddenException(String exception) {
    super(exception);
  }

  public IcsPlantillaForbiddenException(String exception, Throwable t) {
    super(exception, t);
  }
}
