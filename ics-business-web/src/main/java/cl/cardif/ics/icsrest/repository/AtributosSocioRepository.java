package cl.cardif.ics.icsrest.repository;

import cl.cardif.ics.domain.entity.plantillas.IcsAtributosSocio;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface AtributosSocioRepository
    extends JpaRepository<IcsAtributosSocio, Long>, 
    	QueryDslPredicateExecutor<IcsAtributosSocio> {
	
}
