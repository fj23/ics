package cl.cardif.ics.icsrest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cl.cardif.ics.domain.entity.procesos.IcsJobs;

@Repository
public interface JobsRepository 
	extends JpaRepository<IcsJobs, Long> {

}
