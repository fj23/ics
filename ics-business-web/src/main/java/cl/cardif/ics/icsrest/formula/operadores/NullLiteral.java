package cl.cardif.ics.icsrest.formula.operadores;

import java.util.Map;

import cl.cardif.ics.icsrest.formula.FormulaFactor;
import cl.cardif.ics.icsrest.util.FormulaOrigenKey;

public class NullLiteral extends FormulaFactor {
	public NullLiteral() {
		super();
	}

	@Override
	public String toSafeSQL(Map<FormulaOrigenKey, Integer> tableAliases) {
		return "NULL";
	}

	@Override
	public String toSQL(Map<FormulaOrigenKey, Integer> tableAliases) {
		return "NULL";
	}

}
