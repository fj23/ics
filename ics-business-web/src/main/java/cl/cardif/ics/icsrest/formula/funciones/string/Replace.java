package cl.cardif.ics.icsrest.formula.funciones.string;

import java.util.ArrayList;
import java.util.Map;

import cl.cardif.ics.icsrest.formula.FormulaParent;
import cl.cardif.ics.icsrest.formula.IFormulaFactor;
import cl.cardif.ics.icsrest.formula.IStringFactor;
import cl.cardif.ics.icsrest.formula.operadores.NullLiteral;
import cl.cardif.ics.icsrest.formula.operadores.NumberLiteral;
import cl.cardif.ics.icsrest.util.FormulaOrigenKey;

public class Replace extends FormulaParent implements IStringFactor {
	public Replace() {
		hijos = new ArrayList<>();
	}

	@Override
	public int getMaximumChildrenCount() {
		return 3;
	}

	@Override
	public String toSafeSQL(Map<FormulaOrigenKey, Integer> tableAliases) {
		StringBuilder sb = new StringBuilder()
				.append("REPLACE")
				.append("(");
		
		IFormulaFactor cadena = getHijo(0);
		IFormulaFactor subcadena = getHijo(1);
		IFormulaFactor reemplazo = getHijo(2);

		if (cadena instanceof NumberLiteral) {
			sb.append("'").append(cadena.toSafeSQL(tableAliases)).append("'");
		} else {
			sb.append(cadena.toSafeSQL(tableAliases));
		}
		sb.append(",");
		

		if (subcadena instanceof NumberLiteral) {
			sb.append("'").append(subcadena.toSafeSQL(tableAliases)).append("'");
		} else {
			sb.append(subcadena.toSafeSQL(tableAliases));
		}
		sb.append(",");
		
		if (reemplazo instanceof NullLiteral) {
			sb.append("''");
		} else {
			sb.append(reemplazo.toSafeSQL(tableAliases));
		}
		sb.append(")");

		return sb.toString();
	}

	@Override
	public String toSQL(Map<FormulaOrigenKey, Integer> tableAliases) {
		StringBuilder sb = new StringBuilder()
				.append("REPLACE")
				.append("(");
		
		IFormulaFactor cadena = getHijo(0);
		IFormulaFactor subcadena = getHijo(1);
		IFormulaFactor reemplazo = getHijo(2);

		if (cadena instanceof NumberLiteral) {
			sb.append("'").append(cadena.toSQL(tableAliases)).append("'");
		} else {
			sb.append(cadena.toSafeSQL(tableAliases));
		}
		sb.append(",");
		

		if (subcadena instanceof NumberLiteral) {
			sb.append("'").append(subcadena.toSQL(tableAliases)).append("'");
		} else {
			sb.append(subcadena.toSafeSQL(tableAliases));
		}
		sb.append(",");
		
		if (reemplazo instanceof NullLiteral) {
			sb.append("''");
		} else {
			sb.append(reemplazo.toSQL(tableAliases));
		}
		sb.append(")");

		return sb.toString();
	}
}
