package cl.cardif.ics.icsrest.domain.dto.input;

import cl.cardif.ics.icsrest.domain.dto.output.AtributoEnriquecimientoOutput;
import cl.cardif.ics.icsrest.domain.dto.output.AtributoUserIntgOutput;
import cl.cardif.ics.icsrest.domain.dto.output.ColumnaDestinoEntradaModelOutput;

public class MapeoIntegracionInput {

	private Long id;
	private Long columnaDestino;
	private boolean esObligatorio;
	private boolean seValida;
	private Long columnaOrigen;

	/**
	 * Apunta hacia las columnas de la plantilla (es el indice de una columna del
	 * arreglo)
	 */
	private Long columnaTrabajoOrden;

	private ColumnaDestinoEntradaModelOutput columnaOrigenEM;
	private AtributoEnriquecimientoOutput columnaEnriquecimientoEM;
	private AtributoUserIntgOutput columnaDestinoEM;
	private Long IdAtributo;

	private Long idAtributoUser;

	public MapeoIntegracionInput() {
		super();
	}

	public Long getColumnaDestino() {
		return columnaDestino;
	}

	public void setColumnaDestino(Long columnaDestino) {
		this.columnaDestino = columnaDestino;
	}

	public AtributoUserIntgOutput getColumnaDestinoEM() {
		return columnaDestinoEM;
	}

	public void setColumnaDestinoEM(AtributoUserIntgOutput columnaDestinoEM) {
		this.columnaDestinoEM = columnaDestinoEM;
	}

	public AtributoEnriquecimientoOutput getColumnaEnriquecimientoEM() {
		return columnaEnriquecimientoEM;
	}

	public void setColumnaEnriquecimientoEM(AtributoEnriquecimientoOutput columnaEnriquecimientoEM) {
		this.columnaEnriquecimientoEM = columnaEnriquecimientoEM;
	}

	public Long getColumnaOrigen() {
		return columnaOrigen;
	}

	public void setColumnaOrigen(Long columnaOrigen) {
		this.columnaOrigen = columnaOrigen;
	}

	public ColumnaDestinoEntradaModelOutput getColumnaOrigenEM() {
		return columnaOrigenEM;
	}

	public void setColumnaOrigenEM(ColumnaDestinoEntradaModelOutput columnaOrigenEM) {
		this.columnaOrigenEM = columnaOrigenEM;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdAtributo() {
		return IdAtributo;
	}

	public void setIdAtributo(Long idAtributo) {
		IdAtributo = idAtributo;
	}

	public boolean isEsObligatorio() {
		return esObligatorio;
	}

	public void setEsObligatorio(boolean esObligatorio) {
		this.esObligatorio = esObligatorio;
	}

	public boolean isSeValida() {
		return seValida;
	}

	public void setSeValida(boolean seValida) {
		this.seValida = seValida;
	}

	public Long getColumnaTrabajoOrden() {
		return columnaTrabajoOrden;
	}

	public void setColumnaTrabajoOrden(Long columnaTrabajoOrden) {
		this.columnaTrabajoOrden = columnaTrabajoOrden;
	}

	public Long getIdAtributoUser() {
		return idAtributoUser;
	}

	public void setIdAtributoUser(Long idAtributoUser) {
		this.idAtributoUser = idAtributoUser;
	}
}
