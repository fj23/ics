package cl.cardif.ics.icsrest.domain.dto.output;

import cl.cardif.ics.icsrest.domain.pojo.Core;
import cl.cardif.ics.icsrest.domain.pojo.TipoExtension;

public class PlantillaIntegracionOutput extends PlantillaOutput {
	private TipoExtension tipoExtensionIntg;
	private Core coreDestino;
	private String delimitadorColumnas;
	
	public PlantillaIntegracionOutput() {
		super();
	}

	public TipoExtension getTipoExtensionIntg() {
		return tipoExtensionIntg;
	}

	public void setTipoExtensionIntg(TipoExtension tipoExtensionIntg) {
		this.tipoExtensionIntg = tipoExtensionIntg;
	}

	public Core getCoreDestino() {
		return coreDestino;
	}

	public void setCoreDestino(Core coreDestino) {
		this.coreDestino = coreDestino;
	}

	public String getDelimitadorColumnas() {
		return delimitadorColumnas;
	}

	public void setDelimitadorColumnas(String delimitadorColumnas) {
		this.delimitadorColumnas = delimitadorColumnas;
	}

	@Override
	public String toString() {
		return "PlantillaIntegracionOutput [tipoExtensionIntg=" + tipoExtensionIntg + ", coreDestino=" + coreDestino
			+ ", delimitadorColumnas=" + delimitadorColumnas + ", toString()=" + super.toString() + "]";
	}
}
