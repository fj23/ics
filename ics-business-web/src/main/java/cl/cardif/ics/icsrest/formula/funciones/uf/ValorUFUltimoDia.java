package cl.cardif.ics.icsrest.formula.funciones.uf;

import java.util.Map;

import cl.cardif.ics.icsrest.formula.FormulaFactor;
import cl.cardif.ics.icsrest.formula.IMathFactor;
import cl.cardif.ics.icsrest.util.FormulaOrigenKey;

public class ValorUFUltimoDia extends FormulaFactor implements IMathFactor {
	public ValorUFUltimoDia() {
		super();
	}

	public String toSafeSQL(Map<FormulaOrigenKey, Integer> tableAliases) {
		return "1";
	}

	public String toSQL(Map<FormulaOrigenKey, Integer> tableAliases) {
		return "PCK_UTIL_ICS.FNC_VALORUF_ULTIMODIA()";
	}
}
