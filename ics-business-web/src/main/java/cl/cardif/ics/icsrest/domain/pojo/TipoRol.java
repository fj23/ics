package cl.cardif.ics.icsrest.domain.pojo;

import cl.cardif.ics.icsrest.domain.dto.output.TipoRolBase;

import java.util.Map;

public class TipoRol extends TipoRolBase {
  private Map<Long, DetalleRol> detalles;

  public Map<Long, DetalleRol> getDetalles() {
    return detalles;
  }

  public void setDetalles(Map<Long, DetalleRol> detalles) {
    this.detalles = detalles;
  }

@Override
public String toString() {
	return "TipoRol [detalles=" + detalles + ", toString()=" + super.toString() + "]";
}
}
