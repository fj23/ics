package cl.cardif.ics.icsrest.domain.dto.output;

import cl.cardif.ics.icsrest.domain.dto.input.Proceso;

public class ProcesoOutput extends Proceso {
	private PlantillaOutput plantillaEntradaSocio;
	private PlantillaOutput plantillaIntegracion;
	private PlantillaOutput plantillaSalidaSocio;
	private boolean esSalidaAneto;
	private long callCenter;
	private long tipoArchivo;
	private long directorioSalidaError;

	public ProcesoOutput() {
		super();
	}

	public PlantillaOutput getPlantillaEntradaSocio() {
		return plantillaEntradaSocio;
	}

	public void setPlantillaEntradaSocio(PlantillaOutput plantillaEntradaSocio) {
		this.plantillaEntradaSocio = plantillaEntradaSocio;
	}

	public PlantillaOutput getPlantillaIntegracion() {
		return plantillaIntegracion;
	}

	public void setPlantillaIntegracion(PlantillaOutput plantillaIntegracion) {
		this.plantillaIntegracion = plantillaIntegracion;
	}

	public PlantillaOutput getPlantillaSalidaSocio() {
		return plantillaSalidaSocio;
	}

	public void setPlantillaSalidaSocio(PlantillaOutput plantillaSalidaSocio) {
		this.plantillaSalidaSocio = plantillaSalidaSocio;
	}

	public boolean isEsSalidaAneto() {
		return esSalidaAneto;
	}

	public void setEsSalidaAneto(boolean esSalidaAneto) {
		this.esSalidaAneto = esSalidaAneto;
	}

	public long getCallCenter() {
		return callCenter;
	}

	public void setCallCenter(long callCenter) {
		this.callCenter = callCenter;
	}

	public long getTipoArchivo() {
		return tipoArchivo;
	}

	public void setTipoArchivo(long tipoArchivo) {
		this.tipoArchivo = tipoArchivo;
	}

	public long getDirectorioSalidaError() {
		return directorioSalidaError;
	}

	public void setDirectorioSalidaError(long directorioSalidaError) {
		this.directorioSalidaError = directorioSalidaError;
	}

	@Override
	public String toString() {
		return "ProcesoOutput [plantillaEntradaSocio=" + plantillaEntradaSocio + 
				", plantillaIntegracion=" + plantillaIntegracion + 
				", plantillaSalidaSocio=" + plantillaSalidaSocio +
				", " + super.toString()
				+ "]";
	}
	
	
}
