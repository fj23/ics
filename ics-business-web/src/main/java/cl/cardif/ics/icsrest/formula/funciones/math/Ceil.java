package cl.cardif.ics.icsrest.formula.funciones.math;

import java.util.ArrayList;
import java.util.Map;

import cl.cardif.ics.icsrest.formula.FormulaParent;
import cl.cardif.ics.icsrest.formula.IMathFactor;
import cl.cardif.ics.icsrest.util.FormulaOrigenKey;

public class Ceil extends FormulaParent implements IMathFactor {
	public Ceil() {
		hijos = new ArrayList<>();
	}

	@Override
	public int getMaximumChildrenCount() {
		return 1;
	}

	@Override
	public String toSafeSQL(Map<FormulaOrigenKey, Integer> tableAliases) {
		StringBuilder sb = new StringBuilder()
			.append("CEIL")
			.append("(")
			.append(getHijo(0).toSafeSQL(tableAliases))
			.append(")");

		return sb.toString();
	}

	@Override
	public String toSQL(Map<FormulaOrigenKey, Integer> tableAliases) {
		StringBuilder sb = new StringBuilder()
			.append("CEIL")
			.append("(")
			.append(getHijo(0).toSQL(tableAliases))
			.append(")");

		return sb.toString();
	}
}
