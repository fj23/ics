package cl.cardif.ics.icsrest.domain.pojo;

public class Homologacion {

  private long id;
  private long idTipo;
  private String concepto;
  private String vigencia;

  private Socio socio1;
  private Core core1;
  private String valor1;
  private Socio socio2;
  private Core core2;
  private String valor2;

  public Homologacion() {
    // Constructor JavaBean
  }

  public String getConcepto() {
    return concepto;
  }

  public void setConcepto(String concepto) {
    this.concepto = concepto;
  }

  public Core getCore1() {
    return core1;
  }

  public void setCore1(Core core1) {
    this.core1 = core1;
  }

  public Core getCore2() {
    return core2;
  }

  public void setCore2(Core core2) {
    this.core2 = core2;
  }

  public long getId() {
    return id;
  }

  public void setId(long idHomologacion) {
    this.id = idHomologacion;
  }

  public long getIdTipo() {
    return idTipo;
  }

  public void setIdTipo(long idTipo) {
    this.idTipo = idTipo;
  }

  public Socio getSocio1() {
    return socio1;
  }

  public void setSocio1(Socio socio1) {
    this.socio1 = socio1;
  }

  public Socio getSocio2() {
    return socio2;
  }

  public void setSocio2(Socio socio2) {
    this.socio2 = socio2;
  }

  public String getValor1() {
    return valor1;
  }

  public void setValor1(String valor1) {
    this.valor1 = valor1;
  }

  public String getValor2() {
    return valor2;
  }

  public void setValor2(String valor2) {
    this.valor2 = valor2;
  }

  public String getVigencia() {
    return vigencia;
  }

  public void setVigencia(String vigencia) {
    this.vigencia = vigencia;
  }
}
