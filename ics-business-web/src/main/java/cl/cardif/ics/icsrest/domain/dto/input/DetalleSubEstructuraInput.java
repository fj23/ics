package cl.cardif.ics.icsrest.domain.dto.input;

import java.math.BigDecimal;

public class DetalleSubEstructuraInput extends ColumnaInput {
  private String descripcion;
  private String codigo;
  private long idTipoDato;
  private BigDecimal largo;
  private long idSubestructuraPadre;
  private String vigencia;

  public DetalleSubEstructuraInput() {
    // Constructor JavaBean
  }

  public String getCodigo() {
    return codigo;
  }

  public void setCodigo(String codigo) {
    this.codigo = codigo;
  }

  public String getDescripcion() {
    return descripcion;
  }

  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }

  public long getIdSubestructuraPadre() {
    return idSubestructuraPadre;
  }

  public void setIdSubestructuraPadre(long subestructuraPadre) {
    this.idSubestructuraPadre = subestructuraPadre;
  }

  public long getIdTipoDato() {
    return idTipoDato;
  }

  public void setIdTipoDato(long tipoDato) {
    this.idTipoDato = tipoDato;
  }

  public BigDecimal getLargo() {
    return largo;
  }

  public void setLargo(BigDecimal largo) {
    this.largo = largo;
  }

  public String getVigencia() {
    return vigencia;
  }

  public void setVigencia(String vigencia) {
    this.vigencia = vigencia;
  }
}
