package cl.cardif.ics.icsrest.domain.dto.input;

import java.util.ArrayList;
import java.util.List;

public class EstructuraInput extends EstructuraAnetoSimpleInput {
	private List<DetalleEstructuraInput> columnas;
	private String vigencia;

	public EstructuraInput() {
		super();
	}

	public List<DetalleEstructuraInput> getColumnas() {
		if (this.columnas != null) {
			return new ArrayList<>(columnas);
		} else {
			return new ArrayList<>();
		}
	}

	public void setColumnas(List<DetalleEstructuraInput> columnas) {
		if (columnas != null) {
			this.columnas = new ArrayList<>(columnas);
		}
	}

	public String getVigencia() {
		return vigencia;
	}

	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}
}
