package cl.cardif.ics.icsrest.util.converters.topojo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import cl.cardif.ics.domain.entity.plantillas.IcsAtributosIntg;
import cl.cardif.ics.icsrest.domain.dto.input.MapeoIntegracionInput;


@Component
public class MapeoIntegracionPlantillaConverter
		implements Converter<IcsAtributosIntg, MapeoIntegracionInput> {

	static final Logger LOG = LoggerFactory.getLogger(MapeoIntegracionPlantillaConverter.class);


	@Override
	public MapeoIntegracionInput convert(IcsAtributosIntg entity) {
		MapeoIntegracionInput pojo = new MapeoIntegracionInput();
		pojo.setId(entity.getIdAtributoIntegracion());
		pojo.setEsObligatorio("Y".equalsIgnoreCase(entity.getFlgObligatorio()));
		return pojo;
	}
}
