package cl.cardif.ics.icsrest.util.converters.topojo;

import cl.cardif.ics.domain.entity.perfilamiento.IcsDetalleRol;
import cl.cardif.ics.domain.entity.perfilamiento.IcsTipoRol;
import cl.cardif.ics.icsrest.domain.pojo.DetalleRol;
import cl.cardif.ics.icsrest.domain.pojo.TipoRol;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class TipoRolConverter implements Converter<IcsTipoRol, TipoRol> {

  @Override
  public TipoRol convert(IcsTipoRol entity) {
    Map<Long, DetalleRol> detalles = new HashMap<Long, DetalleRol>();
    IcsDetalleRol entityDetail;
    DetalleRol pojoDetail;

    TipoRol pojo = new TipoRol();
    pojo.setFechaCreacion(entity.getFechaCreacion());
    pojo.setIdRol(entity.getIdRol());
    pojo.setNombreRol(entity.getNombreRol());
    pojo.setVigencia(entity.getVigencia());

    for (int i = 0; i < entity.getDetalles().size(); i++) {
      entityDetail = entity.getDetalles().get(i);
      pojoDetail = new DetalleRol();
      pojoDetail.setIdDetalle(entityDetail.getIdDetalle());
      pojoDetail.setFechaCreacion(entityDetail.getFechaCreacion());
      pojoDetail.setIcsTipoRol(pojo.getNombreRol());
      pojoDetail.setCodigo(entityDetail.getCodigo());
      pojoDetail.setVigencia(entityDetail.getVigencia());
      // Map<Long, DetalleRol>
      detalles.put(pojoDetail.getCodigo(), pojoDetail);
    }

    if (entity.getDetalles().isEmpty()) {
      detalles.put(0L, new DetalleRol());
    }

    pojo.setDetalles(detalles);

    return pojo;
  }

  public List<TipoRol> convertList(List<IcsTipoRol> list) {
    List<TipoRol> result = new ArrayList<>();
    for (IcsTipoRol entity : list) {
      result.add(convert(entity));
    }
    return result;
  }
}
