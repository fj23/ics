package cl.cardif.ics.icsrest.exceptions.icsdetallehomologacion;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
public class IcsDetalleHomologacionMethodNotAllowedException extends RuntimeException {

  private static final long serialVersionUID = -6814524510236406273L;

  public IcsDetalleHomologacionMethodNotAllowedException(String exception) {
    super(exception);
  }

  public IcsDetalleHomologacionMethodNotAllowedException(String exception, Throwable t) {
    super(exception, t);
  }
}
