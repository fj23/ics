package cl.cardif.ics.icsrest.formula.interfaces;

import cl.cardif.ics.icsrest.formula.funciones.custom.*;
import cl.cardif.ics.icsrest.formula.funciones.date.*;
import cl.cardif.ics.icsrest.formula.funciones.math.*;
import cl.cardif.ics.icsrest.formula.funciones.string.*;
import cl.cardif.ics.icsrest.formula.funciones.uf.*;
import cl.cardif.ics.icsrest.formula.operadores.*;
import cl.cardif.ics.icsrest.formula.operadores.comparison.*;
import cl.cardif.ics.icsrest.formula.operadores.logic.*;
import cl.cardif.ics.icsrest.formula.operadores.math.*;
import cl.cardif.ics.icsrest.util.FormulaOrigenKey;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "tipo")
@JsonSubTypes({ @Type(value = NumberLiteral.class, name = "NumberLiteral"),
		@Type(value = StringLiteral.class, name = "StringLiteral"),
		@Type(value = NullLiteral.class, name = "NullLiteral"), @Type(value = SysDate.class, name = "SysDate"),

		@Type(value = IfThenElse.class, name = "IfThenElse"), @Type(value = EqualTo.class, name = "EqualTo"),
		@Type(value = NotEqualTo.class, name = "NotEqualTo"), @Type(value = HigherThan.class, name = "HigherThan"),
		@Type(value = LowerThan.class, name = "LowerThan"),
		@Type(value = HigherEqualThan.class, name = "HigherEqualThan"),
		@Type(value = LowerEqualThan.class, name = "LowerEqualThan"), @Type(value = And.class, name = "And"),
		@Type(value = Or.class, name = "Or"),

		@Type(value = Sum.class, name = "Sum"), @Type(value = Subtraction.class, name = "Subtraction"),
		@Type(value = Division.class, name = "Division"), @Type(value = Multiplication.class, name = "Multiplication"),
		@Type(value = Round.class, name = "Round"), @Type(value = Floor.class, name = "Floor"),
		@Type(value = Ceil.class, name = "Ceil"), @Type(value = Trunc.class, name = "Trunc"),

		@Type(value = Concat.class, name = "Concat"), @Type(value = Substring.class, name = "Substring"),
		@Type(value = LPad.class, name = "LPad"), @Type(value = RPad.class, name = "RPad"),
		@Type(value = Replace.class, name = "Replace"), @Type(value = FncSplitFormula.class, name = "FncSplitFormula"),

		@Type(value = ToDate.class, name = "ToDate"), @Type(value = MonthsBetween.class, name = "MonthsBetween"),
		@Type(value = DaysBetween.class, name = "DaysBetween"), @Type(value = AddDays.class, name = "AddDays"),
		@Type(value = AddMonths.class, name = "AddMonths"),

		@Type(value = CampoIntegracion.class, name = "CampoIntegracion"),
		@Type(value = FncValidaRut.class, name = "FncValidaRut"),

		@Type(value = ValorUFActual.class, name = "ValorUFActual"),
		@Type(value = ValorUFDia.class, name = "ValorUFDia"),
		@Type(value = ValorUFPrimerDia.class, name = "ValorUFPrimerDia"),
		@Type(value = ValorUFUltimoDia.class, name = "ValorUFUltimoDia"),

		@Type(value = FncEnriquecimiento.class, name = "FncEnriquecimiento") })
public interface IFormulaFactor {

	/**
	 * Genera un string SQL para que sea guardado como fórmula de un campo de
	 * trabajo de una plantilla de integración.
	 * 
	 * @param tableAliases:
	 *            Un objeto conteniendo un valor entero. Dicho valor se requiere
	 *            para asignar correctamente los alias de las tablas usadas en un
	 *            SELECT FROM con varios orígenes.
	 */
	public String toSQL(Map<FormulaOrigenKey, Integer> tableAliases);

	public String toSafeSQL(Map<FormulaOrigenKey, Integer> tableAliases);

	public String toJSON();
}
