package cl.cardif.ics.icsrest.domain.dto.input;

public abstract class ColumnaMetadatos extends BaseColumnaModel {
  protected Long orden;
  protected Long tipoDato;
  protected Long posicionInicial;
  protected Long posicionFinal;
  protected Long subEstructura;
  protected String vigencia;
  protected boolean mapeada;

  private boolean esObligatorio;
  private boolean esLlave;
  private long idSubEstructura;
  private long idSubEstructuraPadre;

  public long getIdSubEstructura() {
    return idSubEstructura;
  }

  public void setIdSubEstructura(long idSubEstructura) {
    this.idSubEstructura = idSubEstructura;
  }

  public long getIdSubEstructuraPadre() {
    return idSubEstructuraPadre;
  }

  public void setIdSubEstructuraPadre(long idSubEstructuraPadre) {
    this.idSubEstructuraPadre = idSubEstructuraPadre;
  }

  public Long getOrden() {
    return orden;
  }

  public void setOrden(Long orden) {
    this.orden = orden;
  }

  public Long getPosicionFinal() {
    return posicionFinal;
  }

  public void setPosicionFinal(Long posicionFinal) {
    this.posicionFinal = posicionFinal;
  }

  public Long getPosicionInicial() {
    return posicionInicial;
  }

  public void setPosicionInicial(Long posicionInicial) {
    this.posicionInicial = posicionInicial;
  }

  public Long getSubEstructura() {
    return subEstructura;
  }

  public void setSubEstructura(Long subEstructura) {
    this.subEstructura = subEstructura;
  }

  public Long getTipoDato() {
    return tipoDato;
  }

  public void setTipoDato(Long tipoDato) {
    this.tipoDato = tipoDato;
  }

  public String getVigencia() {
    return vigencia;
  }

  public void setVigencia(String vigencia) {
    this.vigencia = vigencia;
  }

  public boolean isEsLlave() {
    return esLlave;
  }

  public void setEsLlave(boolean esLlave) {
    this.esLlave = esLlave;
  }

  public boolean isEsObligatorio() {
    return esObligatorio;
  }

  public void setEsObligatorio(boolean esObligatorio) {
    this.esObligatorio = esObligatorio;
  }

  public boolean isMapeada() {
    return mapeada;
  }

  public void setMapeada(boolean mapeada) {
    this.mapeada = mapeada;
  }
}
