package cl.cardif.ics.icsrest.util.converters.topojo;

import cl.cardif.ics.domain.entity.plantillas.IcsTipoPlantilla;
import cl.cardif.ics.icsrest.domain.pojo.TipoPlantilla;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class TipoPlantillaConverter implements Converter<IcsTipoPlantilla, TipoPlantilla> {

	static final Logger LOG = LoggerFactory.getLogger(TipoPlantillaConverter.class);

	@Override
	public TipoPlantilla convert(IcsTipoPlantilla icsTipoPlantilla) {
		TipoPlantilla tipoPlantilla = new TipoPlantilla();
		tipoPlantilla.setId(icsTipoPlantilla.getIdTipoPlantilla());
		tipoPlantilla.setNombre(icsTipoPlantilla.getNombrePlantilla());
		tipoPlantilla.setVigencia(icsTipoPlantilla.getVigenciaPlantilla());
		return tipoPlantilla;
	}
}
