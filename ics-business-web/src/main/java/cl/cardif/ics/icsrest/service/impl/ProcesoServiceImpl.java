package cl.cardif.ics.icsrest.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityNotFoundException;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.BooleanBuilder;

import cl.cardif.ics.domain.entity.common.IcsCallCenter;
import cl.cardif.ics.domain.entity.common.IcsCallXEventoXSocio;
import cl.cardif.ics.domain.entity.common.IcsDirectoriosSalida;
import cl.cardif.ics.domain.entity.common.IcsTipoArchivo;
import cl.cardif.ics.domain.entity.common.QIcsCallXEventoXSocio;
import cl.cardif.ics.domain.entity.common.QIcsDirectoriosSalida;
import cl.cardif.ics.domain.entity.plantillas.IcsPlantilla;
import cl.cardif.ics.domain.entity.procesos.IcsDetalleProceso;
import cl.cardif.ics.domain.entity.procesos.IcsProceso;
import cl.cardif.ics.domain.entity.procesos.QIcsDetalleProceso;
import cl.cardif.ics.domain.entity.procesos.QIcsProceso;
import cl.cardif.ics.icsrest.domain.dto.input.ProcesoInput;
import cl.cardif.ics.icsrest.domain.dto.output.PlantillaOutput;
import cl.cardif.ics.icsrest.domain.dto.output.ProcesoOutput;
import cl.cardif.ics.icsrest.domain.pojo.CallCenter;
import cl.cardif.ics.icsrest.domain.pojo.DirectorioSalida;
import cl.cardif.ics.icsrest.domain.pojo.TipoArchivo;
import cl.cardif.ics.icsrest.exceptions.icsproceso.IcsProcesoNotFoundException;
import cl.cardif.ics.icsrest.exceptions.icsproceso.IcsProcesoServiceException;
import cl.cardif.ics.icsrest.repository.CallCentersRepository;
import cl.cardif.ics.icsrest.repository.CallXEventoXSocioRepository;
import cl.cardif.ics.icsrest.repository.DetallesProcesoRepository;
import cl.cardif.ics.icsrest.repository.DirectoriosSalidaRepository;
import cl.cardif.ics.icsrest.repository.PlantillaRepository;
import cl.cardif.ics.icsrest.repository.ProcesoRepository;
import cl.cardif.ics.icsrest.repository.TipoArchivoRepository;
import cl.cardif.ics.icsrest.service.ProcesoService;
import cl.cardif.ics.icsrest.util.Constants;
import cl.cardif.ics.icsrest.util.OutputListWrapper;
import cl.cardif.ics.icsrest.util.Util;

@Transactional
@Service
public class ProcesoServiceImpl implements ProcesoService {
	private static final Logger LOG = LoggerFactory.getLogger(ProcesoServiceImpl.class);
	
	private static final String MSJ_PROCESOS_NO_ENCONTRADOS = "Error al obtener procesos";

	private JdbcTemplate jdbcTemplate;
	
	@Autowired private ConversionService conversionService;
	@Autowired private ProcesoRepository procesoRepository;
	@Autowired private DetallesProcesoRepository detallesProcesoRepository;
	@Autowired private PlantillaRepository plantillaRepository;
	@Autowired private CallCentersRepository callCentersRepository;
	@Autowired private CallXEventoXSocioRepository callXEventoXSocioRepository;
	@Autowired private TipoArchivoRepository tipoArchivoRepository;
	@Autowired private DirectoriosSalidaRepository directorioSalidaRepository;
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	private ProcesoOutput convertProcesoAndLoadPlantillas(IcsProceso entity) {
		ProcesoOutput proceso = conversionService.convert(entity, ProcesoOutput.class);

		List<IcsDetalleProceso> detallesEntity = entity.getDetalles();
		for (IcsDetalleProceso entityDetalle : detallesEntity) {
			IcsPlantilla icsPlantilla = entityDetalle.getIcsPlantilla();
			PlantillaOutput plantilla = conversionService.convert(icsPlantilla, PlantillaOutput.class);
			if (plantilla.getTipo().getId() == Constants.ID_TIPO_PLANTILLA_ENTRADA) {
				proceso.setPlantillaEntradaSocio(plantilla);
			} else if (plantilla.getTipo().getId() == Constants.ID_TIPO_PLANTILLA_INTEGRACION) {
				proceso.setPlantillaIntegracion(plantilla);
			} else if (plantilla.getTipo().getId() == Constants.ID_TIPO_PLANTILLA_SALIDA) {
				proceso.setPlantillaSalidaSocio(plantilla);
			} else {
				throw new IcsProcesoServiceException("Tipo de plantilla no reconocida en detalle de proceso");
			}
		}
		return proceso;
	}

	@Override
	public ProcesoOutput getProceso(long idProceso) {
		LOG.info("getProceso");

		BooleanBuilder booleanBuilder = new BooleanBuilder()
				.and(QIcsProceso.icsProceso.idProceso.eq(idProceso));

		IcsProceso entity = null;
		try {
			entity = procesoRepository.findOne(booleanBuilder);
			return this.convertProcesoAndLoadPlantillas(entity);
		} catch (Exception e) {
			throw new IcsProcesoNotFoundException(MSJ_PROCESOS_NO_ENCONTRADOS, e);
		}
	}

	@Override
	public OutputListWrapper<ProcesoOutput> getProcesos(
		int page, 
		int size, 
		String sortColumn, 
		String sortOrder,
		Map<String, String> filters
	) {
		LOG.info("getProcesos");
		
		try {
			
			// debe tener al menos un detalle para ser incluido en la consulta
			BooleanBuilder bb = this.generateQueryFilter(filters)
					.and(QIcsProceso.icsProceso.detalles.size().goe(1));
			
			Set<Long> idsEventosAutorizados = Util.getEventsAuthorized();
			
			if (!filters.isEmpty()) {
	
				String filterSocio = filters.get("socio");
				String filterEvento = filters.get("evento");
				
				if (filterSocio != null) {
					Long idSocio = Long.valueOf(filterSocio);
					bb.and(QIcsProceso.icsProceso.detalles.any().icsPlantilla.icsSocio.idSocio.eq(idSocio));
				}
	
				if (filterEvento != null && idsEventosAutorizados.contains(Long.valueOf(filterEvento))) {
					Long idEvento = Long.valueOf(filterEvento);
					bb.and(QIcsProceso.icsProceso.detalles.any().icsPlantilla.icsEvento.idEvento.eq(idEvento));
				} else {
					bb.and(QIcsProceso.icsProceso.detalles.any().icsPlantilla.icsEvento.idEvento.in(idsEventosAutorizados));
				}
			} else {
				bb.and(QIcsProceso.icsProceso.detalles.any().icsPlantilla.icsEvento.idEvento.in(idsEventosAutorizados));
			}

			Pageable paginador;
			if (sortColumn.isEmpty()) {
				paginador = new PageRequest(page, size);
			} else {
				Sort sorter = this.buildSort(sortColumn, sortOrder);
				paginador = new PageRequest(page, size, sorter);
			}

			Integer cantidadProcesos = (int) procesoRepository.count(bb);
			Iterable<IcsProceso> iteraProcesos = procesoRepository.findAll(bb, paginador);
			
			List<ProcesoOutput>  procesos = new ArrayList<>();
			for (IcsProceso icsProceso : iteraProcesos) {
				ProcesoOutput proceso = this.convertProcesoAndLoadPlantillas(icsProceso);
				if (proceso != null) {
					procesos.add(proceso);
				}
			}

			return new OutputListWrapper<>(procesos, cantidadProcesos, "");

		} catch (Exception e) {
			LOG.error(MSJ_PROCESOS_NO_ENCONTRADOS, e);
			return null;
		}
	}

	private List<IcsDetalleProceso> hacerListaDetallesProcesoDesdeInput(ProcesoInput input) {
		LOG.info("hacerListaDetallesProcesoDesdeInput");
		
		List<IcsDetalleProceso> detallesProc = new ArrayList<>();

		if (null != input.getPlantillaEntradaSocio()) {
			Long ptlEntradaId = input.getPlantillaEntradaSocio().getId();
			if (ptlEntradaId != null && ptlEntradaId != 0L) {
				IcsPlantilla icsPlantilla = plantillaRepository.findOne(ptlEntradaId);
				IcsDetalleProceso dtl = new IcsDetalleProceso();
				dtl.setIcsPlantilla(icsPlantilla);
				detallesProc.add(dtl);
			}
		} else if (!input.isSalidaAneto()) {
			throw new IcsProcesoServiceException("El proceso no tiene una plantilla de entrada válida");
		}

		if (null != input.getPlantillaIntegracion()) {
			Long ptlIntegracionId = input.getPlantillaIntegracion().getId();
			if (ptlIntegracionId != null && ptlIntegracionId != 0L) {
				IcsPlantilla icsPlantilla = plantillaRepository.findOne(ptlIntegracionId);
				IcsDetalleProceso dtl = new IcsDetalleProceso();
				dtl.setIcsPlantilla(icsPlantilla);
				detallesProc.add(dtl);
			}
		} else if (!input.isSalidaAneto()) {
			throw new IcsProcesoServiceException("El proceso no tiene una plantilla de integración válida");
		}

		if (null != input.getPlantillaSalidaSocio()) {
			Long ptlSalidaId = input.getPlantillaSalidaSocio().getId();
			if (ptlSalidaId != null && ptlSalidaId != 0L) {
				IcsPlantilla icsPlantilla = plantillaRepository.findOne(ptlSalidaId);
				IcsDetalleProceso dtl = new IcsDetalleProceso();
				dtl.setIcsPlantilla(icsPlantilla);
				detallesProc.add(dtl);
			}
		} else if (input.isSalidaAneto()) { //debe tener una plantilla de salida, si es salida aneto
			throw new IcsProcesoServiceException("El proceso no tiene una plantilla de salida válida");
		}
		return detallesProc;
	}

	@Override
	public void almacenarProceso(ProcesoInput input, boolean isUpdate) {
		LOG.info("almacenarProceso");
		
		if (input.isSalidaAneto()) {
			String erroresProcesosAneto = this.obtenerMensajeErrorConsultaProcesosAneto();
			if (!erroresProcesosAneto.isEmpty()) {
				throw new IcsProcesoServiceException(erroresProcesosAneto);
			}
		}

		List<IcsDetalleProceso> detallesProc = this.hacerListaDetallesProcesoDesdeInput(input);

		IcsProceso entity;

		if (!isUpdate) {
			entity = conversionService.convert(input, IcsProceso.class);
		} else {
			entity = procesoRepository.findOne(input.getId());
			entity.setNombreProceso(input.getNombre().toUpperCase());
			entity.setCorreoEmail(input.getCorreoEmail());
			entity.setCorreoHitos(input.getCorreoHitos());
			entity.setCorreoSalida(input.getCorreoSalida());
			entity.setFlagAneto(input.isSalidaAneto()? "Y" : "N");
		}

		if (input.getCallCenter() != null) {
			IcsCallCenter callCenter = callCentersRepository.findOne(input.getCallCenter());
			entity.setIcsCallCenter(callCenter);
		}
		
		if (input.getTipoArchivo() != null) {
			IcsTipoArchivo tipoArchivo = tipoArchivoRepository.findOne(input.getTipoArchivo());
			entity.setIcsTipoArchivo(tipoArchivo);
		}
		
		IcsDirectoriosSalida directorioError = null;
		if (input.getDirectorioSalidaError() != null) {
			directorioError = directorioSalidaRepository.findOne(input.getDirectorioSalidaError());
		} else {
			QIcsDirectoriosSalida qIcsDirectoriosSalida = QIcsDirectoriosSalida.icsDirectoriosSalida;
			BooleanBuilder directorioErrorPorDefecto = new BooleanBuilder()
					.and(qIcsDirectoriosSalida.tipoDirectorio.eq(1L))
					.and(qIcsDirectoriosSalida.descripcionDirectorio.eq("CARDIF"));
			
			directorioError = directorioSalidaRepository.findOne(directorioErrorPorDefecto);
		}
		entity.setIcsDirectorioSalidaError(directorioError);
		LOG.debug("almacenarProceso/entity={}",entity);

		entity = procesoRepository.saveAndFlush(entity);
		LOG.debug("almacenarProceso/entitySaved={}",entity);
		
		Long idProceso = entity.getIdProceso();

		// si el proceso ya existia, elimina sus detalles
		if (isUpdate && idProceso != 0L) {

			BooleanBuilder bb = new BooleanBuilder()
					.and(QIcsDetalleProceso.icsDetalleProceso.icsProceso.idProceso.eq(idProceso));

			detallesProcesoRepository.delete(
				detallesProcesoRepository.findAll(bb)
			);
		}

		// inserta los detalles incluidos en el input
		for (IcsDetalleProceso detalleEntity : detallesProc) {
			IcsProceso proceso = procesoRepository.findOne(idProceso);
			detalleEntity.setIcsProceso(proceso);
			LOG.debug("almacenarProceso/detalleEntity={}",detalleEntity);

			try {
				detalleEntity = detallesProcesoRepository.save(detalleEntity);
				LOG.debug("almacenarProceso/detalleEntitySaved={}",detalleEntity);
			} catch (Exception exc) {
				LOG.error("No se pudo guardar el detalle de proceso", exc);
			}
		}
		detallesProcesoRepository.flush();
	}
	
	private String obtenerMensajeErrorConsultaProcesosAneto() {
		LOG.info("obtenerMensajeErrorConsultaProcesosAneto");
		
		SimpleJdbcCall call = new SimpleJdbcCall(jdbcTemplate)
				.withCatalogName(Constants.PACKAGE_UTIL_APP)
				.withProcedureName(Constants.PROCEDURE_EXISTEN_PROCESOS_ANETO)
				.declareParameters(
						new SqlOutParameter(Constants.PV_EXISTEN_PROCESOS_ANETO_OUT, oracle.jdbc.OracleTypes.NUMBER),
						new SqlOutParameter(Constants.PV_EXISTEN_PROCESOS_ANETO_OUT_DESC, oracle.jdbc.OracleTypes.VARCHAR));
		
		Map<String, Object> params = new HashMap<>();
		params.put(Constants.PV_EXISTEN_PROCESOS_ANETO_OUT, null);
		params.put(Constants.PV_EXISTEN_PROCESOS_ANETO_OUT_DESC, null);
		
		Map<String, Object> executionResult = call.execute(params);
		
		if (executionResult.containsKey(Constants.PV_EXISTEN_PROCESOS_ANETO_OUT)) {
			Integer errorCode = ((BigDecimal)executionResult.get(Constants.PV_EXISTEN_PROCESOS_ANETO_OUT)).intValue();
			String errorMessage = (String)executionResult.get(Constants.PV_EXISTEN_PROCESOS_ANETO_OUT_DESC);
			if (errorCode == 0) {
				return errorMessage;
			}
		}

		return "";
	}

	@Override
	public void activarProceso(long idProceso) {
		LOG.info("activarProceso");
		
		IcsProceso entity;
		try {
			entity = procesoRepository.getOne(idProceso);
		} catch (EntityNotFoundException e) {
			throw new IcsProcesoNotFoundException("Proceso no encontrado", e);
		}
		
		if (entity.getFlagAneto().equals("Y") && entity.getVigenciaProceso().equals("N")) {
			
			String erroresProcesosAneto = this.obtenerMensajeErrorConsultaProcesosAneto();
			if (!erroresProcesosAneto.isEmpty()) {
				throw new IcsProcesoServiceException("No se puede activar un proceso Aneto si hay otros en vigencia.");
			}
		}
		
		entity.setVigenciaProceso("Y");
		procesoRepository.saveAndFlush(entity);
	}

	@Override
	public void desactivarProceso(long idProceso) {
		LOG.info("desactivarProceso");

		try {
			IcsProceso entity = procesoRepository.getOne(idProceso);
			entity.setVigenciaProceso("N");
			procesoRepository.saveAndFlush(entity);
		} catch (EntityNotFoundException e) {
			throw new IcsProcesoNotFoundException("Proceso no encontrado", e);
		}

	}

	@Override
	public List<ProcesoOutput> getAllVigentes(Long idSocio) {
		LOG.info("getAllVigentes");
		
		QIcsProceso qIcsProceso = QIcsProceso.icsProceso;
		
		try {
			HashSet<Long> eventos = Util.getEventsAuthorized();			

			BooleanBuilder bb = new BooleanBuilder()
					.and(qIcsProceso.vigenciaProceso.eq("Y"))
					.and(qIcsProceso.detalles.any().icsPlantilla.icsEvento.idEvento.in(eventos));

			Iterable<IcsProceso> iteraProcesos = procesoRepository.findAll(bb);

			List<ProcesoOutput> listaProcesoOutput = new ArrayList<>();
			for (IcsProceso icsProceso : iteraProcesos) {
				ProcesoOutput proceso = conversionService.convert(icsProceso, ProcesoOutput.class);
				listaProcesoOutput.add(proceso);
			}
			return listaProcesoOutput;
			
		} catch (Exception e) {
			throw new IcsProcesoServiceException("Error al obtener procesos vigentes", e);
		}
	}

	public List<ProcesoOutput> transformListToPage(
			List<ProcesoOutput> lista, 
			int page, 
			int size, 
			String sortColumn,
			String sortOrder
	) {
		LOG.info("transformListToPage");

		Pageable paginador;
		if (sortColumn.equals("")) {
			paginador = new PageRequest(page, size);
		} else {
			Sort sorter = this.buildSort(sortColumn, sortOrder);
			paginador = new PageRequest(page, size, sorter);
		}

		Page<ProcesoOutput> auxLista = new PageImpl<>(lista, paginador, lista.size());
		return auxLista.getContent();
	}

	public Sort buildSort(String sortColumn, String sortOrder) {
		LOG.info("buildSort");

		Sort.Direction dir;
		String entitySortField;

		switch (sortColumn) {
			case "idProceso":
				entitySortField = "idProceso";
				break;
			case "nombre":
				entitySortField = "nombreProceso";
				break;
			default:
				return null;
		}

		if ("desc".equalsIgnoreCase(sortOrder)) {
			dir = Sort.Direction.DESC;
		} else {
			dir = Sort.Direction.ASC;
		}

		return new Sort(dir, entitySortField);
	}

	private BooleanBuilder generateQueryFilter(Map<String, String> filters) {
		LOG.info("generateQueryFilter");

		QIcsProceso qProceso = QIcsProceso.icsProceso;
		BooleanBuilder bb = new BooleanBuilder();
		
		final String filtroNombre = "nombreProceso";
		final String filtroVigencia = "estado";

		if (filters.containsKey(filtroNombre) && !filters.get(filtroNombre).trim().isEmpty()) {
			String nombreProceso = filters.get(filtroNombre);
			bb.and(qProceso.nombreProceso.equalsIgnoreCase(nombreProceso));
		}

		if (filters.containsKey(filtroVigencia) && !filters.get(filtroVigencia).trim().isEmpty()) {
			String vigencia = filters.get(filtroVigencia);
			bb.and(qProceso.vigenciaProceso.equalsIgnoreCase(vigencia));
		}
		
		return bb;
	}

	@Override
	public List<String> getNombresAll() {
		LOG.info("getNombresAll");
		try {
			return this.procesoRepository.findDistinctProcesos();
		} catch (Exception e) {
			throw new IcsProcesoServiceException("Error al obtener listado de nombres", e);
		}

	}

	@Override
	public List<CallCenter> getCallCenters(Long idSocio, Long idEvento) {
		LOG.info("getCallCenters");

		QIcsCallXEventoXSocio qIcsCallXEventoXSocio = QIcsCallXEventoXSocio.icsCallXEventoXSocio;
		BooleanBuilder bb = new BooleanBuilder()
				.and(qIcsCallXEventoXSocio.icsEvento.idEvento.eq(idEvento))
				.and(qIcsCallXEventoXSocio.icsSocio.idSocio.eq(idSocio))
				.and(qIcsCallXEventoXSocio.flgActivo.eq("Y"));
		
		Iterable<IcsCallXEventoXSocio> entities = callXEventoXSocioRepository.findAll(bb);

		Set<CallCenter> set = new HashSet<>();
		for (IcsCallXEventoXSocio relationship : entities) {
			IcsCallCenter entity = relationship.getIcsCallCenter();
			CallCenter callCenter = conversionService.convert(entity, CallCenter.class);
			set.add(callCenter);
		}
		List<CallCenter> callCenters = new ArrayList<>();
		callCenters.addAll(set);
		
		return callCenters;
	}

	@Override
	public List<TipoArchivo> getTiposArchivo() {
		LOG.info("getTiposArchivo");
		
		Iterable<IcsTipoArchivo> iteraTiposArchivo = tipoArchivoRepository.findAll();
		
		List<TipoArchivo> tiposArchivo = new ArrayList<>();
		for (IcsTipoArchivo icsTipoArchivo : iteraTiposArchivo) {
			TipoArchivo tipoArchivo = conversionService.convert(icsTipoArchivo, TipoArchivo.class);
			tiposArchivo.add(tipoArchivo);
		}
		
		return tiposArchivo;
	}

	@Override
	public List<DirectorioSalida> getDirectoriosError() {
		LOG.info("getDirectoriosError");
		
		BooleanBuilder soloDirectoriosError = new BooleanBuilder()
				.and(QIcsDirectoriosSalida.icsDirectoriosSalida.tipoDirectorio.eq(1L));
		
		Iterable<IcsDirectoriosSalida> iteraDirectoriosSalidaError = directorioSalidaRepository.findAll(soloDirectoriosError);
		
		List<DirectorioSalida> directoriosSalidaError = new ArrayList<>();
		for (IcsDirectoriosSalida icsDirectoriosSalida : iteraDirectoriosSalidaError) {
			DirectorioSalida tipoArchivo = conversionService.convert(icsDirectoriosSalida, DirectorioSalida.class);
			directoriosSalidaError.add(tipoArchivo);
		}
		
		return directoriosSalidaError;
	}
}
