package cl.cardif.ics.icsrest.security.jwt;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Configuration
public class JwtTokenFilterConfigurer {

	@Value("${security.jwt.token.secret-key}")
	private String SECRET_KEY;

	@PostConstruct
	public void jwtTokenFilterConfigurer() {
		SECRET_KEY = Base64.getEncoder().encodeToString(SECRET_KEY.getBytes(StandardCharsets.UTF_8));
	}

	@Bean
	public JwtParser jwtParser() {
		return Jwts.parser().setSigningKey(SECRET_KEY);
	}

	@Bean
	public JwtBuilder jwtBuilder() {
		return Jwts.builder().signWith(SignatureAlgorithm.HS256, SECRET_KEY);
	}

}
