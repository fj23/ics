package cl.cardif.ics.icsrest.domain.dto.output;

import cl.cardif.ics.icsrest.domain.dto.input.TributoEnriquecimiento;

public class TributoEnriquecimientoOutput extends TributoEnriquecimiento {

  public TributoEnriquecimientoOutput() {
    super();
  }
}
