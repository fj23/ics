package cl.cardif.ics.icsrest.service;

import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;

import cl.cardif.ics.icsrest.domain.dto.input.PlantillaEntradaInput;
import cl.cardif.ics.icsrest.domain.dto.output.PlantillaOutput;
import cl.cardif.ics.icsrest.domain.dto.output.RequestResultOutput;
import cl.cardif.ics.icsrest.domain.pojo.TipoDato;
import cl.cardif.ics.icsrest.util.Permisos;

public interface PlantillaEntradaService {

	/**
	* @param plantilla La plantilla que se desea almacenar en base de datos
	* @param isUpdate Flag que indica si la operacion es una actualizacion o una nueva plantilla
	* @return devuelve el resultado de la operacion en la forma: RequestResultOutput( CODIGO(Numero)
	*     | DESCRIPCION(Texto) )
	* @see cl.cardif.ics.icsrest.domain.dto.output.RequestResultOutput
	*/
	@PreAuthorize(value = "hasAnyAuthority("
	            + Permisos.Mant_Plan_Alta + ","
	            + Permisos.Mant_Plan_Canc + ","
	            + Permisos.Mant_Plan_Reca + ","
                + Permisos.Mant_Plan_Prosp + ","
                + Permisos.Mant_Plan_Intera + ","
                + Permisos.Mant_Plan_Pago + ")")
	RequestResultOutput addOrUpdatePlantillaEntrada(PlantillaEntradaInput plantilla);

	@PreAuthorize(value = "hasAnyAuthority("
	            + Permisos.Cslt_Plan + ","
	            + Permisos.Mant_Plan_Alta + ","
	            + Permisos.Mant_Plan_Canc + ","
	            + Permisos.Mant_Plan_Reca + ","
                + Permisos.Mant_Plan_Prosp + ","
                + Permisos.Mant_Plan_Intera + ","
                + Permisos.Mant_Plan_Pago + ")")
	List<TipoDato> getColumnDataTypes();

	@PreAuthorize(value = "hasAnyAuthority("
	            + Permisos.Cslt_Plan + ","
	            + Permisos.Mant_Plan_Alta + ","
	            + Permisos.Mant_Plan_Canc + ","
	            + Permisos.Mant_Plan_Reca + ","
                + Permisos.Mant_Plan_Prosp + ","
                + Permisos.Mant_Plan_Intera + ","
                + Permisos.Mant_Plan_Pago + ")")
	PlantillaOutput viewTemplateDetalle(PlantillaOutput plantilla);

	@PreAuthorize(value = "hasAnyAuthority("
	            + Permisos.Cslt_Plan + ","
	            + Permisos.Mant_Plan_Alta + ","
	            + Permisos.Mant_Plan_Canc + ","
	            + Permisos.Mant_Plan_Reca + ","
                + Permisos.Mant_Plan_Prosp + ","
                + Permisos.Mant_Plan_Intera + ","
                + Permisos.Mant_Plan_Pago + ")")
	public List<PlantillaOutput> getVigentes(Long idSocio, Long idEvento);
}
