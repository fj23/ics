package cl.cardif.ics.icsrest.formula;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import cl.cardif.ics.icsrest.formula.funciones.custom.FncEnriquecimiento;
import cl.cardif.ics.icsrest.formula.funciones.custom.FncSplitFormula;
import cl.cardif.ics.icsrest.formula.funciones.custom.FncValidaRut;
import cl.cardif.ics.icsrest.formula.funciones.date.AddDays;
import cl.cardif.ics.icsrest.formula.funciones.date.AddMonths;
import cl.cardif.ics.icsrest.formula.funciones.date.DaysBetween;
import cl.cardif.ics.icsrest.formula.funciones.date.MonthsBetween;
import cl.cardif.ics.icsrest.formula.funciones.date.ToDate;
import cl.cardif.ics.icsrest.formula.funciones.math.Ceil;
import cl.cardif.ics.icsrest.formula.funciones.math.Floor;
import cl.cardif.ics.icsrest.formula.funciones.math.Round;
import cl.cardif.ics.icsrest.formula.funciones.math.Trunc;
import cl.cardif.ics.icsrest.formula.funciones.string.Concat;
import cl.cardif.ics.icsrest.formula.funciones.string.LPad;
import cl.cardif.ics.icsrest.formula.funciones.string.RPad;
import cl.cardif.ics.icsrest.formula.funciones.string.Replace;
import cl.cardif.ics.icsrest.formula.funciones.string.Substring;
import cl.cardif.ics.icsrest.formula.funciones.uf.ValorUFActual;
import cl.cardif.ics.icsrest.formula.funciones.uf.ValorUFDia;
import cl.cardif.ics.icsrest.formula.funciones.uf.ValorUFPrimerDia;
import cl.cardif.ics.icsrest.formula.funciones.uf.ValorUFUltimoDia;
import cl.cardif.ics.icsrest.formula.operadores.CampoIntegracion;
import cl.cardif.ics.icsrest.formula.operadores.IfThenElse;
import cl.cardif.ics.icsrest.formula.operadores.NullLiteral;
import cl.cardif.ics.icsrest.formula.operadores.NumberLiteral;
import cl.cardif.ics.icsrest.formula.operadores.StringLiteral;
import cl.cardif.ics.icsrest.formula.operadores.SysDate;
import cl.cardif.ics.icsrest.formula.operadores.comparison.EqualTo;
import cl.cardif.ics.icsrest.formula.operadores.comparison.HigherEqualThan;
import cl.cardif.ics.icsrest.formula.operadores.comparison.HigherThan;
import cl.cardif.ics.icsrest.formula.operadores.comparison.LowerEqualThan;
import cl.cardif.ics.icsrest.formula.operadores.comparison.LowerThan;
import cl.cardif.ics.icsrest.formula.operadores.comparison.NotEqualTo;
import cl.cardif.ics.icsrest.formula.operadores.logic.And;
import cl.cardif.ics.icsrest.formula.operadores.logic.Or;
import cl.cardif.ics.icsrest.formula.operadores.math.Division;
import cl.cardif.ics.icsrest.formula.operadores.math.Multiplication;
import cl.cardif.ics.icsrest.formula.operadores.math.Subtraction;
import cl.cardif.ics.icsrest.formula.operadores.math.Sum;
import cl.cardif.ics.icsrest.util.FormulaOrigenKey;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "tipo"
)
@JsonSubTypes({
        @Type(value = NumberLiteral.class, name = "NumberLiteral"),
        @Type(value = StringLiteral.class, name = "StringLiteral"),
        @Type(value = NullLiteral.class, name = "NullLiteral"),
        @Type(value = SysDate.class, name = "SysDate"),

        @Type(value = IfThenElse.class, name = "IfThenElse"),
        @Type(value = EqualTo.class, name = "EqualTo"),
        @Type(value = NotEqualTo.class, name = "NotEqualTo"),
        @Type(value = HigherThan.class, name = "HigherThan"),
        @Type(value = LowerThan.class, name = "LowerThan"),
        @Type(value = HigherEqualThan.class, name = "HigherEqualThan"),
        @Type(value = LowerEqualThan.class, name = "LowerEqualThan"),
        @Type(value = And.class, name = "And"),
        @Type(value = Or.class, name = "Or"),

        @Type(value = Sum.class, name = "Sum"),
        @Type(value = Subtraction.class, name = "Subtraction"),
        @Type(value = Division.class, name = "Division"),
        @Type(value = Multiplication.class, name = "Multiplication"),
        @Type(value = Round.class, name = "Round"),
        @Type(value = Floor.class, name = "Floor"),
        @Type(value = Ceil.class, name = "Ceil"),
        @Type(value = Trunc.class, name = "Trunc"),

        @Type(value = Concat.class, name = "Concat"),
        @Type(value = Substring.class, name = "Substring"),
        @Type(value = LPad.class, name = "LPad"),
        @Type(value = RPad.class, name = "RPad"),
        @Type(value = Replace.class, name = "Replace"),
        @Type(value = FncSplitFormula.class, name = "FncSplitFormula"),

        @Type(value = ToDate.class, name = "ToDate"),
        @Type(value = MonthsBetween.class, name = "MonthsBetween"),
        @Type(value = DaysBetween.class, name = "DaysBetween"),
        @Type(value = AddDays.class, name = "AddDays"),
        @Type(value = AddMonths.class, name = "AddMonths"),

        @Type(value = CampoIntegracion.class, name = "CampoIntegracion"),
        @Type(value = FncValidaRut.class, name = "FncValidaRut"),

        @Type(value = ValorUFActual.class, name = "ValorUFActual"),
        @Type(value = ValorUFDia.class, name = "ValorUFDia"),
        @Type(value = ValorUFPrimerDia.class, name = "ValorUFPrimerDia"),
        @Type(value = ValorUFUltimoDia.class, name = "ValorUFUltimoDia"),
        
        @Type(value = FncEnriquecimiento.class, name = "FncEnriquecimiento")
})
public interface IFormulaFactor {
	
	/**
	 * Genera un string SQL de las columnas a seleccionar.
	 * Sólo la palabra clave 'WHERE' debiera ser generada por este método.
	 * @param tableAliases Un mapa de referencias a tablas/tipos de persona contra números (usados para determinar alias para las consultas SELECT)
	 * @return Una sentencia SQL
	 */
	public String toSQL(Map<FormulaOrigenKey, Integer> tableAliases);
	
	/**
	 * Genera un string SQL con el que valida que la fórmula esté configurada para generar SQL's correctos. 
	 * Sólo la palabra clave 'WHERE' debiera ser generada por este método.
	 * @param tableAliases Un mapa de referencias a tablas/tipos de persona contra números (usados para determinar alias para las consultas SELECT)
	 * @return Una sentencia SQL 
	 */
	public String toSafeSQL(Map<FormulaOrigenKey, Integer> tableAliases);
	public String toJSON();
}
