package cl.cardif.ics.icsrest.service.impl;

import cl.cardif.ics.domain.entity.carga.IcsCargaArchivo;
import cl.cardif.ics.icsrest.domain.dto.input.CargaArchivoInput;
import cl.cardif.ics.icsrest.domain.dto.output.RequestResultOutput;
import cl.cardif.ics.icsrest.exceptions.icscargaarchivo.IcsCargaArchivoServiceException;
import cl.cardif.ics.icsrest.repository.CargaArchivoRepository;
import cl.cardif.ics.icsrest.service.CargaArchivoService;
import cl.cardif.ics.icsrest.util.Constants;
import cl.cardif.ics.icsrest.util.converters.topojo.CargaArchivoToPojoConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityNotFoundException;
import javax.sql.DataSource;
import javax.sql.rowset.serial.SerialBlob;
import java.io.IOException;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@Service
public class CargaArchivoServiceImpl implements CargaArchivoService {
	private static final Logger LOG = LoggerFactory.getLogger(CargaArchivoServiceImpl.class);

	private static final String MSJ_ERROR_ARCHIVO_NO_GUARDADO = "Error al guardar el archivo";
	
	private JdbcTemplate jdbcTemplate;

	@Autowired private CargaArchivoRepository cargaArchivoRepository;

	@Override
	public RequestResultOutput verifyFileNameWithSp(CargaArchivoInput archivo) {
		LOG.info("verifyFileNameWithSp");

		long idProceso = archivo.getIdProceso();
		String fileName = archivo.getNombreArchivo();

		final String startLog = "verifyFileNameWithSp - idProceso="+idProceso+", fileName="+fileName;
		LOG.debug(startLog);

		RequestResultOutput result = new RequestResultOutput();
		result.setCode(-1);

		if (fileName.isEmpty()) {
			result.setMessage("El archivo no posee un nombre.");
		} else {

			int indexOfDot = fileName.lastIndexOf('.');
			if (indexOfDot == -1) {
				result.setMessage("El archivo no posee una extensión.");
			} else {

				String fileExtension = archivo.getExtensionArchivo();
				fileName = fileName.substring(0, indexOfDot);

				final String extensionLog = "verifyFileNameWithSp - fileExtension="+fileExtension;
				LOG.debug(extensionLog);

				Map<String, Object> spParams;
				try {
					SimpleJdbcCall call = new SimpleJdbcCall(jdbcTemplate)
							.withCatalogName(Constants.PACKAGE_UTIL_APP)
							.withProcedureName(Constants.PROCEDURE_VALIDAR_ARCHIVO)
							.declareParameters(
									new SqlParameter(Constants.PV_VALIDAR_ARCHIVO_PROCESO_ID, oracle.jdbc.OracleTypes.NUMBER),
									new SqlParameter(Constants.PV_VALIDAR_ARCHIVO_NOMBRE, oracle.jdbc.OracleTypes.VARCHAR),
									new SqlParameter(Constants.PV_VALIDAR_ARCHIVO_EXTENSION, oracle.jdbc.OracleTypes.VARCHAR)
							);
					
					spParams = new HashMap<>();
					spParams.put(Constants.PV_VALIDAR_ARCHIVO_PROCESO_ID, idProceso);
					spParams.put(Constants.PV_VALIDAR_ARCHIVO_NOMBRE, fileName);
					spParams.put(Constants.PV_VALIDAR_ARCHIVO_EXTENSION, fileExtension);
					
					LOG.info("verifyFileNameWithSp - Llamando SP");
					
					Map<String, Object> execute = call.execute(new MapSqlParameterSource(spParams));
					return new RequestResultOutput(execute);

				} catch (Exception e) {
					throw new IcsCargaArchivoServiceException("Error al verificar el archivo", e);
				}
			}
		}
		
		return result;
	}

	/**
	 * Carga el archivo en si como registro a la base de datos
	 */
	@Override
	public CargaArchivoInput createRowWithFile(MultipartFile file) {
		IcsCargaArchivo entity = new IcsCargaArchivo();
		try {
			Blob blob = new SerialBlob(file.getBytes());

			entity.setArchivoCarga(blob);
			this.cargaArchivoRepository.saveAndFlush(entity);

		} catch (EntityNotFoundException | SQLException | IOException e) {
			throw new IcsCargaArchivoServiceException(MSJ_ERROR_ARCHIVO_NO_GUARDADO, e);
		}

		return new CargaArchivoToPojoConverter().convert(entity);
	}

	/**
	 * Ingresa los datos de la carga que no son el archivo; fecha, etc
	 */
	@Override
	public void nuevo(CargaArchivoInput input, boolean isUpdate) {
		try {

			IcsCargaArchivo entity = this.cargaArchivoRepository.findOne(input.getId());
			entity.setIdCarga(input.getId());
			entity.setNombreCarga(input.getNombre());
			entity.setFechaRecepcionCarga(input.getFechaRecepcion());
			entity.setCodigoCarga(input.getCodigo());
			entity.setObservacionCarga(input.getObservacion());
			entity.setNombreArchivo(input.getNombreArchivo());
			entity.setExtensionArchivo(input.getExtensionArchivo());
			entity.setIdProceso(input.getIdProceso());

			this.cargaArchivoRepository.saveAndFlush(entity);
		} catch (EntityNotFoundException e) {
			throw new IcsCargaArchivoServiceException(MSJ_ERROR_ARCHIVO_NO_GUARDADO, e);
		}
	}

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
}
