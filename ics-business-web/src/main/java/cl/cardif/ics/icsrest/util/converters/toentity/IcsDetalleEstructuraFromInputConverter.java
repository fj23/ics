package cl.cardif.ics.icsrest.util.converters.toentity;

import cl.cardif.ics.domain.entity.common.IcsTipoDato;
import cl.cardif.ics.domain.entity.common.QIcsTipoDato;
import cl.cardif.ics.domain.entity.estructuras.IcsDetalleEstructura;
import cl.cardif.ics.domain.entity.estructuras.IcsSubestructura;
import cl.cardif.ics.domain.entity.estructuras.QIcsSubestructura;
import cl.cardif.ics.icsrest.domain.dto.input.DetalleEstructuraInput;
import cl.cardif.ics.icsrest.repository.SubestructurasRepository;
import cl.cardif.ics.icsrest.repository.TiposDatosRepository;
import com.querydsl.core.BooleanBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import java.math.BigDecimal;

@Component
public class IcsDetalleEstructuraFromInputConverter
    implements Converter<DetalleEstructuraInput, IcsDetalleEstructura> {

  static final Logger LOG = LoggerFactory.getLogger(IcsDetalleEstructuraFromInputConverter.class);

  @Autowired TiposDatosRepository tipoRepository;

  @Autowired SubestructurasRepository subestructurasRepository;

  @Autowired TiposDatosRepository tiposDatosRepo;

  @Override
  public IcsDetalleEstructura convert(DetalleEstructuraInput pojo) {

    IcsDetalleEstructura detalleEntity = new IcsDetalleEstructura();

    LOG.info("Convertir DetalleEstructuraInput a IcsDetalleEstructura");

    detalleEntity.setCodigoEstructura(pojo.getCodigo());
    detalleEntity.setDescripcionDetalleEstructura(pojo.getDescripcion());
    LOG.info("ID Subestructura : " + pojo.getIdSubEstructura());

    if (pojo.getIdSubEstructura() > 0) {
      BooleanBuilder bb = new BooleanBuilder();
      bb.and(QIcsSubestructura.icsSubestructura.idSubestructura.eq(pojo.getIdSubEstructura()));

      try {
    	IcsSubestructura subestructura = this.subestructurasRepository.findOne(bb);
    	detalleEntity.setIcsSubestructura(subestructura);
        LOG.info("Subestructura columna : " + subestructura.getIdSubestructura());
      } catch (EntityNotFoundException e) {
        throw e;
      }

    } 

    detalleEntity.setLargo(BigDecimal.valueOf(pojo.getLargo()));
    detalleEntity.setOrden(BigDecimal.valueOf(pojo.getOrden()));

    LOG.info("Obtener tipo de dato : " + pojo.getIdTipoDato());

    BooleanBuilder booleanBuilder = new BooleanBuilder();
    QIcsTipoDato qIcsTipoDato = QIcsTipoDato.icsTipoDato;

    booleanBuilder.and(qIcsTipoDato.idTipoDato.eq(pojo.getIdTipoDato()));
    IcsTipoDato tipoEntity = null;
    try {
      tipoEntity = this.tiposDatosRepo.findOne(booleanBuilder);
    } catch (EntityNotFoundException e) {
      throw e;
    }

    LOG.info("Obtiene tipo de dato");

    detalleEntity.setIcsTipoDato(tipoEntity);
    detalleEntity.setTipoDato("");
    detalleEntity.setVigenciaDetalleEstructura(pojo.getVigencia());
    return detalleEntity;
  }
}
