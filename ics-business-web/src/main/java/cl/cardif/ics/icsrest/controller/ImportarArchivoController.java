package cl.cardif.ics.icsrest.controller;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import cl.cardif.ics.domain.entity.carga.IcsArchivoImportado;
import cl.cardif.ics.domain.entity.carga.IcsGeneracionArchivo;
import cl.cardif.ics.domain.entity.carga.IcsResumenImportacion;
import cl.cardif.ics.domain.entity.common.IcsEvento;
import cl.cardif.ics.domain.entity.procesos.IcsResumenTransaccion;
import cl.cardif.ics.icsgeneratefile.entity.FilesInformation;
import cl.cardif.ics.icsrest.exceptions.icsimportaarchivo.IcsImportaArchivoServiceException;
import cl.cardif.ics.icsrest.service.ImportarArchivosService;
import cl.cardif.ics.icsrest.util.Constants;
import cl.cardif.ics.icsrest.util.OutputListWrapper;

@RestController
@RequestMapping("/api/importarArchivos")
public class ImportarArchivoController {
	private static final Logger LOG = LoggerFactory.getLogger(ImportarArchivoController.class);

	@Autowired private ImportarArchivosService importarArchivosService;

	@GetMapping("/eventos/{idProceso}")
	public ResponseEntity<List<IcsEvento>> getEventos(@PathVariable Long idProceso) {
		LOG.info("getEventos");
		return new ResponseEntity<>(this.importarArchivosService.getEventos(idProceso), HttpStatus.OK);
	}

	@GetMapping("/archivos/filters")
	public ResponseEntity<List<IcsResumenTransaccion>> getTrazabilidadArchivos(@RequestParam Map<String, String> filters) {
		LOG.info("getTrazabilidadArchivos");
		List<IcsResumenTransaccion> transacciones = this.importarArchivosService.getArchivosTransaccion(filters);
		for (IcsResumenTransaccion icsResumenTransaccion : transacciones) {
			icsResumenTransaccion.setIcsEvento(null);
			icsResumenTransaccion.setIcsProceso(null);
			icsResumenTransaccion.setIcsSocio(null);
		}

		return ResponseEntity.ok(transacciones);
	}

	@GetMapping(path = "/descargar/{idTransaccion}", produces = MediaType.TEXT_PLAIN_VALUE)
	public ResponseEntity<byte[]> descargarArchivo(@PathVariable Long idTransaccion) {
		LOG.info("descargarArchivo");
		try {
			List<IcsGeneracionArchivo> archivos = this.importarArchivosService.descargarArchivo(idTransaccion);

			if (!archivos.isEmpty()) {
				String filename = archivos.get(0).getNombreArchivo() + archivos.get(0).getExtensionArchivo();

				FilesInformation fileInformation =
					new FilesInformation(archivos.get(0).getNombreArchivo(), archivos.get(0).getExtensionArchivo());

				byte[] file = this.importarArchivosService.generateFile(String.valueOf(idTransaccion), fileInformation);

				if (file == null) {
					throw new IcsImportaArchivoServiceException("No existe el archivo");
				}

				this.importarArchivosService.deleteGeneratedFile(idTransaccion, 1L);
				LOG.debug("descargarArchivo - Archivo de transaccion {} eliminado luego de generar archivo", idTransaccion);

				MultiValueMap<String, String> args = new LinkedMultiValueMap<>();
				args.add("Content-Disposition", "attachment; filename=" + filename);
				args.add("File-Name", filename);

				return new ResponseEntity<>(file, args, HttpStatus.OK);
			} else {
				throw new IcsImportaArchivoServiceException("Datos no encontrados");
			}

		} catch (IcsImportaArchivoServiceException e) {
			LOG.error("Error al descargar archivo", e);
			MultiValueMap<String, String> args = new LinkedMultiValueMap<>();
			args.add("Exception-Message", e.getMessage());
			return new ResponseEntity<>(null, args, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping(path = "/cargar/{idTransaccion}", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<Void> cargarArchivo(@RequestPart("file") MultipartFile file, @PathVariable Long idTransaccion) {
		LOG.info("cargarArchivo");
		List<IcsArchivoImportado> archivoImportados = this.importarArchivosService.cargarArchivo(file, idTransaccion);
		try {
			LOG.info("cargarArchivo - Actualizando Importación");
			this.importarArchivosService.actualizaImportacion(idTransaccion);
		} catch (IcsImportaArchivoServiceException e) {
			this.importarArchivosService.eliminaImportacion(archivoImportados);
			throw new IcsImportaArchivoServiceException("El archivo no se pudo importar", e);
		}
		return ResponseEntity.ok().build();
	}

	@GetMapping("/historial/{idTransaccion}/page/{page}")
	public ResponseEntity<OutputListWrapper<IcsResumenImportacion>> getHistorialArchivos(
		@PathVariable Long idTransaccion, @PathVariable int page) {
		return this.getHistorialArchivos(idTransaccion, page, Constants.DEFAULT_PAGE_SIZE, "", "");
	}

	@GetMapping("/historial/{idTransaccion}/page/{page}/size/{size}")
	public ResponseEntity<OutputListWrapper<IcsResumenImportacion>> getHistorialArchivos(
		@PathVariable Long idTransaccion, @PathVariable int page, @PathVariable int size) {
		return this.getHistorialArchivos(idTransaccion, page, size, "", "");
	}

	@GetMapping("/historial/{idTransaccion}/page/{page}/size/{size}/sort/{sortColumn}/{sortOrder}")
	public ResponseEntity<OutputListWrapper<IcsResumenImportacion>> getHistorialArchivos(
		@PathVariable Long idTransaccion, @PathVariable int page, @PathVariable int size, @PathVariable String sortColumn,
		@PathVariable String sortOrder) {
		LOG.info("Obteniendo pagina de resumen de importacion de estados pagina {} tamaño {} columna {} orden {}", page, size, sortColumn,
				sortOrder);
		return new ResponseEntity<>(this.importarArchivosService.getHistorialArchivos(idTransaccion, page, size, sortColumn, sortOrder),
				HttpStatus.OK);
	}

	@GetMapping("/historial/{idTransaccion}/exportar")
	public ResponseEntity<List<IcsResumenImportacion>> exportResumenImportacion(@PathVariable Long idTransaccion) {
		LOG.info("Obteniendo exportacion de resumen de importacion de estados");
		return new ResponseEntity<>(this.importarArchivosService.exportResumenImportacion(idTransaccion), HttpStatus.OK);
	}

	@ExceptionHandler({ IcsImportaArchivoServiceException.class })
	public ResponseEntity<IcsImportaArchivoServiceException> handlerIcsImportaArchivoServiceException(
		IcsImportaArchivoServiceException ex) {
		return new ResponseEntity<>(ex, HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
