package cl.cardif.ics.icsrest.exceptions.icsdetalleestructura;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class IcsDetalleEstructuraForbiddenException extends RuntimeException {

  private static final long serialVersionUID = 3397992475249736227L;

  public IcsDetalleEstructuraForbiddenException(String exception) {
    super(exception);
  }
}
