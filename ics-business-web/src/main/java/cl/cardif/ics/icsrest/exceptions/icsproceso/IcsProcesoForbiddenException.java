package cl.cardif.ics.icsrest.exceptions.icsproceso;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class IcsProcesoForbiddenException extends RuntimeException {

  private static final long serialVersionUID = 3397992475249736227L;

  public IcsProcesoForbiddenException(String exception) {
    super(exception);
  }

  public IcsProcesoForbiddenException(String exception, Throwable t) {
    super(exception, t);
  }
}
