package cl.cardif.ics.icsrest.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cl.cardif.ics.domain.entity.plantillas.IcsPlantilla;

@Repository
public interface PlantillaRepository
    extends JpaRepository<IcsPlantilla, Long>,
        QueryDslPredicateExecutor<IcsPlantilla> {

	@Query(
			  "SELECT distinct(i.nombrePlantilla)"
			  + " FROM IcsPlantilla i"
	          + " WHERE i.icsEvento.idEvento IN(:where)"
	          + " ORDER BY i.nombrePlantilla")
	  List<String> findDistinctPlantillas(@Param("where") List<Long> where);
	
  @Query(
		  "SELECT distinct(i.nombrePlantilla)"
		  + " FROM IcsPlantilla i"
          + " WHERE i.icsEvento.idEvento IN(:where)"
          + " AND i.icsSocio.idSocio = :idSocio"
          + " ORDER BY i.nombrePlantilla")
  List<String> findDistinctPlantillas(@Param("idSocio") Long idSocio, @Param("where") List<Long> where);
  
  @Query(
	      "SELECT distinct(i.nombrePlantilla)"
	      + " FROM IcsPlantilla i"
	      + " WHERE i.icsEvento.idEvento = :idEvento"
	      + " ORDER BY i.nombrePlantilla")
  List<String> findDistinctPlantillas(@Param("idEvento") Long idEvento);
  
  @Query(
	      "SELECT distinct(i.nombrePlantilla)"
	      + " FROM IcsPlantilla i"
	      + " WHERE i.icsEvento.idEvento = :idEvento"
	      + " AND i.icsSocio.idSocio = :idSocio"
	      + " ORDER BY i.nombrePlantilla")
  List<String> findDistinctPlantillas(@Param("idSocio") Long idSocio, @Param("idEvento") Long idEvento);
}
