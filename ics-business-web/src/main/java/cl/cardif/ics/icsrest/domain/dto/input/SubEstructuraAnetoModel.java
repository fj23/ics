package cl.cardif.ics.icsrest.domain.dto.input;

import java.util.ArrayList;
import java.util.List;

public class SubEstructuraAnetoModel extends BaseColumnaModel {

	private List<ColumnaMetadatosModelInput> columnas;
	private String vigencia;

	public SubEstructuraAnetoModel() {
		super();
	}

	public SubEstructuraAnetoModel(Long id) {
		super();
		this.id = id;
	}

	public List<ColumnaMetadatosModelInput> getColumnas() {
		if (this.columnas != null) {
			return new ArrayList<>(this.columnas);
		} else {
			return new ArrayList<>();
		}
	}

	public void setColumnas(List<ColumnaMetadatosModelInput> columnas) {
		if (columnas != null) {
			this.columnas = new ArrayList<>(columnas);
		}
	}

	public String getVigencia() {
		return vigencia;
	}

	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}
}
