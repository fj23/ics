package cl.cardif.ics.icsrest.util.converters.topojo;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import cl.cardif.ics.domain.entity.procesos.VwResumenTrazabilidad;
import cl.cardif.ics.icsrest.domain.pojo.TrazabilidadArchivo;

@Component
public class TrazabilidadArchivoConverter 
	implements Converter<VwResumenTrazabilidad, TrazabilidadArchivo> {

	@Override
	public TrazabilidadArchivo convert(VwResumenTrazabilidad source) {

		TrazabilidadArchivo archivo = new TrazabilidadArchivo();
		archivo.setId(source.getIdTransaccion());
		archivo.setIdProceso(source.getIcsProceso().getIdProceso());
		archivo.setIdExterno(source.getIdExterno());
		archivo.setNombreSocio(source.getIcsSocio().getNombreSocio());
		archivo.setNombreEvento(source.getIcsEvento().getNombreEvento());
		archivo.setEstadoTransaccion(source.getEstadoDeTransaccion());
		archivo.setFechaInicio(source.getFechaInicio());
		archivo.setFechaFin(source.getFechaFin());
		archivo.setTipoTransaccion(source.getTipoTransaccion());
		archivo.setNombreArchivo(source.getNombreArchivo() != null ? source.getNombreArchivo() : "Sin Nombre");
		archivo.setLineasOriginal(source.getLineasOriginal() != null ? source.getLineasOriginal() : 0);
		archivo.setLineasIcsOk(source.getLineasIcsOk() != null ? source.getLineasIcsOk() : 0);
		archivo.setLineasCoreOk(source.getLineasCoreOk() != null ? source.getLineasCoreOk() : 0);
		archivo.setLineasIcsError(source.getLineasIcsError() != null ? source.getLineasIcsError() : 0);
		archivo.setLineasCoreError(source.getLineasCoreError() != null ? source.getLineasCoreError() : 0);
		archivo.setFlgSalida(source.getFlgSalida().equals("Y"));
		
		return archivo;
	}
}
