package cl.cardif.ics.icsrest.util.converters.toentity;

import cl.cardif.ics.domain.entity.procesos.IcsProceso;
import cl.cardif.ics.icsrest.domain.dto.input.ProcesoInput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class IcsProcesoFromInputConverter implements Converter<ProcesoInput, IcsProceso> {
  static final Logger LOG = LoggerFactory.getLogger(IcsProcesoFromInputConverter.class);

  @Override
  public IcsProceso convert(ProcesoInput input) {

    IcsProceso proceso = new IcsProceso();
    if (input.getId() != null && input.getId() > 0) {
      proceso.setIdProceso(input.getId());
    }
    proceso.setNombreProceso(input.getNombre());
    proceso.setCorreoEmail(input.getCorreoEmail());
    proceso.setCorreoSalida(input.getCorreoSalida());
    proceso.setCorreoHitos(input.getCorreoHitos());
    proceso.setFlagAneto(input.isSalidaAneto()? "Y" : "N");

    if (input.getVigencia() == null || input.getVigencia().isEmpty()) {
    	proceso.setVigenciaProceso("Y");
    } else {
    	proceso.setVigenciaProceso(input.getVigencia());
    }

    return proceso;
  }
}
