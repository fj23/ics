package cl.cardif.ics.icsrest.exceptions.icstrazabilidad;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class IcsTrazabilidadNotFoundException extends RuntimeException {

  private static final long serialVersionUID = -1527596879157446910L;

  public IcsTrazabilidadNotFoundException(String exception) {
    super(exception);
  }

  public IcsTrazabilidadNotFoundException(String exception, Throwable t) {
    super(exception, t);
  }
}
