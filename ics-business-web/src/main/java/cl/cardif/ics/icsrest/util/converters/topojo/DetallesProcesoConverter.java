package cl.cardif.ics.icsrest.util.converters.topojo;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import cl.cardif.ics.domain.entity.procesos.IcsDetalleProceso;
import cl.cardif.ics.icsrest.domain.dto.output.DetalleProcesoOutput;

@Component
public class DetallesProcesoConverter
    implements Converter<IcsDetalleProceso, DetalleProcesoOutput> {

  @Override
  public DetalleProcesoOutput convert(IcsDetalleProceso icsDetalleProceso) {

	  DetalleProcesoOutput detalle = new DetalleProcesoOutput();
      detalle.setId(icsDetalleProceso.getIdDetalleProceso());
      detalle.setIdProceso(icsDetalleProceso.getIcsProceso().getIdProceso());

      return detalle;
  }
}
