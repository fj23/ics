package cl.cardif.ics.icsrest.exceptions.icsplantilla;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class IcsPlantillaBadRequestException extends RuntimeException {

  private static final long serialVersionUID = -6459654653826812166L;

  public IcsPlantillaBadRequestException(String exception) {
    super(exception);
  }

  public IcsPlantillaBadRequestException(String exception, Throwable t) {
    super(exception, t);
  }
}
