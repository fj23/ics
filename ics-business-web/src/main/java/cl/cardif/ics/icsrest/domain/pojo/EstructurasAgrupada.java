package cl.cardif.ics.icsrest.domain.pojo;

public class EstructurasAgrupada {

  private String descripcionAgrupacion;
  private long idEstructura;
  private long idGrupoEstructuras;
  private String indice;

  public EstructurasAgrupada() {
    // Constructor JavaBean
  }

  public String getDescripcionAgrupacion() {
    return descripcionAgrupacion;
  }

  public void setDescripcionAgrupacion(String descripcionAgrupacion) {
    this.descripcionAgrupacion = descripcionAgrupacion;
  }

  public long getIdEstructura() {
    return idEstructura;
  }

  public void setIdEstructura(long idEstructura) {
    this.idEstructura = idEstructura;
  }

  public long getIdGrupoEstructuras() {
    return idGrupoEstructuras;
  }

  public void setIdGrupoEstructuras(long idGrupoEstructuras) {
    this.idGrupoEstructuras = idGrupoEstructuras;
  }

  public String getIndice() {
    return indice;
  }

  public void setIndice(String indice) {
    this.indice = indice;
  }
}
