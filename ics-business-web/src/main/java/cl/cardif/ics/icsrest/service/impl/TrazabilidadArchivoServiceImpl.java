package cl.cardif.ics.icsrest.service.impl;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpHeaders;
import org.springframework.jdbc.UncategorizedSQLException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Service;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.OrderSpecifier;

import cl.cardif.ics.domain.entity.carga.IcsGeneracionArchivo;
import cl.cardif.ics.domain.entity.carga.QIcsGeneracionArchivo;
import cl.cardif.ics.domain.entity.common.IcsParametro;
import cl.cardif.ics.domain.entity.common.IcsTipoExtension;
import cl.cardif.ics.domain.entity.common.QIcsParametro;
import cl.cardif.ics.domain.entity.plantillas.IcsArchivo;
import cl.cardif.ics.domain.entity.procesos.IcsDetalleTransaccion;
import cl.cardif.ics.domain.entity.procesos.IcsJobs;
import cl.cardif.ics.domain.entity.procesos.IcsRespaldoInterfaces;
import cl.cardif.ics.domain.entity.procesos.QIcsDetalleTransaccion;
import cl.cardif.ics.domain.entity.procesos.QIcsRespaldoInterfaces;
import cl.cardif.ics.domain.entity.procesos.QVwResumenTrazabilidad;
import cl.cardif.ics.domain.entity.procesos.VwResumenTrazabilidad;
import cl.cardif.ics.icsgeneratefile.controller.GenerateController;
import cl.cardif.ics.icsgeneratefile.entity.FilesDatas;
import cl.cardif.ics.icsgeneratefile.entity.FilesInformation;
import cl.cardif.ics.icsrest.domain.dto.output.RequestResultOutput;
import cl.cardif.ics.icsrest.domain.pojo.DetalleTransaccion;
import cl.cardif.ics.icsrest.domain.pojo.TrazabilidadArchivo;
import cl.cardif.ics.icsrest.domain.pojo.TrazabilidadLog;
import cl.cardif.ics.icsrest.domain.pojo.TrazabilidadRespaldo;
import cl.cardif.ics.icsrest.exceptions.icstrazabilidad.IcsTrazabilidadServiceException;
import cl.cardif.ics.icsrest.repository.ArchivoRepository;
import cl.cardif.ics.icsrest.repository.DetalleTransaccionRepository;
import cl.cardif.ics.icsrest.repository.GeneracionArchivoRepository;
import cl.cardif.ics.icsrest.repository.JobsRepository;
import cl.cardif.ics.icsrest.repository.ParametrosRepository;
import cl.cardif.ics.icsrest.repository.RespaldoInterfacesRepository;
import cl.cardif.ics.icsrest.repository.TiposExtensionRepository;
import cl.cardif.ics.icsrest.repository.VwResumenTranzabilidadRepository;
import cl.cardif.ics.icsrest.service.TrazabilidadArchivoService;
import cl.cardif.ics.icsrest.util.OutputListWrapper;
import cl.cardif.ics.icsrest.util.Util;

@Service
public class TrazabilidadArchivoServiceImpl implements TrazabilidadArchivoService {

	private static final Logger LOG = LoggerFactory.getLogger(TrazabilidadArchivoServiceImpl.class);

	private static final String SORT_ID = "idTransaccion";
	private static final String SORT_ID_PROCESO = "icsProceso.idProceso";
	private static final String SORT_SOCIO = "icsSocio.nombreSocio";
	private static final String SORT_EVENTO = "icsEvento.nombreEvento";
	private static final String SORT_ARCHIVO = "nombreArchivo";
	private static final String SORT_ESTADO = "estadoDeTransaccion";
	private static final String SORT_FECHA_INICIO = "fechaInicio";
	private static final String SORT_FECHA_FIN = "fechaFin";
	private static final String SORT_LINEAS_ORIGINAL = "lineasOriginal";
	private static final String SORT_LINEAS_ICS_OK = "lineasIcsOk";
	private static final String SORT_LINEAS_CORE_OK = "lineasCoreOk";
	private static final String SORT_LINEAS_ICS_NOK = "lineasIcsError";
	private static final String SORT_LINEAS_CORE_NOK = "lineasCoreError";

	@Autowired
	private GenerateController generateController;

	@Autowired
	private ConversionService conversionService;

	@Autowired
	private VwResumenTranzabilidadRepository vwResumenTranzabilidadRepository;
	@Autowired
	private GeneracionArchivoRepository generacionArchivoRepository;
	@Autowired
	private DetalleTransaccionRepository detalleTransaccionRepository;
	@Autowired
	private ParametrosRepository parametrosRepository;
	@Autowired
	private RespaldoInterfacesRepository respaldoInterfacesRepository;
	@Autowired
	private ArchivoRepository archivoRepository;
	@Autowired
	private TiposExtensionRepository tiposExtensionRepository;
	@Autowired
	private JobsRepository jobsRepository;

	private JdbcTemplate jdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	private String obtenerColumnaOrdenInterna(String sortColumn) {
		switch (sortColumn) {
		case "id":
			return SORT_ID;
		case "idProceso":
			return SORT_ID_PROCESO;
		case "nombreSocio":
			return SORT_SOCIO;
		case "nombreEvento":
			return SORT_EVENTO;
		case "estadoTransaccion":
			return SORT_ESTADO;
		case "nombreArchivo":
			return SORT_ARCHIVO;
		case "fechaInicio":
			return SORT_FECHA_INICIO;
		case "fechaFin":
			return SORT_FECHA_FIN;
		case "lineasOriginal":
			return SORT_LINEAS_ORIGINAL;
		case "lineasIcsOk":
			return SORT_LINEAS_ICS_OK;
		case "lineasCoreOk":
			return SORT_LINEAS_CORE_OK;
		case "lineasIcsError":
			return SORT_LINEAS_ICS_NOK;
		case "lineasCoreError":
			return SORT_LINEAS_CORE_NOK;
		default:
			return null;
		}
	}

	@Override
	public OutputListWrapper<TrazabilidadArchivo> getTrazabilidadArchivos(int page, int size, String sortColumn,
			String sortOrder, Map<String, String> filters) {
		final String method = "getTrazabilidadArchivos";
		LOG.info(method);

		Pageable paginador;

		if (sortColumn.isEmpty()) {
			paginador = new PageRequest(page, size);
		} else {
			String campoOrdenEntity = this.obtenerColumnaOrdenInterna(sortColumn);
			if (campoOrdenEntity != null) {
				Sort sorter = new Sort(Sort.Direction.fromString(sortOrder), campoOrdenEntity);

				final String logSort = method + " - sorter=" + sorter.toString();
				LOG.debug(logSort);

				paginador = new PageRequest(page, size, sorter);
			} else {
				paginador = new PageRequest(page, size);
			}
		}

		final String logPaginador = method + " - paginador=" + paginador.toString();
		LOG.debug(logPaginador);

		BooleanBuilder bb = this.getPredicate(filters);
		Iterable<VwResumenTrazabilidad> iteraTransacciones = vwResumenTranzabilidadRepository.findAll(bb, paginador);
		long countResumenTransaccions = vwResumenTranzabilidadRepository.count(bb);

		final String countLog = "Transacciones obtenidas : " + countResumenTransaccions;
		LOG.debug(countLog);

		List<TrazabilidadArchivo> archivos = new ArrayList<>();
		for (VwResumenTrazabilidad transaccion : iteraTransacciones) {
			TrazabilidadArchivo archivo = conversionService.convert(transaccion, TrazabilidadArchivo.class);
			archivos.add(archivo);
		}

		return new OutputListWrapper<>(archivos, (int) countResumenTransaccions, "OK");
	}

	private BooleanBuilder getPredicate(Map<String, String> filters) {
		BooleanBuilder predicate = new BooleanBuilder();
		QVwResumenTrazabilidad qIcsResumenTransaccion = QVwResumenTrazabilidad.vwResumenTrazabilidad;

		for (Map.Entry<String, String> entry : filters.entrySet()) {
			LOG.debug("Filtrando por (por si aplica) {Key : " + entry.getKey() + " Value : " + entry.getValue() + "}");
		}

		if (filters.containsKey("socio")) {
			predicate.and(qIcsResumenTransaccion.icsSocio.idSocio.eq(Long.valueOf(filters.get("socio"))));
		}

		if (filters.containsKey("idCarga")) {
			predicate.and(qIcsResumenTransaccion.idTransaccion.eq(Long.valueOf(filters.get("idCarga"))));
		}

		if (filters.containsKey("idExterno")) {
			predicate.and(qIcsResumenTransaccion.idExterno.equalsIgnoreCase(filters.get("idExterno")));
		}

		HashSet<Long> idsEventos = Util.getEventsAuthorized();
		if (filters.containsKey("evento") && idsEventos.contains(Long.valueOf(filters.get("evento")))) {
			predicate.and(qIcsResumenTransaccion.icsEvento.idEvento.eq(Long.valueOf(filters.get("evento"))));
		} else {
			predicate.and(qIcsResumenTransaccion.icsEvento.idEvento.in(idsEventos));
		}

		if (filters.containsKey("estado")) {
			predicate.and(qIcsResumenTransaccion.estadoDeTransaccion.eq(filters.get("estado")));
		}

		try {
			if (filters.containsKey("recepcionadoDesde") && filters.containsKey("recepcionadoHasta")) {
				predicate.and(qIcsResumenTransaccion.fechaInicio.between(
						this.convertToStartDate(filters.get("recepcionadoDesde")),
						this.convertToEndDate(filters.get("recepcionadoHasta"))));
			} else {
				if (filters.containsKey("recepcionadoDesde")) {
					predicate.and(qIcsResumenTransaccion.fechaInicio
							.after(this.convertToStartDate(filters.get("recepcionadoDesde"))));
				}
				if (filters.containsKey("recepcionadoHasta")) {
					predicate.and(qIcsResumenTransaccion.fechaInicio
							.before(this.convertToEndDate(filters.get("recepcionadoHasta"))));
				}
			}
		} catch (ParseException e) {
			LOG.error("Error al filtrar por fecha inicio", e);
		}

		try {
			if (filters.containsKey("cargadoDesde") && filters.containsKey("cargadoHasta")) {
				predicate.and(
						qIcsResumenTransaccion.fechaFin.between(this.convertToStartDate(filters.get("cargadoDesde")),
								this.convertToEndDate(filters.get("cargadoHasta"))));
			} else {
				if (filters.containsKey("cargadoDesde")) {
					predicate.and(qIcsResumenTransaccion.fechaFin
							.after(this.convertToStartDate(filters.get("cargadoDesde"))));
				}
				if (filters.containsKey("cargadoHasta")) {
					predicate.and(
							qIcsResumenTransaccion.fechaFin.before(this.convertToEndDate(filters.get("cargadoHasta"))));
				}
			}
		} catch (ParseException e) {
			LOG.error("Error al filtrar por fecha fin", e);
		}

		return predicate;
	}

	private Date convertToStartDate(String time) throws ParseException {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(Long.valueOf(time));
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}

	private Date convertToEndDate(String time) throws ParseException {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(Long.valueOf(time));
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		cal.set(Calendar.MILLISECOND, 59);
		return cal.getTime();
	}

	@Override
	public List<DetalleTransaccion> getDetalleTransaccion(Long id) {
		QIcsDetalleTransaccion qIcsDetalleTransaccion = QIcsDetalleTransaccion.icsDetalleTransaccion;

		BooleanBuilder builder = new BooleanBuilder().and(qIcsDetalleTransaccion.idTransaccion.eq(id));

		OrderSpecifier<Long> sorter = qIcsDetalleTransaccion.idDetalleTransaccion.asc();

		Iterable<IcsDetalleTransaccion> iteraDetallesTransaccion = this.detalleTransaccionRepository.findAll(builder,
				sorter);

		List<DetalleTransaccion> detalles = new ArrayList<>();
		for (IcsDetalleTransaccion icsDetalle : iteraDetallesTransaccion) {
			DetalleTransaccion detalle = conversionService.convert(icsDetalle, DetalleTransaccion.class);
			detalles.add(detalle);
		}

		return detalles;

	}

	@Override
	public List<TrazabilidadArchivo> exportTrazabilidadArchivos(Map<String, String> filters) {
		LOG.info("Obteniendo Exportación de Transacciones");

		Iterable<VwResumenTrazabilidad> iteraResumen = this.vwResumenTranzabilidadRepository
				.findAll(this.getPredicate(filters));

		List<TrazabilidadArchivo> trazaCompleta = new ArrayList<>();
		for (VwResumenTrazabilidad transaccion : iteraResumen) {
			TrazabilidadArchivo trazabilidad = conversionService.convert(transaccion, TrazabilidadArchivo.class);
			trazaCompleta.add(trazabilidad);
		}

		return trazaCompleta;
	}

	@Override
	public List<IcsParametro> getParametrosTrazabilidad() {
		BooleanBuilder builder = new BooleanBuilder();
		QIcsParametro qIcsParametro = QIcsParametro.icsParametro;

		builder.and(qIcsParametro.codParametro.eq("MENSAJES_TRAZA"));
		builder.and(qIcsParametro.vigencia.eq("Y"));

		Sort sorter = new Sort(Direction.ASC, Arrays.asList("valorParametro"));

		List<IcsParametro> parametros = new ArrayList<>();
		Iterable<IcsParametro> iterableParametros = this.parametrosRepository.findAll(builder, sorter);
		for (IcsParametro icsParametro : iterableParametros) {
			parametros.add(icsParametro);
		}

		return parametros;
	}

	@Override
	public TrazabilidadRespaldo downloadOriginal(Long id) {
		TrazabilidadRespaldo archivo = null;

		BooleanBuilder builder;
		QIcsRespaldoInterfaces qIcsRespaldoInterfaces = QIcsRespaldoInterfaces.icsRespaldoInterfaces;

		builder = new BooleanBuilder();
		builder.and(qIcsRespaldoInterfaces.idTrasaccion.eq(id));

		IcsRespaldoInterfaces respaldo = this.respaldoInterfacesRepository.findOne(builder);

		if (respaldo == null)
			throw new IcsTrazabilidadServiceException("No existe archivo original en respaldo interfaces");

		IcsArchivo icsArchivo = this.archivoRepository.findOne(respaldo.getIdArchivo());

		if (icsArchivo == null)
			throw new IcsTrazabilidadServiceException("No existe archivo original");

		IcsTipoExtension icsTipoExtension = this.tiposExtensionRepository
				.findOne(icsArchivo.getIcsTipoExtension().getIdExtension());

		if (icsTipoExtension == null) {
			throw new IcsTrazabilidadServiceException("Archivo original no posee extension de tipo de archivo");
		}

		archivo = new TrazabilidadRespaldo(respaldo.getArchivoOriginal(), icsTipoExtension.getFormatoExtension(),
				icsArchivo.getNombreArchivo());

		return archivo;
	}

	@Override
	public FilesInformation getFileInformation(Long id) {
		return this.generateController.generateFiles(String.valueOf(id));
	}

	@Override
	public byte[] generateFile(String idTransaction, FilesInformation file) {
		return this.generateController.getFile(idTransaction, file);
	}

	@Override
	public void deleteGeneratedFile(Long idTransaction, Long tipoArchivo) {
		this.generateController.deleteInformation(idTransaction, tipoArchivo);
	}

	@Override
	public IcsJobs consolidar() {
		IcsJobs job = this.jobsRepository.findOne(1L);
		if (job == null) {
			LOG.error("No se encontro el job de consolidacion");
			throw new IcsTrazabilidadServiceException(
					"No existe el resumen del proceso de consolidación, intente más tarde.");
		}
		return job;
	}

	@Override
	public void finalizarProceso(Long id) {
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("IN_ID_TRANSAC", id);
			LOG.info("\n\nLlamada a PCK_UTIL_ICS.PRC_ICS_CIERRA_TRANSACCION \n" + id + "\n");
			RequestResultOutput result = null;
			SimpleJdbcCall call = new SimpleJdbcCall(jdbcTemplate).withCatalogName("PCK_UTIL_ICS")
					.withProcedureName("PRC_ICS_CIERRA_TRANSACCION")
					.declareParameters(new SqlParameter("IN_ID_TRANSAC", oracle.jdbc.OracleTypes.NUMBER));

			Map<String, Object> execute = call.execute(new MapSqlParameterSource(map));
			result = new RequestResultOutput(execute);
			if (result.getCode() != 0) {
				LOG.error(result.getMessage());
				throw new IcsTrazabilidadServiceException(result.getMessage());
			}
		} catch (UncategorizedSQLException e) {
			LOG.error("Error en la ejecución del SP", e);
			throw new IcsTrazabilidadServiceException("Error en la ejecución del SP");
		}
	}

	@Override
	public TrazabilidadArchivo getTrazabilidadArchivo(Long id) {
		VwResumenTrazabilidad icsResume = this.vwResumenTranzabilidadRepository.findOne(id);

		if (icsResume == null) {
			return null;
		} else {
			return conversionService.convert(icsResume, TrazabilidadArchivo.class);
		}
	}

	private void ejecutarPrcGeneracionTraza(long idTransaccion) {
		LOG.info("ejecutarPrcGeneracionTrazaLog");
		try {
			SimpleJdbcCall call = new SimpleJdbcCall(jdbcTemplate).withCatalogName("PCK_UTIL_ICS")
					.withProcedureName("PRC_ICS_LOG_TRAZA")
					.declareParameters(new SqlParameter("IN_ID_TRANSAC", oracle.jdbc.OracleTypes.NUMBER),
							new SqlOutParameter("OUT_ERROR", oracle.jdbc.OracleTypes.NUMBER),
							new SqlOutParameter("OUT_DESCERROR", oracle.jdbc.OracleTypes.VARCHAR));

			Map<String, Object> paramMap = new HashMap<>();
			paramMap.put("IN_ID_TRANSAC", idTransaccion);

			Map<String, Object> execute = call.execute(new MapSqlParameterSource(paramMap));
			BigDecimal codigoError = (BigDecimal) execute.get("OUT_ERROR");
			String descError = (String) execute.get("OUT_DESCERROR");

			// 1 = error no controlado
			// 2 = no hay datos
			if (codigoError.intValue() != 0) {
				throw new IcsTrazabilidadServiceException(descError);
			}

		} catch (UncategorizedSQLException e) {
			LOG.error("Error en la ejecución del SP", e);
			throw new IcsTrazabilidadServiceException("Error en la ejecución del SP");
		}
	}

	private Iterable<IcsGeneracionArchivo> buscarLineasLog(Long idTransaccion) {
		QIcsGeneracionArchivo qIcsGeneracionArchivo = QIcsGeneracionArchivo.icsGeneracionArchivo;

		BooleanBuilder lineasArchivoTransaccion = new BooleanBuilder()
				.and(qIcsGeneracionArchivo.idTransaccion.eq(idTransaccion))
				.and(qIcsGeneracionArchivo.tipoArchivo.eq(1L));

		OrderSpecifier<Long> ordenarPorLinea = qIcsGeneracionArchivo.linea.asc();

		Iterable<IcsGeneracionArchivo> iteraRespaldo = this.generacionArchivoRepository
				.findAll(lineasArchivoTransaccion, ordenarPorLinea);
		return iteraRespaldo;
	}

	@Override
	public TrazabilidadLog downloadLog(Long idTransaccion) {
		LOG.info("downloadLog");

		this.ejecutarPrcGeneracionTraza(idTransaccion);

		Iterable<IcsGeneracionArchivo> iteraLineasLog = this.buscarLineasLog(idTransaccion);
		IcsGeneracionArchivo primeraLinea = iteraLineasLog.iterator().next();
		FilesInformation fileInfo = new FilesInformation(primeraLinea.getNombreArchivo(),
				primeraLinea.getExtensionArchivo());

		List<FilesDatas> listData = new ArrayList<>();
		for (IcsGeneracionArchivo respaldo : iteraLineasLog) {
			FilesDatas fData = new FilesDatas(respaldo.getDatos(), respaldo.getHojaDestino(), respaldo.getTipoDato());
			listData.add(fData);
		}

		byte[] data = generateController.generateFileFromInfoData(fileInfo, listData);

		return new TrazabilidadLog(data, primeraLinea.getExtensionArchivo(), primeraLinea.getNombreArchivo());
	}

	@Override
	public HttpHeaders getHttpHeadersForFile(String fileName, String fileType) {

		Map<String, String> contentTypesMap = new HashMap<>();
		contentTypesMap.put(".xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		contentTypesMap.put(".xls", "application/vnd.ms-excel");
		contentTypesMap.put(".txt", "text/plain");
		contentTypesMap.put(".csv", "text/csv");

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition", "attachment; filename=\"" + fileName + fileType + "\"");
		headers.add("File-Name", fileName + fileType);
		headers.add("Content-Type", contentTypesMap.get(fileType));
		return headers;
	}

}
