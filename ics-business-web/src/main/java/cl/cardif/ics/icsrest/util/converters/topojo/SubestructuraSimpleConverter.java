package cl.cardif.ics.icsrest.util.converters.topojo;

import cl.cardif.ics.domain.entity.estructuras.IcsSubestructura;
import cl.cardif.ics.icsrest.domain.dto.output.SubEstructuraSimpleOutput;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class SubestructuraSimpleConverter
    implements Converter<IcsSubestructura, SubEstructuraSimpleOutput> {

  @Override
  public SubEstructuraSimpleOutput convert(IcsSubestructura icsSubestructura) {

    SubEstructuraSimpleOutput subestructura = new SubEstructuraSimpleOutput();
    subestructura.setId(icsSubestructura.getIdSubestructura());
    subestructura.setDescripcion(icsSubestructura.getDescripcionSubestructura());
    subestructura.setCodigo(icsSubestructura.getCodigoSubestructura());

    return subestructura;
  }
}
