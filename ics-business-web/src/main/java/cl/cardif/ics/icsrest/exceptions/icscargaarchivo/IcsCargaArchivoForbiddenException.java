package cl.cardif.ics.icsrest.exceptions.icscargaarchivo;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class IcsCargaArchivoForbiddenException extends RuntimeException {

  private static final long serialVersionUID = 3397992475249736227L;

  public IcsCargaArchivoForbiddenException(String exception) {
    super(exception);
  }

  public IcsCargaArchivoForbiddenException(String exception, Throwable t) {
    super(exception, t);
  }
}
