package cl.cardif.ics.icsrest.util.converters.topojo;

import cl.cardif.ics.domain.entity.homologaciones.IcsTipoHomologacion;
import cl.cardif.ics.icsrest.domain.dto.output.TipoHomologacionOutput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class TipoHomologacionConverter
    implements Converter<IcsTipoHomologacion, TipoHomologacionOutput> {

  static final Logger LOG = LoggerFactory.getLogger(TipoHomologacionConverter.class);

  @Override
  public TipoHomologacionOutput convert(IcsTipoHomologacion entity) {

    TipoHomologacionOutput pojo = new TipoHomologacionOutput();
    pojo.setId(entity.getIdDeTipoHomologacion());
    pojo.setConcepto(entity.getNombreHomologacion());
    pojo.setVigencia(entity.getVigenciaTipoHomologacion());
    return pojo;
  }
}
