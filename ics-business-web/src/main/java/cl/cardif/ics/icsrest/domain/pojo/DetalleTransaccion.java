package cl.cardif.ics.icsrest.domain.pojo;

import java.util.Date;

public class DetalleTransaccion {

	private Long idDetalleTransaccion;
	private String proceso;
	private String socio;
	private String detalleEtapa;
	private Date fechaInicio;
	private Date fechaFin;

	public DetalleTransaccion() {
		super();
	}

	public Long getIdDetalleTransaccion() {
		return idDetalleTransaccion;
	}

	public void setIdDetalleTransaccion(Long idDetalleTransaccion) {
		this.idDetalleTransaccion = idDetalleTransaccion;
	}

	public String getProceso() {
		return proceso;
	}

	public void setProceso(String proceso) {
		this.proceso = proceso;
	}

	public String getSocio() {
		return socio;
	}

	public void setSocio(String socio) {
		this.socio = socio;
	}

	public String getDetalleEtapa() {
		return detalleEtapa;
	}

	public void setDetalleEtapa(String detalleEtapa) {
		this.detalleEtapa = detalleEtapa;
	}

	public Date getFechaInicio() {
		return this.fechaInicio != null ? new Date(this.fechaInicio.getTime()) : null;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio != null ? new Date(fechaInicio.getTime()) : null;
	}

	public Date getFechaFin() {
		return this.fechaFin != null ? new Date(this.fechaFin.getTime()) : null;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin != null ? new Date(fechaFin.getTime()) : null;
	}

}
