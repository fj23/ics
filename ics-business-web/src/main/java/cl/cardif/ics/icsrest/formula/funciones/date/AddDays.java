package cl.cardif.ics.icsrest.formula.funciones.date;

import java.util.ArrayList;
import java.util.Map;

import cl.cardif.ics.icsrest.formula.FormulaParent;
import cl.cardif.ics.icsrest.formula.IFormulaFactor;
import cl.cardif.ics.icsrest.formula.IStringFactor;
import cl.cardif.ics.icsrest.formula.operadores.SysDate;
import cl.cardif.ics.icsrest.util.FormulaOrigenKey;

public class AddDays extends FormulaParent implements IStringFactor {
	public AddDays() {
		hijos = new ArrayList<>();
	}

	@Override
	public int getMaximumChildrenCount() {
		return 2;
	}

	@Override
	public String toSafeSQL(Map<FormulaOrigenKey, Integer> tableAliases) {
		StringBuilder sb = new StringBuilder()
			.append("SYSDATE")
			.append(" + ")
			.append(getHijo(1).toSafeSQL(tableAliases));

		return "(" + sb.toString() + ")";
	}

	@Override
	public String toSQL(Map<FormulaOrigenKey, Integer> tableAliases) {
		StringBuilder sb = new StringBuilder();
		
		IFormulaFactor date = getHijo(0);

		if (date instanceof SysDate) {
			sb.append(date.toSQL(tableAliases));
		} else {
			sb.append("TO_DATE")
				.append("(")
				.append(date.toSQL(tableAliases)).append(",")
				.append("'dd-mm-yyyy'")
				.append(")");
		}
		
		sb.append(" + ")
			.append(getHijo(1).toSQL(tableAliases));

		return "(" + sb.toString() + ")";
	}
}
