package cl.cardif.ics.icsrest.util.converters.topojo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import cl.cardif.ics.domain.entity.plantillas.IcsAtributoUsuario;
import cl.cardif.ics.icsrest.domain.dto.output.AtributoUsuarioOutput;

@Component
public class AtributoUsuarioConverter implements Converter<IcsAtributoUsuario, AtributoUsuarioOutput> {

	@Override
	public AtributoUsuarioOutput convert(IcsAtributoUsuario entity) {
		AtributoUsuarioOutput pojo = new AtributoUsuarioOutput();

		pojo.setIdAtributoUsuario(entity.getIdAtributoUsuario());
		pojo.setNombreColumna(entity.getNombreColumna());
		pojo.setTipoPersona(entity.getTipoPersona());
		pojo.setFlgCardinalidad(entity.getFlgCardinalidad());
		pojo.setNombreTablaDestino(entity.getIcsAtributoNegocio().getNombreTablaDestino());

		return pojo;
	}

	public List<AtributoUsuarioOutput> convertList(Collection<IcsAtributoUsuario> entities) {
		List<AtributoUsuarioOutput> pojoList = new ArrayList<>();

		for (IcsAtributoUsuario entity : entities) {
			pojoList.add(this.convert(entity));
		}

		return pojoList;
	}
}
