package cl.cardif.ics.icsrest.exceptions.icsimportaarchivo;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.io.Serializable;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class IcsImportaArchivoServiceException extends RuntimeException implements Serializable {

  private static final long serialVersionUID = -7033943522744572117L;

  public IcsImportaArchivoServiceException(String exception) {
    super(exception);
  }

  public IcsImportaArchivoServiceException(String exception, Throwable t) {
    super(exception, t);
  }
}
