package cl.cardif.ics.icsrest.exceptions.icssubestructura;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class IcsSubestructuraUnauthorizedException extends RuntimeException {

  private static final long serialVersionUID = 3815682526998866691L;

  public IcsSubestructuraUnauthorizedException(String exception) {
    super(exception);
  }

  public IcsSubestructuraUnauthorizedException(String exception, Throwable t) {
    super(exception, t);
  }
}
