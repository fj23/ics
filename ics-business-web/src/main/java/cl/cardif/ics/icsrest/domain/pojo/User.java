package cl.cardif.ics.icsrest.domain.pojo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class User {
	private static final Logger LOG = LoggerFactory.getLogger(User.class);
	
	List<Role> roles;
	private String username;
	private String email;
	private String password;
	private String versionBackend;
	private Integer inactivityNotice;

	public User() {
		super();
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getVersionBackend() {
		return versionBackend;
	}

	public void setVersionBackend(String versionBackend) {
		this.versionBackend = versionBackend;
	}

	public void setRolesFromMap(Map<Long, DetalleRol> roles) {
		this.roles = new ArrayList<Role>();
		for (Map.Entry<Long, DetalleRol> entry : roles.entrySet()) {
			try {
				this.roles.add(new Role(entry.getValue().getCodigo().toString()));
			} catch (NullPointerException e) {
				LOG.info("entry.getValue() = {}", entry.getValue());
				LOG.error("No se pudo inyectar el rol", e);
			}
		}
	}

	public void setRolesFromString(List<String> roles) {
		this.roles = new ArrayList<Role>();
		for (String role : roles) {
			this.roles.add(new Role(role));
		}
	}

	public Integer getInactivityNotice() {
		return inactivityNotice;
	}

	public void setInactivityNotice(Integer inactivityNotice) {
		this.inactivityNotice = inactivityNotice;
	}

}
