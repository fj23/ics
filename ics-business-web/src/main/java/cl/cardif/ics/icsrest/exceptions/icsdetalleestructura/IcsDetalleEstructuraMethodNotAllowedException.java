package cl.cardif.ics.icsrest.exceptions.icsdetalleestructura;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
public class IcsDetalleEstructuraMethodNotAllowedException extends RuntimeException {

  private static final long serialVersionUID = -6814524510236406273L;

  public IcsDetalleEstructuraMethodNotAllowedException(String exception) {
    super(exception);
  }
}
