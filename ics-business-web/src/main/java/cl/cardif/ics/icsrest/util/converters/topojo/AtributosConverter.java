package cl.cardif.ics.icsrest.util.converters.topojo;

import cl.cardif.ics.domain.entity.plantillas.IcsAtributos;
import cl.cardif.ics.icsrest.domain.dto.output.DetalleEstructuraOutput;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class AtributosConverter implements Converter<IcsAtributos, DetalleEstructuraOutput> {

  @Override
  public DetalleEstructuraOutput convert(IcsAtributos entity) {

    DetalleEstructuraOutput detalle = new DetalleEstructuraOutput();
    detalle.setId(entity.getIdAtributo());
    detalle.setDescripcion(entity.getNombreColumna());
    detalle.setOrden(entity.getNumeroColumna());
    detalle.setPosicionInicial(entity.getPosicionInicial());
    detalle.setPosicionFinal(entity.getPosicionFinal());

    return detalle;
  }
}
