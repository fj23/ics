package cl.cardif.ics.icsrest.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cl.cardif.ics.icsrest.domain.dto.input.ProcesoInput;
import cl.cardif.ics.icsrest.domain.dto.output.ProcesoOutput;
import cl.cardif.ics.icsrest.domain.pojo.CallCenter;
import cl.cardif.ics.icsrest.domain.pojo.DirectorioSalida;
import cl.cardif.ics.icsrest.domain.pojo.Plantilla;
import cl.cardif.ics.icsrest.domain.pojo.TipoArchivo;
import cl.cardif.ics.icsrest.exceptions.icsproceso.IcsProcesoNotFoundException;
import cl.cardif.ics.icsrest.exceptions.icsproceso.IcsProcesoServiceException;
import cl.cardif.ics.icsrest.service.PlantillasService;
import cl.cardif.ics.icsrest.service.ProcesoService;
import cl.cardif.ics.icsrest.util.Constants;
import cl.cardif.ics.icsrest.util.OutputListWrapper;

@RestController
@RequestMapping("/api/procesos")
public class ProcesoController {
	private static final Logger LOG = LoggerFactory.getLogger(ProcesoController.class);

	private static final String MSJ_PROCESO_NO_ENCONTRADO = "Proceso no encontrado";
	private static final String MSJ_LOG_METODO_OBTENER_PROCESOS = "obtenerPagina";

	@Autowired ProcesoService procesoService;
	@Autowired PlantillasService plantillaService;

	@GetMapping("/activar/{id}")
	public ResponseEntity<String> activarProceso(@PathVariable long id) {
		LOG.info("activarProceso");
		try {
			procesoService.activarProceso(id);
			return ResponseEntity.ok().build();
		} catch (IcsProcesoNotFoundException e) {
			LOG.error(MSJ_PROCESO_NO_ENCONTRADO, e);
			return ResponseEntity.notFound().build();
		} catch (IcsProcesoServiceException e) {
			LOG.error(MSJ_PROCESO_NO_ENCONTRADO, e);
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping("/actualizar")
	public ResponseEntity<String> actualizarProceso(@RequestBody ProcesoInput proceso) {
		LOG.info("actualizarProceso");
		try {
			procesoService.almacenarProceso(proceso, true);
			return new ResponseEntity<>(HttpStatus.CREATED);
		} catch (Exception e) {
			LOG.error("Error al actualizar proceso", e);
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping("/nueva")
	public ResponseEntity<String> almacenarProceso(@RequestBody ProcesoInput proceso) {
		LOG.info("almacenarProceso");

		try {
			procesoService.almacenarProceso(proceso, false);
			return new ResponseEntity<>(HttpStatus.CREATED);
		} catch (IcsProcesoServiceException e) {
			LOG.error("Proceso con errores", e);
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (IcsProcesoNotFoundException e) {
			LOG.error(MSJ_PROCESO_NO_ENCONTRADO, e);
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/desactivar/{id}")
	public ResponseEntity<Void> desactivarProceso(@PathVariable long id) {
		LOG.info("desactivarProceso");
		try {
			procesoService.desactivarProceso(id);
			return new ResponseEntity<>(HttpStatus.CREATED);
		} catch (IcsProcesoNotFoundException e) {
			LOG.error(MSJ_PROCESO_NO_ENCONTRADO, e);
			return ResponseEntity.notFound().build();
		}

	}

	@GetMapping("/get/{id}")
	public ProcesoOutput getProceso(@PathVariable long id) {
		LOG.info("getProceso");
		return procesoService.getProceso(id);
	}

	@GetMapping("/")
	public OutputListWrapper<ProcesoOutput> getProcesos() {
		LOG.info(MSJ_LOG_METODO_OBTENER_PROCESOS);
		return procesoService.getProcesos(Constants.DEFAULT_PAGE, Constants.DEFAULT_PAGE_SIZE, "", "", null);
	}

	@GetMapping("/page/{page}")
	public OutputListWrapper<ProcesoOutput> getProcesos(@PathVariable int page) {
		LOG.info(MSJ_LOG_METODO_OBTENER_PROCESOS);
		return procesoService.getProcesos(page, Constants.DEFAULT_PAGE_SIZE, "", "", null);
	}

	@GetMapping("/page/{page}/size/{size}")
	public OutputListWrapper<ProcesoOutput> getProcesos(@PathVariable int page, @PathVariable int size) {
		LOG.info(MSJ_LOG_METODO_OBTENER_PROCESOS);
		return procesoService.getProcesos(page, size, "", "", null);
	}

	@GetMapping("/page/{page}/size/{size}/filters")
	public OutputListWrapper<ProcesoOutput> getProcesos(
		@PathVariable int page, @PathVariable int size, @RequestParam Map<String, String> filters) {
		LOG.info(MSJ_LOG_METODO_OBTENER_PROCESOS);
		return procesoService.getProcesos(page, size, "", "", filters);
	}

	@GetMapping("/page/{page}/size/{size}/sort/{sortColumn}/{sortOrder}/filters")
	public OutputListWrapper<ProcesoOutput> getProcesos(
		@PathVariable int page, @PathVariable int size, @PathVariable String sortColumn, @PathVariable String sortOrder,
		@RequestParam Map<String, String> filters) {
		LOG.info(MSJ_LOG_METODO_OBTENER_PROCESOS);
		return procesoService.getProcesos(page, size, sortColumn, sortOrder, filters);
	}

	@GetMapping("/filters")
	public OutputListWrapper<ProcesoOutput> getProcesos(@RequestParam Map<String, String> filters) {
		LOG.info(MSJ_LOG_METODO_OBTENER_PROCESOS);
		return procesoService.getProcesos(Constants.DEFAULT_PAGE, Constants.DEFAULT_PAGE_SIZE, "", "", filters);
	}

	@GetMapping("/getVigentes")
	public ResponseEntity<List<ProcesoOutput>> getVigentes(@RequestParam(name = "idSocio", required = false) Long idSocio) {
		LOG.info("getVigentes");
		List<ProcesoOutput> entities = new ArrayList<>();
		try {
			entities = procesoService.getAllVigentes(idSocio);
			return new ResponseEntity<>(entities, HttpStatus.CREATED);
		} catch (IcsProcesoServiceException e) {
			LOG.error("Error al obtener procesos vigentes", e);
			return new ResponseEntity<>(entities, HttpStatus.OK);
		}
	}

	@GetMapping("/nombresAll")
	public ResponseEntity<List<String>> getProcesosNombre() {
		LOG.info("getProcesosNombre");
		try {
			List<String> entities = procesoService.getNombresAll();
			return new ResponseEntity<>(entities, HttpStatus.OK);
		} catch (Exception e) {
			LOG.error("Error al obtener procesos", e);
			return ResponseEntity.notFound().build();
		}
	}

	@GetMapping("/plantillas")
	public OutputListWrapper<Plantilla> obtenerPlantillasParaCombos(
		@RequestParam("idSocio") Long idSocio, @RequestParam("idEvento") Long idEvento) {
		LOG.info("obtenerPlantillasParaCombos");
		return plantillaService.obtenerPlantillasPojo(idSocio, idEvento);
	}

	@GetMapping("/plantillas_salida_estructurada")
	public OutputListWrapper<Plantilla> obtenerPlantillasSalidaEstructuradaParaCombo(
		@RequestParam("idSocio") Long idSocio, @RequestParam("idEvento") Long idEvento) {
		LOG.info("obtenerPlantillasSalidaEstructuradaParaCombo");
		return plantillaService.obtenerPlantillasSalidaEstructuradaPojo(idSocio, idEvento);
	}

	@GetMapping("/call_centers")
	public List<CallCenter> obtenerCallCenters(@RequestParam("idSocio") Long idSocio, @RequestParam("idEvento") Long idEvento) {
		LOG.info("obtenerCallCenters");
		return procesoService.getCallCenters(idSocio, idEvento);
	}

	@GetMapping("/tipos_archivo")
	public List<TipoArchivo> obtenerTiposArchivo() {
		LOG.info("obtenerTiposArchivo");
		return procesoService.getTiposArchivo();
	}

	@GetMapping("/directorios_error")
	public List<DirectorioSalida> obtenerDirectoriosError() {
		LOG.info("obtenerDirectoriosError");
		return procesoService.getDirectoriosError();
	}

}
