package cl.cardif.ics.icsrest.domain.dto.input;

public class UnionArchivosInputModel {
	private int indiceArchivo1;
	private int idAtributoUsuario;
	private int indiceArchivo2;
	private int indiceColumnaArchivo1;
	private int indiceColumnaArchivo2;
    
	public UnionArchivosInputModel() {
		super();
	}

	public int getIndiceArchivo1() {
		return indiceArchivo1;
	}

	public void setIndiceArchivo1(int indiceArchivo1) {
		this.indiceArchivo1 = indiceArchivo1;
	}

	public int getIdAtributoUsuario() {
		return idAtributoUsuario;
	}

	public void setIdAtributoUsuario(int idAtributoUsuario) {
		this.idAtributoUsuario = idAtributoUsuario;
	}

	public int getIndiceArchivo2() {
		return indiceArchivo2;
	}

	public void setIndiceArchivo2(int indiceArchivo2) {
		this.indiceArchivo2 = indiceArchivo2;
	}

	public int getIndiceColumnaArchivo1() {
		return indiceColumnaArchivo1;
	}

	public void setIndiceColumnaArchivo1(int indiceColumnaArchivo1) {
		this.indiceColumnaArchivo1 = indiceColumnaArchivo1;
	}

	public int getIndiceColumnaArchivo2() {
		return indiceColumnaArchivo2;
	}

	public void setIndiceColumnaArchivo2(int indiceColumnaArchivo2) {
		this.indiceColumnaArchivo2 = indiceColumnaArchivo2;
	}
    
}
