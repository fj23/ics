package cl.cardif.ics.icsrest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

import cl.cardif.ics.domain.entity.estructuras.IcsEstructurasAgrupada;

@Repository
public interface EstructurasAgrupadasRepository
    extends JpaRepository<IcsEstructurasAgrupada, Long>,
        QueryDslPredicateExecutor<IcsEstructurasAgrupada> {
	
}
