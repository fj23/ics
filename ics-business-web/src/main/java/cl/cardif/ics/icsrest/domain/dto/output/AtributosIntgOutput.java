package cl.cardif.ics.icsrest.domain.dto.output;

import java.math.BigDecimal;

public class AtributosIntgOutput {

	private long idAtributoIntegracion;
	private String flgObligatorio;
	private BigDecimal idAtributoEnriquecimiento;
	private BigDecimal idAtributoNegocio;
	private BigDecimal idDeFormato;
	private BigDecimal idDeTipoHomologacion;
	private BigDecimal idIntegracionEvento;
	private BigDecimal idPlantilla;
	private BigDecimal idTipoFormula;

	public AtributosIntgOutput() {
		  super();
	}

	public String getFlgObligatorio() {
		return this.flgObligatorio;
	}

	public void setFlgObligatorio(String flgObligatorio) {
		this.flgObligatorio = flgObligatorio;
	}

	public BigDecimal getIdAtributoEnriquecimiento() {
		return this.idAtributoEnriquecimiento;
	}

	public void setIdAtributoEnriquecimiento(BigDecimal idAtributoEnriquecimiento) {
		this.idAtributoEnriquecimiento = idAtributoEnriquecimiento;
	}

	public long getIdAtributoIntegracion() {
		return this.idAtributoIntegracion;
	}

	public void setIdAtributoIntegracion(long idAtributoIntegracion) {
		this.idAtributoIntegracion = idAtributoIntegracion;
	}

	public BigDecimal getIdAtributoNegocio() {
		return this.idAtributoNegocio;
	}

	public void setIdAtributoNegocio(BigDecimal idAtributoNegocio) {
		this.idAtributoNegocio = idAtributoNegocio;
	}

	public BigDecimal getIdDeFormato() {
		return this.idDeFormato;
	}

	public void setIdDeFormato(BigDecimal idDeFormato) {
		this.idDeFormato = idDeFormato;
	}

	public BigDecimal getIdDeTipoHomologacion() {
		return this.idDeTipoHomologacion;
	}

	public void setIdDeTipoHomologacion(BigDecimal idDeTipoHomologacion) {
		this.idDeTipoHomologacion = idDeTipoHomologacion;
	}

	public BigDecimal getIdIntegracionEvento() {
		return this.idIntegracionEvento;
	}

	public void setIdIntegracionEvento(BigDecimal idIntegracionEvento) {
		this.idIntegracionEvento = idIntegracionEvento;
	}

	public BigDecimal getIdPlantilla() {
		return this.idPlantilla;
	}

	public void setIdPlantilla(BigDecimal idPlantilla) {
		this.idPlantilla = idPlantilla;
	}

	public BigDecimal getIdTipoFormula() {
		return this.idTipoFormula;
	}

	public void setIdTipoFormula(BigDecimal idTipoFormula) {
		this.idTipoFormula = idTipoFormula;
	}
}
