package cl.cardif.ics.icsrest.util.converters.toentity;

import cl.cardif.ics.domain.entity.common.IcsTipoDato;
import cl.cardif.ics.domain.entity.common.QIcsTipoDato;
import cl.cardif.ics.domain.entity.estructuras.IcsDetalleEstructura;
import cl.cardif.ics.icsrest.domain.pojo.DetalleEstructura;
import cl.cardif.ics.icsrest.domain.pojo.TipoDato;
import cl.cardif.ics.icsrest.repository.SubestructurasRepository;
import cl.cardif.ics.icsrest.repository.TiposDatosRepository;
import com.querydsl.core.BooleanBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import java.math.BigDecimal;

@Component
public class IcsDetalleEstructuraConverter
    implements Converter<DetalleEstructura, IcsDetalleEstructura> {

  static final Logger LOG = LoggerFactory.getLogger(IcsDetalleEstructuraConverter.class);

  @Autowired TiposDatosRepository tipoRepository;

  @Autowired SubestructurasRepository subestructurasRepository;

  @Override
  public IcsDetalleEstructura convert(DetalleEstructura pojo) {

    IcsDetalleEstructura detalleEntity = new IcsDetalleEstructura();

    LOG.info("Detalle obtenido con codigo: " + pojo.getCodigo());
    detalleEntity.setCodigoEstructura(pojo.getCodigo());
    LOG.info("ID SUBESTRUCTURA EN COLUMNA ESTRUCTURA : " + pojo.getIdSubEstructura());

    detalleEntity.setLargo(BigDecimal.valueOf(pojo.getLargo()));
    detalleEntity.setOrden(BigDecimal.valueOf(pojo.getOrden()));

    TipoDato tipoDato = pojo.getTipoDato();
    long idTipo = tipoDato.getId();
    LOG.info("Tipo de dato a consultar en DB : " + idTipo);

    BooleanBuilder booleanBuilder = new BooleanBuilder();
    QIcsTipoDato qIcsTipoDato = QIcsTipoDato.icsTipoDato;

    booleanBuilder.and(qIcsTipoDato.idTipoDato.eq(idTipo));

    IcsTipoDato tipoEntity = null;
    try {
      tipoEntity = this.tipoRepository.findOne(booleanBuilder);
    } catch (EntityNotFoundException e) {
      throw e;
    }

    LOG.info("tipoObtenido desde DB : " + tipoEntity.getIdTipoDato());

    detalleEntity.setIcsTipoDato(tipoEntity);
    detalleEntity.setTipoDato("");
    detalleEntity.setVigenciaDetalleEstructura(pojo.getVigencia());
    return detalleEntity;
  }
}
