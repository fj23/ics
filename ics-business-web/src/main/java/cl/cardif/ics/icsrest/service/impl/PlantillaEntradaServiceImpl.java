package cl.cardif.ics.icsrest.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.OrderSpecifier;

import cl.cardif.ics.domain.entity.common.IcsTipoDato;
import cl.cardif.ics.domain.entity.common.IcsTipoExtension;
import cl.cardif.ics.domain.entity.common.QIcsTipoDato;
import cl.cardif.ics.domain.entity.estructuras.IcsDetalleEstructura;
import cl.cardif.ics.domain.entity.estructuras.IcsEstructura;
import cl.cardif.ics.domain.entity.estructuras.IcsEstructurasAgrupada;
import cl.cardif.ics.domain.entity.estructuras.IcsSubestructura;
import cl.cardif.ics.domain.entity.estructuras.QIcsDetalleEstructura;
import cl.cardif.ics.domain.entity.estructuras.QIcsEstructura;
import cl.cardif.ics.domain.entity.estructuras.QIcsEstructurasAgrupada;
import cl.cardif.ics.domain.entity.plantillas.IcsArchivo;
import cl.cardif.ics.domain.entity.plantillas.IcsAtributoUsuario;
import cl.cardif.ics.domain.entity.plantillas.IcsAtributos;
import cl.cardif.ics.domain.entity.plantillas.IcsAtributosSocio;
import cl.cardif.ics.domain.entity.plantillas.IcsFiltrosMultiArchivo;
import cl.cardif.ics.domain.entity.plantillas.IcsPlantilla;
import cl.cardif.ics.domain.entity.plantillas.QIcsArchivo;
import cl.cardif.ics.domain.entity.plantillas.QIcsAtributoUsuario;
import cl.cardif.ics.domain.entity.plantillas.QIcsAtributos;
import cl.cardif.ics.domain.entity.plantillas.QIcsAtributosSocio;
import cl.cardif.ics.domain.entity.plantillas.QIcsFiltrosMultiArchivo;
import cl.cardif.ics.domain.entity.plantillas.QIcsPlantilla;
import cl.cardif.ics.icsrest.domain.dto.input.ArchivoEntradaInput;
import cl.cardif.ics.icsrest.domain.dto.input.ColumnaMetadatosInput;
import cl.cardif.ics.icsrest.domain.dto.input.ColumnaMetadatosSalidaModel;
import cl.cardif.ics.icsrest.domain.dto.input.MapeoEntradaInput;
import cl.cardif.ics.icsrest.domain.dto.input.PlantillaEntradaInput;
import cl.cardif.ics.icsrest.domain.dto.input.UnionArchivosInputModel;
import cl.cardif.ics.icsrest.domain.dto.output.AtributoUsuarioOutput;
import cl.cardif.ics.icsrest.domain.dto.output.DetalleEstructuraOutput;
import cl.cardif.ics.icsrest.domain.dto.output.EstructuraOutput;
import cl.cardif.ics.icsrest.domain.dto.output.PlantillaEntradaOutput;
import cl.cardif.ics.icsrest.domain.dto.output.PlantillaOutput;
import cl.cardif.ics.icsrest.domain.dto.output.RequestResultOutput;
import cl.cardif.ics.icsrest.domain.dto.output.SubEstructuraOutput;
import cl.cardif.ics.icsrest.domain.pojo.Archivo;
import cl.cardif.ics.icsrest.domain.pojo.TipoDato;
import cl.cardif.ics.icsrest.domain.pojo.TipoExtension;
import cl.cardif.ics.icsrest.exceptions.icsplantilla.IcsPlantillaServiceException;
import cl.cardif.ics.icsrest.repository.ArchivosRepository;
import cl.cardif.ics.icsrest.repository.AtributosPlantillaRepository;
import cl.cardif.ics.icsrest.repository.AtributosSocioRepository;
import cl.cardif.ics.icsrest.repository.AtributosUsuarioRepository;
import cl.cardif.ics.icsrest.repository.DetallesEstructurasRepository;
import cl.cardif.ics.icsrest.repository.EstructurasAgrupadasRepository;
import cl.cardif.ics.icsrest.repository.EstructurasRepository;
import cl.cardif.ics.icsrest.repository.FiltrosMultiArchivoRepository;
import cl.cardif.ics.icsrest.repository.PlantillaRepository;
import cl.cardif.ics.icsrest.repository.TiposDatosRepository;
import cl.cardif.ics.icsrest.service.PlantillaEntradaService;
import cl.cardif.ics.icsrest.service.PlantillasService;
import cl.cardif.ics.icsrest.util.Constants;
import cl.cardif.ics.icsrest.util.PlantillaUtil;
import cl.cardif.ics.icsrest.util.converters.topojo.AtributoUsuarioFromAtributoNegocioConverter;

@Service
public class PlantillaEntradaServiceImpl implements PlantillaEntradaService {
	private static final Logger LOG = LoggerFactory.getLogger(PlantillaEntradaServiceImpl.class);
	
	private static final PlantillaUtil utilities = new PlantillaUtil();

	@Autowired private ConversionService conversionService;
	@Autowired private PlantillasService ptlSvc;
	
	@Autowired private TiposDatosRepository tpDatoRepo;
	@Autowired private ArchivosRepository archivoRepo;
	@Autowired private AtributosPlantillaRepository attrArchivoRepo;
	@Autowired private AtributosSocioRepository attrSocioRepo;
	@Autowired private AtributosUsuarioRepository attrUsuarioRepo;
	@Autowired private EstructurasAgrupadasRepository estructAgrupadasRepo;
	@Autowired private EstructurasRepository estructRepo;
	@Autowired private DetallesEstructurasRepository dtlEstructRepo;
	@Autowired private FiltrosMultiArchivoRepository filtrosMultiArchivoRepo;
	@Autowired private PlantillaRepository plantillaRepo;
	

	private Long obtenerIdAtributoNegocioFromIdAtributousuario(Long idAtributousuario) {
		
		IcsAtributoUsuario atrUsuario = attrUsuarioRepo.findOne(idAtributousuario);
		
		if (atrUsuario != null) {
			return atrUsuario.getIdAtributosNegocio();
		} else {
			return null;
		}
	}

	private String generarStringMapeoEstandar(MapeoEntradaInput mapeo) {
		LOG.info("generarStringMapeoEstandar");

		String columnaDestino = null; 
		String idAtributoUsuario = null;
		String grupoDestino = null;
		String esObligatorio = null;
		String valorParaFiltrar = null;
		String filtroExcluyente = null;
		String encriptarDato = null;

		String filtro = mapeo.getValorParaFiltrar();
		Long idAtrUsuario = mapeo.getIdAtributoUsuario();
		Long idAtrNegocio = this.obtenerIdAtributoNegocioFromIdAtributousuario(idAtrUsuario);
		Boolean filtroExcl = mapeo.isFiltroExclusion();

		idAtributoUsuario = idAtrUsuario.toString();
		columnaDestino = idAtrNegocio != null? idAtrNegocio.toString() : null;
		grupoDestino = mapeo.getGrupoColumnaDestino().toString();
		esObligatorio = (mapeo.getEsObligatorio() ? "Y" : "N");

		if (filtro != null) {
			valorParaFiltrar = filtro;
			if (filtroExcl != null) {
				filtroExcluyente = filtroExcl? "Y" : "N";
			} else {
				filtroExcluyente = "N";
			}
		} else {
			valorParaFiltrar = "null";
			filtroExcluyente = "null";
		}
		
		encriptarDato = mapeo.getEncriptar()? "Y" : "N";

		return new StringBuilder()
			.append(columnaDestino).append(",")
			.append(idAtributoUsuario).append(",")
			.append(grupoDestino).append(",")
			.append(esObligatorio).append(",")
			.append(valorParaFiltrar).append(",")
			.append(filtroExcluyente).append(",")
			.append(encriptarDato)
			.toString();
	}

	private String generarStringMapeosEstandar(List<MapeoEntradaInput> mapeos) {
		StringBuilder sb = new StringBuilder();
		for (MapeoEntradaInput mapeo : mapeos) {
			 String datosMapeo = this.generarStringMapeoEstandar(mapeo);
			 sb = sb.append(datosMapeo).append("|");
		}
		return sb.deleteCharAt(sb.lastIndexOf("|")).toString();
	}

	/**
	 * Obtiene el mapeo de un archivo no estructurado, a partir de un id
	 *
	 * @param mapeosArchivo Una lista de mapeos
	 * @param idColumnaArchivo    El id a buscar
	 * @return El objeto 'mapeo' encontrado por el id, o null
	 * @see cl.cardif.ics.icsrest.domain.dto.input.MapeoEntradaInput
	 */
	private List<MapeoEntradaInput> filtrarMapeosPorIdColumnaArchivo(
		List<MapeoEntradaInput> mapeosArchivo, 
		Long idColumnaArchivo
	) {
		LOG.info("filtrarMapeosPorIdColumnaArchivo");
		
		List<MapeoEntradaInput> mapeosColumna = new ArrayList<>();
		for (MapeoEntradaInput mapeo : mapeosArchivo) {
			if (mapeo.getColumnaOrigen().equals(idColumnaArchivo)) {
				mapeosColumna.add(mapeo);
			}
		}
		return mapeosColumna;
	}

	/** Genera una lista de strings con los datos de las columnas del archivo, incluyendo mapeos.
	 * Esta lista será introducida como parte de los metadatos de la tabla ics_arreglo_util
	 * 
	 * @param columnasArchivo         Desde donde se obtienen o estructuras o columnas
	 * @param mapeosArchivo Lista de mapeos de la plantilla
	 * @return Lista de String (que representa un arreglo de arreglos), delimitado
	 *         por ';'
	 */
	private List<String> generarListaStringColumnasArchivo(
		List<ColumnaMetadatosInput> columnasArchivo, 
		List<MapeoEntradaInput> mapeosArchivo
	) {
		LOG.info("generarListaStringColumnasArchivo");

		List<String> datosColumnas = new ArrayList<>();

		// Crear un String por cada COLUMNA mapeada
		for (ColumnaMetadatosInput columnaTmp : columnasArchivo) {

			Long idColumna = columnaTmp.getId();
			if (idColumna == null || idColumna == 0) {
				idColumna = columnaTmp.getOrden();
			}

			StringBuilder sb = new StringBuilder()
				.append(columnaTmp.getDescripcion()).append(";")
				.append(columnaTmp.getOrden()).append(";")
				.append(columnaTmp.getPosicionInicial()).append(";")
				.append(columnaTmp.getPosicionFinal()).append(";")
				.append(columnaTmp.getTipoDato()).append(";");

			// buscar mapeos para esta columna y asignar datos
			List<MapeoEntradaInput> mapeosColumna = this.filtrarMapeosPorIdColumnaArchivo(mapeosArchivo, idColumna);
			if (!mapeosColumna.isEmpty()) {
				String mapeosString = this.generarStringMapeosEstandar(mapeosColumna);
				sb.append(mapeosString);
			} else {
				sb.append("0");
			}
			datosColumnas.add(sb.toString());
		}

		return datosColumnas;
	}

	private String generarStringMapeoEstructura(MapeoEntradaInput mapeo) {
		LOG.info("generarStringMapeoEstructura");
		
		String columnaDestino = null; 
		String columnaOrigen = null;
		String subEstrucDetalleOrigenId = null;
		String idAtributoUsuario = null;
		String grupoDestino = null;
		String esObligatorio = null;
		String valorParaFiltrar = null;
		String filtroExcluyente = null;
		String encriptarDato = null;
		
		String filtro = mapeo.getValorParaFiltrar();
		Long subEstructuraColumnaOrigenId = mapeo.getSubEstructuraColumnaOrigen();
		Long subEstrucDetalleOrigenIdL = (subEstructuraColumnaOrigenId != null? subEstructuraColumnaOrigenId : 0L);

		Long idAtributoUsuarioL = mapeo.getIdAtributoUsuario();
		if (idAtributoUsuarioL != null) {
			IcsAtributoUsuario icsAtributoUsuario = attrUsuarioRepo.findOne(idAtributoUsuarioL);
			Long idAtributoNegocio = icsAtributoUsuario.getIdAtributosNegocio();
			columnaDestino = utilities.safeLongToStringParser(idAtributoNegocio);
		} else {
			throw new IcsPlantillaServiceException("El mapeo no posee columna de origen");
		}
		
		columnaOrigen = utilities.safeLongToStringParser(mapeo.getColumnaOrigen());
		subEstrucDetalleOrigenId = utilities.safeLongToStringParser(subEstrucDetalleOrigenIdL);
		idAtributoUsuario = utilities.safeLongToStringParser(mapeo.getIdAtributoUsuario());
		grupoDestino = utilities.safeLongToStringParser(mapeo.getGrupoColumnaDestino());
		esObligatorio = (mapeo.getEsObligatorio() ? "Y" : "N");
		
		
		if (filtro != null) {
			valorParaFiltrar = filtro;
			filtroExcluyente = mapeo.isFiltroExclusion()? "Y" : "N";
		} else {
			valorParaFiltrar = "null";
			filtroExcluyente = "null";
		}
		
		encriptarDato = (mapeo.getEncriptar() ? "Y" : "N");

		
		return new StringBuilder()
			.append(columnaDestino).append(";")
			.append(columnaOrigen).append(";")
			.append(subEstrucDetalleOrigenId).append(";")
			.append(idAtributoUsuario).append(";")
			.append(grupoDestino).append(";")
			.append(esObligatorio).append(";")
			.append(valorParaFiltrar).append(";")
			.append(filtroExcluyente).append(";")
			.append(encriptarDato)
			.toString();
	}

	/**
	 * Para un mapeo de archivo estructurado: Busca el id de estructura de origen
	 * dentro de la lista (de los parametros)
	 *
	 * @param lista
	 * @param idEstructura
	 * @return mapeo en forma de MapeoColumnaDestinoInput
	 * @see cl.cardif.ics.icsrest.domain.dto.input.MapeoEntradaInput
	 */
	private List<MapeoEntradaInput> filtrarMapeosParaIdEstructura(List<MapeoEntradaInput> lista, int idEstructura) {
		LOG.info("filtrarMapeosParaIdEstructura");
		
		List<MapeoEntradaInput> listMap = new ArrayList<>();
		for (MapeoEntradaInput mappedColumn : lista) {
			if (mappedColumn.getEstructuraOrigenId() == idEstructura) {
				listMap.add(mappedColumn);
			}
		}
		return listMap;
	}

	/**
	 * Genera string de mapeos para un archivo de entrada estructurada.
	 * @param estructuras         Desde donde se obtienen o estructuras o columnas
	 * @param mapeosInput Lista de mapeos de la plantilla
	 * @return Lista de String (que representa un arreglo de arreglos), delimitado
	 *         por ';'
	 */
	private List<String> generarListaStringMapeosEstructuras(
			List<Integer> estructuras, 
			List<MapeoEntradaInput> mapeosInput
	) {
		LOG.info("generarListaStringMapeosEstructuras");
		
		List<String> salida = new ArrayList<>();

		// Crear un String por cada ESTRUCTURA mapeada
		for (Integer estructuraId : estructuras) {
			List<MapeoEntradaInput> mapeos = this.filtrarMapeosParaIdEstructura(mapeosInput, estructuraId);
			
			for (MapeoEntradaInput mapeo : mapeos) {
				String result = this.generarStringMapeoEstructura(mapeo);
				salida.add(result);
			}
		}
		
		return salida;
	}

	private String unionesToString(List<UnionArchivosInputModel> uniones) {
		if (uniones != null && !uniones.isEmpty()) {
			
			StringBuilder sb = new StringBuilder();
			for (UnionArchivosInputModel union : uniones) {
				
				long idAtributoUsuario = union.getIdAtributoUsuario();
				IcsAtributoUsuario icsAtributoUsuario = attrUsuarioRepo.findOne(idAtributoUsuario);
				long idAtributoNegocio = icsAtributoUsuario.getIdAtributosNegocio();
								
				sb
					.append(union.getIndiceArchivo1()).append(",")
					.append(idAtributoNegocio).append(",")
					.append(union.getIndiceArchivo2()).append(",")
					.append(idAtributoNegocio).append(";");
			}
			return sb.deleteCharAt(sb.lastIndexOf(";")).toString();
		}
		return null;
	}

	public RequestResultOutput callSpAlmacenarPlantilla(PlantillaEntradaInput plantilla, boolean noRealizarTransformaciones) {

		String nombre = plantilla.getNombre();
		String version = plantilla.getVersion();
		String tipo = utilities.safeLongToStringParser(plantilla.getTipo());
		String socio = utilities.safeLongToStringParser(plantilla.getSocio());
		String evento = utilities.safeLongToStringParser(plantilla.getEvento());
		String core = null;
		String observaciones = plantilla.getObservaciones();
		String tipoExtension = utilities.safeLongToStringParser(plantilla.getTipoExtension());
		String tipoSalida = null;
		String delimitador = null; // en multiarchivo pueden haber distintos delimitadores
		String codUsuario = plantilla.getCodUsuario();
		String uniones = this.unionesToString(plantilla.getUniones());
		String realizarTransformaciones = noRealizarTransformaciones? "1" : "0";
		
		Map<String, Object> map = new HashMap<>();
		
		map.put(Constants.PV_TEMPLATE_CREATION_NOMBRE, nombre);
		map.put(Constants.PN_TEMPLATE_CREATION_VERSION, version);
		map.put(Constants.PN_TEMPLATE_CREATION_IDTIPOPLANTILLA, tipo);
		map.put(Constants.PN_TEMPLATE_CREATION_IDSOCIO, socio);
		map.put(Constants.PN_TEMPLATE_CREATION_IDEVENTO, evento);
		map.put(Constants.PN_TEMPLATE_CREATION_IDCORE, core);
		map.put(Constants.PV_TEMPLATE_CREATION_OBSERVACIONES, observaciones);
		map.put(Constants.PN_TEMPLATE_CREATION_IDTIPOEXTENSION, tipoExtension);
		map.put(Constants.PN_TEMPLATE_CREATION_IDTIPOSALIDA, tipoSalida);
		map.put(Constants.PV_TEMPLATE_CREATION_DELIMITADOR, delimitador);
		map.put(Constants.PV_TEMPLATE_CREATION_USUARIO, codUsuario);
		map.put(Constants.PV_TEMPLATE_CREATION_UNIONARCHIVOS, uniones);
		map.put(Constants.PN_TEMPLATE_CREATION_IDVERSIONANTERIOR, realizarTransformaciones);
		map.put(Constants.PV_TEMPLATE_CREATION_DIRECTORIOSALIDA, null);

		try {
			return ptlSvc.llamarSPAlmacenarPlantilla(map);
		} catch (IcsPlantillaServiceException e) {
			throw new IcsPlantillaServiceException("Plantilla: " + e.getMessage(), e);
		}
	}

	private DetalleEstructuraOutput getColumnaOrigenMapeo(Long idtipoExtension, IcsAtributosSocio mapeo) {
		LOG.info("getColumnaOrigenMapeo");
		
		DetalleEstructuraOutput columnaOutput; 
		
		if (idtipoExtension == Constants.ID_TIPO_EXTENSION_ARCHIVO_ESTRUCTURADO) { 
			IcsDetalleEstructura columnaEstructura = mapeo.getIcsDetalleEstructura();
			columnaOutput = conversionService.convert(columnaEstructura, DetalleEstructuraOutput.class);
		} else {
			IcsAtributos atributoMapeo = mapeo.getIcsAtributos();
			columnaOutput = conversionService.convert(atributoMapeo, DetalleEstructuraOutput.class);
		}
		
		return columnaOutput;
	}

	private AtributoUsuarioOutput getAtributoUsuarioOutputFromMapeo(IcsAtributosSocio mapeo) {
		LOG.info("getAtributoUsuarioOutputFromMapeo");
		
		AtributoUsuarioOutput columnaDestinoEM = null;
		
		// si posee una columna de destino
		if (mapeo.getIcsAtributoUsuario() != null) {

			QIcsAtributoUsuario qIcsAtrUsu = QIcsAtributoUsuario.icsAtributoUsuario;
			
			Long idAtributoUsuario = mapeo.getIcsAtributoUsuario().getIdAtributoUsuario();

			BooleanBuilder bb = new BooleanBuilder()
					.and(qIcsAtrUsu.idAtributoUsuario.eq(idAtributoUsuario));
			
			try {
				IcsAtributoUsuario atributosEntity = this.attrUsuarioRepo.findOne(bb);
				columnaDestinoEM = conversionService.convert(atributosEntity, AtributoUsuarioOutput.class);
			} catch (EntityNotFoundException e) {
				LOG.error("IcsAtributoUsuario no encontrado", e);
			}

		} else if (mapeo.getIcsAtributoNegocio() != null) {
			AtributoUsuarioFromAtributoNegocioConverter convert = new AtributoUsuarioFromAtributoNegocioConverter();
			columnaDestinoEM = convert.convert(mapeo.getIcsAtributoNegocio());
		}
		return columnaDestinoEM;
	}

	private List<MapeoEntradaInput> getMapeosArchivo(
		Long idPlantilla, 
		Archivo archivo
	) {
		LOG.info("getMapeosArchivo");
		
		Long idArchivo = archivo.getIdArchivo();
		Long idtipoExtension = archivo.getTipoExtension().getId(); 

		QIcsAtributosSocio qIcsAtributosSocio = QIcsAtributosSocio.icsAtributosSocio;

		BooleanBuilder bb = new BooleanBuilder()
				.and(qIcsAtributosSocio.idArchivo.eq(idArchivo));
		
		Iterable<IcsAtributosSocio> iteraAtributosSocio = attrSocioRepo.findAll(bb);
		
		List<MapeoEntradaInput> mapeos = new ArrayList<>();
		for (IcsAtributosSocio icsAtributoSocio : iteraAtributosSocio) {
		
			MapeoEntradaInput mapeo = conversionService.convert(icsAtributoSocio, MapeoEntradaInput.class);
			
			Long idAtributoSocio = icsAtributoSocio.getIdAtributosSocio();
			Long tipoFiltroMultiarchivo = 1L;
			
			QIcsFiltrosMultiArchivo qIcsFiltro = QIcsFiltrosMultiArchivo.icsFiltrosMultiArchivo;
			bb = new BooleanBuilder()
					.and(qIcsFiltro.idPlantilla.eq(idPlantilla))
					.and(qIcsFiltro.idArchivo1.eq(idArchivo))
					
					.and(qIcsFiltro.idAtributoSocio.eq(idAtributoSocio))
					.and(qIcsFiltro.tipoFiltro.ne(tipoFiltroMultiarchivo));
			
			IcsFiltrosMultiArchivo icsFiltro = filtrosMultiArchivoRepo.findOne(bb);
			if (icsFiltro != null) {
				boolean esFiltroExcluyente = (icsFiltro.getTipoFiltro() == 3);
				mapeo.setValorParaFiltrar(icsFiltro.getConstante());
				mapeo.setFiltroExclusion(esFiltroExcluyente);
			}
			
			DetalleEstructuraOutput columnaOrigenDatosEM = this.getColumnaOrigenMapeo(idtipoExtension, icsAtributoSocio);
			mapeo.setColumnaOrigenDatosEM(columnaOrigenDatosEM);
			
			// atributo negocio/usuario = destino de plantilla entrada
			AtributoUsuarioOutput columnaDestinoEM = this.getAtributoUsuarioOutputFromMapeo(icsAtributoSocio);
			mapeo.setColumnaDestinoEM(columnaDestinoEM);

			// si tiene asociada una columna de estructura
			if (icsAtributoSocio.getIcsDetalleEstructura() != null) {
				IcsDetalleEstructura icsDtlEstr = icsAtributoSocio.getIcsDetalleEstructura();
				IcsEstructura icsEstr = icsDtlEstr.getIcsEstructura();
				EstructuraOutput estructuraOrigenEM = conversionService.convert(icsEstr, EstructuraOutput.class);
				mapeo.setEstructuraOrigenEM(estructuraOrigenEM);

				// si tiene una subestructura completa asociada
				if (icsDtlEstr.getIcsSubestructura() != null) {
					IcsSubestructura icsSubestr = icsAtributoSocio.getIcsDetalleEstructura().getIcsSubestructura();
					SubEstructuraOutput subEstructuraColumnaOrigenEM = conversionService.convert(icsSubestr, SubEstructuraOutput.class);
					mapeo.setSubEstructuraColumnaOrigenEM(subEstructuraColumnaOrigenEM);
				}
			}
			
			mapeos.add(mapeo);
		}
		
		return mapeos;
	}

	private List<Long> obtenerIdsEstructurasAgrupadas(long idGrupoEstructuras) {
		LOG.info("obtenerIdsEstructurasAgrupadas");
		
		BooleanBuilder bb = new BooleanBuilder()
				.and(QIcsEstructurasAgrupada.icsEstructurasAgrupada.idGrupoEstructuras.eq(idGrupoEstructuras));
		
		Iterable<IcsEstructurasAgrupada> iteraGrupoEstructuras = this.estructAgrupadasRepo.findAll(bb);

		List<Long> estructurasAgrupadasIds = new ArrayList<>();
		for (IcsEstructurasAgrupada e : iteraGrupoEstructuras) {
			estructurasAgrupadasIds.add(e.getIdEstructura());
		}
		return estructurasAgrupadasIds;
	}

	private EstructuraOutput hacerEstructuraDesdeIcsEstructura(IcsEstructura icsEstructura) {
		Long idEstructura = icsEstructura.getIdEstructura();
		
		// obtiene las columnas de la estructura
		BooleanBuilder bb = new BooleanBuilder()
				.and(QIcsDetalleEstructura.icsDetalleEstructura.icsEstructura.idEstructura.eq(idEstructura));
		Iterable<IcsDetalleEstructura> iteraDetalles = dtlEstructRepo.findAll(bb);

		List<DetalleEstructuraOutput> detalles = new ArrayList<>();
		for (IcsDetalleEstructura icsDetalle : iteraDetalles) {
			DetalleEstructuraOutput detalle = conversionService.convert(icsDetalle, DetalleEstructuraOutput.class);
			detalles.add(detalle);
		}

		EstructuraOutput estructura = conversionService.convert(icsEstructura, EstructuraOutput.class);
		estructura.setColumnas(detalles);
		return estructura;
	}
	
	private List<EstructuraOutput> getEstructurasDelGrupoDeArchivo(Long idGrupoEstructuras) {
		LOG.info("getEstructurasDelGrupoDeArchivo");

		// obtiene el grupo de ids de estructuras
		List<Long> estructurasAgrupadasIds = this.obtenerIdsEstructurasAgrupadas(idGrupoEstructuras);

		// obtiene las estructuras en si usando este grupo de ids
		BooleanBuilder bb = new BooleanBuilder()
				.and(QIcsEstructura.icsEstructura.idEstructura.in(estructurasAgrupadasIds));
		Iterable<IcsEstructura> iteraEstructuras = estructRepo.findAll(bb);

		List<EstructuraOutput> estructurasOutput = new ArrayList<>();
		for (IcsEstructura icsEstructura : iteraEstructuras) {
			EstructuraOutput estructura = this.hacerEstructuraDesdeIcsEstructura(icsEstructura);
			estructurasOutput.add(estructura);
		}

		return estructurasOutput;
	}

	@Override
	public List<TipoDato> getColumnDataTypes() {
		LOG.info("getColumnDataTypes");

		QIcsTipoDato qIcsTipoDato = QIcsTipoDato.icsTipoDato;

		BooleanBuilder bb = new BooleanBuilder()
					.and(qIcsTipoDato.vigencia.eq("Y"))
					.and(qIcsTipoDato.idTipoDato.notIn(100005L));

		Iterable<IcsTipoDato> iterableTiposDato = this.tpDatoRepo.findAll(bb);
		List<TipoDato> listaDatos = new ArrayList<>();
		for (IcsTipoDato icsTipoDato : iterableTiposDato) {
			TipoDato tipoDato = conversionService.convert(icsTipoDato, TipoDato.class);
			listaDatos.add(tipoDato);
		}
		
		return listaDatos;
	}

	private Archivo hacerArchivoDesdeEntity(IcsArchivo icsArchivo) {
		LOG.info("hacerArchivoDesdeEntity");
		
		Archivo archivo = conversionService.convert(icsArchivo, Archivo.class);
		
		IcsTipoExtension icsTipoExtension = icsArchivo.getIcsTipoExtension();
		
		if (icsTipoExtension != null) {
			
			TipoExtension tipoExtension = conversionService.convert(icsTipoExtension, TipoExtension.class);
			archivo.setTipoExtension(tipoExtension);

			if (icsTipoExtension.getIdExtension() == Constants.ID_TIPO_EXTENSION_ARCHIVO_ESTRUCTURADO) {
	
				Long idGrupoEstructuras = icsArchivo.getIdGrupoEstructuras();
						
				if (idGrupoEstructuras != null) {
					List<EstructuraOutput> estructurasOutput = this.getEstructurasDelGrupoDeArchivo(idGrupoEstructuras);
					archivo.setEstructurasOutput(estructurasOutput);
				} else {
					archivo.setEstructurasOutput(new ArrayList<EstructuraOutput>());
				}
			} else {
				Long idArchivo = icsArchivo.getIdArchivo();
				
				BooleanBuilder bb = new BooleanBuilder()
						.and(QIcsAtributos.icsAtributos.idArchivo.eq(idArchivo));
				OrderSpecifier<Long> ordenAscendente = QIcsAtributos.icsAtributos.numeroColumna.asc();
				
				Iterable<IcsAtributos> iteraAtributos = attrArchivoRepo.findAll(bb, ordenAscendente);
				
				List<ColumnaMetadatosSalidaModel> atributos = new ArrayList<>();
				for (IcsAtributos icsAtributo : iteraAtributos) {
					ColumnaMetadatosSalidaModel atributo = conversionService.convert(icsAtributo, ColumnaMetadatosSalidaModel.class);
					atributos.add(atributo);
				}
				archivo.setColumnas(atributos);
			}
		}
		
		return archivo;
	}

	/**
	 * Obtiene los archivos, con sus columnas/estructuras) correspondientes a la plantilla de entrada, y los carga en ésta.
	 *
	 * @param templateDTO
	 * @param entity
	 */
	private List<Archivo> obtenerArchivosPlantilla(Long idPlantilla) {
		LOG.info("loadArchivosIntoPlantillaEntrada");

		BooleanBuilder bb = new BooleanBuilder()
				.and(QIcsArchivo.icsArchivo.idPlantilla.eq(idPlantilla));
		OrderSpecifier<Long> sortOrder = QIcsArchivo.icsArchivo.idArchivo.asc();
		
		Iterable<IcsArchivo> iteraArchivos = archivoRepo.findAll(bb, sortOrder);
		
		List<Archivo> archivos = new ArrayList<>();
		for (IcsArchivo icsArchivo : iteraArchivos) {
			Archivo archivo = this.hacerArchivoDesdeEntity(icsArchivo);
			archivos.add(archivo);
		}
		
		return archivos;
	}
	
	@Override
	public PlantillaOutput viewTemplateDetalle(PlantillaOutput plantilla) {
		LOG.info("Obteniendo detalle plantilla entrada");
		Long idPlantilla = plantilla.getId();
		
		PlantillaEntradaOutput plantillaEntrada = conversionService.convert(plantilla, PlantillaEntradaOutput.class);

		List<Archivo> archivos = this.obtenerArchivosPlantilla(idPlantilla);
		plantillaEntrada.setArchivos(archivos);

		List<MapeoEntradaInput> mapeos = new ArrayList<>();
		for (Archivo archivo : archivos) {
			List<MapeoEntradaInput> mapeosArchivo = this.getMapeosArchivo(idPlantilla, archivo);
			mapeos.addAll(mapeosArchivo);
		}
		
		plantillaEntrada.setMapeosEntrada(mapeos);

		return plantillaEntrada;
	}
	
	private List<MapeoEntradaInput> filtrarMapeosPorIdArchivo(
			List<MapeoEntradaInput> mapeos,
			Long idArchivo
	) {
		LOG.info("filtrarMapeosPorIdArchivo");
		
		List<MapeoEntradaInput> mapeosFiltrados = new ArrayList<>();
		for (MapeoEntradaInput mapeo : mapeos) {
			if (mapeo.getArchivoOrigen().equals(idArchivo)) {
				mapeosFiltrados.add(mapeo);
			}
		}
		
		return mapeosFiltrados;
	}

	/**
	 * Genera un mapa de strings conteniendo los siguientes datos del archivo:
	 * nombre, delimitador, saltarRegistrosIniciales, saltarRegistroFinales
	 * @param archivo
	 * @param esEstructurado
	 * @param mapeosArchivo
	 * @return
	 */
	private Map<Integer, String> generarMapaStringDatosArchivoEntrada(
			ArchivoEntradaInput archivo,
			boolean esEstructurado, 
			List<MapeoEntradaInput> mapeosArchivo
	) {
		
		String nombre = archivo.getNombre();
		String delimitador = archivo.getDelimitadorColumnas();
		String saltarRegistrosIniciales = utilities.safeIntToStringParser(archivo.getSaltarRegistrosIniciales());
		String saltarRegistrosFinales = utilities.safeIntToStringParser(archivo.getSaltarRegistrosFinales());
		String numeroHoja = utilities.safeIntToStringParser(archivo.getNumeroHoja());
		String estructuras = esEstructurado? utilities.generarStringIdsGrupoEstructuras(archivo.getEstructuras()) : null;
			
		List<String> listaStringAtributosMapeos;
		if (esEstructurado) {
			List<Integer> estructurasArchivo = archivo.getEstructuras();
			listaStringAtributosMapeos = this.generarListaStringMapeosEstructuras(estructurasArchivo, mapeosArchivo);
		} else {
			List<ColumnaMetadatosInput> columnasArchivo = archivo.getColumnas();
			listaStringAtributosMapeos = this.generarListaStringColumnasArchivo(columnasArchivo, mapeosArchivo);
		}

		// crea el mapa, lo pobla con datos...
		Map<Integer, String> hmap = new HashMap<>();
		hmap.put(0, nombre);
		hmap.put(1, delimitador);
		hmap.put(2, saltarRegistrosIniciales);
		hmap.put(3, saltarRegistrosFinales);
		hmap.put(4, numeroHoja);
		hmap.put(5, estructuras);
		
		for (int columnaIndex = 0; columnaIndex < listaStringAtributosMapeos.size(); columnaIndex++) {
			String detalle = listaStringAtributosMapeos.get(columnaIndex);
			hmap.put(6+columnaIndex, detalle);
		}
		
		return hmap;
	}

	@Override
	public RequestResultOutput addOrUpdatePlantillaEntrada(
			PlantillaEntradaInput newTemplate
	) {
		LOG.info("addOrUpdatePlantillaEntrada");
		
		boolean esEstructurado = (newTemplate.getTipoExtension() == Constants.ID_TIPO_EXTENSION_ARCHIVO_ESTRUCTURADO);

		List<MapeoEntradaInput> mapeosPlantilla = newTemplate.getColumnasDestino();
		
		// por cada archivo, un mapa de informacion
		List<Map<Integer, String>> archivosArrayList = new ArrayList<>();
		for (ArchivoEntradaInput archivo : newTemplate.getArchivos()) {
			
			List<MapeoEntradaInput> mapeosArchivo = this.filtrarMapeosPorIdArchivo(mapeosPlantilla, archivo.getId());
			Map<Integer, String> hmap = this.generarMapaStringDatosArchivoEntrada(archivo, esEstructurado, mapeosArchivo);
			archivosArrayList.add(hmap);
		}

		boolean noRealizarTransformaciones = newTemplate.getArchivos().get(0).isNoRealizarTransformaciones();
		try {
			ptlSvc.almacenarArrayBidimensionalEnArregloUtil(archivosArrayList);
			return callSpAlmacenarPlantilla(newTemplate, noRealizarTransformaciones);
		} catch (IcsPlantillaServiceException e) {
			throw new IcsPlantillaServiceException("Error al guardar la plantilla de entrada", e);
		}
	}

	public List<PlantillaOutput> getVigentes(Long idSocio, Long idEvento) {
		
		List<PlantillaOutput> lista = new ArrayList<>();
		QIcsPlantilla icsPlt = QIcsPlantilla.icsPlantilla;
		
		BooleanBuilder bb = new BooleanBuilder()
				.and(icsPlt.icsTipoPlantilla.idTipoPlantilla.eq(Constants.ID_TIPO_PLANTILLA_ENTRADA))
				.and(icsPlt.vigencia.eq("Y"))
				.and(icsPlt.icsEvento.idEvento.eq(idEvento))
				.and(icsPlt.icsSocio.idSocio.eq(idSocio));
		
		// conversión simple
		Iterable<IcsPlantilla> iteraPlantillas = plantillaRepo.findAll(bb); 
		for (IcsPlantilla icsPlantilla : iteraPlantillas) {
			
			PlantillaOutput plantilla = new PlantillaOutput();
			plantilla.setId(icsPlantilla.getIdPlantilla());
			plantilla.setNombre(icsPlantilla.getNombrePlantilla());
			lista.add(plantilla);
		}
		
		return lista;
	}
}
