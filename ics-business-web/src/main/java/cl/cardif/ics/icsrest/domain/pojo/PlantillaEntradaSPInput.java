package cl.cardif.ics.icsrest.domain.pojo;

import java.util.ArrayList;
import java.util.List;

public class PlantillaEntradaSPInput {

	private String nombrePlantilla;
	private Long versionPlantilla;
	private Long idTipoPlantilla;
	private Long idSocio;
	private Long idEvento;
	private Long idCore;
	private String observaciones;
	private Long idTipoExtension;
	private List<PlantillaEntradaArchivoSPInput> archivos;

	public PlantillaEntradaSPInput() {
		super();
	}

	public List<PlantillaEntradaArchivoSPInput> getArchivos() {
		if (archivos != null) {
			return new ArrayList<>(archivos);
		} else {
			return new ArrayList<>();
		}
	}

	public void setArchivos(List<PlantillaEntradaArchivoSPInput> archivos) {
		if (archivos != null) {
			this.archivos = new ArrayList<>(archivos);
		}
	}

	public Long getIdCore() {
		return idCore;
	}

	public void setIdCore(Long idCore) {
		this.idCore = idCore;
	}

	public Long getIdEvento() {
		return idEvento;
	}

	public void setIdEvento(Long idEvento) {
		this.idEvento = idEvento;
	}

	public Long getIdSocio() {
		return idSocio;
	}

	public void setIdSocio(Long idSocio) {
		this.idSocio = idSocio;
	}

	public Long getIdTipoExtension() {
		return idTipoExtension;
	}

	public void setIdTipoExtension(Long idTipoExtension) {
		this.idTipoExtension = idTipoExtension;
	}

	public Long getIdTipoPlantilla() {
		return idTipoPlantilla;
	}

	public void setIdTipoPlantilla(Long idTipoPlantilla) {
		this.idTipoPlantilla = idTipoPlantilla;
	}

	public String getNombrePlantilla() {
		return nombrePlantilla;
	}

	public void setNombrePlantilla(String nombrePlantilla) {
		this.nombrePlantilla = nombrePlantilla;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public Long getVersionPlantilla() {
		return versionPlantilla;
	}

	public void setVersionPlantilla(Long versionPlantilla) {
		this.versionPlantilla = versionPlantilla;
	}
}
