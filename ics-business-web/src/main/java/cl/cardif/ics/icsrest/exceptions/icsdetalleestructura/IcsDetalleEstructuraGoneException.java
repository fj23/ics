package cl.cardif.ics.icsrest.exceptions.icsdetalleestructura;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.GONE)
public class IcsDetalleEstructuraGoneException extends RuntimeException {

  private static final long serialVersionUID = -7209750264039464453L;

  public IcsDetalleEstructuraGoneException(String exception) {
    super(exception);
  }
}
