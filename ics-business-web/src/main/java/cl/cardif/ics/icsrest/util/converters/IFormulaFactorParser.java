package cl.cardif.ics.icsrest.util.converters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import cl.cardif.ics.icsrest.formula.IFormulaFactor;

@Component
public class IFormulaFactorParser implements Converter<String, IFormulaFactor> {
	static final Logger LOG = LoggerFactory.getLogger(IFormulaFactorParser.class);

	@Override
	public IFormulaFactor convert(String input) {
		LOG.info("convert");
		
		IFormulaFactor formula = null;
		ObjectMapper mapper = new ObjectMapper()
						.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		
		try {
			formula = mapper.readValue(input, IFormulaFactor.class);
		} catch (Exception e) {
			LOG.error("Error al convertir el JSON a formula ", e);
		}
		
		return formula;
	}
}
