package cl.cardif.ics.icsrest.domain.dto.input;

import java.util.ArrayList;
import java.util.List;

public class EstructuraAnetoModel extends EstructuraAnetoSimpleInput {
	private List<ColumnaMetadatosModel> columnas;
	private String vigencia;

	public EstructuraAnetoModel() {
		super();
	}

	public EstructuraAnetoModel(long id) {
		super();
		this.id = id;
	}

	public List<ColumnaMetadatosModel> getColumnas() {
		if (this.columnas != null) {
			return new ArrayList<>(columnas);
		} else {
			return new ArrayList<>();
		}
	}

	public void setColumnas(List<ColumnaMetadatosModel> columnas) {
		if (columnas != null) {
			this.columnas = new ArrayList<>(columnas);
		}
	}

	public String getVigencia() {
		return vigencia;
	}

	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}

	@Override
	public String toString() {
		return "EstructuraAnetoModel [columnas=" + columnas + ", vigencia=" + vigencia + ", cardMin=" + cardMin
				+ ", cardMax=" + cardMax + ", id=" + id + ", codigo=" + codigo + ", descripcion=" + descripcion + "]";
	}

}
