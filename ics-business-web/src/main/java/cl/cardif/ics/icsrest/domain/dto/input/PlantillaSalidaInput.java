package cl.cardif.ics.icsrest.domain.dto.input;

import java.util.ArrayList;
import java.util.List;

public class PlantillaSalidaInput extends PlantillaInput {
	private List<ColumnaMetadatosSalidaModel> columnas;
	private List<MapeoEntradaInput> columnasDestino;
	private List<EstructuraAnetoPlantillaSalidaModel> estructuras;
	private String delimitadorColumnas;
	private boolean formatoOriginal;
	private Long directorioSalida;

	public PlantillaSalidaInput() {
		super();
	}

	public PlantillaSalidaInput(Long id) {
		super();
		this.id = id;
	}

	public List<ColumnaMetadatosSalidaModel> getColumnas() {
		if (this.columnas != null) {
			return new ArrayList<>(this.columnas);
		}
		else {
			return new ArrayList<>();
		}
	}

	public void setColumnas(List<ColumnaMetadatosSalidaModel> columnas) {
		if (columnas != null) {
			this.columnas = new ArrayList<>(columnas);
		}
	}

	public List<MapeoEntradaInput> getColumnasDestino() {
		if (this.columnasDestino != null) {
			return new ArrayList<>(this.columnasDestino);
		}
		else {
			return new ArrayList<>();
		}
	}

	public void setColumnasDestino(List<MapeoEntradaInput> columnasDestino) {
		if (columnasDestino != null) {
			this.columnasDestino = new ArrayList<>(columnasDestino);
		}
	}

	public String getDelimitadorColumnas() {
		return delimitadorColumnas;
	}

	public void setDelimitadorColumnas(String delimitadorColumnas) {
		this.delimitadorColumnas = delimitadorColumnas;
	}

	public List<EstructuraAnetoPlantillaSalidaModel> getEstructuras() {
		if (this.estructuras != null) {
			return new ArrayList<>(this.estructuras);
		} else {
			return new ArrayList<>();
		}
	}

	public void setEstructuras(List<EstructuraAnetoPlantillaSalidaModel> estructuras) {
		if (estructuras != null) {
			this.estructuras = new ArrayList<>(estructuras);
		}
	}

	public boolean isFormatoOriginal() {
		return formatoOriginal;
	}

	public void setFormatoOriginal(boolean formatoOriginal) {
		this.formatoOriginal = formatoOriginal;
	}

	public Long getDirectorioSalida() {
		return directorioSalida;
	}

	public void setDirectorioSalida(Long directorioSalida) {
		this.directorioSalida = directorioSalida;
	}
}
