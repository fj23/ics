package cl.cardif.ics.icsrest.security.jwt;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.filter.OncePerRequestFilter;

import com.querydsl.core.BooleanBuilder;

import cl.cardif.ics.domain.entity.common.QIcsLogin;
import cl.cardif.ics.icsrest.config.WebSecurityConfig;
import cl.cardif.ics.icsrest.repository.LoginRepository;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtParser;

@Component
public class JwtTokenFilter 
	extends OncePerRequestFilter {

	private static final Logger LOG = LoggerFactory.getLogger(JwtTokenFilter.class);

	@Autowired private LoginRepository loginRepository;

	@Autowired private JwtTokenProvider jwtTokenProvider;
	@Autowired private JwtParser jwtParser;

	@Override
	protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
		return new AntPathMatcher().match(WebSecurityConfig.LOGIN_AUTH_URL, request.getPathInfo());
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		String token = jwtTokenProvider.resolveToken(request);
		LOG.info("doFilterInternal - token={}", token);

		try {
			
			String sesameToken;
			try {
				Jws<Claims> claims = this.jwtParser.parseClaimsJws(token);
				sesameToken = (String) claims.getBody().get("sesame-token");
			} catch (Exception exc) {
				throw new AccessDeniedException("Token expirado o no valido");
			}

			// Valida si el token aun existe en bd (sesion multiple)
			BooleanBuilder conToken = new BooleanBuilder()
					.and(QIcsLogin.icsLogin.token.eq(sesameToken));

			if (!loginRepository.exists(conToken)) {
				throw new AccessDeniedException("Sesión finalizada debido a otra conexión");
			}

			Authentication auth = jwtTokenProvider.getAuthentication(token);
			SecurityContextHolder.getContext().setAuthentication(auth);
		} catch (AccessDeniedException e) {
			LOG.info("No se pudo validar el token", e);
			response.sendError(HttpStatus.UNAUTHORIZED.value(), e.getMessage());
			response.getWriter().write(e.getMessage());
			return;
		}

		filterChain.doFilter(request, response);
	}

}
