package cl.cardif.ics.icsrest.domain.dto.input;

public class ColumnaDestinoEntradaModelInput {

  private long idAtributoNegocio;
  private String nombreColumnaNegocio;

  public ColumnaDestinoEntradaModelInput() {
    super();
  }

  public long getIdAtributoNegocio() {
    return idAtributoNegocio;
  }

  public void setIdAtributoNegocio(long idAtributoNegocio) {
    this.idAtributoNegocio = idAtributoNegocio;
  }

  public String getNombreColumnaNegocio() {
    return nombreColumnaNegocio;
  }

  public void setNombreColumnaNegocio(String nombreColumnaNegocio) {
    this.nombreColumnaNegocio = nombreColumnaNegocio;
  }
}
