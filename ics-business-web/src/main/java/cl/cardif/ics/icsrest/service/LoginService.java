package cl.cardif.ics.icsrest.service;

import java.util.List;

import cl.cardif.ics.icsrest.domain.pojo.Login;
import cl.cardif.ics.icsrest.exceptions.login.IcsLoginException;
import cl.cardif.ics.icsrest.message.response.AuthLoginResponse;

public interface LoginService {

	AuthLoginResponse autenticar(Login loginInput) throws IcsLoginException;

	List<String> getRoles(String token);

	boolean validarToken(String token);

	Integer getInactivityNoticeTime();

	void logout(String token);

	void saveSession(AuthLoginResponse authLoginResponse);

	List<Integer> getValidationConfig();
}
