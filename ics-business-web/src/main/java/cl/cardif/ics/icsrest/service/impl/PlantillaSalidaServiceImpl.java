package cl.cardif.ics.icsrest.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.OrderSpecifier;

import cl.cardif.ics.domain.entity.common.IcsCore;
import cl.cardif.ics.domain.entity.common.IcsDirectoriosSalida;
import cl.cardif.ics.domain.entity.common.IcsParametro;
import cl.cardif.ics.domain.entity.common.IcsTipoExtension;
import cl.cardif.ics.domain.entity.common.QIcsCore;
import cl.cardif.ics.domain.entity.common.QIcsDirectoriosSalida;
import cl.cardif.ics.domain.entity.common.QIcsParametro;
import cl.cardif.ics.domain.entity.estructuras.IcsDetalleEstructura;
import cl.cardif.ics.domain.entity.estructuras.IcsDetalleSubestructura;
import cl.cardif.ics.domain.entity.estructuras.IcsEstructura;
import cl.cardif.ics.domain.entity.estructuras.IcsEstructurasAgrupada;
import cl.cardif.ics.domain.entity.estructuras.IcsSubestructura;
import cl.cardif.ics.domain.entity.estructuras.QIcsDetalleEstructura;
import cl.cardif.ics.domain.entity.estructuras.QIcsDetalleSubestructura;
import cl.cardif.ics.domain.entity.estructuras.QIcsEstructurasAgrupada;
import cl.cardif.ics.domain.entity.plantillas.IcsArchivo;
import cl.cardif.ics.domain.entity.plantillas.IcsAtributoCnfSalida;
import cl.cardif.ics.domain.entity.plantillas.IcsAtributoSalidaEstruc;
import cl.cardif.ics.domain.entity.plantillas.IcsAtributoUsuario;
import cl.cardif.ics.domain.entity.plantillas.IcsAtributosSalida;
import cl.cardif.ics.domain.entity.plantillas.IcsOrdenTrabajosSalida;
import cl.cardif.ics.domain.entity.plantillas.IcsPlantilla;
import cl.cardif.ics.domain.entity.plantillas.QIcsAtributoCnfSalida;
import cl.cardif.ics.domain.entity.plantillas.QIcsAtributosSalida;
import cl.cardif.ics.domain.entity.plantillas.QIcsOrdenTrabajosSalida;
import cl.cardif.ics.icsrest.domain.dto.input.ColumnaMetadatosSalidaModel;
import cl.cardif.ics.icsrest.domain.dto.input.DetalleEstructuraPlantillaSalidaInput;
import cl.cardif.ics.icsrest.domain.dto.input.EstructuraAnetoPlantillaSalidaModel;
import cl.cardif.ics.icsrest.domain.dto.input.PlantillaSalidaInput;
import cl.cardif.ics.icsrest.domain.dto.output.AtributoSalidaEstrucOutput;
import cl.cardif.ics.icsrest.domain.dto.output.AtributosSalidaOutput;
import cl.cardif.ics.icsrest.domain.dto.output.PlantillaOutput;
import cl.cardif.ics.icsrest.domain.dto.output.PlantillaSalidaOutput;
import cl.cardif.ics.icsrest.domain.dto.output.RequestResultOutput;
import cl.cardif.ics.icsrest.domain.pojo.Archivo;
import cl.cardif.ics.icsrest.domain.pojo.Core;
import cl.cardif.ics.icsrest.domain.pojo.DirectorioSalida;
import cl.cardif.ics.icsrest.domain.pojo.TipoExtension;
import cl.cardif.ics.icsrest.exceptions.icsplantilla.IcsPlantillaServiceException;
import cl.cardif.ics.icsrest.formula.IFormulaFactor;
import cl.cardif.ics.icsrest.repository.ArchivosRepository;
import cl.cardif.ics.icsrest.repository.AtributosCnfSalidaRepository;
import cl.cardif.ics.icsrest.repository.AtributosSalidaEstrucRepository;
import cl.cardif.ics.icsrest.repository.AtributosSalidaRepository;
import cl.cardif.ics.icsrest.repository.AtributosUsuarioRepository;
import cl.cardif.ics.icsrest.repository.CoresRepository;
import cl.cardif.ics.icsrest.repository.DetallesEstructurasRepository;
import cl.cardif.ics.icsrest.repository.DetallesSubestructurasRepository;
import cl.cardif.ics.icsrest.repository.DirectoriosSalidaRepository;
import cl.cardif.ics.icsrest.repository.EstructurasAgrupadasRepository;
import cl.cardif.ics.icsrest.repository.EstructurasRepository;
import cl.cardif.ics.icsrest.repository.OrdenAtributosCnfSalidaRepository;
import cl.cardif.ics.icsrest.repository.ParametrosRepository;
import cl.cardif.ics.icsrest.repository.PlantillaRepository;
import cl.cardif.ics.icsrest.repository.SubestructurasRepository;
import cl.cardif.ics.icsrest.service.PlantillaSalidaService;
import cl.cardif.ics.icsrest.service.PlantillasService;
import cl.cardif.ics.icsrest.util.Constants;
import cl.cardif.ics.icsrest.util.PlantillaUtil;

@Service
public class PlantillaSalidaServiceImpl implements PlantillaSalidaService {
	private static final Logger LOG = LoggerFactory.getLogger(PlantillaSalidaServiceImpl.class);
	
	private PlantillaUtil utilities = new PlantillaUtil();
	
	@Autowired private PlantillasService plantillasService;
	@Autowired private PlantillaRepository plantillaRepository;
	@Autowired private ParametrosRepository parametrosRepository;
	@Autowired private ArchivosRepository archivosRepository;
	@Autowired private EstructurasRepository estructurasRepository;
	@Autowired private SubestructurasRepository subestructurasRepository;
	@Autowired private DetallesEstructurasRepository detallesEstructurasRepository;
	@Autowired private DetallesSubestructurasRepository detallesSubestructurasRepository;
	@Autowired private EstructurasAgrupadasRepository gruposEstructurasRepository;
	@Autowired private CoresRepository coresRepository;
	@Autowired private OrdenAtributosCnfSalidaRepository ordenCnfSalidaRepository;
	@Autowired private AtributosCnfSalidaRepository atributosCnfSalidaRepository;
	@Autowired private AtributosSalidaRepository atributosSalidaRepository;
	@Autowired private AtributosSalidaEstrucRepository atributosSalidaEstrucRepository;
	@Autowired private AtributosUsuarioRepository atributosUsuarioRepository;
	@Autowired private DirectoriosSalidaRepository directorioSalidaRepository;
	@Autowired private ConversionService conversionService;
	
	
	@Override
	public Collection<AtributoSalidaEstrucOutput> consultarAtributosOrigenEstructuras() {
		LOG.info("consultarAtributosOrigenEstructuras");
		
		Iterable<IcsAtributoSalidaEstruc> iteraAtributos = atributosSalidaEstrucRepository.findAll();
		
		List<AtributoSalidaEstrucOutput> lista = new ArrayList<>();
		for (IcsAtributoSalidaEstruc atributoSalidaEstrucOutput : iteraAtributos) {
			AtributoSalidaEstrucOutput atributoSalida = conversionService.convert(atributoSalidaEstrucOutput, AtributoSalidaEstrucOutput.class);
			lista.add(atributoSalida);
		}
		
		return lista;
	}

	@Override
	public Collection<Core> consultarCoresHomologacionesEstructuras() {
		LOG.info("consultarCoresHomologacionesEstructuras");
		
		BooleanBuilder bb;
		IcsParametro paramCores;
		
		String codParametro = "WEB_SALIDA_ESTRUC";
		String conceptoParametro = "CORES_HOMOLOGACIONES_DISPONIBLES";
		
		try {
			QIcsParametro qParam = QIcsParametro.icsParametro;
			bb = new BooleanBuilder()
					.and(qParam.codParametro.eq(codParametro))
					.and(qParam.conceptoParametro.eq(conceptoParametro));
			
			paramCores = parametrosRepository.findOne(bb);			
		} catch (Exception exc) {
			LOG.error("No se pudo encontrar el parámetro '"+codParametro+"."+conceptoParametro+"' en la base de datos", exc);
			paramCores = null;
		}
		
		Iterable<IcsCore> iteraCores = null;
		if (paramCores != null) {
			
			try {
				List<Long> coreList = new ArrayList<>();
				String[] strCores = paramCores.getValorParametro().split(",");
				for (int i = 0; i < strCores.length; i++) {
					Long idCore = Long.valueOf(strCores[i]);
					coreList.add(idCore);
				}
				
				bb = new BooleanBuilder().and(QIcsCore.icsCore.idCore.in(coreList));
				iteraCores = coresRepository.findAll(bb);
			} catch (Exception exc) {
				LOG.error("No se pudo traducir el parámetro '"+codParametro+"."+conceptoParametro+"' de la base de datos", exc);
				iteraCores = null;
			}
		}
		
		if (iteraCores == null) {
			iteraCores = coresRepository.findAll();
		}

		List<Core> lista = new ArrayList<>();
		for (IcsCore icsCore : iteraCores) {
			Core core = conversionService.convert(icsCore, Core.class);
			lista.add(core);
		}
		
		return lista;
	}

	private Long filterIdEstructuraByTipo(IcsAtributoCnfSalida atr) {
		Long idDetalleEstructura = null; 
		IcsDetalleEstructura atrEstruc = atr.getIcsDetalleEstructura();
		IcsDetalleSubestructura atrSubestruc = atr.getIcsDetalleSubestructura();
		if (atrEstruc != null) {
			idDetalleEstructura = atrEstruc.getIdDetalleEstructura();
		} else if (atrSubestruc != null) {
			idDetalleEstructura = atrSubestruc.getIdDetalleSubestructura();
		}
		return idDetalleEstructura;
	}

	private Map<String, Integer> obtenerMapaOrdenTransformacionesPorIdAtributoAnulacion(Long idAtrAnulacion) {
		
		Map<String, Integer> ordenesTransformacion = new HashMap<>();
		BooleanBuilder bb = new BooleanBuilder()
				.and(QIcsOrdenTrabajosSalida.icsOrdenTrabajosSalida.idAtributoAnulacion.eq(idAtrAnulacion));
		Iterable<IcsOrdenTrabajosSalida> iteraOrdenes = ordenCnfSalidaRepository.findAll(bb);
		for (IcsOrdenTrabajosSalida orden: iteraOrdenes) {
			ordenesTransformacion.put(orden.getTipoTrabajo(), orden.getSecuencia().intValue());
		}
		
		return ordenesTransformacion;
	}

	private DetalleEstructuraPlantillaSalidaInput convertirCnfSalidaADetalleEstructuraAPtlSalidaInput(IcsAtributoCnfSalida atr, Long idDetalleEstructura) {

		Long idAtrAnulacion = atr.getIdAtributoAnulacion();
		Map<String, Integer> mapaOrden = this.obtenerMapaOrdenTransformacionesPorIdAtributoAnulacion(idAtrAnulacion);
		
		DetalleEstructuraPlantillaSalidaInput atrInput = new DetalleEstructuraPlantillaSalidaInput();
		
		//puede ser detalle de estructura o subestructura
		atrInput.setIdDetalleEstructura(idDetalleEstructura);
		
		if (atr.getValor() != null && !atr.getValor().isEmpty()) {
			atrInput.setValorFijo(atr.getValor());
		} else if (atr.getIcsAtributoSalidaEstruc() != null) {
			atrInput.setIdAtributoDiccionario(atr.getIcsAtributoSalidaEstruc().getIdAtributoEst());
		}
			
		if (atr.getIcsTipoFormula() != null && atr.getFormulaJSON() != null && !atr.getFormulaJSON().isEmpty()) {
			String formulaJSON = atr.getFormulaJSON();
			IFormulaFactor formula = conversionService.convert(formulaJSON, IFormulaFactor.class);
			atrInput.setFormula(formula);
			if (mapaOrden.isEmpty()) {
				atrInput.setOrdenFormula(1);
			} else {
				atrInput.setOrdenFormula(mapaOrden.get(Constants.TRABAJO_FORMULA));
			}
		}
		
		if (atr.getIcsDetalleFormato() != null) {
			Long idTipoFormato = atr.getIcsDetalleFormato().getIdDetalleFormato();
			atrInput.setIdTipoFormato(idTipoFormato);
			if (mapaOrden.isEmpty()) {
				atrInput.setOrdenFormato(1);
			} else {
				atrInput.setOrdenFormato(mapaOrden.get(Constants.TRABAJO_FORMATO));
			}
		}
		
		if (atr.getIcsTipoHomologacion() != null) {
			Long idTipoHomologacion = atr.getIcsTipoHomologacion().getIdDeTipoHomologacion();
			atrInput.setIdTipoHomologacion(idTipoHomologacion);
			if (mapaOrden.isEmpty()) {
				atrInput.setOrdenHomologacion(1);
			} else {
				atrInput.setOrdenHomologacion(mapaOrden.get(Constants.TRABAJO_HOMOLOGACION));
			}
		}
		return atrInput;
	}

	private Map<Long, DetalleEstructuraPlantillaSalidaInput> mapIdsToAtributosCnfSalidaFoundByIdPlantilla(Long idPlantilla) {
		
		BooleanBuilder bb = new BooleanBuilder()
				.and(QIcsAtributoCnfSalida.icsAtributoCnfSalida.icsPlantilla.idPlantilla.eq(idPlantilla));
		
		Iterable<IcsAtributoCnfSalida> iteraAtributosCnfSalida = atributosCnfSalidaRepository.findAll(bb);
		Map<Long, DetalleEstructuraPlantillaSalidaInput> atrsCnfSalida = new HashMap<>();
		
		for (IcsAtributoCnfSalida atr : iteraAtributosCnfSalida) {
			Long idDetalleEstructura = this.filterIdEstructuraByTipo(atr); 
			DetalleEstructuraPlantillaSalidaInput atrInput = this.convertirCnfSalidaADetalleEstructuraAPtlSalidaInput(atr, idDetalleEstructura);
			
			atrsCnfSalida.put(idDetalleEstructura, atrInput);
		}
		
		return atrsCnfSalida;
	}

	private List<DetalleEstructuraPlantillaSalidaInput> crearDetallesSubstructuraPlantillaSalida(Map<Long, DetalleEstructuraPlantillaSalidaInput> mapaIdCnfSalida, Long idSubestructura) {
		
		BooleanBuilder bb = new BooleanBuilder()
				.and(QIcsDetalleSubestructura.icsDetalleSubestructura.icsSubestructura.idSubestructura.eq(idSubestructura));
		
		Iterable<IcsDetalleSubestructura> iteraDetalles = detallesSubestructurasRepository.findAll(bb);
		List<DetalleEstructuraPlantillaSalidaInput> atrsEstruc = new ArrayList<>();
		
		for (IcsDetalleSubestructura dtl : iteraDetalles) {
			DetalleEstructuraPlantillaSalidaInput estrucDtl;
			
			DetalleEstructuraPlantillaSalidaInput cnfSalida = mapaIdCnfSalida.get(dtl.getIdDetalleSubestructura());
			if (cnfSalida != null) {
				estrucDtl = cnfSalida;
			} else {
				estrucDtl = new DetalleEstructuraPlantillaSalidaInput();
				estrucDtl.setIdDetalleEstructura(dtl.getIdDetalleSubestructura());
				estrucDtl.setCodigoColumna(dtl.getCodigoDetalleSubestructura());
				estrucDtl.setDescripcionColumna(dtl.getDescripcionDetalleSubestructura());
			}
			
			atrsEstruc.add(estrucDtl);
		}
		return atrsEstruc;
	}

	private List<DetalleEstructuraPlantillaSalidaInput> crearDetallesEstructuraPlantillaSalida(Map<Long, DetalleEstructuraPlantillaSalidaInput> mapaIdCnfSalida, Long idEstructura) {
		
		
		BooleanBuilder bb = new BooleanBuilder()
				.and(QIcsDetalleEstructura.icsDetalleEstructura.icsEstructura.idEstructura.eq(idEstructura));
		Iterable<IcsDetalleEstructura> iteraDetalles = detallesEstructurasRepository.findAll(bb);
		List<DetalleEstructuraPlantillaSalidaInput> atrsEstruc = new ArrayList<>();
		
		for (IcsDetalleEstructura dtl : iteraDetalles) {
			DetalleEstructuraPlantillaSalidaInput estrucDtl;
			
			DetalleEstructuraPlantillaSalidaInput cnfSalida = mapaIdCnfSalida.get(dtl.getIdDetalleEstructura());
			if (cnfSalida != null) {
				estrucDtl = cnfSalida;
			} else {
				estrucDtl = new DetalleEstructuraPlantillaSalidaInput();
			}
			estrucDtl.setIdDetalleEstructura(dtl.getIdDetalleEstructura());
			estrucDtl.setCodigoColumna(dtl.getCodigoEstructura());
			estrucDtl.setDescripcionColumna(dtl.getDescripcionDetalleEstructura());
			
			atrsEstruc.add(estrucDtl);
		}
		
		return atrsEstruc;
	}

	@Override
	public List<EstructuraAnetoPlantillaSalidaModel> obtenerEstructurasPlantillaSalida(Long idPlantilla) {

		IcsPlantilla icsPlantilla = plantillaRepository.findOne(idPlantilla);
		if (icsPlantilla != null) {		
			Long idArchivo = icsPlantilla.getIcsArchivo().getIdArchivo();
			IcsArchivo icsArchivo = archivosRepository.findOne(idArchivo);
			if (icsArchivo != null) {
				Long idGrupoEstructuras = icsArchivo.getIdGrupoEstructuras();
				
				if (idGrupoEstructuras != null && idGrupoEstructuras != 0L) {

					Map<Long, DetalleEstructuraPlantillaSalidaInput> mapaIdDetalleEstructuraCnfSalida = this.mapIdsToAtributosCnfSalidaFoundByIdPlantilla(idPlantilla);
					
					BooleanBuilder bb = new BooleanBuilder()
							.and(QIcsEstructurasAgrupada.icsEstructurasAgrupada.idGrupoEstructuras.eq(idGrupoEstructuras));
					Iterable<IcsEstructurasAgrupada> iteraEstructuras = gruposEstructurasRepository.findAll(bb);
					
					List<EstructuraAnetoPlantillaSalidaModel> estructurasGrupo = new ArrayList<>();
					for (IcsEstructurasAgrupada estructuraGrupo : iteraEstructuras) {
						Long idEstructura = estructuraGrupo.getIdEstructura();
						
						// segun sea estructura o subestructura cargaremos datos y columnas
						IcsEstructura icsEstruc = estructurasRepository.findOne(idEstructura);
						IcsSubestructura icsSubestruc = subestructurasRepository.findOne(idEstructura);
						
						List<DetalleEstructuraPlantillaSalidaInput> columnas;
						EstructuraAnetoPlantillaSalidaModel estruc = new EstructuraAnetoPlantillaSalidaModel();
						estruc.setId(idEstructura);
						
						if (icsEstruc != null) { // si es detalle de estructura
							columnas = this.crearDetallesEstructuraPlantillaSalida(mapaIdDetalleEstructuraCnfSalida, idEstructura);
							
							estruc.setDescripcion(icsEstruc.getDescripcionEstructura());
							estruc.setCodigo(icsEstruc.getCodigoEstructura());
						} else if (icsSubestruc != null) { // si es detalle de subestructura
							columnas = this.crearDetallesSubstructuraPlantillaSalida(mapaIdDetalleEstructuraCnfSalida, idEstructura);
							
							estruc.setDescripcion(icsSubestruc.getDescripcionSubestructura());
							estruc.setCodigo(icsSubestruc.getCodigoSubestructura());
						} else {
							LOG.error("No se pudo encontrar una de las estructuras asociadas a la plantilla con id {}", idEstructura);
							continue;
						}
						
						estruc.setColumnas(columnas);
						estructurasGrupo.add(estruc);
					}
					
					return estructurasGrupo;
					
				} else {
					throw new IcsPlantillaServiceException("El archivo asociado no posee un grupo de estructuras.");
				}
			} else {
				throw new IcsPlantillaServiceException("No se pudo encontrar el archivo asociado a la plantilla");
			}
		} else {
			throw new IcsPlantillaServiceException("No se puede encontrar la plantilla con el ID "+idPlantilla);
		}
	}

	private List<AtributosSalidaOutput> getAtributosArchivoSalida(long idArchivo) {
		
		BooleanBuilder bb = new BooleanBuilder()
				.and(QIcsAtributosSalida.icsAtributosSalida.idArchivo.eq(idArchivo));

		Iterable<IcsAtributosSalida> iterableAtributosSalida = atributosSalidaRepository.findAll(bb);
		
		List<AtributosSalidaOutput> columnasArchivo = new ArrayList<>();
		for (IcsAtributosSalida icsAtributosSalida : iterableAtributosSalida) {
			AtributosSalidaOutput atrOutput = conversionService.convert(icsAtributosSalida, AtributosSalidaOutput.class);
			columnasArchivo.add(atrOutput);
		}

		return columnasArchivo;
	}

	@Override
	public PlantillaSalidaOutput viewTemplateDetalle(PlantillaOutput plantilla) {
		LOG.info("viewTemplateDetalle");
		
		PlantillaSalidaOutput plantillaSalida = conversionService.convert(plantilla, PlantillaSalidaOutput.class);

		IcsPlantilla icsPlantilla = plantillaRepository.findOne(plantilla.getId());
				
		IcsArchivo icsArchivo = icsPlantilla.getIcsArchivo();
		if (icsArchivo != null) {
			
			Archivo archivoSalida = conversionService.convert(icsArchivo, Archivo.class);
			List<AtributosSalidaOutput> atributosArchivo = this.getAtributosArchivoSalida(icsArchivo.getIdArchivo());
			archivoSalida.setColumnasSalida(atributosArchivo);

			IcsTipoExtension icsTipoExtension = icsArchivo.getIcsTipoExtension();
			if (icsTipoExtension != null) {
				TipoExtension tipoExtension = conversionService.convert(icsTipoExtension, TipoExtension.class);
				archivoSalida.setTipoExtension(tipoExtension);
			}
			
			plantillaSalida.setArchivo(archivoSalida);
		}
		
		IcsDirectoriosSalida icsDirectoriosSalida = icsPlantilla.getIcsDirectorioSalida();
		if (icsDirectoriosSalida != null) {
			plantillaSalida.setDirectorioSalida(icsDirectoriosSalida.getIdDirectorioSalida());
		}
		
		return plantillaSalida;
	}
	

	private Map<Integer, String> crearMapaAtributosEstrucPtlSalida(DetalleEstructuraPlantillaSalidaInput col) {
		
		String idDetalleEstructura = col.getIdDetalleEstructura() != null ? col.getIdDetalleEstructura().toString() : null;
		String idEstructura = col.getIdEstructura() != null ? col.getIdEstructura().toString() : null;
		String idSubEstructura = col.getIdSubEstructura() != null ? col.getIdDetalleEstructura().toString() : null;
		
		String idAtributoDiccionario = col.getIdAtributoDiccionario() != null? col.getIdAtributoDiccionario().toString() : null;
		String valorFijo = col.getValorFijo() != null? col.getValorFijo() : null;

		String tipoFormato = null;
		String tipoHomologacion = null;
		String formulaJSON = null;
		String formulaSQL = null;
		String ordenFormato = "0";
		String ordenHomologacion = "0";
		
		// sin datos no hay transformacion
		if (idAtributoDiccionario != null || valorFijo != null) {
		
			if (col.getIdTipoFormato() != null) {
				tipoFormato = col.getIdTipoFormato().toString();
				ordenFormato = col.getOrdenFormato().toString();
			}
			
			if (col.getIdTipoHomologacion() != null) {
				tipoHomologacion = col.getIdTipoHomologacion().toString();
				ordenHomologacion = col.getOrdenHomologacion().toString();
			}

			if (col.getFormula() != null) {
				formulaJSON = col.getFormula().toJSON();
				formulaSQL = col.getFormula().toSQL(null);
			}
		}
		
		
		Map<Integer, String> hmap = new HashMap<>();
		hmap.put(0, idDetalleEstructura);
		hmap.put(1, idEstructura);
		hmap.put(2, idSubEstructura);
		hmap.put(3, idAtributoDiccionario);
		hmap.put(4, valorFijo);
		hmap.put(5, tipoFormato);
		hmap.put(6, tipoHomologacion);
		hmap.put(7, formulaJSON);
		hmap.put(8, formulaSQL);
		hmap.put(9, ordenFormato);
		hmap.put(10, ordenHomologacion);
		return hmap;
	}

	private List<Map<Integer, String>> crearArrayBidimensionalAtributosPlantillaSalidaEstructurada(PlantillaSalidaInput newTemplate) {
		List<Map<Integer, String>> arrayListAtributos = new ArrayList<>();
		for (EstructuraAnetoPlantillaSalidaModel estr : newTemplate.getEstructuras()) {
			for (DetalleEstructuraPlantillaSalidaInput col : estr.getColumnas()) {
				Map<Integer, String> hmap = this.crearMapaAtributosEstrucPtlSalida(col);
				
				arrayListAtributos.add(hmap);
			}
		}
		return arrayListAtributos;
	}

	private List<Map<Integer, String>> crearArrayBidimensionalAtributosPtlSalidaEstandar(PlantillaSalidaInput newTemplate) {
		List<Map<Integer, String>> arrayListAtributos = new ArrayList<>();
		for (ColumnaMetadatosSalidaModel columna : newTemplate.getColumnas()) {
			
			Long idAtrUsuario = columna.getColumnaOrigen();
			if (idAtrUsuario == null || idAtrUsuario == 0) {
				throw new IcsPlantillaServiceException("Atributo de usuario invalido en la columna "+columna.getOrden());
			}
			
			IcsAtributoUsuario atrUsuario = this.atributosUsuarioRepository.findOne(idAtrUsuario);
			if (atrUsuario == null) {
				throw new IcsPlantillaServiceException("No existe un atributo de usuario con ID "+idAtrUsuario);
			}
			
			Map<Integer, String> hmap = new HashMap<>();
			hmap.put(0, columna.getNombre());
			hmap.put(1, utilities.safeLongToStringParser(columna.getOrden()));
			hmap.put(2, utilities.safeLongToStringParser(columna.getPosicionInicial()));
			hmap.put(3, utilities.safeLongToStringParser(columna.getPosicionFinal()));
			hmap.put(4, utilities.safeLongToStringParser(atrUsuario.getIdAtributosNegocio()));
			hmap.put(5, utilities.safeLongToStringParser(columna.getColumnaOrigen()));

			arrayListAtributos.add(hmap);
		}
		return arrayListAtributos;
	}

	private Map<String, Object> mapaParametrosDesdePlantilla(PlantillaSalidaInput plantilla) {
		Map<String, Object> map = new HashMap<>();
		
		String plantillaNombre = plantilla.getNombre().toUpperCase();
		
		Long idDirectorioSalida = null;
		if (plantilla.getDirectorioSalida() != null) {
			idDirectorioSalida = plantilla.getDirectorioSalida();
		} else {
			QIcsDirectoriosSalida qIcsDirectoriosSalida = QIcsDirectoriosSalida.icsDirectoriosSalida;
			BooleanBuilder directorioSalidaSocio = new BooleanBuilder()
					.and(qIcsDirectoriosSalida.tipoDirectorio.eq(0L))
					.and(qIcsDirectoriosSalida.descripcionDirectorio.eq("SOCIO"));
			
			IcsDirectoriosSalida icsDirectoriosSalida = directorioSalidaRepository.findOne(directorioSalidaSocio);
			idDirectorioSalida = icsDirectoriosSalida.getIdDirectorioSalida();
		}
		

		map.put(Constants.PV_TEMPLATE_CREATION_NOMBRE, plantillaNombre);
		map.put(Constants.PN_TEMPLATE_CREATION_VERSION, plantilla.getVersion());
		map.put(Constants.PN_TEMPLATE_CREATION_IDTIPOPLANTILLA, plantilla.getTipo());
		map.put(Constants.PN_TEMPLATE_CREATION_IDSOCIO, plantilla.getSocio());
		map.put(Constants.PN_TEMPLATE_CREATION_IDEVENTO, plantilla.getEvento());
		map.put(Constants.PN_TEMPLATE_CREATION_IDCORE, plantilla.getCore());
		map.put(Constants.PV_TEMPLATE_CREATION_OBSERVACIONES, plantilla.getObservaciones());
		map.put(Constants.PN_TEMPLATE_CREATION_IDTIPOEXTENSION, plantilla.getTipoExtension());
		map.put(Constants.PN_TEMPLATE_CREATION_IDTIPOSALIDA, plantilla.isFormatoOriginal()? 1 : 0);
		map.put(Constants.PV_TEMPLATE_CREATION_DELIMITADOR, plantilla.getDelimitadorColumnas());
		map.put(Constants.PN_TEMPLATE_CREATION_IDVERSIONANTERIOR, plantilla.getId());
		map.put(Constants.PV_TEMPLATE_CREATION_UNIONARCHIVOS, null);
		map.put(Constants.PV_TEMPLATE_CREATION_USUARIO, plantilla.getCodUsuario());
		map.put(Constants.PV_TEMPLATE_CREATION_DIRECTORIOSALIDA, idDirectorioSalida);
		return map;
	}

	public RequestResultOutput callSpAlmacenarPlantilla(PlantillaSalidaInput plantilla) {
		LOG.info("callSpAlmacenarPlantilla");

		Map<String, Object> map = this.mapaParametrosDesdePlantilla(plantilla);

		try {
			return plantillasService.llamarSPAlmacenarPlantilla(map);
		} catch (IcsPlantillaServiceException e) {
			throw new IcsPlantillaServiceException("Fallo al crear plantilla: " + e.getMessage(), e);
		}
	}
	
	@Override
	public RequestResultOutput addOrUpdatePlantillaSalida(PlantillaSalidaInput newTemplate, boolean isUpdate) {
		
		if (!newTemplate.isFormatoOriginal()) {
			boolean esEstructurada = (newTemplate.getTipoExtension() == Constants.ID_TIPO_EXTENSION_ARCHIVO_ESTRUCTURADO);
			List<Map<Integer, String>> arrayBidimensionalAtributos;
			
			if (!esEstructurada && newTemplate.getColumnas() != null) {
				arrayBidimensionalAtributos = this.crearArrayBidimensionalAtributosPtlSalidaEstandar(newTemplate);
			} else if (esEstructurada && newTemplate.getEstructuras() != null) {
				arrayBidimensionalAtributos = this.crearArrayBidimensionalAtributosPlantillaSalidaEstructurada(newTemplate);
			} else {
				throw new IcsPlantillaServiceException("El tipo de plantilla de salida no coincide con la información contenida en ella.");
			}
			
			// Guardar el arreglo en base de datos y crear la plantilla
			try {
				plantillasService.almacenarArrayBidimensionalEnArregloUtil(arrayBidimensionalAtributos);
			} catch (Exception e) {
				throw new IcsPlantillaServiceException("Error al crear plantilla", e);
			}
		}
		
		try {
			return this.callSpAlmacenarPlantilla(newTemplate);
		} catch (Exception e) {
			throw new IcsPlantillaServiceException("Error al crear plantilla", e);
		}	

	}

	@Override
	public List<DirectorioSalida> consultarDirectoriosSalida() {
		LOG.info("consultarDirectoriosSalida");
		QIcsDirectoriosSalida qIcsDirectoriosSalida = QIcsDirectoriosSalida.icsDirectoriosSalida;
		
		BooleanBuilder soloDirectoriosSalida = new BooleanBuilder()
				.and(qIcsDirectoriosSalida.tipoDirectorio.eq(0L));
		
		OrderSpecifier<Long> ordenPorId = qIcsDirectoriosSalida.idDirectorioSalida.asc();
		
		Iterable<IcsDirectoriosSalida> iteraDirectoriosSalidaError = directorioSalidaRepository.findAll(soloDirectoriosSalida, ordenPorId);
		
		List<DirectorioSalida> directoriosSalidaError = new ArrayList<>();
		for (IcsDirectoriosSalida icsDirectoriosSalida : iteraDirectoriosSalidaError) {
			DirectorioSalida tipoArchivo = conversionService.convert(icsDirectoriosSalida, DirectorioSalida.class);
			directoriosSalidaError.add(tipoArchivo);
		}
		
		return directoriosSalidaError;
	}
}
