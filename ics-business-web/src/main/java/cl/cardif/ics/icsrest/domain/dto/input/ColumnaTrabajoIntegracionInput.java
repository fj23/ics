package cl.cardif.ics.icsrest.domain.dto.input;

import cl.cardif.ics.icsrest.formula.IFormulaFactor;

public class ColumnaTrabajoIntegracionInput {

	private String nombre;
	private long columnaOrigen;
	private long columnaEnriquecimiento;
	private long homologacion;
	private long formato;

	/** La posicion que usará esta columna en la base de datos */
	private long orden;
	/** El orden entre Homologacion, Formato y Formula para esta columna */
	private long ordenFormato;
	/** El orden entre Homologacion, Formato y Formula para esta columna */
	private long ordenHomologacion;
	/** La formula (en forma de objeto) que podría usarse en esta columna */
	private IFormulaFactor formula;

	private Long columnaOrigenUser;
	private String tipoPersona;

	public ColumnaTrabajoIntegracionInput() {
		super();
	}

	public long getColumnaEnriquecimiento() {
		return columnaEnriquecimiento;
	}

	public void setColumnaEnriquecimiento(long columnaEnriquecimiento) {
		this.columnaEnriquecimiento = columnaEnriquecimiento;
	}

	public long getColumnaOrigen() {
		return columnaOrigen;
	}

	public void setColumnaOrigen(long columnaOrigen) {
		this.columnaOrigen = columnaOrigen;
	}

	public long getFormato() {
		return formato;
	}

	public void setFormato(long formato) {
		this.formato = formato;
	}

	public IFormulaFactor getFormula() {
		return formula;
	}

	public void setFormula(IFormulaFactor formula) {
		this.formula = formula;
	}

	public long getHomologacion() {
		return homologacion;
	}

	public void setHomologacion(long homologacion) {
		this.homologacion = homologacion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public long getOrden() {
		return orden;
	}

	public void setOrden(long orden) {
		this.orden = orden;
	}

	public long getOrdenFormato() {
		return ordenFormato;
	}

	public void setOrdenFormato(long ordenFormato) {
		this.ordenFormato = ordenFormato;
	}

	public long getOrdenHomologacion() {
		return ordenHomologacion;
	}

	public void setOrdenHomologacion(long ordenHomologacion) {
		this.ordenHomologacion = ordenHomologacion;
	}

	public Long getColumnaOrigenUser() {
		return columnaOrigenUser;
	}

	public void setColumnaOrigenUser(Long columnaOrigenUser) {
		this.columnaOrigenUser = columnaOrigenUser;
	}

	public String getTipoPersona() {
		return tipoPersona;
	}

	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	@Override
	public String toString() {
		return "ColumnaTrabajoIntegracionInput [nombre=" + nombre + ", columnaOrigen=" + columnaOrigen + ", columnaEnriquecimiento="
			+ columnaEnriquecimiento + ", homologacion=" + homologacion + ", formato=" + formato + ", orden=" + orden + ", ordenFormato="
			+ ordenFormato + ", ordenHomologacion=" + ordenHomologacion + ", formula=" + formula + ", columnaOrigenUser="
			+ columnaOrigenUser + ", tipoPersona=" + tipoPersona + "]";
	}
}
