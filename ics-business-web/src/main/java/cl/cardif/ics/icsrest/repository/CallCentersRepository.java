package cl.cardif.ics.icsrest.repository;

import cl.cardif.ics.domain.entity.common.IcsCallCenter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface CallCentersRepository
    extends JpaRepository<IcsCallCenter, Long>, 
    	QueryDslPredicateExecutor<IcsCallCenter> {
	
}
