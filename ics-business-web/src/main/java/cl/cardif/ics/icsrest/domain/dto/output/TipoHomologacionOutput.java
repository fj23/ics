package cl.cardif.ics.icsrest.domain.dto.output;

public class TipoHomologacionOutput {

  private long id;
  private String concepto;
  private String vigencia;

  private Long idSistema1;
  private Long idSistema2;
  private Long idEmpresa1;
  private Long idEmpresa2;
  private String nombreHomologacion;

  private Long orden;

  public TipoHomologacionOutput() {
    // Constructor JavaBean
  }

  public String getConcepto() {
    return concepto;
  }

  public void setConcepto(String concepto) {
    this.concepto = concepto;
  }

  public long getId() {
    return id;
  }

  public void setId(long idHomologacion) {
    this.id = idHomologacion;
  }

  public Long getIdEmpresa1() {
    return idEmpresa1;
  }

  public void setIdEmpresa1(Long idEmpresa1) {
    this.idEmpresa1 = idEmpresa1;
  }

  public Long getIdEmpresa2() {
    return idEmpresa2;
  }

  public void setIdEmpresa2(Long idEmpresa2) {
    this.idEmpresa2 = idEmpresa2;
  }

  public Long getIdSistema1() {
    return idSistema1;
  }

  public void setIdSistema1(Long idSistema1) {
    this.idSistema1 = idSistema1;
  }

  public Long getIdSistema2() {
    return idSistema2;
  }

  public void setIdSistema2(Long idSistema2) {
    this.idSistema2 = idSistema2;
  }

  public String getNombreHomologacion() {
    return nombreHomologacion;
  }

  public void setNombreHomologacion(String nombreHomologacion) {
    this.nombreHomologacion = nombreHomologacion;
  }

  public Long getOrden() {
    return orden;
  }

  public void setOrden(Long orden) {
    this.orden = orden;
  }

  public String getVigencia() {
    return vigencia;
  }

  public void setVigencia(String vigencia) {
    this.vigencia = vigencia;
  }
}
