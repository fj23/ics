package cl.cardif.ics.icsrest.formula.funciones.math;

import java.util.ArrayList;
import java.util.Map;

import cl.cardif.ics.icsrest.formula.FormulaParent;
import cl.cardif.ics.icsrest.formula.IMathFactor;
import cl.cardif.ics.icsrest.util.FormulaOrigenKey;

public class Trunc extends FormulaParent implements IMathFactor {
	public Trunc() {
		hijos = new ArrayList<>();
	}

	@Override
	public int getMaximumChildrenCount() {
		return 2;
	}

	@Override
	public String toSafeSQL(Map<FormulaOrigenKey, Integer> tableAliases) {
		StringBuilder sb = new StringBuilder()
			.append("TRUNC")
			.append("(")
			.append(getHijo(0).toSafeSQL(tableAliases)).append(",");

		if (this.hijos.size() > 1) {
			sb.append(getHijo(1).toSafeSQL(tableAliases));
		} else {
			sb.append("0");
		}

		sb.append(")");

		return sb.toString();
	}

	@Override
	public String toSQL(Map<FormulaOrigenKey, Integer> tableAliases) {
		StringBuilder sb = new StringBuilder()
			.append("TRUNC")
			.append("(")
			.append(getHijo(0).toSQL(tableAliases)).append(",");

		if (this.hijos.size() > 1) {
			sb.append(getHijo(1).toSQL(tableAliases));
		} else {
			sb.append("0");
		}

		sb.append(")");

		return sb.toString();
	}
}
