package cl.cardif.ics.icsrest.security.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import cl.cardif.ics.icsrest.domain.pojo.DetalleRol;
import cl.cardif.ics.icsrest.domain.pojo.Role;
import cl.cardif.ics.icsrest.domain.pojo.TipoRol;
import cl.cardif.ics.icsrest.domain.pojo.User;
import cl.cardif.ics.icsrest.service.RolesService;
import cl.cardif.ics.icsrest.util.RolesUtil;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtParser;

@Service
public class UserDetailsImpl implements UserDetailsService {

	@Autowired
	private RolesService rolesService;

	@Autowired
	private JwtParser jwtParser;

	@Value("${security.jwt.token.expire-length}")
	private Integer EXPIRATION_MINUTES;

	@Autowired
	private RolesUtil rolesUtil;

	private User findUserByToken(String token) {
		Jws<Claims> claims = this.jwtParser.parseClaimsJws(token);

		@SuppressWarnings("unchecked")
		List<String> rolesString = (List<String>) claims.getBody().get("roles");
		List<TipoRol> tipoRols = this.rolesService.findRolesByName(rolesString);
		Map<Long, DetalleRol> tipoRolMap = this.rolesUtil.createRichDetalleRolMapFromTipoRolList(tipoRols);

		List<Role> roles = this.getRoles(rolesString);

		for (Map.Entry<Long, DetalleRol> entry : tipoRolMap.entrySet()) {
			roles.add(new Role(entry.getValue().getCodigo().toString()));
		}

		User user = new User();
		user.setUsername(claims.getBody().getSubject());
		user.setRoles(roles);

		return user;
	}

	private List<Role> getRoles(List<String> rolesString) {
		List<Role> roles = new ArrayList<>();
		for (String role : rolesString) {
			if (role.contains("_RECAU")) {
				roles.add(new Role("EVENT_200003"));
			} else if (role.contains("_ALTAS")) {
				roles.add(new Role("EVENT_200005"));
			} else if (role.contains("_PROSP")) {
				roles.add(new Role("EVENT_200011"));
			} else if (role.contains("_INTERA")) {
				roles.add(new Role("EVENT_200010"));
			} else if (role.contains("_PAGO")) {
				roles.add(new Role("EVENT_200012"));
			} else if (role.contains("_CANCE")) {
				roles.add(new Role("EVENT_200000"));
				roles.add(new Role("EVENT_200001"));
				roles.add(new Role("EVENT_200002"));
				roles.add(new Role("EVENT_200004"));
				roles.add(new Role("EVENT_200006"));
				roles.add(new Role("EVENT_200007"));
				roles.add(new Role("EVENT_200008"));
			}
		}
		return roles;
	}

	@Override
	public UserDetails loadUserByUsername(String token) {
		User user = findUserByToken(token);
		return UserPrinciple.build(user);
	}

}
