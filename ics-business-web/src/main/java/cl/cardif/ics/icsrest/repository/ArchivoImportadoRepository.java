package cl.cardif.ics.icsrest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

import cl.cardif.ics.domain.entity.carga.IcsArchivoImportado;

@Repository
public interface ArchivoImportadoRepository 
	extends JpaRepository<IcsArchivoImportado, Long>, 
		QueryDslPredicateExecutor<IcsArchivoImportado> {

}
