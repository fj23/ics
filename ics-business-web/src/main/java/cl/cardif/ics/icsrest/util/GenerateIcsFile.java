package cl.cardif.ics.icsrest.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cl.cardif.ics.icsrest.domain.pojo.OutputFile;
import cl.cardif.ics.icsrest.exceptions.icstrazabilidad.IcsTrazabilidadServiceException;

public class GenerateIcsFile implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2709058357550161368L;

	private static final Logger LOG = LoggerFactory.getLogger(GenerateIcsFile.class);

	private static final String DELIMITER_COMMAND = ";";
	// private static final String SLASH = "/";
	// private static final String DOT = ".";
	private static final String UTF8 = "UTF-8";

	// Extension
	private static final String EXCEL_XLS = ".xls";
	private static final String EXCEL_XLSX = ".xlsx";
	private static final String CSV = ".csv";
	private static final String TXT = ".txt";

	private static final String ERROR_CERRAR_STREAM_CSV = "No se pudo cerrar stream csv";
	private static final String ERROR_UTF8_NO_ENCONTRADO = "No se encontro charset UTF8";

	public static byte[] generate(String extension, OutputFile outputFile) throws IcsTrazabilidadServiceException {
		if (extension.equals(EXCEL_XLSX)) {
			LOG.debug("ARCHIVO EXCEL");
			return generateXlsx(outputFile);
		} else if (extension.equals(EXCEL_XLS)) {
			LOG.debug("ARCHIVO EXCEL ANTIGUO");
			return generateXls(outputFile);
		} else if (extension.equals(CSV)) {
			LOG.debug("ARCHIVO CSV");
			return generateCsv(outputFile);
		} else if (extension.equals(TXT)) {
			LOG.debug("ARCHIVO TEXTO");
			return generateTxt(outputFile);
		} else {
			throw new IcsTrazabilidadServiceException("ARCHIVO CON EXTENSION INCORRECTA");
		}
	}

	private static byte[] generateXlsx(OutputFile outputFile) throws IcsTrazabilidadServiceException {
		LOG.debug("GENERANDO ARCHIVO XLSX");
		try (Workbook workbook = new XSSFWorkbook();) {
			return generateExcel(outputFile, workbook);
		} catch (Exception e) {
			throw new IcsTrazabilidadServiceException("ERROR AL CREAR EL ARCHIVO EXCEL XLS", e);
		}
	}

	private static byte[] generateXls(OutputFile outputFile) throws IcsTrazabilidadServiceException {
		LOG.debug("GENERANDO ARCHIVO XLSX");
		try (Workbook workbook = new HSSFWorkbook();) {
			return generateExcel(outputFile, workbook);
		} catch (Exception e) {
			throw new IcsTrazabilidadServiceException("ERROR AL CREAR EL ARCHIVO EXCEL XLS", e);
		}
	}

	private static List<List<String>> generarListaDatosHoja(OutputFile outputFile, List<String> listSheet,
			List<String> listDataType) {
		List<List<String>> dataAllSheet = new ArrayList<>();
		List<String> dateForSheet;
		List<String> dateForSheetSort;
		for (int i = 0; i < listSheet.size(); i++) {
			String data;
			dateForSheet = new ArrayList<>();
			for (int j = 0; j < outputFile.getSheet().size(); j++) {
				if (listSheet.get(i).equalsIgnoreCase(outputFile.getSheet().get(j))) {
					data = outputFile.getData().get(j);
					dateForSheet.add(data);
				}
			}
			dateForSheetSort = new ArrayList<>();
			for (int f = 0; f < listDataType.size(); f++) {
				for (int k = 0; k < dateForSheet.size(); k++) {
					if (listDataType.get(f).equalsIgnoreCase(outputFile.getDataType().get(k))) {
						dateForSheetSort.add(dateForSheet.get(k));
					}
				}
			}
			dataAllSheet.add(dateForSheetSort);
		}
		return dataAllSheet;
	}

	private static byte[] generateExcel(OutputFile outputFile, Workbook workbook)
			throws IcsTrazabilidadServiceException {

		List<String> listSheet = new ArrayList<>(new HashSet<>(outputFile.getSheet()));
		List<String> listDataType = new ArrayList<>(new HashSet<>(outputFile.getDataType()));
		List<List<String>> dataAllSheet = generarListaDatosHoja(outputFile, listSheet, listDataType);

		for (int x = 0; x < listSheet.size(); x++) {
			Sheet sheet = workbook.createSheet(listSheet.get(x));
			for (int i = 0; i < dataAllSheet.get(x).size(); i++) {
				Row row = sheet.createRow(i);
				String rowData = dataAllSheet.get(x).get(i);
				String[] cellData = rowData.split(DELIMITER_COMMAND);
				for (int j = 0; j < cellData.length; j++) {
					row.createCell(j).setCellValue(cellData[j]);
				}
			}
		}

		OutputStream stream = new ByteArrayOutputStream();
		try {
			workbook.write(stream);
		} catch (IOException e1) {
			throw new IcsTrazabilidadServiceException(ERROR_CERRAR_STREAM_CSV, e1);
		}

		try {
			LOG.debug("GENERADO CORRECTAMENTE EL ARCHIVO EXCEL");
			return stream.toString().getBytes(UTF8);
		} catch (UnsupportedEncodingException e) {
			throw new IcsTrazabilidadServiceException(ERROR_UTF8_NO_ENCONTRADO, e);
		} finally {
			try {
				stream.close();
			} catch (IOException e) {
				LOG.error(ERROR_CERRAR_STREAM_CSV, e);
			}
		}
	}

	private static byte[] generateCsv(OutputFile outputFile) throws IcsTrazabilidadServiceException {
		LOG.debug("GENERANDO EL ARCHIVO CSV");
		OutputStream stream = new ByteArrayOutputStream();
		PrintWriter writer = null;
		writer = new PrintWriter(stream);
		for (int i = 0; i < outputFile.getData().size(); i++) {
			writer.println(outputFile.getData().get(i) + DELIMITER_COMMAND);
		}
		writer.close();
		try {
			LOG.debug("GENERADO CORRECTAMENTE EL ARCHIVO CSV");
			return stream.toString().getBytes(UTF8);
		} catch (UnsupportedEncodingException e) {
			throw new IcsTrazabilidadServiceException(ERROR_UTF8_NO_ENCONTRADO, e);
		} finally {
			try {
				stream.close();
			} catch (IOException e) {
				LOG.error(ERROR_CERRAR_STREAM_CSV, e);
			}
		}
	}

	private static byte[] generateTxt(OutputFile outputFile) throws IcsTrazabilidadServiceException {
		LOG.debug("GENERANDO EL ARCHIVO TXT");
		OutputStream stream = new ByteArrayOutputStream();
		PrintWriter writer = null;
		writer = new PrintWriter(stream);
		for (int i = 0; i < outputFile.getData().size(); i++) {
			writer.println(outputFile.getData().get(i));
		}
		writer.close();
		try {
			LOG.debug("GENERADO CORRECTAMENTE EL ARCHIVO TXT");
			return stream.toString().getBytes(UTF8);
		} catch (UnsupportedEncodingException e) {
			throw new IcsTrazabilidadServiceException(ERROR_UTF8_NO_ENCONTRADO, e);
		} finally {
			try {
				stream.close();
			} catch (IOException e) {
				LOG.error(ERROR_CERRAR_STREAM_CSV, e);
			}
		}
	}

}
