package cl.cardif.ics.icsrest.exceptions.icsdetalleproceso;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.GONE)
public class IcsDetalleProcesoGoneException extends RuntimeException {

  private static final long serialVersionUID = -7209750264039464453L;

  public IcsDetalleProcesoGoneException(String exception) {
    super(exception);
  }

  public IcsDetalleProcesoGoneException(String exception, Throwable t) {
    super(exception, t);
  }
}
