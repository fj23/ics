package cl.cardif.ics.icsrest.domain.pojo;

import cl.cardif.ics.icsrest.domain.dto.input.BaseColumnaModel;

import java.math.BigDecimal;

public class DetalleSubEstructura extends BaseColumnaModel {
  private TipoDato tipoDato;
  private BigDecimal largo;
  private BigDecimal orden;
  private Long idSubestructuraPadre;
  private String vigencia;
  private String nombre;

  public DetalleSubEstructura() {
    // Constructor JavaBean
  }

  public long getIdSubestructuraPadre() {
    return idSubestructuraPadre;
  }

  public void setIdSubestructuraPadre(Long subestructuraPadre) {
    this.idSubestructuraPadre = subestructuraPadre;
  }

  public BigDecimal getLargo() {
    return largo;
  }

  public void setLargo(BigDecimal largo) {
    this.largo = largo;
  }

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public BigDecimal getOrden() {
    return orden;
  }

  public void setOrden(BigDecimal orden) {
    this.orden = orden;
  }

  public TipoDato getTipoDato() {
    return tipoDato;
  }

  public void setTipoDato(TipoDato tipoDato) {
    this.tipoDato = tipoDato;
  }

  public String getVigencia() {
    return vigencia;
  }

  public void setVigencia(String vigencia) {
    this.vigencia = vigencia;
  }
}
