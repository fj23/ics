package cl.cardif.ics.icsrest.util;

public final class Constants {

	/**
	 * Valor parametrico correspondiente al archivo estructurado en TipoExtension
	 */
	public static final long ID_TIPO_EXTENSION_ARCHIVO_ESTRUCTURADO = 300001;

	public static final long ID_TIPO_SALIDA_AUTOMATICA = 5000001;
	public static final long ID_TIPO_SALIDA_MANUAL = 5000002;
	public static final long ID_TIPO_SALIDA_SIN = 5000003;

	/** Valor parametrico del tipo de plantilla de entrada */
	public static final long ID_TIPO_PLANTILLA_ENTRADA = 100000;
	/** Valor parametrico del tipo de plantilla de integracion */
	public static final long ID_TIPO_PLANTILLA_INTEGRACION = 100001;
	/** Valor parametrico del tipo de plantilla de salida */
	public static final long ID_TIPO_PLANTILLA_SALIDA = 100002;
	/** Formato de fecha utilizado para enviar al Front End */
	public static final String DATE_DEFAULT_FRONTEND_FORMAT = "dd/MM/yyyy";

	/** Pagina por defecto para el listado en los mantenedores */
	public static final int DEFAULT_PAGE = 0;
	/**
	 * Cantidad de registros enviados al front por defecto en los listados de
	 * mantenedores
	 */
	public static final int DEFAULT_PAGE_SIZE = 10;
	/**
	 * Nombre del codigo dentro del mapa de la respuesta del Stored Procedure de
	 * creacion y actualizacion de plantillas
	 */
	public static final String DB_RESULT_CODE_NAME = "OUT_ERROR";

	
/* Stored Procedure de guardado de plantillas */
	
	public static final String PACKAGE_TEMPLATE_LOAD = "PCK_CARGA_PLANTILLAS";
	/**
	 * Nombre del procedure relacionado al Stored Procedure de creacion y
	 * actualizacion de plantillas
	 */
	public static final String PROCEDURE_TEMPLATE_CREATION = "PRC_CREACION_PLANTILLAS";
	public static final String PV_TEMPLATE_CREATION_NOMBRE = "PV_NOMBREPLANTILLA";
	public static final String PV_TEMPLATE_CREATION_OBSERVACIONES = "PV_OBSERVACIONES";
	public static final String PN_TEMPLATE_CREATION_VERSION = "PN_VERSION";
	public static final String PN_TEMPLATE_CREATION_IDTIPOPLANTILLA = "PN_IDTIPOPLANTILLA";
	public static final String PN_TEMPLATE_CREATION_IDSOCIO = "PN_IDSOCIO";
	public static final String PN_TEMPLATE_CREATION_IDEVENTO = "PN_IDEVENTO";
	public static final String PN_TEMPLATE_CREATION_IDCORE = "PN_IDCORE";
	public static final String PN_TEMPLATE_CREATION_IDTIPOEXTENSION = "PN_IDTIPOEXTENSION";
	public static final String PN_TEMPLATE_CREATION_IDTIPOSALIDA = "PN_IDTIPOSALIDA";
	public static final String PV_TEMPLATE_CREATION_DELIMITADOR = "PV_DELIMITADOR";
	public static final String PN_TEMPLATE_CREATION_IDVERSIONANTERIOR = "PN_IDVERSIONANTERIOR";
	public static final String PV_TEMPLATE_CREATION_USUARIO = "PV_USUARIO";
	public static final String PV_TEMPLATE_CREATION_UNIONARCHIVOS = "PV_UNIONARCHIVOS";
	public static final String PV_TEMPLATE_CREATION_DIRECTORIOSALIDA = "PV_DIRECTORIOSALIDA";

	/**
	 * Descripcion o mensaje de la respuesta del Stored Procedure de creacion y
	 * actualizacion de plantillas
	 */
	public static final String DB_RESULT_DESCRIPTION_NAME = "OUT_DESCERROR";
	/** Descripcion o mensaje de la respuesta del Stored Procedure por defecto */
	public static final String DB_RESULT_DESCRIPTION_DEFAULT = "No message";
	/** codigo de respuesta por defecto del Stored Procedure */
	public static final Long DB_RESULT_CODE_DEFAULT = (long) -3;
	/** codigo de respuesta correcta del Stored Procedure */
	public static final Long DB_RESULT_CODE_OK = (long) -2;
	

	
/* Package con utilidades */
	

	public static final String PACKAGE_UTIL_ICS = "PCK_UTIL_ICS";
	public static final String PACKAGE_UTIL_APP = "PCK_UTIL_APP";
	
	public static final String FUNCTION_FETCH_BUSINESS_ID = "FNC_ASSIGNSBUSINESSID";
	public static final String PV_USER_ATTRIBUTE_ID = "IN_ID_ATRIBUTO_USER";

	public static final String PROCEDURE_VALIDAR_ARCHIVO = "PRC_VALIDA_NOMBRE_ARCHIVO";
	public static final String PV_VALIDAR_ARCHIVO_PROCESO_ID = "PN_IDPROCESO";
	public static final String PV_VALIDAR_ARCHIVO_NOMBRE = "PV_NOMARCHIVO";
	public static final String PV_VALIDAR_ARCHIVO_EXTENSION = "PV_EXTENSION";

	public static final String PROCEDURE_GET_ATRIBUTOS_USUARIO = "PRC_ATRIBUTO_USUARIO_MAP";
	public static final String PV_GET_ATRIBUTOS_USUARIO_PLANTILLA = "PN_IDPLANTILLA";
	public static final String PV_GET_ATRIBUTOS_USUARIO_OUT = "OUT_CURSOR";
	
	public static final String PROCEDURE_VALID_HOMO_TYPE = "PRC_ICS_VALIDA_TIPO_HOMO";
	public static final String PV_VALID_HOMO_TYPE_SISTEMA1 = "IN_SISTEMA1";
	public static final String PV_VALID_HOMO_TYPE_SISTEMA2 = "IN_SISTEMA2";
	public static final String PV_VALID_HOMO_TYPE_SOCIO1 = "IN_EMPRESA1";
	public static final String PV_VALID_HOMO_TYPE_SOCIO2 = "IN_EMPRESA2";
	public static final String PV_VALID_HOMO_TYPE_NOMBRE_TIPO = "IN_CONCEPTO";
	public static final String PV_VALID_HOMO_TYPE_TIPO_VALIDACION = "IN_TIPOEXEC";
	public static final String PV_VALID_HOMO_TYPE_OUT_ID_TIPO = "OUT_ARR_TIPO";

	public static final String PROCEDURE_EXISTEN_PROCESOS_ANETO = "PRC_BUSCA_PROCESOS_ANETO";
	public static final String PV_EXISTEN_PROCESOS_ANETO_OUT = "OUT_ERROR";
	public static final String PV_EXISTEN_PROCESOS_ANETO_OUT_DESC = "OUT_DESCERROR";

	/**
	 * Version enviada dentro del usuario al momento de hacer login para mostrarla
	 * en el front end
	 */
	public static final String VERSION_BACKEND = "1.4.4";
	
	public static final String TRABAJO_FORMULA = "F1";
	public static final String TRABAJO_FORMATO = "F2";
	public static final String TRABAJO_HOMOLOGACION = "H0";
	

	private Constants() {
	}
}
