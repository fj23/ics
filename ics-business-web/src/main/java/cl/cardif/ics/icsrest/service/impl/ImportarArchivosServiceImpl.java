package cl.cardif.ics.icsrest.service.impl;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.OrderSpecifier;

import cl.cardif.ics.domain.entity.carga.IcsArchivoImportado;
import cl.cardif.ics.domain.entity.carga.IcsGeneracionArchivo;
import cl.cardif.ics.domain.entity.carga.IcsResumenImportacion;
import cl.cardif.ics.domain.entity.carga.QIcsGeneracionArchivo;
import cl.cardif.ics.domain.entity.carga.QIcsResumenImportacion;
import cl.cardif.ics.domain.entity.common.IcsEvento;
import cl.cardif.ics.domain.entity.common.QIcsEvento;
import cl.cardif.ics.domain.entity.plantillas.IcsPlantilla;
import cl.cardif.ics.domain.entity.plantillas.QIcsPlantilla;
import cl.cardif.ics.domain.entity.procesos.IcsDetalleProceso;
import cl.cardif.ics.domain.entity.procesos.IcsResumenTransaccion;
import cl.cardif.ics.domain.entity.procesos.QIcsDetalleProceso;
import cl.cardif.ics.domain.entity.procesos.QIcsResumenTransaccion;
import cl.cardif.ics.icsgeneratefile.controller.GenerateController;
import cl.cardif.ics.icsgeneratefile.entity.FilesInformation;
import cl.cardif.ics.icsrest.domain.dto.output.RequestResultOutput;
import cl.cardif.ics.icsrest.exceptions.icsimportaarchivo.IcsImportaArchivoServiceException;
import cl.cardif.ics.icsrest.repository.ArchivoImportadoRepository;
import cl.cardif.ics.icsrest.repository.DetallesProcesoRepository;
import cl.cardif.ics.icsrest.repository.EventosRepository;
import cl.cardif.ics.icsrest.repository.GeneracionArchivoRepository;
import cl.cardif.ics.icsrest.repository.PlantillaRepository;
import cl.cardif.ics.icsrest.repository.ResumenImportacionRepository;
import cl.cardif.ics.icsrest.repository.ResumenTransaccionRepository;
import cl.cardif.ics.icsrest.service.ImportarArchivosService;
import cl.cardif.ics.icsrest.util.OutputListWrapper;

@Service
public class ImportarArchivosServiceImpl implements ImportarArchivosService {
	private static final Logger LOG = LoggerFactory.getLogger(ImportarArchivosServiceImpl.class);

	private JdbcTemplate jdbcTemplate;

	private static final String SORT_NOMBRE_ARCHIVO = "nombreArchivo";
	private static final String SORT_PROCESO_CARGA = "procesoCarga";
	private static final String SORT_FECHA_CARGA = "id.fechaCarga";
	private static final String SORT_FECHA_PROCESADO = "fechaProceso";
	private static final String SORT_REGISTROS_ACTUALIZADOS = "cantidadRegistros";

	@Autowired private GenerateController generateController;

	@Autowired private DetallesProcesoRepository detallesProcesoRepository;
	@Autowired private PlantillaRepository plantillaRepository;
	@Autowired private EventosRepository eventosRepository;
	@Autowired private ResumenTransaccionRepository resumenTransaccionRepository;
	@Autowired private GeneracionArchivoRepository generacionArchivoRepository;
	@Autowired private ArchivoImportadoRepository archivoImportadoRepository;
	@Autowired private ResumenImportacionRepository resumenImportacionRepository;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	List<String> REQUIRED_HEADERS = new ArrayList<>(
			Arrays.asList("ID_TRANSACCION", "ID_REGISTRO", "ESTADO_REGISTRO", "OBSERVACION_REGISTRO"));

	@Override
	public List<IcsResumenTransaccion> getTransacciones(Long idProceso) {
		QIcsResumenTransaccion qIcsResumenTransaccion = QIcsResumenTransaccion.icsResumenTransaccion;

		BooleanBuilder builder = new BooleanBuilder()
			.and(qIcsResumenTransaccion.icsProceso.idProceso.eq(idProceso))
			.and(qIcsResumenTransaccion.estadoDeTransaccion.eq("Pendiente core"));

		OrderSpecifier<Long> orderSpecifier = qIcsResumenTransaccion.idTransaccion.asc();

		Iterable<IcsResumenTransaccion> iterableTransaccions = this.resumenTransaccionRepository.findAll(builder, orderSpecifier);

		List<IcsResumenTransaccion> transaccions = new ArrayList<>();
		for (IcsResumenTransaccion icsResumenTransaccion : iterableTransaccions) {
			transaccions.add(icsResumenTransaccion);
		}

		return transaccions;
	}

	@Override
	public List<IcsEvento> getEventos(Long idProceso) {
		QIcsDetalleProceso qIcsDetalleProceso = QIcsDetalleProceso.icsDetalleProceso;
		QIcsPlantilla qIcsPlantilla = QIcsPlantilla.icsPlantilla;
		QIcsEvento qIcsEvento = QIcsEvento.icsEvento;
		
		BooleanBuilder bb = new BooleanBuilder()
				.and(qIcsDetalleProceso.icsProceso.idProceso.eq(idProceso));
		Iterable<IcsDetalleProceso> iteraDetallesProcesos = this.detallesProcesoRepository.findAll(bb);
		
		List<Long> plantillasId = new ArrayList<>();
		for (IcsDetalleProceso icsDetalleProceso : iteraDetallesProcesos) {
			Long plantillaId = icsDetalleProceso.getIcsPlantilla().getIdPlantilla();
			plantillasId.add(plantillaId);
		}
		
		
		
		bb = new BooleanBuilder()
			.and(qIcsPlantilla.idPlantilla.in(plantillasId))
			.and(qIcsPlantilla.vigencia.eq("Y"));
		OrderSpecifier<String> orderEventoSpecifier = qIcsEvento.nombreEvento.asc();
		Iterable<IcsPlantilla> iteraPlantillas = this.plantillaRepository.findAll(bb);
		
		List<Long> eventosId = new ArrayList<>();
		for (IcsPlantilla icsPlantilla : iteraPlantillas) {
			Long idEvento = icsPlantilla.getIcsEvento().getIdEvento();
			eventosId.add(idEvento);
		}
		
		

		bb = new BooleanBuilder()
			.and(qIcsEvento.idEvento.in(eventosId))
			.and(qIcsEvento.vigenciaEvento.eq("Y"));
		Iterable<IcsEvento> iteraEventos = this.eventosRepository.findAll(bb, orderEventoSpecifier);
		
		List<IcsEvento> listEventos = new ArrayList<>();
		for (IcsEvento icsEvento : iteraEventos) {
			listEventos.add(icsEvento);
		}

		return listEventos;
	}

	@Override
	public List<IcsResumenTransaccion> getArchivosTransaccion(Map<String, String> filters) {
		QIcsResumenTransaccion qIcsResumenTransaccion = QIcsResumenTransaccion.icsResumenTransaccion;
		OrderSpecifier<String> orderSpecifier = qIcsResumenTransaccion.nombreArchivo.asc();

		Iterable<IcsResumenTransaccion> resumenTransaccions = this.resumenTransaccionRepository
				.findAll(this.getPredicate(filters), orderSpecifier);
		List<IcsResumenTransaccion> transaccions = new ArrayList<>();
		for (IcsResumenTransaccion icsResumenTransaccion : resumenTransaccions) {
			transaccions.add(icsResumenTransaccion);
		}
		
		return transaccions;
	}

	private BooleanBuilder getPredicate(Map<String, String> filters) {
		QIcsResumenTransaccion qIcsResumenTransaccion = QIcsResumenTransaccion.icsResumenTransaccion;

		BooleanBuilder predicate = new BooleanBuilder()
				.and(qIcsResumenTransaccion.estadoDeTransaccion.eq("Pendiente core"));


		if (filters.containsKey("idSocio")) {
			predicate.and(qIcsResumenTransaccion.icsSocio.idSocio.eq(Long.valueOf(filters.get("idSocio"))));
		}

		if (filters.containsKey("idProceso")) {
			predicate.and(qIcsResumenTransaccion.icsProceso.idProceso.eq(Long.valueOf(filters.get("idProceso"))));
		}

		if (filters.containsKey("idTransaccion")) {
			predicate.and(qIcsResumenTransaccion.idTransaccion.eq(Long.valueOf(filters.get("idTransaccion"))));
		}

		if (filters.containsKey("idEvento")) {
			predicate.and(qIcsResumenTransaccion.icsEvento.idEvento.eq(Long.valueOf(filters.get("idEvento"))));
		}

		try {
			if (filters.containsKey("fechaInicio")) {
				predicate.and(
						qIcsResumenTransaccion.fechaInicio.after(this.convertToStartDate(filters.get("fechaInicio"))));
			}
		} catch (ParseException e) {
			LOG.error("Error al filtrar por fecha inicio", e);
		}

		return predicate;
	}

	private Date convertToStartDate(String time) throws ParseException {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(Long.valueOf(time));
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}

	@Override
	public List<IcsGeneracionArchivo> descargarArchivo(Long idTransaccion) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("IN_ID_TRANSAC", idTransaccion);
		LOG.info("\n\nLlamada a PCK_UTIL_ICS.PRC_ICS_IMPORTA_ESTADO \n" + idTransaccion + "\n");
		RequestResultOutput result = null;
		SimpleJdbcCall call = new SimpleJdbcCall(jdbcTemplate).withCatalogName("PCK_UTIL_ICS")
				.withProcedureName("PRC_ICS_IMPORTA_ESTADO")
				.declareParameters(new SqlParameter("IN_ID_TRANSAC", oracle.jdbc.OracleTypes.NUMBER));

		Map<String, Object> execute = call.execute(new MapSqlParameterSource(map));
		result = new RequestResultOutput(execute);
		if (result.getCode() == 1) {
			throw new IcsImportaArchivoServiceException(result.getMessage());
		}

		QIcsGeneracionArchivo qIcsGeneracionArchivo = QIcsGeneracionArchivo.icsGeneracionArchivo;
		BooleanBuilder bb = new BooleanBuilder()
				.and(qIcsGeneracionArchivo.idTransaccion.eq(idTransaccion))
				.and(qIcsGeneracionArchivo.tipoArchivo.eq(0L));
		
		OrderSpecifier<String> orderByHojaDestino = qIcsGeneracionArchivo.hojaDestino.asc();
		OrderSpecifier<String> orderByTipoDato = qIcsGeneracionArchivo.tipoDato.asc();

		Iterable<IcsGeneracionArchivo> iteraRegistrosArchivos = this.generacionArchivoRepository.findAll(bb, orderByHojaDestino, orderByTipoDato);

		List<IcsGeneracionArchivo> generacionArchivos = new ArrayList<>();
		for (IcsGeneracionArchivo icsGeneracionArchivo : iteraRegistrosArchivos) {
			generacionArchivos.add(icsGeneracionArchivo);
		}

		return generacionArchivos;
	}

	@Override
	@Transactional(rollbackFor = IcsImportaArchivoServiceException.class)
	public List<IcsArchivoImportado> cargarArchivo(MultipartFile file, Long idTransaccion)
			throws IcsImportaArchivoServiceException {
		Workbook workbook;
		try {
			workbook = WorkbookFactory.create(file.getInputStream());
		} catch (EncryptedDocumentException e) {
			throw new IcsImportaArchivoServiceException("El archivo se encuentra encriptado, error en la lectura", e);
		} catch (IOException e) {
			throw new IcsImportaArchivoServiceException("Error en la lectura del archivo", e);
		}

		Iterator<Sheet> sheetIterator = workbook.sheetIterator();
		LOG.debug("Obteniendo hojas usando Iterator (java.util)");
		List<IcsArchivoImportado> archivoImportados = new ArrayList<>();
		int s = 0;

		DataFormatter dataFormatter = new DataFormatter();

		try {
			while (sheetIterator.hasNext()) {

				s++;
				Sheet sheet = sheetIterator.next();
				LOG.debug("=> " + sheet.getSheetName());
				LOG.debug("\n\nIterando lineas y columnas usando Iterator (java.util)\n");

				if (sheet.getPhysicalNumberOfRows() > 1) {

					int noColumns = 0;

					try {
						noColumns = sheet.getRow(0).getPhysicalNumberOfCells();
					} catch (Exception e) {
						throw new IcsImportaArchivoServiceException(
								"Error al obtener la cantidad de columnas de la hoja " + sheet.getSheetName()
										+ ", la cabezera no debe estar vacía.",
								e);
					}

					if (noColumns < 4) {
						throw new IcsImportaArchivoServiceException(
								"La cantidad de columnas de la hoja " + sheet.getSheetName() + " debe ser al menos 4");
					}

					for (int ci = 1; ci <= 4; ci++) {
						String cellHeaderName = null;
						String headerRequired = REQUIRED_HEADERS.get(REQUIRED_HEADERS.size() - ci);
						try {
							cellHeaderName = sheet.getRow(0).getCell(noColumns - ci).getStringCellValue().toUpperCase();
						} catch (Exception e) {
							throw new IcsImportaArchivoServiceException("No se encuentra la columna " + headerRequired
									+ "de la hoja " + sheet.getSheetName(), e);
						}
						if (!headerRequired.equals(cellHeaderName))
							throw new IcsImportaArchivoServiceException("No se encuentra la columna " + headerRequired
									+ " en la posición correspondiente de la hoja " + sheet.getSheetName());
					}

					int l = 0;
					Iterator<Row> rowIterator = sheet.rowIterator();
					while (rowIterator.hasNext()) {
						Row row = rowIterator.next();
						IcsArchivoImportado archivoImportado = new IcsArchivoImportado();
						if (l > 0) {
							List<String> cells = new ArrayList<>();
							for (int ci = 0; ci < noColumns; ci++) {
								String requiredCell = sheet.getRow(0).getCell(ci).getStringCellValue();
								String cellAux = "";
								if (row.getCell(ci) != null) {
									cellAux = dataFormatter.formatCellValue(row.getCell(ci));
								}
								if (cellAux.isEmpty() && REQUIRED_HEADERS.contains(requiredCell.toUpperCase())
										&& !requiredCell.toUpperCase().equals("OBSERVACION_REGISTRO"))
									throw new IcsImportaArchivoServiceException(
											"El archivo contiene una celda vacia en la columna "
													+ requiredCell.toUpperCase() + ". Hoja: " + sheet.getSheetName()
													+ ", Linea: " + (l + 1));
								else
									cells.add(cellAux);
							}
							archivoImportado.setLineaArchivo(String.join(";", cells));
							archivoImportado.setLinea(l);
							archivoImportado.setHoja(s);
							archivoImportado.setIdTransaccion(idTransaccion);
							archivoImportados.add(archivoImportado);
						}
						l++;
					}
				} else {
					LOG.info("La hoja " + sheet.getSheetName() + " no contiene registros, será omitida");
				}

			}
		} catch (Exception e) {
			throw new IcsImportaArchivoServiceException("Error en el proceso de lectura del archivo", e);
		}

		if (archivoImportados.size() == 0) {
			throw new IcsImportaArchivoServiceException("El archivo no posee registros que guardar.");
		}

		try {
			this.archivoImportadoRepository.save(archivoImportados);
			this.archivoImportadoRepository.flush();
		} catch (Exception e) {
			throw new IcsImportaArchivoServiceException("Error al guardar el archivo", e);
		}

		try {
			workbook.close();
		} catch (IOException e) {
			LOG.warn("No se pudo cerrar el archivo", e);
		}

		return archivoImportados;

	}

	@Override
	public void actualizaImportacion(Long idTransaction) {
		String pkg = "PCK_UTIL_ICS";
		String prc = "PRC_ICS_ACTUALIZA_IMPORTACION";

		try {
			Map<String, Object> map = new HashMap<>();
			map.put("IN_ID_TRANSACCION", idTransaction);
			LOG.info("\n\nLlamada a {}.{} \n {} \n", pkg, prc, idTransaction);
			RequestResultOutput result = null;
			SimpleJdbcCall call = new SimpleJdbcCall(jdbcTemplate).withCatalogName(pkg).withProcedureName(prc)
					.declareParameters(new SqlParameter("IN_ID_TRANSACCION", oracle.jdbc.OracleTypes.NUMBER));

			Map<String, Object> execute = call.execute(new MapSqlParameterSource(map));
			result = new RequestResultOutput(execute);
			if (result.getCode() != 0) {
				LOG.error(result.getMessage());
				throw new IcsImportaArchivoServiceException(result.getMessage());
			}
		} catch (Exception e) {
			LOG.error("Error en la ejecución del SP: ", e);
			throw new IcsImportaArchivoServiceException("Error en la ejecución del SP");
		}
	}

	@Override
	public byte[] generateFile(String idTransaction, FilesInformation file) {
		return this.generateController.getFile(idTransaction, file);
	}

	@Override
	public void deleteGeneratedFile(Long idTransaction, Long tipoArchivo) {
		this.generateController.deleteInformation(idTransaction, tipoArchivo);
	}

	@Override
	@Transactional(noRollbackFor = Exception.class)
	public void eliminaImportacion(List<IcsArchivoImportado> archivoImportados) {
		try {
			this.archivoImportadoRepository.deleteInBatch(archivoImportados);
			this.archivoImportadoRepository.flush();
		} catch (Exception e) {
			LOG.error(
					"Error al limpiar los registros de importacion por un error en PCK_UTIL_ICS.PRC_ICS_ACTUALIZA_IMPORTACION.\nPuede que no todos los registros fueran eliminados.",
					e);
		}
	}

	@Override
	public OutputListWrapper<IcsResumenImportacion> getHistorialArchivos(Long idTransaccion, int page, int size,
			String sortColumn, String sortOrder) {
		LOG.info("Obteniendo Resumenes de Transacciones");

		Pageable paginador;

		if ("".equals(sortColumn)) {
			paginador = new PageRequest(page, size);
		} else {
			Sort sorter = buildSort(sortColumn, sortOrder);
			paginador = new PageRequest(page, size, sorter);
		}

		List<IcsResumenImportacion> resumenTransaccions = resumenImportacionRepository
				.findAll(this.getPredicate(idTransaccion), paginador).getContent();
		int countResumenTransaccions = this.resumenImportacionRepository.count(idTransaccion);

		LOG.info("Transacciones obtenidas : " + resumenTransaccions.size());

		return new OutputListWrapper<>(resumenTransaccions, countResumenTransaccions, "OK");
	}

	private Sort buildSort(String sortColumn, String sortOrder) {

		String entitySortField = null;

		if (sortColumn.equals("nombreArchivo"))
			entitySortField = SORT_NOMBRE_ARCHIVO;
		else if (sortColumn.equals("procesoCarga"))
			entitySortField = SORT_PROCESO_CARGA;
		else if (sortColumn.equals("fechaCarga"))
			entitySortField = SORT_FECHA_CARGA;
		else if (sortColumn.equals("fechaProcesado"))
			entitySortField = SORT_FECHA_PROCESADO;
		else if (sortColumn.equals("registrosActualizados"))
			entitySortField = SORT_REGISTROS_ACTUALIZADOS;

		return new Sort(Sort.Direction.fromString(sortOrder), entitySortField);
	}

	private BooleanBuilder getPredicate(Long idTransaccion) {
		QIcsResumenImportacion qIcsResumenImportacion = QIcsResumenImportacion.icsResumenImportacion;
		
		BooleanBuilder predicate = new BooleanBuilder()
			.and(qIcsResumenImportacion.id.idTransaccion.eq(idTransaccion))
			.and(qIcsResumenImportacion.id.fechaCarga.isNotNull());
		
		return predicate;
	}

	@Override
	public List<IcsResumenImportacion> exportResumenImportacion(Long idTransaccion) {
		Iterable<IcsResumenImportacion> resumenTransaccions = resumenImportacionRepository
				.findAll(this.getPredicate(idTransaccion));
		
		List<IcsResumenImportacion> listImportacions = new ArrayList<>();
		for (IcsResumenImportacion icsResumenImportacion : resumenTransaccions) {
			listImportacions.add(icsResumenImportacion);
		}
		
		return listImportacions;
	}

}
