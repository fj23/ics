package cl.cardif.ics.icsrest.service;

import cl.cardif.ics.icsrest.domain.dto.input.CargaArchivoInput;
import cl.cardif.ics.icsrest.domain.dto.output.RequestResultOutput;
import cl.cardif.ics.icsrest.util.Permisos;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.multipart.MultipartFile;

public interface CargaArchivoService {

	@PreAuthorize(value = "hasAnyAuthority("
	          + Permisos.Carga_Arch_Alta + ","
	          + Permisos.Carga_Arch_Reca + ","
	          + Permisos.Carga_Arch_Canc + ","
	          + Permisos.Carga_Arch_Prosp + ","
	          + Permisos.Carga_Arch_Intera + ","
	          + Permisos.Carga_Arch_Pago + ")")
	CargaArchivoInput createRowWithFile(MultipartFile file);
	
	@PreAuthorize(value = "hasAnyAuthority("
	          + Permisos.Carga_Arch_Alta + ","
	          + Permisos.Carga_Arch_Reca + ","
	          + Permisos.Carga_Arch_Canc + ","
	          + Permisos.Carga_Arch_Prosp + ","
	          + Permisos.Carga_Arch_Intera + ","
	          + Permisos.Carga_Arch_Pago + ")")
	void nuevo(CargaArchivoInput input, boolean isUpdate);
	
	@PreAuthorize(value = "hasAnyAuthority("
	          + Permisos.Carga_Arch_Alta + ","
	          + Permisos.Carga_Arch_Reca + ","
	          + Permisos.Carga_Arch_Canc + ","
	          + Permisos.Carga_Arch_Prosp + ","
	          + Permisos.Carga_Arch_Intera + ","
	          + Permisos.Carga_Arch_Pago + ")")
	RequestResultOutput verifyFileNameWithSp(CargaArchivoInput archivo);
}
