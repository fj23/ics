package cl.cardif.ics.icsrest.domain.dto.output;

import java.util.ArrayList;
import java.util.List;

import cl.cardif.ics.icsrest.domain.dto.input.MapeoEntradaInput;
import cl.cardif.ics.icsrest.domain.pojo.Archivo;

public class PlantillaEntradaOutput extends PlantillaOutput {
	private List<Archivo> archivos;
	private List<MapeoEntradaInput> mapeosEntrada;

	public PlantillaEntradaOutput() {
		super();
	}

	public List<Archivo> getArchivos() {
		if (this.archivos != null) {
			return new ArrayList<>(this.archivos);
		} else {
			return new ArrayList<>();
		}
	}

	public void setArchivos(List<Archivo> archivos) {
		if (archivos != null) {
			this.archivos = new ArrayList<>(archivos);
		}
	}

	public List<MapeoEntradaInput> getMapeosEntrada() {
		if (mapeosEntrada != null) {
			return new ArrayList<>(mapeosEntrada);
		}
		return new ArrayList<>();
	}

	public void setMapeosEntrada(List<MapeoEntradaInput> mapeosEntrada) {
		if (mapeosEntrada != null) {
			this.mapeosEntrada = new ArrayList<>(mapeosEntrada);
		}
	}

	@Override
	public String toString() {
		return "PlantillaEntradaOutput [archivos=" + archivos + ", mapeosEntrada=" + mapeosEntrada + ", toString()=" + super.toString()
			+ "]";
	}
}
