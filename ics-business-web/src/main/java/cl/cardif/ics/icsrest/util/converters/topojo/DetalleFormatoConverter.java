package cl.cardif.ics.icsrest.util.converters.topojo;

import cl.cardif.ics.domain.entity.formatos.IcsDetalleFormato;
import cl.cardif.ics.icsrest.domain.pojo.DetalleFormato;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class DetalleFormatoConverter implements Converter<IcsDetalleFormato, DetalleFormato> {

  @Override
  public DetalleFormato convert(IcsDetalleFormato entity) {
    DetalleFormato pojo = new DetalleFormato();
    pojo.setId(entity.getIdDetalleFormato());
    pojo.setNombre(entity.getNombreDetalleFormato());
    pojo.setTipo(entity.getIcsTipoFormato().getNombreFormato());
    return pojo;
  }
}
