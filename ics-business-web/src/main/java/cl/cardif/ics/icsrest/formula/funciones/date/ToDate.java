package cl.cardif.ics.icsrest.formula.funciones.date;

import java.util.ArrayList;
import java.util.Map;

import cl.cardif.ics.icsrest.formula.FormulaParent;
import cl.cardif.ics.icsrest.formula.IStringFactor;
import cl.cardif.ics.icsrest.util.FormulaOrigenKey;

public class ToDate extends FormulaParent implements IStringFactor {
	public ToDate() {
		hijos = new ArrayList<>();
	}

	@Override
	public int getMaximumChildrenCount() {
		return 2;
	}

	@Override
	public String toSafeSQL(Map<FormulaOrigenKey, Integer> tableAliases) {
		StringBuilder sb = new StringBuilder()
			.append("TO_DATE")
			.append("(")
			.append(getHijo(0).toSafeSQL(tableAliases)).append(",")
			.append(getHijo(1).toSafeSQL(tableAliases))
			.append(")");

		return "(" + sb.toString() + ")";
	}

	@Override
	public String toSQL(Map<FormulaOrigenKey, Integer> tableAliases) {
		StringBuilder sb = new StringBuilder()
			.append("TO_DATE")
			.append("(")
			.append(getHijo(0).toSQL(tableAliases)).append(",")
			.append(getHijo(1).toSQL(tableAliases))
			.append(")");

		return "(" + sb.toString() + ")";
	}
}
