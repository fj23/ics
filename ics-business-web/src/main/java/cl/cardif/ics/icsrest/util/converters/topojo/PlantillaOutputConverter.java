package cl.cardif.ics.icsrest.util.converters.topojo;

import java.text.SimpleDateFormat;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import cl.cardif.ics.domain.entity.common.IcsCore;
import cl.cardif.ics.domain.entity.common.IcsEvento;
import cl.cardif.ics.domain.entity.common.IcsSocio;
import cl.cardif.ics.domain.entity.common.IcsTipoSalida;
import cl.cardif.ics.domain.entity.plantillas.IcsPlantilla;
import cl.cardif.ics.domain.entity.plantillas.IcsTipoPlantilla;
import cl.cardif.ics.icsrest.domain.dto.output.PlantillaOutput;
import cl.cardif.ics.icsrest.domain.pojo.Core;
import cl.cardif.ics.icsrest.domain.pojo.Evento;
import cl.cardif.ics.icsrest.domain.pojo.Socio;
import cl.cardif.ics.icsrest.domain.pojo.TipoPlantilla;
import cl.cardif.ics.icsrest.domain.pojo.TipoSalida;
import cl.cardif.ics.icsrest.util.Constants;

@Component
public class PlantillaOutputConverter implements Converter<IcsPlantilla, PlantillaOutput> {

	@Override
	public PlantillaOutput convert(IcsPlantilla entity) {
		PlantillaOutput output = new PlantillaOutput();

		output.setId(entity.getIdPlantilla());
		output.setNombre(entity.getNombrePlantilla());
		output.setObservaciones(entity.getObservacionPlantilla());
		output.setPlantillaMultiarchivo(entity.getPlantillaMultiarchivo());
		output.setVersion(entity.getVersionPlantilla());
		output.setVigencia(entity.getVigencia());
		output.setCodUsuario(entity.getCodUsuario());
		
		SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_DEFAULT_FRONTEND_FORMAT);
		output.setFechaCreacion(sdf.format(entity.getFechaCreacion()));

		this.setTipoPlantilla(entity, output);
		this.setTipoSalida(entity, output);
		this.setCore(entity, output);
		this.setEvento(entity, output);
		this.setSocio(entity, output);
		this.setSocioDestino(entity, output);

		return output;
	}

	private void setTipoPlantilla(IcsPlantilla entity, PlantillaOutput output) {
		IcsTipoPlantilla icsTipoPlantilla = entity.getIcsTipoPlantilla();
		if (icsTipoPlantilla != null) {
			TipoPlantilla tipoPlantilla = new TipoPlantilla();
			tipoPlantilla.setId(icsTipoPlantilla.getIdTipoPlantilla());
			tipoPlantilla.setNombre(icsTipoPlantilla.getNombrePlantilla());
			tipoPlantilla.setVigencia(icsTipoPlantilla.getVigenciaPlantilla());
			output.setTipo(tipoPlantilla);
		}
	}
	
	private void setTipoSalida(IcsPlantilla entity, PlantillaOutput output) {
		IcsTipoSalida icsTipoSalida = entity.getIcsTipoSalida();
		if (icsTipoSalida != null) {
			TipoSalida tipoSalida = new TipoSalida();
			tipoSalida.setIdTipoSalida(icsTipoSalida.getIdTipoSalida());
			tipoSalida.setDescripcion(icsTipoSalida.getDescripcion());
			tipoSalida.setVigencia(icsTipoSalida.getVigencia());
			output.setTipoSalida(tipoSalida);
		}
	}

	private void setCore(IcsPlantilla entity, PlantillaOutput output) {

		IcsCore icsCore = entity.getIcsCore();
		if (icsCore != null) {
			Core core = new Core();
			core.setId(icsCore.getIdCore());
			core.setNombre(icsCore.getNombreCore());
			core.setFlgVisible(icsCore.getFlgVisible());
			core.setVigencia(icsCore.getVigenciaCore());
			output.setCore(core);
		}

	}

	private void setEvento(IcsPlantilla entity, PlantillaOutput output) {
		IcsEvento icsEvento = entity.getIcsEvento();
		if (icsEvento != null) {
			Evento evento = new Evento();
			evento.setId(icsEvento.getIdEvento());
			evento.setNombre(icsEvento.getNombreEvento());
			evento.setVigencia(icsEvento.getVigenciaEvento());
			output.setEvento(evento);
		}
	}

	private void setSocio(IcsPlantilla entity, PlantillaOutput output) {
		IcsSocio icsSocio = entity.getIcsSocio();
		if (icsSocio != null) {
			Socio socio = new Socio();
			socio.setId(icsSocio.getIdSocio());
			socio.setNombre(icsSocio.getNombreSocio());
			socio.setVigencia(icsSocio.getVigenciaSocio());
			output.setSocio(socio);
		}
	}

	private void setSocioDestino(IcsPlantilla entity, PlantillaOutput output) {
		IcsSocio icsSocio = entity.getIcsSocioDestino();
		if (icsSocio != null) {
			Socio socio = new Socio();
			socio.setId(icsSocio.getIdSocio());
			socio.setNombre(icsSocio.getNombreSocio());
			socio.setVigencia(icsSocio.getVigenciaSocio());
			output.setSocioDestino(socio);
		}
	}
}
