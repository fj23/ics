package cl.cardif.ics.icsrest.domain.dto.input;

public class DetalleProcesoInput {
  private long id;
  private long idProceso;
  private long idPlantilla;

  public DetalleProcesoInput() {
    super();
  }

  public DetalleProcesoInput(long id, long idProceso, long idPlantilla) {
    super();
    this.id = id;
    this.idProceso = idProceso;
    this.idPlantilla = idPlantilla;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public long getIdPlantilla() {
    return idPlantilla;
  }

  public void setIdPlantilla(long idPlantilla) {
    this.idPlantilla = idPlantilla;
  }

  public long getIdProceso() {
    return idProceso;
  }

  public void setIdProceso(long idProceso) {
    this.idProceso = idProceso;
  }
}
