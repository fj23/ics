package cl.cardif.ics.icsrest.exceptions.icsdetallehomologacion;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.GONE)
public class IcsDetalleHomologacionGoneException extends RuntimeException {

  private static final long serialVersionUID = -7209750264039464453L;

  public IcsDetalleHomologacionGoneException(String exception) {
    super(exception);
  }

  public IcsDetalleHomologacionGoneException(String exception, Throwable t) {
    super(exception, t);
  }
}
