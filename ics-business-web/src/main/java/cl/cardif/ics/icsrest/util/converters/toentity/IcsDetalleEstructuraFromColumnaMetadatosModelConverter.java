package cl.cardif.ics.icsrest.util.converters.toentity;

import java.math.BigDecimal;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import cl.cardif.ics.domain.entity.estructuras.IcsDetalleEstructura;
import cl.cardif.ics.icsrest.domain.dto.input.ColumnaMetadatosModel;

@Component
public class IcsDetalleEstructuraFromColumnaMetadatosModelConverter
		implements Converter<ColumnaMetadatosModel, IcsDetalleEstructura> {

	@Override
	public IcsDetalleEstructura convert(ColumnaMetadatosModel pojo) {

		IcsDetalleEstructura detalleEntity = new IcsDetalleEstructura();

		if (pojo.getId() != null && pojo.getId() > 0) {
			detalleEntity.setIdDetalleEstructura(pojo.getId());
		}
		detalleEntity.setCodigoEstructura(pojo.getCodigo());
		detalleEntity.setDescripcionDetalleEstructura(pojo.getDescripcion());
		detalleEntity.setLargo(BigDecimal.valueOf(pojo.getLargo()));
		detalleEntity.setOrden(BigDecimal.valueOf(pojo.getOrden()));
		detalleEntity.setVigenciaDetalleEstructura(pojo.getVigencia());

		return detalleEntity;
	}
}
