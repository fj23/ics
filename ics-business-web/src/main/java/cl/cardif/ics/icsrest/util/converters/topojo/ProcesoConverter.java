package cl.cardif.ics.icsrest.util.converters.topojo;

import cl.cardif.ics.domain.entity.procesos.IcsProceso;
import cl.cardif.ics.icsrest.domain.dto.output.ProcesoOutput;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class ProcesoConverter implements Converter<IcsProceso, ProcesoOutput> {

  @Override
  public ProcesoOutput convert(IcsProceso icsProceso) {

    ProcesoOutput proceso = new ProcesoOutput();
    proceso.setId(icsProceso.getIdProceso());
    proceso.setNombre(icsProceso.getNombreProceso());
    proceso.setVigencia(icsProceso.getVigenciaProceso());
    proceso.setCorreoEmail(icsProceso.getCorreoEmail());
    proceso.setCorreoSalida(icsProceso.getCorreoSalida());
    proceso.setCorreoHitos(icsProceso.getCorreoHitos());
    
    if (icsProceso.getIcsCallCenter() != null) {
    	proceso.setCallCenter(icsProceso.getIcsCallCenter().getIdCallCenter());
    }
    
    if (icsProceso.getIcsTipoArchivo() != null) {
    	proceso.setTipoArchivo(icsProceso.getIcsTipoArchivo().getIdTipoArchivo());
    }
    
    if (icsProceso.getIcsDirectorioSalidaError() != null) {
    	proceso.setDirectorioSalidaError(icsProceso.getIcsDirectorioSalidaError().getIdDirectorioSalida());
    }
    
    String flgAneto = icsProceso.getFlagAneto();
    if (flgAneto != null) {
        proceso.setEsSalidaAneto(flgAneto.equals("Y"));
    } else {
    	proceso.setEsSalidaAneto(false);
    }

    return proceso;
  }
}
