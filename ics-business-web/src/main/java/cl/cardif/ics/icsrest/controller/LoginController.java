package cl.cardif.ics.icsrest.controller;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cl.cardif.ics.icsrest.domain.pojo.DetalleRol;
import cl.cardif.ics.icsrest.domain.pojo.Login;
import cl.cardif.ics.icsrest.domain.pojo.TipoRol;
import cl.cardif.ics.icsrest.domain.pojo.User;
import cl.cardif.ics.icsrest.exceptions.login.IcsLoginException;
import cl.cardif.ics.icsrest.message.response.AuthLoginResponse;
import cl.cardif.ics.icsrest.message.response.JwtResponse;
import cl.cardif.ics.icsrest.service.AuthService;
import cl.cardif.ics.icsrest.service.LoginService;
import cl.cardif.ics.icsrest.service.RolesService;
import cl.cardif.ics.icsrest.util.Constants;
import cl.cardif.ics.icsrest.util.RolesUtil;

@RestController
@RequestMapping("/api")
public class LoginController {
	private static final Logger LOG = LoggerFactory.getLogger(LoginController.class);

	@Autowired
	private LoginService loginService;
	@Autowired
	private AuthService authService;
	@Autowired
	private RolesService rolesService;
	@Autowired
	private RolesUtil rolesUtil;

	@PostMapping("/auth")
	public ResponseEntity<JwtResponse> autenticar(@RequestBody Login login) throws IcsLoginException {
		LOG.info("autenticar");

		AuthLoginResponse authLoginResponse = loginService.autenticar(login);
		LOG.debug("autenticar/authLoginResponse = {}", authLoginResponse);

		if (authLoginResponse == null || !authLoginResponse.getResult()) {
			return ResponseEntity.notFound().build();
		}

		List<String> roles = loginService.getRoles(authLoginResponse.getToken());
		LOG.debug("autenticar/roles = {}", roles);

		List<TipoRol> tiposRol = rolesService.findRolesByName(roles);
		LOG.debug("autenticar/tiposRol = {}", tiposRol);

		Map<Long, DetalleRol> mapTiposRol = rolesUtil.createRichDetalleRolMapFromTipoRolList(tiposRol);
		LOG.debug("autenticar/mapTiposRol = {}", mapTiposRol);

		authLoginResponse.setRoles(roles);
		authLoginResponse.setDetallesRolMap(mapTiposRol);

		String tokenJwt = authService.signin(authLoginResponse);

		User user = new User();
		user.setUsername(login.getUsername());
		user.setRolesFromMap(mapTiposRol);
		user.setVersionBackend(Constants.VERSION_BACKEND);

		loginService.saveSession(authLoginResponse);

		return ResponseEntity.ok(new JwtResponse(tokenJwt, user));
	}

	@GetMapping("/validar")
	public ResponseEntity<Boolean> validar(@RequestHeader("Authorization") String token) {
		boolean resultado = loginService.validarToken(token);
		return ResponseEntity.ok(resultado);
	}

	@GetMapping("/logout/{token:.+}")
	public ResponseEntity<Void> logout(@PathVariable(name = "token") String token) {
		LOG.info("logout");
		loginService.logout(token);
		return ResponseEntity.ok().build();
	}

	@GetMapping("/validation")
	public ResponseEntity<List<Integer>> validationConfig() {
		List<Integer> config = loginService.getValidationConfig();
		return ResponseEntity.ok(config);
	}

	@ExceptionHandler({ IcsLoginException.class })
	public ResponseEntity<IcsLoginException> handlerIcsLoginException(IcsLoginException ex) {
		return new ResponseEntity<>(ex, HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
