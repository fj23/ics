package cl.cardif.ics.icsrest.exceptions.icsproceso;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class IcsProcesoNotFoundException extends RuntimeException {

  private static final long serialVersionUID = -1527596879157446910L;

  public IcsProcesoNotFoundException(String exception) {
    super(exception);
  }

  public IcsProcesoNotFoundException(String exception, Throwable t) {
    super(exception, t);
  }
}
