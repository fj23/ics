package cl.cardif.ics.icsrest.formula.operadores;

import java.util.ArrayList;
import java.util.Map;

import cl.cardif.ics.icsrest.formula.FormulaParent;
import cl.cardif.ics.icsrest.util.FormulaOrigenKey;

public class IfThenElse extends FormulaParent {
	public IfThenElse() {
		this.hijos = new ArrayList<>();
	}

	@Override
	public int getMaximumChildrenCount() {
		return 3;
	}

	@Override
	public String toSafeSQL(Map<FormulaOrigenKey, Integer> tableAliases) {
		StringBuilder sb = new StringBuilder()
				.append("CASE WHEN (").append(getHijo(0).toSafeSQL(tableAliases)).append(") ")
				.append("THEN (").append(getHijo(1).toSafeSQL(tableAliases)).append(") ")
				.append("ELSE (").append(getHijo(2).toSafeSQL(tableAliases)).append(") ")
				.append("END");

		return sb.toString();
	}

	@Override
	public String toSQL(Map<FormulaOrigenKey, Integer> tableAliases) {
		StringBuilder sb = new StringBuilder()
				.append("CASE WHEN (").append(getHijo(0).toSQL(tableAliases)).append(") ")
				.append("THEN (").append(getHijo(1).toSQL(tableAliases)).append(") ")
				.append("ELSE (").append(getHijo(2).toSQL(tableAliases)).append(") ")
				.append("END");

		return sb.toString();
	}

}
