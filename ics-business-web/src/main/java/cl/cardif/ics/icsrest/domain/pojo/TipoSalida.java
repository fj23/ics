package cl.cardif.ics.icsrest.domain.pojo;

public class TipoSalida {
	private Long idTipoSalida;
	private String descripcion;
	private String vigencia;

	public TipoSalida() {
		super();
	}

	public Long getIdTipoSalida() {
		return this.idTipoSalida;
	}

	public void setIdTipoSalida(Long idTipoSalida) {
		this.idTipoSalida = idTipoSalida;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getVigencia() {
		return this.vigencia;
	}

	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}
}
