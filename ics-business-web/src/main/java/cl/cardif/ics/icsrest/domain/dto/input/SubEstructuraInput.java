package cl.cardif.ics.icsrest.domain.dto.input;

import java.util.ArrayList;
import java.util.List;

public class SubEstructuraInput extends BaseColumnaModel {
	private List<DetalleSubEstructuraInput> detalles;

	public SubEstructuraInput() {
		super();
	}

	public List<DetalleSubEstructuraInput> getDetalles() {
		if (this.detalles != null) {
			return new ArrayList<>(this.detalles);
		} else {
			return new ArrayList<>();
		}
	}

	public void setDetalles(List<DetalleSubEstructuraInput> detalles) {
		if (detalles != null) {
			this.detalles = new ArrayList<>(detalles);
		}
	}
}
