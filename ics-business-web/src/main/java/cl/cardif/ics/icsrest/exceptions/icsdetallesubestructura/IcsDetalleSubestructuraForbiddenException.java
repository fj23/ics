package cl.cardif.ics.icsrest.exceptions.icsdetallesubestructura;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class IcsDetalleSubestructuraForbiddenException extends RuntimeException {

  private static final long serialVersionUID = 3397992475249736227L;

  public IcsDetalleSubestructuraForbiddenException(String exception) {
    super(exception);
  }
}
