package cl.cardif.ics.icsrest.util.converters.topojo;

import cl.cardif.ics.domain.entity.common.IcsTipoDato;
import cl.cardif.ics.domain.entity.plantillas.IcsAtributos;
import cl.cardif.ics.icsrest.domain.dto.input.ColumnaMetadatosSalidaModel;
import cl.cardif.ics.icsrest.domain.dto.input.TipoDatoModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

@Component
public class AtributoPlantillaConverter
    implements Converter<IcsAtributos, ColumnaMetadatosSalidaModel> {
  static final Logger LOG = LoggerFactory.getLogger(AtributoPlantillaConverter.class);

  @Override
  public ColumnaMetadatosSalidaModel convert(IcsAtributos entity) {
    ColumnaMetadatosSalidaModel pojo = new ColumnaMetadatosSalidaModel();

    LOG.debug("AtributoPlantillaConverter IcsAtributos to ColumnaMetadatosSalidaModel");

    pojo.setId(entity.getIdAtributo());
    pojo.setNombre(entity.getNombreColumna());
    pojo.setPosicionInicial(entity.getPosicionInicial());
    pojo.setPosicionFinal(entity.getPosicionFinal());
    pojo.setFlgAgrupacion(entity.getFlgAgrupacion());
    pojo.setOrden(entity.getNumeroColumna());
    try {
      if (entity.getIcsTipoDato() != null) {
        IcsTipoDato icsTipoDato = entity.getIcsTipoDato();
        TipoDatoModel tipoDato = new TipoDatoModel();
        tipoDato.setId(icsTipoDato.getIdTipoDato());
        tipoDato.setNombre(icsTipoDato.getNombreTipoDato());
        tipoDato.setVigencia(icsTipoDato.getVigencia());

        pojo.setTipoDato(tipoDato);
      }
    } catch (EntityNotFoundException e) {
      LOG.info("IcsTipoDato", e);
    }

    return pojo;
  }

  public List<ColumnaMetadatosSalidaModel> convertList(List<IcsAtributos> entities) {
    List<ColumnaMetadatosSalidaModel> result = new ArrayList<>();
    for (IcsAtributos entity : entities) {
      result.add(convert(entity));
    }
    return result;
  }
}
