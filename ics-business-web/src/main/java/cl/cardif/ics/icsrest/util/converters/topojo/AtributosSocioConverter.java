package cl.cardif.ics.icsrest.util.converters.topojo;

import cl.cardif.ics.domain.entity.plantillas.IcsAtributosSocio;
import cl.cardif.ics.icsrest.domain.dto.input.MapeoEntradaInput;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class AtributosSocioConverter implements Converter<IcsAtributosSocio, MapeoEntradaInput> {

	@Override
	public MapeoEntradaInput convert(IcsAtributosSocio entity) {
		MapeoEntradaInput pojo = new MapeoEntradaInput();

		pojo.setAtributoNegocioNombre(entity.getIcsAtributoNegocio().getNombreColumnaNegocio());
		pojo.setColumnaDestino(entity.getIdAtributoNegocio());
		pojo.setArchivoOrigen(entity.getIcsArchivo().getIdArchivo());
		pojo.setArchivoNombre(entity.getIcsArchivo().getNombreArchivo());
		if (entity.getIcsAtributos() != null) {
			pojo.setColumnaOrigen(entity.getIdAtributo());

			pojo.setColumnaNombre(entity.getIcsAtributos().getNombreColumna());
			pojo.setColumnaOrder(String.valueOf(entity.getIcsAtributos().getNumeroColumna()));

			pojo.setEstructuraOrigenId(entity.getIdDetalleEstructura());
		}

		pojo.setSubEstructuraColumnaOrigen(entity.getIdDetalleSubEstructura());
		pojo.setEsObligatorio("Y".equalsIgnoreCase(entity.getColumnaObligatoria()));

		pojo.setLote(entity.getLote());
		pojo.setEncriptar("Y".equalsIgnoreCase(entity.getEncriptado()));

		return pojo;
	}

	public List<MapeoEntradaInput> convertList(List<IcsAtributosSocio> entities) {
		List<MapeoEntradaInput> result = new ArrayList<>();
		for (IcsAtributosSocio entity : entities) {
			result.add(convert(entity));
		}
		return result;
	}
}
