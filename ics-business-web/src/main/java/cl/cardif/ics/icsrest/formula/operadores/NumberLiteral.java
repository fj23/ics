package cl.cardif.ics.icsrest.formula.operadores;

import java.util.Map;

import cl.cardif.ics.icsrest.formula.FormulaFactor;
import cl.cardif.ics.icsrest.formula.IMathFactor;
import cl.cardif.ics.icsrest.util.FormulaOrigenKey;

public class NumberLiteral extends FormulaFactor implements IMathFactor {
	protected static String tipo = "NumberLiteral";
	private Double valor = null;

	public NumberLiteral() {
		super();
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	@Override
	public String toSafeSQL(Map<FormulaOrigenKey, Integer> tableAliases) {
		return valor.toString();
	}

	@Override
	public String toSQL(Map<FormulaOrigenKey, Integer> tableAliases) {
		return valor.toString();
	}

	@Override
	public String toJSON() {
		StringBuilder sb = new StringBuilder()
			.append("{")
			.append("\"tipo\":\"NumberLiteral\"");

		if (valor != null) {
			sb.append(",")
				.append("\"valor\":").append(valor);
		}
		
		sb.append("}");

		return sb.toString();
	}

}
