package cl.cardif.ics.icsrest.util.converters.topojo;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import cl.cardif.ics.domain.entity.procesos.IcsDetalleTransaccion;
import cl.cardif.ics.icsrest.domain.pojo.DetalleTransaccion;

@Component
public class DetalleTransaccionConverter implements Converter<IcsDetalleTransaccion, DetalleTransaccion> {

  @Override
  public DetalleTransaccion convert(IcsDetalleTransaccion source) {
	DetalleTransaccion target = new DetalleTransaccion();
	target.setIdDetalleTransaccion(source.getIdDetalleTransaccion());
	target.setProceso(source.getIcsProceso().getNombreProceso());
	target.setSocio(source.getIcsSocio().getNombreSocio());
	target.setDetalleEtapa(source.getDetalleEtapa());
	target.setFechaInicio(source.getFechaInicio());
	target.setFechaFin(source.getFechaFin());
    return target;
  }
}
