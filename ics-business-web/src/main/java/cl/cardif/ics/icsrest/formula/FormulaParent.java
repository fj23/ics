package cl.cardif.ics.icsrest.formula;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class FormulaParent extends FormulaFactor implements IFormulaParent {
	protected List<IFormulaFactor> hijos;

	protected IFormulaFactor getHijo(int index) {
		return this.hijos.get(index);
	}
	
	@Override
	public Collection<IFormulaFactor> getHijos() {
		if (hijos != null) {
			return new ArrayList<>(hijos);
		} else {
			return new ArrayList<>();
		}
	}

	@Override
	public void setChildren(Collection<IFormulaFactor> children) {
		if (children != null) {
			this.hijos = new ArrayList<>(children);
		} else {
			this.hijos = new ArrayList<>();
		}
	}

	@Override
	public String childrenToJSON() {
		StringBuilder sb = new StringBuilder()
			.append("\"hijos\":[");
		
		final int childrenCount = hijos.size();

		for (int i = 0; i < childrenCount; i++) {
			sb.append(getHijo(i).toJSON());

			if (i + 1 == getMaximumChildrenCount()) {
				break;
			} else if (i + 1 < childrenCount) {
				sb.append(",");
			}
		}

		sb.append("]");

		return sb.toString();
	}

	@Override
	public String toJSON() {
		StringBuilder sb = new StringBuilder()
				.append("{")
				.append("\"tipo\":\"").append(getClass().getSimpleName()).append("\"");

		if (!hijos.isEmpty()) {
			sb.append(",")
				.append(this.childrenToJSON());
		}
		
		sb.append("}");

		return sb.toString();
	}
}
