package cl.cardif.ics.icsrest.util.converters.topojo;

import cl.cardif.ics.domain.entity.common.IcsTipoArchivo;
import cl.cardif.ics.icsrest.domain.pojo.TipoArchivo;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class TipoArchivoConverter implements Converter<IcsTipoArchivo, TipoArchivo> {

  @Override
  public TipoArchivo convert(IcsTipoArchivo source) {
    TipoArchivo target = new TipoArchivo();
    target.setId(source.getIdTipoArchivo());
    target.setNombre(source.getNombreTipoArchivo());
    target.setVigencia(source.getFlgVisible());
    return target;
  }
}
