package cl.cardif.ics.icsrest.domain.dto.output;

import cl.cardif.ics.icsrest.domain.pojo.DetalleEstructura;
import cl.cardif.ics.icsrest.domain.pojo.TipoDato;

public class DetalleEstructuraOutput extends DetalleEstructura {
  private long posicionInicial;
  private long posicionFinal;
  private TipoDato tipoDato;

  public DetalleEstructuraOutput() {
    // Constructor JavaBean
  }

  public long getPosicionFinal() {
    return posicionFinal;
  }

  public void setPosicionFinal(long posicionFinal) {
    this.posicionFinal = posicionFinal;
  }

  public long getPosicionInicial() {
    return posicionInicial;
  }

  public void setPosicionInicial(long posicionInicial) {
    this.posicionInicial = posicionInicial;
  }

  public TipoDato getTipoDatoModel() {
    return tipoDato;
  }

  public void setTipoDatoModel(TipoDato tipoDatoModel) {
    this.tipoDato = tipoDatoModel;
  }
}
