package cl.cardif.ics.icsrest.domain.dto.output;

import cl.cardif.ics.icsrest.domain.pojo.Archivo;

public class PlantillaSalidaOutput extends PlantillaOutput {
	private Archivo archivo;
	private Long directorioSalida;

	public PlantillaSalidaOutput() {
		super();
	}

	public Archivo getArchivo() {
		return archivo;
	}

	public void setArchivo(Archivo archivo) {
		this.archivo = archivo;
	}

	public Long getDirectorioSalida() {
		return directorioSalida;
	}

	public void setDirectorioSalida(Long directorioSalida) {
		this.directorioSalida = directorioSalida;
	}

	@Override
	public String toString() {
		return "PlantillaSalidaOutput [archivo=" + archivo + ", directorioSalida=" + directorioSalida + ", toString()=" + super.toString()
			+ "]";
	}
}
