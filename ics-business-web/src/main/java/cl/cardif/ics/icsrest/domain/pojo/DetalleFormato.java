package cl.cardif.ics.icsrest.domain.pojo;

public class DetalleFormato {

  private long id;
  private String nombre;
  private String tipo;

  public DetalleFormato() {
    // Constructor JavaBean
  }

  public long getId() {
    return this.id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getNombre() {
    return this.nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public String getTipo() {
    return this.tipo;
  }

  public void setTipo(String tipo) {
    this.tipo = tipo;
  }
}
