package cl.cardif.ics.icsrest.util.converters.topojo;

import cl.cardif.ics.domain.entity.estructuras.IcsDetalleEstructura;
import cl.cardif.ics.icsrest.domain.dto.output.DetalleEstructuraSimpleOutput;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class DetallesEstructurasSimpleConverter
    implements Converter<IcsDetalleEstructura, DetalleEstructuraSimpleOutput> {

  @Override
  public DetalleEstructuraSimpleOutput convert(IcsDetalleEstructura icsEstructura) {

    DetalleEstructuraSimpleOutput detalle = new DetalleEstructuraSimpleOutput();
    detalle.setId(icsEstructura.getIdDetalleEstructura());
    detalle.setDescripcion(icsEstructura.getDescripcionDetalleEstructura());

    return detalle;
  }
}
