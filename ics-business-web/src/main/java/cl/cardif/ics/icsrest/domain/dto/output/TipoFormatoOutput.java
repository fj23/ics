package cl.cardif.ics.icsrest.domain.dto.output;

public class TipoFormatoOutput {
  private Long idTipoFormato;
  private String nombreFormato;
  private String vigenciaFormato;
  private Long orden;

  public TipoFormatoOutput() {
    super();
  }

  public TipoFormatoOutput(Long idTipoFormato, String nombreFormato, String vigenciaFormato) {
    super();
    this.idTipoFormato = idTipoFormato;
    this.nombreFormato = nombreFormato;
    this.vigenciaFormato = vigenciaFormato;
  }

  public Long getIdTipoFormato() {
    return idTipoFormato;
  }

  public void setIdTipoFormato(Long idTipoFormato) {
    this.idTipoFormato = idTipoFormato;
  }

  public String getNombreFormato() {
    return nombreFormato;
  }

  public void setNombreFormato(String nombreFormato) {
    this.nombreFormato = nombreFormato;
  }

  public Long getOrden() {
    return orden;
  }

  public void setOrden(Long orden) {
    this.orden = orden;
  }

  public String getVigenciaFormato() {
    return vigenciaFormato;
  }

  public void setVigenciaFormato(String vigenciaFormato) {
    this.vigenciaFormato = vigenciaFormato;
  }
}
