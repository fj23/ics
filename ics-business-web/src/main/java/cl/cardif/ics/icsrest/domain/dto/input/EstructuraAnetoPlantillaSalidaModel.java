package cl.cardif.ics.icsrest.domain.dto.input;

import java.util.ArrayList;
import java.util.List;

public class EstructuraAnetoPlantillaSalidaModel extends BaseColumnaModel {
	private List<DetalleEstructuraPlantillaSalidaInput> columnas;

	public List<DetalleEstructuraPlantillaSalidaInput> getColumnas() {
		if (this.columnas != null) {
			return new ArrayList<>(this.columnas);
		} else {
			return new ArrayList<>();
		}
	}

	public void setColumnas(List<DetalleEstructuraPlantillaSalidaInput> columnas) {
		if (columnas != null) {
			this.columnas = new ArrayList<>(columnas);
		}
	}
	
	
}
