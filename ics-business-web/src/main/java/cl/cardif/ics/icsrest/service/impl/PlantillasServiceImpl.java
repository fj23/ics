package cl.cardif.ics.icsrest.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityNotFoundException;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Service;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Predicate;

import cl.cardif.ics.domain.entity.common.IcsEvento;
import cl.cardif.ics.domain.entity.common.IcsSocio;
import cl.cardif.ics.domain.entity.common.IcsTipoExtension;
import cl.cardif.ics.domain.entity.common.IcsTipoSalida;
import cl.cardif.ics.domain.entity.common.QIcsTipoExtension;
import cl.cardif.ics.domain.entity.plantillas.IcsPlantilla;
import cl.cardif.ics.domain.entity.plantillas.IcsTipoPlantilla;
import cl.cardif.ics.domain.entity.plantillas.QIcsPlantilla;
import cl.cardif.ics.domain.entity.plantillas.QIcsTipoPlantilla;
import cl.cardif.ics.domain.entity.procesos.IcsDetalleProceso;
import cl.cardif.ics.domain.entity.procesos.IcsProceso;
import cl.cardif.ics.domain.entity.procesos.QIcsDetalleProceso;
import cl.cardif.ics.domain.entity.procesos.QIcsProceso;
import cl.cardif.ics.icsrest.domain.dto.output.PlantillaOutput;
import cl.cardif.ics.icsrest.domain.dto.output.RequestResultOutput;
import cl.cardif.ics.icsrest.domain.pojo.Evento;
import cl.cardif.ics.icsrest.domain.pojo.Plantilla;
import cl.cardif.ics.icsrest.domain.pojo.Socio;
import cl.cardif.ics.icsrest.domain.pojo.TipoExtension;
import cl.cardif.ics.icsrest.domain.pojo.TipoPlantilla;
import cl.cardif.ics.icsrest.domain.pojo.TipoSalida;
import cl.cardif.ics.icsrest.exceptions.icsplantilla.IcsPlantillaNotFoundException;
import cl.cardif.ics.icsrest.exceptions.icsplantilla.IcsPlantillaServiceException;
import cl.cardif.ics.icsrest.repository.DetallesProcesoRepository;
import cl.cardif.ics.icsrest.repository.PlantillaRepository;
import cl.cardif.ics.icsrest.repository.ProcesoRepository;
import cl.cardif.ics.icsrest.repository.TiposExtensionRepository;
import cl.cardif.ics.icsrest.service.PlantillasService;
import cl.cardif.ics.icsrest.util.Constants;
import cl.cardif.ics.icsrest.util.OutputListWrapper;
import cl.cardif.ics.icsrest.util.Util;

@Service
public class PlantillasServiceImpl implements PlantillasService {
	private static final Logger LOG = LoggerFactory.getLogger(PlantillasServiceImpl.class);
	
	private static final String TIPO_PLANTILLA = "tipoPlantilla";
	private static final String NOMBRE_PLANTILLA = "nombrePlantilla";
	private static final String VERSION_PLANTILLA = "versionPlantilla";

	private JdbcTemplate jdbcTemplate;

	@Autowired private ConversionService conversionService;
	
	@Autowired private PlantillaRepository plantillaRepository;
	@Autowired private DetallesProcesoRepository detallesProcesoRepository;
	@Autowired private ProcesoRepository procesoRepository;
	@Autowired private TiposExtensionRepository tipoExtensionRepository;
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	private Predicate generarFiltrosIdentificadoresDeGrupoVersionesPlantilla(IcsPlantilla versionActual) {
		LOG.info("generarFiltrosIdentificadoresDeGrupoVersionesPlantilla");
		
		final Long idEvento = versionActual.getIcsEvento().getIdEvento();
		final Long idSocio = versionActual.getIcsSocio().getIdSocio();
		final Long idTipoPlantilla = versionActual.getIcsTipoPlantilla().getIdTipoPlantilla();
		final String nombrePlantilla = versionActual.getNombrePlantilla();

		final QIcsPlantilla qIcsPtl = QIcsPlantilla.icsPlantilla;
		

		return new BooleanBuilder()
				.and(qIcsPtl.icsEvento.idEvento.eq(idEvento))
				.and(qIcsPtl.icsSocio.idSocio.eq(idSocio))
				.and(qIcsPtl.icsTipoPlantilla.idTipoPlantilla.eq(idTipoPlantilla))
				.and(qIcsPtl.nombrePlantilla.eq(nombrePlantilla));
	}

	private List<IcsPlantilla> obtenerListaPlantillas(Predicate filtros, OrderSpecifier<?> orden) {
		Iterable<IcsPlantilla> iteraPlantillas;
		if (orden != null) {
			iteraPlantillas = plantillaRepository.findAll(filtros, orden);
		} else {
			iteraPlantillas = plantillaRepository.findAll(filtros);
		}
		
		List<IcsPlantilla> entities = new ArrayList<>();
		for (IcsPlantilla icsPlantilla : iteraPlantillas) {
			entities.add(icsPlantilla);
		}

		return entities;
	}

	@Override
	public List<IcsPlantilla> obtenerVersionesPlantilla(IcsPlantilla versionActual) {
		Predicate booleanBuilder = this.generarFiltrosIdentificadoresDeGrupoVersionesPlantilla(versionActual);

		OrderSpecifier<Long> orderSpecifier = QIcsPlantilla.icsPlantilla.versionPlantilla.asc();

		return this.obtenerListaPlantillas(booleanBuilder, orderSpecifier);
	}

	@Override
	public Long obtenerNumeroVersionVigente(String nombrePlantilla, Long idSocio, Long idEvento, Long idTipoPlantilla) {

		QIcsPlantilla qPlantilla = QIcsPlantilla.icsPlantilla;
		
		BooleanBuilder bb = new BooleanBuilder()
				.and(qPlantilla.nombrePlantilla.eq(nombrePlantilla))
				.and(qPlantilla.icsEvento.idEvento.eq(idEvento))
				.and(qPlantilla.icsSocio.idSocio.eq(idSocio))
				.and(qPlantilla.icsTipoPlantilla.idTipoPlantilla.eq(idTipoPlantilla));

		OrderSpecifier<Long> ordenarPorVersionDescendente = qPlantilla.versionPlantilla.desc();

		Iterator<IcsPlantilla> it = plantillaRepository.findAll(bb, ordenarPorVersionDescendente).iterator();

		// sólo traer 1 plantilla (tiene la versión más alta)
		if (it.hasNext()) {
			IcsPlantilla versionMayor = it.next();
			return versionMayor.getVersionPlantilla();
		} else {
			return 0L;
		}
	}

	public boolean puedeActivar(long idPlantilla) {
		LOG.info("puedeActivar");

		IcsPlantilla versionActual = plantillaRepository.findOne(idPlantilla);
		
		// saca otras versiones de plantilla
		BooleanBuilder seanVersionesDePlantilla = new BooleanBuilder()
				.and(this.generarFiltrosIdentificadoresDeGrupoVersionesPlantilla(versionActual));
		Iterable<IcsPlantilla> versiones = plantillaRepository.findAll(seanVersionesDePlantilla);
		
		// guarda los ID de estas versiones
		List<Long> idsVersiones = new ArrayList<>();
		for (IcsPlantilla icsPtl : versiones) {
			idsVersiones.add(icsPtl.getIdPlantilla());
		}

		// si 'No Hay' procesos asociados a otra versión de la misma plantilla (coincidiendo cualquier ID)...
		BooleanBuilder procesoAsocieAlgunaVersionPlantilla = new BooleanBuilder()
				.and(QIcsDetalleProceso.icsDetalleProceso.icsPlantilla.idPlantilla.in(idsVersiones));
		long count = detallesProcesoRepository.count(procesoAsocieAlgunaVersionPlantilla);

		// ...puede activar la plantilla enseguida
		return (count == 0);
	}

	@Override
	public void activar(long idPlantilla) {
		LOG.info("activar");

		IcsPlantilla versionActual = null;
		try {
			versionActual = plantillaRepository.getOne(idPlantilla);
		} catch (EntityNotFoundException e) {
			throw new IcsPlantillaNotFoundException("La plantilla no pudo ser encontrada", e);
		}

		// desactiva versiones anteriores y activas de plantillas
		BooleanBuilder bb = new BooleanBuilder()
				.and(this.generarFiltrosIdentificadoresDeGrupoVersionesPlantilla(versionActual))
				.and(QIcsPlantilla.icsPlantilla.vigencia.eq("Y"));

		// almacena ids de estas plantillas para buscar procesos relacionados
		List<Long> idsVersiones = new ArrayList<>();
		List<IcsPlantilla> versiones = this.obtenerListaPlantillas(bb, null);

		for (IcsPlantilla icsPtl : versiones) {
			idsVersiones.add(icsPtl.getIdPlantilla());
			icsPtl.setVigencia("N");
			plantillaRepository.save(icsPtl);
		}
		plantillaRepository.flush();

		// actualiza estos proceso relacionados para apuntar a la nueva version activa
		QIcsDetalleProceso qProceso = QIcsDetalleProceso.icsDetalleProceso;
		bb = new BooleanBuilder()
				.and(qProceso.icsPlantilla.idPlantilla.in(idsVersiones))
				.and(qProceso.icsProceso.vigenciaProceso.eq("Y"));

		Iterable<IcsDetalleProceso> iteraDetalles = detallesProcesoRepository.findAll(bb);

		for (IcsDetalleProceso detalleProceso : iteraDetalles) {
			detalleProceso.setIcsPlantilla(versionActual);
			detallesProcesoRepository.save(detalleProceso);
		}
		detallesProcesoRepository.flush();

		// activa version actual
		versionActual.setVigencia("Y");
		plantillaRepository.saveAndFlush(versionActual);
	}

	@Override
	public boolean puedeDesactivar(long idPlantilla) {
		LOG.info("puedeDesactivar");

		IcsPlantilla versionActual;
		try {
			versionActual = this.plantillaRepository.getOne(idPlantilla);
		} catch (EntityNotFoundException e) {
			throw new IcsPlantillaServiceException("Plantilla no encontrada", e);
		}

		// desactiva versiones anteriores
		BooleanBuilder bb = new BooleanBuilder()
				.and(this.generarFiltrosIdentificadoresDeGrupoVersionesPlantilla(versionActual))
				.and(QIcsPlantilla.icsPlantilla.vigencia.eq("Y"));

		List<IcsPlantilla> versiones = this.obtenerListaPlantillas(bb, null);
		if (versiones.isEmpty()) {
			return true;
		}

		List<Long> idsVersiones = new ArrayList<>();
		for (IcsPlantilla icsPtl : versiones) {
			idsVersiones.add(icsPtl.getIdPlantilla());
		}

		QIcsProceso qProceso = QIcsProceso.icsProceso;
		bb = new BooleanBuilder().and(qProceso.detalles.any().icsPlantilla.idPlantilla.in(idsVersiones))
				.and(qProceso.vigenciaProceso.eq("Y"));

		try {
			Long countProcesosConPlantilla = procesoRepository.count(bb);
			return (countProcesosConPlantilla == 0);
		} catch (EntityNotFoundException e) {
			LOG.warn("Detalle proceso no encontrado", e);
			return true;
		}
	}

	@Override
	public void desactivar(long idPlantilla) {
		LOG.info("desactivar");

		QIcsProceso qProceso = QIcsProceso.icsProceso;
		BooleanBuilder bb = new BooleanBuilder()
				.and(qProceso.detalles.any().icsPlantilla.idPlantilla.eq(idPlantilla))
				.and(qProceso.vigenciaProceso.eq("Y"));

		int countProcesosConPlantilla = 0;
		try {
			countProcesosConPlantilla = (int) procesoRepository.count(bb);
		} catch (EntityNotFoundException exc) {
			LOG.info("No hay encontraron procesos asociados a la plantilla", exc);
		}

		if (countProcesosConPlantilla > 0) {

			Iterable<IcsProceso> procesos = procesoRepository.findAll(bb);
			int countProceso = 0;
			for (IcsProceso icsProc : procesos) {
				try {
					icsProc.setVigenciaProceso("N");
					procesoRepository.save(icsProc);
					countProceso++;
				} catch (Exception exc) {
					LOG.error("No se pudo actualizar el proceso con id {}", icsProc.getIdProceso(), exc);
				}
			}
			
			if (countProceso > 0) {
				procesoRepository.flush();
				LOG.info("Se desactivaron {} procesos asociados a la plantilla con id {}", countProceso, idPlantilla);
			}
		}

		try {
			IcsPlantilla plantilla = plantillaRepository.getOne(idPlantilla);
			plantilla.setVigencia("N");
			plantillaRepository.saveAndFlush(plantilla);
		} catch (EntityNotFoundException e) {
			throw new IcsPlantillaServiceException("Plantilla no se pudo guardar", e);
		}

	}

	/** Guarda la configuracion de una plantilla en la tabla temporal ics_arreglo_util
	 * Esta tabla es posteriormente leida y borrada por el SP de creacion.
	 *
	 * @param a Primera dimension de un arreglo
	 * @param b Segunda dimension de un arreglo
	 * @param c Valor en la posicion [a][b] del arreglo
	 * @return resultado de la consulta sql
	 */
	private int insertarEnArregloUtil(BigDecimal a, BigDecimal b, String c) {
		LOG.info("insertarEnArregloUtil");

		final String table_sql = "ics_arreglo_util";
		final String insert_campos_sql = "indicea,indiceb,indicec";
		final String insert_values_sql = "(?,?,?)";
		
		final StringBuilder sqlBuilder = new StringBuilder()
				.append("INSERT INTO ").append(table_sql).append(" ")
				.append("(").append(insert_campos_sql).append(") ")
				.append("VALUES ").append(insert_values_sql);

		Object[] params = { a, b, c };

		return jdbcTemplate.update(sqlBuilder.toString(), params);
	}

	/**
	 * Recorre un arreglo de dos dimensiones y por cada objeto de la segunda
	 * dimension llama a insertArray(foo, foo, foo) para almacenar datos
	 * relacionados a las columnas mapeadas de una plantilla
	 *
	 * @param archivos Arreglo de dos dimensiones
	 */
	@Override
	public int almacenarArrayBidimensionalEnArregloUtil(List<Map<Integer, String>> archivos) {
		LOG.info("almacenarArrayBidimensionalEnArregloUtil");
		
		Integer registrosInsertados = 0;
		Integer archivoIndex = 0;

		for (Map<Integer, String> archivoMap : archivos) {

			for (Map.Entry<Integer, String> mapEntry : archivoMap.entrySet()) {

				Integer mapKey = mapEntry.getKey();
				String mapValue = mapEntry.getValue();
				
				int inserciones = this.insertarEnArregloUtil(BigDecimal.valueOf(archivoIndex), BigDecimal.valueOf(mapKey), mapValue);
				registrosInsertados += inserciones;
			}
			archivoIndex++;
		}
		
		final String logResult = "almacenarArrayBidimensionalEnArregloUtil - registrosInsertados="+registrosInsertados;
		LOG.info(logResult);

		return registrosInsertados;
	}

	private Sort buildSort(String sortColumn, String sortOrder) {

		Sort.Direction dir;
		String entitySortField;

		switch (sortColumn) {
			case "tipo":
				entitySortField = TIPO_PLANTILLA;
				break;
			case "nombre":
				entitySortField = NOMBRE_PLANTILLA;
				break;
			case "version":
				entitySortField = VERSION_PLANTILLA;
				break;
			case "fechaCreacion":
				entitySortField = sortColumn;
				break;
			default:
				return null;
		}

		if ("desc".equalsIgnoreCase(sortOrder)) {
			dir = Sort.Direction.DESC;
		} else {
			dir = Sort.Direction.ASC;
		}

		return new Sort(dir, entitySortField);
	}

	@Override
	public RequestResultOutput llamarSPAlmacenarPlantilla(Map<String, Object> map) {
		LOG.info("callSpAlmacenarPlantilla");

		try {
			SimpleJdbcCall call = new SimpleJdbcCall(jdbcTemplate).withCatalogName(Constants.PACKAGE_TEMPLATE_LOAD)
					.withProcedureName(Constants.PROCEDURE_TEMPLATE_CREATION)
					.declareParameters(
							new SqlParameter(Constants.PV_TEMPLATE_CREATION_NOMBRE, oracle.jdbc.OracleTypes.VARCHAR),
							new SqlParameter(Constants.PN_TEMPLATE_CREATION_VERSION, oracle.jdbc.OracleTypes.NUMBER),
							new SqlParameter(Constants.PN_TEMPLATE_CREATION_IDTIPOPLANTILLA, oracle.jdbc.OracleTypes.NUMBER),
							new SqlParameter(Constants.PN_TEMPLATE_CREATION_IDSOCIO, oracle.jdbc.OracleTypes.NUMBER),
							new SqlParameter(Constants.PN_TEMPLATE_CREATION_IDEVENTO, oracle.jdbc.OracleTypes.NUMBER),
							new SqlParameter(Constants.PN_TEMPLATE_CREATION_IDCORE, oracle.jdbc.OracleTypes.NUMBER),
							new SqlParameter(Constants.PV_TEMPLATE_CREATION_OBSERVACIONES, oracle.jdbc.OracleTypes.VARCHAR),
							new SqlParameter(Constants.PN_TEMPLATE_CREATION_IDTIPOEXTENSION, oracle.jdbc.OracleTypes.NUMBER),
							new SqlParameter(Constants.PN_TEMPLATE_CREATION_IDTIPOSALIDA, oracle.jdbc.OracleTypes.NUMBER),
							new SqlParameter(Constants.PV_TEMPLATE_CREATION_DELIMITADOR, oracle.jdbc.OracleTypes.VARCHAR),
							new SqlParameter(Constants.PN_TEMPLATE_CREATION_IDVERSIONANTERIOR, oracle.jdbc.OracleTypes.NUMBER),
							new SqlParameter(Constants.PV_TEMPLATE_CREATION_UNIONARCHIVOS, oracle.jdbc.OracleTypes.VARCHAR),
							new SqlParameter(Constants.PV_TEMPLATE_CREATION_USUARIO, oracle.jdbc.OracleTypes.VARCHAR));

			Map<String, Object> execute = call.execute(new MapSqlParameterSource(map));
			return new RequestResultOutput(execute);
		} catch (Exception e) {
			throw new IcsPlantillaServiceException("Error al almacenar plantilla", e);
		}
	}

	@Override
	public List<String> obtenerNombresDeTodasLasPlantillas(Long idSocio, Long idEvento) {
		List<String> lista = new ArrayList<>();
		try {
			HashSet<Long> where = Util.getEventsAuthorized();
			if (idSocio == null) {
				if (idEvento == null) {
					return plantillaRepository.findDistinctPlantillas(new ArrayList<>(where));	
				} else if (where.contains(idEvento)) {
					return plantillaRepository.findDistinctPlantillas(idEvento);
				}
			} else {
				if (idEvento == null) {
					return plantillaRepository.findDistinctPlantillas(idSocio, new ArrayList<>(where));
				} else if (where.contains(idEvento)) {
					return plantillaRepository.findDistinctPlantillas(idSocio, idEvento);
				}
			}
			
			return lista;
			
		} catch (Exception e) {
			throw new IcsPlantillaServiceException("Error al obtener plantillas", e);
		}
	}

	@Override
	public List<TipoExtension> obtenerTiposExtensionEstandar() {
		LOG.info("obtenerTiposExtensionEstandar");
		
		QIcsTipoExtension qIcsTipoExtension = QIcsTipoExtension.icsTipoExtension;

		BooleanBuilder extensionesVigentes = new BooleanBuilder()
				.and(qIcsTipoExtension.vigencia.eq("Y"))
				.and(qIcsTipoExtension.flgVisibleIntg.eq("Y"));
		OrderSpecifier<Long> ordenarPorId = QIcsTipoExtension.icsTipoExtension.idExtension.asc();
		
		Iterable<IcsTipoExtension> iteraTiposExtension = tipoExtensionRepository.findAll(extensionesVigentes, ordenarPorId);
		
		List<TipoExtension> tiposExtension = new ArrayList<>();
		for (IcsTipoExtension icsTipoExtension : iteraTiposExtension) {
			TipoExtension tipoExtension = conversionService.convert(icsTipoExtension, TipoExtension.class);
			tiposExtension.add(tipoExtension);
		}

		return tiposExtension;
	}

	private BooleanBuilder hacerFiltrosDesdeMapaQueryString(Map<String, String> filters) {
		LOG.info("hacerFiltrosDesdeMapaQueryString");

		QIcsPlantilla qIcsPlantilla = QIcsPlantilla.icsPlantilla;
		
		BooleanBuilder bb = new BooleanBuilder();
		
		final String filtroTipo = "tipo";
		final String filtroSocio = "socio";
		final String filtroEvento = "evento";
		final String filtroNombre = "nombre";
		final String filtroVigencia = "vigencia";

		if (filters.containsKey(filtroTipo)) {
			long tipo = Long.parseLong(filters.get(filtroTipo));
			bb.and(qIcsPlantilla.icsTipoPlantilla.idTipoPlantilla.eq(tipo));
		}

		if (filters.containsKey(filtroSocio)) {
			long socio = Long.parseLong(filters.get(filtroSocio));
			bb.and(qIcsPlantilla.icsSocio.idSocio.eq(socio));
		}

		HashSet<Long> idsEventos = Util.getEventsAuthorized();
		if (filters.containsKey(filtroEvento) && idsEventos.contains(Long.valueOf(filters.get(filtroEvento)))) {
			bb.and(qIcsPlantilla.icsEvento.idEvento.eq(Long.valueOf(filters.get(filtroEvento))));
		} else {
			bb.and(qIcsPlantilla.icsEvento.idEvento.in(idsEventos));
		}

		if (filters.containsKey(filtroNombre)) {
			String nombre = filters.get(filtroNombre);
			bb.and(qIcsPlantilla.nombrePlantilla.equalsIgnoreCase(nombre));
		}

		if (filters.containsKey(filtroVigencia)) {
			String vigencia = filters.get(filtroVigencia);
			bb.and(qIcsPlantilla.vigencia.equalsIgnoreCase(vigencia));
		}

		return bb;
	}


	private Plantilla convertirIcsPlantillaAPojo(IcsPlantilla icsPlantilla) {
		IcsEvento icsEvento = icsPlantilla.getIcsEvento();
		IcsSocio icsSocio = icsPlantilla.getIcsSocio();
		IcsTipoPlantilla icsTipoPlantilla = icsPlantilla.getIcsTipoPlantilla();
		IcsTipoSalida icsTipoSalida = icsPlantilla.getIcsTipoSalida();
		
		Plantilla plantilla = conversionService.convert(icsPlantilla, Plantilla.class);
		
		Evento evento = conversionService.convert(icsEvento, Evento.class);
		Socio socio = conversionService.convert(icsSocio, Socio.class);
		TipoPlantilla tipoPlantilla = conversionService.convert(icsTipoPlantilla, TipoPlantilla.class);
		TipoSalida tipoSalida = conversionService.convert(icsTipoSalida, TipoSalida.class);

		plantilla.setEvento(evento);
		plantilla.setSocio(socio);
		plantilla.setTipo(tipoPlantilla);
		plantilla.setTipoSalida(tipoSalida);
		return plantilla;
	}

	@Override
	public OutputListWrapper<PlantillaOutput> obtenerPlantillasOutput(
		int page, 
		int size, 
		String sortColumn, 
		String sortOrder,
		Map<String, String> filters
	) {
		LOG.info("obtenerPlantillasOutput");
		
		// resolver paginación
		Pageable paginador;
		if (sortColumn == null || sortColumn.isEmpty()) {
			paginador = new PageRequest(page, size);
		} else {
			if (sortOrder.isEmpty()) { sortOrder = "asc"; }
			Sort sorter = this.buildSort(sortColumn, sortOrder);
			paginador = new PageRequest(page, size, sorter);
		}

		// consultar plantillas, con filtros o sin ellos
		long countPlantillas;
		Iterable<IcsPlantilla> iteraPlantillas;
		if (filters == null || filters.isEmpty()) {
			countPlantillas = plantillaRepository.count();
			iteraPlantillas = plantillaRepository.findAll(paginador);
		} else {
			BooleanBuilder bb = this.hacerFiltrosDesdeMapaQueryString(filters);
			
			countPlantillas = plantillaRepository.count(bb);
			iteraPlantillas = plantillaRepository.findAll(bb, paginador);
		}

		// añadir plantillas a listado
		List<PlantillaOutput> plantillas = new ArrayList<>();
		for (IcsPlantilla icsPlantilla : iteraPlantillas) {
			PlantillaOutput plantilla = conversionService.convert(icsPlantilla, PlantillaOutput.class);
			plantillas.add(plantilla);
		}
		
		return new OutputListWrapper<>(plantillas, (int) countPlantillas, "");
	}

	@Override
	public OutputListWrapper<PlantillaOutput> obtenerPlantillasOutputSegunTipo(Long idTipoPlantilla) {

		QIcsPlantilla qPlantilla = QIcsPlantilla.icsPlantilla;
		
		BooleanBuilder bb = new BooleanBuilder()
				.and(qPlantilla.icsTipoPlantilla.idTipoPlantilla.eq(idTipoPlantilla));
		OrderSpecifier<Long> ordenPorTipoId = qPlantilla.icsTipoPlantilla.idTipoPlantilla.asc();
		OrderSpecifier<Long> ordenPorId = qPlantilla.idPlantilla.asc();

		Iterable<IcsPlantilla> iteraPlantillas = plantillaRepository.findAll(bb, ordenPorTipoId, ordenPorId);
		long countPlantillas = plantillaRepository.count(bb);
		
		List<PlantillaOutput> plantillas = new ArrayList<>();
		for (IcsPlantilla icsPlantilla : iteraPlantillas)  {
			PlantillaOutput plantilla = conversionService.convert(icsPlantilla, PlantillaOutput.class);
			plantillas.add(plantilla);
		}

		return new OutputListWrapper<>(plantillas, (int) countPlantillas, "");
	}


	private List<Plantilla> iterablePlantillasALista(BooleanBuilder bb) {
		OrderSpecifier<Long> ordenPorId = QIcsPlantilla.icsPlantilla.idPlantilla.asc();
		Iterable<IcsPlantilla> iteraPlantillas = plantillaRepository.findAll(bb, ordenPorId);
		
		List<Plantilla> plantillas = new ArrayList<>();
		for (IcsPlantilla icsPlantilla : iteraPlantillas) {
			Plantilla plantilla = this.convertirIcsPlantillaAPojo(icsPlantilla);
			plantillas.add(plantilla);
		}
		return plantillas;
	}

	@Override
	public OutputListWrapper<Plantilla> obtenerPlantillasPojo(
			Long idSocio, 
			Long idEvento
	) {
		LOG.info("obtenerPlantillasPojo");

		QIcsPlantilla qPlantilla = QIcsPlantilla.icsPlantilla;
		QIcsTipoPlantilla qTipoPlantilla = qPlantilla.icsTipoPlantilla;		

		// todas las plantillas vigentes, filtradas por socio y evento
		// que sean de salida no estructurada, o de entrada o de integración
		BooleanBuilder esPlantillaVigenteDeEventoSocio = new BooleanBuilder()
			.and(qPlantilla.icsSocio.idSocio.eq(idSocio))
			.and(qPlantilla.icsEvento.idEvento.eq(idEvento))
			.and(qPlantilla.vigencia.eq("Y"));

		BooleanBuilder esSalidaNoEstructurada = new BooleanBuilder()
			.and(qPlantilla.icsArchivo.isNotNull())
			.andNot(
				qTipoPlantilla.idTipoPlantilla.eq(Constants.ID_TIPO_PLANTILLA_SALIDA)
				.and(qPlantilla.icsArchivo.icsTipoExtension.idExtension.eq(Constants.ID_TIPO_EXTENSION_ARCHIVO_ESTRUCTURADO)))
			.and(esPlantillaVigenteDeEventoSocio);

		BooleanBuilder esPlantillaSinArchivo = new BooleanBuilder()
			.and(qPlantilla.icsArchivo.isNull())
			.and(esPlantillaVigenteDeEventoSocio);
		
		long countPlantillas = plantillaRepository.count(esSalidaNoEstructurada) + plantillaRepository.count(esPlantillaSinArchivo);

		List<Plantilla> plantillas = this.iterablePlantillasALista(esSalidaNoEstructurada);
		plantillas.addAll(this.iterablePlantillasALista(esPlantillaSinArchivo));
		
		return new OutputListWrapper<>(plantillas, (int) countPlantillas, "");
	}

	@Override
	public OutputListWrapper<Plantilla> obtenerPlantillasSalidaEstructuradaPojo(
		Long idSocio, 
		Long idEvento
	) {
		LOG.info("obtenerPlantillasSalidaEstructuradaPojo");

		QIcsPlantilla qPlantilla = QIcsPlantilla.icsPlantilla;
		
		// orden por defecto
		OrderSpecifier<Long> ordenarPorId = qPlantilla.idPlantilla.asc();

		// consultar plantillas solo plantillas de salida estructuradas
		BooleanBuilder esSalidaEstructurada = new BooleanBuilder()
				.and(qPlantilla.icsTipoPlantilla.idTipoPlantilla.eq(Constants.ID_TIPO_PLANTILLA_SALIDA))
				.and(qPlantilla.icsArchivo.icsTipoExtension.idExtension.eq(Constants.ID_TIPO_EXTENSION_ARCHIVO_ESTRUCTURADO));
		
		BooleanBuilder plantillasVigentesDeSocioEvento = new BooleanBuilder()
				.and(qPlantilla.icsSocio.idSocio.eq(idSocio))
				.and(qPlantilla.icsEvento.idEvento.eq(idEvento))
				.and(qPlantilla.vigencia.eq("Y"))
				.and(esSalidaEstructurada);
		
		long countPlantillas = plantillaRepository.count(plantillasVigentesDeSocioEvento);
		Iterable<IcsPlantilla> iteraPlantillas = plantillaRepository.findAll(plantillasVigentesDeSocioEvento, ordenarPorId);

		// convertir listado
		List<Plantilla> plantillas = new ArrayList<>();
		for (IcsPlantilla icsPlantilla : iteraPlantillas) {
			Plantilla plantilla = this.convertirIcsPlantillaAPojo(icsPlantilla);
			plantillas.add(plantilla);
		}
		
		return new OutputListWrapper<>(plantillas, (int) countPlantillas, "");
	}
	
	@Override
	public PlantillaOutput cargarPlantilla(long idPlantilla) {
		LOG.info("cargarPlantilla");

		try {
			IcsPlantilla entity = plantillaRepository.findOne(idPlantilla);
			return conversionService.convert(entity, PlantillaOutput.class);
		} catch (EntityNotFoundException e) {
			throw new IcsPlantillaNotFoundException("Error al obtener la plantilla", e);
		}
	}
}
