package cl.cardif.ics.icsrest.util.converters.topojo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import cl.cardif.ics.domain.entity.plantillas.IcsAtributosPlantillaIntg;
import cl.cardif.ics.icsrest.domain.dto.output.integracion.ColumnaTrabajoIntgOutput;


@Component
public class AtributoIntegracionPlantillaConverter
		implements Converter<IcsAtributosPlantillaIntg, ColumnaTrabajoIntgOutput> {

	static final Logger LOG = LoggerFactory.getLogger(AtributoIntegracionPlantillaConverter.class);


	@Override
	public ColumnaTrabajoIntgOutput convert(IcsAtributosPlantillaIntg entity) {

		ColumnaTrabajoIntgOutput pojo = new ColumnaTrabajoIntgOutput();
		pojo.setNombre(entity.getNombreColumna());
		pojo.setId(entity.getIdAtributo());
		pojo.setIdPlantilla(entity.getIcsPlantilla().getIdPlantilla());
		pojo.setOrden(entity.getNumeroColumna().longValue());
		pojo.setFormulaSql(entity.getFormulaSql());
		pojo.setFormulaJson(entity.getFormulaJson());
		
		if (entity.getOrdenColumnaArchivo() != null && entity.getOrdenColumnaArchivo().longValue() != 0L) {
			pojo.setOrdenColumnaArchivo(entity.getOrdenColumnaArchivo().longValue());
		}

		return pojo;
	}
}
