package cl.cardif.ics.icsrest.formula.operadores;

import java.util.Map;

import cl.cardif.ics.domain.entity.plantillas.IcsAtributoEnriquecimiento;
import cl.cardif.ics.domain.entity.plantillas.IcsAtributoNegocio;
import cl.cardif.ics.domain.entity.plantillas.IcsAtributoUsuario;
import cl.cardif.ics.icsrest.domain.dto.input.ColumnaTrabajoIntegracionInput;
import cl.cardif.ics.icsrest.exceptions.icsplantilla.IcsPlantillaBadRequestException;
import cl.cardif.ics.icsrest.formula.FormulaFactor;
import cl.cardif.ics.icsrest.formula.IMathFactor;
import cl.cardif.ics.icsrest.formula.IStringFactor;
import cl.cardif.ics.icsrest.service.AtributosService;
import cl.cardif.ics.icsrest.util.FormulaOrigenKey;

public class CampoIntegracion extends FormulaFactor implements IStringFactor, IMathFactor {	
	private ColumnaTrabajoIntegracionInput valor;
	private AtributosService attrSvc;

	public CampoIntegracion() {
		super();
	}

	public ColumnaTrabajoIntegracionInput getValor() {
		return valor;
	}

	public void setValor(ColumnaTrabajoIntegracionInput valor) {
		this.valor = valor;
	}

	public void setAttrSvc(AtributosService svc) {
		this.attrSvc = svc;
	}

	private String origenSQL(Map<FormulaOrigenKey, Integer> tableAliases, boolean soloValidar) {
		
		if (valor.getFormula() != null) {
			return valor.getFormula().toSafeSQL(tableAliases);
		} else {
			if (valor.getColumnaOrigenUser() != null && valor.getColumnaOrigenUser() > 0) {
				IcsAtributoUsuario colOrigen = attrSvc.obtenerAtributoUsuarioPorId(valor.getColumnaOrigenUser());
				IcsAtributoNegocio atrNegocio = colOrigen.getIcsAtributoNegocio();
				String columnaNegocio = atrNegocio.getNombreColumnaNegocio();
				
				if (tableAliases != null) {
					FormulaOrigenKey key = new FormulaOrigenKey(
							colOrigen.getIdAtributoUsuario(), 
							atrNegocio.getIdAtributoNegocio(), 
							atrNegocio.getNombreTablaDestino(), 
							valor.getTipoPersona());
					
					if (tableAliases.containsKey(key)) {	
						Integer numeroAlias = tableAliases.get(key);
						return new StringBuilder()
								.append("T").append(numeroAlias).append(".").append(columnaNegocio)
								.toString();
					} else {
						throw new IcsPlantillaBadRequestException("No se pudo encontrar el alias para la tabla de origen de integracion correspondiente: "+key.toString());
					}
				} else {
					if (!soloValidar) {
						return new StringBuilder()
								.append(columnaNegocio)
								.toString();
					} else {
						return "1";
					}
				}
				
			} else if (valor.getColumnaEnriquecimiento() > 0) {
				if (!soloValidar) {
					IcsAtributoEnriquecimiento colEnriquecimiento = attrSvc.obtenerAtributoEnriquecimientoPorId(valor.getColumnaEnriquecimiento());
					return new StringBuilder()
							.append("PCK_UTIL_ICS.FNC_ENRIQUECIMIENTO(")
								.append("/ENRIQ_EVENTO_ID/,")
								.append(colEnriquecimiento.getIdAtributoEnriquecimiento()).append(",")
								.append("id_transaccion,")
								.append("id_registro")
							.append(")")
							.toString();
				} else {
					return "1";
				}
			} else {
				return "";
			}
		}
	}

	private String transformacionesSQL(String sqlOriginal) {
		
		StringBuilder sb = new StringBuilder().append(sqlOriginal);
		String[] ordenTransformaciones;
		if (valor.getFormato() > 0 && valor.getHomologacion() > 0) {
			if (valor.getOrdenHomologacion() > valor.getOrdenFormato()) {
				ordenTransformaciones = new String[]{ "F", "H" };
			} else {
				ordenTransformaciones = new String[]{ "H", "F" };
			}
		} else if (valor.getHomologacion() > 0) {
			ordenTransformaciones = new String[]{ "H" };
		} else if (valor.getFormato() > 0) {
			ordenTransformaciones = new String[]{ "F" };
		} else {
			ordenTransformaciones = new String[0];
		}

		for (String transformacion : ordenTransformaciones) {

			if (transformacion != null) {
				String sql = sb.toString();
				
				switch (transformacion) {
					case "F":
						sb = new StringBuilder()
							.append("PCK_UTIL_ICS.FNC_FORMATO(")
							.append("(").append(sql).append("), ")
							.append(valor.getFormato()).append(", ")
							.append("'Y'").append(", ")
							.append("'N')");
						break;
					case "H":
						sb = new StringBuilder()
							.append("PCK_UTIL_ICS.FNC_HOMOLOGACION(")
							.append("(").append(sql).append("), ")
							.append(valor.getHomologacion()).append(", ")
							.append("'Y'").append(", ")
							.append("'N')");
						break;
					default:
						throw new IcsPlantillaBadRequestException("Transformación no identificada en columna de trabajo");
				}
			}
		}
		
		return sb.toString();
	}

	@Override
	public String toSafeSQL(Map<FormulaOrigenKey, Integer> tableAliases) {
		if (valor == null) {
			return "";
		} else {
			String sql = this.origenSQL(tableAliases, true);
			sql = this.transformacionesSQL(sql);
			return "(" + sql + ")";
		}
	}

	@Override
	public String toSQL(Map<FormulaOrigenKey, Integer> tableAliases) {
		if (valor == null) {
			return "";
		} else {
			String sql = this.origenSQL(tableAliases, false);
			sql = this.transformacionesSQL(sql);
			return "(" + sql + ")";
		}
	}

	private String campoIntegracionToJSON() {
		StringBuilder sb = new StringBuilder()
				.append("{")
					.append("\"orden\":").append(valor.getOrden()).append(",")
					.append("\"columnaOrigenUser\":").append(valor.getColumnaOrigenUser()).append(",")
					.append("\"columnaEnriquecimiento\":").append(valor.getColumnaEnriquecimiento()).append(",")
					.append("\"ordenHomologacion\":").append(valor.getOrdenHomologacion()).append(",")
					.append("\"ordenFormato\":").append(valor.getOrdenFormato()).append(",");

		if (valor.getFormula() != null) {
			sb.append("\"formula\":").append(valor.getFormula().toJSON()).append(",");
		} else {
			sb.append("\"formula\": null,");
		}

		sb
				.append("\"homologacion\":").append(valor.getHomologacion()).append(",")
				.append("\"formato\":").append(valor.getFormato()).append(",")
				.append("\"nombre\":\"").append(valor.getNombre()).append("\"")
			.append("}");

		return sb.toString();
	}

	@Override
	public String toJSON() {
		StringBuilder sb = new StringBuilder()
				.append("{")
					.append("\"tipo\":\"CampoIntegracion\"");
		if (valor != null) {
			sb.append(",").append("\"valor\":").append(campoIntegracionToJSON());
		}
		sb.append("}");

		return sb.toString();
	}

}
