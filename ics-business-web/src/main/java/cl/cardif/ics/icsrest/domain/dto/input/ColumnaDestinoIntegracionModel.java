package cl.cardif.ics.icsrest.domain.dto.input;

public class ColumnaDestinoIntegracionModel extends ColumnaDestinoIntegracion {

  public ColumnaDestinoIntegracionModel() {
    super();
  }

  public ColumnaDestinoIntegracionModel(long idIntegracionEvento) {
    super();
    this.idIntegracionEvento = idIntegracionEvento;
  }
}
