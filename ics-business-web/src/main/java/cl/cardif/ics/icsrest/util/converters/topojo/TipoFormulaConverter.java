package cl.cardif.ics.icsrest.util.converters.topojo;

import cl.cardif.ics.domain.entity.common.IcsTipoFormula;
import cl.cardif.ics.icsrest.domain.pojo.TipoFormula;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class TipoFormulaConverter implements Converter<IcsTipoFormula, TipoFormula> {

  @Override
  public TipoFormula convert(IcsTipoFormula entity) {
    TipoFormula pojo = new TipoFormula();
    pojo.setId(entity.getIdTipoFormula());
    pojo.setNombre(entity.getNombreFormula());
    pojo.setEstructura(entity.getEstructuraFormula());
    pojo.setTipoDatoResultante(entity.getTipoDatoResultante());
    pojo.setVigencia(entity.getVigenciaFormula());
    return pojo;
  }
}
