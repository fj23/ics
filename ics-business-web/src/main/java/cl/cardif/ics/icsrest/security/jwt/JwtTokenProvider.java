package cl.cardif.ics.icsrest.security.jwt;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import cl.cardif.ics.icsrest.message.response.AuthLoginResponse;
import cl.cardif.ics.icsrest.security.services.UserDetailsImpl;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;

@Component
public class JwtTokenProvider {

	static final Logger LOG = LoggerFactory.getLogger(JwtTokenProvider.class);

	@Autowired
	private JwtParser jwtParser;

	@Autowired
	private JwtBuilder jwtBuilder;

	@Value("${security.jwt.token.expire-length}")
	private Integer EXPIRATION_MINUTES;

	@Autowired
	private UserDetailsImpl userDetails;

	public String createToken(AuthLoginResponse authLoginResponse) {
		Claims claims = Jwts.claims();
		claims.setSubject(authLoginResponse.getUsername());
		claims.setIssuedAt(new Date());
		claims.setExpiration(new Date(System.currentTimeMillis() + (60000 * EXPIRATION_MINUTES)));
		claims.put("sesame-token", authLoginResponse.getToken());
		claims.put("roles", authLoginResponse.getRoles());
		return this.jwtBuilder.addClaims(claims).compact();
	}

	public Authentication getAuthentication(String token) {
		UserDetails userDetails = this.userDetails.loadUserByUsername(token);
		return new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
	}

	public String resolveToken(HttpServletRequest req) {
		String bearerToken = req.getHeader("Authorization");
		if (bearerToken != null && bearerToken.startsWith("Bearer ")) {
			return bearerToken.substring(7);
		}
		return null;
	}

	public boolean validateJwtToken(String token) {
		try {
			this.jwtParser.parseClaimsJws(token);
		} catch (JwtException | IllegalArgumentException e) {
			LOG.info("Expired/invalid JWT token", e);
			return false;
		}
		return true;
	}
}
