package cl.cardif.ics.icsrest.exceptions.icsdetallesubestructura;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.GONE)
public class IcsDetalleSubestructuraGoneException extends RuntimeException {

  private static final long serialVersionUID = -7209750264039464453L;

  public IcsDetalleSubestructuraGoneException(String exception) {
    super(exception);
  }
}
