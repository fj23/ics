package cl.cardif.ics.icsrest.exceptions.icsdetalleestructura;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class IcsDetalleEstructuraUnauthorizedException extends RuntimeException {

  private static final long serialVersionUID = 3815682526998866691L;

  public IcsDetalleEstructuraUnauthorizedException(String exception) {
    super(exception);
  }
}
