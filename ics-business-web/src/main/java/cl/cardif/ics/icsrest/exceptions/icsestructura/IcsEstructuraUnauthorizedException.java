package cl.cardif.ics.icsrest.exceptions.icsestructura;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class IcsEstructuraUnauthorizedException extends RuntimeException {

  private static final long serialVersionUID = 3815682526998866691L;

  public IcsEstructuraUnauthorizedException(String exception) {
    super(exception);
  }

  public IcsEstructuraUnauthorizedException(String exception, Throwable t) {
    super(exception, t);
  }
}
