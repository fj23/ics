package cl.cardif.ics.icsrest.domain.dto.input;

public abstract class ColumnaDestinoIntegracion {
  protected long idIntegracionEvento;
  protected long idEvento;
  protected String nombreColumnaIntegracion;
  protected String nombreTablaDestino;

  public long getIdEvento() {
    return idEvento;
  }

  public void setIdEvento(long idEvento) {
    this.idEvento = idEvento;
  }

  public long getIdIntegracionEvento() {
    return idIntegracionEvento;
  }

  public void setIdIntegracionEvento(long idIntegracionEvento) {
    this.idIntegracionEvento = idIntegracionEvento;
  }

  public String getNombreColumnaIntegracion() {
    return nombreColumnaIntegracion;
  }

  public void setNombreColumnaIntegracion(String nombreColumnaIntegracion) {
    this.nombreColumnaIntegracion = nombreColumnaIntegracion;
  }

  public String getNombreTablaDestino() {
    return nombreTablaDestino;
  }

  public void setNombreTablaDestino(String nombreTablaDestino) {
    this.nombreTablaDestino = nombreTablaDestino;
  }
}
