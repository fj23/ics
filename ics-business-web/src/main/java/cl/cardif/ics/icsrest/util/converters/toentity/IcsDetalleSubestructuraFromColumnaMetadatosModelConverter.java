package cl.cardif.ics.icsrest.util.converters.toentity;

import cl.cardif.ics.domain.entity.common.IcsTipoDato;
import cl.cardif.ics.domain.entity.common.QIcsTipoDato;
import cl.cardif.ics.domain.entity.estructuras.IcsDetalleSubestructura;
import cl.cardif.ics.icsrest.domain.dto.input.ColumnaMetadatosModel;
import cl.cardif.ics.icsrest.exceptions.icstipodato.IcsTipoDatoNotFoundException;
import cl.cardif.ics.icsrest.repository.TiposDatosRepository;
import com.querydsl.core.BooleanBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import java.math.BigDecimal;

@Component
public class IcsDetalleSubestructuraFromColumnaMetadatosModelConverter
    implements Converter<ColumnaMetadatosModel, IcsDetalleSubestructura> {

  static final Logger LOG =
      LoggerFactory.getLogger(IcsDetalleSubestructuraFromColumnaMetadatosModelConverter.class);

  @Autowired TiposDatosRepository tiposDatosRepo;

  @Override
  public IcsDetalleSubestructura convert(ColumnaMetadatosModel pojo) {
    LOG.info("Convertir ColumnaMetadatosModel a IcsDetalleSubestructura");

    IcsDetalleSubestructura detalleEntity = new IcsDetalleSubestructura();

    detalleEntity.setDescripcionDetalleSubestructura(pojo.getDescripcion());
    detalleEntity.setCodigoDetalleSubestructura(pojo.getCodigo());
    detalleEntity.setLargo(BigDecimal.valueOf(pojo.getLargo()));
    detalleEntity.setOrden(BigDecimal.valueOf(pojo.getOrden()));

    BooleanBuilder booleanBuilder = new BooleanBuilder();
    QIcsTipoDato qIcsTipoDato = QIcsTipoDato.icsTipoDato;
    booleanBuilder.and(qIcsTipoDato.idTipoDato.eq(pojo.getTipoDato()));

    IcsTipoDato tipoEntity = null;
    try {
      tipoEntity = this.tiposDatosRepo.findOne(booleanBuilder);
    } catch (EntityNotFoundException e) {
      throw new IcsTipoDatoNotFoundException("Tipo de datos no se encuentra", e);
    }

    if (tipoEntity != null) {
      detalleEntity.setIcsTipoDato(tipoEntity);
    }

    detalleEntity.setTipoDato("");
    detalleEntity.setVigenciaDetalleSubestructura(pojo.getVigencia());
    return detalleEntity;
  }
}
