package cl.cardif.ics.icsrest.util.converters.topojo;

import cl.cardif.ics.domain.entity.plantillas.IcsAtributos;
import cl.cardif.ics.icsrest.domain.dto.output.integracion.ColumnaArchivoSalidaIntegracionOutput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class AtributoIntegracionArchivosConvert
		implements Converter<IcsAtributos, ColumnaArchivoSalidaIntegracionOutput> {

	static final Logger LOG = LoggerFactory.getLogger(AtributoIntegracionArchivosConvert.class);

	@Override
	public ColumnaArchivoSalidaIntegracionOutput convert(IcsAtributos entity) {

		ColumnaArchivoSalidaIntegracionOutput pojo = new ColumnaArchivoSalidaIntegracionOutput();

		pojo.setId(entity.getIdAtributo());
		pojo.setOrden(entity.getNumeroColumna());
		pojo.setPosicionInicial(entity.getPosicionInicial());
		pojo.setPosicionFinal(entity.getPosicionFinal());
		pojo.setSepararArchivo(entity.getFlgAgrupacion().equals("Y"));
		pojo.setIncluir(entity.getFlgCarga().equals("Y"));

		return pojo;
	}
}
