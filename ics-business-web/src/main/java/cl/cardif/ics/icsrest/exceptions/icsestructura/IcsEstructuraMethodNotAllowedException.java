package cl.cardif.ics.icsrest.exceptions.icsestructura;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
public class IcsEstructuraMethodNotAllowedException extends RuntimeException {

  private static final long serialVersionUID = -6814524510236406273L;

  public IcsEstructuraMethodNotAllowedException(String exception) {
    super(exception);
  }

  public IcsEstructuraMethodNotAllowedException(String exception, Throwable t) {
    super(exception, t);
  }
}
