package cl.cardif.ics.icsrest.repository;

import cl.cardif.ics.domain.entity.common.IcsEvento;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface EventosRepository
    extends JpaRepository<IcsEvento, Long>, 
    	QueryDslPredicateExecutor<IcsEvento> {
	
}
