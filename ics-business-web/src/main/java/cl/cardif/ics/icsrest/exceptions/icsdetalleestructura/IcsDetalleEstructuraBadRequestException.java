package cl.cardif.ics.icsrest.exceptions.icsdetalleestructura;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class IcsDetalleEstructuraBadRequestException extends RuntimeException {

  private static final long serialVersionUID = -6459654653826812166L;

  public IcsDetalleEstructuraBadRequestException(String exception) {
    super(exception);
  }
}
