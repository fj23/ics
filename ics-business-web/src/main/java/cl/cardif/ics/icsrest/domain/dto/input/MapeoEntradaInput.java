package cl.cardif.ics.icsrest.domain.dto.input;

import cl.cardif.ics.icsrest.domain.dto.output.AtributoUsuarioOutput;
import cl.cardif.ics.icsrest.domain.dto.output.DetalleEstructuraOutput;
import cl.cardif.ics.icsrest.domain.dto.output.EstructuraOutput;
import cl.cardif.ics.icsrest.domain.dto.output.SubEstructuraOutput;
import cl.cardif.ics.icsrest.domain.pojo.Archivo;

public class MapeoEntradaInput {

	private Long columnaDestino;
	private String atributoNegocioNombre;
	private Long archivoOrigen;
	private String archivoNombre;
	private Long columnaOrigen;
	private String columnaNombre;
	private String columnaOrder;
	private boolean esObligatorio;
	private Long estructuraOrigenId;
	private Long subEstructuraColumnaOrigen;
	private String valorParaFiltrar;
	private Boolean filtroExclusion;
	private Long grupoColumnaDestino;
	private Long idAtributoUsuario;
	private Boolean encriptar; 

	private Archivo archivoOrigenDatosEM;
	private DetalleEstructuraOutput columnaOrigenDatosEM;
	private AtributoUsuarioOutput columnaDestinoEM;
	private EstructuraOutput estructuraOrigenEM;
	private SubEstructuraOutput subEstructuraColumnaOrigenEM;

	private Long tipoDato;
	private Long lote;

	public MapeoEntradaInput() {
		super();
	}

	public Long getColumnaDestino() {
		return columnaDestino;
	}

	public void setColumnaDestino(Long columnaDestino) {
		this.columnaDestino = columnaDestino;
	}

	public String getAtributoNegocioNombre() {
		return atributoNegocioNombre;
	}

	public void setAtributoNegocioNombre(String atributoNegocioNombre) {
		this.atributoNegocioNombre = atributoNegocioNombre;
	}

	public Long getArchivoOrigen() {
		return archivoOrigen;
	}

	public void setArchivoOrigen(Long archivoOrigen) {
		this.archivoOrigen = archivoOrigen;
	}

	public String getArchivoNombre() {
		return archivoNombre;
	}

	public void setArchivoNombre(String archivoNombre) {
		this.archivoNombre = archivoNombre;
	}

	public Long getColumnaOrigen() {
		return columnaOrigen;
	}

	public void setColumnaOrigen(Long columnaOrigen) {
		this.columnaOrigen = columnaOrigen;
	}

	public String getColumnaNombre() {
		return columnaNombre;
	}

	public void setColumnaNombre(String columnaNombre) {
		this.columnaNombre = columnaNombre;
	}

	public String getColumnaOrder() {
		return columnaOrder;
	}

	public void setColumnaOrder(String columnaOrder) {
		this.columnaOrder = columnaOrder;
	}

	public Boolean getEsObligatorio() {
		return esObligatorio;
	}

	public void setEsObligatorio(Boolean esObligatorio) {
		this.esObligatorio = esObligatorio;
	}

	public Long getEstructuraOrigenId() {
		return estructuraOrigenId;
	}

	public void setEstructuraOrigenId(Long estructuraOrigenId) {
		this.estructuraOrigenId = estructuraOrigenId;
	}

	public Long getSubEstructuraColumnaOrigen() {
		return subEstructuraColumnaOrigen;
	}

	public void setSubEstructuraColumnaOrigen(Long subEstructuraColumnaOrigen) {
		this.subEstructuraColumnaOrigen = subEstructuraColumnaOrigen;
	}

	public String getValorParaFiltrar() {
		return valorParaFiltrar;
	}

	public void setValorParaFiltrar(String valorParaFiltrar) {
		this.valorParaFiltrar = valorParaFiltrar;
	}

	public Boolean isFiltroExclusion() {
		return filtroExclusion;
	}

	public void setFiltroExclusion(Boolean filtroExclusion) {
		this.filtroExclusion = filtroExclusion;
	}

	public Long getGrupoColumnaDestino() {
		return grupoColumnaDestino;
	}

	public void setGrupoColumnaDestino(Long grupoColumnaDestino) {
		this.grupoColumnaDestino = grupoColumnaDestino;
	}

	public Long getIdAtributoUsuario() {
		return idAtributoUsuario;
	}

	public void setIdAtributoUsuario(Long idAtributoUsuario) {
		this.idAtributoUsuario = idAtributoUsuario;
	}

	public Archivo getArchivoOrigenDatosEM() {
		return archivoOrigenDatosEM;
	}

	public void setArchivoOrigenDatosEM(Archivo archivoOrigenDatosEM) {
		this.archivoOrigenDatosEM = archivoOrigenDatosEM;
	}

	public DetalleEstructuraOutput getColumnaOrigenDatosEM() {
		return columnaOrigenDatosEM;
	}

	public void setColumnaOrigenDatosEM(DetalleEstructuraOutput columnaOrigenDatosEM) {
		this.columnaOrigenDatosEM = columnaOrigenDatosEM;
	}

	public AtributoUsuarioOutput getColumnaDestinoEM() {
		return columnaDestinoEM;
	}

	public void setColumnaDestinoEM(AtributoUsuarioOutput columnaDestinoEM) {
		this.columnaDestinoEM = columnaDestinoEM;
	}

	public EstructuraOutput getEstructuraOrigenEM() {
		return estructuraOrigenEM;
	}

	public void setEstructuraOrigenEM(EstructuraOutput estructuraOrigenEM) {
		this.estructuraOrigenEM = estructuraOrigenEM;
	}

	public SubEstructuraOutput getSubEstructuraColumnaOrigenEM() {
		return subEstructuraColumnaOrigenEM;
	}

	public void setSubEstructuraColumnaOrigenEM(SubEstructuraOutput subEstructuraColumnaOrigenEM) {
		this.subEstructuraColumnaOrigenEM = subEstructuraColumnaOrigenEM;
	}

	public Long getTipoDato() {
		return tipoDato;
	}

	public void setTipoDato(Long tipoDato) {
		this.tipoDato = tipoDato;
	}

	public Long getLote() {
		return lote;
	}

	public void setLote(Long lote) {
		this.lote = lote;
	}

	public Boolean getEncriptar() {
		return encriptar;
	}

	public void setEncriptar(Boolean encriptar) {
		this.encriptar = encriptar;
	}

	@Override
	public String toString() {
		return "MapeoEntradaInput [columnaDestino=" + columnaDestino + ", atributoNegocioNombre="
				+ atributoNegocioNombre + ", archivoOrigen=" + archivoOrigen + ", archivoNombre=" + archivoNombre
				+ ", columnaOrigen=" + columnaOrigen + ", columnaNombre=" + columnaNombre + ", columnaOrder="
				+ columnaOrder + ", esObligatorio=" + esObligatorio + ", estructuraOrigenId=" + estructuraOrigenId
				+ ", subEstructuraColumnaOrigen=" + subEstructuraColumnaOrigen + ", valorParaFiltrar="
				+ valorParaFiltrar + ", filtroExclusion=" + filtroExclusion + ", grupoColumnaDestino="
				+ grupoColumnaDestino + ", idAtributoUsuario=" + idAtributoUsuario + ", archivoOrigenDatosEM="
				+ archivoOrigenDatosEM + ", columnaOrigenDatosEM=" + columnaOrigenDatosEM + ", columnaDestinoEM="
				+ columnaDestinoEM + ", estructuraOrigenEM=" + estructuraOrigenEM + ", subEstructuraColumnaOrigenEM="
				+ subEstructuraColumnaOrigenEM + ", tipoDato=" + tipoDato + ", lote=" + lote + "]";
	}

}
