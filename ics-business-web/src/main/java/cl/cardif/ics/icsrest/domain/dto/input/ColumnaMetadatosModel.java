package cl.cardif.ics.icsrest.domain.dto.input;

public class ColumnaMetadatosModel extends ColumnaMetadatos {
  private long largo;

  public ColumnaMetadatosModel() {
    super();
  }

  public ColumnaMetadatosModel(long id) {
    super();
    this.id = id;
  }

  public long getLargo() {
    return largo;
  }

  public void setLargo(long largo) {
    this.largo = largo;
  }

	@Override
	public String toString() {
		return "ColumnaMetadatosModel [largo=" + largo + ", orden=" + orden + ", tipoDato=" + tipoDato
				+ ", posicionInicial=" + posicionInicial + ", posicionFinal=" + posicionFinal + ", subEstructura="
				+ subEstructura + ", vigencia=" + vigencia + ", mapeada=" + mapeada + ", id=" + id + ", codigo=" + codigo
				+ ", descripcion=" + descripcion + "]";
	}
  
  
}
