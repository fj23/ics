package cl.cardif.ics.icsrest.exceptions.icsdetallesubestructura;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class IcsDetalleSubestructuraNotFoundException extends RuntimeException {

  private static final long serialVersionUID = -1527596879157446910L;

  public IcsDetalleSubestructuraNotFoundException(String exception) {
    super(exception);
  }
}
