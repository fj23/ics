package cl.cardif.ics.icsrest.util.converters.topojo;

import cl.cardif.ics.domain.entity.estructuras.IcsEstructura;
import cl.cardif.ics.icsrest.domain.dto.output.EstructuraSimpleOutput;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class EstructuraSimpleConverter implements Converter<IcsEstructura, EstructuraSimpleOutput> {

  @Override
  public EstructuraSimpleOutput convert(IcsEstructura icsEstructura) {

    EstructuraSimpleOutput estructura = new EstructuraSimpleOutput();
    estructura.setId(icsEstructura.getIdEstructura());
    estructura.setDescripcion(icsEstructura.getDescripcionEstructura());
    estructura.setCodigo(icsEstructura.getCodigoEstructura());

    return estructura;
  }
}
