package cl.cardif.ics.icsrest.service;

import java.util.List;

import cl.cardif.ics.domain.entity.plantillas.IcsAtributoEnriquecimiento;
import cl.cardif.ics.domain.entity.plantillas.IcsAtributoUsuario;
import cl.cardif.ics.icsrest.domain.dto.output.AtributoEnriquecimientoOutput;
import cl.cardif.ics.icsrest.domain.dto.output.AtributoNegocioOutput;
import cl.cardif.ics.icsrest.domain.dto.output.AtributoUserIntgOutput;
import cl.cardif.ics.icsrest.domain.dto.output.AtributoUsuarioOutput;
import cl.cardif.ics.icsrest.domain.dto.output.integracion.AtributosIntgOutput;

public interface AtributosService {

  List<AtributoEnriquecimientoOutput> obtenerAtributosEnriquecimientoPorCore(Long idCore);

  List<AtributosIntgOutput> obtenerAtributosIntegracion();

  List<AtributoUserIntgOutput> obtenerAtributosDestinoIntegracionPorEvento(Long idEvent);

  List<AtributoNegocioOutput> obtenerAtributosNegocio();

  IcsAtributoEnriquecimiento obtenerAtributoEnriquecimientoPorId(Long id);

  IcsAtributoUsuario obtenerAtributoUsuarioPorId(Long id);

  List<AtributoUsuarioOutput> obtenerAtributosUsuarioPorEvento(Long idEvent);
}
