package cl.cardif.ics.icsrest.util.converters.topojo;

import cl.cardif.ics.domain.entity.common.IcsTipoSalida;
import cl.cardif.ics.icsrest.domain.pojo.TipoSalida;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class TipoSalidaConverter implements Converter<IcsTipoSalida, TipoSalida> {

  static final Logger LOG = LoggerFactory.getLogger(TipoSalidaConverter.class);

  @Override
  public TipoSalida convert(IcsTipoSalida entity) {

    TipoSalida pojo = new TipoSalida();
    pojo.setIdTipoSalida(entity.getIdTipoSalida());
    pojo.setDescripcion(entity.getDescripcion());
    pojo.setVigencia(entity.getVigencia());
    return pojo;
  }
}
