package cl.cardif.ics.icsrest.util.converters.topojo;

import cl.cardif.ics.domain.entity.common.IcsCore;
import cl.cardif.ics.domain.entity.common.IcsSocio;
import cl.cardif.ics.domain.entity.homologaciones.IcsDetalleHomologacion;
import cl.cardif.ics.domain.entity.homologaciones.IcsTipoHomologacion;
import cl.cardif.ics.icsrest.domain.pojo.Core;
import cl.cardif.ics.icsrest.domain.pojo.Homologacion;
import cl.cardif.ics.icsrest.domain.pojo.Socio;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class HomologacionConverter implements Converter<IcsDetalleHomologacion, Homologacion> {

  static final Logger LOG = LoggerFactory.getLogger(HomologacionConverter.class);

  @Override
  public Homologacion convert(IcsDetalleHomologacion icsDetalleHomologacion) {
    LOG.info("Conversion Tipo Homologacion : ");
    Homologacion homologacion = new Homologacion();
    homologacion.setId(icsDetalleHomologacion.getIdHomologacion());

    IcsTipoHomologacion tipoHomologacion = icsDetalleHomologacion.getIcsTipoHomologacion();
    Socio socio1 = new Socio();
    Socio socio2 = new Socio();
    Core core1 = new Core();
    Core core2 = new Core();

    if (tipoHomologacion != null) {
      homologacion.setIdTipo(tipoHomologacion.getIdDeTipoHomologacion());

      IcsSocio icsSocio1 = tipoHomologacion.getIcsSocio1();
      if (icsSocio1 != null) {
        socio1.setId(icsSocio1.getIdSocio());
        socio1.setNombre(icsSocio1.getNombreSocio());
        socio1.setVigencia(icsSocio1.getVigenciaSocio());
      }

      IcsSocio icsSocio2 = tipoHomologacion.getIcsSocio2();
      if (icsSocio2 != null) {
        socio2.setId(icsSocio2.getIdSocio());
        socio2.setNombre(icsSocio2.getNombreSocio());
        socio2.setVigencia(icsSocio2.getVigenciaSocio());
      }

      IcsCore icsCore1 = tipoHomologacion.getIcsCore1();
      if (icsCore1 != null) {
        core1.setId(icsCore1.getIdCore());
        core1.setNombre(icsCore1.getNombreCore());
        core1.setFlgVisible(icsCore1.getFlgVisible());
        core1.setVigencia(icsCore1.getVigenciaCore());
      }

      IcsCore icsCore2 = tipoHomologacion.getIcsCore2();
      if (icsCore2 != null) {
        core2.setId(icsCore2.getIdCore());
        core2.setNombre(icsCore2.getNombreCore());
        core2.setFlgVisible(icsCore2.getFlgVisible());
        core2.setVigencia(icsCore2.getVigenciaCore());
      }

      homologacion.setConcepto(tipoHomologacion.getNombreHomologacion());
    }

    homologacion.setSocio1(socio1);
    homologacion.setSocio2(socio2);

    homologacion.setCore1(core1);
    homologacion.setCore2(core2);

    homologacion.setVigencia(icsDetalleHomologacion.getVigenciaHomologacion());

    homologacion.setValor1(icsDetalleHomologacion.getValorSocio());
    homologacion.setValor2(icsDetalleHomologacion.getValorCore());

    return homologacion;
  }
}
