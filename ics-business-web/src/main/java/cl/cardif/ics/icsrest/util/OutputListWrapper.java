package cl.cardif.ics.icsrest.util;

import java.util.ArrayList;
import java.util.List;

/**
 * Objeto que prepara las respuestas de la API al front-end de la aplicación web.
 *
 */
public class OutputListWrapper<T> {
	private List<T> items;
	private int count;
	private String message;

	public OutputListWrapper() {
		super();
	}

	public OutputListWrapper(List<T> items, int count, String message) {
		this.items = new ArrayList<>(items);
		this.count = count;
		this.message = message;
	}

	public OutputListWrapper(List<T> items, String message) {
		this.items = new ArrayList<>(items);
		this.count = items.size();
		this.message = message;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public List<T> getItems() {
		return new ArrayList<>(items);
	}

	public void setItems(List<T> items) {
		this.items = new ArrayList<>(items);
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
