package cl.cardif.ics.icsrest.formula;

import java.util.Collection;

public interface IFormulaParent extends IFormulaFactor {
	public int getMaximumChildrenCount();

	public String childrenToJSON();

	public Collection<IFormulaFactor> getHijos();

	public void setChildren(Collection<IFormulaFactor> children);
}
