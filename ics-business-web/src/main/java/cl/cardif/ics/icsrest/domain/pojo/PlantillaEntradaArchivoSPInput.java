package cl.cardif.ics.icsrest.domain.pojo;

import java.util.ArrayList;
import java.util.List;

public class PlantillaEntradaArchivoSPInput {
	private String nombre;
	private String delimitador;
	private int lineaInicial;
	private int lineaFinal;
	private int numeroHoja;
	private String estructurasAgrupadas;
	private List<String> atributosSocios;

	public PlantillaEntradaArchivoSPInput() {
		super();
	}

	public List<String> getAtributosSocios() {
		if (atributosSocios != null) {
			return new ArrayList<>(atributosSocios);
		} else {
			return new ArrayList<>();
		}
	}

	public void setAtributosSocios(List<String> atributosSocios) {
		if (atributosSocios != null) {
			this.atributosSocios = new ArrayList<>(atributosSocios);
		}
	}

	public String getDelimitador() {
		return delimitador;
	}

	public void setDelimitador(String delimitador) {
		this.delimitador = delimitador;
	}

	public String getEstructurasAgrupadas() {
		return estructurasAgrupadas;
	}

	public void setEstructurasAgrupadas(String estructurasAgrupadas) {
		this.estructurasAgrupadas = estructurasAgrupadas;
	}

	public int getLineaFinal() {
		return lineaFinal;
	}

	public void setLineaFinal(int lineaFinal) {
		this.lineaFinal = lineaFinal;
	}

	public int getLineaInicial() {
		return lineaInicial;
	}

	public void setLineaInicial(int lineaInicial) {
		this.lineaInicial = lineaInicial;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getNumeroHoja() {
		return numeroHoja;
	}

	public void setNumeroHoja(int numeroHoja) {
		this.numeroHoja = numeroHoja;
	}
}
