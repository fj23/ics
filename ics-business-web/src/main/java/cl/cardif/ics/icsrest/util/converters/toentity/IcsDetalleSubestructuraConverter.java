package cl.cardif.ics.icsrest.util.converters.toentity;

import cl.cardif.ics.domain.entity.common.IcsTipoDato;
import cl.cardif.ics.domain.entity.estructuras.IcsDetalleSubestructura;
import cl.cardif.ics.icsrest.domain.pojo.DetalleSubEstructura;
import cl.cardif.ics.icsrest.domain.pojo.TipoDato;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class IcsDetalleSubestructuraConverter
    implements Converter<DetalleSubEstructura, IcsDetalleSubestructura> {

  static final Logger LOG = LoggerFactory.getLogger(IcsDetalleSubestructuraConverter.class);

  @Override
  public IcsDetalleSubestructura convert(DetalleSubEstructura pojo) {

    IcsDetalleSubestructura detalleEntity = new IcsDetalleSubestructura();

    LOG.info("Detalle obtenido con nombre: " + pojo.getNombre());
    detalleEntity.setIdDetalleSubestructura(pojo.getId());
    detalleEntity.setDescripcionDetalleSubestructura(pojo.getDescripcion());
    detalleEntity.setLargo(pojo.getLargo());
    detalleEntity.setOrden(pojo.getOrden());

    TipoDato tipoDato = pojo.getTipoDato();
    IcsTipoDato tipoEntity = new IcsTipoDato();

    tipoEntity.setIdTipoDato(tipoDato.getId());
    tipoEntity.setNombreTipoDato(tipoDato.getNombre());

    detalleEntity.setIcsTipoDato(tipoEntity);
    detalleEntity.setTipoDato("");
    detalleEntity.setVigenciaDetalleSubestructura(pojo.getVigencia());
    return detalleEntity;
  }
}
