package cl.cardif.ics.icsrest.exceptions.icsproceso;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.GONE)
public class IcsProcesoGoneException extends RuntimeException {

  private static final long serialVersionUID = -7209750264039464453L;

  public IcsProcesoGoneException(String exception) {
    super(exception);
  }

  public IcsProcesoGoneException(String exception, Throwable t) {
    super(exception, t);
  }
}
