package cl.cardif.ics.icsrest.exceptions.icsestructura;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class IcsEstructuraNotFoundException extends RuntimeException {

  private static final long serialVersionUID = -1527596879157446910L;

  public IcsEstructuraNotFoundException(String exception) {
    super(exception);
  }

  public IcsEstructuraNotFoundException(String exception, Throwable t) {
    super(exception, t);
  }
}
