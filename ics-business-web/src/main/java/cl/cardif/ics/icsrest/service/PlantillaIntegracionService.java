package cl.cardif.ics.icsrest.service;

import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;

import cl.cardif.ics.icsrest.domain.dto.input.PlantillaIntegracionInput;
import cl.cardif.ics.icsrest.domain.dto.output.AtributoUsuarioOutput;
import cl.cardif.ics.icsrest.domain.dto.output.PlantillaOutput;
import cl.cardif.ics.icsrest.domain.dto.output.RequestResultOutput;
import cl.cardif.ics.icsrest.util.Permisos;

public interface PlantillaIntegracionService {

	@PreAuthorize(value = "hasAnyAuthority(" + Permisos.Mant_Plan_Alta + "," + Permisos.Mant_Plan_Canc + ","
			+ Permisos.Mant_Plan_Reca + "," + Permisos.Mant_Plan_Prosp + "," + Permisos.Mant_Plan_Intera + ","
			+ Permisos.Mant_Plan_Pago + ")")
	RequestResultOutput addOrUpdatePlantillaIntegracion(PlantillaIntegracionInput plantilla);

	@PreAuthorize(value = "hasAnyAuthority(" + Permisos.Mant_Plan_Alta + "," + Permisos.Mant_Plan_Canc + ","
			+ Permisos.Mant_Plan_Reca + "," + Permisos.Mant_Plan_Prosp + "," + Permisos.Mant_Plan_Intera + ","
			+ Permisos.Mant_Plan_Pago + ")")
	RequestResultOutput callSpAlmacenarPlantilla(PlantillaIntegracionInput plantilla, boolean isManual);

	@PreAuthorize(value = "hasAnyAuthority(" + Permisos.Mant_Plan_Alta + "," + Permisos.Mant_Plan_Canc + ","
			+ Permisos.Mant_Plan_Reca + "," + Permisos.Mant_Plan_Prosp + "," + Permisos.Mant_Plan_Intera + ","
			+ Permisos.Mant_Plan_Pago + ")")
	boolean testFormulaIntegracion(String input);

	@PreAuthorize(value = "hasAnyAuthority(" + Permisos.Cslt_Plan + "," + Permisos.Mant_Plan_Alta + ","
			+ Permisos.Mant_Plan_Canc + "," + Permisos.Mant_Plan_Reca + "," + Permisos.Mant_Plan_Prosp + ","
			+ Permisos.Mant_Plan_Intera + "," + Permisos.Mant_Plan_Pago + ")")
	PlantillaOutput viewTemplateDetalle(PlantillaOutput plantilla);

	/**
	 * Obtiene todos los atributos de usuario que han sido mapeados desde una
	 * plantilla de entrada.
	 * 
	 * @param plantillaId
	 *            Id de la plantilla de entrada.
	 * @return List&lt;{@link AtributoUsuarioOutput}&gt; Atributos de usuario que
	 *         han sido mapeados desde una plantilla de entrada.
	 */
	@PreAuthorize(value = "hasAnyAuthority(" + Permisos.Cslt_Plan + "," + Permisos.Mant_Plan_Alta + ","
			+ Permisos.Mant_Plan_Canc + "," + Permisos.Mant_Plan_Reca + "," + Permisos.Mant_Plan_Prosp + ","
			+ Permisos.Mant_Plan_Intera + "," + Permisos.Mant_Plan_Pago + ")")
	List<AtributoUsuarioOutput> getAtributosUsuarioFromPlantillaEntradaMapeos(long plantillaId);
}
