package cl.cardif.ics.icsrest.domain.pojo;

import java.util.ArrayList;
import java.util.List;

public class FileIcs {

	private List<String> listArchive;
	private List<InputRow> listInputFile;
	private int error;
	private String desError;
	private String outType;

	public FileIcs(final List<String> listArchive, final List<InputRow> listInputFile, final int error,
			final String desError, final String outType) {
		List<String> newListArchive = new ArrayList<>(listArchive);
		this.listArchive = newListArchive;
		List<InputRow> newListInputFile = new ArrayList<>(listInputFile);
		this.listInputFile = newListInputFile;
		this.error = error;
		this.desError = desError;
		this.outType = outType;
	}

	public List<String> getListArchive() {
		return new ArrayList<>(listArchive);
	}

	public List<InputRow> getListInputFile() {
		return new ArrayList<>(listInputFile);
	}

	public String getOutType() {
		return outType;
	}

	public int getError() {
		return error;
	}

	public String getDesError() {
		return desError;
	}
}
