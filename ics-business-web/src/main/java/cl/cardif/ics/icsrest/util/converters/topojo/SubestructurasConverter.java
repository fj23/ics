package cl.cardif.ics.icsrest.util.converters.topojo;

import cl.cardif.ics.domain.entity.common.IcsTipoDato;
import cl.cardif.ics.domain.entity.estructuras.IcsDetalleSubestructura;
import cl.cardif.ics.domain.entity.estructuras.IcsSubestructura;
import cl.cardif.ics.icsrest.domain.dto.input.TipoDatoModel;
import cl.cardif.ics.icsrest.domain.dto.output.DetalleSubEstructuraOutput;
import cl.cardif.ics.icsrest.domain.dto.output.SubEstructuraOutput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

@Component
public class SubestructurasConverter implements Converter<IcsSubestructura, SubEstructuraOutput> {
  static final Logger LOG = LoggerFactory.getLogger(SubestructurasConverter.class);

  @Override
  public SubEstructuraOutput convert(IcsSubestructura icsSubestructura) {

    SubEstructuraOutput subestructura = new SubEstructuraOutput();
    subestructura.setId(icsSubestructura.getIdSubestructura());
    subestructura.setDescripcion(icsSubestructura.getDescripcionSubestructura());
    subestructura.setCodigo(icsSubestructura.getCodigoSubestructura());
    subestructura.setVigencia(icsSubestructura.getVigenciaSubestructura());

    List<DetalleSubEstructuraOutput> detalles = new ArrayList<>();

    int largo = 0;
    
    for (int i = 0; i < icsSubestructura.getIcsDetalleSubestructuras().size(); i++) {
      IcsDetalleSubestructura entity = icsSubestructura.getIcsDetalleSubestructuras().get(i);
      DetalleSubEstructuraOutput detalle = new DetalleSubEstructuraOutput();
      detalle.setId(entity.getIdDetalleSubestructura());
      detalle.setDescripcion(entity.getDescripcionDetalleSubestructura());
      detalle.setCodigo(entity.getCodigoDetalleSubestructura());

      try {
        if (entity.getIcsTipoDato() != null) {
          IcsTipoDato icsTipoDato = entity.getIcsTipoDato();
          TipoDatoModel tipoDato = new TipoDatoModel();
          tipoDato.setId(icsTipoDato.getIdTipoDato());
          tipoDato.setNombre(icsTipoDato.getNombreTipoDato());
          tipoDato.setVigencia(icsTipoDato.getVigencia());

          detalle.setTipoDato(tipoDato);
        }
      } catch (EntityNotFoundException e) {
        LOG.info("IcsTipoDato", e);
      }

      detalle.setLargo(entity.getLargo().longValue());
      detalle.setOrden(entity.getOrden().longValue());
      detalle.setIdSubestructuraPadre(subestructura.getId());
      detalle.setVigencia(entity.getVigenciaDetalleSubestructura());

      IcsSubestructura sub = entity.getIcsSubestructura();
      if (sub != null) {
        detalle.setIdSubestructuraPadre(entity.getIcsSubestructura().getIdSubestructura());
      }      

      detalles.add(detalle);
      
      largo += entity.getLargo().longValue();
    }
    
    subestructura.setLargo(largo);
    subestructura.setColumnas(detalles);

    return subestructura;
  }
}
