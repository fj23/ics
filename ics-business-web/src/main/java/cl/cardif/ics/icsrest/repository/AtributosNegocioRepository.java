package cl.cardif.ics.icsrest.repository;

import cl.cardif.ics.domain.entity.plantillas.IcsAtributoNegocio;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface AtributosNegocioRepository
    extends JpaRepository<IcsAtributoNegocio, Long>, 
    	QueryDslPredicateExecutor<IcsAtributoNegocio> {
  
}