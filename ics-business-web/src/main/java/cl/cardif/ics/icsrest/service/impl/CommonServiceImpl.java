package cl.cardif.ics.icsrest.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.OrderSpecifier;

import cl.cardif.ics.domain.entity.common.IcsCore;
import cl.cardif.ics.domain.entity.common.IcsEvento;
import cl.cardif.ics.domain.entity.common.IcsSocio;
import cl.cardif.ics.domain.entity.common.IcsTipoDato;
import cl.cardif.ics.domain.entity.common.IcsTipoExtension;
import cl.cardif.ics.domain.entity.common.IcsTipoFormula;
import cl.cardif.ics.domain.entity.common.IcsTipoSalida;
import cl.cardif.ics.domain.entity.common.QIcsCore;
import cl.cardif.ics.domain.entity.common.QIcsEvento;
import cl.cardif.ics.domain.entity.common.QIcsSocio;
import cl.cardif.ics.domain.entity.common.QIcsTipoDato;
import cl.cardif.ics.domain.entity.common.QIcsTipoExtension;
import cl.cardif.ics.domain.entity.common.QIcsTipoFormula;
import cl.cardif.ics.domain.entity.common.QIcsTipoSalida;
import cl.cardif.ics.domain.entity.formatos.IcsDetalleFormato;
import cl.cardif.ics.domain.entity.formatos.QIcsDetalleFormato;
import cl.cardif.ics.domain.entity.plantillas.IcsTipoPlantilla;
import cl.cardif.ics.domain.entity.plantillas.QIcsTipoPlantilla;
import cl.cardif.ics.domain.entity.procesos.IcsProceso;
import cl.cardif.ics.domain.entity.procesos.QIcsProceso;
import cl.cardif.ics.icsrest.domain.dto.output.ProcesoOutput;
import cl.cardif.ics.icsrest.domain.pojo.Core;
import cl.cardif.ics.icsrest.domain.pojo.DetalleFormato;
import cl.cardif.ics.icsrest.domain.pojo.Evento;
import cl.cardif.ics.icsrest.domain.pojo.Socio;
import cl.cardif.ics.icsrest.domain.pojo.TipoDato;
import cl.cardif.ics.icsrest.domain.pojo.TipoExtension;
import cl.cardif.ics.icsrest.domain.pojo.TipoFormula;
import cl.cardif.ics.icsrest.domain.pojo.TipoPlantilla;
import cl.cardif.ics.icsrest.domain.pojo.TipoSalida;
import cl.cardif.ics.icsrest.repository.CoresRepository;
import cl.cardif.ics.icsrest.repository.DetallesFormatoRepository;
import cl.cardif.ics.icsrest.repository.EventosRepository;
import cl.cardif.ics.icsrest.repository.ProcesoRepository;
import cl.cardif.ics.icsrest.repository.SociosRepository;
import cl.cardif.ics.icsrest.repository.TiposDatosRepository;
import cl.cardif.ics.icsrest.repository.TiposExtensionRepository;
import cl.cardif.ics.icsrest.repository.TiposFormulaRepository;
import cl.cardif.ics.icsrest.repository.TiposPlantillaRepository;
import cl.cardif.ics.icsrest.repository.TiposSalidaRepository;
import cl.cardif.ics.icsrest.service.CommonService;
import cl.cardif.ics.icsrest.util.Util;

@Service
public class CommonServiceImpl implements CommonService {
	private static final Logger LOG = LoggerFactory.getLogger(CommonServiceImpl.class);

	@Autowired private ConversionService conversionService;
	@Autowired private CoresRepository coresRepository;
	@Autowired private EventosRepository eventsRepository;
	@Autowired private SociosRepository partnersRepository;
	@Autowired private TiposDatosRepository dataTypesRepository;
	@Autowired private TiposExtensionRepository extensionTypesRepository;
	@Autowired private TiposPlantillaRepository templateTypesRepository;
	@Autowired private DetallesFormatoRepository formatDetailsRepository;
	@Autowired private TiposFormulaRepository formulaTypesRepository;
	@Autowired private TiposSalidaRepository tiposSalidaRepository;
	@Autowired private ProcesoRepository procesoRepository;

	@Override
	public List<Core> getAllCores() {
		LOG.info("getAllCores");
		QIcsCore qIcsCore = QIcsCore.icsCore;

		BooleanBuilder bb = new BooleanBuilder()
				.and(qIcsCore.flgVisible.eq("Y"));
		OrderSpecifier<String> ordenarPorNombre = qIcsCore.nombreCore.asc();

		Iterable<IcsCore> iteraCores = this.coresRepository.findAll(bb, ordenarPorNombre);
		
		List<Core> cores = new ArrayList<>();
		for (IcsCore icsCore : iteraCores) {
			Core core = conversionService.convert(icsCore, Core.class);
			cores.add(core);
		}

		return cores;
	}

	@Override
	public List<TipoDato> getAllDataTypes() {
		LOG.info("getAllDataTypes");
		QIcsTipoDato qIcsTipoDato = QIcsTipoDato.icsTipoDato;

		BooleanBuilder bb = new BooleanBuilder()
				.and(qIcsTipoDato.vigencia.eq("Y"));
		OrderSpecifier<String> ordenarPorNombre = qIcsTipoDato.nombreTipoDato.asc();

		Iterable<IcsTipoDato> iteraTiposDato = dataTypesRepository.findAll(bb, ordenarPorNombre);
		
		List<TipoDato> tiposDato = new ArrayList<>();
		for (IcsTipoDato icsTipoDato : iteraTiposDato) {
			TipoDato tipoDato = conversionService.convert(icsTipoDato, TipoDato.class);
			tiposDato.add(tipoDato);
		}
		
		return tiposDato;
	}

	@Override
	public List<Evento> getAllEvents() {
		LOG.info("getAllEvents");
		QIcsEvento qIcsEvento = QIcsEvento.icsEvento;
		
		BooleanBuilder bb = new BooleanBuilder()
				.and(qIcsEvento.vigenciaEvento.eq("Y"));
		OrderSpecifier<String> ordenarPorNombre = qIcsEvento.nombreEvento.asc();
		
		Iterable<IcsEvento> iteraEventos = eventsRepository.findAll(bb, ordenarPorNombre);

		List<Evento> eventos = new ArrayList<>();
		for (IcsEvento icsEvento : iteraEventos) {
			Evento evento = conversionService.convert(icsEvento, Evento.class);
			eventos.add(evento);
		}

		return eventos;
	}

	@Override
	public List<TipoExtension> getAllExtensionTypes() {
		LOG.info("getAllExtensionTypes");
		QIcsTipoExtension qIcsTipoExtension = QIcsTipoExtension.icsTipoExtension;

		BooleanBuilder bb = new BooleanBuilder()
				.and(qIcsTipoExtension.vigencia.eq("Y"));
		OrderSpecifier<String> ordenarPorNombre = qIcsTipoExtension.nombreExtension.asc();
		
		Iterable<IcsTipoExtension> iteraTiposExtension = extensionTypesRepository.findAll(bb, ordenarPorNombre);

		List<TipoExtension> tiposExtension = new ArrayList<>();
		for (IcsTipoExtension icsTipoExtension : iteraTiposExtension) {
			TipoExtension tipoExtension = conversionService.convert(icsTipoExtension, TipoExtension.class);
			tiposExtension.add(tipoExtension);
		}

		return tiposExtension;
	}

	@Override
	public List<DetalleFormato> getAllFormatTypes() {
		LOG.info("getAllFormatTypes");
		QIcsDetalleFormato qIcsDetalleFormato = QIcsDetalleFormato.icsDetalleFormato;

		BooleanBuilder bb = new BooleanBuilder()
				.and(qIcsDetalleFormato.icsTipoFormato.vigenciaFormato.eq("Y"));
		OrderSpecifier<Long> ordenarPorIdTipo = qIcsDetalleFormato.icsTipoFormato.idTipoFormato.asc();

		Iterable<IcsDetalleFormato> iteraDetallesFormato = formatDetailsRepository.findAll(bb, ordenarPorIdTipo);
		
		List<DetalleFormato> formatos = new ArrayList<>();
		for (IcsDetalleFormato icsDetalleFormato : iteraDetallesFormato) {
			DetalleFormato detalleFormato = conversionService.convert(icsDetalleFormato, DetalleFormato.class);
			formatos.add(detalleFormato);
		}

		return formatos;
	}

	@Override
	public List<TipoFormula> getAllFormulaTypes() {
		LOG.info("getAllFormulaTypes");
		QIcsTipoFormula qIcsTipoFormula = QIcsTipoFormula.icsTipoFormula;

		BooleanBuilder bb = new BooleanBuilder()
				.and(qIcsTipoFormula.vigenciaFormula.eq("Y"));
		OrderSpecifier<Long> ordenarPorId = qIcsTipoFormula.idTipoFormula.asc();

		Iterable<IcsTipoFormula> iterableTipoFormula = this.formulaTypesRepository.findAll(bb, ordenarPorId);
		
		List<TipoFormula> tiposFormula = new ArrayList<>();
		for (IcsTipoFormula icsTipoFormula : iterableTipoFormula) {
			TipoFormula tipoFormula = conversionService.convert(icsTipoFormula, TipoFormula.class);
			tiposFormula.add(tipoFormula);
		}

		return tiposFormula;
	}

	@Override
	public List<TipoSalida> getAllOutputTypes() {
		LOG.info("getAllOutputTypes");
		QIcsTipoSalida qIcsTipoSalida = QIcsTipoSalida.icsTipoSalida;
		
		BooleanBuilder bb = new BooleanBuilder()
				.and(qIcsTipoSalida.vigencia.eq("Y"));
		OrderSpecifier<String> ordenarPorNombre = qIcsTipoSalida.descripcion.asc();
		
		Iterable<IcsTipoSalida> iteraTiposSalida = tiposSalidaRepository.findAll(bb, ordenarPorNombre);

		List<TipoSalida> tiposSalida = new ArrayList<>();
		for (IcsTipoSalida icsTipoSalida : iteraTiposSalida) {
			TipoSalida tipoSalida = conversionService.convert(icsTipoSalida, TipoSalida.class);
			tiposSalida.add(tipoSalida);
		}

		return tiposSalida;
	}

	@Override
	public List<Socio> getAllPartners() {
		LOG.info("getAllPartners");
		QIcsSocio qIcsSocio = QIcsSocio.icsSocio;

		BooleanBuilder bb = new BooleanBuilder()
				.and(qIcsSocio.flgVisible.eq("Y"));
		OrderSpecifier<String> ordenarPorNombre = qIcsSocio.nombreSocio.asc();
		
		Iterable<IcsSocio> iteraSocios = partnersRepository.findAll(bb, ordenarPorNombre);

		List<Socio> socios = new ArrayList<>();
		for (IcsSocio icsSocio : iteraSocios) {
			Socio socio = conversionService.convert(icsSocio, Socio.class);
			socios.add(socio);
		}

		return socios;
	}

	@Override
	public List<TipoPlantilla> getAllTemplateTypes() {
		LOG.info("getAllTemplateTypes");
		QIcsTipoPlantilla qIcsTipoPlantilla = QIcsTipoPlantilla.icsTipoPlantilla;

		BooleanBuilder bb = new BooleanBuilder()
				.and(qIcsTipoPlantilla.vigenciaPlantilla.eq("Y"));
		OrderSpecifier<String> ordenarPorNombre = qIcsTipoPlantilla.nombrePlantilla.asc();
		
		Iterable<IcsTipoPlantilla> iteraTiposPlantilla = templateTypesRepository.findAll(bb, ordenarPorNombre);

		List<TipoPlantilla> tiposPlantilla = new ArrayList<>();
		for (IcsTipoPlantilla icsTipoPlantilla : iteraTiposPlantilla) {
			TipoPlantilla tipoPlantilla = conversionService.convert(icsTipoPlantilla, TipoPlantilla.class);
			tiposPlantilla.add(tipoPlantilla);
		}

		return tiposPlantilla;
	}

	@Override
	public List<ProcesoOutput> getProcesos(Long idSocio, Long idEvento) {
		LOG.info("getProcesos");
		QIcsProceso qIcsProceso = QIcsProceso.icsProceso;

		BooleanBuilder bb = new BooleanBuilder()
				.and(qIcsProceso.vigenciaProceso.eq("Y"));

		// al menos un detalle con plantilla por socio
		if (idSocio != null && idSocio > 0) {
			bb.and(qIcsProceso.detalles.any().icsPlantilla.icsSocio.idSocio.eq(idSocio));
		}

		// segun detalles con plantillas por evento
		HashSet<Long> idsEventosAutorizados = Util.getEventsAuthorized();
		if (idEvento != null && idsEventosAutorizados.contains(idEvento)) {
			bb.and(qIcsProceso.detalles.any().icsPlantilla.icsEvento.idEvento.eq(idEvento));
		} else { // si no se especifica o el usuario no puede ver el evento, filtra por los eventos autorizados
			bb.and(qIcsProceso.detalles.any().icsPlantilla.icsEvento.idEvento.in(idsEventosAutorizados));
		}

		OrderSpecifier<String> orderSpecifier = qIcsProceso.nombreProceso.asc();
		Iterable<IcsProceso> iteraProcesos = this.procesoRepository.findAll(bb, orderSpecifier);

		List<ProcesoOutput> procesos = new ArrayList<>();
		for (IcsProceso icsProceso : iteraProcesos) {
			ProcesoOutput proceso = conversionService.convert(icsProceso, ProcesoOutput.class);
			procesos.add(proceso);
		}

		return procesos;
	}

}
