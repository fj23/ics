package cl.cardif.ics.icsrest.exceptions.icsestructura;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class IcsEstructuraForbiddenException extends RuntimeException {

  private static final long serialVersionUID = 3397992475249736227L;

  public IcsEstructuraForbiddenException(String exception) {
    super(exception);
  }

  public IcsEstructuraForbiddenException(String exception, Throwable t) {
    super(exception, t);
  }
}
