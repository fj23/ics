package cl.cardif.ics.icsrest.security.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;

import cl.cardif.ics.icsrest.domain.pojo.Role;
import cl.cardif.ics.icsrest.domain.pojo.User;

public class UserPrinciple implements UserDetails {

	private static final long serialVersionUID = -9106940048092363281L;
	private String username;
	private String email;
	@JsonIgnore
	private String password;
	private transient Collection<? extends GrantedAuthority> authorities;

	public UserPrinciple(String username, String email, String password,
			Collection<? extends GrantedAuthority> authorities) {
		this.username = username;
		this.email = email;
		this.password = password;
		if (authorities != null) {
			this.authorities = new ArrayList<>(authorities);
		}
	}

	public static UserPrinciple build(User user) {
		List<Role> roles = user.getRoles();

		List<GrantedAuthority> authorities = new ArrayList<>();
		for (Role role : roles) {
			GrantedAuthority authority = new SimpleGrantedAuthority(role.getName());
			authorities.add(authority);
		}

		return new UserPrinciple(user.getUsername(), user.getEmail(), user.getPassword(), authorities);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		UserPrinciple user = (UserPrinciple) o;
		return Objects.equals(username, user.username);
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		if (this.authorities != null) {
			return new ArrayList<>(authorities);
		} else {
			return new ArrayList<>();
		}
	}

	public String getEmail() {
		return email;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return username;
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}
}
