package cl.cardif.ics.icsrest.domain.dto.input;

import java.util.ArrayList;
import java.util.List;

public class PlantillaIntegracionInput extends PlantillaInput {
	private long tipoSalida;
	private String delimitadorColumnas;
	private List<ColumnaTrabajoIntegracionInput> columnasTrabajo;
	private List<MapeoIntegracionInput> mapeos;
	private List<ColumnaArchivoSalidaIntegracionInput> columnasArchivoSalida;
	private Boolean incluirEncabezados;

	public PlantillaIntegracionInput() {
		super();
	}

	public List<ColumnaArchivoSalidaIntegracionInput> getColumnasArchivoSalida() {
		if (this.columnasArchivoSalida != null) {
			return new ArrayList<>(this.columnasArchivoSalida);
		}
		else {
			return new ArrayList<>();
		}
	}

	public void setColumnasArchivoSalida(List<ColumnaArchivoSalidaIntegracionInput> columnasArchivoSalida) {
		if (columnasArchivoSalida != null) {
			this.columnasArchivoSalida = new ArrayList<>(columnasArchivoSalida);
		}
	}

	public List<ColumnaTrabajoIntegracionInput> getColumnasTrabajo() {
		if (this.columnasTrabajo != null) {
			return new ArrayList<>(this.columnasTrabajo);
		}
		else {
			return new ArrayList<>();
		}
	}

	public void setColumnasTrabajo(List<ColumnaTrabajoIntegracionInput> columnasTrabajo) {
		if (columnasTrabajo != null) {
			this.columnasTrabajo = new ArrayList<>(columnasTrabajo);
		}
	}

	public String getDelimitadorColumnas() {
		return delimitadorColumnas;
	}

	public void setDelimitadorColumnas(String delimitadorColumnas) {
		this.delimitadorColumnas = delimitadorColumnas;
	}

	public List<MapeoIntegracionInput> getMapeos() {
		if (this.mapeos != null) {
			return new ArrayList<>(this.mapeos);
		}
		else {
			return new ArrayList<>();
		}
	}

	public void setMapeos(List<MapeoIntegracionInput> mapeos) {
		if (mapeos != null) {
			this.mapeos = new ArrayList<>(mapeos);
		}
	}

	public long getTipoSalida() {
		return tipoSalida;
	}

	public void setTipoSalida(long tipoSalida) {
		this.tipoSalida = tipoSalida;
	}

	public Boolean getIncluirEncabezados() {
		return incluirEncabezados;
	}

	public void setIncluirEncabezados(Boolean incluirEncabezados) {
		this.incluirEncabezados = incluirEncabezados;
	}

	@Override
	public String toString() {
		return "PlantillaIntegracionInput [tipoSalida=" + tipoSalida + ", delimitadorColumnas=" + delimitadorColumnas
				+ ", columnasTrabajo=" + columnasTrabajo + ", mapeos=" + mapeos + ", columnasArchivoSalida="
				+ columnasArchivoSalida + ", incluirEncabezados=" + incluirEncabezados + ", version=" + version
				+ ", observaciones=" + observaciones + ", tipo=" + tipo + ", socio=" + socio + ", evento=" + evento
				+ ", core=" + core + ", fechaCreacion=" + fechaCreacion + ", tipoExtension=" + tipoExtension + ", id="
				+ id + ", nombre=" + nombre + ", vigencia=" + vigencia + "]";
	}

}
