package cl.cardif.ics.icsrest.domain.dto.input;

import java.util.Date;

public class CargaArchivoInput {
	private long id;
	private String nombre;
	private Date fechaRecepcion;
	private String codigo;
	private String observacion;
	private String nombreArchivo;
	private String extensionArchivo;
	private long idProceso;

	public CargaArchivoInput() {
		super();
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getExtensionArchivo() {
		return extensionArchivo;
	}

	public void setExtensionArchivo(String extensionArchivo) {
		this.extensionArchivo = extensionArchivo;
	}

	public Date getFechaRecepcion() {
		if (fechaRecepcion != null) {
			return (Date) fechaRecepcion.clone();
		} else {
			return null;
		}
	}

	public void setFechaRecepcion(Date fechaRecepcion) {
		if (fechaRecepcion != null) {
			this.fechaRecepcion = (Date) fechaRecepcion.clone();
		}
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getIdProceso() {
		return idProceso;
	}

	public void setIdProceso(long idProceso) {
		this.idProceso = idProceso;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNombreArchivo() {
		return nombreArchivo;
	}

	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	@Override
	public String toString() {
		return "CargaArchivoInput [id=" + id + ", nombre=" + nombre + ", fechaRecepcion=" + fechaRecepcion + ", codigo="
				+ codigo + ", observacion=" + observacion + ", nombreArchivo=" + nombreArchivo + ", extensionArchivo="
				+ extensionArchivo + ", idProceso=" + idProceso + "]";
	}
}
