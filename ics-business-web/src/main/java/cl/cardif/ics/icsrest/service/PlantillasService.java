package cl.cardif.ics.icsrest.service;

import java.util.List;
import java.util.Map;

import org.springframework.security.access.prepost.PreAuthorize;

import cl.cardif.ics.domain.entity.plantillas.IcsPlantilla;
import cl.cardif.ics.icsrest.domain.dto.output.PlantillaOutput;
import cl.cardif.ics.icsrest.domain.dto.output.RequestResultOutput;
import cl.cardif.ics.icsrest.domain.pojo.Plantilla;
import cl.cardif.ics.icsrest.domain.pojo.TipoExtension;
import cl.cardif.ics.icsrest.util.OutputListWrapper;
import cl.cardif.ics.icsrest.util.Permisos;

public interface PlantillasService {

	@PreAuthorize(value = "hasAnyAuthority("
            + Permisos.Mant_Plan_Alta + ","
            + Permisos.Mant_Plan_Canc + ","
            + Permisos.Mant_Plan_Reca + ","
            + Permisos.Mant_Plan_Prosp + ","
            + Permisos.Mant_Plan_Intera + ","
            + Permisos.Mant_Plan_Pago + ")")
	void activar(long idPlantilla);
  
	/** Consulta dependencias (procesos de carga) a la plantilla obtenida por el id.
	 * @param idPlantilla
	 * @return true cuando no hay dependencias, false cuando sí las hay
	 */
	@PreAuthorize(value = "hasAnyAuthority("
            + Permisos.Mant_Plan_Alta + ","
            + Permisos.Mant_Plan_Canc + ","
            + Permisos.Mant_Plan_Reca + ","
            + Permisos.Mant_Plan_Prosp + ","
            + Permisos.Mant_Plan_Intera + ","
            + Permisos.Mant_Plan_Pago + ")")
	boolean puedeActivar(long idPlantilla);

	@PreAuthorize(value = "hasAnyAuthority("
            + Permisos.Mant_Plan_Alta + ","
            + Permisos.Mant_Plan_Canc + ","
            + Permisos.Mant_Plan_Reca + ","
            + Permisos.Mant_Plan_Prosp + ","
            + Permisos.Mant_Plan_Intera + ","
            + Permisos.Mant_Plan_Pago + ")")
	int almacenarArrayBidimensionalEnArregloUtil(List<Map<Integer, String>> ddArray);

	@PreAuthorize(value = "hasAnyAuthority("
            + Permisos.Mant_Plan_Alta + ","
            + Permisos.Mant_Plan_Canc + ","
            + Permisos.Mant_Plan_Reca + ","
            + Permisos.Mant_Plan_Prosp + ","
            + Permisos.Mant_Plan_Intera + ","
            + Permisos.Mant_Plan_Pago + ")")
	RequestResultOutput llamarSPAlmacenarPlantilla(Map<String, Object> map);

	@PreAuthorize(value = "hasAnyAuthority("
            + Permisos.Mant_Plan_Alta + ","
            + Permisos.Mant_Plan_Canc + ","
            + Permisos.Mant_Plan_Reca + ","
            + Permisos.Mant_Plan_Prosp + ","
            + Permisos.Mant_Plan_Intera + ","
            + Permisos.Mant_Plan_Pago + ")")
	boolean puedeDesactivar(long idPlantilla);
  
	@PreAuthorize(value = "hasAnyAuthority("
	        + Permisos.Mant_Plan_Alta + ","
	        + Permisos.Mant_Plan_Canc + ","
	        + Permisos.Mant_Plan_Reca + ","
            + Permisos.Mant_Plan_Prosp + ","
            + Permisos.Mant_Plan_Intera + ","
            + Permisos.Mant_Plan_Pago + ")")
	void desactivar(long idPlantilla);

	/** Obtiene la versión actualmente vigente de la plantilla obtenida por sus identificadores.
	* @param nombre El nombre de la plantilla
	* @param idSocio El id del socio de la plantilla
	* @param idEvento El id del evento de la plantilla
	* @param idTipoPlantilla El id del tipo de plantilla
	* @return Un número indicando la versión activa (por defecto es 1)
	*/
	@PreAuthorize(value = "hasAnyAuthority("
	        + Permisos.Cslt_Plan + ","
	        + Permisos.Mant_Plan_Alta + ","
	        + Permisos.Mant_Plan_Canc + ","
	        + Permisos.Mant_Plan_Reca + ","
            + Permisos.Mant_Plan_Prosp + ","
            + Permisos.Mant_Plan_Intera + ","
            + Permisos.Mant_Plan_Pago + ")")
	Long obtenerNumeroVersionVigente(String nombre, Long idSocio, Long idEvento, Long idTipoPlantilla);

	@PreAuthorize(value = "hasAnyAuthority("
	        + Permisos.Cslt_Plan + ","
	        + Permisos.Mant_Plan_Alta + ","
	        + Permisos.Mant_Plan_Canc + ","
	        + Permisos.Mant_Plan_Reca + ","
            + Permisos.Mant_Plan_Prosp + ","
            + Permisos.Mant_Plan_Intera + ","
            + Permisos.Mant_Plan_Pago + ")")
	List<String> obtenerNombresDeTodasLasPlantillas(Long idSocio, Long idEvento);

	@PreAuthorize(value = "hasAnyAuthority("
	        + Permisos.Cslt_Plan + ","
	        + Permisos.Mant_Plan_Alta + ","
	        + Permisos.Mant_Plan_Canc + ","
	        + Permisos.Mant_Plan_Reca + ","
            + Permisos.Mant_Plan_Prosp + ","
            + Permisos.Mant_Plan_Intera + ","
            + Permisos.Mant_Plan_Pago + ")")
	List<IcsPlantilla> obtenerVersionesPlantilla(IcsPlantilla versionActual);

	@PreAuthorize(value = "hasAnyAuthority("
	        + Permisos.Cslt_Plan + ","
	        + Permisos.Mant_Plan_Alta + ","
	        + Permisos.Mant_Plan_Canc + ","
	        + Permisos.Mant_Plan_Reca + ","
            + Permisos.Mant_Plan_Prosp + ","
            + Permisos.Mant_Plan_Intera + ","
            + Permisos.Mant_Plan_Pago + ")")
	List<TipoExtension> obtenerTiposExtensionEstandar();

	@PreAuthorize(value = "hasAnyAuthority("
	        + Permisos.Cslt_Plan + ","
	        + Permisos.Mant_Plan_Alta + ","
	        + Permisos.Mant_Plan_Canc + ","
	        + Permisos.Mant_Plan_Reca + ","
            + Permisos.Mant_Plan_Prosp + ","
            + Permisos.Mant_Plan_Intera + ","
            + Permisos.Mant_Plan_Pago + ")")
	OutputListWrapper<PlantillaOutput> obtenerPlantillasOutput(
	    int page, int size, String sortColumn, String sortOrder, Map<String, String> filters);

	@PreAuthorize(value = "hasAnyAuthority("
	        + Permisos.Cslt_Plan + ","
	        + Permisos.Mant_Plan_Alta + ","
	        + Permisos.Mant_Plan_Canc + ","
	        + Permisos.Mant_Plan_Reca + ","
            + Permisos.Mant_Plan_Prosp + ","
            + Permisos.Mant_Plan_Intera + ","
            + Permisos.Mant_Plan_Pago + ")")
	OutputListWrapper<PlantillaOutput> obtenerPlantillasOutputSegunTipo(Long templateTypeId);


	@PreAuthorize(value = "hasAnyAuthority("
	        + Permisos.Cslt_Plan + ","
	        + Permisos.Mant_Plan_Alta + ","
	        + Permisos.Mant_Plan_Canc + ","
	        + Permisos.Mant_Plan_Reca + ","
            + Permisos.Mant_Plan_Prosp + ","
            + Permisos.Mant_Plan_Intera + ","
            + Permisos.Mant_Plan_Pago + ")")
	OutputListWrapper<Plantilla> obtenerPlantillasPojo(Long idSocio, Long idEvento);

	@PreAuthorize(value = "hasAnyAuthority("
	        + Permisos.Cslt_Plan + ","
	        + Permisos.Mant_Plan_Alta + ","
	        + Permisos.Mant_Plan_Canc + ","
	        + Permisos.Mant_Plan_Reca + ","
            + Permisos.Mant_Plan_Prosp + ","
            + Permisos.Mant_Plan_Intera + ","
            + Permisos.Mant_Plan_Pago + ")")
	OutputListWrapper<Plantilla> obtenerPlantillasSalidaEstructuradaPojo(Long idSocio, Long idEvento);

	@PreAuthorize(value = "hasAnyAuthority("
	        + Permisos.Cslt_Plan + ","
	        + Permisos.Mant_Plan_Alta + ","
	        + Permisos.Mant_Plan_Canc + ","
	        + Permisos.Mant_Plan_Reca + ","
            + Permisos.Mant_Plan_Prosp + ","
            + Permisos.Mant_Plan_Intera + ","
            + Permisos.Mant_Plan_Pago + ")")
	PlantillaOutput cargarPlantilla(long idPlantilla);
}
