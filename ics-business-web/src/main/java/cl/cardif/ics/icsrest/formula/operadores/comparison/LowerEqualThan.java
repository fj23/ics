package cl.cardif.ics.icsrest.formula.operadores.comparison;

import java.util.ArrayList;
import java.util.Map;

import cl.cardif.ics.icsrest.formula.FormulaParent;
import cl.cardif.ics.icsrest.formula.ILogicFactor;
import cl.cardif.ics.icsrest.util.FormulaOrigenKey;

public class LowerEqualThan extends FormulaParent implements ILogicFactor {
	public LowerEqualThan() {
		this.hijos = new ArrayList<>();
	}

	@Override
	public int getMaximumChildrenCount() {
		return 2;
	}

	@Override
	public String toSafeSQL(Map<FormulaOrigenKey, Integer> tableAliases) {
		StringBuilder sb = new StringBuilder()
				.append(getHijo(0).toSafeSQL(tableAliases))
				.append(" <= ")
				.append(getHijo(1).toSafeSQL(tableAliases));

		return sb.toString();
	}

	@Override
	public String toSQL(Map<FormulaOrigenKey, Integer> tableAliases) {
		StringBuilder sb = new StringBuilder()
				.append(getHijo(0).toSQL(tableAliases))
				.append(" <= ")
				.append(getHijo(1).toSQL(tableAliases));

		return sb.toString();
	}

}
