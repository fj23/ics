package cl.cardif.ics.icsrest.exceptions.icscargaarchivo;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.GONE)
public class IcsCargaArchivoGoneException extends RuntimeException {

  private static final long serialVersionUID = -7209750264039464453L;

  public IcsCargaArchivoGoneException(String exception) {
    super(exception);
  }

  public IcsCargaArchivoGoneException(String exception, Throwable t) {
    super(exception, t);
  }
}
