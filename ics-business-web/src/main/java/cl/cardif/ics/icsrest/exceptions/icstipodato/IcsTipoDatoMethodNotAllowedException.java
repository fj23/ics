package cl.cardif.ics.icsrest.exceptions.icstipodato;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
public class IcsTipoDatoMethodNotAllowedException extends RuntimeException {

  private static final long serialVersionUID = -6814524510236406273L;

  public IcsTipoDatoMethodNotAllowedException(String exception) {
    super(exception);
  }

  public IcsTipoDatoMethodNotAllowedException(String exception, Throwable t) {
    super(exception, t);
  }
}
