package cl.cardif.ics.icsrest.formula.funciones.uf;

import java.util.ArrayList;
import java.util.Map;

import cl.cardif.ics.icsrest.formula.FormulaParent;
import cl.cardif.ics.icsrest.formula.IMathFactor;
import cl.cardif.ics.icsrest.util.FormulaOrigenKey;

public class ValorUFDia extends FormulaParent implements IMathFactor {
	public ValorUFDia() {
		hijos = new ArrayList<>();
	}

	@Override
	public int getMaximumChildrenCount() {
		return 1;
	}

	@Override
	public String toSafeSQL(Map<FormulaOrigenKey, Integer> tableAliases) {
		return "1";
	}

	@Override
	public String toSQL(Map<FormulaOrigenKey, Integer> tableAliases) {
		StringBuilder sb = new StringBuilder()
				.append("PCK_UTIL_ICS.FNC_VALORUF_PORDIA")
				.append("(")
					.append(getHijo(0).toSQL(tableAliases))
				.append(")");

		return sb.toString();
	}
}
