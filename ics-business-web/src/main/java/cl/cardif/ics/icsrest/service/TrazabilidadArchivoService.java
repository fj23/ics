package cl.cardif.ics.icsrest.service;

import java.util.List;
import java.util.Map;

import org.springframework.http.HttpHeaders;

import cl.cardif.ics.domain.entity.common.IcsParametro;
import cl.cardif.ics.domain.entity.procesos.IcsJobs;
import cl.cardif.ics.icsgeneratefile.entity.FilesInformation;
import cl.cardif.ics.icsrest.domain.pojo.DetalleTransaccion;
import cl.cardif.ics.icsrest.domain.pojo.TrazabilidadArchivo;
import cl.cardif.ics.icsrest.domain.pojo.TrazabilidadLog;
import cl.cardif.ics.icsrest.domain.pojo.TrazabilidadRespaldo;
import cl.cardif.ics.icsrest.util.OutputListWrapper;

public interface TrazabilidadArchivoService {

	OutputListWrapper<TrazabilidadArchivo> getTrazabilidadArchivos(
		int page, int size, String sortColumn, String sortOrder, Map<String, String> filters);

	List<DetalleTransaccion> getDetalleTransaccion(Long id);

	List<TrazabilidadArchivo> exportTrazabilidadArchivos(Map<String, String> filters);

	List<IcsParametro> getParametrosTrazabilidad();

	TrazabilidadRespaldo downloadOriginal(Long id);

	FilesInformation getFileInformation(Long id);

	byte[] generateFile(String idTransaction, FilesInformation file);

	void deleteGeneratedFile(Long idTransaction, Long tipoArchivo);

	IcsJobs consolidar();

	void finalizarProceso(Long id);

	TrazabilidadArchivo getTrazabilidadArchivo(Long id);

	TrazabilidadLog downloadLog(Long id);

	HttpHeaders getHttpHeadersForFile(String fileName, String fileType);

}
