package cl.cardif.ics.component;

import java.net.MalformedURLException;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;

import cl.cardif.ics.config.ConfigProperties;

@Component
public class CustomZuulFilter extends ZuulFilter {

	private static final Logger LOG = LoggerFactory.getLogger(CustomZuulFilter.class);

	

	@Autowired
	private ConfigProperties configProp;

	@Override
	public boolean shouldFilter() {
		return true;
	}

	@Override
	public Object run() {
		RequestContext ctx = RequestContext.getCurrentContext();

		String host = this.configProp.getBusinessAddress();
		String context = this.configProp.getContextAddress();

		if (host == null || host.isEmpty()) {
			LOG.warn("La dirección de la API REST Business, no está definida correcta, '' es tomado como valor por defecto");
		}

		try {
			ctx.setRouteHost(new URL(host + context));
		} catch (MalformedURLException e) {
			LOG.error("La dirección de la API REST Business, no es correcta", e);
		}

		return null;
	}

	@Override
	public String filterType() {
		return "pre";
	}

	@Override
	public int filterOrder() {
		return 6;
	}

}
