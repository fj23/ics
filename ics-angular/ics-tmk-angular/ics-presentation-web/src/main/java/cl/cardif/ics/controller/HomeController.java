package cl.cardif.ics.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

	static final Logger LOG = LoggerFactory.getLogger(HomeController.class);

	@GetMapping(value = { "/", "/login" })
	public String welcome() {
		return "forward:/index.html";
	}
}
