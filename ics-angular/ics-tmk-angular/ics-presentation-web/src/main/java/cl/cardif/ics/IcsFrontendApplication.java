package cl.cardif.ics;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;

import cl.cardif.ics.component.CustomZuulFilter;

@EnableZuulProxy
@SpringBootApplication(scanBasePackages = "cl.cardif.ics")
public class IcsFrontendApplication extends SpringBootServletInitializer {

	@Bean
	public CustomZuulFilter customZuulFilter() {
		return new CustomZuulFilter();
	}

	public static void main(String[] args) {
		ConfigurableApplicationContext context = null;
		try {
			context = SpringApplication.run(IcsFrontendApplication.class, args);
		} finally {
			if (context != null) {
				context.close();
			}
		}
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(IcsFrontendApplication.class);
	}

}
