package cl.cardif.ics.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
public class ConfigProperties {

	@Autowired
	private Environment env;

	public String getBusinessAddress() {
		return this.env.getProperty("ICS_BUSINESS_ADDRESS", String.class);
	}

	public String getContextAddress() {
		return this.env.getProperty("ICS_CONTEXT_ADDRESS", String.class);
	}
}
