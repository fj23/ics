export class FiltrosBaseModel {
    public pageIndex: number = 0;
    public pageSize: number = 10;
    public sortColumn: string = "";
    public sortOrder: string = "asc";
}