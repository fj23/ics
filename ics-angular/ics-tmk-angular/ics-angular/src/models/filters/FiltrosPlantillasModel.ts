import { FiltrosBaseModel } from './FiltrosBaseModel';
export class FiltrosPlantillasModel extends FiltrosBaseModel {
    public socio: number = 0;
    public evento: number = 0;
    public tipo: number = 0;
    public nombre: string = '';
    public vigencia: string = '';
}