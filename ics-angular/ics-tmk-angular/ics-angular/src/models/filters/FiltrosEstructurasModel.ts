import { FiltrosBaseModel } from './FiltrosBaseModel';
export class FiltrosEstructurasModel extends FiltrosBaseModel {
    public codigo: string = '';
    public nombre: string = '';
}