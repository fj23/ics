import { FiltrosBaseModel } from './FiltrosBaseModel';
export class FiltrosTrazabilidadArchivosModel extends FiltrosBaseModel {
    public socio: number = 0;
    public evento: number = 0;
    public estado: string = null;
    public idCarga: number = 0;
    public idExterno: number = 0;
    public recepcionadoDesde: Date = null;
    public recepcionadoHasta: Date = null;
    public cargadoDesde: Date = null;
    public cargadoHasta: Date = null;
}