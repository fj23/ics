import { FiltrosBaseModel } from './FiltrosBaseModel';
export class FiltrosHomologacionesModel extends FiltrosBaseModel {
    public socio1: number = 0;
    public sistema1: number = 0;
    public socio2: number = 0;
    public sistema2: number = 0;
    public concepto: string = '';
    public vigencia: string = '';
}