import { ColumnaMetadatos } from '../../compartido/ColumnaMetadatos';

import { ColumnaDestinoEntrada } from './ColumnaDestinoEntrada';
import { ArchivoEntrada } from './ArchivoEntrada';
import { EstructuraAneto } from '../../estructuras/EstructuraAneto';

export class MapeoEntrada {
    public columnaDestino: ColumnaDestinoEntrada;
    public archivoOrigenDatos: ArchivoEntrada;
    public columnaOrigenDatos: ColumnaMetadatos; //columna de un archivo o estructura
    public estructuraOrigen: EstructuraAneto;
    public subEstructuraColumnaOrigen: ColumnaMetadatos;
    public valorParaFiltrar: string = "";
    public esObligatorio: boolean = true;
    public grupoColumnaDestino: number;
    public filtroExclusion: boolean = false;
    public encriptar: boolean = false;

    public get tieneCardinalidad(): boolean {
        return !!this.columnaDestino && this.columnaDestino.flgCardinalidad==='Y';
    }
}