import { ColumnaMetadatos } from 'src/models/compartido/ColumnaMetadatos';
import { EstructuraAneto } from 'src/models/estructuras/EstructuraAneto';
import { TipoExtension } from '../../compartido/TipoExtension';
import { TipoExtensionEnum } from '../../../enums/TipoExtensionEnum';

export class ArchivoEntrada {
    public id: number = 0;
    public nombre: string = "(sin nombre)";
    public tipo: TipoExtension;
    public noRealizarTransformaciones: boolean = false;
    public delimitadorColumnas: string = "";
    public numeroHoja: number;
    public saltarRegistrosIniciales: number = 0;
    public saltarRegistrosFinales: number = 0;
    public columnas: ColumnaMetadatos[];
    public estructuras: EstructuraAneto[];
    public mapeado?: boolean;
    public vigencia: string = "Y";

    private get tieneColumnasSinPosiciones() {
        return this.columnas.some( 
            (col) => { 
                return isNaN(col.posicionInicial) || 
                        isNaN(col.posicionFinal) || 
                        col.posicionInicial < 0 ||
                        col.posicionFinal < 0;
            }
        );
    }

    public get errorValidacion(): string {

        if (!this.nombre) {
            return "no tiene nombre.";
        } else if (!this.tipo) {
            return "no tiene tipo de extensión.";
        } else if (isNaN(this.saltarRegistrosIniciales) || isNaN(this.saltarRegistrosFinales)) {
            return "la cantidad de registros a saltar no es válida.";
        } else {
            if (this.tipo.id === TipoExtensionEnum.ESTRUCTURADO) {
                if (!this.estructuras || this.estructuras.length === 0) {
                    return "no posee estructuras.";
                }
            } else {
                if (!this.columnas || this.columnas.length === 0) {
                    return "no posee columnas."
                } else {
                    switch (this.tipo.id) {
                        case TipoExtensionEnum.PLANO:
                            if (!this.delimitadorColumnas && this.tieneColumnasSinPosiciones) {
                                return "tiene columnas con posición inicial o final incorrectas.";
                            }
                            break;
                        case TipoExtensionEnum.CSV:
                            if (!this.delimitadorColumnas) {
                                return "no posee un delimitador de columnas.";
                            }
                            break;
                        case TipoExtensionEnum.EXCEL:
                            if (!this.numeroHoja) {
                                return "no posee un número de hoja.";
                            }
                            break;
                    }
                }
            }
        }

        return null;
    }
}
