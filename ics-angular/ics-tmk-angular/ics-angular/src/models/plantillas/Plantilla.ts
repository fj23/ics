import { SistemaCore } from '../compartido/SistemaCore';
import { Evento } from '../compartido/Evento';
import { Socio } from '../compartido/Socio';
import { TipoPlantilla } from '../compartido/TipoPlantilla';

export class Plantilla {
    public id: number;
    public nombre: string;
    public version: string;
    public vigencia: string;
    public observaciones: string;
    public tipo: TipoPlantilla;
    public socio: Socio;
    public evento: Evento;
    public core: SistemaCore = null;
    public fechaCreacion: string;
}