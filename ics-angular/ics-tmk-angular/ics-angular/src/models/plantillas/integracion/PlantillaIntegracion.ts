import { Plantilla } from '../Plantilla';
import { MapeoIntegracion } from './MapeoIntegracion';
import { ColumnaTrabajoIntegracion } from './ColumnaTrabajoIntegracion';
import { TipoSalidaIntegracion } from '../../compartido/TipoSalidaIntegracion';
import { SistemaCore } from '../../compartido/SistemaCore';
import { TipoExtension } from '../../compartido/TipoExtension';
import { ColumnaArchivoIntegracion } from './ColumnaArchivoIntegracion';

export class PlantillaIntegracion extends Plantilla {
    public tipoSalida: TipoSalidaIntegracion;
    public core: SistemaCore;
    public extensionSalidaManual: TipoExtension;
    public delimitadorColumnas: string;
    public incluirEncabezados: boolean;
    public columnasTrabajo: ColumnaTrabajoIntegracion[];
    public columnasDestino: MapeoIntegracion[];
    public columnasArchivoDestino: ColumnaArchivoIntegracion[];
}