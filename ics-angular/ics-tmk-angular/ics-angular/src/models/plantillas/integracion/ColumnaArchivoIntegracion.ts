import { MapeoIntegracion } from './MapeoIntegracion';

export class ColumnaArchivoIntegracion {
    public id: number = 0;
    public orden: number;
    public origen: MapeoIntegracion = null;
    public posicionInicial: number;
    public posicionFinal: number;
    public separarArchivo: boolean = false;
    public incluir: boolean = true;

    public flgVisible: string = "Y";

    public get tienePosicionInicialFinal(): boolean {
        return !isNaN(this.posicionFinal) && !isNaN(this.posicionInicial) && 
            this.posicionFinal > this.posicionInicial;
    }
}