export class ColumnaDestinoIntegracion {
    public idAtributoUsuario: number;
    public idAtributoUser: number;
    public nombreColumna: string;
    public tipoPersona: string;
    public flgCardinalidad: string;
    public idIntegracionEvento: number;
    public traduccion: string;

    public idAtributoIntegracionEvento: number;
    public nombreColumnaIntegracion: string;    
}