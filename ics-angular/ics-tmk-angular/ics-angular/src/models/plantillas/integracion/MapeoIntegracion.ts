import { ColumnaTrabajoIntegracion } from './ColumnaTrabajoIntegracion';
import { ColumnaDestinoIntegracion } from 'src/models/plantillas/integracion/ColumnaDestinoIntegracion';

export class MapeoIntegracion {
    public id: number = 0;
    public columnaOrigen: ColumnaTrabajoIntegracion = null;
    public columnaDestino: ColumnaDestinoIntegracion = null;
    public enArchivoSalida: boolean = false;
    public obligatorio: boolean=true;
}