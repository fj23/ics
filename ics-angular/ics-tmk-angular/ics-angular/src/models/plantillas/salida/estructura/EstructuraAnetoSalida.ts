import { ColumnaEstructuraAnetoSalida } from "./ColumnaEstructuraAnetoSalida";

export class EstructuraAnetoSalida {
    public id: number;
    public codigo: string;
    public descripcion: string;
    public columnas: ColumnaEstructuraAnetoSalida[];
}