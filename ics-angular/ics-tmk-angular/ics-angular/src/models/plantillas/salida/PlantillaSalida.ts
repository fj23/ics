import { Plantilla } from '../Plantilla';
import { TipoExtension } from '../../compartido/TipoExtension';
import { ColumnaArchivoSalida } from './ColumnaArchivoSalida';
import { EstructuraAnetoSalida } from './estructura/EstructuraAnetoSalida';

export class PlantillaSalida extends Plantilla {
    public tipoExtension: TipoExtension;
    public columnas: ColumnaArchivoSalida[];
    public delimitadorColumnas: string;
    public estructuras: EstructuraAnetoSalida[];
    public formatoOriginal: boolean;
}