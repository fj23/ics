import { EstructuraAneto } from 'src/models/estructuras/EstructuraAneto';
import { ColumnaDestinoEntrada } from '../entrada/ColumnaDestinoEntrada';

export class ColumnaArchivoSalida {
    public id: number;
    public estructura: EstructuraAneto;
    // public columnaEstructura: ColumnaMetadatosModel;
    public columnaOrigen: ColumnaDestinoEntrada;
    public orden: number;
    public posicionInicial: number;
    public posicionFinal: number;

    public get tienePosiciones(): boolean {
        let resultado = this.posicionInicial && !isNaN(this.posicionInicial) && this.posicionInicial >= 0 &&
                this.posicionFinal && !isNaN(this.posicionFinal) && this.posicionFinal >= 0;
        return resultado;

    }

}