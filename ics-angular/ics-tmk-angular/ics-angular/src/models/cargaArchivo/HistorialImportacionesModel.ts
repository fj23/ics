import { HistorialImportacionesId } from "./HistorialImportacionesId";

export class HistorialImportacionesModel {
    public id: HistorialImportacionesId;
    public nombreArchivo: string;
    public procesoCarga: string;
    public fechaProceso: number;
    public cantidadRegistros: number;
}