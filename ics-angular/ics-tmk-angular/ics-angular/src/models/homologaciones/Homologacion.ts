import { Socio } from 'src/models/compartido/Socio';
import { SistemaCore } from 'src/models/compartido/SistemaCore';

export class Homologacion {
    public id: number;
    public idTipo: number;
    public concepto: string;
    public vigencia: string;
    public socio1: Socio;
    public core1: SistemaCore;
    public valor1: string;
    public socio2: Socio;
    public core2: SistemaCore;
    public valor2: string;
}