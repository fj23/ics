import { Socio } from "../compartido/Socio";

export class HomologacionesImportadorModel {
    public socio1: Socio;
    public socio2: Socio;
    public core1: Socio;
    public core2: Socio;
    public archivo: File;
}