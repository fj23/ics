import { ColumnaDestinoEntrada } from "src/models/plantillas/entrada/ColumnaDestinoEntrada";
import { AtributoEnriquecimiento } from "src/models/plantillas/AtributoEnriquecimiento";
import { TipoFormulaOutput } from "./TipoFormulaOutput";
import { TipoHomologacionOutput } from "./TipoHomologacionOutput";
import { ColumnaDestinoIntegracion } from "src/models/plantillas/integracion/ColumnaDestinoIntegracion";
import { DetalleFormatoEModel } from "./DetalleFormatoEModel";

export class AtributosIntgOutputEModel {
	public idAtributoIntegracion: number;
	public flgObligatorio: string;
	public idAtributoEnriquecimiento: number;
	public idAtributoNegocio: number;
	public idDeFormato: number;
	public idDeTipoHomologacion: number;
	public idIntegracionEvento: number;
	public idPlantilla: number;
	public idTipoFormula: number;

	public columnaOrigen: ColumnaDestinoEntrada;
	public columnaEnriquecimiento: AtributoEnriquecimiento;
	public columnaDestino: ColumnaDestinoIntegracion;
	
	public formato: DetalleFormatoEModel;
	public formula: TipoFormulaOutput;
	public homologacion: TipoHomologacionOutput;
}