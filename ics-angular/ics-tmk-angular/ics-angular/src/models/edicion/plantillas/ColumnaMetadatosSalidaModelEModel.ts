import { TipoDato } from "src/models/compartido/TipoDato";

export class ColumnaMetadatosSalidaModelEModel {
    public id: number;
	public nombre: string;
	public estructura: number;
	public columnaOrigen: number;
	public orden: number;
	public posicionInicial: number;
	public posicionFinal: number;
	public tipoDato: TipoDato;
}