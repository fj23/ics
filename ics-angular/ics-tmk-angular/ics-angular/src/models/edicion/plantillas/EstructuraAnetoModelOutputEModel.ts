import { ColumnaMetadatosModelInputEModel } from "./ColumnaMetadatosModelInputEModel";
import { TipoDato } from "src/models/compartido/TipoDato";

export class EstructuraAnetoModelOutputEModel {
    public id: number;
    public codigo: string;
    public descripcion: string;
    public cardMin: number;
    public cardMax: number;
    public columnas: ColumnaMetadatosModelInputEModel[];
    public vigencia: string;
    public tipoDato: TipoDato;
}