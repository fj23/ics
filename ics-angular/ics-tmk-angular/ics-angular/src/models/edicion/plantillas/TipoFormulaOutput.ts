export class TipoFormulaOutput {
    public idTipoFormula: number;
	public estructuraFormula: string;
	public nombreFormula: string;
	public tipoDatoResultante: string;
	public vigenciaFormula: string;
	public orden: number;
}