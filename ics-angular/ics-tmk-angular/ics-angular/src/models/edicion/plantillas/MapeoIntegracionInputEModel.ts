import { ColumnaDestinoIntegracion } from "src/models/plantillas/integracion/ColumnaDestinoIntegracion";
import { ColumnaDestinoEntrada } from "src/models/plantillas/entrada/ColumnaDestinoEntrada";
import { AtributoEnriquecimiento } from "src/models/plantillas/AtributoEnriquecimiento";

export class MapeoIntegracionInputEModel {
    public id: number;
	public columnaDestino: number;
	public obligatorio: boolean;

	public columnaOrigenEM: ColumnaDestinoEntrada;
	public columnaEnriquecimientoEM: AtributoEnriquecimiento;
	public columnaDestinoEM: ColumnaDestinoIntegracion;
	public esObligatorio: boolean;
	public idAtributo: number;
}