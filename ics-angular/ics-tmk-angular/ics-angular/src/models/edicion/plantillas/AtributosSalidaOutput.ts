import { ArchivoEModel } from "./ArchivoEMode";
import { AtributoUsuarioOutput } from "./AtributoUsuarioOutput";
import { ColumnaMetadatosSalidaModelEModel } from "./ColumnaMetadatosSalidaModelEModel";

export class AtributosSalidaOutput {
    public archivoEM: ArchivoEModel;
	public columnaOrigenEM: AtributoUsuarioOutput;	
	public atributo: ColumnaMetadatosSalidaModelEModel;
}