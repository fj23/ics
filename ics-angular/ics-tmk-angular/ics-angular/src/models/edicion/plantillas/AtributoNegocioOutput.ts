export class AtributoNegocioOutput {
    public idAtributoNegocio: number;
    public idEvento: number;
    public nombreColumnaNegocio: string;
    public nombreTablaDestino: string;
}