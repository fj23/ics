import { TipoExtensionEModel } from "./TipoExtensionEModel";
import { EstructurasAgrupadaEModel } from "./EstructurasAgrupadaEModel";
import { ColumnaMetadatosSalidaModelEModel } from "./ColumnaMetadatosSalidaModelEModel";
import { EstructuraAneto } from "src/models/estructuras/EstructuraAneto";
import { AtributosSalidaOutput } from "./AtributosSalidaOutput";

export class ArchivoEModel {
    public idArchivo: number;
	public delimitador: string;
	public formulaNombre: string;
	public tipoExtension: TipoExtensionEModel;
	public idGrupoEstructuras: EstructurasAgrupadaEModel;
	public lineaFinal: number;
	public lineaInicial: number;
	public nombreArchivo: string;
	public tipoArchivo: string;
	public vigenciaArchivo: string;
	public columnas: ColumnaMetadatosSalidaModelEModel[];
	public estructurasOutput: EstructuraAneto[];
	public columnasSalida: AtributosSalidaOutput[];
	public numeroHoja: number;
	public flgSinTransformacion: string;
	public valorFiltrado: string;
}
