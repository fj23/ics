import { TipoDato } from "src/models/compartido/TipoDato";

export class ColumnaMetadatosModelInputEModel {
    public id: number;
    public codigo: string;
    public descripcion: string;
    public orden: number;
    
    public esObligatorio: boolean;
    public esLlave: boolean;
    public posicionInicial: number;
    public posicionFina: number;
    public idSubEstructura: number;
    public idSubEstructuraPadre: number;
    public subEstructura: number;
    public vigencia: string;
    public mapead: boolean;

    public tipoDato: TipoDato;
}