export class TipoDatoEModel {
    public id: number;
    public nombre: string;
    public vigencia: string;
}