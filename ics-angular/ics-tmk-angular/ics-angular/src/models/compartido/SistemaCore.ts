export class SistemaCore {
    public id: number;
    public flgVisible: string;
    public nombre: string;
    public vigencia: string;
}