export class TipoFormato {
    public id: number;
    public nombre: string;
    public tipo: string;
    public orden: number;
    public vigencia: string;
}