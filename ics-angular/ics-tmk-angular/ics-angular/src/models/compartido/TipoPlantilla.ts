export class TipoPlantilla {
    public id: number;
    public nombre: string;
    public vigencia: string;
}