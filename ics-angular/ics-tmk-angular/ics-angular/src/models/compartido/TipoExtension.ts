export class TipoExtension {
    public id: number;
    public nombre: string;
    public vigencia: string;
    public formato: string;
}