export class TipoDato {
    public id: number;
    public nombre: string;
    public vigencia: string;
}