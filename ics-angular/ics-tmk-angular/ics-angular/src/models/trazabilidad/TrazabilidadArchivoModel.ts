export class TrazabilidadArchivoModel {
  public id: number;
  public idProceso: number;
  public nombreSocio: string;
  public nombreEvento: string;
  public nombreArchivo: string;
  public estadoTransaccion: string;
  public lineasOriginal: number;
  public fechaInicio: number;
  public fechaFin: number;
  public lineasIcsOk: number;
  public lineasCoreOk: number;
  public lineasIcsError: number;
  public lineasCoreError: number;
  public flgSalida: boolean;
}
