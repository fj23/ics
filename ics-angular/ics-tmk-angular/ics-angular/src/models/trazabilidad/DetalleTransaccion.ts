export class DetalleTransaccion {
    public idDetalleTransaccion: number;
    public proceso: string;
    public socio: string;
    public detalleEtapa: string;
    public fechaInicio: Date;
    public fechaFin: Date;
  }
  