import { DetalleProcesoCargaModel } from './DetalleProcesoCargaModel';
import { Plantilla } from '../plantillas/Plantilla';
import { CallCenter } from '../compartido/CallCenter';
import { TipoArchivo } from '../compartido/TipoArchivo';

export class ProcesosCargaModel {
    public id: number;
    public nombre: string;
    public correoSalida: string;
    public correoEmail: string;
    public correoHitos: string;
    public detalleProceso: DetalleProcesoCargaModel[];
    public plantillaEntradaSocio: Plantilla;
    public plantillaIntegracion: Plantilla;
    public plantillaSalidaSocio: Plantilla;
    public estado: number;
    public vigencia: string;
    public esSalidaAneto: boolean;
    public callCenter?: CallCenter;
    public tipoArchivo?: TipoArchivo;
}