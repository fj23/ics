import { ColumnaMetadatos } from '../compartido/ColumnaMetadatos';

export class EstructuraAneto {
    public id: number;
    public codigo: string;
    public descripcion: string;
    public columnas: ColumnaMetadatos[];
    public vigencia: string;
    public largo?: number;

    public cardMin?: number;
    public cardMax?: number;
}