import { Component, Output, Input, EventEmitter, OnDestroy, HostListener } from "@angular/core";
import { AuthHttpService } from "src/http-services/auth.http-service";
import { MatDialog } from "@angular/material";
import { FontawesomeIconsService } from "../../../../services/fontawesome-icons/fontawesome-icons.service";
import { SessionNoticeDialog } from "../../../dialogs/session-notice/session-notice-dialog";

@Component({
    selector: "app-cabecera",
    templateUrl: "./cabecera.component.html",
    styleUrls: ["./cabecera.component.css"]
})
export class CabeceraComponent implements OnDestroy {

    public localExpDate: Date;
    public intervalSessionTime: number = 1000;
    public inactivitySessionTimeout: number = null;

    protected _intervalSessionCheck;
    protected _isDialogOpen: boolean = false;

    public username: string;

    constructor(
        private authSvc: AuthHttpService, 
        public dialog: MatDialog, 
        public icons: FontawesomeIconsService
    ) {
        this.localExpDate = new Date();
        this.username = localStorage.getItem("username");

        this.inactivitySessionTimeout = parseInt(localStorage.getItem("inactivityNotice")) * 60 * 1000;
        this.localExpDate.setTime(new Date().getTime() + this.inactivitySessionTimeout);

        this._intervalSessionCheck = setInterval(() => {
            if (this.localExpDate.getTime() - 60000 < new Date().getTime() && !this._isDialogOpen) {
                this.abrirDialogoSesionInactiva();
            }
        }, this.intervalSessionTime);
    }

    @HostListener("document:mousedown", ["$event"])
    ngMousedown() {
        if (this.inactivitySessionTimeout != null) {
            this.localExpDate.setTime(new Date().getTime() + this.inactivitySessionTimeout);
        }
    }

    @Input() public sidenavOpen: boolean = false;
    @Output() public sidenavChange = new EventEmitter<boolean>();

    protected abrirDialogoSesionInactiva(): void {
        this._isDialogOpen = true;
        this.dialog.open(SessionNoticeDialog, {
            width: "500px"
        }).afterClosed().subscribe(x => {
            this._isDialogOpen = false;
        });
    }

    ngOnDestroy(): void {
        clearInterval(this._intervalSessionCheck);
    }

    public triggerLogout(): void {
        this.authSvc.logout("Sesión cerrada correctamente", true);
    }

    public toggleSidenav(): void {
        this.sidenavOpen = !this.sidenavOpen;
        this.sidenavChange.emit(this.sidenavOpen);
    }
}