import { Component, OnInit } from '@angular/core';
import { AuthHttpService } from 'src/http-services/auth.http-service';
import { BehaviorSubject, } from 'rxjs';
import { Router, NavigationEnd } from '@angular/router';
import { Permisos } from '../../compartido/Permisos';
import { faClone, IconDefinition, faThLarge, faPlusCircle, faFileSignature, faFileExport, faProjectDiagram, faFileUpload, faFileImport } from '@fortawesome/free-solid-svg-icons';

export class SidenavLink {
  texto: string;
  nivel: number;
  url: string; //debe incluir slash (/) de prefijo
  iconoFontAwesome: IconDefinition;
  router: boolean;
  roles: string;
  hidden: boolean;
}

@Component({
  selector: "app-navegador",
  templateUrl: "./navegador.component.html",
  styleUrls: ["./navegador.component.css"]
})
export class NavegadorComponent implements OnInit {
  private enlacesNavegacionChangeSubject = new BehaviorSubject<SidenavLink[]>(
    []
  );
  public enlacesNavegacionChange$ = this.enlacesNavegacionChangeSubject.asObservable();
  
  public enlacesNavegacion: SidenavLink[] = [
    { texto: "Homologaciones", nivel: 0, url: "/homologaciones", iconoFontAwesome: faClone, router: true, roles: Permisos.ConsultarHomologaciones(), hidden: true },
    { texto: "Estructuras Aneto", nivel: 0, url: "/estructuras", iconoFontAwesome: faThLarge, router: true, roles: Permisos.ConsultarEstructuras(), hidden: true },
    { texto: "Nueva Estructura", nivel: 1, url: "/estructuras/nueva", iconoFontAwesome: faPlusCircle, router: true, roles: Permisos.MantenerEstructura(), hidden: true },
    { texto: "Plantillas", nivel: 0, url: "/plantillas", iconoFontAwesome: faFileSignature, router: true, roles: Permisos.ConsultarPlantillas(), hidden: true },
    { texto: "Nueva Plantilla", nivel: 1, url: "/plantillas/nueva", iconoFontAwesome: faPlusCircle, router: true, roles: Permisos.MantenerPlantilla(), hidden: true },
    { texto: "Procesos de Carga", nivel: 0, url: "/procesos_carga", iconoFontAwesome: faFileExport, router: true, roles: Permisos.ConsultarProcesosDeCarga(), hidden: true },
    { texto: "Trazabilidad de Archivos", nivel: 0, url: "/trazabilidad", iconoFontAwesome: faProjectDiagram, router: true, roles: Permisos.TrazabilidadArchivos(), hidden: true },
    { texto: "Carga Archivo", nivel: 0, url: "/archivos/cargar", iconoFontAwesome: faFileUpload, router: true, roles: Permisos.MantenerCargaArchivo(), hidden: true },
    { texto: "Importar Archivo", nivel: 0, url: "/archivos/importar", iconoFontAwesome: faFileImport, router: true, roles: Permisos.ImportarArchivo(), hidden: true },
  ]

  ngOnInit(): void {
    this.actualizarVisibilidadEnlaces();

    this.router.events.subscribe(evt => {
      if (evt instanceof NavigationEnd) {
        this.actualizarVisibilidadEnlaces();
      }
    });
  }

  private actualizarVisibilidadEnlaces() {
    this.enlacesNavegacion.forEach(elem => {
      elem.hidden = Permisos.DenegarAcceso(elem.roles);
    });
    this.enlacesNavegacionChangeSubject.next(this.enlacesNavegacion);
  }

  public versionBackend: string;

  constructor(private authSvc: AuthHttpService, private router: Router) {
    this.versionBackend = localStorage.getItem("versionBackend");
  }

  isSidenavOpen: boolean = true;

  onSidenavToggle(state: boolean) {
    this.isSidenavOpen = state;
  }
}
