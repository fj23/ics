import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { MAT_SNACK_BAR_DEFAULT_OPTIONS } from "@angular/material/snack-bar";

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgSelectModule } from "@ng-select/ng-select";
import { NgxLoadingModule } from "ngx-loading";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";

import { ConfirmationDialogComponent } from "./dialogs/confirmation/confirmation.dialog.component";
import { FileUploadDialogComponent } from "./dialogs/file-upload/file.upload.dialog.component";
import { InformationDialogComponent } from "./dialogs/information/information.dialog.component";
import { CabeceraComponent } from './paginas/navegador/cabecera/cabecera.component';
import { NavegadorComponent } from "./paginas/navegador/navegador.component";

import { ArchivosCargarComponent } from "./vistas/archivos-cargar/archivos-cargar.component";
import { ArchivosImportarComponent } from "./vistas/archivos-importar/archivos-importar.component";

import { FormularioEstructurasComponent } from "./vistas/estructuras-crear/estructuras-crear.component";
import { EstructurasComponent } from "./vistas/estructuras/estructuras.component";
import { FiltrosEstructurasComponent } from "./vistas/estructuras/filtros/filtros.estructuras.component";
import { TablaEstructurasComponent } from "./vistas/estructuras/tabla/tabla.estructuras.component";
import { TablaSubEstructurasComponent } from "./vistas/estructuras/tabla_sub/tabla.subestructuras.component";

import { FormularioHomologacionComponent } from "./vistas/homologaciones/creacion/formulario.homologaciones.component";
import { FiltrosHomologacionesComponent } from "./vistas/homologaciones/filtros/filtros.homologaciones.component";
import { HomologacionesComponent } from "./vistas/homologaciones/homologaciones.component";
import { ImportarHomologacionesComponent } from "./vistas/homologaciones/importar/importar.homologaciones.component";
import { TablaHomologacionesComponent } from "./vistas/homologaciones/tabla/tabla.homologaciones.component";

import { LoginComponent } from "./paginas/login/login.component";

import { P404Component } from "./paginas/404/404.component";
import { P500Component } from "./paginas/500/500.component";

import { DialogoSubestructuraPlantillaEntradaEdicionComponent } from "./vistas/plantillas-editar/entrada/estructurada/dialogo-subestructura/dialogo-subestructura-plantilla-entrada-edicion.component";
import { SubEstructuraDialogPlantillaSalidaEdicionComponent } from "./vistas/plantillas-editar/salida/estructurada/subestructura/sub-estructura-dialog-plantilla-salida-edicion.component";

import { CreacionProcesosCargaComponent } from "./vistas/procesos-carga/creacion-procesos-carga/creacion-procesos-carga.component";
import { FiltrosProcesosCargaComponent } from "./vistas/procesos-carga/filtros-procesos-carga/filtros-procesos-carga.component";
import { ProcesosCargaComponent } from "./vistas/procesos-carga/procesos-carga.component";
import { TablaProcesosCargaComponent } from "./vistas/procesos-carga/tabla-procesos-carga/tabla-procesos-carga.component";

import { TokenInterceptor } from "../services/auth/token-interceptor.service";
import { CanActivateChildGuard } from "../services/routing/can-activate-child-guard";
import { CanDeactivateGuard } from "../services/routing/can-deactivate-guard";

import { FiltrosTrazabilidadArchivosComponent } from "./vistas/trazabilidad-archivos/filtros/filtros.trazabilidad.component";
import { TablaTrazabilidadArchivosComponent } from "./vistas/trazabilidad-archivos/tabla/tabla.trazabilidad.component";
import { TrazabilidadArchivosComponent } from "./vistas/trazabilidad-archivos/trazabilidad.component";
import { SessionNoticeDialog } from "./dialogs/session-notice/session-notice-dialog";
import { AngularModule } from "./compartido/angular.module";
import { MaterialModule } from "./compartido/material.module";
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { PlantillasModule } from "./plantillas.app.module";
import { FormulaDialogColumnaPlantillaIntegracionComponent } from './dialogs/formulas/formulario.formulas.plantilla-integracion.component';

const SITE_TEMPLATE = [
    NavegadorComponent,
    CabeceraComponent
];

const DIALOGS = [
    ConfirmationDialogComponent,
    FileUploadDialogComponent,
    InformationDialogComponent,
    SessionNoticeDialog,
    DialogoSubestructuraPlantillaEntradaEdicionComponent,
    SubEstructuraDialogPlantillaSalidaEdicionComponent,
    FormulaDialogColumnaPlantillaIntegracionComponent
];

const HOMOLOGACIONES = [
    HomologacionesComponent,
    TablaHomologacionesComponent,
    FiltrosHomologacionesComponent,
    ImportarHomologacionesComponent,
    FormularioHomologacionComponent
];

const ESTRUCTURAS = [
    EstructurasComponent,
    TablaEstructurasComponent,
    FiltrosEstructurasComponent,
    FormularioEstructurasComponent,
    TablaSubEstructurasComponent
];


const PROCESOS_CARGA = [
    ProcesosCargaComponent,
    TablaProcesosCargaComponent,
    FiltrosProcesosCargaComponent,
    CreacionProcesosCargaComponent
];

const TRAZABILIDAD = [
    TrazabilidadArchivosComponent,
    TablaTrazabilidadArchivosComponent,
    FiltrosTrazabilidadArchivosComponent
];

const ARCHIVOS = [
    ArchivosCargarComponent,
    ArchivosImportarComponent
];

const PAGES = [
    P404Component, 
    P500Component
];

@NgModule({
    imports: [
        AngularModule,
        MaterialModule,

        NgSelectModule,
        NgxLoadingModule.forRoot({}),

        FontAwesomeModule,
        
        AppRoutingModule,
        PlantillasModule
    ],
    declarations: [
        AppComponent,
        LoginComponent,
        ...SITE_TEMPLATE,
        ...DIALOGS,
        ...HOMOLOGACIONES,
        ...ESTRUCTURAS,
        ...TRAZABILIDAD,
        ...PROCESOS_CARGA,
        ...ARCHIVOS,
        ...PAGES
    ],
    entryComponents: [
        ...DIALOGS
    ],
    providers: [
        CanActivateChildGuard,
        CanDeactivateGuard,
        { provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: { duration: 3000 } },
        { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
        { provide: MAT_DATE_LOCALE, useValue: "es-CL" }
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
