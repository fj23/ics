import { NativeDateAdapter } from "@angular/material";

export class CustomDateAdapter extends NativeDateAdapter {
  format(date: Date): string {
    const day = date.getUTCDate();
    const month = date.getUTCMonth() + 1;
    const year = date.getFullYear();
    return `${day}/${month}/${year}`;
  }

  getFirstDayOfWeek(): number {
    return 1;
  }
}
