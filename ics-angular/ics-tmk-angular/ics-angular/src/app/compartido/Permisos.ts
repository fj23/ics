/**
 * Clase que contiene los permisos individualizados y ademas algunos permisos agrupados
 * dentro de metodos.
 * Usada tambien para validar si un permiso es denegado o no
 * @see DenegarAcceso(string,string?)
 */
export abstract class Permisos {
    public static get roles() { return localStorage.getItem("roles"); }
    public static separador = ";";

    //PLANTILLAS
    /** F1R */
    public static conf_Plant_Alta = "F1R";
    /** F2R */
    public static conf_Plant_Reca = "F2R";
    /** F3R */
    public static conf_Plant_Canc = "F3R";
    /** F29R */
    public static cons_Plant = "F29R";

    //PROCESOS DE CARGA
    /** F4R */
    public static admi_Proce_Alta = "F4R";
    /** F5R */
    public static admi_Proce_Reca = "F5R";
    /** F6R */
    public static admi_Proce_Canc = "F6R";
    /** F25R */
    public static term_Proce_carga_Alta = "F25R";
    /** F26R */
    public static term_Proce_carga_Reca = "F26R";
    /** F27R */
    public static term_Proce_carga_Canc = "F27R";
    /** F30R */
    public static cons_Confi_Proceso = "F30R";

    //ESTRUCTURAS
    /** F7R */
    public static mant_Estru_Alta = "F7R";
    /** F8R */
    public static mant_Estru_Reca = "F8R";
    /** F9R */
    public static mant_Estru_Canc = "F9R";
    /** F31R */
    public static cons_Estru = "F31R";

    //HOMOLOGACIONES
    /** F10R */
    public static mant_Homol_Alta = "F10R";
    /** F11R */
    public static mant_Homol_Reca = "F11R";
    /** F12R */
    public static mant_Homol_Canc = "F12R";
    /** F32R */
    public static cons_Homol = "F32R";

    //ARCHIVOS
    /** F13R */
    public static impo_Archi_Alta = "F13R";
    /** F14R */
    public static impo_Archi_Reca = "F14R";
    /** F15R */
    public static impo_Archi_Canc = "F15R";
    /** F16R */
    public static carg_Archi_Alta = "F16R";
    /** F17R */
    public static carg_Archi_Reca = "F17R";
    /** F18R */
    public static carg_Archi_Canc = "F18R";
    /** F19R */
    public static expo_Archi_Alta = "F19R";
    /** F20R */
    public static expo_Archi_Reca = "F20R";
    /** F21R */
    public static expo_Archi_Canc = "F21R";

    //IMPORTAR ARCHIVOS
    /** F22R */
    public static actu_Estad_Reg_Carga_Alta = "F22R";
    /** F23R */
    public static actu_Estad_Reg_Carga_Reca = "F23R";
    /** F24R */
    public static actu_Estad_Reg_Carga_Canc = "F24R";

    //TRAZABILIDAD
    /** F28R */
    public static cons_Traza = "F28R";

    /**
     * Devuelve FALSE si el usuario tiene acceso
     * @param roles Los roles que tiene el usuario
     * @param necessaryRoles Los roles que le otorgan acceso
     */
    public static DenegarAcceso(necessaryRoles: string, roles?: string): boolean {
        if (necessaryRoles === null || roles === null || this.roles === null) {
            return true;
        }

        if (!roles) {
            roles = this.roles;
        }

        let reqRoles: string[] = necessaryRoles.split(";");

        //si ningun rol de usuario coincide con los requeridos (indexOf devuelve siempre -1): true
        //si algun rol fuera encontrado coincidente: false
        for (let rol of reqRoles) {
            if (roles.indexOf(rol) >= 0) {
                return false;
            }
        }

        return true;
    }

    /**
     * Verifica que se deniegue el acceso a una URL determinada según los permisos del usuario.
     * Devuelve false si se permite el acceso.
     * @param url La URL a validar
     */
    public static DenegarAccesoByUrl(url: string): boolean {
        let necessaryRoles: string = Permisos.getNecessaryRolesForURL(url);
        return necessaryRoles ? Permisos.DenegarAcceso(necessaryRoles) : true;
    }

    public static getNecessaryRolesForURL(url: string): string {
        if (url == "/homologaciones") {
            return (
                Permisos.ConsultarHomologaciones() + ";" + Permisos.MantenerHomologaciones()
            );
        } else if (url == "/estructuras") {
            return Permisos.ConsultarEstructuras();
        } else if (url == "/plantillas") {
            return Permisos.ConsultarPlantillas();
        } else if (url == "/procesos_carga") {
            return (
                Permisos.ConsultarProcesosDeCarga() + ";" + Permisos.MantenerProcesoCarga()
            );
        } else if (
            url.indexOf("/estructuras/nueva") != -1 ||
            url.indexOf("/estructuras/editar") != -1 ||
            url.indexOf("/estructuras/ver") !== -1
        ) {
            return Permisos.MantenerEstructura();
        } else if (url.indexOf("/plantilla/") != -1) {
            return Permisos.ConsultarPlantillas() + ";" + Permisos.MantenerPlantilla();
        } else if (url.indexOf("/plantillas/nueva") != -1) {
            return Permisos.MantenerPlantilla();
        } else if (url.indexOf("/archivos") != -1) {
            return Permisos.MantenerCargaArchivo();
        } else if (url.indexOf("/trazabilidad") != -1) {
            return Permisos.TrazabilidadArchivos();
        } else if (url.indexOf("/archivos/importar") != -1) {
            return Permisos.ImportarArchivo();
        } else {
            return "";
        }
    }

    /**
     * F10R
     * F11R
     * F12R
     * F32R
     */
    public static ConsultarHomologaciones() {
        return (
            Permisos.mant_Homol_Alta +
            Permisos.separador +
            Permisos.mant_Homol_Reca +
            Permisos.separador +
            Permisos.mant_Homol_Canc +
            Permisos.separador +
            Permisos.cons_Homol
        );
    }

    /**
     * F11R
     * F12R
     * F32R
     */
    public static MantenerHomologaciones() {
        return (
            Permisos.mant_Homol_Reca +
            Permisos.separador +
            Permisos.mant_Homol_Reca +
            Permisos.separador +
            Permisos.mant_Homol_Canc +
            Permisos.separador +
            Permisos.cons_Homol
        );
    }

    /**
     * F7R
     * F8R
     * F9R
     * F31R
     */
    public static ConsultarEstructuras() {
        return (
            Permisos.mant_Estru_Alta +
            Permisos.separador +
            Permisos.mant_Estru_Reca +
            Permisos.separador +
            Permisos.mant_Estru_Canc +
            Permisos.separador +
            Permisos.cons_Estru
        );
    }

    /**
     * F7R
     * F8R
     * F9R
     */
    public static MantenerEstructura() {
        return (
            Permisos.mant_Estru_Alta +
            Permisos.separador +
            Permisos.mant_Estru_Reca +
            Permisos.separador +
            Permisos.mant_Estru_Canc +
            Permisos.separador +
            Permisos.cons_Estru
        );
    }

    /**
     * F1R
     * F2R
     * F3R
     * F29R
     */
    public static ConsultarPlantillas() {
        return (
            Permisos.conf_Plant_Alta +
            Permisos.separador +
            Permisos.conf_Plant_Reca +
            Permisos.separador +
            Permisos.conf_Plant_Canc +
            Permisos.separador +
            Permisos.cons_Plant
        );
    }

    /**
     * F1R
     * F2R
     * F3R
     */
    public static MantenerPlantilla() {
        return (
            Permisos.conf_Plant_Alta +
            Permisos.separador +
            Permisos.conf_Plant_Reca +
            Permisos.separador +
            Permisos.conf_Plant_Canc
        );
    }

    /**
     * F4R
     * F5R
     * F6R
     * F30R
     */
    public static ConsultarProcesosDeCarga() {
        return (
            Permisos.admi_Proce_Alta +
            Permisos.separador +
            Permisos.admi_Proce_Reca +
            Permisos.separador +
            Permisos.admi_Proce_Canc +
            Permisos.separador +
            Permisos.cons_Confi_Proceso
        );
    }

    /**
     * F4R
     * F5R
     * F6R
     */
    public static MantenerProcesoCarga() {
        return (
            Permisos.admi_Proce_Alta +
            Permisos.separador +
            Permisos.admi_Proce_Reca +
            Permisos.separador +
            Permisos.admi_Proce_Canc
        );
    }

    /** F28R */
    public static TrazabilidadArchivos() {
        return Permisos.cons_Traza;
    }

    public static ImportarArchivo(): string {
        return (
            Permisos.actu_Estad_Reg_Carga_Alta +
            Permisos.separador +
            Permisos.actu_Estad_Reg_Carga_Reca +
            Permisos.separador +
            Permisos.actu_Estad_Reg_Carga_Canc
        );
    }

    /**
     * noterminado;NOTERMINADO
     */
    public static Inaccesible() {
        return "noterminado;NOTERMINADO";
    }

    /**
     * F16R
     * F17R
     * F18R
     */
    public static MantenerCargaArchivo() {
        return (
            Permisos.carg_Archi_Alta +
            Permisos.separador +
            Permisos.carg_Archi_Reca +
            Permisos.separador +
            Permisos.carg_Archi_Canc
        );
    }
}
