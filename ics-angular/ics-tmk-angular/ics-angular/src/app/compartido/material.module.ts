import { NgModule } from "@angular/core";
import { MatCheckboxModule, MatDialogModule, MatIconModule, MatListModule, MatPaginatorModule, MatProgressSpinnerModule, MatSidenavModule, MatSnackBarModule, MatSortModule, MatTableModule, MatTooltipModule, MatDatepickerModule, MatNativeDateModule, MatTreeModule, MatFormFieldModule, MatButtonModule, MatInputModule, MatAutocompleteModule, MatCardModule, MatSelectModule, MatProgressBarModule } from "@angular/material";

@NgModule({
    imports: [
        MatCheckboxModule,
        MatDialogModule,
        MatIconModule,
        MatListModule,
        MatPaginatorModule,
        MatProgressSpinnerModule,
        MatSidenavModule,
        MatSnackBarModule,
        MatSortModule,
        MatTableModule,
        MatTooltipModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatTreeModule,
        MatFormFieldModule,
        MatButtonModule,
        MatInputModule,
        MatAutocompleteModule,
        MatCardModule,
        MatSelectModule,
        MatProgressBarModule
    ],
    exports: [
        MatCheckboxModule,
        MatDialogModule,
        MatIconModule,
        MatListModule,
        MatPaginatorModule,
        MatProgressSpinnerModule,
        MatSidenavModule,
        MatSnackBarModule,
        MatSortModule,
        MatTableModule,
        MatTooltipModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatTreeModule,
        MatFormFieldModule,
        MatButtonModule,
        MatInputModule,
        MatAutocompleteModule,
        MatCardModule,
        MatSelectModule,
        MatProgressBarModule
    ]
})
export class MaterialModule {}
