import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatSort, MatPaginator, MatDialog, MatSnackBar } from '@angular/material';
import { merge } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

import { MatPaginatorES } from 'src/app/compartido/mat.paginator.es';
import { EstructurasDataSource } from './tabla.estructuras.datasource';
import { EstructuraAneto } from 'src/models/estructuras/EstructuraAneto';
import { Permisos } from 'src/app/compartido/Permisos';
import { InformationDialogComponent } from 'src/app/dialogs/information/information.dialog.component';
import { FontawesomeIconsService } from 'src/services/fontawesome-icons/fontawesome-icons.service';
import { Title } from '@angular/platform-browser';
import { EstructurasHttpService } from 'src/http-services/estructuras.http-service';
import { ConfirmationDialogComponent } from 'src/app/dialogs/confirmation/confirmation.dialog.component';

@Component({
    selector: 'app-tabla-estructuras',
    templateUrl: './tabla.estructuras.component.html',
    styleUrls: ['./tabla.estructuras.component.css']
})
export class TablaEstructurasComponent implements OnInit, AfterViewInit {

    @ViewChild(MatPaginator) private paginator: MatPaginator;
    @ViewChild(MatSort) private sort: MatSort;
    displayedColumns = ['codigo', 'nombre', 'largo', 'acciones'];
    dataSource: EstructurasDataSource;
    public ocultarPorPermisos: boolean;

    constructor(
        private localSvc: EstructurasHttpService,
        public dialog: MatDialog,
        private snackBar: MatSnackBar,
        private title: Title,
        public icons: FontawesomeIconsService
    ) {
        this.title.setTitle("ICS | Estructuras Aneto");
        this.ocultarPorPermisos = Permisos.DenegarAcceso(Permisos.MantenerEstructura());
    }

    ngOnInit(): void {
        this.paginator._intl = new MatPaginatorES();
        this.dataSource = new EstructurasDataSource(this.localSvc);
    }

    ngAfterViewInit(): void {
        //cuando el orden aplicado a los datos cambie, vuelve a la pagina 1
        this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

        //cuando el orden o el numero de pagina cambie, recarga los datos
        merge(
            this.sort.sortChange,
            this.paginator.page
        ).pipe(
            tap(() => this.recargar())
        ).subscribe();

        this.dataSource.estructurasCount$.subscribe(
            count => this.paginator.length = count
        );
    }

    private recargar(): void {
        this.dataSource.getEstructurasPage(
            this.paginator.pageIndex,
            this.paginator.pageSize,
            this.sort.active,
            this.sort.direction
        );
    }

    public onClickActivarEstructura(estructura: EstructuraAneto): void {
        const confirmationDialog = this.dialog.open(ConfirmationDialogComponent, {
            width: '550px',
            data: {
                titulo: "Activar Estructura",
                mensaje: "Al hacer clic en \"Sí\", esta estructura podrá ser usada en las plantillas del sistema. ¿Desea confirmar esta acción?",
                boton_si_clases: "btn-success",
                boton_no_clases: ""
            }
        });

        confirmationDialog.afterClosed().subscribe(
            (confirmed: boolean) => {
                if (confirmed) {
                    this.localSvc.activar(estructura)
                        .pipe(
                            catchError((err, observable) => { return observable; })
                        ).subscribe(
                            () => {
                                this.snackBar.open("Estructura activada.");
                                estructura.vigencia = "Y";
                            }
                        );
                }
            }
        );
    }

    public onClickDesactivarEstructura(estructura: EstructuraAneto): void {
        const confirmationDialog = this.dialog.open(ConfirmationDialogComponent, {
            width: '550px',
            data: {
                titulo: "Desactivar Estructura",
                mensaje: "Al hacer clic en \"Sí\", esta estructura no podrá ser usada en las plantillas del sistema. ¿Desea confirmar esta acción?",
                boton_si_clases: "btn-warning",
                boton_no_clases: ""
            }
        });

        confirmationDialog.afterClosed().subscribe(
            (confirmed: boolean) => {
                if (confirmed) {
                    this.localSvc.desactivar(estructura).subscribe(
                            resp => {
                                this.snackBar.open("Estructura desactivada.");
                                estructura.vigencia = "N";
                            },
                            err => {
                                console.error(err);
                                this.dialog.open(InformationDialogComponent, {
                                    width: "475px",
                                    data: {
                                      title: "Error al Desactivar",
                                      content: err.error.message
                                    }
                                  });
                            }
                        );
                }
            }
        );
    }

}
