import { OnDestroy } from '@angular/core';
import { DataSource } from "@angular/cdk/table";
import { CollectionViewer } from "@angular/cdk/collections";
import { Observable, BehaviorSubject, of, Subject, Subscription } from 'rxjs';
import { catchError, finalize } from "rxjs/operators";

import { PaginaRegistros } from 'src/models/compartido/PaginaRegistros';
import { SubEstructuraAneto } from 'src/models/estructuras/SubEstructuraAneto';
import { FiltrosEstructurasModel } from 'src/models/filters/FiltrosEstructurasModel';
import { SubEstructurasHttpService } from 'src/http-services/subestructuras.http-service';
import { EstructurasHttpService } from 'src/http-services/estructuras.http-service';

export class SubEstructurasDataSource implements OnDestroy, DataSource<SubEstructuraAneto> {

	private filtrosChangeSub: Subscription;

	/** 
	 * Recibe y transmite el array de estructuras activo a la vista. 
	 */
	private subEstructurasSubject = new Subject<SubEstructuraAneto[]>();

	/** 
	 * Recibe y transmite la cantidad de estructuras conseguidas por los filtros activos.
	 * Esta cantidad suele superar el número de registros en la página actual. 
	 */
	private subEstructurasCountSubject = new BehaviorSubject<number>(0);

	private cargaSubject = new BehaviorSubject<boolean>(false);

	public subEstructurasCount$ = this.subEstructurasCountSubject.asObservable();
	public carga$ = this.cargaSubject.asObservable();	

	constructor(
		private localSvc: SubEstructurasHttpService,
		private estSvc: EstructurasHttpService
	) { 
		this.filtrosChangeSub = this.estSvc.filtrosChange$.subscribe(
			() => this.getSubEstructuras()
		);
	}

	ngOnDestroy(): void {
		this.filtrosChangeSub.unsubscribe();
	}

	connect(collectionViewer: CollectionViewer): Observable<SubEstructuraAneto[] | ReadonlyArray<SubEstructuraAneto>> {
		return this.subEstructurasSubject.asObservable();
	}

	disconnect(collectionViewer: CollectionViewer): void {
		this.subEstructurasSubject.complete();
		this.cargaSubject.complete();
	}

	/**
	 * Solicita la página de estructuras respectiva.
	 * @param pageIndex El índice (con base 0) de la página.
	 * @param pageSize La cantidad de registros a mostrar por página.
	 */
	public getSubEstructurasPage(pageIndex: number, pageSize: number, sortColumn: string, sortOrder: string): void {
		let filtros = this.estSvc.getFiltros();
		filtros.pageIndex = pageIndex;
		filtros.pageSize = pageSize;
		filtros.sortColumn = sortColumn;
		filtros.sortOrder = sortOrder;
		
		this.estSvc.setFiltros(filtros);
	}

	/**
	 * Recarga directamente las estructuras haciendo una llamada al servicio de homologaciones.
	 * Éste debiera ser el último método llamado en la cadena de ejecución para este DataSource.
	 */
	private getSubEstructuras(): void {
		if (!this.cargaSubject.getValue()) {
			this.cargaSubject.next(true);
			const filtros: FiltrosEstructurasModel = this.estSvc.getFiltros();

			this.localSvc.listar(filtros).pipe(
				catchError(() => of([])),
				finalize(() => this.cargaSubject.next(false))
			).subscribe(
				(payload: PaginaRegistros<SubEstructuraAneto>) => {
					this.subEstructurasSubject.next(payload.items);
					if (this.subEstructurasCountSubject.getValue() != payload.count) {
						this.subEstructurasCountSubject.next(payload.count);
					}
				}
			);
		}
	}

}
