import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatSort, MatPaginator, MatDialog, MatSnackBar } from '@angular/material';
import { merge } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

import { MatPaginatorES } from 'src/app/compartido/mat.paginator.es';
import { SubEstructurasDataSource } from './tabla.subestructuras.datasource';

import { SubEstructuraAneto } from 'src/models/estructuras/SubEstructuraAneto';
import { ConfirmationDialogComponent } from 'src/app/dialogs/confirmation/confirmation.dialog.component';
import { Permisos } from 'src/app/compartido/Permisos';
import { InformationDialogComponent } from 'src/app/dialogs/information/information.dialog.component';
import { FontawesomeIconsService } from 'src/services/fontawesome-icons/fontawesome-icons.service';
import { SubEstructurasHttpService } from 'src/http-services/subestructuras.http-service';
import { EstructurasHttpService } from 'src/http-services/estructuras.http-service';

@Component({
    selector: 'app-tabla-subestructuras',
    templateUrl: './tabla.subestructuras.component.html',
    styleUrls: ['./tabla.subestructuras.component.css']
})
export class TablaSubEstructurasComponent implements OnInit, AfterViewInit {

    @ViewChild(MatPaginator) private paginator: MatPaginator;
    @ViewChild(MatSort) private sort: MatSort;
    displayedColumns = ['codigo', 'nombre', 'largo', 'acciones'];
    dataSource: SubEstructurasDataSource;
    public ocultarPorPermisos: boolean;

    constructor(
        private localSvc: SubEstructurasHttpService,
        private estSvc: EstructurasHttpService,
        public dialog: MatDialog,
        private snackBar: MatSnackBar,
        public icons: FontawesomeIconsService
    ) {
        this.ocultarPorPermisos = Permisos.DenegarAcceso(Permisos.MantenerEstructura());
    }

    ngOnInit(): void {
        this.paginator._intl = new MatPaginatorES();
        this.dataSource = new SubEstructurasDataSource(this.localSvc, this.estSvc);
    }

    ngAfterViewInit(): void {
        //cuando el orden aplicado a los datos cambie, vuelve a la pagina 1
        this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

        //cuando el orden o el numero de pagina cambie, recarga los datos
        merge(
            this.sort.sortChange,
            this.paginator.page
        ).pipe(
            tap(() => this.recargar())
        ).subscribe();

        this.dataSource.subEstructurasCount$.subscribe(
            count => this.paginator.length = count
        );
    }

    private recargar(): void {
        this.dataSource.getSubEstructurasPage(
            this.paginator.pageIndex,
            this.paginator.pageSize,
            this.sort.active,
            this.sort.direction
        );
    }

    public onClickActivarSubEstructura(estructura: SubEstructuraAneto): void {
        const confirmationDialog = this.dialog.open(ConfirmationDialogComponent, {
            width: '550px',
            data: {
                titulo: "Activar SubEstructura",
                mensaje: "Al hacer clic en \"Sí\", esta estructura podrá ser usada en las plantillas del sistema. ¿Desea confirmar esta acción?",
                boton_si_clases: "btn-success",
                boton_no_clases: ""
            }
        });

        confirmationDialog.afterClosed().subscribe(
            (confirmed: boolean) => {
                if (confirmed) {
                    this.localSvc.activar(estructura.id)
                        .pipe(
                            catchError((err, observable) => { return observable; })
                        ).subscribe(
                            () => {
                                this.snackBar.open("Subestructura activada.");
                                estructura.vigencia = "Y";
                            }
                        );
                }
            }
        );
    }

    public onClickDesactivarSubEstructura(estructura: SubEstructuraAneto): void {
        const confirmationDialog = this.dialog.open(ConfirmationDialogComponent, {
            width: '550px',
            data: {
                titulo: "Desactivar Estructura",
                mensaje: "Al hacer clic en \"Sí\", esta Subestructura no podrá ser usada en las plantillas del sistema. ¿Desea confirmar esta acción?",
                boton_si_clases: "btn-warning",
                boton_no_clases: ""
            }
        });

        confirmationDialog.afterClosed().subscribe(
            (confirmed: boolean) => {
                if (confirmed) {
                    this.localSvc.desactivar(estructura.id)
                        .subscribe(
                            resp => {
                                this.snackBar.open("Subestructura desactivada.");
                                estructura.vigencia = "N";
                            },
                            err => {
                                console.error(err);
                                this.dialog.open(InformationDialogComponent, {
                                    width: "475px",
                                    data: {
                                      title: "Error al Desactivar",
                                      content: err.error.message
                                    }
                                  });
                            }
                        );
                }
            }
        );
    }

}
