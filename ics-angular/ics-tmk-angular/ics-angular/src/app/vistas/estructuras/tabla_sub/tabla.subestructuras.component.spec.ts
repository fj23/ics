import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TablaSubEstructurasComponent } from './tabla.subestructuras.component';

describe('LoginComponent', () => {
  let component: TablaSubEstructurasComponent;
  let fixture: ComponentFixture<TablaSubEstructurasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TablaSubEstructurasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TablaSubEstructurasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
