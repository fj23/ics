import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { FiltrosEstructurasModel } from 'src/models/filters/FiltrosEstructurasModel';
import { EstructuraAneto } from 'src/models/estructuras/EstructuraAneto';
import { Observable, of } from 'rxjs';
import { PaginaRegistros } from 'src/models/compartido/PaginaRegistros';
import { EstructurasHttpService } from 'src/http-services/estructuras.http-service';

@Component({
    selector: 'app-filtros-estructuras',
    templateUrl: './filtros.estructuras.component.html',
    styleUrls: ['./filtros.estructuras.component.css']
})
export class FiltrosEstructurasComponent implements OnInit {
    estructuras$: Observable<EstructuraAneto[]>;

    formFiltros: FormGroup;

    constructor(
        private localSvc: EstructurasHttpService,
        private fb: FormBuilder
    ) {
        this.formFiltros = this.fb.group({
            codigo: [null],
            nombre: [null]
        });
    }

    ngOnInit(): void {
        this.localSvc.todos().subscribe(
            (data: PaginaRegistros<EstructuraAneto>) => {
                this.estructuras$ = of(data.items);

                let filtroCodigo = sessionStorage.getItem("filtros.estructuras.codigo");
                let filtroNombre = sessionStorage.getItem("filtros.estructuras.nombre");

                if (filtroCodigo && filtroNombre) {

                    sessionStorage.removeItem("filtros.estructuras.codigo");
                    sessionStorage.removeItem("filtros.estructuras.nombre");
                    
                    let selection = data.items.find((est: EstructuraAneto) => {
                        return est.codigo === filtroCodigo && est.descripcion === filtroNombre;
                    });
    
                    if (selection) {
                        this.nombre.setValue(selection);
                        this.codigo.setValue(selection);

                        this.submit();
                    }
                }
            }
        );
    }

    public get nombre() { return this.formFiltros.get("nombre"); }
    public get codigo() { return this.formFiltros.get("codigo"); }

    codigoChange() {
        if (this.codigo.value) {
            this.nombre.patchValue(this.codigo.value);
        }
    }

    /**
     * Genera un nuevo objeto de filtros y lo manda al servicio.
     */
    submit(): void {
        let filtros = new FiltrosEstructurasModel();

        filtros.codigo = "";
        if (this.codigo.value) {
            filtros.codigo = this.codigo.value.codigo;
            sessionStorage.setItem("filtros.estructuras.codigo", this.codigo.value.codigo);
        }

        filtros.nombre = "";
        if (this.nombre.value) {
            filtros.nombre = this.nombre.value.descripcion;
            sessionStorage.setItem("filtros.estructuras.nombre", this.nombre.value.descripcion);
        }

        let settedFiltros: FiltrosEstructurasModel = this.localSvc.getFiltros();
        filtros.pageSize = settedFiltros.pageSize;
        filtros.sortColumn = settedFiltros.sortColumn;
        filtros.sortOrder = settedFiltros.sortOrder;

        this.localSvc.setFiltros(filtros);
    }
}
