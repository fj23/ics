import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';

import { Socio } from 'src/models/compartido/Socio';
import { Evento } from 'src/models/compartido/Evento';
import { TipoPlantilla } from 'src/models/compartido/TipoPlantilla';
import { FiltrosPlantillasModel } from 'src/models/filters/FiltrosPlantillasModel';
import { CommonDataHttpService } from 'src/http-services/common-data.http-service';
import { PlantillasHttpService } from 'src/http-services/plantillas.http-service';

@Component({
  selector: 'app-filtros-plantillas',
  templateUrl: './filtros.plantillas.component.html',
  styleUrls: ['./filtros.plantillas.component.css']
})
export class FiltrosPlantillasComponent implements OnInit {

  plantillaNombresLoading: boolean = true;

  socios$: Observable<Socio[]>;
  eventos$: Observable<Evento[]>;
  tiposPlantilla$: Observable<TipoPlantilla[]>;
  //private estadosPlantilla$: Observable<EstadoPlantillaModel[]>;
  nombresPlantilla = [];

  estadosVigencia = [
    { id: "Y", nombre: "Vigente" },
    { id: "N", nombre: "No Vigente" }
  ];

  formFiltros: FormGroup;

  constructor(
    private commonSvc: CommonDataHttpService,
    private localSvc: PlantillasHttpService,
    private fb: FormBuilder
  ) {
    this.formFiltros = fb.group({
      socio: [null],
      evento: [null],
      tipo: [null],
      vigencia: [null],
      nombre: [null]
    });
  }

  ngOnInit(): void {
    this.socios$ = this.commonSvc.getSocios();
    this.eventos$ = this.commonSvc.getEventos();
    this.tiposPlantilla$ = this.commonSvc.getTiposPlantilla();
    //this.estadosPlantilla$ = this.commonSvc.getEstadosPlantilla();
    this.localSvc.getNombresPlantilla().subscribe(plantillas => {
      this.nombresPlantilla = plantillas;
    });

    this.formFiltros.controls.socio.valueChanges.subscribe(value => {
      this.formFiltros.controls.nombre.setValue(null);
      let evento = this.formFiltros.controls.evento.value;
      this.localSvc.getNombresPlantilla(value, evento).subscribe(resp => {
        this.nombresPlantilla = resp;
      });
    });

    this.formFiltros.controls.evento.valueChanges.subscribe(value => {
      this.formFiltros.controls.nombre.setValue(null);
      let socio = this.formFiltros.controls.socio.value;
      this.localSvc.getNombresPlantilla(socio, value).subscribe(resp => {
        this.nombresPlantilla = resp;
      });
    });
  }

  submit(): void {
    let filtros: FiltrosPlantillasModel = new FiltrosPlantillasModel();

    filtros.socio = this.formFiltros.controls.socio.value;
    filtros.evento = this.formFiltros.controls.evento.value;
    filtros.tipo = this.formFiltros.controls.tipo.value;
    filtros.vigencia = this.formFiltros.controls.vigencia.value;
    filtros.nombre = "";
    if (this.formFiltros.controls.nombre.value ) {
      filtros.nombre = this.formFiltros.controls.nombre.value;
    }
    
    let settedFiltros: FiltrosPlantillasModel = this.localSvc.getFiltros();
    filtros.pageSize = settedFiltros.pageSize;
    filtros.sortColumn = settedFiltros.sortColumn;
    filtros.sortOrder = settedFiltros.sortOrder;

    this.localSvc.setFiltros(filtros);
  }

}
