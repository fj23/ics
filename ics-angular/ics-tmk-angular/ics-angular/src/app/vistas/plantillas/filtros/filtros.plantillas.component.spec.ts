import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FiltrosPlantillasComponent } from './filtros.plantillas.component';

describe('FiltrosPlantillasComponent', () => {
  let component: FiltrosPlantillasComponent;
  let fixture: ComponentFixture<FiltrosPlantillasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FiltrosPlantillasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FiltrosPlantillasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
