import { DataSource } from "@angular/cdk/table";
import { CollectionViewer } from "@angular/cdk/collections";
import { Observable, BehaviorSubject, of, Subscription } from 'rxjs';
import { catchError, finalize } from "rxjs/operators";

import { Plantilla } from "src/models/plantillas/Plantilla";
import { OnDestroy } from "@angular/core";
import { PlantillasHttpService } from "src/http-services/plantillas.http-service";
import { FiltrosPlantillasModel } from "src/models/filters/FiltrosPlantillasModel";
import { PaginaRegistros } from "src/models/compartido/PaginaRegistros";

export class PlantillasDataSource implements DataSource<Plantilla>, OnDestroy {
	
	private cargaSubject = new BehaviorSubject<boolean>(false);
	
    private plantillasSubject = new BehaviorSubject<Plantilla[]>([]);
    public plantillas$: Observable<Plantilla[]> = this.plantillasSubject.asObservable();

	/** Recibe y transmite la cantidad de homologaciones conseguidas por los filtros activos.
	 * Esta cantidad suele superar el número de registros en la página actual. */
	private plantillasCountSubject = new BehaviorSubject<number>(0);

	public plantillasCount$ = this.plantillasCountSubject.asObservable();
	public carga$ = this.cargaSubject.asObservable();
	
	private filtrosChangeSub: Subscription;
	
	constructor(
		private localSvc: PlantillasHttpService
	) { 
		//cuando los filtros, el paginado y/o el orden de la tabla cambien, obtiene las homologaciones
		this.filtrosChangeSub = this.localSvc.filtrosChange$.subscribe(
			() => this.getPlantillas()
		);
	}

	ngOnDestroy(): void {
		this.filtrosChangeSub.unsubscribe();
	}

	connect(collectionViewer: CollectionViewer): Observable<Plantilla[] | ReadonlyArray<Plantilla>> {
		return this.plantillasSubject.asObservable();
	}
	
	disconnect(collectionViewer: CollectionViewer): void {
		this.plantillasSubject.complete();
		this.cargaSubject.complete();
	}

	/**
	 * Solicita la página respectiva.
	 * @param pageIndex El índice (con base 0) de la página.
	 * @param pageSize La cantidad de registros a mostrar por página.
	 */
	public getPlantillasPage(pageIndex: number, pageSize: number, sortColumn: string, sortOrder: string): void {
		let filtros: FiltrosPlantillasModel = this.localSvc.getFiltros();
		filtros.pageIndex = pageIndex;
		filtros.pageSize = pageSize;
		filtros.sortColumn = sortColumn;
		filtros.sortOrder = sortOrder;
		
		this.localSvc.setFiltros(filtros);
	}

	public getPlantillas(): void {

		//si ya está cargando, evita sobrecargar las peticiones
		if (!this.cargaSubject.getValue()) {

			this.cargaSubject.next(true);

			this.localSvc.listar()
			.pipe(
				catchError(() => of([])),
				finalize(() => this.cargaSubject.next(false))
			).subscribe(
				(payload: PaginaRegistros<Plantilla>) =>  {
					this.plantillasSubject.next(payload.items); //el listado se almacena
					if (this.plantillasCountSubject.getValue() != payload.count) { 
						this.plantillasCountSubject.next(payload.count);
					}
				}
			);
		}
	}
	
}
