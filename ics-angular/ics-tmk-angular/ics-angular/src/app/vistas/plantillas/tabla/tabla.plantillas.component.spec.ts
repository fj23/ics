import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TablaPlantillasComponent } from './tabla.plantillas.component';

describe('TablaPlantillasComponent', () => {
  let component: TablaPlantillasComponent;
  let fixture: ComponentFixture<TablaPlantillasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TablaPlantillasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TablaPlantillasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
