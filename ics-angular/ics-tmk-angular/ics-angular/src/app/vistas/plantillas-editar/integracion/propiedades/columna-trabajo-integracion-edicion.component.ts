import { Component, OnInit, Input, OnDestroy, DoCheck  } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import {  MatSnackBar, MatDialog } from '@angular/material';
import { Observable, Subscription, of } from 'rxjs';
import { AtributoEnriquecimiento } from 'src/models/plantillas/AtributoEnriquecimiento';
import { CommonDataHttpService } from 'src/http-services/common-data.http-service';
import { PlantillasHttpService } from 'src/http-services/plantillas.http-service';
import { HomologacionesHttpService } from 'src/http-services/homologaciones.http-service';
import { FormulaIntegracion as Formulas } from 'src/models/plantillas/integracion/FormulaIntegracion';
import { MapeoIntegracion } from 'src/models/plantillas/integracion/MapeoIntegracion';
import { ConfirmationDialogComponent } from 'src/app/dialogs/confirmation/confirmation.dialog.component';
import { Socio } from 'src/models/compartido/Socio';
import { FontawesomeIconsService } from 'src/services/fontawesome-icons/fontawesome-icons.service';
import { ColumnaPlantillaIntegracionComponent } from '../../../plantillas-crear/integracion/columna/columna-trabajo-integracion.component';
import { FormularioPlantillasService } from '../../../../../services/formulario-plantillas/formulario-plantillas.service';

const TRANSFORMACION_FORMATO = "Formato";
const TRANSFORMACION_HOMOLOGACION = "Homologación";

@Component({
    selector: 'app-columna-trabajo-integracion-edicion',
    templateUrl: './columna-trabajo-integracion-edicion.component.html',
    styleUrls: ['./columna-trabajo-integracion-edicion.component.css']
})
export class ColumnaPlantillaIntegracionEdicionComponent 
extends ColumnaPlantillaIntegracionComponent implements OnInit, OnDestroy  {

    protected atributosEnriquecimiento: AtributoEnriquecimiento[];

    public enriquecimientosCargados$: Observable<boolean> = of(false);

    protected changeDarFormatoSub: Subscription;
    protected changeRealizarHomologacionSub: Subscription;
    protected changeAplicarFormulaSub: Subscription;

    private _accionUser: string;
    @Input() public set AccionUser(accionUser: string) {
        const thenMode = this.isVerMode;
        this._accionUser = accionUser;
        const nowMode = this.isVerMode;

        if (thenMode !== nowMode) {
            this.onChangeColumnaTrabajo();
        }
    }

    @Input() public mapeosIntegracion: MapeoIntegracion[];

    constructor(
        protected commonSvc: CommonDataHttpService,
        protected localSvc: PlantillasHttpService,
        protected formSvc: FormularioPlantillasService,
        protected fb: FormBuilder,
        protected snackBar: MatSnackBar,
        protected dialog: MatDialog,
        protected homoSvc: HomologacionesHttpService,
        public icons: FontawesomeIconsService
    ) {
        super(commonSvc, localSvc, formSvc, fb, snackBar, dialog, homoSvc, icons);
    }

    ngOnInit(): void {

        this.atributosEnriquecimiento$ = this.localSvc.getAtributosEnriquecimiento(this._core.id);
        this.formatoFechasItems$ = this.commonSvc.getTiposFormato();

        this.atributosEnriquecimiento$.subscribe(
            (atributos: AtributoEnriquecimiento[]) => {
                this.atributosEnriquecimiento = atributos;
                this.enriquecimientosCargados$ = of(true);
            }
        );

        this.iniciarSuscripciones();

        this.formSvc.socio$.subscribe(
            (socio: Socio) => {
                this._socio = socio;
                this.actualizarTiposHomologacion();
            }
        );
    }

    ngOnDestroy(): void {
        this.changeDarFormatoSub.unsubscribe();
        this.changeRealizarHomologacionSub.unsubscribe();
        this.changeAplicarFormulaSub.unsubscribe();
    }

    public get isVerMode(): boolean {
        if (this._accionUser) {
            return this._accionUser.toLocaleLowerCase() === 'ver';
        } else {
            return true;
        }
    }

    protected onChangeColumnaTrabajo(): void {
        
        this.limpiarFormulario();

        if (this.columnaTrabajo) { 
            const noChange = {emitEvent: false, onlySelf: true};
            const verMode = this.isVerMode;
            
            this.nombreColumna.setValue(this.columnaTrabajo.nombre, noChange);

            if (this.columnaTrabajo.formula) {
                if (!verMode) {
                    this.aplicarFormula.enable(noChange);
                }
                this.darColumnaOrigen.disable(noChange);
                this.darAtributoEnriquecimiento.disable(noChange);
                
                this.aplicarFormula.setValue(true, noChange);
            } else if (this.columnaTrabajo.columnaOrigen) {
                if (!verMode) {
                    this.darColumnaOrigen.enable(noChange);
                    this.columnaOrigen.enable(noChange);
                }
                this.darAtributoEnriquecimiento.disable(noChange);
                this.aplicarFormula.disable(noChange);
                
                this.darColumnaOrigen.setValue(true, noChange);
                this.columnaOrigen.setValue(this.columnaTrabajo.columnaOrigen, noChange);
            } else if (this.columnaTrabajo.columnaEnriquecimiento) {
                if (!verMode) {
                    this.darAtributoEnriquecimiento.enable(noChange);
                    this.atributoEnriquecimiento.enable(noChange);
                }
                this.darColumnaOrigen.disable(noChange);
                this.aplicarFormula.disable(noChange);

                this.darAtributoEnriquecimiento.setValue(true, noChange);
                this.atributoEnriquecimiento.setValue(this.columnaTrabajo.columnaEnriquecimiento, noChange);
            } else {
                if (!verMode) {
                    this.aplicarFormula.enable(noChange);
                    this.darColumnaOrigen.enable(noChange);
                    this.darAtributoEnriquecimiento.enable(noChange);
                }
            }

            this.transformaciones = [];

            if (!verMode) {
                this.darFormato.enable(noChange);
                this.realizarHomologacion.enable(noChange);
            }

            if (this.columnaTrabajo.formato) {
                if (!this.columnaTrabajo.formato.orden) {
                    this.columnaTrabajo.formato.orden = 1;
                }
                this.transformaciones[this.columnaTrabajo.formato.orden - 1] = TRANSFORMACION_FORMATO;
                this.darFormato.setValue(true, noChange);
                this.formatoFecha.setValue(this.columnaTrabajo.formato.id, noChange);
                if (!verMode) {
                    this.formatoFecha.enable(noChange);
                }
            }

            if (this.columnaTrabajo.homologacion) {
                if (!this.columnaTrabajo.homologacion.orden) {
                    this.columnaTrabajo.homologacion.orden = 1;
                }
                this.transformaciones[this.columnaTrabajo.homologacion.orden - 1] = TRANSFORMACION_HOMOLOGACION;
                this.realizarHomologacion.setValue(true, noChange);
                this.tipoHomologacion.setValue(this.columnaTrabajo.homologacion.id, noChange);
                if (!verMode) {
                    this.tipoHomologacion.enable(noChange);
                }
            }

            this._transformaciones$.next(this.transformaciones);

        }
    }

    protected findExistsInMapeos(id: number): boolean {
        for (let i = 0; i < this.mapeosIntegracion.length; i++) {
            let mapeo: MapeoIntegracion = this.mapeosIntegracion[i];
            if (mapeo.columnaOrigen.columnaEnriquecimiento) {
                if (mapeo.columnaOrigen.columnaEnriquecimiento.idAtributoEnriquecimiento == id) {
                    return true;
                }
            } else if (mapeo.columnaOrigen.columnaOrigen) {
                if (mapeo.columnaOrigen.columnaOrigen.idAtributoNegocio == id) {
                    return true;
                }
            }

        }
        return false;
    }

    public onToggleDarAtributoEnriquecimiento(): void {
        const noChange = {onlySelf: true, emitEvent: false};
        if (this.darAtributoEnriquecimiento.value) {
            super.onToggleDarAtributoEnriquecimiento();
        } else {
            const enriquecimiento: AtributoEnriquecimiento = this.atributoEnriquecimiento.value;
            if (enriquecimiento && this.findExistsInMapeos(enriquecimiento.idAtributoEnriquecimiento)) {
                
                this.dialog.open(ConfirmationDialogComponent, {
                    width: '600px',
                    data: {
                        titulo: "Plantilla Integración",
                        mensaje: "Actualmente existen otras columnas de integración de destino asociadas a atributos de enrequecimiento. ¿Está seguro de cambiar su origen?",
                        boton_si_clases: "btn-success",
                        boton_no_clases: ""
                    }
                }).afterClosed().subscribe(
                    (confirmed: boolean) => {
                        if (confirmed) {
                            super.onToggleDarAtributoEnriquecimiento();
                        } else {
                            this.darAtributoEnriquecimiento.setValue(true, noChange);
                            this.darColumnaOrigen.disable(noChange);
                            this.aplicarFormula.disable(noChange);
                        }
                    }
                );
            } else {
                this.atributoEnriquecimiento.setValue(null, noChange);
                this.atributoEnriquecimiento.disable(noChange);
                if (!this.isVerMode) {
                    this.darColumnaOrigen.enable();
                }
            }
        }
    }
    
}
