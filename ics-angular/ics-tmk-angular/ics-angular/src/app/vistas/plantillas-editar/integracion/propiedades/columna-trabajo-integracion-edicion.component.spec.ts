import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColumnaPlantillaIntegracionEdicionComponent } from './columna-trabajo-integracion-edicion.component';

describe('ColumnaPlantillaIntegracionEdicionComponent', () => {
  let component: ColumnaPlantillaIntegracionEdicionComponent;
  let fixture: ComponentFixture<ColumnaPlantillaIntegracionEdicionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ColumnaPlantillaIntegracionEdicionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColumnaPlantillaIntegracionEdicionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
