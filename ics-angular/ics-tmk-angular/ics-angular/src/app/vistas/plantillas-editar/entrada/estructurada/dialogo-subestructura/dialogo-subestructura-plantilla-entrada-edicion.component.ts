import { Component, OnInit, ViewChild, OnDestroy, Input, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef, MatCheckboxChange, MatSnackBar } from '@angular/material';
import { Subscription, Observable } from 'rxjs';
import { ColumnaDestinoEntrada } from 'src/models/plantillas/entrada/ColumnaDestinoEntrada';
import { ColumnaMetadatos } from 'src/models/compartido/ColumnaMetadatos';
import { PlantillasHttpService } from 'src/http-services/plantillas.http-service';
import { Evento } from 'src/models/compartido/Evento';
import { FormularioPlantillasService } from 'src/services/formulario-plantillas/formulario-plantillas.service';

@Component({
    selector: 'app-dialogo-subestructura-plantilla-entrada-edicion',
    templateUrl: './dialogo-subestructura-plantilla-entrada-edicion.component.html',
    styleUrls: ['./dialogo-subestructura-plantilla-entrada-edicion.component.css']
})
export class DialogoSubestructuraPlantillaEntradaEdicionComponent implements OnInit, OnDestroy {

    public columnasDestinoDisponibles$: Observable<ColumnaDestinoEntrada[]>;
    public indiceColumnaSeleccionada: number;

    public columnaDestinoSeleccionada: ColumnaDestinoEntrada;

    public subEstructuraColumnasDisplayedColumns: string[] = ["codigo", "nombre", "tipoDato", "acciones"];

    public filtroForm: FormGroup;

    public accionUser: string;
    @Input() set AccionUser(accionUser: string) {
        this.accionUser = accionUser;

        if (this.accionUser == "ver") {
            this.filtroForm.disable();

        } else if (this.accionUser == "editar") {
            this.filtroForm.enable();
        }
    }

    private evento: Evento;

    private campoFiltrarRegistrosSub: Subscription;


    constructor(
        @Inject(MAT_DIALOG_DATA) public data: {
            columnaOrigen: ColumnaMetadatos,
            accionUser: string,
            evento: Evento
        },
        private localSvc: PlantillasHttpService,
        private formSvc: FormularioPlantillasService,
        private selfDialog: MatDialogRef<DialogoSubestructuraPlantillaEntradaEdicionComponent>,
        private snackBar: MatSnackBar,
        private fb: FormBuilder
    ) {
        this.filtroForm = this.fb.group({
            filtrarRegistros: [false],
            valorFiltrado: ['']
        });

        this.AccionUser = this.data.accionUser;
        this.evento = data.evento;
    }

    get filtrarRegistros() { return this.filtroForm.get("filtrarRegistros"); }
    get valorFiltrado() { return this.filtroForm.get("valorFiltrado"); }


    ngOnInit(): void {
        this.columnasDestinoDisponibles$ = this.localSvc.getColumnasDestinoEntradaDisponiblesUsuarios(this.evento.id);

        this.campoFiltrarRegistrosSub = this.filtrarRegistros.valueChanges.subscribe(
            (filtrar: boolean) => {
                if (filtrar) {
                    this.valorFiltrado.enable({ onlySelf: true, emitEvent: false });
                    this.valorFiltrado.setValidators(Validators.required);
                }
                else {
                    this.valorFiltrado.disable({ onlySelf: true, emitEvent: false });
                    this.valorFiltrado.patchValue("");
                    this.valorFiltrado.setValidators(null);
                }

            }
        );
    }

    ngOnDestroy(): void {
        this.campoFiltrarRegistrosSub.unsubscribe();
    }

    onClickSelectColumna(event: MatCheckboxChange, index: number): void {
        this.indiceColumnaSeleccionada = (event.checked) ? index : null;
    }

    onClickMapear(): void {
        if (!this.columnaDestinoSeleccionada) {
            this.snackBar.open("Debe seleccionar una columna de destino para mapear.");
        }
        else if (!this.indiceColumnaSeleccionada && this.indiceColumnaSeleccionada !== 0) {
            this.snackBar.open("Debe seleccionar una columna de la subestructura para mapear.");
        }
        else {
            const valorParaFiltrar = this.filtrarRegistros.value? this.valorFiltrado.value : null;
            this.selfDialog.close({
                columnaDestino: JSON.parse(JSON.stringify(this.columnaDestinoSeleccionada)),
                columnaSubEstructuraIndex: this.indiceColumnaSeleccionada,
                valorFiltrado: valorParaFiltrar
            });
        }

    }

}
