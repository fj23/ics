import { Component, OnInit, Input, OnDestroy, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { Subscription, Observable } from 'rxjs';
import { TipoDato } from 'src/models/compartido/TipoDato';
import { ColumnaMetadatos } from 'src/models/compartido/ColumnaMetadatos';
import { CommonDataHttpService } from 'src/http-services/common-data.http-service';
import { PlantillasHttpService } from 'src/http-services/plantillas.http-service';
import { TipoExtensionEnum } from 'src/enums/TipoExtensionEnum';
import { FormularioPlantillasService } from 'src/services/formulario-plantillas/formulario-plantillas.service';

@Component({
    selector: 'app-columna-archivo-entrada-edicion',
    templateUrl: './columna-archivo-entrada-edicion.component.html',
    styleUrls: ['./columna-archivo-entrada-edicion.component.css']
})
export class ColumnaArchivoEstandarPlantillaEntradaEdicionComponent implements OnInit, OnDestroy {

    public tiposDato$: Observable<TipoDato[]>;

    public campoArchivoForm: FormGroup;

    @Output() public columnaSubmit: EventEmitter<ColumnaMetadatos> = new EventEmitter<ColumnaMetadatos>();

    public formatos = { txt: "Plano", estructurado: "Estructurado", csv: "CSV", excel: "Excel" };

    @Input() public accionUser: string;

    public delimitadorColumnas: string

    private entradaFormatoSub: Subscription;

    constructor(
        private commonSvc: CommonDataHttpService,
        private formSvc: FormularioPlantillasService,
        private localSvc: PlantillasHttpService,
        private fb: FormBuilder,
        private snackBar: MatSnackBar
    ) {
        this.campoArchivoForm = this.fb.group({
            nombre: ['', Validators.required],
            tipoDato: ['', Validators.required],
            posInicial: [''],
            posFinal: [''],
            llave: [false]
        });
    }

    public get nombre() { return this.campoArchivoForm.get("nombre"); }
    public get tipoDato() { return this.campoArchivoForm.get("tipoDato"); }
    public get posInicial() { return this.campoArchivoForm.get("posInicial"); }
    public get posFinal() { return this.campoArchivoForm.get("posFinal"); }
    public get llave() { return this.campoArchivoForm.get("llave"); }

    ngOnInit(): void {
        this.tiposDato$ = this.localSvc.getTiposDatoColumnaEstandar();

        if (this.accionUser === "ver") {
            this.campoArchivoForm.disable();

        } else if (this.accionUser === "editar") {
            this.campoArchivoForm.enable();
        }

        this.entradaFormatoSub = this.formSvc.entradaFormatoSubject.subscribe(() => { this.updateColumnPositionRequirement(); });
    }

    ngOnDestroy() {
        this.entradaFormatoSub.unsubscribe();
    }

    @Input() set DelimitadorColumnas(delimitadorColumnas: string) {
        this.delimitadorColumnas = delimitadorColumnas;
        this.updateColumnPositionRequirement();
    }

    private updateColumnPositionRequirement() {
        if (this.formSvc.entradaFormatoSubject.getValue() === TipoExtensionEnum.PLANO && 
            !(this.delimitadorColumnas && this.delimitadorColumnas.length > 0)) {
            this.posInicial.enable();
            this.posFinal.enable();
            this.posInicial.setValidators(Validators.compose([Validators.required, Validators.min(0)]));
            this.posFinal.setValidators(Validators.compose([Validators.required, Validators.min(1)]));
        }
        else  {
            this.posInicial.disable();
            this.posFinal.disable();
            this.posInicial.setValue(null);
            this.posFinal.setValue(null);
            this.posInicial.setValidators(null);
            this.posFinal.setValidators(null);
        }
        this.posInicial.updateValueAndValidity();
        this.posFinal.updateValueAndValidity();
    }

    /**
     * Con los datos del formulario en el recuadro 'nueva columna', crea un objeto modelo.
     */
    private buildColumna(): ColumnaMetadatos {
        let model: ColumnaMetadatos = new ColumnaMetadatos();

        model.tipoDato = this.tipoDato.value;
        model.descripcion = this.nombre.value;
        model.esLlave = this.llave.value;
        model.mapeada = false;

        let formatoEntrada: number = this.formSvc.entradaFormatoSubject.getValue();

        if (this.delimitadorColumnas && this.delimitadorColumnas.length > 0) {
            model.posicionInicial = this.posInicial.value;
            model.posicionFinal = this.posFinal.value;
        } 
        else if (formatoEntrada === TipoExtensionEnum.PLANO) {
            if (this.posInicial.value && this.posFinal.value) {
                if (this.posInicial.value > this.posFinal.value) {
                    return null;
                }
                else {
                    model.posicionInicial = this.posInicial.value;
                    model.posicionFinal = this.posFinal.value;
                }
            }
        }

        return model;
    }

    /**
     * Valida el formulario de columna, la crea y la agrega al archivo actual.
     */
    public submitColumna(): void {
        if (!this.campoArchivoForm.valid) {
            for (const ctrlId in this.campoArchivoForm.controls) {
                this.campoArchivoForm.controls[ctrlId].markAsTouched();
            }
            this.snackBar.open("Debe rellenar correctamente los datos del formulario para agregar una columna.");
        }
        else {
            let columna: ColumnaMetadatos = this.buildColumna();
            if (columna != null) {
                this.columnaSubmit.emit(columna);
                this.campoArchivoForm.reset();
            }
        }
    }

}
