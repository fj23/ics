import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArchivoEstructuradoPlantillaEntradaEdicionComponent } from './archivo-entrada-estructurada-edicion.component';

describe('ArchivoEstructuradoPlantillaEntradaEdicionComponent', () => {
  let component: ArchivoEstructuradoPlantillaEntradaEdicionComponent;
  let fixture: ComponentFixture<ArchivoEstructuradoPlantillaEntradaEdicionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArchivoEstructuradoPlantillaEntradaEdicionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArchivoEstructuradoPlantillaEntradaEdicionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
