import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArchivoEstandarPlantillaEntradaEdicionComponent } from './archivo-entrada-estandar-edicion.component';

describe('ArchivoEstandarPlantillaEntradaEdicionComponent', () => {
  let component: ArchivoEstandarPlantillaEntradaEdicionComponent;
  let fixture: ComponentFixture<ArchivoEstandarPlantillaEntradaEdicionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArchivoEstandarPlantillaEntradaEdicionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArchivoEstandarPlantillaEntradaEdicionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
