import { Component, OnInit, Input, OnDestroy, Output, EventEmitter, ApplicationRef } from '@angular/core';
import { FormBuilder,  } from '@angular/forms';
import { MatSnackBar, MatDialog } from '@angular/material';
import { MapeoEntrada } from 'src/models/plantillas/entrada/MapeoEntrada';
import { CommonDataHttpService } from 'src/http-services/common-data.http-service';
import { ArchivoEstandarPlantillaEntradaComponent } from '../../../plantillas-crear/entrada/estandar/archivo-entrada-estandar.component';
import { FontawesomeIconsService } from 'src/services/fontawesome-icons/fontawesome-icons.service';
import { FormularioPlantillasService } from '../../../../../services/formulario-plantillas/formulario-plantillas.service';

@Component({
    selector: 'app-archivo-entrada-estandar-edicion',
    templateUrl: './archivo-entrada-estandar-edicion.component.html',
    styleUrls: ['./archivo-entrada-estandar-edicion.component.css']
})
export class ArchivoEstandarPlantillaEntradaEdicionComponent extends ArchivoEstandarPlantillaEntradaComponent implements OnInit, OnDestroy {

    protected _accionUser: string;
    @Input() public set AccionUser(accionUser: string) {
        this._accionUser = accionUser;
        this.procesarHabilidadCamposFormulario();
    }

    @Output() public mapeoColumnaDestino = new EventEmitter<MapeoEntrada>();

    constructor(
        protected appRef: ApplicationRef,
        protected commonSvc: CommonDataHttpService,
        protected formSvc: FormularioPlantillasService,
        protected fb: FormBuilder,
        protected snackBar: MatSnackBar,
        protected dialog: MatDialog,
        public icons: FontawesomeIconsService
    ) {
        super(appRef, commonSvc, formSvc, fb, snackBar, dialog, icons);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    public get isVerMode(): boolean {
        if (this._accionUser) {
            return this._accionUser.toLocaleLowerCase() === 'ver';
        } else {
            return true;
        }
    }

    private procesarHabilidadCamposFormulario() {

        if (this.isVerMode) {
            this.creacionArchivoEntradaForm.disable({onlySelf: true, emitEvent: false});
        } else {
            this.creacionArchivoEntradaForm.enable({onlySelf: true, emitEvent: false});
        }
    }

    public onCambioArchivo() {
        // habilita o deshabilita TODO el formulario
        this.procesarHabilidadCamposFormulario();
        
        // setTimeout es un hack menor, concede tiempo de renderizado
        // en un futuro se podria considerar usar this.appRef.tick() en vez de este hack
        setTimeout(
            ()=>{ 
                super.onCambioArchivo();
            }, 
            1
        ); 

    }


}
