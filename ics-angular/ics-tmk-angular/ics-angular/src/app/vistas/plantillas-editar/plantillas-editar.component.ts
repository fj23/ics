import { Component, OnInit, OnDestroy, ApplicationRef, DoCheck, ViewChild } from '@angular/core';
import { Observable, Subscription, of, merge, Subject, BehaviorSubject } from 'rxjs';
import { FormBuilder } from '@angular/forms';
import { CommonDataHttpService } from 'src/http-services/common-data.http-service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormularioPlantillasService } from '../../../services/formulario-plantillas/formulario-plantillas.service';
import { MatSnackBar, MatDialog } from '@angular/material';
import { PlantillaEModel } from 'src/models/edicion/plantillas/PlantillaEModel';
import { PlantillasCrearComponent } from '../plantillas-crear/plantillas-crear.component';
import { PlantillaEntradaEdicionComponent } from './entrada/plantilla-entrada-edicion.component';
import { PlantillaSalidaEdicionComponent } from './salida/plantilla-salida-edicion.component';
import { FontawesomeIconsService } from 'src/services/fontawesome-icons/fontawesome-icons.service';
import { Title } from '@angular/platform-browser';
import { PlantillasHttpService } from 'src/http-services/plantillas.http-service';

@Component({
    selector: 'app-plantillas-editar',
    templateUrl: './plantillas-editar.component.html',
    styleUrls: ['./plantillas-editar.component.css']
})
export class PlantillaEdicionComponent extends PlantillasCrearComponent implements OnInit, OnDestroy {
    
    public _accionUser: string;
    public accionUser$: Observable<string>;

    private set AccionUser(accionUser: string) {
        let accion: string;
        if (!accionUser || accionUser !== "editar") {
            accion = "ver";
        } else {
            accion = accionUser;
        } 
        this.loadPlantillaFromId(this.idPlantilla);


        this._accionUser = accion;
        this.accionUser$ = of(accion);
    }

    protected _plantilla$: Subject<PlantillaEModel>;
    public plantilla$: Observable<PlantillaEModel>;


    @ViewChild("plantillaEntrada") public plantillaEntrada: PlantillaEntradaEdicionComponent;
    @ViewChild("plantillaSalida") private plantillaSalidaEdicion: PlantillaSalidaEdicionComponent;

    private getPlantillaFromIdSub: Subscription;
    private routerAccionUserSub: Subscription;

    constructor(
        protected appRef: ApplicationRef,
        protected commonSvc: CommonDataHttpService,
        protected localSvc: PlantillasHttpService,
        protected router: Router,
        protected formSvc: FormularioPlantillasService,
        protected fb: FormBuilder,
        protected snackBar: MatSnackBar,
        protected dialog: MatDialog,
        private route: ActivatedRoute,
        public title: Title,
        public icons: FontawesomeIconsService
    ) {
        super(appRef, commonSvc, localSvc, router, formSvc, fb, snackBar, dialog, title, icons);
        this._plantilla$ = new BehaviorSubject(null);
        this.plantilla$ = this._plantilla$.asObservable();
    }

    public get isVerMode(): boolean { return this._accionUser.toLocaleLowerCase() === 'ver'; }
    public get idPlantilla(): number { 
        const id = this.route.snapshot.paramMap.get('id'); 
        if (id) {
            const nId = Number(id);
            if (!isNaN(nId)) {
                return nId;
            }
        }
        return null;
    }


    ngOnInit(): void {
        // NO USAR super.ngOnInit() 
        // iria a consultar la version de plantilla
        // y provocaria un loop infinito

        this.eventos$ = this.commonSvc.getEventos();
        this.socios$ = this.commonSvc.getSocios();
        this.tiposPlantilla$ = this.commonSvc.getTiposPlantilla();
        this.nombresPlantilla$ = this.localSvc.getNombresPlantilla();

        this.changeBloqueoDatosBasicosSub = this.formSvc.bloquearDatosBasicosPlantillaSubject.subscribe((bloquear) => { this.onChangeBloqueoDatosBasicosPlantilla(bloquear); });
        this.changeCompletitudFormularioSub = this.formSvc.completitudFormularioSubject.subscribe((completo) => { this.onChangeCompletitudFormulario(completo); });

        //este setter desencadena el proceso completo de carga de la plantilla en base al tipo de accion ver/editar
        this.routerAccionUserSub = this.route.params.subscribe((params) => {
            this.AccionUser = params.accion;
        });
        
        if (this.isVerMode) {
            this.observaciones.disable();
            this.salidaFormatoOriginalEstado.disable();
        }

    }

    ngOnDestroy(): void {
        if (this.getPlantillaFromIdSub) this.getPlantillaFromIdSub.unsubscribe();
    }

    public get canDeactivateForm(): boolean {
        return this.isVerMode || this.formSubmitted;
    }

    private loadPlantillaFromId(id: number) {
        this._plantilla$.next(null);

        if (!id) {
            this.snackBar.open("Id de plantilla inválido.");
            this.router.navigate(["/plantillas"]);
        } else {
            this.getPlantillaFromIdSub = this.localSvc.getPlantillaByIdEdicion(id)
                .subscribe((plantilla: PlantillaEModel) => {
                    
                    if (plantilla) {
                        this.plantillaId = plantilla.id;

                        const noChange = { onlySelf: true, emitEvent: false };
                        this.nombre.setValue(plantilla.nombre, noChange);
                        this.observaciones.setValue(plantilla.observaciones, noChange);
                        this.tipo.setValue(plantilla.tipo, noChange);
                        this.evento.setValue(plantilla.evento, noChange);
                        this.socio.setValue(plantilla.socio, noChange);

                        this.creacionPlantillaForm.updateValueAndValidity();
                        this.creacionPlantillaForm.disable();
                        if (!this.isVerMode) {
                            this.salidaFormatoOriginalEstado.enable();
                            this.observaciones.enable();
                        }

                        if (plantilla.tipo.id === 100002 && plantilla.tipoSalida.idTipoSalida === 5000001) {
                            this.salidaFormatoOriginalEstado.setValue(true, noChange);
                            this.formSvc.completitudFormularioSubject.next(true);
                        }

                        if (this.isVerMode) {
                            this.version.setValue(plantilla.version, noChange);
                        } else {
                            this.version.setValue("Cargando...", noChange);
                            this.onChangeIdentificadoresPlantilla();
                        }
                        this._plantilla$.next(plantilla);
                    }
                }, (err) => {
                    console.error(err);
                }
            );
        }
    }

    public isPlantillaSalidaEditable(): boolean {
        if (this.salidaFormatoOriginalEstado.value) {
            return false;
        } else {
            if ((this.tipo.value && this.tipo.value.id === 100002) && this.idPlantilla) {
                return true;
            }
        }
        return false;
    }

}
