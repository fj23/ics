import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstandarSalidaEdicionComponent } from './salida-estandar-edicion.component';

describe('EstandarSalidaEdicionComponent', () => {
  let component: EstandarSalidaEdicionComponent;
  let fixture: ComponentFixture<EstandarSalidaEdicionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstandarSalidaEdicionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstandarSalidaEdicionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
