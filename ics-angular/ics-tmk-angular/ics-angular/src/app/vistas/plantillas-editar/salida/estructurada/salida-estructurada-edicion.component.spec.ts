import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstructuradaSalidaEdicionComponent } from './salida-estructurada-edicion.component';

describe('EstructuradaSalidaEdicionComponent', () => {
  let component: EstructuradaSalidaEdicionComponent;
  let fixture: ComponentFixture<EstructuradaSalidaEdicionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstructuradaSalidaEdicionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstructuradaSalidaEdicionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
