import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatTable, MatSnackBar, MatDialog } from '@angular/material';
import { CommonDataHttpService } from 'src/http-services/common-data.http-service';
import { PlantillasHttpService } from 'src/http-services/plantillas.http-service';
import { EstructurasHttpService } from 'src/http-services/estructuras.http-service';
import { FontawesomeIconsService } from 'src/services/fontawesome-icons/fontawesome-icons.service';
import { FormularioEstructurasPlantillaSalidaComponent } from '../../../plantillas-crear/salida/estructurada/salida-estructurada.component';
import { EstructuraAnetoPlantillaSalidaOutputModel, ColumnaEstructuraPlantillaSalidaOutputModel } from 'src/output_models/EstructuraAnetoPlantillaSalidaOutputModel';
import { EstructuraAnetoSalida } from 'src/models/plantillas/salida/estructura/EstructuraAnetoSalida';
import { ColumnaEstructuraAnetoSalida } from 'src/models/plantillas/salida/estructura/ColumnaEstructuraAnetoSalida';
import { FormulaIntegracion as Formulas } from 'src/models/plantillas/integracion/FormulaIntegracion';
import { FormularioPlantillasService } from '../../../../../services/formulario-plantillas/formulario-plantillas.service';
import { HomologacionesHttpService } from 'src/http-services/homologaciones.http-service';

@Component({
    selector: 'app-salida-estructurada-edicion',
    templateUrl: './salida-estructurada-edicion.component.html',
    styleUrls: ['./salida-estructurada-edicion.component.css']
})
export class EstructuradaSalidaEdicionComponent extends FormularioEstructurasPlantillaSalidaComponent
    implements OnInit {

    public set estructurasOutput(estructurasSalida: EstructuraAnetoPlantillaSalidaOutputModel[]) {
        this.estructuras = estructurasSalida.map(
            estrSalida => {
                let estr = new EstructuraAnetoSalida();
                estr.id = estrSalida.id;
                estr.codigo = estrSalida.codigo;
                estr.descripcion = estrSalida.descripcion;
                estr.columnas = estrSalida.columnas.map(
                    (colSalida: ColumnaEstructuraPlantillaSalidaOutputModel) => {
                        return this.colEstrucOutputToModel(colSalida);
                    }
                );
                return estr;
            }
        );
    }

    constructor(
        protected commonSvc: CommonDataHttpService,
        protected localSvc: PlantillasHttpService,
        protected formSvc: FormularioPlantillasService,
        protected estSvc: EstructurasHttpService,
        protected fb: FormBuilder,
        protected snackBar: MatSnackBar,
        protected dialog: MatDialog,
        protected homoSvc: HomologacionesHttpService,
        public icons: FontawesomeIconsService,
    ) {
        super(commonSvc, localSvc, formSvc, estSvc, fb, snackBar, dialog, homoSvc, icons);
    }

    private colEstrucOutputToModel(colSalida: ColumnaEstructuraPlantillaSalidaOutputModel): ColumnaEstructuraAnetoSalida {
        let col = new ColumnaEstructuraAnetoSalida();
        col.id = colSalida.idDetalleEstructura;
        col.codigo = colSalida.codigoColumna;
        col.descripcion = colSalida.descripcionColumna;
        
        if (colSalida.valorFijo) {
            col.tipoDato = 1;
            col.valorFijo = colSalida.valorFijo;
        } else if (colSalida.idAtributoDiccionario) {
            col.tipoDato = 2;
            col.atributoDiccionario = colSalida.idAtributoDiccionario;
        }

        if (colSalida.formula) {
            const formula: Formulas.FormulaFactor = colSalida.formula;
            col.formula = { 
                orden: colSalida.ordenFormula, 
                valor: formula 
            };
        }
        if (colSalida.idTipoFormato && colSalida.ordenFormato) {
            col.formato = { 
                orden: colSalida.ordenFormato, 
                valor: colSalida.idTipoFormato 
            };
        }
        if (colSalida.idTipoHomologacion && colSalida.ordenHomologacion) {
            col.homologacion = { 
                orden: colSalida.ordenHomologacion, 
                valor: colSalida.idTipoHomologacion 
            };
        }

        return col;
    }

    ngOnInit(): void {
        this.cargandoEstructura = true;
        super.ngOnInit();
    }

}
