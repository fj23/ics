import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { MatSnackBar, DateAdapter, MAT_DATE_LOCALE } from "@angular/material";
import { Observable, BehaviorSubject, Subject } from 'rxjs';
import { ArchivosHttpService } from "../../../http-services/archivos.http-service";
import { CargaArchivoOutputModel } from "src/output_models/CargaArchivoOutputModel";
import { RequestResult } from "src/models/compartido/RequestResult";
import { FormCanDeactivate } from "../../../services/routing/form-can-deactivate";
import { Title } from "@angular/platform-browser";
import { Socio } from "src/models/compartido/Socio";
import { CommonDataHttpService } from "src/http-services/common-data.http-service";
import { CustomDateAdapter } from "src/app/compartido/custom-date-adapter.class";

@Component({
  selector: "app-archivos-cargar",
  templateUrl: "./archivos-cargar.component.html",
  styleUrls: ["./archivos-cargar.component.css"],
  providers: [
    { provide: DateAdapter, useClass: CustomDateAdapter },
    { provide: MAT_DATE_LOCALE, useValue: "es-CL" }
  ]
})
export class ArchivosCargarComponent extends FormCanDeactivate
  implements OnInit {
  public archivosCargarForm: FormGroup;
  public nombresProceso = [];
  public socios$: Observable<Socio[]>;

  public errorSubida: Subject<boolean>;
  public errorSubidaDesc: string = null;
  public fileloading: Subject<boolean>;
  public notloading: Subject<boolean>;

  public fileToUpload: File = null;

  public fechaRec = new Date();

  constructor(
    private fb: FormBuilder,
    private snackBar: MatSnackBar,
    private title: Title,
    private localSvc: ArchivosHttpService,
    private commonSvc: CommonDataHttpService
  ) {
    super();
    this.title.setTitle("ICS | Carga Archivo");

    this.errorSubida = new BehaviorSubject(false);
    this.fileloading = new BehaviorSubject(false);
    this.notloading = new BehaviorSubject(true);

    this.archivosCargarForm = this.fb.group({
      socio: null,
      nombreProceso: [null, Validators.required],
      fechaRecepcion: [null, Validators.required],
      observaciones: [null],
      codigoWebSocios: [null]
    });
  }

  public get nombreSocio() { return this.archivosCargarForm.get("socio"); }
  public get nombreProceso() { return this.archivosCargarForm.get("nombreProceso"); }
  public get fechaRecepcion() { return this.archivosCargarForm.get("fechaRecepcion"); }
  public get observaciones() { return this.archivosCargarForm.get("observaciones"); }
  public get codigoWebSocios() { return this.archivosCargarForm.get("codigoWebSocios"); }

  public get canDeactivateForm(): boolean {
    return !this.archivosCargarForm.touched;
  }

  ngOnInit() {
    this.socios$ = this.commonSvc.getSocios();
    this.commonSvc.getProcesos().subscribe(resp => {
      this.nombresProceso = resp;
    });

    this.archivosCargarForm.controls.socio.valueChanges.subscribe(value => {
      this.archivosCargarForm.controls.nombreProceso.setValue(null);
      this.commonSvc.getProcesos(value).subscribe(resp => {
        this.nombresProceso = resp;
      });
    });
  }


  public onFileChange(files: FileList) {
    let fileElement = files.item(files.length - 1);
    this.fileToUpload = fileElement;
    console.log(this.archivosCargarForm);
    
  }

  loading(status: boolean) {
    if (status) {
      this.fileloading.next(true);
      this.notloading.next(false);
    } else {
      this.fileloading.next(false);
      this.notloading.next(true);
    }
  }

  submitConfirmar() {
    if (this.archivosCargarForm.valid) {
      this.loading(true);
      this.errorSubida.next(false);

      if (!!!this.fileToUpload) {
        this.errorSubidaDesc = "Debe seleccionar un archivo.";
        this.errorSubida.next(true);
        this.loading(false);
      } else {
        let nombreArchivo: string = this.fileToUpload.name;

        if (nombreArchivo.indexOf(".") === -1) {
          this.errorSubidaDesc = "No se encontró la extensión del archivo.";
          this.errorSubida.next(true);
          this.loading(false);
        } else if (this.nombreProceso.value == null) {
          this.errorSubidaDesc = "Debe seleccionar un proceso de carga.";
          this.errorSubida.next(true);
          this.loading(false);
        } else {
          const fecha: Date = this.fechaRecepcion.value;

          if (
            fecha &&
            fecha.getFullYear() < 2018 &&
            fecha.toLocaleDateString() === fecha.toString()
          ) {
            this.errorSubidaDesc = "Fecha inválida.";
            this.errorSubida.next(true);
            this.loading(false);
          } else {
            let posExtension = nombreArchivo.lastIndexOf(".") + 1;
            let extension = nombreArchivo.substr(
              posExtension,
              nombreArchivo.length - posExtension
            );
            const extensionLC = extension.toLowerCase();

            if (
              extensionLC !== "csv" &&
              extensionLC !== "xlsx" &&
              extensionLC !== "xls" &&
              extensionLC !== "txt"
            ) {
              this.errorSubidaDesc =
                "No se puede subir este tipo de archivo (" + extension + ")";
              this.errorSubida.next(true);
              this.loading(false);
            } else {
              let cargaArchivo: CargaArchivoOutputModel = new CargaArchivoOutputModel();
              cargaArchivo.nombreArchivo = nombreArchivo;
              cargaArchivo.codigo = this.codigoWebSocios.value;
              cargaArchivo.idProceso = this.nombreProceso.value;
              for (let proceso of this.nombresProceso) {
                if (proceso.idProceso == this.nombreProceso.value) {
                  cargaArchivo.nombre = proceso.nombreProceso;
                  break;
                }
              }
              cargaArchivo.fechaRecepcion = fecha;
              cargaArchivo.observacion = this.observaciones.value;
              cargaArchivo.extensionArchivo = extension;

              this.localSvc
                .verifyFileName(cargaArchivo)
                .subscribe((result: RequestResult) => {
                  if (result.code !== 0) {
                    this.cargarArchivo(cargaArchivo);
                  } else {
                    if (result.message) {
                      this.errorSubidaDesc = result.message;
                    } else {
                      this.errorSubidaDesc = "Error desconocido.";
                    }
                    this.errorSubida.next(true);
                    this.loading(false);
                  }
                });
            }
          }
        }
      }
    }
  }

  private cargarArchivo(cargaArchivo: CargaArchivoOutputModel) {
    this.localSvc
      .setFileCargaArchivo(this.fileToUpload)
      .subscribe((cargaReturned: CargaArchivoOutputModel) => {
        if (!cargaReturned.id) {
          this.errorSubidaDesc =
            "No se pudo subir el archivo, intente nuevamente.";
          this.errorSubida.next(true);
          this.loading(false);
        } else {
          cargaArchivo.id = cargaReturned.id;
          this.localSvc.setCargaArchivo(cargaArchivo).subscribe(() => {
            this.snackBar.open("Operación finalizada con éxito.");
            this.fileloading.next(false);
            this.loading(false);
            this.fileToUpload = null;
            (<HTMLFormElement>(
              document.getElementById("archivosCargarForm")
            )).reset();
          });
        }
      });
  }
}
