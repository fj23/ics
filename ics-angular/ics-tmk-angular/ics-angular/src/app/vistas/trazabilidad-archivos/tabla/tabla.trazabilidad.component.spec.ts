import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TablaTrazabilidadArchivosComponent } from './tabla.trazabilidad.component';

describe('LoginComponent', () => {
  let component: TablaTrazabilidadArchivosComponent;
  let fixture: ComponentFixture<TablaTrazabilidadArchivosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TablaTrazabilidadArchivosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TablaTrazabilidadArchivosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
