import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrazabilidadArchivosComponent } from './trazabilidad.component';

describe('LoginComponent', () => {
  let component: TrazabilidadArchivosComponent;
  let fixture: ComponentFixture<TrazabilidadArchivosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrazabilidadArchivosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrazabilidadArchivosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
