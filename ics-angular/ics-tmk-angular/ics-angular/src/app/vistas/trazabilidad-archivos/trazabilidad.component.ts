import { Component } from "@angular/core";
import { Title } from "@angular/platform-browser";
import { TrazabilidadArchivosHttpService } from 'src/http-services/trazabilidad-archivos.http-service';

@Component({
  selector: "app-trazabilidad",
  templateUrl: "./trazabilidad.component.html",
  styleUrls: ["./trazabilidad.component.css"],
  providers: [TrazabilidadArchivosHttpService]
})
export class TrazabilidadArchivosComponent {
  constructor(private title: Title, private localSvc: TrazabilidadArchivosHttpService) {
    this.title.setTitle("ICS | Trazabilidad de Archivos");
  }
}
