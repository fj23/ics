import {
  Component,
  OnInit
} from "@angular/core";
import { Observable } from "rxjs";


import { FiltrosTrazabilidadArchivosModel } from "src/models/filters/FiltrosTrazabilidadArchivosModel";
import { Socio } from "src/models/compartido/Socio";
import { Evento } from "src/models/compartido/Evento";

import {
  FormGroup,
  FormControl,
  Validators,
  FormBuilder
} from "@angular/forms";

import { MAT_DATE_LOCALE, DateAdapter } from "@angular/material/core";
import { CustomDateAdapter } from "src/app/compartido/custom-date-adapter.class";
import { Angular5Csv } from "angular5-csv/dist/Angular5-csv";
import { Parametro } from "src/models/compartido/Parametro";
import { InformationDialogComponent } from "src/app/dialogs/information/information.dialog.component";
import { MatDialog } from "@angular/material";
import { TrazabilidadArchivosHttpService } from '../../../../http-services/trazabilidad-archivos.http-service';
import { CommonDataHttpService } from '../../../../http-services/common-data.http-service';

@Component({
  selector: "app-filtros-trazabilidad",
  templateUrl: "./filtros.trazabilidad.component.html",
  styleUrls: ["./filtros.trazabilidad.component.css"],
  providers: [
    { provide: DateAdapter, useClass: CustomDateAdapter },
    { provide: MAT_DATE_LOCALE, useValue: "es-CL" }
  ]
})
export class FiltrosTrazabilidadArchivosComponent implements OnInit {
  filtrosForm: FormGroup;

  socios$: Observable<Socio[]>;
  eventos$: Observable<Evento[]>;
  estados$: Observable<Parametro[]>;

  datasource = [];

  constructor(
    private localSvc: TrazabilidadArchivosHttpService,
    private commonSvc: CommonDataHttpService,
    private formBuilder: FormBuilder,
    public dialog: MatDialog
  ) {}

  ngOnInit() {
    let filtros = new FiltrosTrazabilidadArchivosModel();
    var date = new Date();
    date.setDate(date.getDate() - 7);

    this.filtrosForm = this.formBuilder.group({
      socio: null,
      evento: null,
      estado: null,
      idCarga: new FormControl(null, Validators.pattern("[0-9]*")),
      idExterno: null,
      dpRecepionadoDesde: date,
      dpRecepionadoHasta: null,
      dpCargadoDesde: date,
      dpCargadoHasta: null
    });

    this.estados$ = this.commonSvc.getParametrosTrazabilidad();
    this.socios$ = this.commonSvc.getSocios();
    this.eventos$ = this.commonSvc.getEventos();
    
    filtros.recepcionadoDesde = date;
    filtros.cargadoDesde = date;

    let settedFiltros: FiltrosTrazabilidadArchivosModel = this.localSvc.getFiltros();
    filtros.pageSize = settedFiltros.pageSize;
    filtros.sortColumn = settedFiltros.sortColumn;
    filtros.sortOrder = settedFiltros.sortOrder;

    this.localSvc.setFiltros(filtros);
  }

  resetDate(controlName: string) {
    this.filtrosForm.controls[controlName].setValue(null);
  }

  submit() {
    if (this.filtrosForm.status == "VALID") {
      let filtros = new FiltrosTrazabilidadArchivosModel();
      filtros.socio = this.filtrosForm.controls.socio.value;
      filtros.evento = this.filtrosForm.controls.evento.value;
      filtros.estado = this.filtrosForm.controls.estado.value;
      filtros.idCarga = parseInt(this.filtrosForm.controls.idCarga.value);
      filtros.idExterno = this.filtrosForm.controls.idExterno.value;
      filtros.recepcionadoDesde = this.filtrosForm.controls.dpRecepionadoDesde.value;
      filtros.recepcionadoHasta = this.filtrosForm.controls.dpRecepionadoHasta.value;
      filtros.cargadoDesde = this.filtrosForm.controls.dpCargadoDesde.value;
      filtros.cargadoHasta = this.filtrosForm.controls.dpCargadoHasta.value;

      let settedFiltros: FiltrosTrazabilidadArchivosModel = this.localSvc.getFiltros();
      filtros.pageSize = settedFiltros.pageSize;
      filtros.sortColumn = settedFiltros.sortColumn;
      filtros.sortOrder = settedFiltros.sortOrder;

      this.localSvc.setFiltros(filtros);
    }
  }

  exportLoading: boolean = false;
  onClickExportar() {
    let options = {
      fieldSeparator: ";",
      quoteStrings: '"',
      decimalseparator: ",",
      showLabels: true,
      showTitle: false,
      useBom: true,
      noDownload: false,
      title: "",
      headers: [
        "Transacción Id",
        "Proceso Id",
        "Socio",
        "Evento",
        "Estado",
        "Fecha Recepción",
        "Fecha Fin Transacción",
        "N° Registros Originales",
        "N° Registros Cargados en ICS",
        "N° Registros Cargados en Core",
        "N° Registros Rechazados en ICS",
        "N° Registros Rechazados en Core"
      ],
      nullToEmptyString: true
    };

    let data = [];
    this.exportLoading = true;
    this.localSvc.exportar().subscribe(resp => {
      for (let t of resp) {
        data.push([
          t.id,
          t.idProceso,
          t.nombreSocio,
          t.nombreEvento,
          t.estadoTransaccion,
          this.formatDate(t.fechaInicio),
          this.formatDate(t.fechaFin),
          t.lineasOriginal,
          t.lineasIcsOk,
          t.lineasCoreOk,
          t.lineasIcsError,
          t.lineasCoreError
        ]);
      }
      this.exportLoading = false;
      return new Angular5Csv(data, "Transacciones", options);
    });
  }

  consolidar() {
    this.localSvc.consolidar().subscribe(
      resp => {
        this.dialog.open(InformationDialogComponent, {
          width: "475px",
          data: {
            title: "Consulta Consolidación",
            content: "Última Fecha Consolidación: " + new Date(resp.fechaEjecucion).toLocaleString() + "<br>Observación: " + resp.observacion
          }
        });
      },
      err => {
        console.error(err);
        try {
          this.dialog.open(InformationDialogComponent, {
            width: "475px",
            data: {
              title: "Error",
              content: err.error.message
            }
          });
        } catch (error) {
          this.dialog.open(InformationDialogComponent, {
            width: "475px",
            data: {
              title: "Error",
              content: err.statusText
            }
          });
        }
      }
    );
  }

  private formatDate(time: number): string {
    if (time == null) {
      return "";
    } else {
      var date = new Date(time)
      return date.getDate() + "/" + date.getMonth() +
        "/" +
        date.getFullYear() +
        " " +
        date.getHours() +
        ":" +
        date.getMinutes() +
        ":" +
        date.getSeconds()
      ;
    }
  }
}
