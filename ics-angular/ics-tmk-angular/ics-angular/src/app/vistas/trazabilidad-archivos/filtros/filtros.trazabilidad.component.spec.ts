import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FiltrosTrazabilidadArchivosComponent } from './filtros.trazabilidad.component';

describe('LoginComponent', () => {
  let component: FiltrosTrazabilidadArchivosComponent;
  let fixture: ComponentFixture<FiltrosTrazabilidadArchivosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FiltrosTrazabilidadArchivosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FiltrosTrazabilidadArchivosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
