import { Component, ViewChild, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup, ValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';
import { MatTable, MatSnackBar, MatDialog } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, Subscription, of, BehaviorSubject, merge, Subject } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { EstructuraAneto } from 'src/models/estructuras/EstructuraAneto';
import { SubEstructuraAneto } from 'src/models/estructuras/SubEstructuraAneto';
import { TipoDato } from 'src/models/compartido/TipoDato';
import { ConfirmationDialogComponent } from  '../../dialogs/confirmation/confirmation.dialog.component';
import { FontawesomeIconsService } from 'src/services/fontawesome-icons/fontawesome-icons.service';
import { Title } from '@angular/platform-browser';
import { FormCanDeactivate } from 'src/services/routing/form-can-deactivate';
import { ColumnaMetadatos } from 'src/models/compartido/ColumnaMetadatos';
import { CommonDataHttpService } from 'src/http-services/common-data.http-service';
import { EstructurasHttpService } from 'src/http-services/estructuras.http-service';
import { SubEstructurasHttpService } from 'src/http-services/subestructuras.http-service';

const TIPO_DATO_SUBESTRUCTURA_ID = 100005;

@Component({
    selector: 'app-formulario-estructuras',
    templateUrl: './estructuras-crear.component.html',
    styleUrls: ['./estructuras-crear.component.css']
})
export class FormularioEstructurasComponent extends FormCanDeactivate implements OnInit {

    protected _tiposDato: TipoDato[];
    protected _estructuraColumnas: ColumnaMetadatos[];
    protected columnaTipoDatoChangeSub: Subscription;
    protected _idEstructura: number;
    protected _modoVisualizar: boolean;
    protected _esSubestructura: boolean;

    protected _subEstructuras: SubEstructuraAneto[];

    protected _estructuraColumnas$: Subject<ColumnaMetadatos[]>;
    protected _loading$: Subject<boolean>;
    protected _titulo$: Subject<string>;
    protected _enviando$: Subject<boolean>;

    public tiposDato$: Observable<TipoDato[]>;
    public subEstructuras$: Observable<SubEstructuraAneto[]>;
    public loading$: Observable<boolean>;
    public enviando$: Observable<boolean>;
    public titulo$: Observable<string>;

    public get idEstructura(): number { return this._idEstructura; }
    public get modoVer(): boolean { return this._modoVisualizar; }
    public get esSubestructura(): boolean { return this._esSubestructura; }

    public creacionEstructuraForm: FormGroup;
    public creacionColumnaForm: FormGroup;

    @ViewChild("tablaColumnas") public estructuraColumnasTable: MatTable<ColumnaMetadatos>;
    public displayedColumns: string[] = ['codigo', 'descripcion', 'tipo', 'largo', 'acciones'];
    public estructuraColumnas$: Observable<ColumnaMetadatos[]>;
    public get estructuraColumnas(): ColumnaMetadatos[] { return this._estructuraColumnas; }

    constructor(
        public router: Router,
        protected route: ActivatedRoute,
        protected commonSvc: CommonDataHttpService,
        protected estSvc: EstructurasHttpService,
        protected subestSvc: SubEstructurasHttpService,
        protected fb: FormBuilder,
        protected dialog: MatDialog,
        protected snackBar: MatSnackBar,
        protected title: Title,
        public icons: FontawesomeIconsService
    ) {
        super();
        this.title.setTitle("ICS | Nueva Estructura");

        this._estructuraColumnas = [];

        this._loading$ = new BehaviorSubject(false);
        this.loading$ = this._loading$.asObservable();

        this._loading$ = new BehaviorSubject(false);
        this.loading$ = this._loading$.asObservable();

        this._enviando$ = new BehaviorSubject(false);
        this.enviando$ = this._enviando$.asObservable();

        this._idEstructura = NaN;
        this._modoVisualizar = false;
        this._esSubestructura = false;

        this.creacionEstructuraForm = this.fb.group({
            codigo: ['', Validators.required],
            descripcion: ['', Validators.required],
            cardMin: ['', Validators.compose([Validators.required, Validators.min(0)])],
            cardMax: ['', Validators.required],
            esSubEstructura: [false]
        });
        
        this.creacionColumnaForm = this.fb.group({
            codigo: ['', Validators.required],
            descripcion: ['', Validators.required],
            largo: ['', Validators.required],
            tipoDato: [null, Validators.required],
            subEstructura: [{ value: null, disabled: true }]
        });

        this._estructuraColumnas$ = new Subject();
        this.estructuraColumnas$ = this._estructuraColumnas$.asObservable();
    }

    public get codigo() { return this.creacionEstructuraForm.get("codigo"); }
    public get descripcion() { return this.creacionEstructuraForm.get("descripcion"); }
    public get cardMin() { return this.creacionEstructuraForm.get("cardMin"); }
    public get cardMax() { return this.creacionEstructuraForm.get("cardMax"); }
    public get esSubEstructuraCheckbox() { return this.creacionEstructuraForm.get("esSubEstructura"); }
    
    public get columnaCodigo() { return this.creacionColumnaForm.get("codigo"); }
    public get columnaDescripcion() { return this.creacionColumnaForm.get("descripcion"); }
    public get columnaLargo() { return this.creacionColumnaForm.get("largo"); }
    public get columnaTipoDato() { return this.creacionColumnaForm.get("tipoDato"); }
    public get columnaSubEstructura() { return this.creacionColumnaForm.get("subEstructura"); }

    public get canDeactivateForm(): boolean {
        return this.creacionEstructuraForm.pristine;
    }

    ngOnInit(): void {
        this.commonSvc.getSubEstructuras().subscribe(
            (subestructuras: SubEstructuraAneto[]) => {
                this._subEstructuras = subestructuras;
                this.subEstructuras$ = of(subestructuras);
                if (this._estructuraColumnas && this._estructuraColumnas.length) {
                    this.cargarSubestructurasEnColumnas();
                }
            }
        );
        this.commonSvc.getTiposDato().subscribe(
            (tiposDato: TipoDato[]) => {
                this._tiposDato = tiposDato;
                this.tiposDato$ = of(tiposDato);
            }
        );

        if (this.route.snapshot.url.length > 1) {
            this.procesarURL();
            this.cargarEstructura();
        }
        
        this.esSubEstructuraCheckbox.valueChanges.subscribe( () => { this.actualizarFormularioSegunCasillaSubestructura(); } );

        //activa el dropdown de subestructuras en la creacion de columna
        //sólo si se elige ese tipo de dato y no se está creando una subestructura
        this.columnaTipoDatoChangeSub = this.columnaTipoDato.valueChanges.subscribe( () => { this.onChangeColumnaTipoDato(); } );
    }

    protected cargarSubestructurasEnColumnas() {
        this._estructuraColumnas = this._estructuraColumnas.map((col: ColumnaMetadatos) => {
            if (col.idSubEstructura) {
                const subest = this._subEstructuras.find(s => s.id === col.idSubEstructura);
                if (subest) {
                    col.subEstructura = subest;
                }
            }
            return col;
        });
    }

    protected alCambiarModoVisualizar() {
        const noChange = { onlySelf: true, emitEvent: false };
        if (this._modoVisualizar) {
            this.codigo.disable(noChange);
            this.descripcion.disable(noChange);
            this.cardMin.disable(noChange);
            this.cardMax.disable(noChange);
        } else {
            this.codigo.enable(noChange);
            this.descripcion.enable(noChange);
            this.cardMin.enable(noChange);
            this.cardMax.enable(noChange);
        }
    }

    private procesarURL() {
        const routeURL = this.route.snapshot.url;
        
        const accionEsVisualizar = (routeURL[1].toString() === "ver");
        this._modoVisualizar = accionEsVisualizar;
        this.alCambiarModoVisualizar();

        if (routeURL.length > 2) {
            const tipoEsSubestructura = (routeURL[2].toString() === "subest");
            this._esSubestructura = tipoEsSubestructura;

            if (routeURL.length > 3) {
                const idEstructura = Number(routeURL[3].toString());
                if (!isNaN(idEstructura)) {
                    this._idEstructura = idEstructura;
                }
            } else {
                this._idEstructura = NaN;   
            }
        }
    }

    /**
     * Método llamado cuando se ha cargado la estructura a editar
     * @param input La subestructura a editar
     */
    protected actualizarFormularioPorEstructura(input: EstructuraAneto) { 
        this.codigo.setValue(input.codigo);
        this.descripcion.setValue(input.descripcion);
        this._estructuraColumnas = input.columnas;
        if (this._subEstructuras) { this.cargarSubestructurasEnColumnas(); }
        this._estructuraColumnas$.next(this._estructuraColumnas);

        const tieneCardMin = ('cardMin' in input);
        const tieneCardMax = ('cardMax' in input);
        if (!this._esSubestructura) {
            if (tieneCardMin && tieneCardMax) {
                const est = <EstructuraAneto>input;
                this.cardMin.setValue(est.cardMin);
                this.cardMax.setValue(est.cardMax);
                this.esSubEstructuraCheckbox.setValue(false);
            } else {
                throw new Error("Se solicitó una estructura pero se obtuvo una subestructura");
            }
        } else if (tieneCardMin || tieneCardMax) {
            throw new Error("Se solicitó una subestructura pero se obtuvo una estructura");
        } else {
            this.esSubEstructuraCheckbox.setValue(true);
        }
    }

    /**
     * Limpia el formulario para evitar que salte el pop-up de confirmación
     */
    protected limpiarFormulario(): void {
        this.creacionEstructuraForm.markAsPristine();
        this.creacionColumnaForm.markAsPristine();
        this._estructuraColumnas = [];
        this._estructuraColumnas$.next(this._estructuraColumnas);
    }

    private cargarEstructura() { 
        if (!isNaN(this._idEstructura)) {
            this._loading$.next(true);

            let obs: Observable<any>;
            if (this._esSubestructura) {
                obs = this.subestSvc.buscar(this._idEstructura);
            } else {
                obs = this.estSvc.buscar(this._idEstructura);
            }

            obs.subscribe((estructura: EstructuraAneto) => {
                this._loading$.next(false);
                this.actualizarFormularioPorEstructura(estructura);
            });

        } else {
            this.limpiarFormulario();
        }
    }

    protected actualizarFormularioSegunCasillaSubestructura() {
        if (this.esSubEstructuraCheckbox.value) {
            if (this._estructuraColumnas.some((columna) => columna.tipoDato.id === TIPO_DATO_SUBESTRUCTURA_ID)) {
                this.snackBar.open("Quite todas las columnas de tipo 'Sub-Estructura' primero.");
                this.esSubEstructuraCheckbox.setValue(false, {emitEvent: false, onlySelf: true});
            } else {
                const tiposSinSubestructura = this._tiposDato.filter(tipo => (tipo.id !== TIPO_DATO_SUBESTRUCTURA_ID))
                this.tiposDato$ = of(tiposSinSubestructura);

                this.cardMin.disable();
                this.cardMax.disable();
                this.columnaSubEstructura.disable();

                this.cardMin.reset();
                this.cardMax.reset();
                this.columnaSubEstructura.reset();

                if (this.columnaTipoDato.value && this.columnaTipoDato.value.id === TIPO_DATO_SUBESTRUCTURA_ID) {
                    this.columnaTipoDato.reset();
                }
            }
        } else {
            this.tiposDato$ = of(this._tiposDato);
            this.cardMin.enable();
            this.cardMax.enable();
        }
    }

    protected onChangeColumnaTipoDato() {
        if (this.columnaTipoDato.value && this.columnaTipoDato.value.id === TIPO_DATO_SUBESTRUCTURA_ID && !this.esSubEstructuraCheckbox.value) {
            this.columnaSubEstructura.enable();
        } else {
            this.columnaSubEstructura.setValue(null);
            this.columnaSubEstructura.disable();
        }
    }

    /**
     * Con los datos del formulario en el recuadro 'nueva columna', crea un objeto modelo.
     */
    protected buildColumna(): ColumnaMetadatos {
        let model: ColumnaMetadatos = new ColumnaMetadatos();

        model.codigo = this.columnaCodigo.value;
        model.descripcion = this.columnaDescripcion.value;
        model.orden = this._estructuraColumnas.length + 1;
        model.vigencia = "Y";
        model.tipoDato = this.columnaTipoDato.value;
        model.largo = this.columnaLargo.value;

        if (Number(model.tipoDato.id) === TIPO_DATO_SUBESTRUCTURA_ID) {
            model.subEstructura = this.columnaSubEstructura.value;
        }

        return model;
    }

    /**
     * Valida el formulario de columna, la crea y la agrega a la estructura actual.
     */
    public submitColumna(): void {
        this.creacionColumnaForm.updateValueAndValidity();
        if (this.creacionColumnaForm.invalid) {
            this.snackBar.open("Debe completar todos los datos en el formulario para agregar una columna.");
        } else {
            const minNombreLength = 3;
            const minCodigoLength = 3;

            if (this.columnaCodigo.value.length < minCodigoLength) {
                this.snackBar.open("El código debe poseer al menos " + minCodigoLength + " caracteres.");
            } else if (this.columnaDescripcion.value.length < minNombreLength) {
                this.snackBar.open("La descripción debe poseer al menos " + minNombreLength + " caracteres.");
            } else if (this.columnaTipoDato.value.id === TIPO_DATO_SUBESTRUCTURA_ID && this.columnaSubEstructura.value === null) {
                this.snackBar.open("Debe elegir una subestructura.");
            } else if (this.columnaLargo.value < 1) {
                this.snackBar.open("La columna no puede tener largo 0 o menor.");
            } else {
                let columnaModel: ColumnaMetadatos = this.buildColumna();

                if (this._estructuraColumnas.some((columna) => columna.codigo === columnaModel.codigo)) {
                    this.snackBar.open("Ya hay una columna con este código.");
                } else if (this._estructuraColumnas.some((columna) => columna.descripcion === columnaModel.descripcion)) {
                    this.snackBar.open("Ya hay una columna con este descripcion.");
                } else {
                    this._estructuraColumnas.push(columnaModel);
                    this._estructuraColumnas$.next(this._estructuraColumnas);
                    this.creacionColumnaForm.reset();
                }
            }
        }
    }

    protected updateColumnasOrden() {
        for (let i = 0; i < this._estructuraColumnas.length; i++) {
            let columa = this._estructuraColumnas[i];
            columa.orden = i + 1;
        }
        this._estructuraColumnas$.next(this._estructuraColumnas);
    }

    /**
     * Desplaza la columna (obtenida por su índice) un puesto más arriba (antes).
     * @param index El índice de la columna a subir.
     */
    public onClickSubirColumna(index: number): void {
        if (index > 0) {
            const estaColumna: ColumnaMetadatos = this._estructuraColumnas[index];
            const columnaSuperior: ColumnaMetadatos = this._estructuraColumnas[index - 1];
            this._estructuraColumnas[index - 1] = estaColumna;
            this._estructuraColumnas[index] = columnaSuperior;
            this.updateColumnasOrden();
        }
    }

    /**
     * Desplaza la columna (obtenida por su índice) un puesto más abajo (después).
     * @param index El índice de la columna a bajar.
     */
    public onClickBajarColumna(index: number): void {
        if (index < this._estructuraColumnas.length - 1) {
            const estaColumna: ColumnaMetadatos = this._estructuraColumnas[index];
            const columnaInferior: ColumnaMetadatos = this._estructuraColumnas[index + 1];
            this._estructuraColumnas[index + 1] = estaColumna;
            this._estructuraColumnas[index] = columnaInferior;
            this.updateColumnasOrden();
        }
    }

    /**
     * Retira la columna (obtenida por su índice).
     * @param index El índice de la columna a quitar.
     */
    public onClickQuitarColumna(index: number): void {
        this._estructuraColumnas.splice(index, 1);
        this.updateColumnasOrden();
    }

    /**
     * Habilita o deshabilita la columna según corresponda, cuando se hace clic en el botón pertinente.
     * Este método sólo se llama una vez que la estructura ya existe.
     */
    public onClickAlternarColumna(index: number): void {
        if (index <= this._estructuraColumnas.length - 1) {
            if (this._estructuraColumnas[index].vigencia === "Y") {
                this._estructuraColumnas[index].vigencia = "N";
            }
            else if (this._estructuraColumnas[index].vigencia === "N") {
                this._estructuraColumnas[index].vigencia = "Y";
            }
            this._estructuraColumnas$.next(this._estructuraColumnas);
        }
    }


    /**
     * Envía los datos del formulario como un objeto.
     */
    onClickSubmit(): void {
        this.creacionEstructuraForm.updateValueAndValidity();

        if (this.creacionEstructuraForm.invalid) {
            this.snackBar.open("Debe completar todos los datos en el formulario para crear una estructura.");
        } else if (this._estructuraColumnas.length === 0) {
            this.snackBar.open("La estructura debe tener al menos una columna definida.");
        } else {

            const minNombreLength = 3;
            const minCodigoLength = 3;
            
            if (this.esSubEstructuraCheckbox.value) {
                if (this.codigo.value.length < minCodigoLength) {
                    this.snackBar.open("El código de la subestructura debe poseer al menos " + minCodigoLength + " caracteres.");
                } else if (this.descripcion.value.length < minNombreLength) {
                    this.snackBar.open("La descripción de la subestructura debe poseer al menos " + minNombreLength + " caracteres.");
                } else {
                    let subestructura: SubEstructuraAneto = new SubEstructuraAneto();
                    subestructura.codigo = this.codigo.value;
                    subestructura.descripcion = this.descripcion.value;
                    subestructura.columnas = this._estructuraColumnas;
    
                    if (this._idEstructura) {
                        subestructura.id = this._idEstructura;
                        this._enviando$.next(true);
                        this.subestSvc.actualizar(subestructura).subscribe(
                            () => {
                                this.snackBar.open("Subestructura actualizada con éxito");
                                this.saveFormDataForFiltros();
                                this.limpiarFormulario();

                                this.router.navigate(["/estructuras"]);
                            },
                            error => {
                                this.snackBar.open("Ha ocurrido un error");
                            },
                            () => { this._enviando$.next(false); }
                        );
                    } else {
                        this.subestSvc.existe(subestructura).subscribe(
                            (subEstrExiste) => {

                                if (subEstrExiste) {
                                    this.snackBar.open("Ya existe una subestructura con este código o descripción.");
                                } else {
                                    this._enviando$.next(true);
                                    this.subestSvc.crear(subestructura).subscribe(
                                        () => {
                                            this.snackBar.open("Subestructura creada con éxito");
                                            this.saveFormDataForFiltros();
                                            this.limpiarFormulario();
            
                                            this.router.navigate(["/estructuras"]);
                                        },
                                        error => {
                                            this.snackBar.open("Ha ocurrido un error");
                                        },
                                        () => { this._enviando$.next(false); }
                                    );
                                }
                            }
                        );
                    }
                }
            } else {
                if (this.codigo.value.length < minCodigoLength) {
                    this.snackBar.open("El código de la subestructura debe poseer al menos " + minCodigoLength + " caracteres.");
                } else if (this.descripcion.value.length < minNombreLength) {
                    this.snackBar.open("La descripción de la subestructura debe poseer al menos " + minNombreLength + " caracteres.");
                } else if (Number(this.cardMin.value) > Number(this.cardMax.value)) {
                    this.snackBar.open("La cardinalidad mínima no puede exceder la cardinalidad máxima.");
                } else {
                    let estructura: EstructuraAneto = new EstructuraAneto();
                    estructura.codigo = this.codigo.value;
                    estructura.descripcion = this.descripcion.value;
                    estructura.cardMin = this.cardMin.value;
                    estructura.cardMax = this.cardMax.value;
                    estructura.columnas = this._estructuraColumnas;

                    if (this._idEstructura) {
                        estructura.id = this._idEstructura;
                        this._enviando$.next(true);
                        this.estSvc.actualizar(estructura).subscribe(
                            () => {
                                this.snackBar.open("Estructura actualizada con éxito");
                                this.saveFormDataForFiltros();
                                this.limpiarFormulario();

                                this.router.navigate(["/estructuras"]);
                            },
                            error => {
                                this.snackBar.open("Ha ocurrido un error");
                            },
                            () => { this._enviando$.next(false); }
                        );
                    } else {
                        this.estSvc.existe(estructura).subscribe(
                            (estrExiste) => {

                                if (estrExiste) {
                                    this.snackBar.open("Ya existe una estructura con este código o descripción.");
                                } else {
                                    this._enviando$.next(true);
                                    this.estSvc.crear(estructura).subscribe(
                                        () => {
                                            this.snackBar.open("Estructura creada con éxito");
                                            this.saveFormDataForFiltros();
                                            this.limpiarFormulario();

                                            this.router.navigate(["/estructuras"]);
                                        },
                                        error => {
                                            this.snackBar.open("Ha ocurrido un error");
                                        },
                                        () => { this._enviando$.next(false); }
                                    );
                                }
                            }
                        );
                    }
                }
            }
        }
    }

    saveFormDataForFiltros() {
        sessionStorage.setItem("filtros.estructuras.codigo", this.codigo.value);
        sessionStorage.setItem("filtros.estructuras.nombre", this.descripcion.value);
    }

    dialogoConfirmacionSalirFormulario(): Observable<any> {
        const confirmationDialog = this.dialog.open(
            ConfirmationDialogComponent,
            {
                data: {
                    titulo: "Volver al Listado de Estructuras",
                    mensaje: "Si hace clic en \"Sí\", los datos del formulario para crear una estructura que está llenando serán descartados. ¿Está seguro que desea continuar?",
                    boton_si_clases: "btn-warning",
                    boton_no_clases: ""
                }
            }
        );

        return confirmationDialog.beforeClosed();
    }

    onClickCancelar(): void {
        if (this.creacionEstructuraForm.touched || this._estructuraColumnas.length > 0) {
            this.dialogoConfirmacionSalirFormulario().subscribe(
                (confirmed: boolean) => {

                    if (confirmed) {
                        this.router.navigate(["/estructuras"]);
                    }
                }
            );
        }
        else {
            this.router.navigate(["/estructuras"]);
        }
    }

    onClickEditar(): void {
        if (this.router.url.indexOf('/estructuras/ver/est') >= 0) {
            this.router.navigate(["/estructuras/editar/est/"+this._idEstructura]);
        } else {
            this.router.navigate(["/estructuras/editar/subest/"+this._idEstructura]);
        }
    }

}
