import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormularioEstructurasComponent } from './estructuras-crear.component';

describe('FormularioComponent', () => {
  let component: FormularioEstructurasComponent;
  let fixture: ComponentFixture<FormularioEstructurasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormularioEstructurasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {FormularioEstructurasComponent
    fixture = TestBed.createComponent(FormularioEstructurasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
