import { Component, OnInit, OnDestroy } from "@angular/core";
import { Socio } from "src/models/compartido/Socio";
import { Observable, Subscription } from "rxjs";
import { FormGroup, FormBuilder } from "@angular/forms";
import { CommonDataHttpService } from "src/http-services/common-data.http-service";
import { FiltrosProcesosCargaModel } from "src/models/filters/FiltrosProcesosCargaModel";
import { Evento } from "src/models/compartido/Evento";
import { ProcesosCargaHttpService } from '../../../../http-services/procesos-carga.http-service';

@Component({
  selector: "app-filtros-procesos-carga",
  templateUrl: "./filtros-procesos-carga.component.html",
  styleUrls: ["./filtros-procesos-carga.component.css"]
})
export class FiltrosProcesosCargaComponent implements OnInit, OnDestroy {
  estadosVigencia = [
    { id: "Y", descripcion: "Vigente" },
    { id: "N", descripcion: "No Vigente" }
  ];

  socios$: Observable<Socio[]>;
  eventos$: Observable<Evento[]>;

  nombresProceso = [];

  formFiltros: FormGroup;

  newHomologacionSub: Subscription;

  constructor(
    private localSvc: ProcesosCargaHttpService,
    private commonSvc: CommonDataHttpService,
    private fb: FormBuilder
  ) {
    this.formFiltros = this.fb.group({
      socios: null,
      eventos: null,
      nombreProceso: null,
      estado: null
    });
  }

  ngOnInit(): void {
    this.socios$ = this.commonSvc.getSocios();
    this.eventos$ = this.commonSvc.getEventos();
    this.commonSvc.getProcesos().subscribe(resp => {
      this.nombresProceso = resp;
    });

    this.formFiltros.controls.socios.valueChanges.subscribe(value => {
      this.formFiltros.controls.nombreProceso.setValue(null);
      let evento = this.formFiltros.controls.eventos.value;
      this.commonSvc.getProcesos(value, evento).subscribe(resp => {
        this.nombresProceso = resp;
      });
    });

    this.formFiltros.controls.eventos.valueChanges.subscribe(value => {
      this.formFiltros.controls.nombreProceso.setValue(null);
      let socio = this.formFiltros.controls.socios.value;
      this.commonSvc.getProcesos(socio, value).subscribe(resp => {
        this.nombresProceso = resp;
      });
    });
  }

  ngOnDestroy(): void {}

  /**
   * Genera un nuevo objeto de filtros y lo manda al servicio.
   */
  submit(): void {
    let filtros: FiltrosProcesosCargaModel = new FiltrosProcesosCargaModel();

    filtros.socio = this.formFiltros.controls.socios.value;
    filtros.evento = this.formFiltros.controls.eventos.value;
    filtros.nombreProceso = this.formFiltros.controls.nombreProceso.value;
    filtros.estado = this.formFiltros.controls.estado.value;

    let settedFiltros: FiltrosProcesosCargaModel = this.localSvc.getFiltros();
    filtros.pageSize = settedFiltros.pageSize;
    filtros.sortColumn = settedFiltros.sortColumn;
    filtros.sortOrder = settedFiltros.sortOrder;

    this.localSvc.setFiltros(filtros);
  }

  confirmarFiltrarResultados() {}
}
