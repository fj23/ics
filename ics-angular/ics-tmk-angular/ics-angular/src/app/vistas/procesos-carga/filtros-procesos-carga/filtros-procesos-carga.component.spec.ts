import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FiltrosProcesosCargaComponent } from './filtros-procesos-carga.component';

describe('FiltrosProcesosCargaComponent', () => {
  let component: FiltrosProcesosCargaComponent;
  let fixture: ComponentFixture<FiltrosProcesosCargaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FiltrosProcesosCargaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FiltrosProcesosCargaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
