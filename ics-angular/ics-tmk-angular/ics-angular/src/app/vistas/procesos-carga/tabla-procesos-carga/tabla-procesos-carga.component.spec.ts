import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TablaProcesosCargaComponent } from './tabla-procesos-carga.component';

describe('TablaProcesosCargaComponent', () => {
  let component: TablaProcesosCargaComponent;
  let fixture: ComponentFixture<TablaProcesosCargaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TablaProcesosCargaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TablaProcesosCargaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
