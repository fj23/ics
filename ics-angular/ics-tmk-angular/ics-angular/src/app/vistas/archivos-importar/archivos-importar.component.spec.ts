import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArchivosImportarComponent } from './archivos-importar.component';

describe('ArchivosImportarComponent', () => {
  let component: ArchivosImportarComponent;
  let fixture: ComponentFixture<ArchivosImportarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArchivosImportarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArchivosImportarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
