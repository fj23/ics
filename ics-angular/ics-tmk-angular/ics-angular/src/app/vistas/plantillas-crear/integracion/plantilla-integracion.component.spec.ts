import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormularioPlantillaIntegracionComponent } from './plantilla-integracion.component';

describe('FormularioPlantillaIntegracionComponent', () => {
  let component: FormularioPlantillaIntegracionComponent;
  let fixture: ComponentFixture<FormularioPlantillaIntegracionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormularioPlantillaIntegracionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormularioPlantillaIntegracionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
