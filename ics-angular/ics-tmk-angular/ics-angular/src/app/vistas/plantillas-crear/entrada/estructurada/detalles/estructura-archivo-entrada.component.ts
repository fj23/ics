import { Component, Input, ViewChild } from '@angular/core';
import { MatTable, MatSnackBar, MatCheckboxChange, MatDialog } from '@angular/material';
import { EstructuraAneto } from 'src/models/estructuras/EstructuraAneto';
import { Evento } from 'src/models/compartido/Evento';
import { ColumnaMetadatos } from 'src/models/compartido/ColumnaMetadatos';
import { CommonDataHttpService } from 'src/http-services/common-data.http-service';
import { FormularioPlantillasService } from 'src/services/formulario-plantillas/formulario-plantillas.service';
import { SubEstructurasHttpService } from 'src/http-services/subestructuras.http-service';
import { PlantillasHttpService } from 'src/http-services/plantillas.http-service';
import { FontawesomeIconsService } from 'src/services/fontawesome-icons/fontawesome-icons.service';
import { SubEstructuraAneto } from 'src/models/estructuras/SubEstructuraAneto';
import { ColumnaDestinoEntrada } from 'src/models/plantillas/entrada/ColumnaDestinoEntrada';
import { MapeoEntrada } from 'src/models/plantillas/entrada/MapeoEntrada';
import { MapearSubEstructuraPlantillaEntradaDialogComponent } from 'src/app/dialogs/mapear-subestructura/subestructura.dialog.plantilla-entrada.component';


@Component({
    selector: 'app-estructura-archivo-entrada',
    templateUrl: './estructura-archivo-entrada.component.html',
    styleUrls: ['./estructura-archivo-entrada.component.css']
})
export class EstructuraArchivoEstructuradoPlantillaEntradaComponent {

    @Input() public estructura: EstructuraAneto;
    @Input() public evento: Evento;

    @ViewChild(MatTable) public estructuraColumnasTable: MatTable<ColumnaMetadatos>;
    estructuraColumnasDisplayedColumns: string[] = ["nombre", "tipoDato", "acciones"];

    public indiceColumnaSeleccionada: number;

    public columnaSubEstructuraSeleccionada: ColumnaMetadatos;

    public loading: boolean = false;

    constructor(
        protected commonSvc: CommonDataHttpService,
        protected formSvc: FormularioPlantillasService,
        protected subEstSvc: SubEstructurasHttpService,
        protected localSvc: PlantillasHttpService,
        protected snackBar: MatSnackBar,
        protected dialog: MatDialog,
        public icons: FontawesomeIconsService
    ) {

    }

    public onClickSelectColumna(event: MatCheckboxChange, index: number): void {
        this.columnaSubEstructuraSeleccionada = null;
        this.indiceColumnaSeleccionada = (event.checked) ? index : null;
    }

    public onClickVerDetalles(index: number): void {
        let columnaEstructura: ColumnaMetadatos = this.estructura.columnas[index];
        if (!columnaEstructura.subEstructura) {
            this.loading = true;
            this.subEstSvc.buscar(columnaEstructura.idSubEstructura).subscribe(
                (subEstructura: SubEstructuraAneto) => {
                    this.loading = false;
                    if (subEstructura) {
                        columnaEstructura.subEstructura = subEstructura;
                        this.realizarMapeoSubEstructura(columnaEstructura);
                    }
                }
            );
        }
        else {
            this.realizarMapeoSubEstructura(columnaEstructura);
        }

    }

    protected realizarMapeoSubEstructura(columnaOrigen: ColumnaMetadatos): void {

        this.dialog.open(MapearSubEstructuraPlantillaEntradaDialogComponent,
            {
                data: {
                    columnaOrigen: columnaOrigen,
                    evento: this.evento
                }
            }).afterClosed().subscribe( //ejecuta la funcion siguiente tras realizar el mapeo o hacer clic en cancelar
                (data: { columnaDestino: ColumnaDestinoEntrada, columnaSubEstructuraIndex: number, valorFiltrado: string }) => {
                    if (data) {

                        let mapeo = new MapeoEntrada();
                        mapeo.columnaDestino = data.columnaDestino;
                        mapeo.valorParaFiltrar = data.valorFiltrado;
                        mapeo.estructuraOrigen = this.estructura;
                        mapeo.columnaOrigenDatos = columnaOrigen;
                        mapeo.subEstructuraColumnaOrigen = columnaOrigen.subEstructura.columnas[data.columnaSubEstructuraIndex];

                        mapeo.subEstructuraColumnaOrigen.mapeada = true;
                        this.formSvc.entradaMapeoColumnaSubEstructuraSubject.next(mapeo);
                    }
                }
            );
    }

}
