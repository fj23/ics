import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArchivoEstandarPlantillaEntradaComponent } from './archivo-entrada-estandar.component';

describe('ArchivoEstandarPlantillaEntradaComponent', () => {
  let component: ArchivoEstandarPlantillaEntradaComponent;
  let fixture: ComponentFixture<ArchivoEstandarPlantillaEntradaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArchivoEstandarPlantillaEntradaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArchivoEstandarPlantillaEntradaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
