import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { EstructuraArchivoEstructuradoPlantillaEntradaComponent } from './estructura-archivo-entrada.component';


describe('EstructuraArchivoEstructuradoPlantillaEntradaComponent', () => {
  let component: EstructuraArchivoEstructuradoPlantillaEntradaComponent;
  let fixture: ComponentFixture<EstructuraArchivoEstructuradoPlantillaEntradaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstructuraArchivoEstructuradoPlantillaEntradaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstructuraArchivoEstructuradoPlantillaEntradaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
