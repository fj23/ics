import { Component, OnInit, Input, ViewChild, OnDestroy, Output, EventEmitter, ApplicationRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatTable, MatSnackBar, MatCheckboxChange, MatDialog } from '@angular/material';
import { Subscription, Observable, Subject, of } from 'rxjs';

import { ArchivoEntrada } from 'src/models/plantillas/entrada/ArchivoEntrada';
import { MapeoEntrada } from 'src/models/plantillas/entrada/MapeoEntrada';
import { ColumnaMetadatos } from 'src/models/compartido/ColumnaMetadatos';
import { FormularioPlantillasService } from '../../../../../services/formulario-plantillas/formulario-plantillas.service';
import { ColumnaArchivoEstandarPlantillaEntradaComponent } from './columna/columna-archivo-entrada.component';
import { TipoExtensionEnum } from 'src/enums/TipoExtensionEnum';
import { FileUploadDialogComponent } from 'src/app/dialogs/file-upload/file.upload.dialog.component';
import { TipoExtension } from 'src/models/compartido/TipoExtension';
import { TipoDatoModelEnum } from 'src/enums/TipoDatoModelEnum';
import { CommonDataHttpService } from 'src/http-services/common-data.http-service';
import { FontawesomeIconsService } from 'src/services/fontawesome-icons/fontawesome-icons.service';

@Component({
    selector: 'app-archivo-entrada-estandar',
    templateUrl: './archivo-entrada-estandar.component.html',
    styleUrls: ['./archivo-entrada-estandar.component.css']
})
export class ArchivoEstandarPlantillaEntradaComponent implements OnInit, OnDestroy {

    public creacionArchivoEntradaForm: FormGroup;
    protected fileToUpload: File;

    public _archivo: ArchivoEntrada;
    @Input() public set Archivo(archivo: ArchivoEntrada) {
        this._archivo = archivo;
        if (this._tipoExtension) {
            this.onCambioArchivo();
        }
    }
    
    protected _tipoExtension: number;
    @Input() public set TipoExtension(extension: TipoExtension) {
        this._tipoExtension = extension.id;
        if (this._archivo) {
            this.onCambioArchivo();
        }
    }

    @Output() public mapeoColumnaDestino = new EventEmitter<MapeoEntrada>();
    @Output() public onChangeValidez = new EventEmitter<boolean>();

    public showCampoArchivoForm: boolean = false;
    @ViewChild(ColumnaArchivoEstandarPlantillaEntradaComponent) public formularioColumna: ColumnaArchivoEstandarPlantillaEntradaComponent;

    @ViewChild("tablaColumnasArchivo") protected tablaColumnasArchivo: MatTable<ColumnaMetadatos>;
    public archivoCamposDisplayedColumns: string[] = [];
    public indiceColumnaSeleccionada: number;

    protected changeNombreArchivoSub: Subscription;
    protected changeDelimitadorArchivoSub: Subscription;
    protected changeNumeroHojaArchivoSub: Subscription;
    protected changeRealizarTransformacionesArchivoSub: Subscription;
    protected changeSaltoRegistrosInicialesSub: Subscription;
    protected changeSaltoRegistrosFinalesSub: Subscription;

    protected posInicial: number = 0;
    protected posFinal: number = 0;

    constructor(
        protected appRef: ApplicationRef,
        protected commonSvc: CommonDataHttpService,
        protected formSvc: FormularioPlantillasService,
        protected fb: FormBuilder,
        protected snackBar: MatSnackBar,
        protected dialog: MatDialog,
        public icons: FontawesomeIconsService
    ) {

        this.creacionArchivoEntradaForm = this.fb.group({
            nombre: ['', Validators.required],
            noRealizarTransformaciones: [false],
            delimitadorColumnas: [''],
            saltarRegistrosIniciales: ['', Validators.compose([Validators.min(0), Validators.maxLength(3)])],
            saltarRegistrosFinales: ['', Validators.compose([Validators.min(0), Validators.maxLength(3)])],
            numeroHoja: ['']
        });
    }

    public get nombre() { return this.creacionArchivoEntradaForm.get("nombre"); }
    public get noRealizarTransformaciones() { return this.creacionArchivoEntradaForm.get("noRealizarTransformaciones"); }
    public get delimitadorColumnas() { return this.creacionArchivoEntradaForm.get("delimitadorColumnas"); }
    public get saltarRegistrosIniciales() { return this.creacionArchivoEntradaForm.get("saltarRegistrosIniciales"); }
    public get saltarRegistrosFinales() { return this.creacionArchivoEntradaForm.get("saltarRegistrosFinales"); }
    public get numeroHoja() { return this.creacionArchivoEntradaForm.get("numeroHoja"); }

    ngOnInit(): void {
        this.changeNombreArchivoSub = this.nombre.valueChanges.subscribe(() => { this.onChangeNombre(); });
        this.changeDelimitadorArchivoSub = this.delimitadorColumnas.valueChanges.subscribe(() => { 
            this.onChangeDelimitadorColumnas();
        });
        this.changeNumeroHojaArchivoSub = this.numeroHoja.valueChanges.subscribe(() => { this.onChangeNumeroHoja(); });
        this.changeRealizarTransformacionesArchivoSub = this.noRealizarTransformaciones.valueChanges.subscribe(() => { this._archivo.noRealizarTransformaciones = this.noRealizarTransformaciones.value; });
        this.changeSaltoRegistrosInicialesSub = this.saltarRegistrosIniciales.valueChanges.subscribe(() => { this.onChangeSaltoLineasIniciales(); })
        this.changeSaltoRegistrosFinalesSub = this.saltarRegistrosFinales.valueChanges.subscribe(() => { this.onChangeSaltoLineasFinales(); })

    }

    ngOnDestroy(): void {
        this.onChangeValidez.complete();
        this.changeNombreArchivoSub.unsubscribe();
        this.changeDelimitadorArchivoSub.unsubscribe();
        this.changeNumeroHojaArchivoSub.unsubscribe();
        this.changeRealizarTransformacionesArchivoSub.unsubscribe();
        this.changeSaltoRegistrosInicialesSub.unsubscribe();
        this.changeSaltoRegistrosFinalesSub.unsubscribe();
    }

    protected actualizarValidezArchivo() {
        this.creacionArchivoEntradaForm.updateValueAndValidity();
        
        if (this.creacionArchivoEntradaForm.enabled && this.creacionArchivoEntradaForm.invalid) {
            this.onChangeValidez.next(false);
        } else if (this._archivo.columnas && this._archivo.columnas.length === 0) {
            this.onChangeValidez.next(false);
        } else if (this._archivo.estructuras && this._archivo.estructuras.length === 0) {
            this.onChangeValidez.next(false);
        } else {
            this.onChangeValidez.next(true);
        }
    }

    protected actualizarValidadorDelimitadorColumnas() {

        this.delimitadorColumnas.setValidators(null);
        let extension = this.formSvc.entradaFormatoSubject.getValue();
        if (extension === TipoExtensionEnum.CSV || extension === TipoExtensionEnum.PLANO) {
            
            if (extension === TipoExtensionEnum.PLANO && !this.delimitadorColumnas.value) {
                this.archivoCamposDisplayedColumns = ['nombre', 'tipo', 'posicion', 'acciones'];
            } else {
                this.archivoCamposDisplayedColumns = ['nombre', 'tipo', 'acciones'];
                if (extension === TipoExtensionEnum.CSV) {
                    this.delimitadorColumnas.setValidators(Validators.required);
                }
            }
        } else {
            this.delimitadorColumnas.disable();
            this.delimitadorColumnas.setValue(null);
            this.archivoCamposDisplayedColumns = ['nombre', 'tipo', 'acciones'];
        }
        
        this.tablaColumnasArchivo.dataSource = of(this._archivo.columnas);
    }

    protected actualizarValidadorNumeroHoja() {
        const noChange = {onlySelf: true, emitEvent: false};
    
        if (this._tipoExtension === TipoExtensionEnum.EXCEL || this._tipoExtension === TipoExtensionEnum.EXCEL97_2003) {
            if (this.creacionArchivoEntradaForm.enabled) {
                this.numeroHoja.enable(noChange);
            }
            this.numeroHoja.setValidators(Validators.required);
        } else {
            this.numeroHoja.disable(noChange);
            this.numeroHoja.setValue('', noChange);
        }
    }

    protected agregarNuevaColumna(nueva: ColumnaMetadatos) {
        if (this._archivo.columnas.some((columna: ColumnaMetadatos) => { return columna.descripcion === nueva.descripcion; })) {
            this.snackBar.open("Ya existe una columna con ese nombre.");
        } else {
            this._archivo.columnas.push(nueva);
            this.updateColumnasOrden();
            this.tablaColumnasArchivo.renderRows();
            this.actualizarValidezArchivo();
        }
    }

    /**
     * Establece al FileReader que lea el archivo subido y entrega un Observable para esperar a que la lectura finalice.
     * @param reader 
     */
    protected leerArchivoMetadatos(reader: FileReader): Observable<void> {
        let readEventEmitter = new Subject<void>();
        reader.onload = function () { readEventEmitter.next(); }; //cuando termine de leer emitira un void
        reader.readAsText(this.fileToUpload);
        return readEventEmitter.asObservable();
    }

    protected updateColumnasOrden() {
        for (let i = 0; i < this._archivo.columnas.length; i++) {
            let columa = this._archivo.columnas[i];
            columa.orden = i + 1;
        }
    }

    /**
     * Con el contenido del archivo original, genera un array de columnas de archivo de entrada y las adhiere como tal
     */
    protected procesarArchivoMetadatos(fileData: string | ArrayBuffer): void {
        let fileRows: string[] = fileData.toString().split(/\n/); //separa el 'string' por los saltos de línea
        this._archivo.columnas = []; //limpia la grilla de columnas del archivo

        for (let i = 0; i < fileRows.length; i++) {
            if (fileRows[i]) {
                const row = fileRows[i].trim();
                if (row) {
                    const rowData: string[] = row.split(";"); //separa columnas por punto y coma
                    if (rowData === null || rowData.length !== 4) {
                        continue;
                    }
    
                    let nueva = new ColumnaMetadatos();
                    nueva.descripcion = rowData[0];
                    nueva.tipoDato = TipoDatoModelEnum[rowData[1].toUpperCase()];
                    nueva.posicionInicial = Number(rowData[2]);
                    nueva.posicionFinal = Number(rowData[3]);
    
                    this.agregarNuevaColumna(nueva);
                }
            }
        }

        this.fileToUpload = null;
        this.updateColumnasOrden();
        this.tablaColumnasArchivo.renderRows();

    }

    /**
     * Genera un diálogo que pide confirmación para soltar la homologación que esté siendo creada o editada.
     */
    protected dialogoSeleccionArchivoMetadatos(): Observable<any> {
        const confirmationDialog = this.dialog.open(
            FileUploadDialogComponent,
            {
                width: "50%",
                data:
                {
                    titulo: "Seleccionar archivo a importar...",
                    boton_si_clases: "btn-warning",
                    boton_no_clases: ""
                }
            }
        );

        return confirmationDialog.beforeClosed();
    }


    protected onCambioArchivo(): void {
        this.indiceColumnaSeleccionada = null;

        const noChange = { onlySelf: true, emitEvent: false };
        if (this._archivo) {

            this._archivo.estructuras = null;
            if (!this._archivo.columnas) {
                this._archivo.columnas = [];
            }
                
            this.nombre.setValue(this._archivo.nombre, noChange);
            this.noRealizarTransformaciones.setValue(this._archivo.noRealizarTransformaciones, noChange);
            this.saltarRegistrosIniciales.setValue(this._archivo.saltarRegistrosIniciales, noChange);
            this.saltarRegistrosFinales.setValue(this._archivo.saltarRegistrosFinales, noChange);
            
            if (this._archivo.numeroHoja) {  
                this.numeroHoja.setValue(this._archivo.numeroHoja, noChange);
            } else {
                this.numeroHoja.setValue('', noChange);
            }
            
            if (this._archivo.delimitadorColumnas) {  
                this.delimitadorColumnas.setValue(this._archivo.delimitadorColumnas, noChange);
            } else {
                this.delimitadorColumnas.setValue('', noChange);
            }

            this.actualizarValidadores();
            this.actualizarValidezArchivo();
        }
    }

    private onChangeNombre() {
        this._archivo.nombre = this.nombre.value;
        this.actualizarValidezArchivo();
    }

    public onChangeDelimitadorColumnas() {
        this._archivo.delimitadorColumnas = this.delimitadorColumnas.value;

        if (this.delimitadorColumnas.value || this._tipoExtension !== TipoExtensionEnum.PLANO) {
            this.archivoCamposDisplayedColumns = ['nombre', 'tipo', 'acciones'];
        } else {
            this.archivoCamposDisplayedColumns = ['nombre', 'tipo', 'posicion', 'acciones'];
        }
        this.actualizarValidezArchivo();
    }

    protected onChangeNumeroHoja() {
        
        let numHoja: number = Number(this.numeroHoja.value);
        
        if (!isNaN(numHoja)) {
            this._archivo.numeroHoja = numHoja;
        } else {
            this.numeroHoja.setValue('', {onlySelf: true, emitEvent: false});
        }
        this.actualizarValidezArchivo();
    }

    protected onChangeSaltoLineasIniciales() {
        let saltosIniciales: number = Number(this.saltarRegistrosIniciales.value);
        if (!isNaN(saltosIniciales)) {
            this._archivo.saltarRegistrosIniciales = saltosIniciales;
        } else {
            this.saltarRegistrosIniciales.setValue(0);
        }
        this.actualizarValidezArchivo();
    }

    protected onChangeSaltoLineasFinales() {
        let saltosFinales: number = Number(this.saltarRegistrosFinales.value);
        if (!isNaN(saltosFinales)) {
            this._archivo.saltarRegistrosFinales = saltosFinales;
        } else {
            this.saltarRegistrosFinales.setValue(0);
        }
        this.actualizarValidezArchivo();
    }

    public onClickAgregarColumna(): void {
        this.showCampoArchivoForm = !this.showCampoArchivoForm;
    }

    /**
     * Valida el formulario de columna, la crea y la agrega al archivo actual.
     */
    public onReceiveColumna(nueva: ColumnaMetadatos): void {
        this.agregarNuevaColumna(nueva);
    }

    public onClickGenerarDesdeArchivo(): void {
        this.dialogoSeleccionArchivoMetadatos().subscribe(
            (file: File) => {
                if (file != null) {
                    this.fileToUpload = file;
                    let reader: FileReader = new FileReader();
                    let readSubscription: Subscription = this.leerArchivoMetadatos(reader).subscribe(
                        () => {
                            readSubscription.unsubscribe();
                            if (reader.result != null) {
                                this.procesarArchivoMetadatos(reader.result);
                            }
                        }
                    )
                }
            }
        );
    }

    /**
     * Desplaza la columna (obtenida por su índice) un puesto más arriba (antes).
     * @param index El índice de la columna a subir.
     */
    public onClickSubirColumna(index: number): void {
        if (index > 0) {
            if (this.indiceColumnaSeleccionada === index) {
                this.indiceColumnaSeleccionada--;
            }
            let estaColumna: ColumnaMetadatos = this._archivo.columnas[index];
            let columnaSuperior: ColumnaMetadatos = this._archivo.columnas[index - 1];
            this._archivo.columnas[index - 1] = estaColumna;
            this._archivo.columnas[index] = columnaSuperior;
            this.updateColumnasOrden();
            this.tablaColumnasArchivo.renderRows();
        }
    }

    /**
     * Desplaza la columna (obtenida por su índice) un puesto más abajo (después).
     * @param index El índice de la columna a bajar.
     */
    public onClickBajarColumna(index: number): void {
        if (index < this._archivo.columnas.length - 1) {
            if (this.indiceColumnaSeleccionada === index) {
                this.indiceColumnaSeleccionada++;
            }
            let estaColumna: ColumnaMetadatos = this._archivo.columnas[index];
            let columnaInferior: ColumnaMetadatos = this._archivo.columnas[index + 1];
            this._archivo.columnas[index + 1] = estaColumna;
            this._archivo.columnas[index] = columnaInferior;
            this.updateColumnasOrden();
            this.tablaColumnasArchivo.renderRows();
        }
    }

    /**
     * Retira la columna (obtenida por su índice).
     * @param index El índice de la columna a subir.
     */
    public onClickQuitarColumna(index: number): void {
        if (index <= this._archivo.columnas.length - 1) {
            if (this._archivo.columnas[index].mapeada) {
                this.snackBar.open("Esta columna está mapeada. Primero retire el mapeo asociado.");
            } else {

                if (this.indiceColumnaSeleccionada === index) {
                    this.indiceColumnaSeleccionada = null;
                }
                this._archivo.columnas.splice(index, 1);
                this.updateColumnasOrden();
                this.tablaColumnasArchivo.renderRows();
                this.actualizarValidezArchivo();
            }
        }
    }

    public onClickSelectColumna(event: MatCheckboxChange, index: number) {
        this.indiceColumnaSeleccionada = (event.checked) ? index : null;
    }

    public onReceivePosInicial(posInicial: number): void {
        this.posInicial = posInicial;
        this.PosInicialFinalChange();
    }

    public onReceivePosFinal(posFinal: number): void {
        this.posFinal = posFinal;
        this.PosInicialFinalChange();
    }

    protected PosInicialFinalChange() {
        if(this.posInicial && this.posFinal) {
            this.delimitadorColumnas.setValidators(null);
        }
    }

    protected actualizarValidadores(): void {
        this.actualizarValidadorNumeroHoja();
        this.actualizarValidadorDelimitadorColumnas();
    }
    
    public onChangeEstadoMapeo() {
        this.actualizarValidadores();
        this.actualizarValidezArchivo();
    }

}
