import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlantillasCrearComponent } from './plantillas-crear.component';

describe('FormularioPlantillaComponent', () => {
  let component: PlantillasCrearComponent;
  let fixture: ComponentFixture<PlantillasCrearComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlantillasCrearComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlantillasCrearComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
