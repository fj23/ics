import { Component, ViewChild, Input } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatTable, MatSnackBar } from '@angular/material';
import { Observable } from 'rxjs';

import { FormularioPlantillasService } from '../../../../../services/formulario-plantillas/formulario-plantillas.service';

import { Evento } from 'src/models/compartido/Evento';
import { ColumnaDestinoEntrada } from 'src/models/plantillas/entrada/ColumnaDestinoEntrada';
import { TipoExtension } from 'src/models/compartido/TipoExtension';
import { ColumnaArchivoSalida } from 'src/models/plantillas/salida/ColumnaArchivoSalida';
import { CommonDataHttpService } from 'src/http-services/common-data.http-service';
import { PlantillasHttpService } from 'src/http-services/plantillas.http-service';
import { FontawesomeIconsService } from 'src/services/fontawesome-icons/fontawesome-icons.service';
import { TipoExtensionEnum } from 'src/enums/TipoExtensionEnum';

@Component({
    selector: 'app-salida-estandar',
    templateUrl: './salida-estandar.component.html',
    styleUrls: ['./salida-estandar.component.css']
})
export class FormularioEstandarPlantillaSalidaComponent {

    public columnasDestinoDisponibles$: Observable<ColumnaDestinoEntrada[]>;

    public campoDisponibleSeleccionado: ColumnaDestinoEntrada;

    public _tipoExtension: TipoExtension;
    @Input() public set TipoExtension(extension: TipoExtension) {
        this._tipoExtension = extension;
        this.onChangeTipoExtension();
    }
    
    public _delimitadorColumnas: string;
    @Input() public set DelimitadorColumnas(delimitador: string) {
        this._delimitadorColumnas = delimitador;
        this.onChangeTipoExtension();
    }

    public _evento: Evento;
    @Input() public set Evento(evento: Evento) {
        this._evento = evento;
        this.campoDisponibleSeleccionado = null;
        this.columnasDestinoDisponibles$ = this.localSvc.getColumnasDestinoEntradaDisponiblesUsuarios(evento.id);
    }

    @ViewChild('tablaColumnasSalida') public tablaColumnasSalida: MatTable<ColumnaArchivoSalida>;
    public columnasDisplayedColumns = ['nombre', 'posicion', 'accion'];
    public _columnasSalida: ColumnaArchivoSalida[] = [];
    public get columnasSalida(): ColumnaArchivoSalida[] { return this._columnasSalida; }

    public columnaSeleccionada: ColumnaArchivoSalida;

    constructor(
        protected commonSvc: CommonDataHttpService,
        protected localSvc: PlantillasHttpService,
        protected formSvc: FormularioPlantillasService,
        protected fb: FormBuilder,
        protected snackBar: MatSnackBar,
        public icons: FontawesomeIconsService
    ) {
        
    }

    protected onChangeTipoExtension(): void {
        if (this._tipoExtension) {    
            if (this._tipoExtension.id === TipoExtensionEnum.PLANO && !this._delimitadorColumnas) {
                this.columnasDisplayedColumns = ['nombre', 'posicion', 'accion'];
            } else {
                this.columnasDisplayedColumns = ['nombre', 'accion'];
            }   
        }
    }

    public onClickAgregarColumna(): void {
        if (!this.campoDisponibleSeleccionado) {
            this.snackBar.open("Debe seleccionar un campo de datos de origen disponible.");
        }
        else {
            let columna: ColumnaArchivoSalida = new ColumnaArchivoSalida();
            columna.orden = this._columnasSalida.length + 1;
            columna.columnaOrigen = this.campoDisponibleSeleccionado;
            this._columnasSalida.push(columna);

            this.formSvc.completitudFormularioSubject.next(true);
            this.formSvc.bloquearDatosBasicosPlantillaSubject.next(true);
            this.tablaColumnasSalida.renderRows();
        }
    }

    public onClickEditarColumna(index: number): void {
        if (this._columnasSalida[index] !== this.columnaSeleccionada) {
            this.columnaSeleccionada = this._columnasSalida[index];
        }
        else {
            this.columnaSeleccionada = null;
        }
    }

    public onClickSubirColumna(index: number): void {
        if (index > 0) {
            let estaColumna: ColumnaArchivoSalida = this._columnasSalida[index];
            let columnaSuperior: ColumnaArchivoSalida = this._columnasSalida[index - 1];
            this._columnasSalida[index - 1] = estaColumna;
            this._columnasSalida[index] = columnaSuperior;
            this.tablaColumnasSalida.renderRows();
        }
    }

    public onClickBajarColumna(index: number): void {
        if (index < this._columnasSalida.length - 1) {
            let estaColumna: ColumnaArchivoSalida = this._columnasSalida[index];
            let columnaInferior: ColumnaArchivoSalida = this._columnasSalida[index + 1];
            this._columnasSalida[index + 1] = estaColumna;
            this._columnasSalida[index] = columnaInferior;
            this.tablaColumnasSalida.renderRows();
        }
    }

    public onClickQuitarColumna(index: number): void {
        if (index <= this._columnasSalida.length) {
            this._columnasSalida.splice(index, 1);

            if (this._columnasSalida.length === 0) {
                this.formSvc.completitudFormularioSubject.next(false);
                this.formSvc.bloquearDatosBasicosPlantillaSubject.next(false);
            }
            this.tablaColumnasSalida.renderRows();
        }
    }
}
