import { Component, ViewChild, Input, OnInit } from '@angular/core';
import { EstructuraAnetoSalida } from 'src/models/plantillas/salida/estructura/EstructuraAnetoSalida';
import { MatTable } from '@angular/material';
import { ColumnaEstructuraAnetoSalida } from 'src/models/plantillas/salida/estructura/ColumnaEstructuraAnetoSalida';
import { Observable } from 'rxjs';
import { TipoFormato } from 'src/models/compartido/TipoFormato';
import { Homologacion } from 'src/models/homologaciones/Homologacion';
import { SistemaCore } from 'src/models/compartido/SistemaCore';
import { CommonDataHttpService } from 'src/http-services/common-data.http-service';
import { FontawesomeIconsService } from 'src/services/fontawesome-icons/fontawesome-icons.service';
import { AtributoDiccionarioEstructuraAnetoSalida } from 'src/models/plantillas/salida/estructura/AtributoDiccionarioEstructuraAnetoSalida';
import { PlantillasHttpService } from 'src/http-services/plantillas.http-service';
import { HomologacionesHttpService } from 'src/http-services/homologaciones.http-service';
import { Socio } from 'src/models/compartido/Socio';
import { Evento } from 'src/models/compartido/Evento';

@Component({
    selector: 'app-detalles-estructura-salida',
    templateUrl: './detalles-estructura-salida.component.html',
    styleUrls: ['./detalles-estructura-salida.component.css']
})
export class DetallesEstructuraPlantillaSalidaComponent
    implements OnInit {

    protected _evento: Evento;
    @Input() public set Evento(evento: Evento) { this._evento = evento; }
    public get evento() { return this._evento; }
        
    protected _socioId: number;
    public get socioId() { return this._socioId; }
    @Input() public set Socio(socio: Socio) { 
        if (!!socio) {
            this._socioId = socio.id;  
        } else {
            this._socioId = null;
        }
        this.actualizarHomologaciones();
    }
    
    protected _coreId: number;
    public get coreId() { return this._coreId; }
    @Input() public set Core(core: SistemaCore) { 
        if (!!core) {
            this._coreId = core.id;
        } else {
            this._coreId = null;
         }
        this.actualizarHomologaciones(); 
    }
    
    @Input() public estructura: EstructuraAnetoSalida;
    @ViewChild('tablaDetallesEstructura') public tablaDetallesEstructura: MatTable<ColumnaEstructuraAnetoSalida>;
    public estructuraColumnasDisplayedColumns: string[] = ['nombre', 'valor', 'acciones'];
    public tiposDato = [ { id: 1, descripcion: "Constante" }, { id: 2, descripcion: "Atributo" } ];

    protected _columnaEnTransformacion: ColumnaEstructuraAnetoSalida;
    public get columnaEnTransformacion() { return this._columnaEnTransformacion; }

    public atributosDiccionario$: Observable<AtributoDiccionarioEstructuraAnetoSalida[]>;

    public formatoFechasItems$: Observable<TipoFormato[]>;
    public homologacionesItems$: Observable<Homologacion[]>;

    constructor(
        protected commonSvc: CommonDataHttpService,
        protected localSvc: PlantillasHttpService,
        protected homoSvc: HomologacionesHttpService,
        public icons: FontawesomeIconsService
    ) {
        this._columnaEnTransformacion = null;
    }

    public get descripcion() { return this.estructura.descripcion; }
    public get columnas() { return this.estructura.columnas; }

    ngOnInit() {
        this.formatoFechasItems$ = this.commonSvc.getTiposFormato();
        this.atributosDiccionario$ = this.localSvc.getAtributosDiccionarioSalida();
    }

    private actualizarHomologaciones(): void {
        if (this.socioId && this.coreId) {
            this.homologacionesItems$ = this.homoSvc.getTipos(this.socioId, this.coreId);
        }
    }

    public onChangeTipoDato(col: ColumnaEstructuraAnetoSalida): void {
        if (col.tipoDato !== 1) {
            col.valorFijo = undefined;
        }
        if (col.tipoDato !== 2) {
            col.atributoDiccionario = undefined;
        }

        if (!col.tipoDato) {
            col.formula = null;
            col.formato = null;
            col.homologacion = null;
        }
    }

    public onClickVerTransformacionesColumna(index: number): void {
        const columna = this.columnas[index];
        
        if (this._columnaEnTransformacion !== columna) {
            this._columnaEnTransformacion = columna;
        } else {
            this._columnaEnTransformacion = null;
        }        
    }

}