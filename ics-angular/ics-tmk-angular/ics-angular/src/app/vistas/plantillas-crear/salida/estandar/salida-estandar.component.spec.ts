import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormularioEstandarPlantillaSalidaComponent } from './salida-estandar.component';

describe('FormularioEstandarPlantillaSalidaComponent', () => {
  let component: FormularioEstandarPlantillaSalidaComponent;
  let fixture: ComponentFixture<FormularioEstandarPlantillaSalidaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormularioEstandarPlantillaSalidaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormularioEstandarPlantillaSalidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
