import { Component, OnInit, ViewChild, Input, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { Observable, Subscription, of, Subject, BehaviorSubject } from 'rxjs';

import { CommonDataHttpService } from 'src/http-services/common-data.http-service';

import { FormularioEstandarPlantillaSalidaComponent } from './estandar/salida-estandar.component';
import { FormularioEstructurasPlantillaSalidaComponent } from './estructurada/salida-estructurada.component';

import { TipoExtension } from 'src/models/compartido/TipoExtension';
import { FormularioPlantillasService } from '../../../../services/formulario-plantillas/formulario-plantillas.service';
import { Evento } from 'src/models/compartido/Evento';
import { SistemaCore } from 'src/models/compartido/SistemaCore';
import { Socio } from 'src/models/compartido/Socio';
import { PlantillasHttpService } from 'src/http-services/plantillas.http-service';
import { TipoExtensionEnum } from 'src/enums/TipoExtensionEnum';

@Component({
    selector: 'app-plantilla-salida',
    templateUrl: './plantilla-salida.component.html',
    styleUrls: ['./plantilla-salida.component.css']
})
export class FormularioPlantillaSalidaComponent implements OnInit, OnDestroy {

    protected _coresSalida: SistemaCore[];
    public tiposExtension$: Observable<TipoExtension[]>;
    public coresSalida$: Observable<SistemaCore[]>;

    public creacionPlantillaSalidaForm: FormGroup;

    @ViewChild("formularioEstandar") public formularioEstandar: FormularioEstandarPlantillaSalidaComponent;
    @ViewChild("formularioEstructurado") public formularioEstructurado: FormularioEstructurasPlantillaSalidaComponent;

    @Input() public evento: Evento;
    protected _esFormularioEstructurado$: Subject<boolean> = new BehaviorSubject(false);
    public esFormularioEstructurado$: Observable<boolean> = this._esFormularioEstructurado$.asObservable();

    protected _socio: Socio;
    @Input() public set Socio(socio: Socio) { this._socio = socio; }
    public get Socio() { return this._socio; }

    protected changeTipoExtensionSub: Subscription;

    constructor(
        protected commonSvc: CommonDataHttpService,
        protected localSvc: PlantillasHttpService,
        protected formSvc: FormularioPlantillasService,
        protected fb: FormBuilder,
        protected snackBar: MatSnackBar
    ) {
        //this.formSvc.completitudFormularioSubject.next(false);
        this.creacionPlantillaSalidaForm = this.fb.group({
            tipoExtension: [null, Validators.required],
            delimitadorColumnas: [{ value: '', disabled: true }],
            core: [{ value: null, disabled: true }]
        });
    }

    public get tipoExtension() { return this.creacionPlantillaSalidaForm.get("tipoExtension"); }
    public get delimitadorColumnas() { return this.creacionPlantillaSalidaForm.get("delimitadorColumnas"); }
    public get core() { return this.creacionPlantillaSalidaForm.get("core"); }

    ngOnInit(): void {
        this.tiposExtension$ = this.commonSvc.getTiposExtension();
        this.localSvc.getCoresEstructurasSalida().subscribe(
            cores => {
                this._coresSalida = cores;
                this.coresSalida$ = of(cores);
            }
        );

        this.changeTipoExtensionSub = this.tipoExtension.valueChanges.subscribe(() => { this.onChangeTipoExtension(); });
    }

    ngOnDestroy(): void {
        if (this.changeTipoExtensionSub) {
            this.changeTipoExtensionSub.unsubscribe();
        }
    }

    protected onChangeTipoExtension() {
        this.formSvc.salidaTipoExtensionSubject.next(this.tipoExtension.value);
        let extensionId = this.tipoExtension.value.id;
        if (extensionId === TipoExtensionEnum.CSV || extensionId === TipoExtensionEnum.PLANO) {
            this.delimitadorColumnas.enable();
            if (extensionId === TipoExtensionEnum.CSV) {
                this.delimitadorColumnas.setValidators(Validators.required);
            }
            else {
                this.delimitadorColumnas.setValidators(null);
            }
            this.delimitadorColumnas.updateValueAndValidity();
        }
        else {
            this.delimitadorColumnas.disable();
            this.delimitadorColumnas.setValue('');
            this.delimitadorColumnas.setValidators(null);
        }
        const esEstructurado: boolean = (extensionId === TipoExtensionEnum.ESTRUCTURADO);
        if (esEstructurado) {
            this.core.enable();
        }
        else if (this.core.enabled) {
            this.core.disable();
        }
        this._esFormularioEstructurado$.next(esEstructurado);
    }
}
