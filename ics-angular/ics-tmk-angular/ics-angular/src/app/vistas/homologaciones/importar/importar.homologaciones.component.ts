import { Component, OnInit } from '@angular/core';
import { Observable, of, Subject, Subscription, BehaviorSubject, concat } from 'rxjs';


import { Socio } from 'src/models/compartido/Socio';
import { SistemaCore } from 'src/models/compartido/SistemaCore';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar, MatDialog } from '@angular/material';
import { catchError, finalize } from 'rxjs/operators';
import { FiltrosHomologacionesModel } from 'src/models/filters/FiltrosHomologacionesModel';
import { HomologacionesHttpService } from 'src/http-services/homologaciones.http-service';
import { CommonDataHttpService } from 'src/http-services/common-data.http-service';
import { Homologacion } from 'src/models/homologaciones/Homologacion';
import { InformationDialogComponent, InformationDialogData } from 'src/app/dialogs/information/information.dialog.component';

@Component({
    selector: 'app-importar-homologaciones',
    templateUrl: './importar.homologaciones.component.html',
    styleUrls: ['./importar.homologaciones.component.css']
})
export class ImportarHomologacionesComponent implements OnInit {

    public sistemas$: Observable<SistemaCore[]>;
    public socios$: Observable<Socio[]>;
    public conceptos$: Observable<String[]>;

    public importarHomologacionesForm: FormGroup;
    private fileToUpload: File;
    private loadingFileSubject = new BehaviorSubject<boolean>(false);
    public loadingFile$ = this.loadingFileSubject.asObservable();

    private readSubscription: Subscription;

    constructor(
        private localSvc: HomologacionesHttpService,
        private commonSvc: CommonDataHttpService,
        private fb: FormBuilder,
        private snackBar: MatSnackBar,
        private dialog: MatDialog
    ) {
        this.importarHomologacionesForm = this.fb.group({
            concepto: [null, Validators.required],
            socio1: [null, Validators.required],
            socio2: [null, Validators.required],
            core1: [null, Validators.required],
            core2: [null, Validators.required],
            archivo: [null, Validators.required]
        });
    }

    public get concepto() { return this.importarHomologacionesForm.get("concepto"); }
    public get socio1() { return this.importarHomologacionesForm.get("socio1"); }
    public get socio2() { return this.importarHomologacionesForm.get("socio2"); }
    public get core1() { return this.importarHomologacionesForm.get("core1"); }
    public get core2() { return this.importarHomologacionesForm.get("core2"); }
    public get archivo() { return this.importarHomologacionesForm.get("archivo"); }

    ngOnInit(): void {
        this.sistemas$ = this.commonSvc.getSistemasCore();
        this.socios$ = this.commonSvc.getSocios();
        this.conceptos$ = this.localSvc.getConceptosHomologacion();
    }

    public onFileSelect(files: FileList) {
        this.fileToUpload = files.item(0);
    }

    /**
     * Establece al FileReader que lea el archivo subido y entrega un Observable para suscribirse al evento en que la lectura finaliza.
     * @param reader El leactor de archivos.
     */
    private readFile(reader: FileReader): Observable<void> {
        let readEventEmitter = new Subject<void>();
        reader.onload = function () { 
            readEventEmitter.next(); 
            readEventEmitter.complete(); 
        };
        reader.readAsText(this.fileToUpload);
        return readEventEmitter.asObservable();
    }

    /**
     * Intenta validar el formulario y leer los datos del archivo subido. 
     * En caso de éxito, llama al método submit() con los contenidos del archivo leído.
     */
    public onSubmit(): void {
        if (!this.importarHomologacionesForm.valid) {
            for (const ctrlId in this.importarHomologacionesForm.controls) {
                this.importarHomologacionesForm.controls[ctrlId].markAsTouched();
            }
            this.snackBar.open("Debe rellenar todos los datos del formulario para importar homologaciones.");
        }
        else if (this.socio1.value === this.socio2.value) {
            this.snackBar.open("Los socios de origen y destino deben ser distintos.");
        }
        else if (this.loadingFileSubject.getValue()) {
            this.snackBar.open("Ya hay un archivo en proceso.");
        }
        else {

            //el archivo debería tener la extensión .csv al final del nombre
            const nameLength = this.fileToUpload.name.length;
            const fileExtensionStartingIndex = this.fileToUpload.name.toLowerCase().lastIndexOf(".csv");
            if (
                fileExtensionStartingIndex === -1 || //la extensión no está
                fileExtensionStartingIndex < nameLength - 4 //hay caracteres después de la extensión
            ) {
                this.snackBar.open("Sólo se aceptan archivos de extensión CSV.");
            }
            else if (nameLength - fileExtensionStartingIndex === 0) {
                this.snackBar.open("Archivo inválido.");
            }
            else {
                this.loadingFileSubject.next(true);
                let reader: FileReader = new FileReader();
                this.readSubscription = this.readFile(reader).subscribe(
                    () => {
                        this.readSubscription.unsubscribe();
                        this.submitFile(reader.result);
                    }
                );
            }
        }
    }

    /**
     * Intenta mandar los datos del formulario al servidor.
     * @param fileData La data en duro del archivo subido.
     */
    private submitFile(fileData: string | ArrayBuffer) {
        let homologaciones: Homologacion[];
        try {
            homologaciones = this.processFileIntoArray(fileData);
        }
        catch (exc) {
            homologaciones = null;
        }

        if (homologaciones === null) {
            this.loadingFileSubject.next(false);
        }
        else {

            this.localSvc.crearMultiple(
                homologaciones
            ).pipe(
                finalize(() => { this.loadingFileSubject.next(false); })
            ).subscribe(
                (respuestas: string[]) => {

                    if (respuestas.length > 0) {
                        let concatMensaje: string;
                        let dialogTitulo: string = "Archivo de homologaciones cargado con errores";
                        if (respuestas.length === homologaciones.length) {
                            dialogTitulo =  "Archivo no cargado";
                            concatMensaje = "No se importó ninguna homologación del archivo. Presumiblemente éstas ya habían sido creadas con anterioridad.";
                        } else {
                            this.recargarTablaConNuevaHomologacion(homologaciones[0]);
                            concatMensaje = "Hubo registros del archivo que no pudieron ser importados por los motivos descritos a continuación:";
                            respuestas.forEach((resp: string) => {
                                if (resp) {
                                    concatMensaje += resp + " - ";
                                }
                            });
                        }

                        const dialogData: InformationDialogData = {
                            title: dialogTitulo,
                            content: concatMensaje
                        };

                        const confirmationDialog = this.dialog.open(
                            InformationDialogComponent,
                            {
                                width: "50%",
                                data: dialogData
                            }
                        );
                
                        return confirmationDialog.beforeClosed();
                    } else {
                        this.localSvc.newHomologacionSubject.next();
                        this.importarHomologacionesForm.reset();
                        this.recargarTablaConNuevaHomologacion(homologaciones[0]);
                        this.snackBar.open( homologaciones.length + " homologaciones importadas con éxito.");
                    }

                },
                err => {
                    console.log(err);
                    
                }
            );
        }
    }

    private processFileIntoArray(fileData: string | ArrayBuffer) {
        let homologaciones: Homologacion[] = [];

        const fileRows: string[] = fileData.toString().split(/\n/);
        if (fileRows[0].trim().length === 0) {
            this.snackBar.open("El archivo no posee datos.");
            return null;
        }
        else {
            for (let i = 0; i < fileRows.length; i++) {
                const row = fileRows[i];
                if (row.indexOf(";") === -1) {
                    this.snackBar.open("La línea "+(i+1)+" del archivo no posee 2 valores. Corríjalo e inténtelo nuevamente.");
                    return null;
                }
                else {
                    const rowData = row.split(";");
                    if (rowData.length > 2) {
                        this.snackBar.open("La línea "+(i+1)+" del archivo posee más de 2 valores. Corríjalo e inténtelo nuevamente.");
                        return null;
                    }
                    else if (rowData[0].trim() === "" || rowData[1].trim() === "") {
                        this.snackBar.open("La línea "+(i+1)+" posee un valor vacío. Corríjalo e inténtelo nuevamente.");
                        return null;
                    }
                    else {
                        let nueva = new Homologacion();
                        nueva.concepto = this.concepto.value;
                        nueva.socio1 = this.socio1.value;
                        nueva.socio2 = this.socio2.value;
                        nueva.core1 = this.core1.value;
                        nueva.core2 = this.core2.value;
                        nueva.valor1 = rowData[0];
                        nueva.valor2 = rowData[1];
                        nueva.vigencia = "Y";
                        homologaciones.push(nueva);
                    }
                }
            }
            return homologaciones;
        }
    }
    

    private recargarTablaConNuevaHomologacion(homologacion: Homologacion): void {
        let filtro: FiltrosHomologacionesModel  = new FiltrosHomologacionesModel();
        filtro.sistema1 = homologacion.core1.id;
        filtro.sistema2 = homologacion.core2.id;
        filtro.socio1 = homologacion.socio1.id;
        filtro.socio2 = homologacion.socio2.id;
        filtro.concepto = homologacion.concepto;
        this.localSvc.setFiltros(filtro);
    }
}
