import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormularioHomologacionComponent } from './formulario.homologaciones.component';

describe('FormularioHomologacionComponent', () => {
  let component: FormularioHomologacionComponent;
  let fixture: ComponentFixture<FormularioHomologacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormularioHomologacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormularioHomologacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
