import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TablaHomologacionesComponent } from './tabla.homologaciones.component';

describe('LoginComponent', () => {
  let component: TablaHomologacionesComponent;
  let fixture: ComponentFixture<TablaHomologacionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TablaHomologacionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TablaHomologacionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
