import {
  Component,
  OnInit,
  ViewChild,
  OnDestroy,
  Output,
  EventEmitter
} from "@angular/core";
import {
  MatSort,
  MatPaginator,
  MatDialog,
  MatSnackBar
} from "@angular/material";
import { merge, Subscription } from "rxjs";
import { catchError } from "rxjs/operators";

import { MatPaginatorES } from "src/app/compartido/mat.paginator.es";
import { HomologacionesDataSource } from "./tabla.homologaciones.datasource";
import { Homologacion } from "src/models/homologaciones/Homologacion";
import { Permisos } from "src/app/compartido/Permisos";
import { Angular5Csv } from "angular5-csv/dist/Angular5-csv";
import { FontawesomeIconsService } from "src/services/fontawesome-icons/fontawesome-icons.service";
import { HomologacionesHttpService } from "src/http-services/homologaciones.http-service";
import { ConfirmationDialogComponent } from "src/app/dialogs/confirmation/confirmation.dialog.component";

@Component({
    selector: "app-tabla-homologaciones",
    templateUrl: "./tabla.homologaciones.component.html",
    styleUrls: ["./tabla.homologaciones.component.css"]
})
export class TablaHomologacionesComponent implements OnInit, OnDestroy {

    @Output() public editHomologacion: EventEmitter<Homologacion> = new EventEmitter<Homologacion>();

    @ViewChild(MatPaginator) private paginator: MatPaginator;
    @ViewChild(MatSort) private sort: MatSort;
    public displayedColumns: string[] = [
        "concepto",
        "core1",
        "socio1",
        "valor1",
        "core2",
        "socio2",
        "valor2",
        "acciones"
    ];
    public dataSource: HomologacionesDataSource;
    public ocultarPorPermisos: boolean;

    private homologaciones: Homologacion[];

    private reloadTableSub: Subscription;
    private homologacionesCountSub: Subscription;
    private homologacionesSub: Subscription;

    constructor(
        private localSvc: HomologacionesHttpService,
        public dialog: MatDialog,
        private snackBar: MatSnackBar,
        public icons: FontawesomeIconsService
    ) {
        this.dataSource = new HomologacionesDataSource(this.localSvc);
        this.ocultarPorPermisos = Permisos.DenegarAcceso(Permisos.MantenerEstructura());
    }

    ngOnInit(): void {
        this.paginator._intl = new MatPaginatorES();

        //cuando el orden aplicado a los datos cambie, vuelve a la pagina 1
        this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

        //cuando el orden o el numero de pagina cambie, recarga los datos
        this.reloadTableSub = merge(
            this.sort.sortChange,
            this.paginator.page
        ).subscribe(() => this.recargar());

        this.homologacionesCountSub = this.dataSource.homologacionesCount$.subscribe(
            count => (this.paginator.length = count)
        );

        this.homologacionesSub = this.dataSource.homologaciones$.subscribe(
            (homologaciones: Homologacion[]) => {
                this.homologaciones = homologaciones;
            }
        );
    }

    ngOnDestroy(): void {
        if (this.homologacionesCountSub != null) {
            this.homologacionesCountSub.unsubscribe();
        }
        if (this.reloadTableSub != null)  {
            this.reloadTableSub.unsubscribe(); 
        }
        if (this.homologacionesSub != null) { 
            this.homologacionesSub.unsubscribe();
        }
    }

    /**
     * Envía la página y el tamaño de ésta al DataSource para solicitar nuevamente las homologaciones.
     */
    private recargar(): void {
        this.dataSource.getHomologacionesPage(
            this.paginator.pageIndex,
            this.paginator.pageSize,
            this.sort.active,
            this.sort.direction
        );
    }

    /**
     * Intenta atar la homologación seleccionada al formulario de creación/edición
     */
    public onClickEditarHomologacion(homologacion: Homologacion): void {
        this.editHomologacion.emit(homologacion);
    }

    /**
     * Solicita al usuario confirmar la acción. Si confirma, establece la homologación como activa.
     */
    public onClickActivarHomologacion(homologacion: Homologacion): void {
        const confirmationDialog = this.dialog.open(ConfirmationDialogComponent, {
            width: "600px",
            data: {
                titulo: "Activar Homologación",
                mensaje:
                    'Al hacer clic en "Sí", esta homologación podrá ser nuevamente usada en los procesos que requieran homologar datos. ¿Desea confirmar esta acción?',
                boton_si_clases: "btn-success",
                boton_no_clases: ""
            }
        });

        confirmationDialog.afterClosed().subscribe((confirmed: boolean) => {
            if (confirmed) {
                this.localSvc
                    .activar(homologacion)
                    .pipe(
                        catchError((err, observable) => {
                            return observable;
                        })
                    )
                    .subscribe(() => {
                        this.snackBar.open("Homologación activada.");
                        homologacion.vigencia = "Y";
                    });
            }
        });
    }

    /**
     * Solicita al usuario confirmar la acción. Si confirma, establece la homologación como inactiva.
     */
    public onClickDesactivarHomologacion(homologacion: Homologacion): void {
        const confirmationDialog = this.dialog.open(ConfirmationDialogComponent, {
            width: "600px",
            data: {
                titulo: "Desactivar Homologación",
                mensaje:
                    'Al hacer clic en "Sí", esta homologación no podrá ser usada en ningún proceso que requiera homologar datos. ¿Desea confirmar esta acción?',
                boton_si_clases: "btn-danger",
                boton_no_clases: ""
            }
        });

        confirmationDialog.afterClosed().subscribe((confirmed: boolean) => {
            if (confirmed) {
                this.localSvc
                    .desactivar(homologacion)
                    .pipe(
                        catchError((err, observable) => {
                            return observable;
                        })
                    )
                    .subscribe(() => {
                        this.snackBar.open("Homologación desactivada.");
                        homologacion.vigencia = "N";
                    });
            }
        });
    }

    public onClickExportar(): void {
        let options = {
            fieldSeparator: ";",
            quoteStrings: '"',
            decimalseparator: ",",
            showLabels: true,
            showTitle: false,
            useBom: true,
            noDownload: false,
            title: "",
            headers: [
                "Concepto",
                "Sistema 1",
                "Compañía 1",
                "Valor 1",
                "Sistema 2",
                "Compañía 2",
                "Valor 2"
            ],
            nullToEmptyString: true
        };

        let data = [];
        this.homologaciones.forEach(homologacion => {
            let estado = homologacion.vigencia === "Y" ? "Vigente" : "No vigente";
            data.push({
                concepto: homologacion.concepto,
                core1: homologacion.core1.nombre,
                socio1: homologacion.socio1.nombre,
                valor1: homologacion.valor1,
                core2: homologacion.core2.nombre,
                socio2: homologacion.socio2.nombre,
                valor2: homologacion.valor2
            });
        });

        let csv: Angular5Csv = new Angular5Csv(data, "Homologaciones", options);
    }

    public canExport(): boolean {
        return this.paginator && this.paginator.length > 0;
    }
}
