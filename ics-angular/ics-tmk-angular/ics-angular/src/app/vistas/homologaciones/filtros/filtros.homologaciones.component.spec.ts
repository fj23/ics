import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FiltrosHomologacionesComponent } from './filtros.homologaciones.component';

describe('LoginComponent', () => {
  let component: FiltrosHomologacionesComponent;
  let fixture: ComponentFixture<FiltrosHomologacionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FiltrosHomologacionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FiltrosHomologacionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
