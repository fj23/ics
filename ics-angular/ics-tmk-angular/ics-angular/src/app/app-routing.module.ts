import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomologacionesComponent } from './vistas/homologaciones/homologaciones.component';
import { EstructurasComponent } from './vistas/estructuras/estructuras.component';
import { FormularioEstructurasComponent } from './vistas/estructuras-crear/estructuras-crear.component';
import { TrazabilidadArchivosComponent } from './vistas/trazabilidad-archivos/trazabilidad.component';
import { PlantillasComponent } from './vistas/plantillas/plantillas.component';
import { PlantillasCrearComponent } from './vistas/plantillas-crear/plantillas-crear.component';
import { FormularioPlantillaEntradaComponent } from './vistas/plantillas-crear/entrada/plantilla-entrada.component';
import { ProcesosCargaComponent } from './vistas/procesos-carga/procesos-carga.component';
import { ArchivosCargarComponent } from './vistas/archivos-cargar/archivos-cargar.component';
import { ArchivosImportarComponent } from './vistas/archivos-importar/archivos-importar.component';
//import { ViewFormularioPlantillaComponent } from './plantillas/visualizacion/ver.formulario.plantillas.component';
import { PlantillaEdicionComponent } from './vistas/plantillas-editar/plantillas-editar.component';
import { LoginComponent } from './paginas/login/login.component';
import { P404Component } from './paginas/404/404.component';
import { P500Component } from './paginas/500/500.component';
import { CanDeactivateGuard } from "../services/routing/can-deactivate-guard";
import { CanActivateChildGuard } from '../services/routing/can-activate-child-guard';
import { NavegadorComponent } from './paginas/navegador/navegador.component';

const routes: Routes = [
    {
        path: '',
        redirectTo: '/',
        pathMatch: 'full'
    },
  {
    path: '404',
    component: P404Component,
    pathMatch: 'full'
  },
  {
    path: '500',
    component: P500Component,
    pathMatch: 'full'
  },
  { 
    path: 'login', 
    component: LoginComponent,
    pathMatch: 'full'
  },
  {
    path: '',
    canActivate: [CanActivateChildGuard],
    canActivateChild: [CanActivateChildGuard],
    component: NavegadorComponent,
    children: [
      { path: 'homologaciones', component: HomologacionesComponent, canDeactivate: [CanDeactivateGuard] },
      { path: 'estructuras', component: EstructurasComponent},
      { path: 'estructuras/nueva', component: FormularioEstructurasComponent, canDeactivate: [CanDeactivateGuard]},
      { path: 'estructuras/ver/est/:id', component: FormularioEstructurasComponent, canDeactivate: [CanDeactivateGuard]},
      { path: 'estructuras/ver/subest/:id', component: FormularioEstructurasComponent,canDeactivate: [CanDeactivateGuard]},
      { path: 'estructuras/editar/est/:id', component: FormularioEstructurasComponent,canDeactivate: [CanDeactivateGuard]},
      { path: 'estructuras/editar/subest/:id', component: FormularioEstructurasComponent,canDeactivate: [CanDeactivateGuard]},
      { path: 'trazabilidad', component: TrazabilidadArchivosComponent },
      { path: 'plantillas', component: PlantillasComponent },
      //{ path: 'plantillas/ver/:id', component: ViewFormularioPlantillaComponent },
      { path: 'plantilla/:id/:accion', component: PlantillaEdicionComponent, canDeactivate: [CanDeactivateGuard]},
      { path: 'plantillas/nueva', component: PlantillasCrearComponent, canDeactivate: [CanDeactivateGuard]},
      { path: 'plantillas/nueva/entrada', component: FormularioPlantillaEntradaComponent },
      { path: 'procesos_carga', component: ProcesosCargaComponent, canDeactivate: [CanDeactivateGuard]},
      //{ path: 'archivos', component: ArchivosComponent},
      { path: 'archivos/cargar', component: ArchivosCargarComponent, canDeactivate: [CanDeactivateGuard]},
      { path: 'cargaArchivos/nueva', component: ArchivosCargarComponent },
      { path: 'archivos/importar', component: ArchivosImportarComponent }
      ]
    },
  {
    path: '**', 
    component: P404Component,
    pathMatch: 'full' 
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule],
  providers: [CanActivateChildGuard]
})
export class AppRoutingModule { }
