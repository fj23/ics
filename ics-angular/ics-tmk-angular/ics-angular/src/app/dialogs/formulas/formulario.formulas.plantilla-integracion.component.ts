import { Component, OnInit, OnDestroy, Inject, Input } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef, MatTreeNestedDataSource, MatSnackBar, MatCheckboxChange } from '@angular/material';

import { NestedTreeControl } from '@angular/cdk/tree';
import { FormularioPlantillasService } from '../../../services/formulario-plantillas/formulario-plantillas.service';

import { Subscription } from 'rxjs';
import { FormulaOutputModel } from 'src/output_models/FormulaOutputModel';
import { ColumnaTrabajoIntegracion } from 'src/models/plantillas/integracion/ColumnaTrabajoIntegracion';
import { AtributoEnriquecimiento } from 'src/models/plantillas/AtributoEnriquecimiento';
import { FormulaIntegracion } from 'src/models/plantillas/integracion/FormulaIntegracion';
import { PlantillasHttpService } from 'src/http-services/plantillas.http-service';
import { FontawesomeIconsService } from 'src/services/fontawesome-icons/fontawesome-icons.service';

export interface FormulaDialogData {
    eventoNombre: string;
    socioId: number;
    formulaIn: FormulaIntegracion.IFormulaFactor;
    editMode: boolean;
    columnaNombre: string;
    columnasTrabajo: ColumnaTrabajoIntegracion[];
    atributosEnriquecimiento: AtributoEnriquecimiento[];
    permitirConstanteComoValor: boolean;
}

@Component({
    selector: 'app-formulario-formulas-plantilla-integracion',
    templateUrl: './formulario.formulas.plantilla-integracion.component.html',
    styleUrls: ['./formulario.formulas.plantilla-integracion.component.css'],
    providers: []
})
export class FormulaDialogColumnaPlantillaIntegracionComponent implements OnInit, OnDestroy {

    public formulasPlantillaIntegracionForm: FormGroup;
    public eventoNombre: string;
    public columnaNombre: string;
    public socioId: number;
    public columna: ColumnaTrabajoIntegracion;
    public constanteEsNumerica: boolean = false;
    public editMode: boolean = true;

    public columnasTrabajo: ColumnaTrabajoIntegracion[] = [];
    public atributosEnriquecimiento: AtributoEnriquecimiento[];
    public estructurasLogicas: FormulaIntegracion.FormulaFactor[];
    public operadoresFunciones: FormulaIntegracion.FormulaFactor[];

    public formula: FormulaIntegracion.FormulaFactor = null;
    public nodoSeleccionado: FormulaIntegracion.FormulaFactor;
    public treeControl: NestedTreeControl<FormulaIntegracion.FormulaFactor>;
    public treeDataSource: MatTreeNestedDataSource<FormulaIntegracion.FormulaFactor>;
    public formulaNotacionExcel: string = "";

    protected changeFuncionSub: Subscription;

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: FormulaDialogData,
        protected fb: FormBuilder,
        protected selfDialog: MatDialogRef<FormulaDialogColumnaPlantillaIntegracionComponent>,
        protected snackBar: MatSnackBar,
        protected localSvc: PlantillasHttpService,
        protected formSvc: FormularioPlantillasService,
        public icons: FontawesomeIconsService
    ) {

        this.eventoNombre = data.eventoNombre;
        this.columnaNombre = data.columnaNombre;
        this.editMode = data.editMode;
        this.socioId = data.socioId;
        if (data.columnasTrabajo) {
            this.columnasTrabajo = data.columnasTrabajo;
        }
        if (data.atributosEnriquecimiento) {
            this.atributosEnriquecimiento = data.atributosEnriquecimiento;
        }

        if (data.formulaIn) {
            
            //clona la formula para trabajar con ella
            //si fuera una referencia directa se guardaria siempre, aun si el usuario cancelara la edicion
            this.formula = FormulaIntegracion.cloneFormulaObject(data.formulaIn, this.columnasTrabajo, this.atributosEnriquecimiento);
        }

        this.actualizarFormula();

        this.estructurasLogicas = [
            new FormulaIntegracion.IfThenElse(undefined, undefined, undefined),
            new FormulaIntegracion.And(undefined),
            new FormulaIntegracion.Or(undefined),
            new FormulaIntegracion.EqualTo(undefined, undefined),
            new FormulaIntegracion.NotEqualTo(undefined, undefined),
            new FormulaIntegracion.HigherEqualThan(undefined, undefined),
            new FormulaIntegracion.HigherThan(undefined, undefined),
            new FormulaIntegracion.LowerEqualThan(undefined, undefined),
            new FormulaIntegracion.LowerThan(undefined, undefined)
        ];
        this.operadoresFunciones = [
            new FormulaIntegracion.Sum(undefined),
            new FormulaIntegracion.Subtraction(undefined),
            new FormulaIntegracion.Multiplication(undefined),
            new FormulaIntegracion.Division(undefined),
            new FormulaIntegracion.Ceil(undefined),
            new FormulaIntegracion.Round(undefined),
            new FormulaIntegracion.Floor(undefined),
            new FormulaIntegracion.Trunc(undefined),
            
            new FormulaIntegracion.Concat(undefined),
            new FormulaIntegracion.Substring(undefined),
            new FormulaIntegracion.Replace(undefined),
            new FormulaIntegracion.LPad(undefined),
            new FormulaIntegracion.RPad(undefined),
            new FormulaIntegracion.FncSplitFormula(undefined),
    
            new FormulaIntegracion.SysDate(),
            new FormulaIntegracion.DaysBetween(undefined, undefined),
            new FormulaIntegracion.MonthsBetween(undefined, undefined),
            new FormulaIntegracion.AddDays(undefined, undefined),
            new FormulaIntegracion.AddMonths(undefined, undefined),
    
            new FormulaIntegracion.ValorUFActual(),
            new FormulaIntegracion.ValorUFPrimerDia(),
            new FormulaIntegracion.ValorUFUltimoDia(),
            new FormulaIntegracion.ValorUFDia(undefined),
    
            new FormulaIntegracion.FncEnriquecimiento(undefined, undefined)
        ];

        this.formulasPlantillaIntegracionForm = this.fb.group({
            columnaTrabajo: [null],
            valorConstante: [''],
            estructuraLogica: [null],
            operadorFuncion: [null],
            atributoEnriquecimiento: [{value: null, disabled: true}]
        });
    }

    

    public get valorConstante() { return this.formulasPlantillaIntegracionForm.get("valorConstante"); }
    public get estructuraLogica() { return this.formulasPlantillaIntegracionForm.get("estructuraLogica"); }
    public get operadorFuncion() { return this.formulasPlantillaIntegracionForm.get("operadorFuncion"); }
    public get columnaTrabajo() { return this.formulasPlantillaIntegracionForm.get("columnaTrabajo"); }
    public get atributoEnriquecimiento() { return this.formulasPlantillaIntegracionForm.get("atributoEnriquecimiento"); }

    ngOnInit(): void {
        this.treeControl = new NestedTreeControl<FormulaIntegracion.FormulaFactor>(node => node.hijos);

        this.changeFuncionSub = this.operadorFuncion.valueChanges.subscribe(
            () => {
                if (this.operadorFuncion.value) {
                    if (this.operadorFuncion.value instanceof FormulaIntegracion.FncEnriquecimiento) {
                        this.atributoEnriquecimiento.enable();
                    }
                    else {
                        this.atributoEnriquecimiento.disable();
                    }
                }
            }
        );

        if (this.columnasTrabajo.length == 0) {
            this.columnaTrabajo.disable();
        }
    }

    ngOnDestroy(): void {
        if (this.changeFuncionSub) {
            this.changeFuncionSub.unsubscribe();
        }
    }

    isChildNode = (_: number, node: FormulaIntegracion.FormulaFactor) => {
        return 'padre' in node && node.padre;
    };
    isRootNode = (_: number, node: FormulaIntegracion.FormulaFactor) => {
        return (!this.isChildNode(_, node));
    };
    isParentNode = (_: number, node: FormulaIntegracion.FormulaFactor) => {
        return 'hijos' in node && node.hijos;
    };
    isPrimitiveNode = (_: number, node: FormulaIntegracion.FormulaFactor) => {
        return ('valor' in node && node.valor !== null);
    };
    isNullNode = (_: number, node: FormulaIntegracion.FormulaFactor) => {
        return (node instanceof FormulaIntegracion.NullLiteral);
    };
    isPredefinedNode = (_: number, node: FormulaIntegracion.FormulaFactor) => {
        return (node instanceof FormulaIntegracion.NullLiteral || node instanceof FormulaIntegracion.CampoIntegracion || node instanceof FormulaIntegracion.SysDate);
    };

    hasChildren = (_: number, node: FormulaIntegracion.FormulaFactor) => {
        return this.isParentNode(_, node) && node.hijos.length > 0;
    };
    hasInfiniteChildrenNodes = (_: number, node: FormulaIntegracion.FormulaFactor) => {
        return this.isParentNode(_, node) && node.maxHijos === Infinity;
    };

    isNullRootNode = (_: number, node: FormulaIntegracion.FormulaFactor) => {
        return (this.isRootNode(_, node) && this.isNullNode(_, node));
    };
    isPrimitiveRootNode = (_: number, node: FormulaIntegracion.FormulaFactor) => {
        return (this.isRootNode(_, node) && this.isPrimitiveNode(_, node) && !this.isPredefinedNode(_, node));
    };
    isPredefinedRootNode = (_: number, node: FormulaIntegracion.FormulaFactor) => {
        return (this.isRootNode(_, node) && this.isPredefinedNode(_, node));
    };
    isParentRootNode = (_: number, node: FormulaIntegracion.FormulaFactor) => {
        return (this.isRootNode(_, node) && this.isParentNode(_,node) && !this.hasInfiniteChildrenNodes(_, node));
    };
    isRootNodeWithInfiniteChildrenNodes = (_: number, node: FormulaIntegracion.FormulaFactor) => {
        return (this.isRootNode(_, node) && this.isParentNode(_,node) && this.hasInfiniteChildrenNodes(_, node));
    };
    isEnriquecimiento = (_: number, node: FormulaIntegracion.FormulaFactor) => {
        return (node instanceof FormulaIntegracion.FncEnriquecimiento);
    };
    isEnriquecimientoRootNode = (_: number, node: FormulaIntegracion.FormulaFactor) => {
        return (this.isRootNode(_, node) && this.isEnriquecimiento(_,node));
    };

    isPredefinedChildNode = (_: number, node: FormulaIntegracion.FormulaFactor) => {
        return (this.isChildNode(_, node) && this.isPredefinedNode(_, node));
    };
    isPrimitiveChildNode = (_: number, node: FormulaIntegracion.FormulaFactor) => {
        return (this.isChildNode(_, node) && this.isPrimitiveNode(_, node));
    };
    isParentChildNode = (_: number, node: FormulaIntegracion.FormulaFactor) => {
        return (this.isChildNode(_, node) && this.isParentNode(_,node) && !this.hasInfiniteChildrenNodes(_, node));
    };
    isChildNodeWithInfiniteChildrenNodes = (_: number, node: FormulaIntegracion.FormulaFactor) => {
        return (this.isChildNode(_, node) && this.isParentNode(_,node) && this.hasInfiniteChildrenNodes(_, node));
    };
    isEnriquecimientoChildNode = (_: number, node: FormulaIntegracion.FormulaFactor) => {
        return (this.isChildNode(_, node) && this.isEnriquecimiento(_,node.padre));
    };

    hasInfiniteSiblings = (_: number, node: FormulaIntegracion.FormulaFactor) => {
        return (this.isChildNode(_, node) && this.hasInfiniteChildrenNodes(_, node.padre));
    };

    isConditionStructChild = (_: number, node: FormulaIntegracion.FormulaFactor) => {
        return ('padre' in node && node.padre instanceof FormulaIntegracion.IfThenElse);
    };
    isCondition = (_: number, node: FormulaIntegracion.FormulaFactor) => {
        return (this.isConditionStructChild(_, node) && (<FormulaIntegracion.IfThenElse>node.padre).condition === node);
    };
    isIfTrue = (_: number, node: FormulaIntegracion.FormulaFactor) => {
        return (this.isConditionStructChild(_, node) && (<FormulaIntegracion.IfThenElse>node.padre).case1 === node);
    };
    isIfFalse = (_: number, node: FormulaIntegracion.FormulaFactor) => {
        return (this.isConditionStructChild(_, node) && (<FormulaIntegracion.IfThenElse>node.padre).case2 === node);
    };

    isCtrlExterno = (_: number, node: FormulaIntegracion.FormulaFactor) => {
        return (this.isEnriquecimientoChildNode(_, node) && (<FormulaIntegracion.FncEnriquecimiento>node.padre).ctrlExterno === node);
    };
    isPoliza = (_: number, node: FormulaIntegracion.FormulaFactor) => {
        return (this.isEnriquecimientoChildNode(_, node) && (<FormulaIntegracion.FncEnriquecimiento>node.padre).poliza === node);
    };
    isRut = (_: number, node: FormulaIntegracion.FormulaFactor) => {
        return (this.isEnriquecimientoChildNode(_, node) && (<FormulaIntegracion.FncEnriquecimiento>node.padre).rut === node);
    };

    labels = (_: number, node: FormulaIntegracion.FormulaFactor) => { 
        
        return (
            this.isCondition(_,node)?'[Condicion] ' : (
                this.isIfTrue(_,node)?'[Si se cumple] ' : (
                    this.isIfFalse(_,node)?'[Si no se cumple] ' : (
                        this.isCtrlExterno(_,node)?'[Ctrl externo] ' : (
                            this.isPoliza(_,node)?'[Poliza] ' : (
                                this.isRut(_,node)?'[Rut] ' : 
                                    ''
                            )
                        )
                    )
                )
            )
            + (
                this.isEnriquecimiento(_,node)?' [Enriquecer: ' + (<FormulaIntegracion.FncEnriquecimiento>node).atrEnriquecimiento.nombreColumna + ']' : ''
            )
        );

    }



    protected actualizarFormula(): void {
        if (this.treeDataSource && this.treeDataSource.data) {
            this.treeDataSource.data = null;
        }

        this.treeDataSource = new MatTreeNestedDataSource<FormulaIntegracion.FormulaFactor>();
        if (this.formula !== null) {
            this.treeDataSource.data = [this.formula];
            this.formulaNotacionExcel = this.formula.getNotacionExcel();
        }
    }

    protected asignarNuevoNodo(nodo: FormulaIntegracion.FormulaFactor): void {

        if (this.formula === null) { //si no existe un nodo arbol, lo crea
            this.formula = nodo;
            this.nodoSeleccionado = null;
            this.actualizarFormula();
        }
        else if (this.nodoSeleccionado) { //si se ha seleccionado un nodo para reemplazar

            if (this.nodoSeleccionado.padre && 'hijos' in this.nodoSeleccionado.padre) {

                //si el nodo posee un padre, desde este padre busca en el array de hijos, el indice al que corresponde el nodo seleccionado
                let padre = this.nodoSeleccionado.padre as FormulaIntegracion.FormulaParent;
                let hijoIndex = padre.hijos.findIndex((hijo) => { return hijo === this.nodoSeleccionado; })
                nodo.padre = padre;

                //reemplaza el nodo seleccionado por el nodo creado
                padre.setHijo(nodo, hijoIndex);
                nodo.padre = this.nodoSeleccionado.padre;
            }
            else {
                //si el nodo no posee un padre, es el nodo arbol y reemplaza toda la formula
                this.formula = nodo;
            }
            this.nodoSeleccionado = nodo;
            this.actualizarFormula();
        }
        else {
            this.snackBar.open("Debe seleccionar un nodo del árbol de la fórmula para asignarle el operador o función deseado.");
        }
    }

    public onToggleConstanteNumerica(): void {
        this.constanteEsNumerica = !this.constanteEsNumerica;
    }

    public onClickAgregarConstante(): void {

        if (!(this.constanteEsNumerica && !!!this.valorConstante.value)) {
            let instancia: FormulaIntegracion.FormulaFactor;
            let constante: string = this.valorConstante.value;

            if (this.constanteEsNumerica) {
                if (!isNaN(Number(this.valorConstante.value))) {
                    instancia = new FormulaIntegracion.NumberLiteral(Number(this.valorConstante.value));
                }
                else {
                    this.snackBar.open("El número no es válido.");
                }
            } else {
                let constanteUC = constante.toUpperCase();
                if (constanteUC === "NULL" || constanteUC === "NULO") {
                    instancia = new FormulaIntegracion.NullLiteral();
                } else {
                    instancia = new FormulaIntegracion.StringLiteral(constante);
                }
            }

            if (instancia) {
                this.asignarNuevoNodo(instancia);
                this.valorConstante.setValue("");
            }
        }
    }

    public onClickAgregarEstructuraLogica(): void {

        if (this.estructuraLogica.value) {

            //crea un nodo usando la clase obtenida de la propiedad 'tipo' ("IfThenElse", etc...)
            let estructuraLogica: FormulaIntegracion.FormulaFactor = this.estructuraLogica.value;
            let clase = FormulaIntegracion[estructuraLogica.tipo]; //namespace para elegir la clase correcta
            let instancia = new clase();

            this.asignarNuevoNodo(instancia);
            this.estructuraLogica.setValue(null);
        }
    }

    public onClickAgregarOperadorOFuncion(): void {

        if (this.operadorFuncion.value) {

            if (this.operadorFuncion.value instanceof FormulaIntegracion.FncEnriquecimiento) {

                if (!this.atributoEnriquecimiento.value) {
                    this.snackBar.open("Debe seleccionar un atributo de enriquecimiento.");
                }
                else {
                    let instancia = new FormulaIntegracion.FncEnriquecimiento(this.socioId, this.atributoEnriquecimiento.value);
        
                    this.asignarNuevoNodo(instancia);
                    this.atributoEnriquecimiento.setValue(null);
                    this.operadorFuncion.setValue(null);
                    this.atributoEnriquecimiento.disable();
                }
            }
            else {

                let operadorFuncion: FormulaIntegracion.FormulaFactor = this.operadorFuncion.value;
                let clase = FormulaIntegracion[operadorFuncion.tipo];
                let instancia = new clase();
    
                this.asignarNuevoNodo(instancia);
                this.operadorFuncion.setValue(null);
            }
        }
    }

    public onClickAgregarColumnaTrabajo(): void {

        if (this.columnaTrabajo.value) {
            let instancia = new FormulaIntegracion.CampoIntegracion(this.columnaTrabajo.value);

            this.asignarNuevoNodo(instancia);
            this.columnaTrabajo.setValue(null);
        }
    }

    public onClickNodoFormula(event: MatCheckboxChange, nodo: FormulaIntegracion.FormulaFactor): void {
        if (event.checked) {
            this.nodoSeleccionado = nodo;
        }
        else {
            this.nodoSeleccionado = null;
        }
    }

    public onClickAgregarHijo(nodo: FormulaIntegracion.FormulaParent): void {
        if (nodo.hijos.length < nodo.maxHijos) {
            let instancia = new FormulaIntegracion.NullLiteral();
            instancia.padre = nodo;
            nodo.addHijo(instancia);
            this.actualizarFormula();
        }
        else {
            this.snackBar.open("Este nodo ya posee la máxima cantidad posible de descendientes.");
        }
    }

    public onClickRemoverNodo(nodo: FormulaIntegracion.IFormulaFactor): void {
        let padre = nodo.padre;
        let nodoIndex = padre.hijos.findIndex((node: FormulaIntegracion.IFormulaFactor) => { return nodo === node; });
        padre.hijos.splice(nodoIndex, 1);
        this.actualizarFormula();
    }

    public onClickAceptarFormula(): void {
        let nodoInvalido: FormulaIntegracion.FormulaErrorNotifierWrapper = this.formula.getNodoInvalido();
        if (nodoInvalido !== null) {
            this.treeControl.collapseAll();
            nodoInvalido.origen.valido = false;

            while (nodoInvalido.origen) {
                this.treeControl.expand(nodoInvalido.origen);
                nodoInvalido.origen = nodoInvalido.origen.padre? nodoInvalido.origen.padre : null;
            }
            this.snackBar.open(nodoInvalido.mensaje);
        }
        else {
            let formulaOutput: FormulaOutputModel = this.formSvc.formulaToOutputModel(this.formula);

            this.localSvc.testFormulaIntegracion(
                formulaOutput
            ).subscribe(
                (valida: boolean) => {
                    if (!valida) {
                        this.snackBar.open("Su fórmula no es válida, revísela y vuelva a intentarlo.");
                    }
                    else {
                        this.selfDialog.close(this.formula);  //enviamos la formula tal cual
                    }
                }
            );
        }

    }
}
