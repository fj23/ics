import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MapearSubEstructuraPlantillaEntradaDialogComponent } from './subestructura.dialog.plantilla-entrada.component';

describe('SubEstructuraDialogArchivoEstructuradoPlantillaEntradaComponent', () => {
  let component: MapearSubEstructuraPlantillaEntradaDialogComponent;
  let fixture: ComponentFixture<MapearSubEstructuraPlantillaEntradaDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapearSubEstructuraPlantillaEntradaDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapearSubEstructuraPlantillaEntradaDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
