import { OnDestroy, Component } from '@angular/core';
import { AuthHttpService } from '../../../http-services/auth.http-service';
import { MatDialogRef } from '@angular/material';

@Component({
    selector: "session-notice-dialog",
    templateUrl: "session-notice-dialog.html"
})
export class SessionNoticeDialog implements OnDestroy {

    protected intervaloPopupInactividad: number = 1000;
    protected intervalSessionCheck;

    public seconds: number;

    constructor(
        private authSvc: AuthHttpService,
        public dialogRef: MatDialogRef<SessionNoticeDialog>
    ) {
        this.seconds = 60;
        this.intervalSessionCheck = setInterval(() => {
            this.seconds--;
            if (this.seconds < 1) {
                this.dialogRef.close();
                this.authSvc.logout("Sesión cerrada por inactividad", true);
            }
        }, this.intervaloPopupInactividad);
    }

    closeClick(): void {
        clearInterval(this.intervalSessionCheck);
        this.dialogRef.close();
    }

    ngOnDestroy(): void {
        clearInterval(this.intervalSessionCheck);
    }
}