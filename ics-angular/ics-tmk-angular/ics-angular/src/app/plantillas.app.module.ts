
import { NgModule } from "@angular/core";

import { FormularioPlantillaEntradaComponent } from "./vistas/plantillas-crear/entrada/plantilla-entrada.component";
import { ArchivoEstandarPlantillaEntradaComponent } from "./vistas/plantillas-crear/entrada/estandar/archivo-entrada-estandar.component";
import { ColumnaArchivoEstandarPlantillaEntradaComponent } from "./vistas/plantillas-crear/entrada/estandar/columna/columna-archivo-entrada.component";
import { ArchivoEstructuradoPlantillaEntradaComponent } from "./vistas/plantillas-crear/entrada/estructurada/archivo-entrada-estructurada.component";
import { EstructuraArchivoEstructuradoPlantillaEntradaComponent } from "./vistas/plantillas-crear/entrada/estructurada/detalles/estructura-archivo-entrada.component";
import { MapearSubEstructuraPlantillaEntradaDialogComponent } from "./dialogs/mapear-subestructura/subestructura.dialog.plantilla-entrada.component";
import { FormularioPlantillaIntegracionComponent } from "./vistas/plantillas-crear/integracion/plantilla-integracion.component";
import { ColumnaPlantillaIntegracionComponent } from "./vistas/plantillas-crear/integracion/columna/columna-trabajo-integracion.component";
import { FormulaDialogColumnaPlantillaIntegracionComponent } from "./dialogs/formulas/formulario.formulas.plantilla-integracion.component";
import { FormularioPlantillaSalidaComponent } from "./vistas/plantillas-crear/salida/plantilla-salida.component";
import { FormularioEstandarPlantillaSalidaComponent } from "./vistas/plantillas-crear/salida/estandar/salida-estandar.component";
import { FormularioEstructurasPlantillaSalidaComponent } from "./vistas/plantillas-crear/salida/estructurada/salida-estructurada.component";
import { FormularioTransformacionesColumnaEstructuraPlantillaSalidaComponent } from "./vistas/plantillas-crear/salida/estructurada/transformaciones-columna/transformaciones-columna-estructura.component";
import { PlantillasCrearComponent } from "./vistas/plantillas-crear/plantillas-crear.component";
import { ArchivoEstandarPlantillaEntradaEdicionComponent } from "./vistas/plantillas-editar/entrada/estandar/archivo-entrada-estandar-edicion.component";
import { ColumnaArchivoEstandarPlantillaEntradaEdicionComponent } from "./vistas/plantillas-editar/entrada/estandar/columna/columna-archivo-entrada-edicion.component";
import { ArchivoEstructuradoPlantillaEntradaEdicionComponent } from "./vistas/plantillas-editar/entrada/estructurada/archivo-entrada-estructurada-edicion.component";
import { DetallesEstructuraPlantillaEntradaEdicionComponent } from "./vistas/plantillas-editar/entrada/estructurada/detalles/estructura-archivo-entrada-edicion.component";
import { DialogoSubestructuraPlantillaEntradaEdicionComponent } from "./vistas/plantillas-editar/entrada/estructurada/dialogo-subestructura/dialogo-subestructura-plantilla-entrada-edicion.component";
import { PlantillaEntradaEdicionComponent } from "./vistas/plantillas-editar/entrada/plantilla-entrada-edicion.component";
import { ColumnaPlantillaIntegracionEdicionComponent } from "./vistas/plantillas-editar/integracion/propiedades/columna-trabajo-integracion-edicion.component";
import { PlantillaIntegracionEdicionComponent } from "./vistas/plantillas-editar/integracion/plantilla-integracion-edicion.component";
import { EstandarSalidaEdicionComponent } from "./vistas/plantillas-editar/salida/estandar/salida-estandar-edicion.component";
import { EstructuradaSalidaEdicionComponent } from "./vistas/plantillas-editar/salida/estructurada/salida-estructurada-edicion.component";
import { SubEstructuraDialogPlantillaSalidaEdicionComponent } from "./vistas/plantillas-editar/salida/estructurada/subestructura/sub-estructura-dialog-plantilla-salida-edicion.component";
import { PlantillaSalidaEdicionComponent } from "./vistas/plantillas-editar/salida/plantilla-salida-edicion.component";
import { PlantillaEdicionComponent } from "./vistas/plantillas-editar/plantillas-editar.component";
import { PlantillasComponent } from "./vistas/plantillas/plantillas.component";
import { TablaPlantillasComponent } from "./vistas/plantillas/tabla/tabla.plantillas.component";
import { FiltrosPlantillasComponent } from "./vistas/plantillas/filtros/filtros.plantillas.component";
import { AngularModule } from './compartido/angular.module';
import { MaterialModule } from './compartido/material.module';
import { AppRoutingModule } from './app-routing.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgSelectModule } from '@ng-select/ng-select';
import { DetallesEstructuraPlantillaSalidaComponent } from './vistas/plantillas-crear/salida/estructurada/detalles-estructura/detalles-estructura-salida.component';


const PLANTILLA_ENTRADA = [
    FormularioPlantillaEntradaComponent,
    ArchivoEstandarPlantillaEntradaComponent,
    ColumnaArchivoEstandarPlantillaEntradaComponent,
    ArchivoEstructuradoPlantillaEntradaComponent,
    EstructuraArchivoEstructuradoPlantillaEntradaComponent,
    MapearSubEstructuraPlantillaEntradaDialogComponent
];

const PLANTILLA_INTEGRACION = [
    FormularioPlantillaIntegracionComponent,
    ColumnaPlantillaIntegracionComponent
];

const PLANTILLA_SALIDA = [
    FormularioPlantillaSalidaComponent,
    FormularioEstandarPlantillaSalidaComponent,
    FormularioEstructurasPlantillaSalidaComponent,
    FormularioTransformacionesColumnaEstructuraPlantillaSalidaComponent
];

const PLANTILLA_CREACION = [
    PlantillasCrearComponent,
    ...PLANTILLA_ENTRADA,
    ...PLANTILLA_INTEGRACION,
    ...PLANTILLA_SALIDA
];
const PLANTILLA_EDICION_ENTRADA = [
    ArchivoEstandarPlantillaEntradaEdicionComponent,
    ColumnaArchivoEstandarPlantillaEntradaEdicionComponent,
    ArchivoEstructuradoPlantillaEntradaEdicionComponent,
    DetallesEstructuraPlantillaEntradaEdicionComponent,
    PlantillaEntradaEdicionComponent
];

const PLANTILLA_EDICION_INTEGRACION = [
    ColumnaPlantillaIntegracionEdicionComponent,
    PlantillaIntegracionEdicionComponent
];

const PLANTILLA_EDICION_SALIDA = [
    EstandarSalidaEdicionComponent,
    EstructuradaSalidaEdicionComponent,
    PlantillaSalidaEdicionComponent,
    DetallesEstructuraPlantillaSalidaComponent
];

const PLANTILLA_EDICION = [
    PlantillaEdicionComponent,
    ...PLANTILLA_EDICION_ENTRADA,
    ...PLANTILLA_EDICION_INTEGRACION,
    ...PLANTILLA_EDICION_SALIDA
];

const PLANTILLAS = [
    PlantillasComponent,
    TablaPlantillasComponent,
    FiltrosPlantillasComponent,
    ...PLANTILLA_CREACION,
    ...PLANTILLA_EDICION
];


@NgModule({
    imports: [
        AngularModule,
        MaterialModule,
        AppRoutingModule,
        FontAwesomeModule,
        NgSelectModule
    ],
    declarations: PLANTILLAS
})
export class PlantillasModule { }
