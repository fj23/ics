export class TipoDatoModelEnum {
    public static X={id: 100001,nombre:'No Aplica',vigencia:'N'};
    public static A={id: 100002,nombre:'Alfanumérico',vigencia:'Y'};
    public static N={id: 100003,nombre:'Numérico',vigencia:'Y'};
    public static F={id: 100004,nombre:'Fecha',vigencia:'Y'};
    public static S={id: 100005,nombre:'Subestructura',vigencia:'Y'}
}