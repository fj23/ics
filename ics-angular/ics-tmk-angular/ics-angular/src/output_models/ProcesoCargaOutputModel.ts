import { Plantilla } from 'src/models/plantillas/Plantilla';
import { DetalleProcesoCargaModel } from 'src/models/procesosCarga/DetalleProcesoCargaModel';

export class ProcesoCargaOutputModel {
  public id: number;
  public nombre: string;
  public correoSalida: string;
  public correoEmail: string;
  public correoHitos: string;
  public detalleProceso: DetalleProcesoCargaModel[];
  public plantillaEntradaSocio: Plantilla;
  public plantillaIntegracion: Plantilla;
  public plantillaSalidaSocio: Plantilla;
  public estado: number;
  public vigencia: string;
  public esSalidaAneto: boolean;
  public callCenter: number;
  public tipoArchivo: number;
}