import { MapeoIntegracionOutputModel } from './MapeoIntegracionOutputModel';
import { ColumnaDestinoIntegracionOutputModel } from './ColumnaDestinoIntegracionOutputModel';
import { ColumnaTrabajoIntegracionOutputModel } from './ColumnaTrabajoIntegracionOutputModel';
export class PlantillaIntegracionOutputModel {
    public id: number;
    public vigencia: string;
    public nombre: string;
    public version: string;
    public observaciones: string;
    public tipo: number;
    public socio: number;
    public evento: number;
    public core: number;
    public tipoSalida: number = null;
    public tipoExtension: number = null;
    public delimitadorColumnas: string = null;
    public incluirEncabezados: boolean = false;
    public columnasTrabajo: ColumnaTrabajoIntegracionOutputModel[] = null;
    public columnasArchivoSalida: ColumnaDestinoIntegracionOutputModel[] = null;
    public mapeos: MapeoIntegracionOutputModel[] = null;
    public codUsuario: string;
}