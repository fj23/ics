import { ColumnaMetadatosOutputModel } from './ColumnaMetadatosModel';

export class EstructuraAnetoOutputModel {
    id: number;
    codigo: string;
    descripcion: string;
    cardMin: number;
    cardMax: number;
    columnas: ColumnaMetadatosOutputModel[];
    vigencia: string;
}