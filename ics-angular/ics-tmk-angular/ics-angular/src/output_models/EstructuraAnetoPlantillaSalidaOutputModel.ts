import { FormulaOutputModel } from './FormulaOutputModel';
export class ColumnaEstructuraPlantillaSalidaOutputModel {
    public idDetalleEstructura: number;
    public idEstructura: number;
    public idSubEstructura: number;
    public subestructura: EstructuraAnetoPlantillaSalidaOutputModel;
    public codigoColumna: string;
    public descripcionColumna: string;
	public idAtributoDiccionario: number;
	public valorFijo: string;
	public idTipoHomologacion: number;
	public idTipoFormato: number;
	public formula: FormulaOutputModel;
	public idPlantilla: number;
	public ordenHomologacion: number;
	public ordenFormato: number;
	public ordenFormula: number;
}

export class EstructuraAnetoPlantillaSalidaOutputModel {
    public id: number;
    public codigo: string;
    public descripcion: string;
    public columnas: ColumnaEstructuraPlantillaSalidaOutputModel[];
}