export class  ColumnaMetadatosOutputModel {
    public id: number;
    public codigo: string;
    public descripcion: string;
    public orden: number;
    public tipoDato: number;
    public esObligatorio: boolean;
    public posicionInicial: number;
    public posicionFinal: number;
    public subEstructura: number;
    public vigencia: string;
    public mapeada: boolean;
    public largo: number;
}