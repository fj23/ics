export class HomologacionOutputModel {
    public idHomologacion: number;
    public socio1: number;
    public socio2: number;
    public core1: number;
    public core2: number;
    public concepto: string;
    public vigencia: string;   
}