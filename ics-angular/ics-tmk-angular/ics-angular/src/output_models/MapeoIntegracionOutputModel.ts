export class MapeoIntegracionOutputModel {
    public id: number = 0;
    public columnaOrigen: number;
    public columnaDestino: number;
    public esObligatorio: boolean;
    public columnaTrabajoOrden: number;

    public idAtributoUser: number;
}