export class CargaArchivoOutputModel {
    public id: number;
    public nombre: string;
    public fechaRecepcion: Date;
    public codigo: number = null;
    public observacion: string;
    public nombreArchivo: string;
    public extensionArchivo: string;
    public idProceso : number;
}