export class ColumnaDestinoIntegracionOutputModel {
    public id: number;
    public nombre: string;
    public orden: number;
    public posicionInicial: number = 0;
    public posicionFinal: number = 0;
    public separarArchivo: boolean;
    public incluirEnArchivo: boolean;
    public indiceMapeoOrigen: number;

    public flgVisible: string;
    
    public columnaOrigen?: number;
}