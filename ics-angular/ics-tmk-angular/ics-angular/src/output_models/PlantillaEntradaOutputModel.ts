import { ArchivoEntradaOutputModel } from './ArchivoEntradaOutputModel';
import { MapeoEntradaOutputModel } from './MapeoEntradaOutputModel';
import { UnionArchivosOutputModel } from './UnionArchivosOutput';


export class PlantillaEntradaOutputModel {
    public id: number;
    public vigencia: string;
    public nombre: string;
    public version: string;
    public observaciones: string;
    public tipo: number;
    public socio: number;
    public evento: number;
    public core: number;
    public fechaCreacion: string;
    public tipoExtension: number;
    public archivos: ArchivoEntradaOutputModel[];
    public uniones: UnionArchivosOutputModel[];
    public columnasDestino: MapeoEntradaOutputModel[];
    public codUsuario: string;
}