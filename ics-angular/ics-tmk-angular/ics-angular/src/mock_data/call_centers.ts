import { CallCenter } from '../models/compartido/CallCenter';

export const MOCK_CALL_CENTERS: CallCenter[] = [
    { id: 0, nombre: "SOEX", vigencia: "Y" },
    { id: 1, nombre: "SOEC", vigencia: "Y" }
];