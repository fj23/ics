import { Injectable } from "@angular/core";
import {
  HttpClient,
  HttpResponse
} from "@angular/common/http";

import { Subject } from "rxjs";


import { BaseHttpService } from "./base.http-service";
import { MatSnackBar } from "@angular/material";
import { Router } from "@angular/router";
import { Credenciales } from '../models/compartido/Credenciales';

interface Role {
  name: string;
  authority: string;
}

interface tokenExtraction extends HttpResponse<any> {
  accessToken: string;
  tokenType: string;
  user: {
    email: string;
    roles: Role[];
    username: string;
    versionBackend: string;
    expires: number;
    inactivityNotice: number;
  };
}

/**
 * Servicio que obtiene datos de sesión y realiza solicitudes de inicio de sesión
 */
@Injectable({ providedIn: "root" })
export class AuthHttpService extends BaseHttpService {
  private urls = {
    auth: "auth",
    salir: "logout"
  };

  public logStateChange = new Subject<boolean>();

  constructor(
    http: HttpClient,
    private snackBar: MatSnackBar,
    private router: Router
  ) {
    super(http);
  }

  public extractToken(res: tokenExtraction) {
    localStorage.setItem("token", res.accessToken);
    localStorage.setItem("username", res.user.username);
    localStorage.setItem("versionBackend", res.user.versionBackend);
    localStorage.setItem("inactivityNotice", res.user.inactivityNotice + "");
    let aux: string = "";
    res.user.roles.forEach(role => {
      if (role.name.indexOf("EVENT_") === -1) {
        aux += "F";
        aux += role.name;
        aux += "R";
        aux += ";";
      }
    });
    localStorage.setItem("roles", aux);
    Storage;
    return res.user;
  }

  /**
   * Intenta iniciar sesión con las credenciales
   * @param loginModel
   */
  validarLogin(loginModel: Credenciales) {
    return this.http.post(this.getServiceURL() + this.urls.auth, loginModel);
  }

  /**
   * Elimina los datos de sesión del usuario.
   */
  public logout(msg: string, force: boolean): void {
    let token = localStorage.getItem("token");
    if (token != null) {
      this.http.get(this.getServiceURL() + this.urls.salir + `/${token}`).subscribe();
    }
    localStorage.clear();
    sessionStorage.clear();
    if (force) sessionStorage.setItem("force_exit", "T");
    this.snackBar.open(msg);
    this.logStateChange.next(false);
    this.router.navigateByUrl("/login");
  }
}
