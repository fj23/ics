import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, BehaviorSubject, Subject } from 'rxjs';

import { PaginaRegistros } from 'src/models/compartido/PaginaRegistros';
import { Homologacion } from 'src/models/homologaciones/Homologacion';
import { FiltrosHomologacionesModel } from 'src/models/filters/FiltrosHomologacionesModel';

import { BaseHttpService } from './base.http-service';

@Injectable({providedIn: 'root'})
export class HomologacionesHttpService extends BaseHttpService {

  private urls = {
    listar: 'homologaciones',
    crear: 'homologaciones/nueva',
    actualizar: 'homologaciones/actualizar',
    activar: 'homologaciones/activar',
    desactivar: 'homologaciones/desactivar',
    conceptos: 'homologaciones/conceptos',
    crearMultiple: 'homologaciones/nuevo_grupo',
    tipos: 'homologaciones/tipos',
  };

  private filtros: FiltrosHomologacionesModel;

  /**
   * Transmite el evento de cambio de filtros.
   */
  private filtrosChangeSubject = new Subject<void>();
  
  /**
   * Transmite el evento de cambio de filtros.
   */
  public newHomologacionSubject = new Subject<void>();

  /**
   * Recibe y transmite la homologación actualmente en edición.
   */
  public editingTargetSubject = new BehaviorSubject<Homologacion>(null);
  
  //observables
  public filtrosChange$ = this.filtrosChangeSubject.asObservable();

  constructor(http: HttpClient) {
    super(http);
  }

  public setFiltros(filtros: FiltrosHomologacionesModel): void {
    this.filtros = filtros;
    this.filtrosChangeSubject.next();
  }

  public getFiltros(): FiltrosHomologacionesModel {
    if (!!!this.filtros) {
      this.filtros = new FiltrosHomologacionesModel();
    }
    return this.filtros;
  }

  public filtrosRefresh(): void {
    this.filtrosChangeSubject.next();
  }

  /**
   * Transforma los filtros activos en un objeto de parámetros adjuntables a una petición HTTP.
   */
  private modelToHttpParameters( ): HttpParams {
    let parameters = new HttpParams();
    
    if (this.filtros.concepto){
      parameters = parameters.set("concepto", this.filtros.concepto);
    }

    if (this.filtros.socio1 > 0){
      parameters = parameters.set("socio1", this.filtros.socio1.toString());
    }

    if (this.filtros.sistema1 > 0){
      parameters = parameters.set("sistema1", this.filtros.sistema1.toString());
    }

    if (this.filtros.socio2 > 0){
      parameters = parameters.set("socio2", this.filtros.socio2.toString());
    }

    if (this.filtros.sistema2 > 0){
      parameters = parameters.set("sistema2", this.filtros.sistema2.toString());
    }

    if (this.filtros.vigencia){
      parameters = parameters.set("vigencia", this.filtros.vigencia);
    }

    return parameters;
  }
  
  /**
   * Solicita y obtiene los (distintos) conceptos homologados en la base de datos.
   */
  public getConceptosHomologacion(): Observable<String[]> {
    return this.http.get<String[]>(
      this.getServiceURL() + this.urls.conceptos
    );
  }

  /**
   * Solicita y obtiene las homologaciones guardadas en la base de datos.
   */
  public listar(): Observable<PaginaRegistros<Homologacion>> {
    if (!!!this.filtros) {
      this.filtros = new FiltrosHomologacionesModel();
    }
    let paging = "/page/"+this.filtros.pageIndex;
    let pageSizing = "/size/"+this.filtros.pageSize;
    let sortOrder = "";
    if (this.filtros.sortColumn && this.filtros.sortOrder) {
      sortOrder = "/sort/"+this.filtros.sortColumn+"/"+this.filtros.sortOrder;
    }
    let params = this.modelToHttpParameters();
  

    return this.http.get<PaginaRegistros<Homologacion>>( 
      this.getServiceURL() + this.urls.listar + paging + pageSizing + sortOrder + "/filters", 
      { params: params } 
    );
  }

  /**
   * Hace una petición POST para insertar una homologación en la base de datos.
   * @param model El objeto que va a intentar ser insertado.
   */
  public crear(model: Homologacion): Observable<any> {
    return this.http.post( 
      this.getServiceURL() + this.urls.crear, 
      model
    );
  }

  /**
   * Hace una petición PUT para actualizar una homologación en la base de datos.
   * @param model El objeto que va a intentar ser actualizado.
   */
  public actualizar(model: Homologacion): Observable<any> {
    return this.http.put( 
      this.getServiceURL() + this.urls.actualizar, 
      model 
    );
  }

  /**
   * Hace una petición POST para activar una subestructura de la base de datos.
   * @param model El objeto que va a intentar ser actualizado.
   */
  public activar(homologacion: Homologacion): Observable<any> {
    return this.http.post( 
      this.getServiceURL() + this.urls.activar, 
      homologacion
    );
  }

  /**
   * Hace una petición POST para desactivar una subestructura de la base de datos.
   * @param model El objeto que va a intentar ser actualizado.
   */
  public desactivar(homologacion: Homologacion): Observable<any> {
    return this.http.post( 
      this.getServiceURL() + this.urls.desactivar, 
      homologacion
    );
  }

  public crearMultiple(homologaciones: Homologacion[]): Observable<string[]> {
    return this.http.post<string[]>(
      this.getServiceURL() + this.urls.crearMultiple,
      homologaciones
    )
  }


  public getTipos(socioId: number, coreId: number): Observable<Homologacion[]> {
    return this.http.get<Homologacion[]>(
      this.getServiceURL() + this.urls.tipos + "/" + socioId + "/" + coreId
    );
  }
}
