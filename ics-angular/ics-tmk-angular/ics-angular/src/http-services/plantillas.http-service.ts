import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';

import { BaseHttpService } from 'src/http-services/base.http-service';

import { Plantilla } from 'src/models/plantillas/Plantilla';
import { FiltrosPlantillasModel } from 'src/models/filters/FiltrosPlantillasModel';
import { ColumnaDestinoEntrada } from 'src/models/plantillas/entrada/ColumnaDestinoEntrada';
import { AtributoEnriquecimiento } from 'src/models/plantillas/AtributoEnriquecimiento';
import { ColumnaDestinoIntegracion } from 'src/models/plantillas/integracion/ColumnaDestinoIntegracion';

import { PlantillaIntegracionOutputModel } from 'src/output_models/PlantillaIntegracionOutputModel';
import { PlantillaSalidaOutputModel } from 'src/output_models/PlantillaSalidaOutputModel';
import { PlantillaEntradaOutputModel } from 'src/output_models/PlantillaEntradaOutputModel';
import { TipoExtension } from 'src/models/compartido/TipoExtension';
import { TipoDato } from '../models/compartido/TipoDato';
import { PlantillaEModel } from 'src/models/edicion/plantillas/PlantillaEModel';
import { FormulaOutputModel } from '../output_models/FormulaOutputModel';
import { AtributoDiccionarioEstructuraAnetoSalida } from '../models/plantillas/salida/estructura/AtributoDiccionarioEstructuraAnetoSalida';
import { SistemaCore } from '../models/compartido/SistemaCore';
import { EstructuraAnetoPlantillaSalidaOutputModel } from '../output_models/EstructuraAnetoPlantillaSalidaOutputModel';
import { PaginaRegistros } from '../models/compartido/PaginaRegistros';

@Injectable({ providedIn: 'root' })
export class PlantillasHttpService extends BaseHttpService {


    private urls = {
        listar: 'plantillas',
        listarNombres: 'plantillas/nombresAll',
        listarTiposExtensionEstandar: 'plantillas/tipos_salida_estandar',

        detalles: 'plantillas/ver',
        activar: 'plantillas/activar',
        desactivar: 'plantillas/desactivar',
        version: 'plantillas/version',

        listarColumnasDestinoEntradaDisponiblesUsuario: 'plantillas/columnas_destino_disponibles_usuario',
        listarTiposDatoColumnaEstandar: 'plantillas/entrada/tipos_datos',
        listarPlantillasEntradaVigentes: 'plantillas/entrada/vigentes',
        almacenarPlantillaEntrada: 'plantillas/entrada/nueva',

        listarTiposExtensionIntegracion: 'plantillas/tipos_extension_intg',
        listarAtributosEnriquecimiento: 'plantillas/atributos_enriquecimiento',
        listarColumnasDestinoIntegracionDisponibles: 'plantillas/integracion/atributos_destino',
        listarColumnasDestinoIntegracionPlantillaEntrada: 'plantillas/integracion/atributos_origen_plantilla_entrada',
        verificarFormulaIntegracion: 'plantillas/integracion/verificar_formula',
        almacenarPlantillaIntegracion: 'plantillas/integracion/nueva',

        listarAtributosDiccionarioSalida: 'plantillas/salida/atributos_estructuras',
        listarCoresEstructurasSalida: 'plantillas/salida/cores_estructuras',
        listarEstructurasPlantillaSalida: 'plantillas/salida/estructuras',
        almacenarSalida: 'plantillas/salida/nueva'
    };


    /**
     * Objeto que encapsula los filtros activos.
     */
    private filtros: FiltrosPlantillasModel;

    /**
     * Subject que avisa cuando los filtros varían.
     */
    private filtrosChangeSubject = new Subject<void>();

    /**
     * Observable al que otros componentes pueden suscribirse para poder 'observar' los eventos de cambio de filtros.
     */
    public filtrosChange$ = this.filtrosChangeSubject.asObservable();

    constructor(http: HttpClient) {
        super(http);
    }

    /**
     * Obtiene los filtros.
     */
    public getFiltros(): FiltrosPlantillasModel {
        if (!!!this.filtros) {
            this.filtros = new FiltrosPlantillasModel();
        }
        return this.filtros;
    }

    /**
     * Establece los filtros y gatilla un evento.
     * @param filtros El objeto de filtros a establecer.
     */
    public setFiltros(filtros: FiltrosPlantillasModel): void {
        this.filtros = filtros;
        this.filtrosChangeSubject.next();
    }

    public refreshFiltros(): void {
        this.filtrosChangeSubject.next();
    }

    /**
     * Transforma los filtros activos en un objeto de parámetros adjuntables a una petición HTTP.
     */
    private modelToHttpParameters(): HttpParams {
        let parameters = new HttpParams();

        if (this.filtros.socio > 0) {
            parameters = parameters.set("socio", this.filtros.socio.toString());
        }

        if (this.filtros.evento > 0) {
            parameters = parameters.set("evento", this.filtros.evento.toString());
        }

        if (this.filtros.tipo > 0) {
            parameters = parameters.set("tipo", this.filtros.tipo.toString());
        }

        if (this.filtros.nombre) {
            parameters = parameters.set("nombre", this.filtros.nombre);
        }

        if (this.filtros.vigencia) {
            parameters = parameters.set("vigencia", this.filtros.vigencia);
        }

        return parameters;
    }


    public listar(): Observable<PaginaRegistros<Plantilla>> {
        
        if (!!!this.filtros) {
            this.filtros = new FiltrosPlantillasModel();
        }
        let paging = "/page/" + this.filtros.pageIndex;
        let pageSizing = "/size/" + this.filtros.pageSize;
        let sortOrder = "";
        if (this.filtros.sortColumn && this.filtros.sortOrder) {
            sortOrder = "/sort/" + this.filtros.sortColumn + "/" + this.filtros.sortOrder;
        }
        let params = this.modelToHttpParameters();

        return this.http.get<PaginaRegistros<Plantilla>>(
            this.getServiceURL() + this.urls.listar + paging + pageSizing + sortOrder + "/filters", 
            { params: params }
        );
    }

    public activar(plantilla: Plantilla, desactivarProceso: boolean):
        Observable<HttpResponse<void>> {
        return this.http.get<void>(
            this.getServiceURL() + this.urls.activar + "/" + plantilla.id + "/" + desactivarProceso,
            { observe: 'response' }
        );
    }

    public desactivar(plantilla: Plantilla, desactivarProceso: boolean):
        Observable<HttpResponse<void>> {
        return this.http.get<void>(
            this.getServiceURL() + this.urls.desactivar + "/" + plantilla.id + "/" + desactivarProceso,
            { observe: 'response' }
        );
    }

    public getVersion(plantilla: Plantilla):
        Observable<number> {
        return this.http.post<number>(
            this.getServiceURL() + this.urls.version,
            {
                nombre: plantilla.nombre,
                socio: plantilla.socio.id,
                evento: plantilla.evento.id,
                tipo: plantilla.tipo.id
            }
        );
    }

    public getNombresPlantilla(socio?: number, evento?: number): Observable<string[]> {
        let parameters = new HttpParams();

        if (socio) {
            parameters = parameters.set("idSocio", socio.toString());
        }

        if (evento) {
            parameters = parameters.set("idEvento", evento.toString());
        }

        return this.http.get<string[]>(
            this.getServiceURL() + this.urls.listarNombres, { params: parameters }
        );
    }

    public getTiposDatoColumnaEstandar() {
        return this.http.get<TipoDato[]>(
            this.getServiceURL() + this.urls.listarTiposDatoColumnaEstandar
        );
    }

    public getColumnasDestinoEntradaDisponiblesUsuarios(evento: number): Observable<ColumnaDestinoEntrada[]> {
        if (evento) {
            return this.http.get<ColumnaDestinoEntrada[]>(
                this.getServiceURL() + this.urls.listarColumnasDestinoEntradaDisponiblesUsuario + "/" + evento
            );
        }
        else {
            return this.http.get<ColumnaDestinoEntrada[]>(
                this.getServiceURL() + this.urls.listarColumnasDestinoEntradaDisponiblesUsuario
            );
        }
    }

    public getPlantillasEntrada(idEvento: number, idSocio: number): Observable<Plantilla[]> {
        let params: HttpParams = new HttpParams()
            .append("evento", String(idEvento))
            .append("socio", String(idSocio));

        return this.http.get<Plantilla[]>(
            this.getServiceURL() + this.urls.listarPlantillasEntradaVigentes + "", {
                params: params
            }
        );
    }

    public getColumnasOrigenIntegracionDesdePlantillaEntrada(plantillaId: number): Observable<ColumnaDestinoEntrada[]> {
        return this.http.get<ColumnaDestinoEntrada[]>(
            this.getServiceURL() + this.urls.listarColumnasDestinoIntegracionPlantillaEntrada + "/" + plantillaId
        );
    }

    public almacenarPlantillaEntrada(plantilla: PlantillaEntradaOutputModel): Observable<any> {
        return this.http.post(
            this.getServiceURL() + this.urls.almacenarPlantillaEntrada,
            plantilla
        );
    }

    public almacenarPlantillaIntegracion(plantilla: PlantillaIntegracionOutputModel): Observable<any> {
        return this.http.post(
            this.getServiceURL() + this.urls.almacenarPlantillaIntegracion,
            plantilla
        );
    }

    public almacenarPlantillaSalida(plantilla: PlantillaSalidaOutputModel): Observable<any> {
        return this.http.post(
            this.getServiceURL() + this.urls.almacenarSalida,
            plantilla
        );
    }

    public getTiposExtensionEstandar(): Observable<TipoExtension[]> {
        return this.http.get<TipoExtension[]>(
            this.getServiceURL() + this.urls.listarTiposExtensionEstandar
        );
    }

    public getAtributosEnriquecimiento(coreId: number): Observable<AtributoEnriquecimiento[]> {
        return this.http.get<AtributoEnriquecimiento[]>(
            this.getServiceURL() + this.urls.listarAtributosEnriquecimiento + "/" + coreId
        );
    }

    public getColumnasDestinoIntegracionDisponibles(eventoId: number): Observable<ColumnaDestinoIntegracion[]> {
        return this.http.get<ColumnaDestinoIntegracion[]>(
            this.getServiceURL() + this.urls.listarColumnasDestinoIntegracionDisponibles + "/" + eventoId
        );
    }

    public getPlantillaByIdEdicion(idPlantilla: number): Observable<PlantillaEModel> {
        return this.http.get<PlantillaEModel>(
            this.getServiceURL() + this.urls.detalles + "/" + idPlantilla
        );
    }

    public testFormulaIntegracion(formula: FormulaOutputModel): Observable<boolean> {
        return this.http.post<boolean>(
            this.getServiceURL() + this.urls.verificarFormulaIntegracion,
            formula
        );
    }



    public getAtributosDiccionarioSalida(): Observable<AtributoDiccionarioEstructuraAnetoSalida[]> {
        return this.http.get<AtributoDiccionarioEstructuraAnetoSalida[]>(
            this.getServiceURL() + this.urls.listarAtributosDiccionarioSalida
        );
    }

    public getCoresEstructurasSalida(): Observable<SistemaCore[]> {
        return this.http.get<SistemaCore[]>(
            this.getServiceURL() + this.urls.listarCoresEstructurasSalida
        );
    }

    public getEstructurasPlantillaSalida(idPlantilla: number): Observable<EstructuraAnetoPlantillaSalidaOutputModel[]> {
        const params: HttpParams = new HttpParams()
            .append("idPlantilla", idPlantilla.toString());

        return this.http.get<EstructuraAnetoPlantillaSalidaOutputModel[]>(
            this.getServiceURL() + this.urls.listarEstructurasPlantillaSalida, { params: params }
        );
    }
}
