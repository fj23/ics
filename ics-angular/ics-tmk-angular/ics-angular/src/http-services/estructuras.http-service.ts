import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';

import { BaseHttpService } from './base.http-service';

import { PaginaRegistros } from 'src/models/compartido/PaginaRegistros';
import { EstructuraAneto } from 'src/models/estructuras/EstructuraAneto';
import { FiltrosEstructurasModel } from 'src/models/filters/FiltrosEstructurasModel';
import { EstructuraAnetoOutputModel } from '../output_models/EstructuraAnetoOutputModel';
import { ColumnaMetadatosOutputModel } from '../output_models/ColumnaMetadatosModel';

/** 
 * Servicio que obtiene datos y realiza operaciones confinadas al módulo de estructuras.
 */
@Injectable({ providedIn: 'root' })
export class EstructurasHttpService extends BaseHttpService {

  //TODO: trasladar todos los metodos de subestructuras a un servicio nuevo y especifico

  private urls = {
    listar: 'estructuras',
    buscar: 'estructuras/get',
    existe: 'estructuras/exists',
    crear: 'estructuras/nueva',
    actualizar: 'estructuras/actualizar',
    activar: 'estructuras/activar',
    desactivar: 'estructuras/desactivar',
    todos: 'estructuras/all'
  };

  /**
   * Objeto que encapsula los filtros activos.
   */
  private filtros: FiltrosEstructurasModel;

  /**
   * Subject que avisa cuando los filtros varían.
   */
  private filtrosChangeSubject = new Subject<void>();

  /**
   * Observable al que otros componentes pueden suscribirse para poder 'observar' los eventos de cambio de filtros.
   */
  public filtrosChange$ = this.filtrosChangeSubject.asObservable();


  constructor(http: HttpClient) {
    super(http);
  }

  /**
   * Obtiene los filtros.
   */
  public getFiltros(): FiltrosEstructurasModel {
    if (!!!this.filtros) {
      this.filtros = new FiltrosEstructurasModel();
    }
    return this.filtros;
  }

  /**
   * Establece los filtros y gatilla un evento.
   * @param filtros El objeto de filtros a establecer.
   */
  public setFiltros(filtros: FiltrosEstructurasModel): void {
    this.filtros = filtros;
    this.filtrosChangeSubject.next();
  }

  /**
   * Transforma los filtros activos en un objeto de parámetros adjuntables a una petición HTTP.
   */
  public modelToHttpParameters(): HttpParams {
    let parameters = new HttpParams();

    if (this.filtros.codigo) {
      parameters = parameters.set("codigo", this.filtros.codigo);
    }

    if (this.filtros.nombre) {
      parameters = parameters.set("nombre", this.filtros.nombre);
    }

    return parameters;
  }

  private convertToOutput(model: EstructuraAneto): EstructuraAnetoOutputModel {

    let columnasOutput: ColumnaMetadatosOutputModel[] = [];

    for (let i = 0; i < model.columnas.length; i++) {
      const col = model.columnas[i];
      let colOutput: ColumnaMetadatosOutputModel = {
        id: col.id ? col.id : 0,
        codigo: col.codigo,
        descripcion: col.descripcion,
        orden: col.orden,
        tipoDato: col.tipoDato.id,
        subEstructura: col.subEstructura ? col.subEstructura.id : 0,
        vigencia: col.vigencia,
        esObligatorio: null,
        // esLlave: null,
        posicionInicial: 0,
        posicionFinal: 1,
        mapeada: null,
        largo: col.largo
      };
      columnasOutput.push(colOutput);
    }

    const outputObject: EstructuraAnetoOutputModel = {
      id: model.id ? model.id : 0,
      codigo: model.codigo,
      descripcion: model.descripcion,
      cardMin: model.cardMin,
      cardMax: model.cardMax,
      columnas: columnasOutput,
      vigencia: model.vigencia
    };

    return outputObject;
  }

  /**
   * Hace una petición GET para obtener las estructuras guardadas en la base de datos.
   */
  public listar(): Observable<PaginaRegistros<EstructuraAneto>> {
    if (!!!this.filtros) {
      this.filtros = new FiltrosEstructurasModel();
    }
    let paging = "/page/" + this.filtros.pageIndex;
    let pageSizing = "/size/" + this.filtros.pageSize;
    let sortOrder = "";
    if (this.filtros.sortColumn && this.filtros.sortOrder) {
      sortOrder = "/sort/" + this.filtros.sortColumn + "/" + this.filtros.sortOrder;
    }
    let params = this.modelToHttpParameters();

    return this.http.get<PaginaRegistros<EstructuraAneto>>(
      this.getServiceURL() + this.urls.listar + paging + pageSizing + sortOrder + "/filters",
      { params: params }
    );
  }

  public buscar(id: number): Observable<EstructuraAneto> {
    return this.http.get<EstructuraAneto>(
      this.getServiceURL() + this.urls.buscar + "/" + id
    );
  }

  public existe(est: EstructuraAneto): Observable<boolean> {
    return this.http.put<boolean>(
      this.getServiceURL() + this.urls.existe, est
    );
  }

  /**
   * Hace una petición POST para insertar una estructura en la base de datos.
   * @param model El objeto que va a intentar ser insertado.
   */
  public crear(model: EstructuraAneto): Observable<any> {

    let outputModel: EstructuraAnetoOutputModel = this.convertToOutput(model);

    return this.http.post(
      this.getServiceURL() + this.urls.crear,
      outputModel
    );
  }

  /**
   * Hace una petición PUT para actualizar una estructura en la base de datos.
   * @param model El objeto que va a intentar ser insertado.
   */
  public actualizar(model: EstructuraAneto): Observable<any> {

    let outputModel: EstructuraAnetoOutputModel = this.convertToOutput(model);

    return this.http.put(
      this.getServiceURL() + this.urls.actualizar,
      outputModel
    );
  }

  /**
   * Hace una petición PUT para desactivar una subestructura de la base de datos.
   * @param model El objeto que va a intentar ser actualizado.
   */
  public activar(estructura: EstructuraAneto): Observable<any> {
    return this.http.get(
      this.getServiceURL() + this.urls.activar + "/" + estructura.id
    );
  }

  /**
   * Hace una petición PUT para desactivar una subestructura de la base de datos.
   * @param model El objeto que va a intentar ser actualizado.
   */
  public desactivar(estructura: EstructuraAneto): Observable<any> {
    return this.http.get<string>(
      this.getServiceURL() + this.urls.desactivar + "/" + estructura.id
    );
  }


  public todos(): Observable<PaginaRegistros<EstructuraAneto>> {
    return this.http.get<PaginaRegistros<EstructuraAneto>>(
      this.getServiceURL() + this.urls.todos
    );
  }
}
