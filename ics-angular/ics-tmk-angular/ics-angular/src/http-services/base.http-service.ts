import { HttpClient } from "@angular/common/http";

export class BaseHttpService {
  private host = "http://localhost:9080";

  constructor(protected http: HttpClient) {
    if (location.hostname != "localhost") {
      this.host = location.protocol + "//" + location.host;
    }
  }

  public getServiceURL(): string {
    return this.host + "/ics_frontend/backend/api/";
    // Pruebas solo con ng serve.
    //return "http://presentaciond.cl.xcd.net.intra/ics_frontend/backend/api/";
  }

  public getFileServiceURL(): string {
    return this.host + "/ics_frontend/zuul/backend/api/";
    // Pruebas solo con ng serve.
    //return "http://presentaciond.cl.xcd.net.intra/ics_frontend/zuul/backend/api/";
  }
}
