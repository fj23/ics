import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';   

import { BaseHttpService } from './base.http-service';

import { PaginaRegistros } from 'src/models/compartido/PaginaRegistros';
import { FiltrosEstructurasModel } from 'src/models/filters/FiltrosEstructurasModel';
import { SubEstructuraAneto } from 'src/models/estructuras/SubEstructuraAneto';
import { ColumnaMetadatos } from '../models/compartido/ColumnaMetadatos';
import { SubEstructuraOutputModel } from 'src/output_models/SubEstructuraOutputModel';
import { ColumnaMetadatosOutputModel } from '../output_models/ColumnaMetadatosModel';

/** 
 * Servicio que obtiene datos y realiza operaciones confinadas al módulo de estructuras.
 */
@Injectable({providedIn: 'root'})
export class SubEstructurasHttpService extends BaseHttpService {

  //TODO: trasladar todos los metodos de subestructuras a un servicio nuevo y especifico

  private urls = {
    listar: 'subestructuras',
    buscar: 'subestructuras/get',
    existe: 'subestructuras/exists',
    crear: 'subestructuras/nueva',
    actualizar: 'subestructuras/actualizar',
    activar: 'subestructuras/activar',
    desactivar: 'subestructuras/desactivar'
  };

  // /**
  //  * Objeto que encapsula los filtros activos.
  //  */
  // private filtros: FiltrosEstructurasModel;

  // /**
  //  * Subject que avisa cuando los filtros varían.
  //  */
  // private filtrosChangeSubject = new Subject<void>();

  // /**
  //  * Observable al que otros componentes pueden suscribirse para poder 'observar' los eventos de cambio de filtros.
  //  */
  // public filtrosChange$ = this.filtrosChangeSubject.asObservable();


  constructor(http: HttpClient) { 
    super(http);
  }

  // /**
  //  * Obtiene los filtros.
  //  */
  // public getFiltros(): FiltrosEstructurasModel {
  //   if (!!!this.filtros) {
  //     this.filtros = new FiltrosEstructurasModel();
  //   }
  //   return this.filtros;
  // }

  // /**
  //  * Establece los filtros y gatilla un evento.
  //  * @param filtros El objeto de filtros a establecer.
  //  */
  // public setFiltros(filtros: FiltrosEstructurasModel): void {
  //   this.filtros = filtros;
  //   this.filtrosChangeSubject.next();
  // }

  /**
   * Transforma los filtros activos en un objeto de parámetros adjuntables a una petición HTTP.
   */
  public modelToHttpParameters(filtros: FiltrosEstructurasModel): HttpParams {
    let parameters = new HttpParams();
    
    if (filtros.codigo){
      parameters = parameters.set("codigo", filtros.codigo);
    }

    if (filtros.nombre){
      parameters = parameters.set("nombre", filtros.nombre);
    }

    return parameters;
  }

  private convertToOutput(model: SubEstructuraAneto): SubEstructuraOutputModel {

    let columnasOutput: ColumnaMetadatosOutputModel[] = [];

    for (let i = 0; i < model.columnas.length; i++) {
      const col = model.columnas[i];
      let colOutput: ColumnaMetadatosOutputModel = {
        id: col.id,
        codigo: col.codigo,
        descripcion: col.descripcion,
        orden: col.orden,
        tipoDato: col.tipoDato.id,
        vigencia: col.vigencia,
        subEstructura: null,
        // esLlave: null,
        esObligatorio: null,
        posicionInicial: 0,
        posicionFinal: 1,
        mapeada: null,
        largo: col.largo
      };
      columnasOutput.push(colOutput);
    }

    const outputObject: SubEstructuraOutputModel = {
      id: model.id,
      codigo: model.codigo,
      descripcion: model.descripcion,
      columnas: columnasOutput,
      vigencia: model.vigencia
    };

    return outputObject;
  }

  /**
   * Hace una petición GET para obtener las estructuras guardadas en la base de datos.
   */
  public listar(filtros: FiltrosEstructurasModel) : Observable<PaginaRegistros<SubEstructuraAneto>> {
    if (!!!filtros) {
      filtros = new FiltrosEstructurasModel();
    }
    let paging = "/page/" +filtros.pageIndex;
    let pageSizing = "/size/" + filtros.pageSize;
    let sortOrder = "";
    if (filtros.sortColumn) {
      sortOrder = "/sort/"+filtros.sortColumn+"/"+filtros.sortOrder;
    }
    let params = this.modelToHttpParameters(filtros);

    return this.http.get<PaginaRegistros<SubEstructuraAneto>>( 
      this.getServiceURL() + this.urls.listar + paging + pageSizing + sortOrder + "/filters", 
      { params: params } 
    );
  }

  public buscar(id: number): Observable<SubEstructuraAneto> {
    return this.http.get<SubEstructuraAneto>(
      this.getServiceURL() + this.urls.buscar + "/" + id
    );
  }

  public existe(subest: SubEstructuraAneto): Observable<boolean> {
    return this.http.put<boolean>(
      this.getServiceURL() + this.urls.existe, subest
    );
  }

  /**
   * Hace una petición POST para insertar una estructura en la base de datos.
   * @param model El objeto que va a intentar ser insertado.
   */
  public crear(model: SubEstructuraAneto) : Observable<any> {

    let outputModel: SubEstructuraOutputModel = this.convertToOutput(model);

    return this.http.post( 
      this.getServiceURL() + this.urls.crear, 
      outputModel
    );
  }

  /**
   * Hace una petición PUT para actualizar una estructura en la base de datos.
   * @param model El objeto que va a intentar ser insertado.
   */
  public actualizar(model: SubEstructuraAneto) : Observable<any> {

    let outputModel: SubEstructuraOutputModel = this.convertToOutput(model);

    return this.http.put( 
      this.getServiceURL() + this.urls.actualizar, 
      outputModel
    );
  }

  /**
   * Hace una petición POST para desactivar una subestructura de la base de datos.
   * @param model El objeto que va a intentar ser actualizado.
   */
  public activar(codigo: number) : Observable<any> {
    return this.http.get( 
      this.getServiceURL() + this.urls.activar + "/" + codigo
    );
  }

  /**
   * Hace una petición POST para desactivar una subestructura de la base de datos.
   * @param model El objeto que va a intentar ser actualizado.
   */
  public desactivar(codigo: number) : Observable<any> {
    return this.http.get( 
      this.getServiceURL() + this.urls.desactivar + "/" + codigo
    );
  }
 
}
