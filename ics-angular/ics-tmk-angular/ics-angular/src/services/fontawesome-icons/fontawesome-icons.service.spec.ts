import { TestBed } from '@angular/core/testing';

import { FontawesomeIconsService } from './fontawesome-icons.service';

describe('FontawesomeIconsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FontawesomeIconsService = TestBed.get(FontawesomeIconsService);
    expect(service).toBeTruthy();
  });
});
