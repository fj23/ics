import {ComponentCanDeactivate} from "./component-can-deactivate";

export abstract class FormCanDeactivate extends ComponentCanDeactivate{

    abstract get canDeactivateForm():boolean;

    canDeactivate(): boolean{
        return this.canDeactivateForm;
    }
}
