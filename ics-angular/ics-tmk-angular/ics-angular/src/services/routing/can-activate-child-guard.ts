import { Injectable } from '@angular/core';
import { CanActivateChild, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanActivate } from '@angular/router';

@Injectable({providedIn: 'root'})
export class CanActivateChildGuard implements CanActivateChild, CanActivate {

    constructor(private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        return this.selfCanActivate();
    }

    canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        return this.selfCanActivate();
    }

    private selfCanActivate() {
        if (!localStorage.getItem("token")) {
            this.router.navigateByUrl("/login");
            return false;
        }
        return true;
    }

}