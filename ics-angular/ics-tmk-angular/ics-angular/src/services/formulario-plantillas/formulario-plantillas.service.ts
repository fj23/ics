import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject, Observable } from 'rxjs';

import { Socio } from 'src/models/compartido/Socio';

import { PlantillaEntrada } from 'src/models/plantillas/entrada/PlantillaEntrada';

import { PlantillaIntegracion } from 'src/models/plantillas/integracion/PlantillaIntegracion';
import { ColumnaTrabajoIntegracion } from 'src/models/plantillas/integracion/ColumnaTrabajoIntegracion';
import { MapeoIntegracion } from 'src/models/plantillas/integracion/MapeoIntegracion';

import { PlantillaSalida } from 'src/models/plantillas/salida/PlantillaSalida';

import { PlantillaEntradaOutputModel } from 'src/output_models/PlantillaEntradaOutputModel';
import { ArchivoEntradaOutputModel } from 'src/output_models/ArchivoEntradaOutputModel';
import { ColumnaMetadatosOutputModel } from 'src/output_models/ColumnaMetadatosModel';
import { UnionArchivosOutputModel } from 'src/output_models/UnionArchivosOutput';
import { MapeoEntradaOutputModel } from 'src/output_models/MapeoEntradaOutputModel';

import { PlantillaIntegracionOutputModel } from 'src/output_models/PlantillaIntegracionOutputModel';
import { ColumnaTrabajoIntegracionOutputModel } from 'src/output_models/ColumnaTrabajoIntegracionOutputModel';
import { MapeoIntegracionOutputModel } from 'src/output_models/MapeoIntegracionOutputModel';

import { PlantillaSalidaOutputModel } from 'src/output_models/PlantillaSalidaOutputModel';
import { ColumnaDestinoIntegracionOutputModel } from 'src/output_models/ColumnaDestinoIntegracionOutputModel';
import { Plantilla } from 'src/models/plantillas/Plantilla';
import { ColumnaArchivoSalida } from 'src/models/plantillas/salida/ColumnaArchivoSalida';
import { TipoExtension } from 'src/models/compartido/TipoExtension';
import { ColumnaArchivoIntegracion } from 'src/models/plantillas/integracion/ColumnaArchivoIntegracion';
import { FormulaIntegracion as Formulas } from 'src/models/plantillas/integracion/FormulaIntegracion';
import { FormulaOutputModel } from 'src/output_models/FormulaOutputModel';
import { ColumnaEstructuraAnetoSalida } from 'src/models/plantillas/salida/estructura/ColumnaEstructuraAnetoSalida';
import { EstructuraAnetoSalida } from 'src/models/plantillas/salida/estructura/EstructuraAnetoSalida';
import { BaseHttpService } from 'src/http-services/base.http-service';
import { ColumnaEstructuraPlantillaSalidaOutputModel, EstructuraAnetoPlantillaSalidaOutputModel } from 'src/output_models/EstructuraAnetoPlantillaSalidaOutputModel';
import { TipoSalidaEnum } from 'src/enums/TipoSalidaEnum';
import { HttpClient } from '@angular/common/http';
import { MapeoEntrada } from 'src/models/plantillas/entrada/MapeoEntrada';
import { SubEstructuraAneto } from 'src/models/estructuras/SubEstructuraAneto';
import { TipoExtensionEnum } from 'src/enums/TipoExtensionEnum';
import { ArchivoEntrada } from 'src/models/plantillas/entrada/ArchivoEntrada';
import { EstructuraAneto } from 'src/models/estructuras/EstructuraAneto';
import { ColumnaMetadatos } from 'src/models/compartido/ColumnaMetadatos';

@Injectable({ providedIn: 'root' })
export class FormularioPlantillasService extends BaseHttpService {
    /**
     * Evento actual del formulario, obtenido desde el formulario general de plantillas.
     * Varios sub-componentes del formulario hacen uso de él para cargar datos en comboboxes.
     */
    public socio: Socio = null;
    public socio$: Observable<Socio> = new Observable<Socio>();

    /**
     * Cuando se agrega una columna o archivo al formulario, o por el contrario, se quitan todas.
     * Al ser 'true' debe bloquear los input de socio y evento del formulario general.
     */
    public bloquearDatosBasicosPlantillaSubject = new Subject<boolean>();

    /**
     * Cuando el formulario de plantilla está completo.
     * El botón "crear plantilla" no se presenta al usuario hasta que sea 'true'.
     */
    public completitudFormularioSubject: Subject<boolean> = new Subject<boolean>();

    /**
     * Cuando cambia el formato de los archivos de entrada.
     * Los datos de varios comboboxes y la disponibilidad de algunos input se deben recargar en base a esto.
     */
    public entradaFormatoSubject = new BehaviorSubject<number>(null);

    /**
     * Cuando se mapea desde una subestructura.
     * El componente diálogo de subestructura envía el objeto 'mapeo' al formulario general de entrada, desde donde se le asigna el archivo activo de origen de dicho mapeo.
     */
    public entradaMapeoColumnaSubEstructuraSubject = new Subject<MapeoEntrada>();

    /**
     * Varios elementos en el formulario de salida cambian según el tipo de extensión.
     */
    public salidaTipoExtensionSubject = new Subject<TipoExtension>();


    constructor(http: HttpClient) {
        super(http);
    }


    // // // // // // // // // // // // // // // // 
    // TRANSFORMACIONES A OBJETOS MODELO_OUTPUT  // 
    // // // // // // // // // // // // // // // // 

    /**
     * Rellena un objeto de plantilla con los datos comunes a toda plantilla: id, nombre, versión, evento, socio, etc...
     * @param plantilla La plantilla (objeto modelo del formulario) de la cual se obtienen los datos.
     * @param target El objeto (una plantilla del directorio 'output_models') al que se le asignarán los datos.
     */
    private fillCommonOutputData(plantilla: Plantilla, target: any) {
        target.id = plantilla.id ? plantilla.id : 0;
        target.vigencia = "Y";
        target.nombre = plantilla.nombre;
        target.version = plantilla.version;
        target.observaciones = plantilla.observaciones;
        target.tipo = plantilla.tipo.id;
        target.socio = plantilla.socio.id;
        target.evento = plantilla.evento.id;        
        target.codUsuario = localStorage.getItem("username");
        if (plantilla.core) {
            target.core = plantilla.core.id;
        }
        return target;
    }

    public formulaToOutputModel(input: Formulas.FormulaFactor): FormulaOutputModel {

        if (input === null) {
            return null;
        }

        let output = new FormulaOutputModel();

        output.tipo = input.tipo;

        if (input.valor || 
            input.valor === 0 || 
            (typeof input.valor === "string" && input.valor !== "") //string con espacios " " son aceptados, no ""
        ) {
            if (input.valor instanceof ColumnaTrabajoIntegracion) {
                output.valor = this.columnaTrabajoIntegracionToOutput(input.valor);
            }
            else {
                output.valor = input.valor;
            }
        } else if (input.hijos) {

            output.hijos = input.hijos.map(f => this.formulaToOutputModel(f)); 

            if (input instanceof Formulas.FncEnriquecimiento) {
                output.atrEnriquecimientoId = input.atrEnriquecimiento.idAtributoEnriquecimiento;
                output.socioId = input.socioId;
            }
        }

        return output;
    }

    // // // // // // // // // // // // // // // // 
    //           PLANTILLA DE ENTRADA            // 
    // // // // // // // // // // // // // // // // 

    private archivoEntradaEstructuradaToOutput(archivo: ArchivoEntrada, index: number): ArchivoEntradaOutputModel {
        let estructuras: number[] = archivo.estructuras.map(
            (estructura: EstructuraAneto) => { return estructura.id; }
        );

        return {
            id: archivo.id ? archivo.id : index,
            nombre: archivo.nombre,
            vigencia: "Y",
            noRealizarTransformaciones: archivo.noRealizarTransformaciones,
            // archivoMultievento: archivo.archivoMultievento,
            delimitadorColumnas: archivo.delimitadorColumnas,
            numeroHoja: archivo.numeroHoja ? archivo.numeroHoja : 0,
            // saltoLinea: archivo.saltoLinea? archivo.saltoLinea : null,
            saltarRegistrosIniciales: archivo.saltarRegistrosIniciales ? archivo.saltarRegistrosIniciales : null,
            saltarRegistrosFinales: archivo.saltarRegistrosFinales ? archivo.saltarRegistrosFinales : null,
            columnas: null,
            estructuras: estructuras
        };
    }

    private mapeoEntradaEstructuradaToOutput(mapeo: MapeoEntrada, archivoId: number): MapeoEntradaOutputModel {
        const mapeoOutput: MapeoEntradaOutputModel = {
            columnaDestino: mapeo.columnaDestino.idAtributoUsuario,
            archivoOrigen: archivoId,
            estructuraOrigenId: mapeo.estructuraOrigen.id,
            columnaOrigen: mapeo.columnaOrigenDatos.id? mapeo.columnaOrigenDatos.id : mapeo.columnaOrigenDatos.orden,
            subEstructuraColumnaOrigen: mapeo.subEstructuraColumnaOrigen ? mapeo.subEstructuraColumnaOrigen.id : null,
            valorParaFiltrar: mapeo.valorParaFiltrar,
            esObligatorio: mapeo.esObligatorio,
            idAtributoUsuario: mapeo.columnaDestino.idAtributoUsuario,
            grupoColumnaDestino: mapeo.grupoColumnaDestino,
            filtroExclusion: mapeo.filtroExclusion ? mapeo.filtroExclusion : false
        };

        return mapeoOutput;
    }

    private columnaArchivoEntradaEstandarToOutput(columna: ColumnaMetadatos): ColumnaMetadatosOutputModel {
        return {
            id: columna.id ? columna.id : columna.orden,
            codigo: columna.codigo ? columna.codigo : null,
            descripcion: columna.descripcion,
            orden: columna.orden,
            tipoDato: columna.tipoDato.id,
            esObligatorio: !!columna.esObligatorio,
            posicionInicial: columna.posicionInicial && columna.posicionInicial !== 0 ? columna.posicionInicial : 0,
            posicionFinal: columna.posicionFinal && columna.posicionFinal !== 0 ? columna.posicionFinal : 0,
            subEstructura: columna.subEstructura ? columna.subEstructura.id : null,
            vigencia: columna.vigencia,
            mapeada: null,
            largo: columna.largo
        };
    }

    private archivoEntradaEstandarToOutput(archivo: ArchivoEntrada, index: number): ArchivoEntradaOutputModel {
        let columnas: ColumnaMetadatosOutputModel[] = archivo.columnas.map(
            (columna: ColumnaMetadatos) => {
                return this.columnaArchivoEntradaEstandarToOutput(columna);
            }
        );

        return {
            id: archivo.id ? archivo.id : index,
            nombre: archivo.nombre,
            vigencia: "Y",
            noRealizarTransformaciones: archivo.noRealizarTransformaciones,
            // archivoMultievento: archivo.archivoMultievento,
            delimitadorColumnas: archivo.delimitadorColumnas ? archivo.delimitadorColumnas : '',
            numeroHoja: archivo.numeroHoja ? archivo.numeroHoja : 0,
            // saltoLinea: archivo.saltoLinea? archivo.saltoLinea : null,
            saltarRegistrosIniciales: archivo.saltarRegistrosIniciales ? archivo.saltarRegistrosIniciales : 0,
            saltarRegistrosFinales: archivo.saltarRegistrosFinales ? archivo.saltarRegistrosFinales : 0,
            columnas: columnas,
            estructuras: null
        };
    }

    private mapeoEntradaEstandarToOutput(mapeo: MapeoEntrada, archivoOrigenIndex: number): MapeoEntradaOutputModel {
        const columnaOrigen = mapeo.columnaOrigenDatos;
        
        const conv: MapeoEntradaOutputModel = {
            columnaDestino: mapeo.columnaDestino.idAtributoNegocio,
            archivoOrigen: archivoOrigenIndex,
            columnaOrigen: columnaOrigen.id ? columnaOrigen.id : columnaOrigen.orden,
            estructuraOrigenId: null,
            subEstructuraColumnaOrigen: null,
            valorParaFiltrar: mapeo.valorParaFiltrar,
            esObligatorio: mapeo.esObligatorio,
            idAtributoUsuario: mapeo.columnaDestino.idAtributoUsuario,
            grupoColumnaDestino: mapeo.grupoColumnaDestino,
            filtroExclusion: mapeo.filtroExclusion ? mapeo.filtroExclusion : false
        };

        return conv;
    }

    private getIndiceArchivoOrigenMapeo(mapeo: MapeoEntrada, archivos: ArchivoEntrada[]) {
        return archivos.findIndex((archivo) => {
            return archivo === mapeo.archivoOrigenDatos;
        });
    }

    private procesarUnionesArchivosPlantilla(plantilla: PlantillaEntrada): UnionArchivosOutputModel[] {
        
        // para obtener el "indice" del archivo a traves de sus nombres
        let mapArchivos = {};
        plantilla.archivos.map(
            (arch: ArchivoEntrada, i: number) => {
                if (!(arch.nombre in mapArchivos)) {
                    mapArchivos[arch.nombre] = i;
                } else {
                    throw new Error("No pueden haber dos archivos con el mismo nombre.");
                }
            }
        );

        // crea un array simple por cada mapeo, con el indice del archivo, de la columna, y el id atributo de negocio a mapear
        let datosMapeosArchivo: { indiceArchivo: number, idAtributoUsuario: number, indiceColumna: number }[] = [];
        for (const colDestino of plantilla.columnasDestino) {
            const nombreArchOrigenMapeo = colDestino.archivoOrigenDatos.nombre;
            const atributoUsuario = colDestino.columnaDestino.idAtributoUsuario;
            const colOrigenOrden = colDestino.columnaOrigenDatos.orden;

            datosMapeosArchivo.push( { 
                indiceArchivo: mapArchivos[nombreArchOrigenMapeo], 
                idAtributoUsuario: atributoUsuario,
                indiceColumna: colOrigenOrden
            } );
        }
        

        // en cada dato de este nuevo array revisa si hay coincidencias de atributos de negocio,
        // que no esten en el mismo archivo
        let uniones: UnionArchivosOutputModel[] = [];
        for (const datosMapeo of datosMapeosArchivo) {
            const esteIndiceArchivo = datosMapeo.indiceArchivo;
            const esteIdAtributoUsuario = datosMapeo.idAtributoUsuario;
            const esteIndiceColumna = datosMapeo.indiceColumna;

            const mapeoDeOtroArchivoConMismoDestino = datosMapeosArchivo.find(otroMapeo => (
                esteIdAtributoUsuario === otroMapeo.idAtributoUsuario &&
                esteIndiceArchivo !== otroMapeo.indiceArchivo
            ));

            // si encuentra una coincidencia, genera la union de archivos
            if (mapeoDeOtroArchivoConMismoDestino) {
                const estaUnion: UnionArchivosOutputModel = {
                    idAtributoUsuario: esteIdAtributoUsuario,
                    indiceArchivo1: esteIndiceArchivo,
                    indiceArchivo2: mapeoDeOtroArchivoConMismoDestino.indiceArchivo,
                    indiceColumnaArchivo1: esteIndiceColumna,
                    indiceColumnaArchivo2: mapeoDeOtroArchivoConMismoDestino.indiceColumna
                };
                // seguramente esta misma union se encuentra una segunda vez, desde 'el otro' archivo
                // se debe evitar duplicar las uniones
                if (!uniones.some(otraUnion => (
                        estaUnion.idAtributoUsuario === otraUnion.idAtributoUsuario && (
                            (estaUnion.indiceArchivo1 === otraUnion.indiceArchivo1  || 
                                estaUnion.indiceArchivo2 === otraUnion.indiceArchivo1 ) 
                            && 
                            (estaUnion.indiceArchivo1 === otraUnion.indiceArchivo2  ||
                                estaUnion.indiceArchivo2 === otraUnion.indiceArchivo2)
                        )
                    ))
                ) {
                    uniones.push(estaUnion);
                }
            }
        }

        return uniones;
    }


    // // // // // // // // // // // // // // // // 
    //         PLANTILLA DE INTEGRACION          // 
    // // // // // // // // // // // // // // // // 

    private columnaTrabajoIntegracionToOutput(columna: ColumnaTrabajoIntegracion): ColumnaTrabajoIntegracionOutputModel {        
        let columnaOutput = new ColumnaTrabajoIntegracionOutputModel();

        columnaOutput.id = columna.id;
        columnaOutput.nombre = columna.nombre;
        columnaOutput.orden = columna.orden;

        if (columna.columnaOrigen) {           
            columnaOutput.columnaOrigen = columna.columnaOrigen.idAtributoNegocio;
            columnaOutput.columnaOrigenUser = columna.columnaOrigen.idAtributoUsuario;
            columnaOutput.tipoPersona = columna.columnaOrigen.tipoPersona;
        } else if (columna.columnaEnriquecimiento) {
            columnaOutput.columnaOrigenUser = null;
            columnaOutput.columnaOrigen = null;
            columnaOutput.columnaEnriquecimiento = columna.columnaEnriquecimiento.idAtributoEnriquecimiento;
        }

        if (columna.formato != null) {
            columnaOutput.ordenFormato = columna.formato.orden;
            columnaOutput.formato = columna.formato.id;
        }

        if (columna.homologacion != null) {
            columnaOutput.ordenHomologacion = columna.homologacion.orden;
            columnaOutput.homologacion = columna.homologacion.id;
        }

        if (columna.formula != null) {
            columnaOutput.formula = this.formulaToOutputModel(columna.formula.cuerpo);
        }

        return columnaOutput;
    }

    private mapeoIntegracionToOutput(mapeo: MapeoIntegracion): MapeoIntegracionOutputModel {
        let mapeoOutput = new MapeoIntegracionOutputModel();

        if (mapeo.columnaOrigen.columnaOrigen != null) {
            mapeoOutput.columnaOrigen = mapeo.columnaOrigen.columnaOrigen.idAtributoNegocio;            
        }
        else if (mapeo.columnaOrigen.columnaEnriquecimiento != null) {
            mapeoOutput.columnaOrigen = mapeo.columnaOrigen.columnaEnriquecimiento.idAtributoEnriquecimiento;            
        }
        mapeoOutput.columnaTrabajoOrden = mapeo.columnaOrigen.orden;
        mapeoOutput.columnaDestino = mapeo.columnaDestino.idAtributoIntegracionEvento;
        mapeoOutput.idAtributoUser = mapeo.columnaDestino.idAtributoUser;
        mapeoOutput.esObligatorio = mapeo.obligatorio;
        mapeoOutput.id = mapeo.id;

        return mapeoOutput;
    }

    private columnaArchivoIntegracionToOutput(columna: ColumnaArchivoIntegracion, incluirPosicion: boolean): ColumnaDestinoIntegracionOutputModel {
        let columnaOutput = new ColumnaDestinoIntegracionOutputModel();

        columnaOutput.separarArchivo = columna.separarArchivo;
        columnaOutput.incluirEnArchivo = columna.incluir;
        columnaOutput.orden = columna.orden;
        columnaOutput.flgVisible = columna.flgVisible;
        columnaOutput.id = columna.id;

        if (incluirPosicion) {
            columnaOutput.posicionInicial = columna.posicionInicial;
            columnaOutput.posicionFinal = columna.posicionFinal;
        }
        else {
            columnaOutput.posicionInicial = 0;
            columnaOutput.posicionFinal = 0;
        }
        return columnaOutput;
    }


    // // // // // // // // // // // // // // // // 
    //            PLANTILLA DE SALIDA            // 
    // // // // // // // // // // // // // // // // 


    /* LECTURA */

    public columnaSubestructuraToPlantillaSalidaModel(sourceSubCol: ColumnaMetadatos): ColumnaEstructuraAnetoSalida {
        let subcol = new ColumnaEstructuraAnetoSalida();
        subcol.id = sourceSubCol.id;
        subcol.codigo = sourceSubCol.codigo;
        subcol.descripcion = sourceSubCol.descripcion;
        return subcol;
    }

    public subestructuraAnetoToPlantillaSalidaModel(subEstrSource: SubEstructuraAneto): EstructuraAnetoSalida {
        let substr = new EstructuraAnetoSalida();
        substr.id = subEstrSource.id;
        substr.codigo = subEstrSource.codigo;
        substr.descripcion = subEstrSource.descripcion;
        if (subEstrSource.columnas && subEstrSource.columnas.length > 0) {
            substr.columnas = subEstrSource.columnas.map((sourceSubCol: ColumnaMetadatos) => {
                return this.columnaSubestructuraToPlantillaSalidaModel(sourceSubCol);
            });
        }
        return substr;
    }

    public columnaEstructuraToPlantillaSalidaModel(sourceCol: ColumnaMetadatos): ColumnaEstructuraAnetoSalida {
        let col = new ColumnaEstructuraAnetoSalida();
        col.id = sourceCol.id;
        col.codigo = sourceCol.codigo;
        col.descripcion = sourceCol.descripcion;
        const subEstrSource = sourceCol.subEstructura;
        if (subEstrSource) {
            col.subestructura = this.subestructuraAnetoToPlantillaSalidaModel(subEstrSource);
        }
        return col;
    }

    public estructuraAnetoToPlantillaSalidaModel(source: EstructuraAneto): EstructuraAnetoSalida {
        let estr = new EstructuraAnetoSalida();
        estr.id = source.id;
        estr.codigo = source.codigo;
        estr.descripcion = source.descripcion;
        estr.columnas = source.columnas.map((sourceCol: ColumnaMetadatos) => {
            return this.columnaEstructuraToPlantillaSalidaModel(sourceCol);
        });
        return estr;
    }

    /* ESCRITURA/GUARDADO */

    private columnaPlantillaSalidaToOutput(columna: ColumnaArchivoSalida, index: number, tipoExtensionId: number): ColumnaDestinoIntegracionOutputModel {
        let colOutput = new ColumnaDestinoIntegracionOutputModel()
        colOutput.id = columna.id ? columna.id : 0;
        colOutput.nombre = columna.columnaOrigen.nombreColumna;
        colOutput.columnaOrigen = columna.columnaOrigen.idAtributoUsuario; //TODO cambiar a idAtributoUsuario
        colOutput.orden = index + 1;
        colOutput.posicionInicial = columna.posicionInicial;
        colOutput.posicionFinal = columna.posicionFinal;

        return colOutput;
    }


    private columnaSubestructuraToPlantillaSalidaOutputModel(sourceSubCol: ColumnaEstructuraAnetoSalida): ColumnaEstructuraPlantillaSalidaOutputModel {
        let subcol = new ColumnaEstructuraPlantillaSalidaOutputModel();
        subcol.idDetalleEstructura = sourceSubCol.id;
        subcol.codigoColumna = sourceSubCol.codigo;
        subcol.descripcionColumna = sourceSubCol.descripcion;

        if (sourceSubCol.atributoDiccionario) {
            subcol.idAtributoDiccionario = sourceSubCol.atributoDiccionario;
        } else if (sourceSubCol.valorFijo) {
            subcol.valorFijo = sourceSubCol.valorFijo;
        }

        if (sourceSubCol.formato) {
            subcol.idTipoFormato = sourceSubCol.formato.valor;
            subcol.ordenFormato = sourceSubCol.formato.orden;
        }
        if (sourceSubCol.homologacion) {
            subcol.idTipoHomologacion = sourceSubCol.homologacion.valor;
            subcol.ordenHomologacion = sourceSubCol.homologacion.orden;
        }
        if (sourceSubCol.formula) { 
            subcol.formula = sourceSubCol.formula.valor;
            subcol.ordenFormula = sourceSubCol.formula.orden;
        }
        return subcol;
    }

    private subestructuraAnetoToPlantillaSalidaOutputModel(subEstrSource: EstructuraAnetoSalida): EstructuraAnetoPlantillaSalidaOutputModel {
        let substr = new EstructuraAnetoPlantillaSalidaOutputModel();
        substr.id = subEstrSource.id;
        substr.codigo = subEstrSource.codigo;
        substr.descripcion = subEstrSource.descripcion;
        if (subEstrSource.columnas && subEstrSource.columnas.length > 0) {
            substr.columnas = subEstrSource.columnas.map(
                sourceSubCol => {
                    let col = this.columnaSubestructuraToPlantillaSalidaOutputModel(sourceSubCol);
                    col.idSubEstructura = substr.id;
                    return col;
                }
            );
        }
        return substr;
    }

    

    private columnaEstructuraToPlantillaSalidaOutputModel(sourceCol: ColumnaEstructuraAnetoSalida): ColumnaEstructuraPlantillaSalidaOutputModel {
        let col = new ColumnaEstructuraPlantillaSalidaOutputModel();
        col.idDetalleEstructura = sourceCol.id;
        col.codigoColumna = sourceCol.codigo;
        col.descripcionColumna = sourceCol.descripcion;

        const subEstrSource = sourceCol.subestructura;
        if (subEstrSource) {
            col.subestructura = this.subestructuraAnetoToPlantillaSalidaOutputModel(subEstrSource);
        } 

        if (sourceCol.esValida) {
            if (sourceCol.atributoDiccionario) {
                col.idAtributoDiccionario = sourceCol.atributoDiccionario;
            } else if (sourceCol.valorFijo) {
                col.valorFijo = sourceCol.valorFijo;
            }
    
            if (sourceCol.formato) {
                col.idTipoFormato = sourceCol.formato.valor;
                col.ordenFormato = sourceCol.formato.orden;
            }
            if (sourceCol.homologacion) {
                col.idTipoHomologacion = sourceCol.homologacion.valor;
                col.ordenHomologacion = sourceCol.homologacion.orden;
            }
            if (sourceCol.formula) { 
                col.formula = this.formulaToOutputModel(sourceCol.formula.valor);
                col.ordenFormula = sourceCol.formula.orden;                
            }
        }

        return col;
    }

    private estructuraAnetoToPlantillaSalidaOutputModel(source: EstructuraAnetoSalida): EstructuraAnetoPlantillaSalidaOutputModel {
        let estr = new EstructuraAnetoPlantillaSalidaOutputModel();
        estr.id = source.id;
        estr.codigo = source.codigo;
        estr.descripcion = source.descripcion;
        estr.columnas = source.columnas.map(
            sourceCol => {
                let col = this.columnaEstructuraToPlantillaSalidaOutputModel(sourceCol);
                col.idEstructura = estr.id;
                return col;
            }
        );
        return estr;
    }


    // // // // // // // // // // // // // // // // 
    //       CREACION DE PLANTILLAS OUTPUT       // 
    // // // // // // // // // // // // // // // // 

    public crearPlantillaEntradaOutput(plantilla: PlantillaEntrada): PlantillaEntradaOutputModel {

        let plantillaOutput: PlantillaEntradaOutputModel = this.fillCommonOutputData(
            plantilla,
            new PlantillaEntradaOutputModel()
        );

        const tpExtensionId = plantilla.formatoEntrada.id;
        const esEstructurado = (tpExtensionId === TipoExtensionEnum.ESTRUCTURADO);

        plantillaOutput.tipoExtension = tpExtensionId;

        if (esEstructurado) {
            plantillaOutput.archivos = plantilla.archivos.map(
                (archivo: ArchivoEntrada, index: number) => {
                    return this.archivoEntradaEstructuradaToOutput(archivo, index);
                }
            );

            plantillaOutput.columnasDestino = plantilla.columnasDestino.map(
                (mapeo: MapeoEntrada) => {
                    const archivoOrigenIndex = this.getIndiceArchivoOrigenMapeo(mapeo, plantilla.archivos);
                    const archivo = plantilla.archivos[archivoOrigenIndex];
                    let archivoId = (archivo.id > 4000000)? archivo.id : archivoOrigenIndex;
                    return this.mapeoEntradaEstructuradaToOutput(mapeo, archivoId);
                }
            );
        } else {
            plantillaOutput.archivos = plantilla.archivos.map(
                (archivo: ArchivoEntrada, index: number) => {
                    return this.archivoEntradaEstandarToOutput(archivo, index);
                }
            );

            plantillaOutput.columnasDestino = plantilla.columnasDestino.map(
                (mapeo: MapeoEntrada) => {
                    const archivoOrigenIndex = this.getIndiceArchivoOrigenMapeo(mapeo, plantilla.archivos);
                    
                    let idArchivo = plantilla.archivos[archivoOrigenIndex].id;
                    if (!idArchivo) { idArchivo = archivoOrigenIndex }
                    
                    return this.mapeoEntradaEstandarToOutput(mapeo, idArchivo);
                }
            );
        }

        if (plantillaOutput.archivos.length > 1) {
            plantillaOutput.uniones = this.procesarUnionesArchivosPlantilla(plantilla);
        }

        return plantillaOutput;
    }


    public crearPlantillaIntegracionOutput(plantilla: PlantillaIntegracion): PlantillaIntegracionOutputModel {

        console.log("crearPlantillaIntegracionOutput");
        
        let plantillaOutput: PlantillaIntegracionOutputModel = this.fillCommonOutputData(
            plantilla,
            new PlantillaIntegracionOutputModel()
        );

        
        plantillaOutput.columnasTrabajo = plantilla.columnasTrabajo.map(
            (columna: ColumnaTrabajoIntegracion, index: number) => {
                return this.columnaTrabajoIntegracionToOutput(columna);
            }
        );
        console.log(plantillaOutput.columnasTrabajo);
        plantillaOutput.mapeos = plantilla.columnasDestino.map(
            (mapeo: MapeoIntegracion) => {
                return this.mapeoIntegracionToOutput(mapeo);
            }
        );

        const tpSalidaId = plantilla.tipoSalida.idTipoSalida;
        const esSalidaManual = (tpSalidaId === TipoSalidaEnum.MANUAL);

        plantillaOutput.tipoSalida = tpSalidaId;

        if (esSalidaManual) {

            const tpExtensionId = plantilla.extensionSalidaManual.id;
            const esArchivoPlano = (plantilla.extensionSalidaManual.id === TipoExtensionEnum.PLANO);
            const debeIncluirPosicionesColumnas = (!plantilla.delimitadorColumnas && esArchivoPlano);
            
            plantillaOutput.tipoExtension = tpExtensionId;
            plantillaOutput.incluirEncabezados = !!plantilla.incluirEncabezados;
            plantillaOutput.delimitadorColumnas = plantilla.delimitadorColumnas? plantilla.delimitadorColumnas : null;
            
            const columnasArchivoSalidaReordenadas = plantilla.columnasArchivoDestino.filter(c => c.incluir);
            columnasArchivoSalidaReordenadas.push(...plantilla.columnasArchivoDestino.filter(c => !c.incluir));
            const columnasArchivoSalida = [];
            let i = 1;
            for (const columna of columnasArchivoSalidaReordenadas) {
                const columnaOutput = this.columnaArchivoIntegracionToOutput(columna, debeIncluirPosicionesColumnas);
                columnaOutput.indiceMapeoOrigen = plantilla.columnasDestino.findIndex(m => m===columna.origen);
                columnaOutput.orden = i;
                columnasArchivoSalida.push(columnaOutput);
                i++;
            }

            plantillaOutput.columnasArchivoSalida = columnasArchivoSalida;
        }

        return plantillaOutput;
    }

    public crearPlantillaSalidaOutput(plantilla: PlantillaSalida): PlantillaSalidaOutputModel {

        let plantillaOutput: PlantillaSalidaOutputModel = this.fillCommonOutputData(
            plantilla,
            new PlantillaSalidaOutputModel()
        );
        
        if (plantilla.formatoOriginal) {
            plantillaOutput.formatoOriginal = true;
        } else {
            const tpExtensionId = plantilla.tipoExtension.id;
            const esEstructurado = (tpExtensionId === TipoExtensionEnum.ESTRUCTURADO);
            
            plantillaOutput.tipoExtension = tpExtensionId;
            
            if (esEstructurado) {
                plantillaOutput.estructuras = plantilla.estructuras.map(
                    e => this.estructuraAnetoToPlantillaSalidaOutputModel(e)
                );
            } else {
                plantillaOutput.columnas = plantilla.columnas.map(
                    (columna: ColumnaArchivoSalida, index: number) => {
                        return this.columnaPlantillaSalidaToOutput(columna, index, tpExtensionId);
                    }
                );
                
                if (tpExtensionId === TipoExtensionEnum.CSV || tpExtensionId === TipoExtensionEnum.PLANO) {
                    const delimitador = plantilla.delimitadorColumnas? plantilla.delimitadorColumnas : null;
                    plantillaOutput.delimitadorColumnas = delimitador;
                }
            }
        }

        return plantillaOutput;
    }
}