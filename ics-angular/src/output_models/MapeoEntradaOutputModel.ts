export class MapeoEntradaOutputModel {
    public columnaDestino: number;
    public archivoOrigen: number;
    public estructuraOrigenId: number;
    public columnaOrigen: number;
    public subEstructuraColumnaOrigen: number;
    public valorParaFiltrar: string;
    public esObligatorio: boolean;
    public filtroExclusion: boolean;
    public encriptar: boolean;

    public idAtributoUsuario: number;
    public grupoColumnaDestino: number;
}