export class FormulaOutputModel {
    public tipo: string;
    public hijos?: FormulaOutputModel[];
    public valor?: any;
    public atrEnriquecimientoId?: number;
    public socioId?: number;
}