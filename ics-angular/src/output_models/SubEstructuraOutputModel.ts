import { ColumnaMetadatosOutputModel } from './ColumnaMetadatosModel';

export interface SubEstructuraOutputModel {
    id: number;
    codigo: string;
    descripcion: string;
    columnas: ColumnaMetadatosOutputModel[];
    vigencia: string;
}