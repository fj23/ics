export interface UnionArchivosOutputModel {
    idAtributoUsuario: number;
    indiceArchivo1: number;
    indiceArchivo2: number;
    indiceColumnaArchivo1: number;
    indiceColumnaArchivo2: number;
}