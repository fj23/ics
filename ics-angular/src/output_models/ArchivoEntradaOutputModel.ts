import { ColumnaMetadatosOutputModel } from './ColumnaMetadatosModel';

export class ArchivoEntradaOutputModel {
    public id: number;
    public nombre: string;
    public vigencia: string;
    public noRealizarTransformaciones: boolean;
    // public archivoMultievento: boolean;
    public delimitadorColumnas: string;
    public numeroHoja: number;
    // public saltoLinea: string;
    public saltarRegistrosIniciales: number;
    public saltarRegistrosFinales: number;
    public columnas: ColumnaMetadatosOutputModel[];
    public estructuras: number[];
}