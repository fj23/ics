import { ColumnaDestinoIntegracionOutputModel } from './ColumnaDestinoIntegracionOutputModel';
import { EstructuraAnetoPlantillaSalidaOutputModel } from './EstructuraAnetoPlantillaSalidaOutputModel';

export class PlantillaSalidaOutputModel {
    public id: number;
    public vigencia: string;
    public nombre: string;
    public version: string;
    public observaciones: string;
    public tipo: number;
    public socio: number;
    public evento: number;
    public core: number;
    public tipoExtension: number = null;
    public delimitadorColumnas: string = null;
    public columnas: ColumnaDestinoIntegracionOutputModel[] = null;
    public estructuras: EstructuraAnetoPlantillaSalidaOutputModel[] = null;
    public formatoOriginal: boolean;
    public codUsuario: string;
    public directorioSalida: number;
}