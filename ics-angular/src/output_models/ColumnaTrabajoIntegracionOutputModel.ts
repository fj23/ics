import { FormulaOutputModel } from './FormulaOutputModel';
export class ColumnaTrabajoIntegracionOutputModel {
    public id: number;
    public nombre: string;
    public orden: number = 0;
    public columnaOrigen: number = 0;
    public columnaEnriquecimiento: number = 0;
    public ordenHomologacion: number = 0;
    public ordenFormato: number = 0;
    public formula: FormulaOutputModel = null;
    public homologacion: number = 0;
    public formato: number = 0;
    public columnaOrigenUser: number = 0;
    public tipoPersona: string = null;
}