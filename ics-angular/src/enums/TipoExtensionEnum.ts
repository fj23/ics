export enum TipoExtensionEnum {
    CSV=300000,
    ESTRUCTURADO=300001,
    EXCEL=300002,
    PLANO=300003,
    EXCEL97_2003=300004
}