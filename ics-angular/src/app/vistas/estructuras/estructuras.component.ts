import { Component, OnInit } from '@angular/core';
import { EstructurasHttpService } from 'src/http-services/estructuras.http-service';
import { FiltrosEstructurasModel } from 'src/models/filters/FiltrosEstructurasModel';
import { Permisos } from '../../compartido/Permisos';

@Component({
    selector: 'app-estructuras',
    templateUrl: './estructuras.component.html',
    styleUrls: ['./estructuras.component.css'],
    providers: [EstructurasHttpService]
})
export class EstructurasComponent implements OnInit {

    /**
     * Acción elegida en el mantenedor. Según esta se muestran y ocultan sub-componentes de este módulo.
     */
    protected _verFiltros: boolean;
    protected _ocultarCreacionPorPermisos: boolean;

    public get verFiltros() { return this._verFiltros; }
    public get ocultarCreacionPorPermisos() { return this._ocultarCreacionPorPermisos; }

    constructor(
        protected localSvc: EstructurasHttpService
    ) {
        this._verFiltros = true;
        this._ocultarCreacionPorPermisos = Permisos.DenegarAcceso(Permisos.MantenerEstructura());
    }

    ngOnInit(): void {
        const filtrosEstablecidos: FiltrosEstructurasModel = this.localSvc.getFiltros();
        let filtros = new FiltrosEstructurasModel();
        filtros.pageSize = filtrosEstablecidos.pageSize;
        filtros.sortColumn = filtrosEstablecidos.sortColumn;
        filtros.sortOrder = filtrosEstablecidos.sortOrder;

        this.localSvc.setFiltros(filtros);
    }

    /**
     * Evento llamado cuando se da clic a un botón que cambia la acción seleccionada.
     * @param input La acción que se ha seleccionado.
     */
    public onClickAlternarFiltros(): void {
        //si se hace dos veces clic en la misma accion, se deselecciona
        this._verFiltros = !this._verFiltros;
    }
}
