import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TablaEstructurasComponent } from './tabla.estructuras.component';

describe('LoginComponent', () => {
  let component: TablaEstructurasComponent;
  let fixture: ComponentFixture<TablaEstructurasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TablaEstructurasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TablaEstructurasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
