import { OnDestroy } from "@angular/core";
import { DataSource } from "@angular/cdk/table";
import { CollectionViewer } from "@angular/cdk/collections";
import { Observable, BehaviorSubject, of, Subscription, Subject } from 'rxjs';
import { catchError, finalize } from "rxjs/operators";

import { PaginaRegistros } from 'src/models/compartido/PaginaRegistros';
import { EstructuraAneto } from "src/models/estructuras/EstructuraAneto";
import { EstructurasHttpService } from 'src/http-services/estructuras.http-service';

export class EstructurasDataSource implements OnDestroy, DataSource<EstructuraAneto> {

	private filtrosChangeSub: Subscription;
	
	
	/** 
	 * Recibe y transmite el array de estructuras activo a la vista. 
	 */
	private estructurasSubject = new Subject<EstructuraAneto[]>();
	
	/** 
	 * Recibe y transmite la cantidad de estructuras conseguidas por los filtros activos.
	 * Esta cantidad suele superar el número de registros en la página actual. 
	 */
	private estructurasCountSubject = new BehaviorSubject<number>(0);

	private cargaSubject = new BehaviorSubject<boolean>(false);

	public estructurasCount$ = this.estructurasCountSubject.asObservable();
	public carga$ = this.cargaSubject.asObservable();	
	
	constructor(
		private localSvc: EstructurasHttpService
	) { 
		this.filtrosChangeSub = this.localSvc.filtrosChange$.subscribe(
			() => this.getEstructuras()
		);
	}

	ngOnDestroy(): void {
		this.filtrosChangeSub.unsubscribe();
	}

	connect(collectionViewer: CollectionViewer): Observable<EstructuraAneto[] | ReadonlyArray<EstructuraAneto>> {
		return this.estructurasSubject.asObservable();
	}
	
	disconnect(collectionViewer: CollectionViewer): void {
		this.estructurasSubject.complete();
		this.cargaSubject.complete();
	}

	/**
	 * Solicita la página de estructuras respectiva.
	 * @param pageIndex El índice (con base 0) de la página.
	 * @param pageSize La cantidad de registros a mostrar por página.
	 */
	public getEstructurasPage(pageIndex: number, pageSize: number, sortColumn: string, sortOrder: string): void {
		let filtros = this.localSvc.getFiltros();
		filtros.pageIndex = pageIndex;
		filtros.pageSize = pageSize;
		filtros.sortColumn = sortColumn;
		filtros.sortOrder = sortOrder;
		
		this.localSvc.setFiltros(filtros);
	}

	/**
	 * Recarga directamente las estructuras haciendo una llamada al servicio de homologaciones.
	 * Éste debiera ser el último método llamado en la cadena de ejecución para este DataSource.
	 */
	private getEstructuras(): void {
		if (!this.cargaSubject.getValue()) {	
			this.cargaSubject.next(true);

			this.localSvc.listar().pipe(
				catchError(() => of([])),
				finalize(() => this.cargaSubject.next(false))
			).subscribe(
				(payload: PaginaRegistros<EstructuraAneto>) => {
					this.estructurasSubject.next(payload.items);
					if (this.estructurasCountSubject.getValue() != payload.count) {
						this.estructurasCountSubject.next(payload.count);
					}
				}
			);
		}
	}
	
}
