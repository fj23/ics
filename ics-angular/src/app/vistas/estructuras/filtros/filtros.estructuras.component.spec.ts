import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FiltrosEstructurasComponent } from './filtros.estructuras.component';

describe('LoginComponent', () => {
  let component: FiltrosEstructurasComponent;
  let fixture: ComponentFixture<FiltrosEstructurasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FiltrosEstructurasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FiltrosEstructurasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
