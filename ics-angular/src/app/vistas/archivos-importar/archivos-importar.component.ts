import { Component, OnInit, ViewChild } from "@angular/core";
import { FormGroup, FormBuilder } from "@angular/forms";

import { MAT_DATE_LOCALE, DateAdapter } from "@angular/material/core";
import { CustomDateAdapter } from "src/app/compartido/custom-date-adapter.class";
import { MatDialog, MatPaginator, MatSort } from "@angular/material";
import { InformationDialogComponent } from "src/app/dialogs/information/information.dialog.component";

import { saveAs } from "file-saver";
import { Title } from "@angular/platform-browser";
import { merge, Observable, Subscription } from "rxjs";
import { Socio } from "src/models/compartido/Socio";
import { CommonDataHttpService } from "src/http-services/common-data.http-service";
import { Evento } from "src/models/compartido/Evento";
import { HistorialDataSource } from "./tabla.importar.datasource";
import { MatPaginatorES } from "src/app/compartido/mat.paginator.es";
import { Angular5Csv } from "angular5-csv/dist/Angular5-csv";
import { ArchivosHttpService } from "src/http-services/archivos.http-service";

@Component({
  selector: "app-archivos-importar",
  templateUrl: "./archivos-importar.component.html",
  styleUrls: ["./archivos-importar.component.css"],
  providers: [
    { provide: DateAdapter, useClass: CustomDateAdapter },
    { provide: MAT_DATE_LOCALE, useValue: "es-CL" }
  ]
})
export class ArchivosImportarComponent implements OnInit {
  /*Bar Spinner*/
  loadingDownload: boolean = false;
  loadingUpload: boolean = false;
  barspinner = {
    color: "primary",
    mode: "indeterminate",
    bufferValue: 75
  };

  displayedColumns: string[] = [
    "nombreArchivo",
    "procesoCarga",
    "fechaCarga",
    "fechaProcesado",
    "registrosActualizados"
  ];

  fileToUpload: File = null;

  public archivoImportarForm: FormGroup;

  socios$: Observable<Socio[]>;
  procesos$ = [];
  eventos$: Observable<Evento[]>;
  transacciones$ = [];

  public dataSource: HistorialDataSource;

  @ViewChild(MatPaginator) private paginator: MatPaginator;
  @ViewChild(MatSort) private sort: MatSort;

  private reloadTableSub: Subscription;
  private archivosCountSub: Subscription;

  idTransaccionSelected: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private importarArchivoService: ArchivosHttpService,
    private title: Title,
    public dialog: MatDialog,
    private commonSvc: CommonDataHttpService
  ) {
    this.title.setTitle("ICS | Importar Archivo");
    this.dataSource = new HistorialDataSource(this.importarArchivoService);
  }

  ngOnInit() {

    this.paginator._intl = new MatPaginatorES();

    //cuando el orden aplicado a los datos cambie, vuelve a la pagina 1
    this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

    //cuando el orden o el numero de pagina cambie, recarga los datos
    this.reloadTableSub = merge(
      this.sort.sortChange,
      this.paginator.page
    ).subscribe(() => this.recargar());

    this.archivosCountSub = this.dataSource.archivosCount$.subscribe(
      count => (this.paginator.length = count)
    );

    /** Se crea el formulario */
    this.archivoImportarForm = this.formBuilder.group({
      socio: null,
      evento: null,
      proceso: null,
      transaccion: null,
      dpDesde: null,
      archivo: null
    });

    /** Carga de Filtros */
    this.socios$ = this.commonSvc.getSocios();
    this.eventos$ = this.commonSvc.getEventos();

    /** Subcribe a los cambio de valores de los filtros */
    this.archivoImportarForm.controls.socio.valueChanges.subscribe(value => {
      this.archivoImportarForm.controls.transaccion.setValue(null);
      this.archivoImportarForm.controls.archivo.setValue(null);
      this.archivoImportarForm.controls.proceso.setValue(null);
      if (value) {
        let evento = this.archivoImportarForm.controls.evento.value;
        this.commonSvc.getProcesos(value, evento).subscribe(resp => {
          this.procesos$ = resp;
        });
      } else {
        this.procesos$ = [];
        this.transacciones$ = [];
      }
    });

    this.archivoImportarForm.controls.evento.valueChanges.subscribe(value => {
      this.archivoImportarForm.controls.transaccion.setValue(null);
      this.archivoImportarForm.controls.archivo.setValue(null);
      this.archivoImportarForm.controls.proceso.setValue(null);
      let socio = this.archivoImportarForm.controls.socio.value;
      if (socio) {
        this.commonSvc.getProcesos(socio, value).subscribe(resp => {
          this.procesos$ = resp;
        });
      } else {
        this.procesos$ = [];
        this.transacciones$ = [];
      }
    });

    this.archivoImportarForm.controls.proceso.valueChanges.subscribe(value => {
      this.archivoImportarForm.controls.transaccion.setValue(null);
      this.archivoImportarForm.controls.archivo.setValue(null);
      let socio = this.archivoImportarForm.controls.socio.value;
      if (socio) {
        this.importarArchivoService.getArchivos(this.getFiltro()).subscribe(resp => {
          this.transacciones$ = [];
          for (let a of resp) {
            if (!a.nombreArchivo) a.nombreArchivo = 'Sin Nombre';
            this.transacciones$.push(a);
          }
        });
      }
    });

    this.archivoImportarForm.controls.dpDesde.valueChanges.subscribe(() => {
      this.archivoImportarForm.controls.transaccion.setValue(null);
      this.archivoImportarForm.controls.archivo.setValue(null);
      let socio = this.archivoImportarForm.controls.socio.value;
      if (socio) {
        this.importarArchivoService.getArchivos(this.getFiltro()).subscribe(resp => {
          this.transacciones$ = [];
          for (let a of resp) {
            if (!a.nombreArchivo) a.nombreArchivo = 'Sin Nombre';
            this.transacciones$.push(a);
          }
        });
      }
    });

    this.archivoImportarForm.controls.transaccion.valueChanges.subscribe(value => {
      this.archivoImportarForm.controls.archivo.setValue(value);
    });

    this.archivoImportarForm.controls.archivo.valueChanges.subscribe(value => {
      if (value) { 
        this.idTransaccionSelected = true;
        this.dataSource.getArchivos(value);
      } else this.idTransaccionSelected = false;
    });
  }

  private recargar(): void {
    this.dataSource.getArchivosPage(
      this.archivoImportarForm.controls.archivo.value,
      this.paginator.pageIndex,
      this.paginator.pageSize,
      this.sort.active,
      this.sort.direction
    );
  }

  private getFiltro() {
    let socio = this.archivoImportarForm.get("socio").value;
    let proceso = this.archivoImportarForm.get("proceso").value;
    let evento = this.archivoImportarForm.get("evento").value;
    let fechaDesde = this.archivoImportarForm.get("dpDesde").value;
    let filtro = {
      socio: socio,
      proceso: proceso,
      evento: evento,
      fechaDesde: fechaDesde
    };
    return filtro;
  }

  handleFileInput(files: FileList) {
    let fileElement = files.item(files.length - 1);
    this.fileToUpload = fileElement;
  }

  descargarArchivo() {
    this.loadingDownload = true;
    this.importarArchivoService
      .descargarArchivo(this.archivoImportarForm.get("archivo").value)
      .subscribe(
        resp => {
          var blob = new Blob([resp.body], {
            type: "text/plain;charset=utf-8"
          });
          let filename: string = resp.headers.get("File-Name");
          saveAs(blob, filename);
        },
        err => {
          console.error(err);
          this.loadingDownload = false;
          this.dialog.open(InformationDialogComponent, {
            width: "475px",
            data: {
              title: "Error",
              content: err.headers.get("Exception-Message")
            }
          });
        },
        () => {
          this.loadingDownload = false;
        }
      );
  }

  cargarArchivo() {
    let posExtension = this.fileToUpload.name.lastIndexOf(".") + 1;
    let extension = this.fileToUpload.name.substr(
      posExtension,
      this.fileToUpload.name.length - posExtension
    );
    let extensionLC = extension.toLowerCase();

    if (extensionLC != "xlsx" && extensionLC != "xls") {
      this.dialog.open(InformationDialogComponent, {
        width: "475px",
        data: {
          title: "Error",
          content:
            "No se puede cargar este tipo de archivo (" +
            extension +
            ").<br/>Formatos validos son xlsx ó xls"
        }
      });
      return;
    }

    this.loadingUpload = true;
    this.importarArchivoService
      .cargarArchivoImportado(
        this.fileToUpload,
        this.archivoImportarForm.get("archivo").value
      )
      .subscribe(
        resp => {
          this.dialog.open(InformationDialogComponent, {
            width: "475px",
            data: {
              title: "Éxito",
              content: "Archivo Almacenado Correctamente"
            }
          });
          this.fileToUpload = null;
        },
        err => {
          this.loadingUpload = false;
          console.error(err);
          try {
            this.dialog.open(InformationDialogComponent, {
              width: "475px",
              data: {
                title: "Error al cargar archivo",
                content: err.error.message
              }
            });
          } catch (error) {
            this.dialog.open(InformationDialogComponent, {
              width: "475px",
              data: {
                title: "Error al cargar archivo",
                content: err.statusText
              }
            });
          }
        },
        () => {
          this.loadingUpload = false;
        }
      );
  }

  exportLoading: boolean = false;
  onClickExportar() {
    let options = {
      fieldSeparator: ";",
      quoteStrings: '"',
      decimalseparator: ",",
      showLabels: true,
      showTitle: false,
      useBom: true,
      noDownload: false,
      title: "",
      headers: [
        "Nombre de Archivo",
        "Proceso de Carga",
        "Fecha de Carga",
        "Fecha de Procesamiento",
        "Registros Actualizados"
      ],
      nullToEmptyString: true
    };

    let data = [];
    this.exportLoading = true;
    let idTrasaccion = this.archivoImportarForm.controls.archivo.value;
    this.importarArchivoService.exportar(idTrasaccion).subscribe(resp => {
      for (let t of resp) {
        data.push([
          t.nombreArchivo,
          t.procesoCarga,
          this.formatDate(t.id.fechaCarga),
          this.formatDate(t.fechaProceso),
          t.cantidadRegistros
        ]);
      }
      this.exportLoading = false;
      return new Angular5Csv(data, "Historial Importaciones Transaccion " + idTrasaccion, options);
    },
    error => {
      console.error(error);
      this.exportLoading = false;
    });
  }

  private formatDate(time: number): string {
    if (time == null) {
      return "";
    } else {
      var date = new Date(time)
      return date.getDate() + "/" + date.getMonth() +
        "/" +
        date.getFullYear() +
        " " +
        date.getHours() +
        ":" +
        date.getMinutes() +
        ":" +
        date.getSeconds()
      ;
    }
  }
}
