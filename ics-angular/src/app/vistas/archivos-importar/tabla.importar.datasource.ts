import { DataSource } from "@angular/cdk/table";
import { CollectionViewer } from "@angular/cdk/collections";
import { Observable, BehaviorSubject, of, Subject, Subscription } from "rxjs";
import { catchError, finalize } from "rxjs/operators";

import { PaginaRegistros } from "src/models/compartido/PaginaRegistros";
import { HistorialImportacionesModel } from "src/models/cargaArchivo/HistorialImportacionesModel";
import { ArchivosHttpService } from "../../../http-services/archivos.http-service";
import { FiltrosBaseModel } from "src/models/filters/FiltrosBaseModel";

export class HistorialDataSource
  implements DataSource<HistorialImportacionesModel> {
  private cargaSubject = new BehaviorSubject<boolean>(false);

  /** Recibe y transmite el array de archivos activo a la vista. */
  private archivosSubject = new Subject<HistorialImportacionesModel[]>();
  public archivos$: Observable<HistorialImportacionesModel[]> = this.archivosSubject.asObservable();

  /** Recibe y transmite la cantidad de archivos conseguidas por los filtros activos.
   * Esta cantidad suele superar el número de registros en la página actual. */
  private archivosCountSubject = new BehaviorSubject<number>(0);

  //observables
  public archivosCount$ = this.archivosCountSubject.asObservable();
  public carga$ = this.cargaSubject.asObservable();

  constructor(private localSvc: ArchivosHttpService) {
  }

  connect(collectionViewer: CollectionViewer): Observable<HistorialImportacionesModel[] | ReadonlyArray<HistorialImportacionesModel>> {
    return this.archivosSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this.archivosSubject.complete();
    this.cargaSubject.complete();
  }

  /**
   * Solicita la página de archivos respectiva.
   * @param pageIndex El índice (con base 0) de la página.
   * @param pageSize La cantidad de registros a mostrar por página.
   */
  public getArchivosPage(
    idTransaccion,
    pageIndex: number,
    pageSize: number,
    sortColumn: string,
    sortOrder: string
  ) {
    let filtros: FiltrosBaseModel = new FiltrosBaseModel();
    filtros.pageIndex = pageIndex;
    filtros.pageSize = pageSize;
    filtros.sortColumn = sortColumn;
    filtros.sortOrder = sortOrder;
    this.getArchivos(idTransaccion, filtros);
  }

  /**
   * Recarga directamente las archivos haciendo una llamada al servicio de archivos.
   * Éste debiera ser el último método llamado en la cadena de ejecución para este DataSource.
   */
  public getArchivos(idTransaccion, filtros?): void {
    //si ya está cargando archivos, evita sobrecargar las peticiones
    if (!this.cargaSubject.getValue()) {
      this.cargaSubject.next(true);

      this.localSvc
        .listarHistorialImportaciones(idTransaccion, filtros)
        .pipe(
          catchError(() => of([])),
          finalize(() => this.cargaSubject.next(false)) //termine bien o mal la peticion, la carga se detiene
        )
        .subscribe(
          (payload: PaginaRegistros<HistorialImportacionesModel>) => {
            this.archivosSubject.next(payload.items); //el listado de archivos se almacena
            if (this.archivosCountSubject.getValue() != payload.count) {
              this.archivosCountSubject.next(payload.count);
            }
          }
        );
    }
  }
}
