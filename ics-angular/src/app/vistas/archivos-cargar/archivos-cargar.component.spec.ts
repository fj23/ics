import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArchivosCargarComponent } from './archivos-cargar.component';

describe('ArchivosCargarComponent', () => {
  let component: ArchivosCargarComponent;
  let fixture: ComponentFixture<ArchivosCargarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArchivosCargarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArchivosCargarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
