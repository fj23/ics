import { Component, OnInit, OnDestroy } from "@angular/core";
import { FormGroup, FormBuilder } from "@angular/forms";
import { Observable, Subscription } from "rxjs";

import { FiltrosHomologacionesModel } from "src/models/filters/FiltrosHomologacionesModel";
import { SistemaCore } from "src/models/compartido/SistemaCore";
import { Socio } from "src/models/compartido/Socio";
import { HomologacionesHttpService } from "src/http-services/homologaciones.http-service";
import { CommonDataHttpService } from "src/http-services/common-data.http-service";

@Component({
  selector: "app-filtros-homologaciones",
  templateUrl: "./filtros.homologaciones.component.html",
  styleUrls: ["./filtros.homologaciones.component.css"]
})
export class FiltrosHomologacionesComponent implements OnInit, OnDestroy {
  public estadosVigencia = [
    { id: "Y", descripcion: "Vigente" },
    { id: "N", descripcion: "No Vigente" }
  ];

  public sistemas$: Observable<SistemaCore[]>;
  public socios$: Observable<Socio[]>;
  public conceptos$: Observable<String[]>;

  public formFiltros: FormGroup;

  private newHomologacionSub: Subscription;
  private changeFiltrosSub: Subscription;

  constructor(
    private localSvc: HomologacionesHttpService,
    private commonSvc: CommonDataHttpService,
    private fb: FormBuilder
  ) {
    this.formFiltros = this.fb.group({
      sistema1: [],
      socio1: [],
      sistema2: [],
      socio2: [],
      concepto: [],
      vigencia: []
    });
  }

  public get sistema1() { return this.formFiltros.get("sistema1"); }
  public get socio1() { return this.formFiltros.get("socio1"); }
  public get sistema2() { return this.formFiltros.get("sistema2"); }
  public get socio2() { return this.formFiltros.get("socio2"); }
  public get concepto() { return this.formFiltros.get("concepto"); }
  public get vigencia() { return this.formFiltros.get("vigencia"); }

  ngOnInit(): void {
    this.sistemas$ = this.commonSvc.getSistemasCore();
    this.socios$ = this.commonSvc.getSocios();
    this.conceptos$ = this.localSvc.getConceptosHomologacion();

    //al crear una homologacion pueden generar un nuevo concepto. esto los recarga.
    this.newHomologacionSub = this.localSvc.newHomologacionSubject.subscribe(
      () => {
        this.conceptos$ = this.localSvc.getConceptosHomologacion();
      }
    );

    this.changeFiltrosSub = this.localSvc.filtrosChange$.subscribe(
        () => {
            let filtros: FiltrosHomologacionesModel = this.localSvc.getFiltros();

            if (filtros.concepto !== this.concepto.value) { this.concepto.setValue(filtros.concepto); }
            if (filtros.sistema1 !== this.sistema1.value) { this.sistema1.setValue(filtros.sistema1); }
            if (filtros.socio1 !== this.socio1.value) { this.socio1.setValue(filtros.socio1); }
            if (filtros.sistema2 !== this.sistema2.value) { this.sistema2.setValue(filtros.sistema2); }
            if (filtros.socio2 !== this.socio2.value) { this.socio2.setValue(filtros.socio2); }
        }
    );
  }

  ngOnDestroy(): void {
    if (this.newHomologacionSub != null) this.newHomologacionSub.unsubscribe();
    if (this.changeFiltrosSub != null) this.changeFiltrosSub.unsubscribe();
  }

  /**
   * Genera un nuevo objeto de filtros y lo manda al servicio.
   */
  public submit(): void {
    let filtros: FiltrosHomologacionesModel = new FiltrosHomologacionesModel();

    filtros.sistema1 = this.sistema1.value;
    filtros.socio1 = this.socio1.value;
    filtros.sistema2 = this.sistema2.value;
    filtros.socio2 = this.socio2.value;
    filtros.concepto = this.concepto.value;
    filtros.vigencia = this.vigencia.value;

    let settedFiltros: FiltrosHomologacionesModel = this.localSvc.getFiltros();
    filtros.pageSize = settedFiltros.pageSize;
    filtros.sortColumn = settedFiltros.sortColumn;
    filtros.sortOrder = settedFiltros.sortOrder;

    this.localSvc.setFiltros(filtros);
  }
}
