import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable, of, Subscription, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { CommonDataHttpService } from 'src/http-services/common-data.http-service';

import { SistemaCore } from 'src/models/compartido/SistemaCore';
import { Socio } from 'src/models/compartido/Socio';
import { Homologacion } from 'src/models/homologaciones/Homologacion';
import { HomologacionesHttpService } from 'src/http-services/homologaciones.http-service';
import { FiltrosHomologacionesModel } from 'src/models/filters/FiltrosHomologacionesModel';

@Component({
    selector: 'app-formulario-homologaciones',
    templateUrl: './formulario.homologaciones.component.html',
    styleUrls: ['./formulario.homologaciones.component.css']
})
export class FormularioHomologacionComponent implements OnInit, OnDestroy {

    public sistemas$: Observable<SistemaCore[]>;
    public socios$: Observable<Socio[]>;
    public conceptos$: Observable<String[]>;
    
    public homologacionFormGroup: FormGroup;

    public homologacionId: number;
    
    private editingHomologacionSub: Subscription;

    constructor(
        private commonSvc: CommonDataHttpService,
        private localSvc: HomologacionesHttpService,
        private fb: FormBuilder,
        private snackBar: MatSnackBar
    ) {
        this.homologacionFormGroup = this.fb.group({
            socio1: [null, Validators.required],
            socio2: [null, Validators.required],
            concepto: [null, Validators.required],
            core1: [null, Validators.required],
            valor1: [null, Validators.required],
            core2: [null, Validators.required],
            valor2: [null, Validators.required]
        });
    }

    public get socio1() { return this.homologacionFormGroup.get("socio1"); }
    public get socio2() { return this.homologacionFormGroup.get("socio2"); }
    public get concepto() { return this.homologacionFormGroup.get("concepto"); }
    public get core1() { return this.homologacionFormGroup.get("core1"); }
    public get valor1() { return this.homologacionFormGroup.get("valor1"); }
    public get core2() { return this.homologacionFormGroup.get("core2"); }
    public get valor2() { return this.homologacionFormGroup.get("valor2"); }

    ngOnInit(): void {
        this.sistemas$ = this.commonSvc.getSistemasCore();
        this.socios$ = this.commonSvc.getSocios();
        this.conceptos$ = this.localSvc.getConceptosHomologacion();

        this.editingHomologacionSub = this.localSvc.editingTargetSubject.subscribe(
            (homologacion: Homologacion) => {
                if (homologacion) {
                    this.homologacionId = homologacion.id;
                    this.resetForm();
                    this.concepto.patchValue(homologacion.concepto);
                    this.socio1.patchValue(homologacion.socio1);
                    this.socio2.patchValue(homologacion.socio2);
                    this.core1.patchValue(homologacion.core1);
                    this.core2.patchValue(homologacion.core2);
                    this.valor1.patchValue(homologacion.valor1.toUpperCase());
                    this.valor2.patchValue(homologacion.valor2.toUpperCase());
                }
                else {
                    this.homologacionId = null;
                    this.resetForm();
                }
            }
        );
    }

    ngOnDestroy(): void {
        this.editingHomologacionSub.unsubscribe();
    }

    private resetForm(): void {
        this.homologacionFormGroup.reset();
        let noChange = { onlySelf: true, emitEvent: false };
        if (this.homologacionId) {
            this.concepto.disable(noChange);
            this.socio1.disable(noChange);
            this.socio2.disable(noChange);
            this.core1.disable(noChange);
            this.core2.disable(noChange);
        }
        else {
            this.concepto.enable(noChange);
            this.socio1.enable(noChange);
            this.socio2.enable(noChange);
            this.core1.enable(noChange);
            this.core2.enable(noChange);
        }
    }

    /**
     * Envía los datos del formulario como un objeto.
     */
    public onSubmit(): void {
        if (!this.homologacionFormGroup.valid) {
            for (const ctrlId in this.homologacionFormGroup.controls) {
                this.homologacionFormGroup.controls[ctrlId].markAsTouched();
            }
            this.snackBar.open("Debe completar todos los datos en el formulario.");
        }
        else if (this.socio1.value.id === this.socio2.value.id && this.core1.value.id === this.core2.value.id) {
            this.snackBar.open("Los socios y sistemas de origen y destino no pueden ser idénticos.");
        }
        else {
            let val1: string = this.valor1.value;
            let val2: string = this.valor2.value;
            let homologacion: Homologacion = {
                id: 0,
                idTipo: 0,
                concepto: this.concepto.value,
                vigencia: "Y",
                socio1: this.socio1.value,
                core1: this.core1.value,
                valor1: val1.toUpperCase(),
                socio2: this.socio2.value,
                core2: this.core2.value,
                valor2: val2.toUpperCase()
            }

            if (this.homologacionId) {
                homologacion.id = this.homologacionId;
                this.localSvc.actualizar(homologacion)
                    .pipe(
                        catchError(this.handleError)
                    ).subscribe(
                        () => {
                            this.localSvc.newHomologacionSubject.next();
                            this.snackBar.open("Homologación actualizada con éxito.");
                            this.recargarTablaConNuevaHomologacion(homologacion);
                        },
                        (err) => {
                            console.error(err);
                            this.snackBar.open("Error no se pudo actualizar la homologación");
                        }
                    );
            }
            else {
                this.localSvc.crear(homologacion)
                    .pipe(
                        catchError(this.handleError)
                    ).subscribe(
                        () => {
                            this.localSvc.editingTargetSubject.next(homologacion);
                            this.localSvc.newHomologacionSubject.next();
                            this.recargarTablaConNuevaHomologacion(homologacion);
                            this.snackBar.open("Homologación creada con éxito.");
                        },
                        err => {
                            console.error(err);
                            
                            if (err.status === 406 ) {
                                this.snackBar.open("La homologación con esos datos ya existe, por favor cambie los datos");
                            } else {
                                this.snackBar.open("Error no se pudo crear la homologación");
                            }
                        }
                    );
            }
        }
    }

    private recargarTablaConNuevaHomologacion(homologacion: Homologacion): void {
        let filtro: FiltrosHomologacionesModel  = new FiltrosHomologacionesModel();
        filtro.sistema1 = homologacion.core1.id;
        filtro.sistema2 = homologacion.core2.id;
        filtro.socio1 = homologacion.socio1.id;
        filtro.socio2 = homologacion.socio2.id;
        filtro.concepto = homologacion.concepto;
        this.localSvc.setFiltros(filtro);
    }

    private handleError(error) {
        if (error.error instanceof ErrorEvent) {
            // client-side error
            error = error.error;
        }

        return throwError(error);
    }

}
