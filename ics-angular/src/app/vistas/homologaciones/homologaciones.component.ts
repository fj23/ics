import { Component, OnInit, OnDestroy, ViewChild } from "@angular/core";
import { MatDialog } from "@angular/material";
import { Subscription, Observable } from "rxjs";
import { FormularioHomologacionComponent } from "./creacion/formulario.homologaciones.component";
import { ImportarHomologacionesComponent } from "./importar/importar.homologaciones.component";
import { Title } from "@angular/platform-browser";
import { HomologacionesHttpService } from "src/http-services/homologaciones.http-service";
import { FormCanDeactivate } from "src/services/routing/form-can-deactivate";
import { Permisos } from "src/app/compartido/Permisos";
import { Homologacion } from "src/models/homologaciones/Homologacion";
import { ConfirmationDialogComponent } from "src/app/dialogs/confirmation/confirmation.dialog.component";

@Component({
  selector: "app-homologaciones",
  templateUrl: "./homologaciones.component.html",
  styleUrls: ["./homologaciones.component.css"],
  providers: [HomologacionesHttpService]
})
export class HomologacionesComponent extends FormCanDeactivate
  implements OnInit, OnDestroy {
  /**
   * Acción elegida en el mantenedor. Según esta se muestran y ocultan sub-componentes de este módulo.
   */
  public selectedAction: string = "filtrar";
  public ocultarPorPermisos: boolean;
  private newHomologacionSub: Subscription;

  @ViewChild(FormularioHomologacionComponent)
  formChild: FormularioHomologacionComponent;
  @ViewChild(ImportarHomologacionesComponent)
  formImportar: ImportarHomologacionesComponent;

  get canDeactivateForm(): boolean {
    if (this.formChild != null) {
      if (
        this.formChild.homologacionFormGroup.dirty ||
        this.formChild.homologacionFormGroup.touched
      ) {
        return false;
      }
    } else if (this.formImportar != null) {
      if (
        this.formImportar.importarHomologacionesForm.dirty ||
        this.formImportar.importarHomologacionesForm.touched
      ) {
        return false;
      }
    }
    return true;
  }

  constructor(
    private localSvc: HomologacionesHttpService,
    private title: Title,
    public dialog: MatDialog
  ) {
    super();
    this.title.setTitle("ICS | Homologaciones");
    this.ocultarPorPermisos = Permisos.DenegarAcceso(Permisos.MantenerEstructura());
  }

  ngOnInit(): void {
    this.newHomologacionSub = this.localSvc.newHomologacionSubject.subscribe(
      () => {
        this.selectedAction = "filtrar";
        //this.localSvc.setFiltros(new FiltrosHomologacionesModel());
      }
    );
  }

  ngOnDestroy(): void {
    if (this.newHomologacionSub) this.newHomologacionSub.unsubscribe();
  }

  /**
   * Cuando se desea cambiar el sub-componente mostrado sobre la tabla del mantenedor.
   * @param action La acción que se llama.
   */
  public onClickAccion(action: string): void {
    //si se estaba editando una homologación, se pide confirmar para no perder los cambios hechos
    if (
      this.selectedAction === "editar" &&
      this.localSvc.editingTargetSubject.getValue()
    ) {
      this.dialogoConfirmacionEditarHomologacion().subscribe(
        (confirmed: boolean) => {
          if (confirmed) {
            //si el usuario confirmó querer dejar de editar y elige crear (el botón para crear llama a 'editar')
            //significa que quiere crear una nueva homologa ción; sólo cambia la homologación activa por una vacía.
            if (action === "editar") {
              this.localSvc.editingTargetSubject.next(null);
            } else {
              //si la misma accion ya estaba seleccionada, se deselecciona
              this.selectedAction =
                this.selectedAction !== action ? action : "";
              this.localSvc.editingTargetSubject.next(null);
            }
          }
        }
      );
    } else {
      //si la misma accion ya estaba seleccionada, se minimiza componente
      this.selectedAction = this.selectedAction !== action ? action : "";
      if (action === "editar") {
        this.localSvc.editingTargetSubject.next(null);
      }
    }
  }

  /**
   * Cuando se quiere editar una homologación.
   * @param homologacion La homologación que se desea editar.
   */
  public onClickEditHomologacion(homologacion: Homologacion): void {
    //si ya se estaba editando -otra- homologación, se pregunta al usuario si realmente quiere dejar de lado la anterior antes
    if (this.selectedAction === "editar") {
      this.dialogoConfirmacionEditarHomologacion().subscribe(
        (confirmed: boolean) => {
          if (confirmed) {
            this.localSvc.editingTargetSubject.next(homologacion);
          }
        }
      );
    } else {
      this.selectedAction = "editar";
      this.localSvc.editingTargetSubject.next(homologacion);
    }
  }

  /**
   * Genera un diálogo que pide confirmación para soltar la homologación que esté siendo creada o editada.
   */
  private dialogoConfirmacionEditarHomologacion(): Observable<any> {
    let mensaje: string;
    if (this.localSvc.editingTargetSubject.getValue()) {
      mensaje =
        "Una homologación está siendo editada. Los cambios que haya realizado en ésta se perderán. ¿Está seguro/a que desea salir?";
    } else {
      mensaje =
        "Una homologación está siendo creada. Los datos de ésta se perderán. ¿Está seguro/a que desea salir?";
    }

    const confirmationDialog = this.dialog.open(ConfirmationDialogComponent, {
      data: {
        titulo: "Descartar Acción Actual",
        mensaje: mensaje,
        boton_si_clases: "btn-warning",
        boton_no_clases: ""
      }
    });

    return confirmationDialog.beforeClosed();
  }
}
