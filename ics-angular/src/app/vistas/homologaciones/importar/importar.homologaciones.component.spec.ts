import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportarHomologacionesComponent } from './importar.homologaciones.component';

describe('ImportarHomologacionesComponent', () => {
  let component: ImportarHomologacionesComponent;
  let fixture: ComponentFixture<ImportarHomologacionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImportarHomologacionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportarHomologacionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
