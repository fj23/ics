import { DataSource } from "@angular/cdk/table";
import { OnDestroy } from '@angular/core';
import { CollectionViewer } from "@angular/cdk/collections";
import { Observable, BehaviorSubject, of, Subscription, Subject } from 'rxjs';
import { catchError, finalize, tap } from 'rxjs/operators';

import { PaginaRegistros } from 'src/models/compartido/PaginaRegistros';
import { Homologacion } from 'src/models/homologaciones/Homologacion';
import { HomologacionesHttpService } from "src/http-services/homologaciones.http-service";
import { FiltrosHomologacionesModel } from "src/models/filters/FiltrosHomologacionesModel";
	
export class HomologacionesDataSource implements OnDestroy, DataSource<Homologacion> {
	
	private cargaSubject = new BehaviorSubject<boolean>(false);

	/** Recibe y transmite el array de homologaciones activo a la vista. */
    private homologacionesSubject = new Subject<Homologacion[]>();
    public homologaciones$: Observable<Homologacion[]> = this.homologacionesSubject.asObservable();
	
	/** Recibe y transmite la cantidad de homologaciones conseguidas por los filtros activos.
	 * Esta cantidad suele superar el número de registros en la página actual. */
	private homologacionesCountSubject = new BehaviorSubject<number>(0);
	
	
	//observables
	public homologacionesCount$ = this.homologacionesCountSubject.asObservable();
	public carga$ = this.cargaSubject.asObservable();
	
	private filtrosChangeSub: Subscription;
	
	constructor(
		private localSvc: HomologacionesHttpService
	) { 
		//cuando los filtros, el paginado y/o el orden de la tabla cambien, obtiene las homologaciones
		this.filtrosChangeSub = this.localSvc.filtrosChange$.subscribe(() => { this.getHomologaciones() });
	}

	ngOnDestroy(): void {
		this.filtrosChangeSub.unsubscribe();
	}

	connect(collectionViewer: CollectionViewer): Observable<Homologacion[] | ReadonlyArray<Homologacion>> {
		return this.homologacionesSubject.asObservable();
	}    
	
	disconnect(collectionViewer: CollectionViewer): void {
		this.homologacionesSubject.complete();
		this.cargaSubject.complete();
	}

	/**
	 * Solicita la página de homologaciones respectiva.
	 * @param pageIndex El índice (con base 0) de la página.
	 * @param pageSize La cantidad de registros a mostrar por página.
	 */
	public getHomologacionesPage(pageIndex: number, pageSize: number, sortColumn: string, sortOrder: string): void {
		let filtros: FiltrosHomologacionesModel = this.localSvc.getFiltros();
		filtros.pageIndex = pageIndex;
		filtros.pageSize = pageSize;
		filtros.sortColumn = sortColumn;
		filtros.sortOrder = sortOrder;
		
		this.localSvc.setFiltros(filtros);
	}

	/**
	 * Recarga directamente las homologaciones haciendo una llamada al servicio de homologaciones.
	 * Éste debiera ser el último método llamado en la cadena de ejecución para este DataSource.
	 */
	private getHomologaciones(): void {

		//si ya está cargando homologaciones, evita sobrecargar las peticiones
		if (!this.cargaSubject.getValue()) {

			this.cargaSubject.next(true);

			this.localSvc.listar()
			.pipe(
				catchError(() => of([])),
				finalize(() => this.cargaSubject.next(false)) //termine bien o mal la peticion, la carga se detiene
			).subscribe(
				(payload: PaginaRegistros<Homologacion>) =>  {
					this.homologacionesSubject.next(payload.items); //el listado de homologaciones se almacena
					if (this.homologacionesCountSubject.getValue() != payload.count) { 
						this.homologacionesCountSubject.next(payload.count);
					}
				}
			);
		}
	}
	
}
