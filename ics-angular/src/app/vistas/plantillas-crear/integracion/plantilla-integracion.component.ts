import { Component, OnInit, ViewChild, Input, OnDestroy, ApplicationRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatTable, MatSnackBar, MatDialog, MatTableDataSource } from '@angular/material';
import { Observable, Subscription, of, Subject, BehaviorSubject } from 'rxjs';

import { FormularioPlantillasService } from '../../../../services/formulario-plantillas/formulario-plantillas.service';

import { ColumnaPlantillaIntegracionComponent } from './columna/columna-trabajo-integracion.component';
import { ConfirmationDialogComponent } from 'src/app/dialogs/confirmation/confirmation.dialog.component';

import { Evento } from 'src/models/compartido/Evento';
import { SistemaCore } from 'src/models/compartido/SistemaCore';
import { ColumnaTrabajoIntegracion } from 'src/models/plantillas/integracion/ColumnaTrabajoIntegracion';
import { TipoSalidaIntegracion } from 'src/models/compartido/TipoSalidaIntegracion';
import { FormatoArchivo } from 'src/models/compartido/FormatoArchivo';
import { MapeoIntegracion } from 'src/models/plantillas/integracion/MapeoIntegracion';
import { ColumnaDestinoIntegracion } from 'src/models/plantillas/integracion/ColumnaDestinoIntegracion';
import { ColumnaArchivoIntegracion } from 'src/models/plantillas/integracion/ColumnaArchivoIntegracion';
import { TipoExtensionEnum } from 'src/enums/TipoExtensionEnum';
import { Plantilla } from 'src/models/plantillas/Plantilla';
import { ColumnaDestinoEntrada } from 'src/models/plantillas/entrada/ColumnaDestinoEntrada';
import { Socio } from 'src/models/compartido/Socio';
import { CommonDataHttpService } from 'src/http-services/common-data.http-service';
import { PlantillasHttpService } from 'src/http-services/plantillas.http-service';
import { FontawesomeIconsService } from 'src/services/fontawesome-icons/fontawesome-icons.service';
import { TipoSalidaEnum } from '../../../../enums/TipoSalidaEnum';


@Component({
    selector: 'app-plantilla-integracion',
    templateUrl: './plantilla-integracion.component.html',
    styleUrls: ['./plantilla-integracion.component.css']
})
export class FormularioPlantillaIntegracionComponent implements OnInit, OnDestroy {

    public evento: Evento; //para recargar campos de mapeo disponibles
    @Input() public set Evento(evento: Evento) {
        this.evento = evento;
        this.recargarPlantillasEntrada();
    }
    
    public socio: Socio; //para pasar al formulario de col. de trabajo, para cargar homologaciones
    @Input() public set Socio(socio: Socio) {
        this.socio = socio;
        this.recargarPlantillasEntrada();
    }

    public tiposSalida$: Observable<TipoSalidaIntegracion[]>;
    public formatosSalida$: Observable<FormatoArchivo[]>;
    public sistemas$: Observable<SistemaCore[]>;


    public plantillasEntrada$: Observable<Plantilla[]>;
    public columnasOrigen: ColumnaDestinoEntrada[] = null;
    public columnasOrigen$: Observable<ColumnaDestinoEntrada[]> = null;

    public camposMapeoDisponibles$: Observable<ColumnaDestinoIntegracion[]>;

    public creacionPlantillaIntegracionForm: FormGroup;

    @ViewChild('tablaColumnasTrabajo') protected tablaColumnasTrabajo: MatTable<ColumnaTrabajoIntegracion>;
    public columnasTrabajoDisplayedColumns: string[];
    public columnasTrabajo: ColumnaTrabajoIntegracion[] = [];
    @ViewChild('formularioColumnaTrabajo') public formularioColumnaTrabajo: ColumnaPlantillaIntegracionComponent;
    public columnaSeleccionada: ColumnaTrabajoIntegracion;

    @ViewChild('tablaMapeosIntegracion') protected tablaMapeosIntegracion: MatTable<MapeoIntegracion>;
    public columnasIntegracionDisplayedColumns: string[];
    protected _mapeosIntegracion: MapeoIntegracion[] = [];
    public get mapeosIntegracion() { return this._mapeosIntegracion; }
    protected _mapeosIntegracion$: Subject<MapeoIntegracion[]>;
    public mapeosIntegracion$: Observable<MapeoIntegracion[]>;
    
    @ViewChild('tablaColumnasArchivoIntegracion') protected tablaColumnasArchivoIntegracion: MatTable<ColumnaArchivoIntegracion>;
    public columnasArchivoIntegracionDisplayedColumns: string[];
    protected _columnasArchivoIntegracion: ColumnaArchivoIntegracion[] = [];
    public get columnasArchivoIntegracion() { return this._columnasArchivoIntegracion; }
    protected _columnasArchivoIntegracion$: Subject<ColumnaArchivoIntegracion[]>;
    public columnasArchivoIntegracion$: Observable<ColumnaArchivoIntegracion[]>;
    public columnaArchivoSeleccionada: ColumnaArchivoIntegracion;

    protected changeFiltrarOrigenesPlantillaSub: Subscription;
    protected changePlantillaFiltroSub: Subscription;
    protected tipoSalidaChangeSub: Subscription;
    protected formatoSalidaChangeSub: Subscription;
    protected coreChangeSub: Subscription;

    constructor(
        protected commonSvc: CommonDataHttpService,
        protected localSvc: PlantillasHttpService,
        protected formSvc: FormularioPlantillasService,
        protected fb: FormBuilder,
        protected snackBar: MatSnackBar,
        protected dialog: MatDialog,
        protected appRef: ApplicationRef,
        public icons: FontawesomeIconsService
    ) {
        this.columnasTrabajoDisplayedColumns = ['nombre', 'acciones'];
        this.columnasIntegracionDisplayedColumns = ['nombre', 'origen', 'obligatorio', 'validar', 'acciones'];
        this.columnasArchivoIntegracionDisplayedColumns = ['origen', 'separar', 'incluir', 'acciones'];

        this.formSvc.completitudFormularioSubject.next(false);
        this.creacionPlantillaIntegracionForm = this.fb.group({
            tipoSalida: [null, Validators.required],
            core: [null, Validators.required],
            filtrarOrigenesPlantilla: [false],
            plantillaFiltro: [{value: null, disabled: true}],
            formatoSalida: [{ value: null, disabled: true }],
            delimitadorColumnas: [{ value: '', disabled: true }],
            columnaDestinoIntegracion: [null],
            incluirEncabezados: [{ value: false, disabled: true }]
        });
        

        this.changeFiltrarOrigenesPlantillaSub = this.filtrarOrigenesPlantilla.valueChanges.subscribe(() => { this.onChangeFiltrarOrigenesPlantilla(); });
        this.changePlantillaFiltroSub = this.plantillaFiltro.valueChanges.subscribe(() => { this.onChangePlantillaFiltro(); });
        
        this.tipoSalidaChangeSub = this.tipoSalida.valueChanges.subscribe(() => { this.onChangeTipoSalida(); });
        this.formatoSalidaChangeSub = this.formatoSalida.valueChanges.subscribe(() => { this.onChangeFormatoSalidaManual(); });
        this.coreChangeSub = this.core.valueChanges.subscribe(() => { this.onChangeCore(); });

        this._columnasArchivoIntegracion$ = new BehaviorSubject(this._columnasArchivoIntegracion);
        this.columnasArchivoIntegracion$ = this._columnasArchivoIntegracion$.asObservable();

        this._mapeosIntegracion$ = new BehaviorSubject(this._mapeosIntegracion);
        this.mapeosIntegracion$ = this._mapeosIntegracion$.asObservable();
    }

    public get tipoSalida() { return this.creacionPlantillaIntegracionForm.get("tipoSalida"); }
    public get core() { return this.creacionPlantillaIntegracionForm.get("core"); }
    public get filtrarOrigenesPlantilla() { return this.creacionPlantillaIntegracionForm.get("filtrarOrigenesPlantilla"); }
    public get plantillaFiltro() { return this.creacionPlantillaIntegracionForm.get("plantillaFiltro"); }
    public get formatoSalida() { return this.creacionPlantillaIntegracionForm.get("formatoSalida"); }
    public get delimitadorColumnas() { return this.creacionPlantillaIntegracionForm.get("delimitadorColumnas") as FormControl; }
    public get columnaDestinoIntegracion() { return this.creacionPlantillaIntegracionForm.get("columnaDestinoIntegracion"); }
    public get incluirEncabezados() { return this.creacionPlantillaIntegracionForm.get("incluirEncabezados"); }

    public get columnaTrabajoSinMapeos() {
        return this.columnasTrabajo.find(
            (colTrabajo) => { 
                return !this.columnaTrabajoTieneMapeos(colTrabajo); 
            }
        );
    }

    public get esIntegracionManual(): boolean {
        return (this.tipoSalida.value && this.tipoSalida.value.idTipoSalida === TipoSalidaEnum.MANUAL && this.formatoSalida.value);
    }

    ngOnInit() {
        this.tiposSalida$ = this.commonSvc.getTiposSalidaIntegracion();
        this.sistemas$ = this.commonSvc.getSistemasCore();
        this.formatosSalida$ = this.localSvc.getTiposExtensionEstandar();
    }

    ngOnDestroy(): void {
        this.changePlantillaFiltroSub.unsubscribe();
        this.tipoSalidaChangeSub.unsubscribe();
        this.formatoSalidaChangeSub.unsubscribe();
    }

    protected recargarPlantillasEntrada(): void {
        if (this.socio && this.evento) {
            this.plantillasEntrada$ = this.localSvc.getPlantillasEntrada(this.evento.id, this.socio.id);
        }
    }

    protected quitarColumnaTrabajo(index: number) {
        if (this.columnasTrabajo[index] === this.columnaSeleccionada) {
            this.columnaSeleccionada = null;
        }

        this.columnasTrabajo.splice(index, 1);
        this.actualizarOrdenColumnasTrabajo();
        this.tablaColumnasTrabajo.renderRows();

        if (this.columnasTrabajo.length === 0) {
            this.formSvc.bloquearDatosBasicosPlantillaSubject.next(false);
        }
    }

    protected actualizarOrdenColumnasTrabajo(): void {
        for (let i = 0; i < this.columnasTrabajo.length; i++) {
            this.columnasTrabajo[i].orden = i+1;
        }
    }

    protected actualizarOrdenColumnasArchivoIntegracion() {
        //TODO corregir orden considerando columnas que no se incluyen en el archivo
        for (let i = 0; i < this._columnasArchivoIntegracion.length; i++) {
            const col = this._columnasArchivoIntegracion[i];
            col.orden = i+1;
        }
    }

    protected actualizarValidezPlantilla() {
        if (this.tipoSalida.value) {

            const estanTodasLasColumnasTrabajoMapeadas = (this.columnasTrabajo.length > 0) && 
                this.columnasTrabajo.every(
                    (col: ColumnaTrabajoIntegracion) => {
                        return col.mapeada;
                    }
                );
            let plantillaValida: boolean = estanTodasLasColumnasTrabajoMapeadas; 
            const tipoSalidaId = this.tipoSalida.value.idTipoSalida;

            // si se ha elegido un tipo de salida manual; con archivo
            if (tipoSalidaId === 5000002) {
                const hayColumnasSalidaArchivo = (this._columnasArchivoIntegracion.length > 0);
                plantillaValida = estanTodasLasColumnasTrabajoMapeadas && hayColumnasSalidaArchivo;
            }
            
            this.formSvc.completitudFormularioSubject.next(plantillaValida);
        } else {
            this.formSvc.completitudFormularioSubject.next(false);
        }
    }
    
    public columnaTrabajoTieneMapeos(colTrabajo: ColumnaTrabajoIntegracion): boolean {        
        return this._mapeosIntegracion.some(
            (mapeo: MapeoIntegracion) => {
                return mapeo.columnaOrigen.orden === colTrabajo.orden;
            }
        );
    }

    public todasLasColumnasArchivoTienenPosiciones(): boolean {
        return this._columnasArchivoIntegracion.every(
            (col: ColumnaArchivoIntegracion) => {
                return col.tienePosicionInicialFinal;
            }
        );
    }

    protected reiniciarDatosColumnasDependientesDeCore(): void {
        this.columnasTrabajo = this.columnasTrabajo.map(
            c => {
                c.columnaEnriquecimiento = null;
                c.homologacion = null;
                return c;
            }
        );
    }

    protected onChangeCore(): void {
        if (this.columnaSeleccionada) {
            const colT = this.columnaSeleccionada;
            this.columnaSeleccionada = null;
            this.appRef.tick();
        
            this.reiniciarDatosColumnasDependientesDeCore();

            this.columnaSeleccionada = colT;
        } else {
            this.reiniciarDatosColumnasDependientesDeCore();
        }
    }

    public onChangeFiltrarOrigenesPlantilla(): void {
        const noChange = { onlySelf: true, emitEvent: false };
        if (this.filtrarOrigenesPlantilla.value) {
            this.plantillaFiltro.enable(noChange);
        } else {
            this.plantillaFiltro.disable(noChange);
            this.plantillaFiltro.setValue(null, noChange);
            this.onChangePlantillaFiltro();
        }
    }

    public onChangePlantillaFiltro(): void {
        if (this.plantillaFiltro.value) {
            const plantilla = this.plantillaFiltro.value;
            if (plantilla.id) {
                this.localSvc.getColumnasOrigenIntegracionDesdePlantillaEntrada(plantilla.id).subscribe( 
                    (data: ColumnaDestinoEntrada[]) => {
                        if (data && data.length > 0) {
                            let columnasOrigen: ColumnaDestinoEntrada[] = [];
                            columnasOrigen.push(...data);
                            this.columnasOrigen = columnasOrigen;
                            this.columnasOrigen$ = of(columnasOrigen);
                        } else {
                            this.recargarPlantillasEntrada();
                        }
                    }
                );
                return;
            }
        }

        this.columnasOrigen = null;
        this.columnasOrigen$ = null;
    }

    public onClickAgregarColumnaTrabajo(): void {
        let columna: ColumnaTrabajoIntegracion = new ColumnaTrabajoIntegracion();
        
        columna.orden = this.columnasTrabajo.length + 1;
        columna.nombre = "Sin nombre [" + columna.orden + "]";
        this.columnasTrabajo.push(columna);
        this.tablaColumnasTrabajo.renderRows();

        this.columnaSeleccionada = columna;

        // al agregar una c. de trabajo, ésta no tiene mapeos, por ello la consideramos inmediatamente "incompleta"
        this.formSvc.completitudFormularioSubject.next(false);

        // al agregar una c. de trabajo, bloqueamos la información general de la plantilla
        // y recargan las columnas de destino en base al evento seleccionado
        if (this.columnasTrabajo.length === 1) {
            this.formSvc.bloquearDatosBasicosPlantillaSubject.next(true);
            this.camposMapeoDisponibles$ = this.localSvc.getColumnasDestinoIntegracionDisponibles(this.evento.id);
        }
    }
    
    public onClickEditarColumnaTrabajo(index: number): void {
        let seleccion: ColumnaTrabajoIntegracion = this.columnasTrabajo[index];
        
        if (this.columnaSeleccionada !== seleccion) {
            this.columnaSeleccionada = seleccion;
        } else {
            this.columnaSeleccionada = null;
        }
    }

    public onClickQuitarColumnaTrabajo(index: number): void {
        if (index <= this.columnasTrabajo.length - 1) {
            if (this.columnasTrabajo[index].mapeada) {
                this.snackBar.open("No puede eliminar una columna mapeada. Primero elimine el mapeo realizado.");
            } else {

                this.dialog.open(ConfirmationDialogComponent, {
                    data: {
                        titulo: "Eliminar Columna",
                        mensaje: "Si quita esta columna, no podrá recuperar la configuración efectuada en ella. ¿Desea confirmar esta acción?",
                        boton_si_clases: "btn-warning",
                        boton_no_clases: ""
                    }
                }).afterClosed().subscribe(
                    (confirmed: boolean) => {
                        if (confirmed) {
                            this.quitarColumnaTrabajo(index);
                            this.actualizarValidezPlantilla();
                        }
                    }
                );
            }
        }
    }

    public onChangeValidar(row: MapeoIntegracion) {
        if (row.validar && row.obligatorio) {
            row.obligatorio = false;
        }
    }

    public onChangeObligatorio(row: MapeoIntegracion) {
        if (row.validar && row.obligatorio) {
            row.validar = false;
        }
    }

    public onChangeTipoSalida(): void {
        const noChange = { onlySelf: true, emitEvent: false };
        if (this.tipoSalida.value && this.tipoSalida.value.idTipoSalida === TipoSalidaEnum.MANUAL) {
            this.formatoSalida.enable(noChange);
            this.formatoSalida.setValidators(Validators.required);
            this.incluirEncabezados.enable();
        } else {
            this.delimitadorColumnas.setValidators(null);
            this.formatoSalida.setValidators(null);
            this.formatoSalida.reset({value: null, disabled: true}, noChange);
            this.delimitadorColumnas.reset({value: null, disabled: true}, noChange);
            this.incluirEncabezados.reset({value: false, disabled: true}, noChange);
        }
        this.formatoSalida.updateValueAndValidity({emitEvent: false});
        this.actualizarValidezPlantilla();
    }

    public onChangeFormatoSalidaManual(): void {
        if (this.formatoSalida.value &&
            (
                this.formatoSalida.value.id === TipoExtensionEnum.CSV ||
                this.formatoSalida.value.id === TipoExtensionEnum.PLANO
            )
        ) { //'CSV' o 'TXT'
        
            this.delimitadorColumnas.enable({ onlySelf: true, emitEvent: false });
            if (this.formatoSalida.value.id === TipoExtensionEnum.PLANO) {
                this.delimitadorColumnas.setValidators(null);
                this.columnasArchivoIntegracionDisplayedColumns = ['origen', 'posicion', 'separar', 'incluir', 'acciones'];
            } else {
                this.delimitadorColumnas.setValidators(Validators.required);
                this.columnasArchivoIntegracionDisplayedColumns = ['origen', 'separar', 'incluir', 'acciones'];
            }
        } else {
            this.delimitadorColumnas.disable({ onlySelf: true, emitEvent: false });
            this.delimitadorColumnas.setValue('');
            this.delimitadorColumnas.setValidators(null);
            this.columnasArchivoIntegracionDisplayedColumns = ['origen', 'separar', 'incluir', 'acciones'];
        }
        this.delimitadorColumnas.updateValueAndValidity();
        this.actualizarValidezPlantilla();
    }

    public onClickMapearIntegracion(): void { 
        
        this.formularioColumnaTrabajo.columnaPlantillaIntegracionForm.updateValueAndValidity();
        this.creacionPlantillaIntegracionForm.updateValueAndValidity();

        if (!this.columnaSeleccionada) {
            this.snackBar.open("Debe seleccionar una columna de origen.");
        } else if (this.formularioColumnaTrabajo.columnaPlantillaIntegracionForm.invalid) {
            this.snackBar.open("Se deben completar todos los datos requeridos de la columna de trabajo para mapearla.");
        } else if (!this.columnaDestinoIntegracion.value) {
            this.snackBar.open("Debe seleccionar una columna de destino de integración.");
        } else if (this.creacionPlantillaIntegracionForm.invalid) {
            this.snackBar.open("Debe completar los campos requeridos.");
        } else if (this._mapeosIntegracion.some( (mapeo: MapeoIntegracion) => { return mapeo.columnaOrigen === this.columnaSeleccionada && mapeo.columnaDestino === this.columnaDestinoIntegracion.value; } )) {
            this.snackBar.open("No puede realizar el mismo mapeo más de una vez.");
        } else {
            this.columnaSeleccionada.mapeada = true;

            let mapeo: MapeoIntegracion = new MapeoIntegracion();
            mapeo.columnaOrigen = this.columnaSeleccionada;
            mapeo.columnaDestino = this.columnaDestinoIntegracion.value;

            this.columnaDestinoIntegracion.setValue(null);
            this._mapeosIntegracion.push(mapeo);
            this._mapeosIntegracion$.next(this._mapeosIntegracion);
        }

        this.actualizarValidezPlantilla();
    }

    public onClickAgregarMapeoArchivoIntegracion(index: number) {
        this._mapeosIntegracion[index].enArchivoSalida = true;
        let columnaArchivoNueva = new ColumnaArchivoIntegracion();
        columnaArchivoNueva.origen = this._mapeosIntegracion[index];
        this._columnasArchivoIntegracion.push(columnaArchivoNueva);
        this._columnasArchivoIntegracion$.next(this._columnasArchivoIntegracion);

        this.actualizarOrdenColumnasArchivoIntegracion();
        this.actualizarValidezPlantilla();
    }

    public onClickQuitarMapeoIntegracion(index: number) {
        const mapeo = this._mapeosIntegracion[index];
        const colTrabajo = mapeo.columnaOrigen;
        if (mapeo.enArchivoSalida) {
            this.snackBar.open("No puede remover un mapeo agregado al archivo de salida. Primero elimínelo de dicha grilla.");
        } else {
            this._mapeosIntegracion.splice(index, 1);
            this._mapeosIntegracion$.next(this._mapeosIntegracion);

            const mapeosParaColTrabajo: number = this._mapeosIntegracion.filter(m => m.columnaOrigen===colTrabajo).length;
            colTrabajo.mapeada = (mapeosParaColTrabajo > 0);
            
            this.actualizarValidezPlantilla();
        }
    }

    public onClickDetallesColumnaArchivoIntegracion(index: number) {
        if (this._columnasArchivoIntegracion[index] === this.columnaArchivoSeleccionada) {
            this.columnaArchivoSeleccionada = null;
        } else {
            this.columnaArchivoSeleccionada = this._columnasArchivoIntegracion[index];
        }

    }

    public onClickSubirColumnaArchivoIntegracion(index: number) {
        if (index > 0) {
            let estaColumna = this.columnasArchivoIntegracion[index];
            let columnaSuperior = this.columnasArchivoIntegracion[index - 1];
            this.columnasArchivoIntegracion[index - 1] = estaColumna;
            this.columnasArchivoIntegracion[index] = columnaSuperior;
            this._columnasArchivoIntegracion$.next(this._columnasArchivoIntegracion);
        
            this.actualizarOrdenColumnasArchivoIntegracion();
            this.actualizarValidezPlantilla();
        }
    }

    public onClickBajarColumnaArchivoIntegracion(index: number) {
        if (index < this.columnasArchivoIntegracion.length - 1) {
            let estaColumna = this.columnasArchivoIntegracion[index];
            let columnaInferior = this.columnasArchivoIntegracion[index + 1];
            this.columnasArchivoIntegracion[index + 1] = estaColumna;
            this.columnasArchivoIntegracion[index] = columnaInferior;
            this._columnasArchivoIntegracion$.next(this._columnasArchivoIntegracion);
        
            this.actualizarOrdenColumnasArchivoIntegracion();
            this.actualizarValidezPlantilla();
        }
    }

    public onClickQuitarColumnaArchivoIntegracion(index: number) {
        this._columnasArchivoIntegracion[index].origen.enArchivoSalida = false;
        this._columnasArchivoIntegracion.splice(index, 1);
        this._columnasArchivoIntegracion$.next(this._columnasArchivoIntegracion);
        
        this.actualizarOrdenColumnasArchivoIntegracion();
        this.actualizarValidezPlantilla();
    }
}
