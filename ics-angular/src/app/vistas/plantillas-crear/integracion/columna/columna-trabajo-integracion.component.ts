import { Component, OnInit, ViewChild, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatTable, MatSnackBar, MatDialog } from '@angular/material';
import { Observable, Subscription, of, Subject, BehaviorSubject } from 'rxjs';
import { ColumnaTrabajoIntegracion } from 'src/models/plantillas/integracion/ColumnaTrabajoIntegracion';
import { ColumnaDestinoEntrada } from 'src/models/plantillas/entrada/ColumnaDestinoEntrada';
import { Evento } from 'src/models/compartido/Evento';
import { Socio } from 'src/models/compartido/Socio';
import { SistemaCore } from 'src/models/compartido/SistemaCore';
import { AtributoEnriquecimiento } from 'src/models/plantillas/AtributoEnriquecimiento';
import { TipoFormato } from 'src/models/compartido/TipoFormato';
import { Homologacion } from 'src/models/homologaciones/Homologacion';
import { FormulaIntegracion } from 'src/models/plantillas/integracion/FormulaIntegracion';
import { CommonDataHttpService } from 'src/http-services/common-data.http-service';
import { PlantillasHttpService } from 'src/http-services/plantillas.http-service';
import { FormularioPlantillasService } from 'src/services/formulario-plantillas/formulario-plantillas.service';
import { HomologacionesHttpService } from 'src/http-services/homologaciones.http-service';
import { FontawesomeIconsService } from 'src/services/fontawesome-icons/fontawesome-icons.service';
import { FormulaDialogData, FormulaDialogColumnaPlantillaIntegracionComponent } from 'src/app/dialogs/formulas/formulario.formulas.plantilla-integracion.component';

const TRANSFORMACION_FORMATO = "Formato";
const TRANSFORMACION_HOMOLOGACION = "Homologación";

@Component({
    selector: 'app-columna-trabajo-integracion',
    templateUrl: './columna-trabajo-integracion.component.html',
    styleUrls: ['./columna-trabajo-integracion.component.css']
})
export class ColumnaPlantillaIntegracionComponent implements OnInit, OnDestroy {

    protected columnaTrabajo: ColumnaTrabajoIntegracion;
    @Input() public set ColumnaTrabajo(columna: ColumnaTrabajoIntegracion) {
        if (this.columnaTrabajo) {
            if (!this.columnaOrigen.value) {
                this.columnaTrabajo.columnaOrigen = null;
            }
            if (!this.atributoEnriquecimiento.value) {
                this.columnaTrabajo.columnaEnriquecimiento = null;
            }
            if (!this.formatoFecha.value) {
                this.columnaTrabajo.formato = null;
            }
            if (!this.tipoHomologacion.value) {
                this.columnaTrabajo.homologacion = null;
            }
        }
        
        this.columnaTrabajo = columna;
        this.onChangeColumnaTrabajo();
    }

    public columnasOrigen$: Observable<ColumnaDestinoEntrada[]> = null;
    @Input() public set ColumnasOrigen(columnasOrigen: ColumnaDestinoEntrada[]) {
        if (columnasOrigen) {
            this.columnasOrigen$ = of(columnasOrigen);
        } else if (this._evento) {
            this.cargarColumnasOrigen();
        } else {
            this.columnasOrigen$ = null;
        }
    }
    
    protected _evento: Evento;
    @Input() public set Evento(evento: Evento) {
        if (evento) {
            this._evento = evento;
        }

        if (!this.columnasOrigen$ && this._evento) {
            this.cargarColumnasOrigen();
        }
    }

    protected _socio: Socio;
    @Input() public set Socio(socio: Socio) {
        this._socio = socio;
        this.actualizarTiposHomologacion();
    }

    protected _core: SistemaCore;
    @Input() public set Core(core: SistemaCore) {
        this._core = core;
        this.actualizarTiposHomologacion();
        this.actualizarEnriquecimientos();
    } 

    protected _columnasTrabajo: ColumnaTrabajoIntegracion[];
    @Input() public set OtrasColumnasTrabajo(columnasTrabajo: ColumnaTrabajoIntegracion[]) {
        this._columnasTrabajo = columnasTrabajo;
    };

    public enriquecimientosCargados: boolean;
    public atributosEnriquecimiento$: Observable<AtributoEnriquecimiento[]>;
    public formatoFechasItems$: Observable<TipoFormato[]>;
    public homologacionesItems$: Observable<Homologacion[]>;

    public direcciones$: Observable<string[]>;

    public columnaPlantillaIntegracionForm: FormGroup;

    public formula: FormulaIntegracion.IFormulaFactor = null;
    @ViewChild('tablaOrdenOperaciones') protected tablaOrdenTransformaciones: MatTable<string>;
    public operacionesDisplayedColumns = ['operacion', 'acciones'];

    protected transformaciones: string[] = [];
    protected _transformaciones$: Subject<string[]>;
    public transformaciones$: Observable<string[]>;

    protected atributosEnriquecimiento: AtributoEnriquecimiento[];

    protected changeNombreColumnaSub: Subscription;
    protected changeDarColumnaOrigenSub: Subscription;
    protected changeColumnaOrigenSub: Subscription;
    protected changeDarAtributoEnriquecimientoSub: Subscription;
    protected changeAtributoEnriquecimientoSub: Subscription;
    protected changeDarFormatoSub: Subscription;
    protected changeFormatoSub: Subscription;
    protected changeRealizarHomologacionSub: Subscription;
    protected changeTipoHomologacionSub: Subscription;
    protected changeAplicarFormulaSub: Subscription;

    constructor(
        protected commonSvc: CommonDataHttpService,
        protected localSvc: PlantillasHttpService,
        protected formSvc: FormularioPlantillasService,
        protected fb: FormBuilder,
        protected snackBar: MatSnackBar,
        protected dialog: MatDialog,
        protected homoSvc: HomologacionesHttpService,
        public icons: FontawesomeIconsService
    ) {
        this.enriquecimientosCargados = false;
        this._transformaciones$ = new BehaviorSubject([]);
        this.transformaciones$ = this._transformaciones$.asObservable();
        this.columnaPlantillaIntegracionForm = this.fb.group({
            nombreColumna: ['', Validators.required],
            darColumnaOrigen: [false],
            columnaOrigen: [{ value: null, disabled: true }],
            darAtributoEnriquecimiento: [false],
            atributoEnriquecimiento: [{ value: '', disabled: true }],
            darFormato: [false],
            formatoFecha: [{ value: null, disabled: true }],
            realizarHomologacion: [false],
            tipoHomologacion: [{ value: null, disabled: true }],
            aplicarFormula: [false]
        });
    }

    public get nombreColumna() { return this.columnaPlantillaIntegracionForm.get("nombreColumna"); }
    public get darColumnaOrigen() { return this.columnaPlantillaIntegracionForm.get("darColumnaOrigen"); }
    public get columnaOrigen() { return this.columnaPlantillaIntegracionForm.get("columnaOrigen"); }
    public get darAtributoEnriquecimiento() { return this.columnaPlantillaIntegracionForm.get("darAtributoEnriquecimiento"); }
    public get atributoEnriquecimiento() { return this.columnaPlantillaIntegracionForm.get("atributoEnriquecimiento"); }
    public get darFormato() { return this.columnaPlantillaIntegracionForm.get("darFormato"); }
    public get formatoFecha() { return this.columnaPlantillaIntegracionForm.get("formatoFecha"); }
    public get realizarHomologacion() { return this.columnaPlantillaIntegracionForm.get("realizarHomologacion"); }
    public get tipoHomologacion() { return this.columnaPlantillaIntegracionForm.get("tipoHomologacion"); }
    public get aplicarFormula() { return this.columnaPlantillaIntegracionForm.get("aplicarFormula"); }

    ngOnInit(): void {

        this.formatoFechasItems$ = this.commonSvc.getTiposFormato();

        this.localSvc.getAtributosEnriquecimiento(this._core.id).subscribe(
            (atributos: AtributoEnriquecimiento[]) => {
                this.atributosEnriquecimiento = atributos;
                this.atributosEnriquecimiento$ = of(atributos);
                this.enriquecimientosCargados = true;
            }
        );

        this.iniciarSuscripciones();
    }

    ngOnDestroy(): void {
        this.changeDarColumnaOrigenSub.unsubscribe();
        this.changeDarAtributoEnriquecimientoSub.unsubscribe();
        this.changeDarFormatoSub.unsubscribe();
        this.changeRealizarHomologacionSub.unsubscribe();
        this.changeAplicarFormulaSub.unsubscribe();
        this.changeNombreColumnaSub.unsubscribe();
        this.changeColumnaOrigenSub.unsubscribe();
        this.changeAtributoEnriquecimientoSub.unsubscribe();
        this.changeFormatoSub.unsubscribe();
        this.changeTipoHomologacionSub.unsubscribe();
    }

    protected iniciarSuscripciones(): void {
        this.changeDarColumnaOrigenSub = this.darColumnaOrigen.valueChanges.subscribe(() => { this.onToggleDarColumnaOrigen(); });
        this.changeDarAtributoEnriquecimientoSub = this.darAtributoEnriquecimiento.valueChanges.subscribe(() => { this.onToggleDarAtributoEnriquecimiento(); });
        this.changeDarFormatoSub = this.darFormato.valueChanges.subscribe(() => { this.onToggleDarFormato(); });
        this.changeRealizarHomologacionSub = this.realizarHomologacion.valueChanges.subscribe(() => { this.onToggleDarHomologacion(); });
        this.changeAplicarFormulaSub = this.aplicarFormula.valueChanges.subscribe(() => { this.onToggleDarFormula(); })

        this.changeNombreColumnaSub = this.nombreColumna.valueChanges.subscribe(() => { this.onChangeNombreColumna(); });
        this.changeColumnaOrigenSub = this.columnaOrigen.valueChanges.subscribe(() => { this.onChangeColumnaOrigen(); });
        this.changeAtributoEnriquecimientoSub = this.atributoEnriquecimiento.valueChanges.subscribe(() => { this.onChangeAtributoEnriquecimiento(); });
        this.changeFormatoSub = this.formatoFecha.valueChanges.subscribe(() => { this.onChangeFormato(); });
        this.changeTipoHomologacionSub = this.tipoHomologacion.valueChanges.subscribe(() => { this.onChangeHomologacion(); });
    }

    protected cargarColumnasOrigen() {
        this.columnasOrigen$ = this.localSvc.getColumnasDestinoEntradaDisponiblesUsuarios(this._evento.id);
    }

    protected actualizarTiposHomologacion(): void {
        if (this._socio && this._core) {
            this.homologacionesItems$ = this.homoSvc.getTipos(this._socio.id, this._core.id);
        }
    }

    protected actualizarEnriquecimientos(): void {
        if (this._core) {
            this.enriquecimientosCargados = false;
            this.localSvc.getAtributosEnriquecimiento(this._core.id).subscribe(
                (atributos: AtributoEnriquecimiento[]) => {
                    this.atributosEnriquecimiento = atributos;
                    this.atributosEnriquecimiento$ = of(atributos);
                    this.enriquecimientosCargados = true;
                }
            );
        }
    }

    protected actualizarOrdenTransformaciones(): void {

        for (let i = 0; i < this.transformaciones.length; i++) {

            if (this.transformaciones[i] === TRANSFORMACION_FORMATO) {
                if (this.columnaTrabajo.formato) {
                    this.columnaTrabajo.formato.orden = i + 1;
                }
            }
            else if (this.transformaciones[i] === TRANSFORMACION_HOMOLOGACION) {
                if (this.columnaTrabajo.homologacion) {
                    this.columnaTrabajo.homologacion.orden = i + 1;
                }
            }
        }
        this._transformaciones$.next(this.transformaciones);
    }

    protected retirarTransformacionDeOrden(tipoTransformacion: string) {
        const formatoIndex: number = this.transformaciones.findIndex(trf => (trf === tipoTransformacion));
        if (formatoIndex || formatoIndex === 0) {
            this.transformaciones.splice(formatoIndex, 1);
        }
    }

    /**
     * Restaura todos los controles el formulario sin gatillar eventos internos.
     */
    protected limpiarFormulario(): void {
        const noChange = { onlySelf: true, emitEvent: false };
        const unchecked = { value: false, disabled: false };
        const unselected = { value: undefined, disabled: true };

        this.nombreColumna.setValue('', noChange);

        this.darColumnaOrigen.reset(unchecked, noChange);
        this.columnaOrigen.reset(unselected, noChange);
        this.darAtributoEnriquecimiento.reset(unchecked, noChange);
        this.atributoEnriquecimiento.reset(unselected, noChange);
        this.aplicarFormula.reset(unchecked, noChange);

        this.darFormato.reset(unchecked, noChange);
        this.realizarHomologacion.reset(unchecked, noChange);
        this.formatoFecha.reset(unselected, noChange);
        this.tipoHomologacion.reset(unselected, noChange);
    }

    /**
      * Limpia los controles del formulario que interactuan con los orígenes de la c. trabajo.
      * Además retira las propiedades de origen/enriq./formula de la misma, usando los eventos internos del formulario.
     */
    protected eliminarOrigenesColumnaTrabajo() {
        const noChange = { onlySelf: true, emitEvent: false };
        const unchecked = { value: false, disabled: false };
        const unselected = { value: undefined, disabled: true };
        this.darColumnaOrigen.reset(unchecked, noChange);
        this.darAtributoEnriquecimiento.reset(unchecked, noChange);
        this.aplicarFormula.reset(unchecked, noChange);

        this.atributoEnriquecimiento.reset(unselected);
        this.columnaOrigen.reset(unselected);
        this.columnaTrabajo.formula = null;
    }

    protected onChangeColumnaTrabajo(): void {
        
        this.limpiarFormulario();

        if (this.columnaTrabajo) {  
            
            const noChange = { onlySelf: true, emitEvent: false };
            this.nombreColumna.setValue(this.columnaTrabajo.nombre, noChange);

            if (this.columnaTrabajo.formula) {
                this.darColumnaOrigen.reset({ value: false, disabled: true }, noChange);
                this.darAtributoEnriquecimiento.reset({ value: false, disabled: true }, noChange);
                this.aplicarFormula.reset({ value: true, disabled: false }, noChange);
            } else if (this.columnaTrabajo.columnaOrigen) {
                this.columnaOrigen.reset({ value: this.columnaTrabajo.columnaOrigen, disabled: false }, noChange);
                this.darColumnaOrigen.reset({ value: true, disabled: false }, noChange);
                this.darAtributoEnriquecimiento.reset({ value: false, disabled: true }, noChange);
                this.aplicarFormula.reset({ value: false, disabled: true }, noChange);
            } else if (this.columnaTrabajo.columnaEnriquecimiento) {
                this.atributoEnriquecimiento.reset({ value: this.columnaTrabajo.columnaEnriquecimiento, disabled: false }, noChange);
                this.darColumnaOrigen.reset({ value: false, disabled: true }, noChange);
                this.darAtributoEnriquecimiento.reset({ value: true, disabled: false }, noChange);
                this.aplicarFormula.reset({ value: false, disabled: true }, noChange);
            } else {
                this.darAtributoEnriquecimiento.enable(noChange);
                this.darColumnaOrigen.enable(noChange);
                this.aplicarFormula.enable(noChange);
            }

            this.transformaciones = [];

            if (this.columnaTrabajo.formato) {
                this.transformaciones[this.columnaTrabajo.formato.orden - 1] = TRANSFORMACION_FORMATO;
                this.darFormato.setValue(true, noChange);
                this.formatoFecha.enable(noChange);
                this.formatoFecha.setValue(this.columnaTrabajo.formato.id, noChange);
            }

            if (this.columnaTrabajo.homologacion) {
                this.transformaciones[this.columnaTrabajo.homologacion.orden - 1] = TRANSFORMACION_HOMOLOGACION;
                this.realizarHomologacion.setValue(true, noChange);
                this.tipoHomologacion.enable(noChange);
                this.tipoHomologacion.setValue(this.columnaTrabajo.homologacion.id, noChange);
            }

            this._transformaciones$.next(this.transformaciones);
        }
    }

    public onChangeNombreColumna(): void {
        const nombre = this.nombreColumna.value;
        if (nombre) {
            this.columnaTrabajo.nombre = nombre;
        }
    }


    /***** ORIGENES DE DATOS *****/

    public onToggleDarColumnaOrigen(): void {
        const noChange = { onlySelf: true, emitEvent: false };
        if (this.darColumnaOrigen.value) {
            this.columnaOrigen.enable(noChange);
            this.darAtributoEnriquecimiento.reset({ value: false, disabled: true }, noChange);
            this.atributoEnriquecimiento.reset({ value: undefined, disabled: true }, noChange);
            this.aplicarFormula.reset({ value: false, disabled: true }, noChange);
        } else {
            this.eliminarOrigenesColumnaTrabajo();
        }
    }

    public onChangeColumnaOrigen(): void {
        const origen = this.columnaOrigen.value;
        if (origen) {
            this.columnaTrabajo.columnaOrigen = origen;
            this.columnaTrabajo.columnaEnriquecimiento = null;
            this.columnaTrabajo.formula = null;
        } else {
            this.columnaTrabajo.columnaOrigen = null;
        }
    }

    public onToggleDarAtributoEnriquecimiento(): void {
        const noChange = { onlySelf: true, emitEvent: false };
        if (this.darAtributoEnriquecimiento.value) {
            this.atributoEnriquecimiento.enable(noChange);
            this.darColumnaOrigen.reset({ value: false, disabled: true }, noChange);
            this.aplicarFormula.reset({ value: false, disabled: true }, noChange);
        } else {
            this.eliminarOrigenesColumnaTrabajo();
        }
    }
    
    public onChangeAtributoEnriquecimiento(): void {
        const enriq = this.atributoEnriquecimiento.value;
        if (enriq) {
            this.columnaTrabajo.columnaEnriquecimiento = enriq;
            this.columnaTrabajo.columnaOrigen = null;
            this.columnaTrabajo.formula = null;
        } else {
            this.columnaTrabajo.columnaEnriquecimiento = null;
        }
    }

    public onToggleDarFormula(): void {
        const noChange = { onlySelf: true, emitEvent: false };
        if (this.aplicarFormula.value) {
            this.darColumnaOrigen.reset({ value: false, disabled: true }, noChange);
            this.darAtributoEnriquecimiento.reset({ value: false, disabled: true }, noChange);
        } else {
            this.eliminarOrigenesColumnaTrabajo();
        }
    }

    public onClickEditarFormula(): void {

        if (!this.atributosEnriquecimiento) {
            this.snackBar.open("Los atributos de enriquecimiento, requeridos para las fórmulas, aún no se han cargado. Por favor, vuelva a intentarlo en unos momentos.", 'OK', { duration: -1 });
        } else {
            const formulaIn: FormulaIntegracion.IFormulaFactor = this.columnaTrabajo.formula? this.columnaTrabajo.formula.cuerpo : null;
            const columnas = this._columnasTrabajo.filter(c => c!=this.columnaTrabajo);

            const dialogData: FormulaDialogData = {
                editMode: true,
                eventoNombre: this._evento.nombre,
                columnaNombre: this.columnaTrabajo.nombre,
                permitirConstanteComoValor: true,
                columnasTrabajo: columnas,
                atributosEnriquecimiento: this.atributosEnriquecimiento,
                formulaIn: formulaIn,
                socioId: this._socio.id
            };

            this.dialog.open(FormulaDialogColumnaPlantillaIntegracionComponent, {
                width: '80%', //el limite impuesto por el componente angular material es 80vw
                data: dialogData
            }).afterClosed().subscribe(
                (nuevaFormula: FormulaIntegracion.IFormulaFactor) => {
                    if (nuevaFormula) {
                        if (this.columnaTrabajo.formula) {
                            this.columnaTrabajo.formula.cuerpo = Object.assign(nuevaFormula);
                        }
                        else {
                            this.formula = nuevaFormula;
                            this.columnaTrabajo.formula = {
                                orden: undefined, //se asigna en actualizarOrdenTransformaciones()
                                cuerpo: this.formula,
                                jsonStr: ""
                            };
                        }
                        this.actualizarOrdenTransformaciones();
                    }
                }
            );
        }
    }


    /***** TRANSFORMACIONES *****/

    public onToggleDarFormato(): void {
        if (this.darFormato.value) {
            this.formatoFecha.enable();
            this.formatoFecha.setValidators(Validators.required);
            this.transformaciones.push(TRANSFORMACION_FORMATO);
        }
        else {
            this.formatoFecha.disable();
            this.formatoFecha.setValidators(null);
            this.formatoFecha.setValue(null, { onlySelf: true, emitEvent: false });

            this.retirarTransformacionDeOrden(TRANSFORMACION_FORMATO);
            this.columnaTrabajo.formato = null;
        }
        this.tablaOrdenTransformaciones.renderRows();
        this.actualizarOrdenTransformaciones();
    }

    protected onChangeFormato() {
        if (this.formatoFecha.value) {
            if (this.columnaTrabajo.formato) {
                this.columnaTrabajo.formato.id = this.formatoFecha.value;
            }
            else {
                this.columnaTrabajo.formato = {
                    orden: undefined,
                    id: this.formatoFecha.value
                };
            }
        }
        else {
            this.columnaTrabajo.formato = null;
        }
        this.actualizarOrdenTransformaciones();
    }

    public onToggleDarHomologacion(): void {
        if (this.realizarHomologacion.value) {
            this.tipoHomologacion.enable();
            this.formatoFecha.setValidators(Validators.required);
            this.transformaciones.push(TRANSFORMACION_HOMOLOGACION);
        }
        else {
            this.tipoHomologacion.disable();
            this.formatoFecha.setValidators(null);
            this.tipoHomologacion.setValue(null, { onlySelf: true, emitEvent: false });

            this.retirarTransformacionDeOrden(TRANSFORMACION_HOMOLOGACION);
            this.columnaTrabajo.homologacion = null;
        }
        this.tablaOrdenTransformaciones.renderRows();
        this.actualizarOrdenTransformaciones();
    }

    public onChangeHomologacion() {
        if (this.tipoHomologacion.value) {
            if (this.columnaTrabajo.homologacion) {
                this.columnaTrabajo.homologacion.id = this.tipoHomologacion.value;
            }
            else {
                this.columnaTrabajo.homologacion = {
                    orden: undefined,
                    id: this.tipoHomologacion.value
                };
            }
        }
        else {
            this.columnaTrabajo.homologacion = null;
        }
        this.actualizarOrdenTransformaciones();
    }

    public onClickSubirOrdenTransformacion(index: number): void {
        if (index > 0) {
            let estaTransformacion: string = this.transformaciones[index];
            let transformacionSuperior: string = this.transformaciones[index - 1];
            this.transformaciones[index - 1] = estaTransformacion;
            this.transformaciones[index] = transformacionSuperior;
            this.actualizarOrdenTransformaciones();
        }
    }

    /**
     * Desplaza la columna (obtenida por su índice) un puesto más abajo (después).
     * @param index El índice de la columna a bajar.
     */
    public onClickBajarOrdenTransformacion(index: number): void {
        if (index < this.transformaciones.length - 1) {
            let estaTransformacion: string = this.transformaciones[index];
            let transformacionInferior: string = this.transformaciones[index + 1];
            this.transformaciones[index + 1] = estaTransformacion;
            this.transformaciones[index] = transformacionInferior;
            this.actualizarOrdenTransformaciones();
        }
    }

}
