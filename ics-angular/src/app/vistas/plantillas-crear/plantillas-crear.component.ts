import { Component, OnInit, OnDestroy, ViewChild, DoCheck, ApplicationRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar, MatCheckboxChange, MatDialog } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, Subscription, merge, of } from 'rxjs';

import { FormularioPlantillaEntradaComponent } from './entrada/plantilla-entrada.component';
import { FormularioPlantillaIntegracionComponent } from './integracion/plantilla-integracion.component';

import { Evento } from 'src/models/compartido/Evento';
import { Socio } from 'src/models/compartido/Socio';
import { TipoPlantilla } from 'src/models/compartido/TipoPlantilla';
import { Plantilla } from 'src/models/plantillas/Plantilla';
import { PlantillaEntrada } from 'src/models/plantillas/entrada/PlantillaEntrada';
import { PlantillaIntegracion } from 'src/models/plantillas/integracion/PlantillaIntegracion';
import { PlantillaSalida } from 'src/models/plantillas/salida/PlantillaSalida';
import { RequestResult } from 'src/models/compartido/RequestResult';
import { Title } from '@angular/platform-browser';
import { FormCanDeactivate } from 'src/services/routing/form-can-deactivate';
import { FormularioPlantillaSalidaComponent } from './salida/plantilla-salida.component';
import { FormularioPlantillasService } from '../../../services/formulario-plantillas/formulario-plantillas.service';
import { ColumnaTrabajoIntegracion } from 'src/models/plantillas/integracion/ColumnaTrabajoIntegracion';
import { MapeoIntegracion } from 'src/models/plantillas/integracion/MapeoIntegracion';
import { TipoExtensionEnum } from 'src/enums/TipoExtensionEnum';
import { ColumnaDestinoEntrada } from 'src/models/plantillas/entrada/ColumnaDestinoEntrada';
import { CommonDataHttpService } from 'src/http-services/common-data.http-service';
import { PlantillasHttpService } from 'src/http-services/plantillas.http-service';
import { FontawesomeIconsService } from 'src/services/fontawesome-icons/fontawesome-icons.service';
import { ConfirmationDialogComponent, ConfirmationDialogData } from '../../dialogs/confirmation/confirmation.dialog.component';
import { catchError, finalize } from 'rxjs/operators';

@Component({
    selector: 'app-plantillas-crear',
    templateUrl: './plantillas-crear.component.html',
    styleUrls: ['./plantillas-crear.component.css']
})
export class PlantillasCrearComponent extends FormCanDeactivate implements OnInit, OnDestroy {

    public nombresPlantilla$: Observable<string[]>;
    public tiposPlantilla$: Observable<TipoPlantilla[]>;
    public eventos$: Observable<Evento[]>;
    public socios$: Observable<Socio[]>;

    public formularioCompleto$: Observable<boolean> = of(false);
    public creacionPlantillaForm: FormGroup;
    public formSubmitted: boolean = false;
    public submitting: boolean = false;

    @ViewChild("plantillaEntrada") public plantillaEntrada: FormularioPlantillaEntradaComponent;
    @ViewChild("plantillaIntegracion") public plantillaIntegracion: FormularioPlantillaIntegracionComponent;
    @ViewChild("plantillaSalida") public plantillaSalida: FormularioPlantillaSalidaComponent;

    protected changeCompletitudFormularioSub: Subscription;
    protected changeBloqueoDatosBasicosSub: Subscription;
    protected changeIdentifierSub: Subscription;
    public plantillaId: number = 0;

    constructor(
        protected appRef: ApplicationRef,
        protected commonSvc: CommonDataHttpService,
        protected localSvc: PlantillasHttpService,
        protected router: Router,
        protected formSvc: FormularioPlantillasService,
        protected fb: FormBuilder,
        protected snackBar: MatSnackBar,
        protected dialog: MatDialog,
        public title: Title,
        public icons: FontawesomeIconsService
    ) {
        super();
        this.title.setTitle("ICS | Nueva Plantilla");
        this.creacionPlantillaForm = this.fb.group({
            nombre: [null, Validators.required],
            version: [{ value: '', disabled: true }],
            tipo: [null, Validators.required],
            socio: [null, Validators.required],
            observaciones: [''],
            evento: [null, Validators.required],
            salidaFormatoOriginalEstado: [false]
        });
    }

    public get nombre() { return this.creacionPlantillaForm.get("nombre"); }
    public get version() { return this.creacionPlantillaForm.get("version"); }
    public get tipo() { return this.creacionPlantillaForm.get("tipo"); }
    public get socio() { return this.creacionPlantillaForm.get("socio"); }
    public get evento() { return this.creacionPlantillaForm.get("evento"); }
    public get observaciones() { return this.creacionPlantillaForm.get("observaciones"); }
    public get salidaFormatoOriginalEstado() { return this.creacionPlantillaForm.get("salidaFormatoOriginalEstado"); }

    ngOnInit(): void {

        this.eventos$ = this.commonSvc.getEventos();
        this.socios$ = this.commonSvc.getSocios();
        this.tiposPlantilla$ = this.commonSvc.getTiposPlantilla();
        this.nombresPlantilla$ = this.localSvc.getNombresPlantilla();

        this.changeBloqueoDatosBasicosSub = this.formSvc.bloquearDatosBasicosPlantillaSubject.subscribe((bloquear) => { this.onChangeBloqueoDatosBasicosPlantilla(bloquear); });
        this.changeCompletitudFormularioSub = this.formSvc.completitudFormularioSubject.subscribe((completo) => { this.onChangeCompletitudFormulario(completo); });

        this.changeIdentifierSub = merge( 
            this.nombre.valueChanges, 
            this.socio.valueChanges, 
            this.evento.valueChanges, 
            this.tipo.valueChanges 
        ).subscribe( () => { this.onChangeIdentificadoresPlantilla(); } );
    }

    ngOnDestroy(): void {
        if (this.changeCompletitudFormularioSub) this.changeCompletitudFormularioSub.unsubscribe();   
        if (this.changeBloqueoDatosBasicosSub) this.changeBloqueoDatosBasicosSub.unsubscribe();
        if (this.changeIdentifierSub) this.changeIdentifierSub.unsubscribe();
    }
    
    public get canDeactivateForm(): boolean {
       return this.formSubmitted || this.creacionPlantillaForm.pristine;
    }

    /**
     * Recibiendo un RequestResult resultado de la suscripcion a un servicio de almacenamiento,
     * imprime el mensaje correspondiente al usuario.
     * @param requestResult 
     * @param creacion 
     */
    protected notificarResultadoCreacion(requestResult: RequestResult, creacion: boolean) {
        if (requestResult.code == -2) {
            let mensaje = creacion? "Plantilla creada exitosamente." : "Plantilla actualizada exitosamente.";
            this.formSubmitted = true;
            this.snackBar.open(mensaje);
            this.appRef.tick();
            this.router.navigate(["/plantillas"]);
        } else {
            this.snackBar.open(requestResult.message, "OK", {duration:-1});
        }
        this.submitting = false;
    }

    protected armarPlantillaComun(plantilla: Plantilla): Plantilla {
        plantilla.nombre = this.nombre.value;
        plantilla.version = this.version.value;
        plantilla.socio = this.socio.value;
        plantilla.evento = this.evento.value;
        plantilla.observaciones = this.observaciones.value;
        plantilla.tipo = this.tipo.value;
        return plantilla;
    }

    protected armarPlantillaEntrada(): PlantillaEntrada {
        let plantilla: PlantillaEntrada = new PlantillaEntrada();
        this.armarPlantillaComun(plantilla);

        plantilla.archivos = this.plantillaEntrada.archivos;
        plantilla.formatoEntrada = this.plantillaEntrada.tipoExtension.value;
        plantilla.tipo = this.tipo.value;
        plantilla.columnasDestino = this.plantillaEntrada.mapeos;
        
        return plantilla;
    }

    protected armarPlantillaIntegracion(): PlantillaIntegracion {
        let plantilla: PlantillaIntegracion = new PlantillaIntegracion();
        this.armarPlantillaComun(plantilla);
        plantilla.core = this.plantillaIntegracion.core.value;
        plantilla.tipoSalida = this.plantillaIntegracion.tipoSalida.value;
        plantilla.extensionSalidaManual = this.plantillaIntegracion.formatoSalida.value;
        plantilla.delimitadorColumnas = this.plantillaIntegracion.delimitadorColumnas.value;
        plantilla.incluirEncabezados = this.plantillaIntegracion.incluirEncabezados.value;
        plantilla.columnasTrabajo = this.plantillaIntegracion.columnasTrabajo;
        plantilla.columnasArchivoDestino = this.plantillaIntegracion.columnasArchivoIntegracion;
        plantilla.columnasDestino = this.plantillaIntegracion.mapeosIntegracion;

        return plantilla;
    }

    protected armarPlantillaSalidaEstandar(): PlantillaSalida {
        let plantilla: PlantillaSalida = new PlantillaSalida();
        this.armarPlantillaComun(plantilla);
        plantilla.formatoOriginal = true;
        plantilla.columnas = [];

        if (!this.salidaFormatoOriginalEstado.value) {
            plantilla.directorioSalida = this.plantillaSalida.directorioSalida.value;
            plantilla.tipoExtension = this.plantillaSalida.tipoExtension.value;
            plantilla.delimitadorColumnas = this.plantillaSalida.delimitadorColumnas.value;
            plantilla.columnas = this.plantillaSalida.formularioEstandar._columnasSalida;
            plantilla.formatoOriginal = false;
        }

        return plantilla;
    }

    protected armarPlantillaSalidaEstructurada(): PlantillaSalida {
        let plantilla: PlantillaSalida = new PlantillaSalida();
        this.armarPlantillaComun(plantilla);
        plantilla.tipoExtension = this.plantillaSalida.tipoExtension.value;
        plantilla.formatoOriginal = false;
        plantilla.estructuras = this.plantillaSalida.formularioEstructurado.estructuras;
        plantilla.formatoOriginal = false;
        plantilla.core = this.plantillaSalida.core.value;
        plantilla.directorioSalida = this.plantillaSalida.directorioSalida.value;

        return plantilla;
    }
    
    protected columnaTrabajoTieneMapeos(colTrabajo: ColumnaTrabajoIntegracion): boolean {
        return this.plantillaIntegracion.mapeosIntegracion.some(
            (mapeo: MapeoIntegracion) => {
                return mapeo.columnaOrigen.orden === colTrabajo.orden;
            }
        );
    }

    protected onChangeIdentificadoresPlantilla() {
        if (this.nombre.value &&
            this.socio.value &&
            this.evento.value &&
            this.tipo.value
        ) {
            let plantilla: Plantilla = new Plantilla();
            plantilla.nombre = this.nombre.value;
            plantilla.socio = this.socio.value;
            plantilla.evento = this.evento.value;
            plantilla.tipo = this.tipo.value;
            this.version.setValue("...", { onlySelf: true, emitEvent: false });
            this.localSvc.getVersion(plantilla).subscribe(
                (versionActual: number) => {
                    const version = (versionActual > 0)? versionActual + 1 : 1;
                    this.version.setValue(version, { onlySelf: true, emitEvent: false });
                }
            );
        }
    }

    protected onChangeCompletitudFormulario(completo: boolean) {
        this.formularioCompleto$ = of(completo);
    }

    protected onChangeBloqueoDatosBasicosPlantilla(bloquear: boolean) {
        if (this.creacionPlantillaForm) {
            const noChange = {onlySelf: true, emitEvent: false};
            if (bloquear) {
                this.creacionPlantillaForm.disable(noChange);
                this.observaciones.enable();
            } else {
                this.creacionPlantillaForm.enable(noChange);
            }
        }
    }

    public onChangeSalidaFormatoOriginalEstado(event: MatCheckboxChange) {
        if (event.checked) {
            this.formularioCompleto$ = of(true);
        } else {
            
            this.formularioCompleto$ = of(false);
        }
    }

    /**
     * Se suscribe a un observable de almacenamiento de plantilla. 
     * Si la transaccion falla, devuelve un mensaje de error estandar.
     * Siempre se llama a notificarResultadoCreacion() para mostrar mensaje al usuario.
     * @param obs 
     * @param esNueva 
     */
    private llamarServicioAlmacenamientoPlantilla(obs: Observable<any>, esNueva: boolean): void {
        obs.pipe(
            catchError(() => { 
                this.submitting = false;
                let asd = new RequestResult();
                asd.message = "Hubo un error al almacenar la plantilla. Por favor, inténtelo de nuevo.";
                return of(asd);
             })
        ).subscribe(
            (res: RequestResult) => { 
                this.notificarResultadoCreacion(res, esNueva); 
            }
        );
    }
    
    protected almacenarPlantillaEntrada() {
        const plantilla: PlantillaEntrada = this.armarPlantillaEntrada();
        const plantillaOutput = this.formSvc.crearPlantillaEntradaOutput(plantilla);
        const esNueva = (Number(plantillaOutput.version) === 1);
        const servicio = this.localSvc.almacenarPlantillaEntrada(plantillaOutput);
        this.llamarServicioAlmacenamientoPlantilla(servicio, esNueva);
    }

    private almacenarPlantillaIntegracion() {
        const plantilla: PlantillaIntegracion = this.armarPlantillaIntegracion();
        const plantillaOutput = this.formSvc.crearPlantillaIntegracionOutput(plantilla);
        const esNueva = (Number(plantillaOutput.version) === 1);
        const servicio = this.localSvc.almacenarPlantillaIntegracion(plantillaOutput);
        this.llamarServicioAlmacenamientoPlantilla(servicio, esNueva);
    }

    protected almacenarPlantillaSalidaEstandar() {
        const plantilla: PlantillaSalida = this.armarPlantillaSalidaEstandar();
        const plantillaOutput = this.formSvc.crearPlantillaSalidaOutput(plantilla);
        const esNueva = (Number(plantillaOutput.version) === 1);
        const servicio = this.localSvc.almacenarPlantillaSalida(plantillaOutput);
        this.llamarServicioAlmacenamientoPlantilla(servicio, esNueva);
    }
    
    protected almacenarPlantillaSalidaEstructurada() {
        const plantilla: PlantillaSalida = this.armarPlantillaSalidaEstructurada();
        const plantillaOutput = this.formSvc.crearPlantillaSalidaOutput(plantilla);
        const esNueva = (Number(plantillaOutput.version) === 1);
        const servicio = this.localSvc.almacenarPlantillaSalida(plantillaOutput);
        this.llamarServicioAlmacenamientoPlantilla(servicio, esNueva);
    }

    protected solicitarConfirmacionSubmit(): Observable<boolean> {
        const dialogData: ConfirmationDialogData = {
            titulo: "¿Desea guardar los cambios?",
            mensaje: "Esto generará una nueva versión de plantilla.",
            boton_si_clases: "btn-success"
        };

        return this.dialog.open(
            ConfirmationDialogComponent,
            { data: dialogData }
        ).afterClosed();
    }

    public onClickSubmitPlantilla(): void {
        this.submitting = true;
        this.creacionPlantillaForm.updateValueAndValidity();
        if (this.creacionPlantillaForm.enabled && this.creacionPlantillaForm.invalid) {
            this.snackBar.open("Debe rellenar todos los campos requeridos.");
        } else {
            const tipoPlantillaId = this.tipo.value.id;
            switch (tipoPlantillaId) {
                case 100000: 
                    // la validación de las plantillas de entrada se realiza constantemente en los subcomponentes.
                    // sólo cuando es válida, se habilita el botón que llama al método aquí presente.
                    // el estado de validación lo maneja el servicio del formulario de plantillas, un Subject<boolean>.
                    // esta es la manera ideal en que los otros dos tipos de plantilla deberían validarse,
                    // no teniendo validaciones a nivel del componente mayor, como se hace en intg. y salida más abajo
                    if (!this.plantillaEntrada.hayMapeoDeSeguro) {
                        this.snackBar.open("Debe mapear al menos un dato del seguro para almacenar correctamente esta plantilla.");
                    } else {
                        this.solicitarConfirmacionSubmit().subscribe(
                            confirmado => {
                                if (confirmado) {
                                    this.almacenarPlantillaEntrada();
                                } else {
                                    this.submitting = false;
                                }
                            }
                        );
                        return;
                    }
                    break;
                case 100001:
                    const formPlantillaIntg = this.plantillaIntegracion.creacionPlantillaIntegracionForm;
                    formPlantillaIntg.updateValueAndValidity();                        

                    if (formPlantillaIntg.invalid) {
                        this.snackBar.open("Debe rellenar todos los campos requeridos.");
                    } else if (
                        this.plantillaIntegracion.formatoSalida.value && 
                        this.plantillaIntegracion.formatoSalida.value.id === TipoExtensionEnum.PLANO && 
                        !this.plantillaIntegracion.delimitadorColumnas.value &&
                        !this.plantillaIntegracion.todasLasColumnasArchivoTienenPosiciones()
                    ) { 
                        this.snackBar.open("Todas las columnas del archivo deben tener una posición inicial y final válidas.", "OK", {duration:-1});
                    } else {
                        const cTrabajoSinOrigen: ColumnaTrabajoIntegracion = this.plantillaIntegracion.columnasTrabajo.find((colTrabajo) => { return !colTrabajo.tieneOrigen; });
                        if (cTrabajoSinOrigen) {
                            this.plantillaIntegracion.columnaSeleccionada = cTrabajoSinOrigen;
                            this.snackBar.open("Se ha detectado una columna de trabajo sin origen; corríjala e intente nuevamente, por favor.", "OK", {duration:-1});
                        } else {
                            let cTrabajoConOrigenQueNoEsDePlantillaEntrada: ColumnaTrabajoIntegracion;
                            if (this.plantillaIntegracion.columnasOrigen) {
                                const columnasOrigenIntegracion = this.plantillaIntegracion.columnasOrigen;
                                cTrabajoConOrigenQueNoEsDePlantillaEntrada = this.getColumnaTrabajoConOrigenNoIncluidoEnArray(columnasOrigenIntegracion);
                            }

                            if (cTrabajoConOrigenQueNoEsDePlantillaEntrada) {
                                this.plantillaIntegracion.columnaSeleccionada = cTrabajoConOrigenQueNoEsDePlantillaEntrada;
                                this.snackBar.open("El filtro de plantilla de entrada no incluye la columna de negocio seleccionada en esta columna de trabajo. Por favor, corrija esta columna e inténtelo nuevamente.", "OK", {duration:-1});
                            } else {
                                
                                this.solicitarConfirmacionSubmit().subscribe(
                                    confirmado => {
                                        if (confirmado) {
                                            this.almacenarPlantillaIntegracion();
                                        } else {
                                            this.submitting = false;
                                        }
                                    }
                                );
                                return;
                            }
                        }
                    }
                    break;

                case 100002:
                    if (this.salidaFormatoOriginalEstado.value) {
                        
                        this.solicitarConfirmacionSubmit().subscribe(
                            confirmado => {
                                if (confirmado) {
                                    this.almacenarPlantillaSalidaEstandar();
                                } else {
                                    this.submitting = false;
                                }
                            }
                        );
                        return;

                    } else if (this.plantillaSalida) {
                        //CON CONFIGURACION Y MAPEOS
                        const subForm = this.plantillaSalida.creacionPlantillaSalidaForm;
                        subForm.updateValueAndValidity();

                        if (subForm.invalid) {
                            this.snackBar.open("Debe rellenar todos los campos requeridos.", "OK", {duration:-1});
                        } else {
                            
                            const tipoExtensionId = this.plantillaSalida.tipoExtension.value.id;
                            if (tipoExtensionId !== TipoExtensionEnum.ESTRUCTURADO) {
                                if (this.plantillaSalida.formularioEstandar._columnasSalida.length === 0) {
                                    this.snackBar.open("Debe realizar al menos un mapeo al archivo de salida.", "OK", {duration:-1});
                                } else {
                                    const delimColumnas = this.plantillaSalida.delimitadorColumnas.value;
                                    const columnas = this.plantillaSalida.formularioEstandar._columnasSalida;
                                    if (tipoExtensionId === TipoExtensionEnum.PLANO && !delimColumnas && 
                                        columnas.some((c) => { return !c.tienePosiciones; })
                                    ) {
                                        this.snackBar.open("Todas las columnas deben tener posicion inicial y final", "OK", {duration:-1});
                                    } else {
                                        
                                        this.solicitarConfirmacionSubmit().subscribe(
                                            confirmado => {
                                                if (confirmado) {
                                                    this.almacenarPlantillaSalidaEstandar();
                                                } else {
                                                    this.submitting = false;
                                                }
                                            }
                                        );
                                        return;
                                    }
                                }
                            } else {
                                const estructuras = this.plantillaSalida.formularioEstructurado.estructuras;
                                const estructuraConTodasSusColumnaSinDatos = estructuras.find(e => { return e.columnas.every(c => !c.esValida); });
                                if (estructuraConTodasSusColumnaSinDatos) {
                                    this.plantillaSalida.formularioEstructurado.estructuraSeleccionada = estructuraConTodasSusColumnaSinDatos;
                                    this.snackBar.open("Esta estructura debe poseer al menos una columna con datos.", "OK", {duration:-1});
                                } else {
                                    this.solicitarConfirmacionSubmit().subscribe(
                                        confirmado => {
                                            if (confirmado) {
                                                this.almacenarPlantillaSalidaEstructurada();
                                            } else {
                                                this.submitting = false;
                                            }
                                        }
                                    );
                                    return;
                                }
                            }
                        }
                    }
                    break;
                default: 
                    this.snackBar.open("El tipo de plantilla elegida no es válida.", "OK", {duration:-1});
                    break;
            }
        }
        this.submitting = false;
    }
    
    protected getColumnaTrabajoConOrigenNoIncluidoEnArray(columnasOrigenIntegracion: ColumnaDestinoEntrada[]) {
        let columnaTrabajo: ColumnaTrabajoIntegracion = this.plantillaIntegracion.columnasTrabajo.find((ctrabajo: ColumnaTrabajoIntegracion) => {
            if (ctrabajo.columnaOrigen) {
                const cTrabajoOrigenId = ctrabajo.columnaOrigen.idAtributoUsuario;     
                const origenExisteEnArrayColumnasOrigenIntegracion = columnasOrigenIntegracion.some((cOrigen: ColumnaDestinoEntrada) => {
                    return cOrigen.idAtributoUsuario === cTrabajoOrigenId;
                });
                return !origenExisteEnArrayColumnasOrigenIntegracion;           
            } else {
                return false;
            }
        });

        return columnaTrabajo;
    }

}