import { Component, OnInit, Input, ViewChild, OnDestroy, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatTable, MatSnackBar } from '@angular/material';
import { Subscription, Observable, of } from 'rxjs';
import { EstructuraAneto } from 'src/models/estructuras/EstructuraAneto';
import { ArchivoEntrada } from 'src/models/plantillas/entrada/ArchivoEntrada';
import { Evento } from 'src/models/compartido/Evento';
import { EstructuraArchivoEstructuradoPlantillaEntradaComponent } from './detalles/estructura-archivo-entrada.component';
import { CommonDataHttpService } from 'src/http-services/common-data.http-service';
import { PlantillasHttpService } from 'src/http-services/plantillas.http-service';
import { FormularioPlantillasService } from 'src/services/formulario-plantillas/formulario-plantillas.service';
import { EstructurasHttpService } from 'src/http-services/estructuras.http-service';
import { FontawesomeIconsService } from 'src/services/fontawesome-icons/fontawesome-icons.service';
import { ColumnaMetadatos } from 'src/models/compartido/ColumnaMetadatos';


@Component({
    selector: 'app-archivo-entrada-estructurada',
    templateUrl: './archivo-entrada-estructurada.component.html',
    styleUrls: ['./archivo-entrada-estructurada.component.css']
})
export class ArchivoEstructuradoPlantillaEntradaComponent implements OnInit, OnDestroy {

    public estructurasSimples$: Observable<EstructuraAneto[]>;

    public cargandoEstructura: boolean = false;

    @ViewChild('estructuras') public estructurasTable: MatTable<EstructuraAneto>;
    public archivoCamposDisplayedColumns: string[] = ["codigo", "nombre", "columnas", "acciones"];

    public creacionArchivoEntradaForm: FormGroup;
    public edicionArchivoForm: FormGroup;

    public _archivo: ArchivoEntrada;
    @Input() public set Archivo(archivo: ArchivoEntrada) {
        this._archivo = archivo;
        this.onCambioArchivo();
    }

    public evento$: Observable<Evento>;
    @Input() public set Evento(evento: Evento) {
        this.evento$ = of(evento);
    }

    public estructuraSeleccionada: EstructuraAneto;

    @ViewChild("formularioEstructura") public formularioEstructura: EstructuraArchivoEstructuradoPlantillaEntradaComponent;

    @Output() public onChangeValidez = new EventEmitter<boolean>();

    private changeNombreArchivoSub: Subscription;

    constructor(
        protected commonSvc: CommonDataHttpService,
        protected localSvc: PlantillasHttpService,
        protected formSvc: FormularioPlantillasService,
        protected estSvc: EstructurasHttpService,
        protected fb: FormBuilder,
        protected snackBar: MatSnackBar,
        public icons: FontawesomeIconsService
    ) {
        this.creacionArchivoEntradaForm = fb.group({
            nombre: ['', Validators.required]
        });

        this.edicionArchivoForm = fb.group({
            estructura: [null]
        });
    }

    public get nombre() { return this.creacionArchivoEntradaForm.get("nombre"); }
    public get estructura() { return this.edicionArchivoForm.get("estructura"); }

    ngOnInit(): void {
        this.estructurasSimples$ = this.commonSvc.getEstructuras();

        this.changeNombreArchivoSub = this.nombre.valueChanges.subscribe(() => { this._archivo.nombre = this.nombre.value; });
    }

    ngOnDestroy(): void {
        this.changeNombreArchivoSub.unsubscribe();
    }

    protected actualizarValidezArchivo() {
        for (const ctrlId in this.creacionArchivoEntradaForm.controls) {
            this.creacionArchivoEntradaForm.controls[ctrlId].markAsTouched();
        }

        if (this.creacionArchivoEntradaForm.enabled && this.creacionArchivoEntradaForm.invalid) {
            this.onChangeValidez.next(false);
        } else if (this._archivo.columnas && this._archivo.columnas.length === 0) {
            this.onChangeValidez.next(false);
        } else if (this._archivo.estructuras && this._archivo.estructuras.length === 0) {
            this.onChangeValidez.next(false);
        } else {
            this.onChangeValidez.next(true);
        }
    }
    
    protected estructuraTieneMapeos(target: EstructuraAneto) {
        return target.columnas.some((columna: ColumnaMetadatos) => {
            return columna.mapeada || (columna.subEstructura && columna.subEstructura.columnas.some((subcolumna: ColumnaMetadatos) => { return subcolumna.mapeada; }));
        });
    }

    protected onCambioArchivo(): void {
        this.estructuraSeleccionada = null;
        this.estructura.setValue(null);

        const noChange = { onlySelf: true, emitEvent: false };
        if (this._archivo) {

            this._archivo.columnas = null;
            if (!this._archivo.estructuras) {
                this._archivo.estructuras = [];
            }
            
            this.nombre.setValue(this._archivo.nombre, noChange);
            this.actualizarValidezArchivo();
            
        }
    }

    public onClickAgregarEstructura(): void {
        if (this.estructura.value === null) {
            this.snackBar.open("Debe seleccionar una estructura para agregarla");
        }
        else {
            if (this._archivo.estructuras.some((estructura: EstructuraAneto) => { return this.estructura.value.id === estructura.id; })) {
                this.snackBar.open("Esta estructura ya se encuentra asignada al archivo.");
            }
            else {
                this.edicionArchivoForm.disable();
                this.cargandoEstructura = true;
                this.estSvc.buscar(this.estructura.value.id).subscribe(
                    (estr: EstructuraAneto) => {
                        this.cargandoEstructura = false;
                        this._archivo.estructuras.push(estr);
                        this.estructurasTable.renderRows();
                        this.edicionArchivoForm.reset();
                        this.edicionArchivoForm.enable();
                        this.actualizarValidezArchivo();
                    }
                );
            }
        }
    }

    public onClickVerDetalles(index: number) {
        const seleccion = this._archivo.estructuras[index];
        if (this.estructuraSeleccionada !== seleccion) {
            this.estructuraSeleccionada = seleccion;
            if (this.formularioEstructura) {
                this.formularioEstructura.indiceColumnaSeleccionada = null;
            }
        }
        else {
            this.estructuraSeleccionada = null;
        }
    }

    public onClickQuitarEstructura(index: number) {
        const target = this._archivo.estructuras[index];
        const hayMapeosEnEstructura = this.estructuraTieneMapeos(target);

        //si alguna de las columnas de la estructura, subestructura estan mapeadas
        if (hayMapeosEnEstructura) {
            this.snackBar.open("No puede quitar esta estructura; primero elimine todo mapeo asociado a sus columnas o subestructuras.");
        } else {
            if (target === this.estructuraSeleccionada) {
                this.estructuraSeleccionada = null;
            }
            this._archivo.estructuras.splice(index, 1);
            this.estructurasTable.renderRows();
            this.actualizarValidezArchivo();
        }
    }

    public onChangeEstadoMapeo() {
        this.actualizarValidezArchivo();
    }

}
