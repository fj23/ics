import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ArchivoEstructuradoPlantillaEntradaComponent } from './archivo-entrada-estructurada.component';

describe('ArchivoEstructuradoPlantillaEntradaComponent', () => {
  let component: ArchivoEstructuradoPlantillaEntradaComponent;
  let fixture: ComponentFixture<ArchivoEstructuradoPlantillaEntradaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArchivoEstructuradoPlantillaEntradaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArchivoEstructuradoPlantillaEntradaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
