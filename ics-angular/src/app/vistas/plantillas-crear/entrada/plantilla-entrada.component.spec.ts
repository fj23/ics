import { FormularioPlantillaEntradaComponent } from "./plantilla-entrada.component";
import { ComponentFixture, TestBed } from "@angular/core/testing";
import { async } from "q";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";

describe('FormularioPlantillaEntradaComponent', () => {
  let component: FormularioPlantillaEntradaComponent;
  let fixture: ComponentFixture<FormularioPlantillaEntradaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, FormsModule],
      declarations: [ FormularioPlantillaEntradaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormularioPlantillaEntradaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
