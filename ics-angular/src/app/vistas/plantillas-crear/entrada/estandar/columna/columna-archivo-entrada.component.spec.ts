import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColumnaArchivoEstandarPlantillaEntradaComponent } from './columna-archivo-entrada.component';

describe('ColumnaArchivoEstandarPlantillaEntradaComponent', () => {
  let component: ColumnaArchivoEstandarPlantillaEntradaComponent;
  let fixture: ComponentFixture<ColumnaArchivoEstandarPlantillaEntradaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ColumnaArchivoEstandarPlantillaEntradaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColumnaArchivoEstandarPlantillaEntradaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
