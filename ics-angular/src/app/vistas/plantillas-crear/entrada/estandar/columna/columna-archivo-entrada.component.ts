import { Component, OnInit, Input, OnDestroy, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { Subscription, Observable } from 'rxjs';


import { ColumnaMetadatos } from 'src/models/compartido/ColumnaMetadatos';
import { TipoDato } from 'src/models/compartido/TipoDato';
import { TipoExtensionEnum } from 'src/enums/TipoExtensionEnum';
import { FormularioPlantillasService } from '../../../../../../services/formulario-plantillas/formulario-plantillas.service';
import { CommonDataHttpService } from 'src/http-services/common-data.http-service';
import { PlantillasHttpService } from 'src/http-services/plantillas.http-service';

@Component({
    selector: 'app-columna-archivo-entrada',
    templateUrl: './columna-archivo-entrada.component.html',
    styleUrls: ['./columna-archivo-entrada.component.css']
})
export class ColumnaArchivoEstandarPlantillaEntradaComponent implements OnInit, OnDestroy {

    public tiposDato$: Observable<TipoDato[]>;

    public campoArchivoForm: FormGroup;

    @Output() public columnaSubmit: EventEmitter<ColumnaMetadatos> = new EventEmitter<ColumnaMetadatos>();

    private _delimitadorColumnas: string

    private entradaFormatoSub: Subscription;

    constructor(
        private commonSvc: CommonDataHttpService,
        private formSvc: FormularioPlantillasService,
        private localSvc: PlantillasHttpService,
        private fb: FormBuilder,
        private snackBar: MatSnackBar
    ) {
        this.campoArchivoForm = this.fb.group({
            nombre: ['', Validators.required],
            tipoDato: ['', Validators.required],
            posInicial: [null],
            posFinal: [null]
        });
    }

    public get nombre() { return this.campoArchivoForm.get("nombre"); }
    public get tipoDato() { return this.campoArchivoForm.get("tipoDato"); }
    public get posInicial() { return this.campoArchivoForm.get("posInicial"); }
    public get posFinal() { return this.campoArchivoForm.get("posFinal"); }

    ngOnInit(): void {
        this.tiposDato$ = this.localSvc.getTiposDatoColumnaEstandar();

        this.entradaFormatoSub = this.formSvc.entradaFormatoSubject.subscribe(() => { this.updateColumnPositionRequirement(); });
    }

    ngOnDestroy() {
        this.entradaFormatoSub.unsubscribe();
    }

    @Output() public PosInicial: EventEmitter<number> = new EventEmitter<number>();

    @Output() public PosFinal: EventEmitter<number> = new EventEmitter<number>();

    @Input() public set delimitadorColumnas(delimitadorColumnas: string) {
        this._delimitadorColumnas = delimitadorColumnas;
        this.updateColumnPositionRequirement();
    }

    private updateColumnPositionRequirement() {
        if (this.formSvc.entradaFormatoSubject.getValue() === TipoExtensionEnum.PLANO && 
            !(this._delimitadorColumnas && this._delimitadorColumnas.length > 0)) {
            this.posInicial.enable();
            this.posFinal.enable();
            this.posInicial.setValidators(Validators.compose([Validators.required, Validators.min(0)]));
            this.posFinal.setValidators(Validators.compose([Validators.required, Validators.min(1)]));
        }
        else  {
            this.posInicial.disable();
            this.posFinal.disable();
            this.posInicial.setValue(null);
            this.posFinal.setValue(null);
            this.posInicial.setValidators(null);
            this.posFinal.setValidators(null);
        }
        this.posInicial.updateValueAndValidity();
        this.posFinal.updateValueAndValidity();
    }

    /**
     * Con los datos del formulario en el recuadro 'nueva columna', crea un objeto modelo.
     */
    private buildColumna(): ColumnaMetadatos {
        let model: ColumnaMetadatos = new ColumnaMetadatos();

        model.tipoDato = this.tipoDato.value;
        model.descripcion = this.nombre.value;
        model.mapeada = false;

        if (!this.posInicial.disabled && !this.posFinal.disabled) {
            model.posicionInicial = this.posInicial.value;
            model.posicionFinal = this.posFinal.value;
        }

        return model;
    }

    /**
     * Valida el formulario de columna, la crea y la agrega al archivo actual.
     */
    public onSubmitColumna(): void {
        if (!this.campoArchivoForm.valid) {
            for (const ctrlId in this.campoArchivoForm.controls) {
                this.campoArchivoForm.controls[ctrlId].markAsTouched();
            }
            this.snackBar.open("Debe completar todos los datos requeridos del formulario.");
        }
        else {
            let columna: ColumnaMetadatos = this.buildColumna();
            if (columna != null) {
                this.columnaSubmit.emit(columna);
                this.campoArchivoForm.reset();
            }
        }
    }

}
