import { Component, OnInit, ViewChild, OnDestroy, Input, ApplicationRef, ViewChildren, QueryList, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ValidationErrors } from '@angular/forms';
import { MatTable, MatDialog, MatSnackBar } from '@angular/material';
import { Subscription, Observable, BehaviorSubject, of, Subject } from 'rxjs';

import { ArchivoEstandarPlantillaEntradaComponent } from './estandar/archivo-entrada-estandar.component';
import { FormularioPlantillasService } from '../../../../services/formulario-plantillas/formulario-plantillas.service';

import { ArchivoEntrada } from 'src/models/plantillas/entrada/ArchivoEntrada';
import { ColumnaDestinoEntrada } from 'src/models/plantillas/entrada/ColumnaDestinoEntrada';
import { MapeoEntrada } from 'src/models/plantillas/entrada/MapeoEntrada';
import { TipoExtension } from 'src/models/compartido/TipoExtension';
import { ArchivoEstructuradoPlantillaEntradaComponent } from './estructurada/archivo-entrada-estructurada.component';
import { ColumnaMetadatos } from 'src/models/compartido/ColumnaMetadatos';
import { TipoExtensionEnum } from 'src/enums/TipoExtensionEnum';
import { Evento } from 'src/models/compartido/Evento';
import { ConfirmationDialogComponent } from 'src/app/dialogs/confirmation/confirmation.dialog.component';
import { CommonDataHttpService } from 'src/http-services/common-data.http-service';
import { PlantillasHttpService } from 'src/http-services/plantillas.http-service';
import { FontawesomeIconsService } from 'src/services/fontawesome-icons/fontawesome-icons.service';
import { LoginComponent } from '../../../paginas/login/login.component';

export const TIPOS_PERSONA_CON_CINCO_NIVELES = [ "Beneficiario", "Asegurados adicionales", "Siniestrado" ];

@Component({
    selector: 'app-plantilla-entrada',
    templateUrl: './plantilla-entrada.component.html',
    styleUrls: ['./plantilla-entrada.component.css']
})
export class FormularioPlantillaEntradaComponent implements OnInit, OnDestroy {

    protected _mapeos: MapeoEntrada[];
    protected _archivos: ArchivoEntrada[] = [];

    protected _mapeos$: Subject<MapeoEntrada[]>;
    protected _archivos$: Subject<ArchivoEntrada[]>;

    public creacionPlantillaEntradaForm: FormGroup;
    public mapearColumnasForm: FormGroup;
    public tiposExtension$: Observable<TipoExtension[]>;
    public columnasDestinoDisponibles$: Observable<ColumnaDestinoEntrada[]>;
    
    @ViewChild("formularioEstandar") public formularioArchivo: ArchivoEstandarPlantillaEntradaComponent;
    @ViewChild("formularioEstructurado") public formularioArchivoEstructurado: ArchivoEstructuradoPlantillaEntradaComponent;

    @ViewChild("tablaArchivos") protected tablaArchivos: MatTable<ArchivoEntrada>;
    public archivosDisplayedColumns = ['nombre', 'acciones'];
    public archivos$: Observable<ArchivoEntrada[]>;
    public get archivos(): ArchivoEntrada[] { return this._archivos; }
    
    @ViewChildren("tablaMapeos") protected tablaMapeos: MatTable<MapeoEntrada>;
    public columnasDestinoMapeadasDisplayedColumns: string[];
    public mapeos$: Observable<MapeoEntrada[]>;
    public get mapeos(): MapeoEntrada[] { return this._mapeos; }
    
    public archivoSeleccionado: ArchivoEntrada;
    
    /** Usado por ng-select. Nunca trabajar con este array como si fuera inmutable; ver https://github.com/ng-select/ng-select#change-detection */
    public gruposMapeo: number[] = [];

    protected tipoExtensionChangeSub: Subscription;
    protected campoFiltrarRegistrosSub: Subscription;
    protected filtroExclusionSub: Subscription;
    protected mapeoColumnaSubEstructuraSub: Subscription;
    protected tablaRefChangeSub: Subscription;

    public emptyGrupoColumna = '';

    protected _evento: Evento;
    @Input() public set Evento(evento: Evento) {
        this._evento = evento;
        this.columnasDestinoDisponibles$ = this.localSvc.getColumnasDestinoEntradaDisponiblesUsuarios(this._evento.id);
    }

    constructor(
        protected appRef: ApplicationRef,
        protected commonSvc: CommonDataHttpService,
        protected localSvc: PlantillasHttpService,
        protected formSvc: FormularioPlantillasService,
        protected fb: FormBuilder,
        protected dialog: MatDialog,
        protected snackBar: MatSnackBar,
        public icons: FontawesomeIconsService
    ) {
        this.creacionPlantillaEntradaForm = this.fb.group({
            tipoExtension: [null, Validators.required]
        });

        this.mapearColumnasForm = this.fb.group({
            filtrarRegistros: [false],
            filtroExclusion: [{ value: false, disabled: true }],
            valorFiltrado: [{ value: '', disabled: true }],
            columnaDestinoSeleccionada: [null]
        });

        this._mapeos = [];
        this._archivos = [];

        this._mapeos$ = new BehaviorSubject(this._mapeos);
        this._archivos$ = new BehaviorSubject(this._archivos);

        this.mapeos$ = this._mapeos$.asObservable();
        this.archivos$ = this._archivos$.asObservable();
    }

    public get tipoExtension() { return this.creacionPlantillaEntradaForm.get("tipoExtension"); }
    
    public get filtrarRegistros() { return this.mapearColumnasForm.get("filtrarRegistros"); }
    public get filtroExclusion() { return this.mapearColumnasForm.get("filtroExclusion"); }
    public get valorFiltrado() { return this.mapearColumnasForm.get("valorFiltrado"); }
    public get columnaDestinoSeleccionada() { return this.mapearColumnasForm.get("columnaDestinoSeleccionada"); }

    public get hayMapeos(): boolean { return this._mapeos.length > 0; }
    public get hayMapeoDeSeguro(): boolean { return this._mapeos.some(m => m.columnaDestino.tipoPersona === "NO APLICA"); }

    ngOnInit(): void {
        this.tiposExtension$ = this.commonSvc.getTiposExtension();

        this.tipoExtensionChangeSub = this.tipoExtension.valueChanges.subscribe(() => { this.onChangeTipoExtension(); });
        this.campoFiltrarRegistrosSub = this.filtrarRegistros.valueChanges.subscribe(() => { this.onToggleFiltrarRegistros() });
        
        this.mapeoColumnaSubEstructuraSub = this.formSvc.entradaMapeoColumnaSubEstructuraSubject.subscribe(
            (mapeo: MapeoEntrada) => { this.onMapeoColumnaSubEstructura(mapeo); }
        );
    }

    ngOnDestroy(): void {
        if (this.tipoExtensionChangeSub) {
            this.tipoExtensionChangeSub.unsubscribe();
        }

        if (this.campoFiltrarRegistrosSub) {
            this.campoFiltrarRegistrosSub.unsubscribe();
        }

        if (this.mapeoColumnaSubEstructuraSub) {
            this.mapeoColumnaSubEstructuraSub.unsubscribe();
        }

        if (this.tablaRefChangeSub) {
            this.tablaRefChangeSub.unsubscribe();
        }
    }

    protected onChangeTipoExtension() {
        const tpExtId = this.tipoExtension.value.id;
        this.formSvc.entradaFormatoSubject.next(tpExtId);
        this.afterChangeTipoExtension(tpExtId);
    }

    /**
     * Devuelve los problemas de validación del archivo actual como string, o null si no los hay
     */
    protected getProblemasArchivoActual(): string {
        if (this.archivoSeleccionado) {
            let formArchivo: FormGroup;
            if (this.formularioArchivo) {
                formArchivo = this.formularioArchivo.creacionArchivoEntradaForm;
            } else if (this.formularioArchivoEstructurado) {
                formArchivo = this.formularioArchivoEstructurado.creacionArchivoEntradaForm;
            } else {
                return "no se ha disponibilizado en el formulario.";
            }

            for (const ctrlId in formArchivo.controls) {
                formArchivo.controls[ctrlId].markAsTouched();
            }

            if (formArchivo.enabled && formArchivo.invalid) {
                return "posee campos requeridos sin llenar.";
            } else {
                if (this.formularioArchivo && this.archivoSeleccionado.columnas && this.archivoSeleccionado.columnas.length === 0) {
                    return "no posee columnas.";
                } else if (this.formularioArchivoEstructurado && this.archivoSeleccionado.estructuras && this.archivoSeleccionado.estructuras.length === 0) {
                    return "no posee estructuras.";
                }
            } 
        }
        return null;
    }
    
    /**
     * Devuelve los problemas de validación de los archivos de la plantilla actual, o null si no los hay
     */
    protected getProblemasArchivos(): string { 

        for (let i = 0; i < this._archivos.length; i++) {
            const arch: ArchivoEntrada = this._archivos[i];

            const archivoValidationString: string = arch.errorValidacion;
            if (archivoValidationString) {
                return "El archivo " + (i + 1) + ", '" + (arch.nombre) + "', " + archivoValidationString + " Corríjalo e intente nuevamente.";
            }
        }

        return null;
    }

    /**
     * Devuelve los problemas de validación de la plantilla actual como string, o null si no los hay.
     */
    protected getProblemasPlantilla(): string {
        for (const ctrlId in this.creacionPlantillaEntradaForm.controls) {
            this.creacionPlantillaEntradaForm.controls[ctrlId].markAsTouched();
        }
        this.creacionPlantillaEntradaForm.updateValueAndValidity();
        

        if (this.creacionPlantillaEntradaForm.enabled && this.creacionPlantillaEntradaForm.invalid) {
            return "Debe rellenar todos los campos requeridos.";
        } else if (this._archivos.length === 0) {
            return "Debe agregar al menos un archivo para crear una plantilla de entrada.";
        } else {
            const problemasArchivos = this.getProblemasArchivos();
            if (problemasArchivos) {
                return problemasArchivos;
            } else if (this._mapeos.length === 0) {
                return "Debe mapear al menos una columna de destino";
            }
        }

        return null;
    }

    /**
     * Devuelve false y notifica al usuario, si hay problemas de validación para la plantilla actual.
     * De lo contrario, sólo devuelve true.
     */
    protected validarPlantilla(): boolean {
        const problemasPlantilla: string = this.getProblemasPlantilla();
        if (problemasPlantilla) {
            this.snackBar.open(problemasPlantilla);
            return false;
        } else {
            return true;
        }
    }

    protected actualizarValidezPlantilla() {
        const problemasPlantilla: string = this.getProblemasPlantilla();
        if (!problemasPlantilla) {
            this.formSvc.completitudFormularioSubject.next(true);
        } else {
            this.formSvc.completitudFormularioSubject.next(false);
        }
    }

    protected agregarArchivoVacio() {
        const numeroArchivo: number = this._archivos.length + 1;
        let nuevo: ArchivoEntrada = new ArchivoEntrada();
        nuevo.nombre += " [" + (numeroArchivo) + "]";
        nuevo.tipo = this.tipoExtension.value;
        this._archivos.push(nuevo);
        this._archivos$.next(this._archivos);
        return nuevo;
    }

    protected quitarArchivo(index: number): void {
        const archivoAEliminar: ArchivoEntrada = this._archivos[index];
        if (archivoAEliminar === this.archivoSeleccionado) {
            this.archivoSeleccionado = null;
        }

        this._archivos.splice(index, 1);
        this._archivos$.next(this._archivos);

        if (this._archivos.length === 0) {
            this.tipoExtension.enable();
            this.formSvc.bloquearDatosBasicosPlantillaSubject.next(false);
        }
        
        this.actualizarValidezPlantilla();
    }

    protected mapeoAlcanzaLimitePorArchivo(mapeo: MapeoEntrada): boolean {
        const archOrigen = mapeo.archivoOrigenDatos;
        const colOrigen = mapeo.columnaOrigenDatos;
        const destino = mapeo.columnaDestino;
        
        if (mapeo.tieneCardinalidad) {
            let mapeosAlMismoDestino = 0;
            const mapeosArchivo = this._mapeos.filter(m => m.archivoOrigenDatos === archOrigen);

            for (const mapeoExistente of mapeosArchivo) {

                if (mapeoExistente.columnaDestino.idAtributoUsuario === destino.idAtributoUsuario) {

                    let tieneMismoOrigen: boolean;
                    
                    if (mapeo.estructuraOrigen || mapeo.subEstructuraColumnaOrigen) {
                        tieneMismoOrigen = (mapeoExistente.columnaOrigenDatos.id === colOrigen.id);
                    } else {
                        tieneMismoOrigen = (mapeoExistente.columnaOrigenDatos.orden === colOrigen.orden);
                    }

                    if (tieneMismoOrigen) {
                        // la misma columna de archivo, mapeada al mismo destino...
                        return true; 
                    } else {
                        mapeosAlMismoDestino++;
                    }
                }
            }

            return (mapeosAlMismoDestino >= destino.cardinalidadMaxima);
        } else {
            return this._mapeos.some( 
                (mapeoExistente: MapeoEntrada) => {
                    return (
                        mapeoExistente.columnaDestino.idAtributoNegocio === destino.idAtributoNegocio && 
                        mapeoExistente.columnaDestino.idAtributoUsuario === destino.idAtributoUsuario &&
                        mapeoExistente.archivoOrigenDatos.nombre === archOrigen.nombre
                    );
                }
            );
        }
    }

    protected agregarGruposMapeo(cantidad: number) {
        let i = 0;
        while (i < cantidad) {
            this.gruposMapeo = [...this.gruposMapeo, this.gruposMapeo.length+1];
            i++;
        }
    }

    protected quitarGruposMapeo(cantidad: number) {
        const gruposResultantes = (this.gruposMapeo.length-cantidad >= 0)? this.gruposMapeo.length-cantidad : 0;
        this.gruposMapeo = this.gruposMapeo.slice(0, gruposResultantes);
        for (const mapeo of this._mapeos) {
            if (mapeo.grupoColumnaDestino > gruposResultantes) {
                mapeo.grupoColumnaDestino = gruposResultantes;
            }
        }
    }

    protected logicaNuevoGrupoMapeos(nuevoMapeo: MapeoEntrada) {
        let seDebenAgregarNuevosGrupos = false;

        const destinoNuevoMapeo = nuevoMapeo.columnaDestino;
        const tpPersonaNuevoMapeo = destinoNuevoMapeo.tipoPersona;

        if (this.gruposMapeo.length === 0) {                    
            seDebenAgregarNuevosGrupos = true;
        } else {
            const mapeosPreviosAEsteTipoPersona = this._mapeos.filter(m => m.columnaDestino.tipoPersona === tpPersonaNuevoMapeo && m !== nuevoMapeo);

            if (mapeosPreviosAEsteTipoPersona.length === 0) {
                seDebenAgregarNuevosGrupos = true;
            } else {

                // se agregaran nuevos grupos, si ya se habia mapeado la misma columna de destino para el mismo tipo de persona...
                // esto podria ocurrir mas de una vez
                seDebenAgregarNuevosGrupos = mapeosPreviosAEsteTipoPersona.some(m => m.columnaDestino.idAtributoUsuario === destinoNuevoMapeo.idAtributoUsuario);

                if (!seDebenAgregarNuevosGrupos) {
                    nuevoMapeo.grupoColumnaDestino = mapeosPreviosAEsteTipoPersona[mapeosPreviosAEsteTipoPersona.length-1].grupoColumnaDestino;
                }
            }
        }

        if (seDebenAgregarNuevosGrupos) {

            // el nuevo mapeo estará asociado enseguida al primero de los nuevos grupos
            nuevoMapeo.grupoColumnaDestino = this.gruposMapeo.length + 1;

            if (TIPOS_PERSONA_CON_CINCO_NIVELES.includes(nuevoMapeo.columnaDestino.tipoPersona)) {
                this.agregarGruposMapeo(5);
            } else {
                this.agregarGruposMapeo(1);
            }
        }

    }

    /**
     * Recibe un objeto de mapeo, le pasa una referencia al archivo en edición,
     * y lo almacena en el array de mapeos de la plantilla
     * @param mapeo Un objeto de mapeo
     */
    protected mapearColumna(mapeo: MapeoEntrada) {
        console.log(mapeo);
        
        if (mapeo.tieneCardinalidad) {
            mapeo.grupoColumnaDestino = 1;
            this.logicaNuevoGrupoMapeos(mapeo);
        } else {
            mapeo.grupoColumnaDestino = 0;
        }

        //se asume que el mapeo ya viene con las referencias, según corresponda, a:
        // 1)la columna de destino (atributo usuario)
        // 2A)la columna de archivo (índice o ID)
        // 2B)la estructura y su columna
        // 2C)la estructura, columna, id de subestructura
        // 2D)la estructura, columna, id de subestructura y columna de ésta

        //solo falta la referencia al archivo...
        mapeo.archivoOrigenDatos = this.archivoSeleccionado;
        this.archivoSeleccionado.mapeado = true;
        
        //y agregarlo al array correspondiente
        this._mapeos.push(mapeo);
        this._mapeos$.next(this._mapeos);
        
        if (this.formularioArchivo) {
            this.formularioArchivo.onChangeEstadoMapeo();
        } else if (this.formularioArchivoEstructurado) {
            this.formularioArchivoEstructurado.onChangeEstadoMapeo();
        }
        
        this.mapearColumnasForm.reset();
        
        let problemas = this.getProblemasPlantilla();
        
        if (!problemas) {
            this.formSvc.completitudFormularioSubject.next(true); //mostrar el boton "crear plantilla"
        }
    }

    protected onMapeoColumnaSubEstructura(mapeo: MapeoEntrada) {
        if (this.mapeoAlcanzaLimitePorArchivo(mapeo)) {
            this.snackBar.open("No se puede mapear dos veces un mismo destino desde un mismo archivo.");
        } else {
            this.mapearColumna(mapeo);
        }
    }

    protected mapearDesdeArchivoEstandar(indiceColumnaSeleccionada: number) {
        let mapeo: MapeoEntrada = new MapeoEntrada();
        mapeo.columnaDestino = this.columnaDestinoSeleccionada.value;
        mapeo.archivoOrigenDatos = this.archivoSeleccionado;
        mapeo.columnaOrigenDatos = this.archivoSeleccionado.columnas[indiceColumnaSeleccionada];
        if (this.filtrarRegistros.value) {
            mapeo.valorParaFiltrar = this.valorFiltrado.value;
            mapeo.filtroExclusion = this.filtroExclusion.value;
        } else {
            mapeo.valorParaFiltrar = null;
        }

        if (this.mapeoAlcanzaLimitePorArchivo(mapeo)) {
            this.snackBar.open("Este destino ha alcanzado el límite de veces que se puede mapear.");
        } else {
            this.formularioArchivo.indiceColumnaSeleccionada = null;
            this.archivoSeleccionado.columnas[indiceColumnaSeleccionada].mapeada = true;
            this.mapearColumna(mapeo);
        }
    }

    protected mapearDesdeArchivoEstructurado() {
        const formularioEstructura = this.formularioArchivoEstructurado.formularioEstructura;

        let mapeo = new MapeoEntrada();
        mapeo.columnaDestino = this.columnaDestinoSeleccionada.value;
        mapeo.archivoOrigenDatos = this.archivoSeleccionado;
        mapeo.estructuraOrigen = this.formularioArchivoEstructurado.estructuraSeleccionada;

        if (formularioEstructura.columnaSubEstructuraSeleccionada) {
            mapeo.subEstructuraColumnaOrigen = formularioEstructura.columnaSubEstructuraSeleccionada;
        } else {
            const indiceColumnaSeleccionada = formularioEstructura.indiceColumnaSeleccionada;
            if (!indiceColumnaSeleccionada && indiceColumnaSeleccionada !== 0) {
                this.snackBar.open("Debe seleccionar una columna de estructura o de subestructura desde la que mapear.");
            } else if (indiceColumnaSeleccionada || indiceColumnaSeleccionada === 0) {
                mapeo.columnaOrigenDatos = mapeo.estructuraOrigen.columnas[indiceColumnaSeleccionada];
            }
        }

        if (mapeo.columnaOrigenDatos || mapeo.subEstructuraColumnaOrigen) {
            if (this.mapeoAlcanzaLimitePorArchivo(mapeo)) {
                this.snackBar.open("Este destino ha alcanzado el límite de veces que se puede mapear.");
            } else {

                if (formularioEstructura.columnaSubEstructuraSeleccionada) {
                    mapeo.subEstructuraColumnaOrigen.mapeada = true;
                } else {
                    formularioEstructura.indiceColumnaSeleccionada = null;
                    mapeo.columnaOrigenDatos.mapeada = true;
                }

                if (this.filtrarRegistros.value) {
                    mapeo.valorParaFiltrar = this.valorFiltrado.value;
                    mapeo.filtroExclusion = this.filtroExclusion.value;
                } else {
                    mapeo.valorParaFiltrar = null;
                }
                
                this.mapearColumna(mapeo);
            }
        }
    }

    public afterChangeTipoExtension(tpExt: number) {
        if (tpExt === TipoExtensionEnum.ESTRUCTURADO) { 
            this.columnasDestinoMapeadasDisplayedColumns = ["nombre", "origenEstructurado", "encriptar", "filtro", "obligatorio", "grupo", "acciones"];
        } else {
            this.columnasDestinoMapeadasDisplayedColumns = ["nombre", "origenEstandar", "encriptar", "filtro", "obligatorio", "grupo", "acciones"];
        }
    }

    public onClickAgregarArchivoVacio(): void {
        if (!this.tipoExtension.value) {
            this.snackBar.open("Debe seleccionar un formato de entrada.");
        } else  {
            const problemasArchivoActual = this.getProblemasArchivoActual();
            if (problemasArchivoActual) {
                this.snackBar.open("No puede agregar otro archivo: el actual "+problemasArchivoActual);
            } else {
                let nuevo: ArchivoEntrada = this.agregarArchivoVacio();
                this.archivoSeleccionado = nuevo;
                
                this.tipoExtension.disable({ onlySelf: true, emitEvent: false });
                this.formSvc.bloquearDatosBasicosPlantillaSubject.next(true);

                // archivo nuevo significa archivo incompleto
                this.formSvc.completitudFormularioSubject.next(false);
            }
        }
    }

    public onClickEditarArchivo(index: number): void {
        const problemas = this.getProblemasArchivoActual();
        if (problemas) {
            this.snackBar.open("Antes de configurar otro archivo, corrija el actual; "+problemas, "OK");
        } else {
            const seleccion = this._archivos[index];
            if (this.archivoSeleccionado !== seleccion) {
                this.archivoSeleccionado = seleccion;
            } else {
                this.archivoSeleccionado = null;
            }
        }
    }

    public onClickQuitarArchivo(index: number): void {
        if (index <= this._archivos.length - 1) {
            const archivo: ArchivoEntrada = this._archivos[index];
            const cantidadMapeos = this.cantidadMapeosParaArchivo(archivo);
            if (cantidadMapeos > 0) {
                this.snackBar.open("Este archivo posee "+cantidadMapeos+" mapeos. Primero debe deshacerlos antes de eliminar el archivo de la plantilla.");
            } else {
                const confirmationDialog = this.dialog.open(ConfirmationDialogComponent, {
                    data: {
                        titulo: "Quitar Archivo",
                        mensaje: "Si quita este archivo del listado, no podrá recuperar la configuración efectuada en él. ¿Desea confirmar esta acción?",
                        boton_si_clases: "btn-warning",
                        boton_no_clases: ""
                    }
                });
    
                confirmationDialog.afterClosed().subscribe(
                    (confirmed: boolean) => {
                        if (confirmed) {
                            this.quitarArchivo(index);
                        }
                    }
                );
            }
        }
    }

    public onChangeValidezArchivoSeleccionado(valido: boolean) {
        if (valido) {
            this.filtrarRegistros.enable();
            this.columnaDestinoSeleccionada.enable({ onlySelf: true, emitEvent: false });
        } else {
            this.filtrarRegistros.disable({ onlySelf: true, emitEvent: false });
            this.columnaDestinoSeleccionada.disable({ onlySelf: true, emitEvent: false });
        }
        this.actualizarValidezPlantilla();
    }

    public onToggleFiltrarRegistros() {
        if (this.filtrarRegistros.value) {
            this.filtroExclusion.enable();
            this.valorFiltrado.enable({ onlySelf: true, emitEvent: false });
            this.valorFiltrado.setValidators(Validators.required);
        } else {
            this.filtroExclusion.disable();
            this.filtroExclusion.setValue(false);
            this.valorFiltrado.disable({ onlySelf: true, emitEvent: false });
            this.valorFiltrado.setValue("");
            this.valorFiltrado.setValidators(null);
        }
    }

    public onClickMapearColumna(): void {
        const problemas = this.getProblemasArchivoActual();
        if (problemas) {
            this.snackBar.open("No puede mapear desde este archivo, porque "+problemas);
        } else {
            
            const desdeArchivoEstructurado: boolean = (this.tipoExtension.value.id === TipoExtensionEnum.ESTRUCTURADO);
            if (!this.columnaDestinoSeleccionada.value) {
                this.snackBar.open("Debe seleccionar una columna de destino para mapear.");
            } else if (desdeArchivoEstructurado) { 

                if (!this.formularioArchivoEstructurado.estructuraSeleccionada) {
                    this.snackBar.open("Debe seleccionar una estructura para realizar un mapeo.");
                } else {
                    this.mapearDesdeArchivoEstructurado();
                }
            } else {
                
                const indiceColumnaSeleccionada = this.formularioArchivo.indiceColumnaSeleccionada;
                const columnasArchivo = this.archivoSeleccionado.columnas;
                const columnaOrigenSeleccionada = columnasArchivo[indiceColumnaSeleccionada];

                if (isNaN(indiceColumnaSeleccionada) || (!indiceColumnaSeleccionada && indiceColumnaSeleccionada !== 0)) {
                    this.snackBar.open("Debe seleccionar algún campo de archivo para hacer un mapeo.");
                } else if (columnaOrigenSeleccionada.subEstructura && columnaOrigenSeleccionada.subEstructura.columnas.some((columna: ColumnaMetadatos) => { return columna.mapeada; })) {
                    this.snackBar.open("La columna seleccionada es una subestructura y posee columnas que ya han sido mapeadas individualmente.");
                } else {
                    this.mapearDesdeArchivoEstandar(indiceColumnaSeleccionada);
                }
            }
        }
    }

    protected cantidadMapeosParaArchivo(archivo: ArchivoEntrada): number {
        const mapeosArchivo = this._mapeos.filter(mapeo => (mapeo.archivoOrigenDatos === archivo));
        return mapeosArchivo.length;
    }

    public onClickQuitarColumnaDestinoMapeada(index: number): void {

        const mapeo: MapeoEntrada = this._mapeos[index];
        const archivo: ArchivoEntrada = mapeo.archivoOrigenDatos;
        mapeo.columnaOrigenDatos.mapeada = false;
        if (mapeo.subEstructuraColumnaOrigen) {
            mapeo.subEstructuraColumnaOrigen.mapeada = false;
        }

        if (mapeo.columnaDestino.flgCardinalidad === "Y") {
            const tipoPersona = mapeo.columnaDestino.tipoPersona;
            const mapeosPreviosAEsteTipoPersona = this._mapeos.filter(m => m.columnaDestino.tipoPersona === tipoPersona && m !== mapeo);

            if (mapeosPreviosAEsteTipoPersona.length === 0) {
                const esPersonaConCincoNiveles = (TIPOS_PERSONA_CON_CINCO_NIVELES.includes(tipoPersona));
                if (esPersonaConCincoNiveles) {
                    this.quitarGruposMapeo(5);
                } else if (mapeo.columnaDestino.flgCardinalidad === "Y") {
                    this.quitarGruposMapeo(1);
                }
            }
        }

        this._mapeos.splice(index, 1);
        this._mapeos$.next(this._mapeos);

        if (this.cantidadMapeosParaArchivo(archivo) === 0) {
            archivo.mapeado = false;
            if (this.formularioArchivo) {
                this.formularioArchivo.onChangeEstadoMapeo();
            } else if (this.formularioArchivoEstructurado) {
                this.formularioArchivoEstructurado.onChangeEstadoMapeo();
            }
        }

        this.actualizarValidezPlantilla();
    }

}
