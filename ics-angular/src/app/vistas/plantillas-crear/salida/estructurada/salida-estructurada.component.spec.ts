import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormularioEstructurasPlantillaSalidaComponent } from './salida-estructurada.component';

describe('FormularioEstructurasPlantillaSalidaComponent', () => {
  let component: FormularioEstructurasPlantillaSalidaComponent;
  let fixture: ComponentFixture<FormularioEstructurasPlantillaSalidaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormularioEstructurasPlantillaSalidaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormularioEstructurasPlantillaSalidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
