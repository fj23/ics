import { Component, Input, Output, ViewChild, OnInit, OnDestroy } from '@angular/core';
import { TipoFormato } from 'src/models/compartido/TipoFormato';
import { Homologacion } from 'src/models/homologaciones/Homologacion';
import { ColumnaEstructuraAnetoSalida } from 'src/models/plantillas/salida/estructura/ColumnaEstructuraAnetoSalida';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatTable, MatDialog } from '@angular/material';
import { Subscription, Observable, of, Subject, BehaviorSubject } from 'rxjs';
import { FormulaIntegracion } from 'src/models/plantillas/integracion/FormulaIntegracion';
import { FormulaDialogColumnaPlantillaIntegracionComponent, FormulaDialogData } from '../../../../../dialogs/formulas/formulario.formulas.plantilla-integracion.component';
import { FontawesomeIconsService } from '../../../../../../services/fontawesome-icons/fontawesome-icons.service';

const NO_CHANGE = { onlySelf: true, emitEvent: false };
const TRANSFORMACION_FORMATO = "Formato";
const TRANSFORMACION_HOMOLOGACION = "Homologación";
const TRANSFORMACION_FORMULA = "Fórmula";

@Component({
    selector: 'app-transformaciones-columna-estructura',
    templateUrl: './transformaciones-columna-estructura.component.html',
    styleUrls: ['./transformaciones-columna-estructura.component.css']
})
export class FormularioTransformacionesColumnaEstructuraPlantillaSalidaComponent 
    implements OnInit, OnDestroy {

    public readonly TRANSFORMACION_FORMULA = TRANSFORMACION_FORMULA;

    protected _columnaEstructura: ColumnaEstructuraAnetoSalida = null;
    public get columnaEnTransformacion() { return this._columnaEstructura; }
    @Input() public set columnaEnTransformacion(col: ColumnaEstructuraAnetoSalida) {  
        
        if (!!this._columnaEstructura) {
            this.limpiarTransformacionesColumna();
        }

        this._columnaEstructura = col;
        
        if (!!col) {
            this.cargarTransformacionesColumna();
        }

    }
    @Input() public formatoFechasItems: TipoFormato[];
    @Input() public homologacionesItems: Homologacion[];
    @Input() public eventoNombre: string;
    @Input() public socioId: number;
    
    public transformacionesForm: FormGroup;

    protected _transformaciones: string[];
    protected _transformaciones$: Subject<string[]>;
    @ViewChild("tablaOrdenTransformaciones") public tablaOrdenTransformaciones: MatTable<string>;
    public transformacionesDisplayedColumns: string[] = ['orden', 'operacion', 'acciones'];
    public transformaciones$: Observable<string[]>;
    public get transformaciones() { return this._transformaciones; };
    
    protected trfDarFormatoSub: Subscription;
    protected trfRealizarHomologacionSub: Subscription;
    protected trfAplicarFormulaSub: Subscription;
    protected trfTipoFormatoSub: Subscription;
    protected trfTipoHomologacionSub: Subscription;


    constructor(
        protected fb: FormBuilder,
        protected dialog: MatDialog,
        public icons: FontawesomeIconsService
    ) {

        this._transformaciones = [];
        this._transformaciones$ = new BehaviorSubject(this._transformaciones);
        this.transformaciones$ = this._transformaciones$.asObservable();

        this.transformacionesForm = this.fb.group({
            darFormato: [false],
            tipoFormato: [{value: null, disabled: true}],
            realizarHomologacion: [false],
            tipoHomologacion: [{value: null, disabled: true}],
            aplicarFormula: [false]
        });
    }

    public get trfDarFormato() { return this.transformacionesForm.get("darFormato"); }
    public get trfTipoFormato() { return this.transformacionesForm.get("tipoFormato"); }
    public get trfRealizarHomologacion() { return this.transformacionesForm.get("realizarHomologacion"); }
    public get trfTipoHomologacion() { return this.transformacionesForm.get("tipoHomologacion"); }
    public get trfAplicarFormula() { return this.transformacionesForm.get("aplicarFormula"); }

    ngOnInit() {
        this.trfDarFormatoSub = this.trfDarFormato.valueChanges.subscribe(() => { 
            this.onToggleTrfDarFormato();
        });
        this.trfRealizarHomologacionSub = this.trfRealizarHomologacion.valueChanges.subscribe(() => { 
            this.onToggleTrfRealizarHomologacion();
        });
        this.trfAplicarFormulaSub = this.trfAplicarFormula.valueChanges.subscribe(() => { 
            this.onToggleTrfAplicarFormula();
        });

        this.trfTipoFormatoSub = this.trfTipoFormato.valueChanges.subscribe(() => { 
            this.onChangeTrfTipoFormato(); 
        });

        this.trfTipoHomologacionSub = this.trfTipoHomologacion.valueChanges.subscribe(() => { 
            this.onChangeTrfTipoHomologacion(); 
        });
    }

    ngOnDestroy() {
        if (this.trfDarFormatoSub) {
            this.trfDarFormatoSub.unsubscribe();
        }
        if (this.trfRealizarHomologacionSub) {
            this.trfRealizarHomologacionSub.unsubscribe();
        }
        if (this.trfAplicarFormulaSub) {
            this.trfAplicarFormulaSub.unsubscribe();
        }
        if (this.trfTipoFormatoSub) {
            this.trfTipoFormatoSub.unsubscribe();
        }
        if (this.trfTipoHomologacionSub) {
            this.trfTipoHomologacionSub.unsubscribe();
        }
    }

    private limpiarTransformacionesColumna() {
        const colFormato = this._columnaEstructura.formato;
        const colHomologacion = this._columnaEstructura.homologacion;
        const colFormula = this._columnaEstructura.formula;        

        //se quitan las transformaciones una a una para evitar que existan indices vacios en el array
        if (!!!colFormula || !!!colFormula.valor) {
            const formulaConstruida = colFormula.valor;
            this.retirarTransformacionDeOrden(TRANSFORMACION_FORMULA);
            this._columnaEstructura.formula = null;
            if (!formulaConstruida) {
                this.actualizarOrdenTransformaciones();
            }
        }
        if (!!!colFormato || !!!colFormato.valor) {
            this.retirarTransformacionDeOrden(TRANSFORMACION_FORMATO);
            this._columnaEstructura.formato = null;
        }   
        if (!!!colHomologacion || !!!colHomologacion.valor) {
            this.retirarTransformacionDeOrden(TRANSFORMACION_HOMOLOGACION);
            this._columnaEstructura.homologacion = null;
        }
        this._transformaciones = [];
        this.actualizarOrdenTransformaciones();
    }

    private cargarTransformacionesColumna() {

        const colFormato = this._columnaEstructura.formato;
        const colHomologacion = this._columnaEstructura.homologacion;
        const colFormula = this._columnaEstructura.formula;

        if (colFormato) {
            this.trfDarFormato.setValue(true, NO_CHANGE);
            this.trfTipoFormato.enable(NO_CHANGE);
            this.trfTipoFormato.setValue(colFormato.valor, NO_CHANGE);
            const fOrden = colFormato.orden;
            this._transformaciones[fOrden-1] = TRANSFORMACION_FORMATO;
        } else {
            this.trfDarFormato.setValue(false, NO_CHANGE);
            this.trfTipoFormato.disable(NO_CHANGE);
            this.trfTipoFormato.setValue(null, NO_CHANGE);
        }
        
        if (colHomologacion) {
            this.trfRealizarHomologacion.setValue(true, NO_CHANGE);
            this.trfTipoHomologacion.enable(NO_CHANGE);
            this.trfTipoHomologacion.setValue(colHomologacion.valor, NO_CHANGE);
            const hOrden = colHomologacion.orden;
            this._transformaciones[hOrden-1] = TRANSFORMACION_HOMOLOGACION;
        } else {
            this.trfRealizarHomologacion.setValue(false, NO_CHANGE);
            this.trfTipoHomologacion.disable(NO_CHANGE);
            this.trfTipoHomologacion.setValue(null, NO_CHANGE);
        }

        if (colFormula) {
            this.trfAplicarFormula.setValue(true, NO_CHANGE);
            const fOrden = colFormula.orden;
            this._transformaciones[fOrden-1] = TRANSFORMACION_FORMULA;
        } else {
            this.trfAplicarFormula.setValue(false, NO_CHANGE);
        }
        
        this._transformaciones$.next(this._transformaciones);
    }
    
    private actualizarOrdenTransformaciones(): void {

        for (let i = 0; i < this._transformaciones.length; i++) {

            if (this._transformaciones[i] === TRANSFORMACION_FORMULA) {
                if (this._columnaEstructura.formula) {
                    this._columnaEstructura.formula.orden = i + 1;
                }
            } else if (this._transformaciones[i] === TRANSFORMACION_FORMATO) {
                if (this._columnaEstructura.formato) {
                    this._columnaEstructura.formato.orden = i + 1;
                }
            } else if (this._transformaciones[i] === TRANSFORMACION_HOMOLOGACION) {
                if (this._columnaEstructura.homologacion) {
                    this._columnaEstructura.homologacion.orden = i + 1;
                }
            }
        }
        this._transformaciones$.next(this._transformaciones);
    }

    private retirarTransformacionDeOrden(tipoTransformacion: string) {
        const formatoIndex: number = this._transformaciones.findIndex(
            (trf: string) => { return trf === tipoTransformacion; }
        );
        if (formatoIndex > -1) {
            this._transformaciones.splice(formatoIndex, 1);
        }
    }

    private onToggleTrfRealizarHomologacion() {
        if (this.trfRealizarHomologacion.value) {
            this.trfTipoHomologacion.enable(NO_CHANGE);
            this.trfTipoHomologacion.setValidators(Validators.required);
            this._transformaciones.push(TRANSFORMACION_HOMOLOGACION);
        } else {
            this.trfTipoHomologacion.disable(NO_CHANGE);
            this.trfTipoHomologacion.setValue(null);
            this.retirarTransformacionDeOrden(TRANSFORMACION_HOMOLOGACION);
        }
        this.actualizarOrdenTransformaciones();
    }

    private onToggleTrfDarFormato() {
        if (this.trfDarFormato.value) {
            this.trfTipoFormato.enable(NO_CHANGE);
            this.trfTipoFormato.setValidators(Validators.required);
            this._transformaciones.push(TRANSFORMACION_FORMATO);
        } else {
            this.trfTipoFormato.disable(NO_CHANGE);
            this.trfTipoFormato.setValue(null);
            this.retirarTransformacionDeOrden(TRANSFORMACION_FORMATO);
        }
        this.actualizarOrdenTransformaciones();
    }

    private onToggleTrfAplicarFormula() {
        if (this.trfAplicarFormula.value) {
            this._transformaciones.unshift(TRANSFORMACION_FORMULA); //la formula siempre debe ir primero
            this._columnaEstructura.formula = {
                orden: 1,
                valor: undefined
            };
        } else {
            this._columnaEstructura.formula = null;
            this.retirarTransformacionDeOrden(TRANSFORMACION_FORMULA);
        }
        this.actualizarOrdenTransformaciones();
    }

    private onChangeTrfTipoFormato() {
        if (this.trfTipoFormato.value) {
            if (this._columnaEstructura.formato) {
                this._columnaEstructura.formato.valor = this.trfTipoFormato.value;
            } else {    
                const formatoIndex: number = this._transformaciones.findIndex(trf => (trf === TRANSFORMACION_FORMATO));
                this._columnaEstructura.formato = {
                    orden: formatoIndex+1,
                    valor: this.trfTipoFormato.value
                };
            }
        } else {
            this._columnaEstructura.formato = null
        }
        this.actualizarOrdenTransformaciones();
    }

    private onChangeTrfTipoHomologacion() {
        if (this.trfTipoHomologacion.value) {
            if (this._columnaEstructura.homologacion) {
                this._columnaEstructura.homologacion.valor = this.trfTipoHomologacion.value;
            } else {    
                const homologacionIndex: number = this._transformaciones.findIndex(trf => (trf === TRANSFORMACION_HOMOLOGACION));
                this._columnaEstructura.homologacion = {
                    orden: homologacionIndex,
                    valor: this.trfTipoHomologacion.value
                };
            }
        } else {
            this._columnaEstructura.homologacion = null;
        }
        this.actualizarOrdenTransformaciones();
    }

    
    public onClickEditarFormula(): void {

        const formulaIn = this.columnaEnTransformacion.formula?
                    this.columnaEnTransformacion.formula.valor : null;

        const dialogData: FormulaDialogData = {
            eventoNombre: this.eventoNombre,
            columnaNombre: this.columnaEnTransformacion.descripcion,
            permitirConstanteComoValor: false,
            formulaIn: formulaIn,
            socioId: this.socioId,
            columnasTrabajo: null,
            editMode: true,
            atributosEnriquecimiento: null
        };

        this.dialog.open(FormulaDialogColumnaPlantillaIntegracionComponent, {
            width: '80%', //el limite impuesto por el componente angular material es ~80% del ancho de la pantalla
            data: dialogData
        }).afterClosed().subscribe(
            (nuevaFormula: FormulaIntegracion.IFormulaFactor) => {
                if (nuevaFormula) {
                    if (!!formulaIn) {
                        this.columnaEnTransformacion.formula.valor = Object.assign(nuevaFormula);
                    } else {
                        const formulaIndex: number = this._transformaciones.findIndex(trf => (trf === TRANSFORMACION_FORMULA));
                        if (this.columnaEnTransformacion.formula) {
                            this.columnaEnTransformacion.formula.valor = nuevaFormula;
                        } else {
                            this.columnaEnTransformacion.formula = {
                                orden: undefined, //se asigna en actualizarOrdenTransformaciones()
                                valor: nuevaFormula
                            };
                            this.actualizarOrdenTransformaciones();
                        }
                    }
                }
            }
        );
    }

    public onClickSubirOrdenTransformacion(index: number) {
        if (index > 0) {
            let transformacionAnterior: string = this._transformaciones[index - 1];
            let estaTransformacion: string = this._transformaciones[index];

            this._transformaciones[index - 1] = estaTransformacion;
            this._transformaciones[index] = transformacionAnterior;

            this.actualizarOrdenTransformaciones();
        }
    }

    public onClickBajarOrdenTransformacion(index: number) {
        if (index < this._transformaciones.length - 1) {
            let estaTransformacion: string = this._transformaciones[index];
            let transformacionSiguiente: string = this._transformaciones[index + 1];

            this._transformaciones[index + 1] = estaTransformacion;
            this._transformaciones[index] = transformacionSiguiente;

            this.actualizarOrdenTransformaciones();
        }
    }
}