import { Component, OnInit, ViewChild, Input, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatTable, MatSnackBar, MatDialog } from '@angular/material';
import { Observable, Subscription } from 'rxjs';

import { FormularioPlantillasService } from '../../../../../services/formulario-plantillas/formulario-plantillas.service';

import { EstructuraAneto } from 'src/models/estructuras/EstructuraAneto';
import { AtributoDiccionarioEstructuraAnetoSalida } from 'src/models/plantillas/salida/estructura/AtributoDiccionarioEstructuraAnetoSalida';
import { TipoFormato } from 'src/models/compartido/TipoFormato';
import { Homologacion } from 'src/models/homologaciones/Homologacion';
import { Evento } from 'src/models/compartido/Evento';
import { Socio } from 'src/models/compartido/Socio';
import { EstructuraAnetoSalida } from 'src/models/plantillas/salida/estructura/EstructuraAnetoSalida';
import { ColumnaEstructuraAnetoSalida } from 'src/models/plantillas/salida/estructura/ColumnaEstructuraAnetoSalida';
import { CommonDataHttpService } from 'src/http-services/common-data.http-service';
import { PlantillasHttpService } from 'src/http-services/plantillas.http-service';
import { EstructurasHttpService } from 'src/http-services/estructuras.http-service';
import { HomologacionesHttpService } from 'src/http-services/homologaciones.http-service';
import { FontawesomeIconsService } from 'src/services/fontawesome-icons/fontawesome-icons.service';

const NO_CHANGE = { onlySelf: true, emitEvent: false };
const TRANSFORMACION_FORMATO = "Formato";
const TRANSFORMACION_HOMOLOGACION = "Homologación";
const TRANSFORMACION_FORMULA = "Fórmula";
const TIPOS_DATO = [ { id: 1, descripcion: "Constante" }, { id: 2, descripcion: "Atributo" } ];

@Component({
    selector: 'app-salida-estructurada',
    templateUrl: './salida-estructurada.component.html',
    styleUrls: ['./salida-estructurada.component.css']
})
export class FormularioEstructurasPlantillaSalidaComponent implements OnInit {

    public readonly TRANSFORMACION_FORMULA = TRANSFORMACION_FORMULA;
    public readonly tiposDato = TIPOS_DATO;

    public estructurasSimples: EstructuraAneto[];
    public cargandoEstructura: boolean;
    public cargandoSubEstructura: boolean;
    public estructuraDisponibleSeleccionada: EstructuraAneto;
    public atributosDiccionario: AtributoDiccionarioEstructuraAnetoSalida[];

    protected _evento: Evento;
    @Input() public set Evento(evento: Evento) { this._evento = evento; }
    public get evento() { return this._evento; }

    protected _socioId: number;
    public get socioId() { return this._socioId; }
    @Input() public set Socio(socio: Socio) { 
        if (!!socio) {
            this._socioId = socio.id;  
        } else {
            this._socioId = null;
        }
        this.actualizarHomologaciones();
    }

    protected _coreId: number;
    public get coreId() { return this._coreId; }
    @Input() public set Core(core: Socio) { 
        if (!!core) {
            this._coreId = core.id; 
        } else {
            this._coreId = null;
         }
        this.actualizarHomologaciones(); 
    }

    @ViewChild('tablaEstructuras') public tablaEstructuras: MatTable<EstructuraAnetoSalida>;
    public estructuraDisplayedColumns: string[];
    public estructuras: EstructuraAnetoSalida[]

    public estructuraSeleccionada: EstructuraAnetoSalida;
    @ViewChild('tablaDetallesEstructura') public tablaDetallesEstructura: MatTable<ColumnaEstructuraAnetoSalida>;
    public estructuraColumnasDisplayedColumns: string[];

    protected _columnaEnTransformacion: ColumnaEstructuraAnetoSalida;
    @ViewChild("tablaOrdenTransformaciones") public tablaOrdenTransformaciones: MatTable<string>;
    public transformacionesDisplayedColumns: string[];
    public formatoFechasItems: TipoFormato[];
    public homologacionesItems: Homologacion[];
    
    public get columnaEnTransformacion() { return this._columnaEnTransformacion; }

    constructor(
        protected commonSvc: CommonDataHttpService,
        protected localSvc: PlantillasHttpService,
        protected formSvc: FormularioPlantillasService,
        protected estSvc: EstructurasHttpService,
        protected snackBar: MatSnackBar,
        protected dialog: MatDialog,
        protected homoSvc: HomologacionesHttpService,
        public icons: FontawesomeIconsService
    ) {
        this.estructurasSimples = [];
        this.cargandoEstructura = true;
        this.cargandoSubEstructura = true;
        this.estructuraDisponibleSeleccionada = null;
        this.atributosDiccionario = [];
        
        this.estructuraDisplayedColumns = ['codigo', 'nombre', 'columnas', 'acciones'];
        this.estructuraColumnasDisplayedColumns = ['nombre', 'valor', 'acciones'];
        this.transformacionesDisplayedColumns = ['orden', 'operacion', 'acciones'];
        this.estructuras = [];
        this.formatoFechasItems = [];
        this.homologacionesItems = [];
        this._columnaEnTransformacion = null;
    }

    ngOnInit() {
        this.commonSvc.getEstructuras().subscribe(estructs => { 
            this.estructurasSimples = estructs; 
            this.cargandoEstructura = false;
        });
        this.commonSvc.getTiposFormato().subscribe(formatos => { this.formatoFechasItems = formatos; });
        this.localSvc.getAtributosDiccionarioSalida().subscribe(atrs => { this.atributosDiccionario = atrs; });
    }

    private actualizarHomologaciones(): void {
        if (this.socioId && this.coreId) {
            this.homoSvc.getTipos(this.socioId, this.coreId).subscribe(hmlgs => { this.homologacionesItems = hmlgs; });
        }
    }


    public onClickAgregarEstructura(): void {
        if (!this.estructuraDisponibleSeleccionada) {
            this.snackBar.open("Debe seleccionar una estructura para agregarla al archivo de salida.");
        } else {
            this.cargandoEstructura = true;
            this.estSvc.buscar(this.estructuraDisponibleSeleccionada.id).subscribe(
                (source: EstructuraAneto) => {
                    let estr = this.formSvc.estructuraAnetoToPlantillaSalidaModel(source);
                    this.estructuras.push(estr);
                    this.tablaEstructuras.renderRows();
                    this.estructuraDisponibleSeleccionada = null;
                    if (this.estructuras.length === 1) {
                        this.formSvc.bloquearDatosBasicosPlantillaSubject.next(true);
                        this.formSvc.completitudFormularioSubject.next(true);
                    }
                }, 
                err => {
                    console.error(err);
                    this.snackBar.open("Hubo un problema al cargar la estructura. Inténtelo de nuevo.");
                },
                () => { this.cargandoEstructura = false;  }
            );
        }
    }

    public onClickVerDetalles(index: number): void {
        if (this.estructuraSeleccionada !== this.estructuras[index]) {
            this.estructuraSeleccionada = this.estructuras[index];
        } else {
            this.estructuraSeleccionada = null;
        }
        this._columnaEnTransformacion = null;
    }

    public onClickQuitarEstructura(index: number): void {
        if (this.estructuraSeleccionada === this.estructuras[index]) {
            this.estructuraSeleccionada = null;
            this._columnaEnTransformacion = null;
        }
        this.estructuras.splice(index, 1);

        if (this.estructuras.length === 0) {
            this.formSvc.bloquearDatosBasicosPlantillaSubject.next(false);
            this.formSvc.completitudFormularioSubject.next(false);
        }
        this.tablaEstructuras.renderRows();
    }

    public onChangeTipoDato(col: ColumnaEstructuraAnetoSalida): void {
        if (col.tipoDato !== 1) {
            col.valorFijo = undefined;
        }
        if (col.tipoDato !== 2) {
            col.atributoDiccionario = undefined;
        }

        if (!col.tipoDato) {
            col.formula = null;
            col.formato = null;
            col.homologacion = null;
        }
    }

    public onClickVerTransformacionesColumna(index: number): void {
        const columna = this.estructuraSeleccionada.columnas[index];
        
        if (this._columnaEnTransformacion !== columna) {
            this._columnaEnTransformacion = columna;
        } else {
            this._columnaEnTransformacion = null;
        }        
    }

    // public onClickVerDetallesSubEstructura(index: number) {
    //     let columnaEstructura: ColumnaEstructuraPlantillaSalidaModel = this.estructuraSeleccionada.columnas[index];
    //     if (!columnaEstructura.subestructura) {
    //         this.cargandoSubEstructura = true;
    //         this.subEstSvc.buscar(columnaEstructura.subestructura.id).subscribe(
    //             (subEstructura: SubEstructuraAnetoModel) => {
    //                 this.cargandoSubEstructura = false;
    //                 if (subEstructura) {
    //                     columnaEstructura.subestructura = this.subestructuraAnetoToSalidaModel(subEstructura);
    //                     this.realizarMapeoSubEstructura(columnaEstructura);
    //                 }
    //             }
    //         );
    //     }
    //     else {
    //         this.realizarMapeoSubEstructura(columnaEstructura);
    //     }
    // }
}
