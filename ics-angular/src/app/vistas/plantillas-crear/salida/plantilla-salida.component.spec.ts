import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormularioPlantillaSalidaComponent } from './plantilla-salida.component';

describe('FormularioPlantillaSalidaComponent', () => {
  let component: FormularioPlantillaSalidaComponent;
  let fixture: ComponentFixture<FormularioPlantillaSalidaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormularioPlantillaSalidaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormularioPlantillaSalidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
