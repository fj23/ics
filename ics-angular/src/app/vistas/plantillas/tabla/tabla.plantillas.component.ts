import { Component, OnInit, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';
import { MatSort, MatPaginator, MatSnackBar, MatDialog } from '@angular/material';
import { merge, Subscription } from 'rxjs';

import { MatPaginatorES } from 'src/app/compartido/mat.paginator.es';
import { PlantillasDataSource } from './tabla.plantillas.datasource';
import { Plantilla } from 'src/models/plantillas/Plantilla';
import { ConfirmationDialogComponent } from 'src/app/dialogs/confirmation/confirmation.dialog.component';
import { Permisos } from 'src/app/compartido/Permisos';
import { Angular5Csv } from 'angular5-csv/dist/Angular5-csv';
import { HttpResponse } from '@angular/common/http';
import { FontawesomeIconsService } from 'src/services/fontawesome-icons/fontawesome-icons.service';
import { PlantillasHttpService } from 'src/http-services/plantillas.http-service';

@Component({
    selector: 'app-tabla-plantillas',
    templateUrl: './tabla.plantillas.component.html',
    styleUrls: ['./tabla.plantillas.component.css']
})
export class TablaPlantillasComponent implements OnInit, OnDestroy {
    @ViewChild(MatPaginator) private paginator: MatPaginator;
    @ViewChild(MatSort) private sort: MatSort;
    public displayedColumns = ['nombre', 'version', 'fechaCreacion', 'socio', 'estado', 'codUsuario', 'acciones'];
    public dataSource: PlantillasDataSource;

    public ocultarPorPermisos: boolean;


    private plantillas: Plantilla[];

    private plantillasSub: Subscription;
    private reloadTableSub: Subscription;
    private plantillasCountSub: Subscription;

    constructor(
        private localSvc: PlantillasHttpService,
        public dialog: MatDialog,
        private snackBar: MatSnackBar,
        public icons: FontawesomeIconsService
    ) {
        this.dataSource = new PlantillasDataSource(this.localSvc);
        this.ocultarPorPermisos = Permisos.DenegarAcceso(Permisos.MantenerPlantilla());
    }

    ngOnInit(): void {
        this.paginator._intl = new MatPaginatorES();

        //cuando el orden aplicado a los datos cambie, vuelve a la pagina 1
        this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

        //cuando el orden o el numero de pagina cambie, recarga los datos
        this.reloadTableSub = merge(
            this.sort.sortChange,
            this.paginator.page
        ).subscribe(
            () => this.recargar()
        );

        this.plantillasCountSub = this.dataSource.plantillasCount$.subscribe(
            (count: number) => { 
                this.paginator.length = count; 
            }
        );

        this.plantillasSub = this.dataSource.plantillas$.subscribe(
            (plantillas: Plantilla[]) => { 
                this.plantillas = plantillas; 
            }
        );
    }

    ngOnDestroy(): void {
        if (this.reloadTableSub) this.reloadTableSub.unsubscribe();
        if (this.plantillasCountSub) this.plantillasCountSub.unsubscribe();
        if (this.plantillasSub) this.plantillasSub.unsubscribe();
    }

    private recargar(): void {
        this.dataSource.getPlantillasPage(
            this.paginator.pageIndex,
            this.paginator.pageSize,
            this.sort.active,
            this.sort.direction
        );
    }

    public onClickActivarPlantilla(plantilla: Plantilla): void {
        this.localSvc.activar(plantilla, false)
            .subscribe(
                (response: HttpResponse<void>) => {
                    if (response.status === 202) {
                        const confirmationDialog = this.dialog.open(ConfirmationDialogComponent, {
                            width: '600px',
                            data: {
                                titulo: "Activar Plantilla",
                                mensaje: "Al activar esta plantilla se deben desactivar las otras versiones, pero una de ellas tiene asociado un Proceso ¿Desea desactivarlo y activar esta Plantilla?",
                                boton_si_clases: "btn-success",
                                boton_no_clases: ""
                            }
                        });

                        confirmationDialog.afterClosed().subscribe(
                            (confirmed: boolean) => {
                                if (confirmed) {
                                    this.localSvc.activar(plantilla, true)
                                        .subscribe(
                                            (response2: HttpResponse<void>) => {
                                                if (response2.status === 200) {
                                                    this.snackBar.open("Plantilla activada.");
                                                    plantilla.vigencia = "Y";
                                                    this.localSvc.refreshFiltros();
                                                }
                                            }
                                        );
                                }
                            }
                        );
                    } else if (response.status === 200) {
                        this.snackBar.open("Plantilla activada.");
                        plantilla.vigencia = "Y";
                        this.localSvc.refreshFiltros();
                    }
                }
            );
    }

    public onClickDesactivarPlantilla(plantilla: Plantilla): void {
        this.localSvc.desactivar(plantilla, false)
            .subscribe(
                (response: HttpResponse<void>) => {
                    if (response.status === 202) {
                        this.dialog.open(ConfirmationDialogComponent, {
                            width: '600px',
                            data: {
                                titulo: "Desactivar Plantilla",
                                mensaje: "Esta plantilla tiene asociado un proceso, ¿Desea desactivar esta plantilla y su proceso en cuestion?",
                                boton_si_clases: "btn-danger",
                                boton_no_clases: ""
                            }
                        }).afterClosed().subscribe(
                            (confirmed: boolean) => {
                                if (confirmed) {
                                    this.localSvc.desactivar(plantilla, true)
                                        .subscribe((response2: HttpResponse<void>) => {
                                            if (response2.status === 200) {
                                                this.snackBar.open("Plantilla desactivada.");
                                                plantilla.vigencia = "N";
                                                this.localSvc.refreshFiltros();
                                            }
                                        }
                                    );
                                }
                            }
                        );
                    } else if (response.status === 200) {
                        this.snackBar.open("Plantilla desactivada.");
                        plantilla.vigencia = "N";
                        this.localSvc.refreshFiltros();
                    }
                }
            );
    }

    
    //TODO terminar logica
    public onClickExportar(): void {
        let options = {
            fieldSeparator: ';',
            quoteStrings: '"',
            decimalseparator: ',',
            showLabels: true,
            showTitle: false,
            useBom: true,
            noDownload: false,
            title: '',
            headers: ["Nombre Plantilla", "Versión", "Fecha Creación", "Socio", "Vigencia"],
            nullToEmptyString: true
        };
        
        let data = [];
        this.plantillas.forEach(
            (plantilla: Plantilla) => {
                data.push({
                    nombre: plantilla.nombre,
                    version: plantilla.version,
                    fechaCreacion: plantilla.fechaCreacion,
                    socio: plantilla.socio.nombre,
                    vigencia: plantilla.vigencia === 'Y' ? 'Vigente' : 'No vigente'
                });
            }
        );

        let csv: Angular5Csv = new Angular5Csv(data, 'Plantillas', options);
    }

    public canExport(): boolean {
        return (this.paginator && this.paginator.length > 0);
    }

}
