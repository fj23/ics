import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { PlantillasHttpService } from 'src/http-services/plantillas.http-service';
import { Permisos } from 'src/app/compartido/Permisos';

@Component({
  selector: 'app-plantillas',
  templateUrl: './plantillas.component.html',
  styleUrls: ['./plantillas.component.css'],
  providers: [PlantillasHttpService]
})
export class PlantillasComponent {

  /**
   * Acción elegida en el mantenedor. Según esta se muestran y ocultan sub-componentes de este módulo.
   */
  selectedAction: string = "filtrar";
  public roles: string = "";
  public ocultarPorPermisos : boolean;

  constructor(
    private title: Title,
    private localSvc: PlantillasHttpService
  ) {
    this.title.setTitle("ICS | Plantillas");
    this.ocultarPorPermisos = Permisos.DenegarAcceso(Permisos.MantenerPlantilla());
  }

  /**
   * Evento llamado cuando se da clic a un botón que cambia la acción seleccionada.
   * @param action La acción que se ha seleccionado.
   */
  onClickAccion(action: string): void {
    //si se hace dos veces clic en la misma accion, se deselecciona
    this.selectedAction = (this.selectedAction !== action) ? action : "";
  }
}
