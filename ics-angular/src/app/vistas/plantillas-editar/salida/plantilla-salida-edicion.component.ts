import { Component, OnInit, ViewChild, Input, DoCheck, ApplicationRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatTable, MatSnackBar } from '@angular/material';
import { Observable, Subscription, BehaviorSubject, of, Subject } from 'rxjs';
import { EstandarSalidaEdicionComponent } from './estandar/salida-estandar-edicion.component';
import { EstructuradaSalidaEdicionComponent } from './estructurada/salida-estructurada-edicion.component';
import { CommonDataHttpService } from 'src/http-services/common-data.http-service';
import { FormularioPlantillasService } from '../../../../services/formulario-plantillas/formulario-plantillas.service';
import { PlantillaEModel } from 'src/models/edicion/plantillas/PlantillaEModel';
import { ArchivoEModel } from 'src/models/edicion/plantillas/ArchivoEMode';
import { AtributosSalidaOutput } from 'src/models/edicion/plantillas/AtributosSalidaOutput';
import { ColumnaArchivoSalida } from 'src/models/plantillas/salida/ColumnaArchivoSalida';
import { ColumnaDestinoEntrada } from 'src/models/plantillas/entrada/ColumnaDestinoEntrada';
import { FormularioPlantillaSalidaComponent } from '../../plantillas-crear/salida/plantilla-salida.component';
import { PlantillasHttpService } from 'src/http-services/plantillas.http-service';
import { TipoSalidaEnum } from 'src/enums/TipoSalidaEnum';
import { TipoExtensionEnum } from 'src/enums/TipoExtensionEnum';
import { EstructuraAnetoPlantillaSalidaOutputModel } from 'src/output_models/EstructuraAnetoPlantillaSalidaOutputModel';


@Component({
    selector: 'app-plantilla-salida-edicion',
    templateUrl: './plantilla-salida-edicion.component.html',
    styleUrls: ['../../plantillas-crear/salida/plantilla-salida.component.css']
})
export class PlantillaSalidaEdicionComponent 
    extends FormularioPlantillaSalidaComponent 
    implements OnInit, DoCheck {

    @ViewChild("formularioEstandar") public formularioEstandar: EstandarSalidaEdicionComponent;
    @ViewChild("formularioEstructurado") public formularioEstructurado: EstructuradaSalidaEdicionComponent;

    private _accionUser: string;
    public get accionUser() { return this._accionUser; }
    @Input() public set AccionUser(accionUser: string) {
        this._accionUser = accionUser;
        this.onChangeAccionUser();
    }

    public forceDirtyForm: boolean = false;

    public _colsSalida$: Subject<ColumnaArchivoSalida[]> = new BehaviorSubject([]);
    public colsSalida$: Observable<ColumnaArchivoSalida[]> = this._colsSalida$.asObservable();

    constructor(
        protected commonSvc: CommonDataHttpService,
        protected localSvc: PlantillasHttpService,
        protected formSvc: FormularioPlantillasService,
        protected fb: FormBuilder,
        protected snackBar: MatSnackBar,
        protected appRef: ApplicationRef
    ) {
        super(commonSvc, localSvc, formSvc, fb, snackBar);
    }

    public get isVerMode(): boolean { return this._accionUser.toLocaleLowerCase() === 'ver' };

    ngOnInit(): void {
        super.ngOnInit();
        this.forceDirtyForm = false;
    }

    ngDoCheck() {
        if (this.creacionPlantillaSalidaForm.dirty ||
            this.creacionPlantillaSalidaForm.touched) {
            this.formSvc.completitudFormularioSubject.next(true);
        }
    }

    private onChangeAccionUser() {
        const noChange = {emitEvent: false, onlySelf: true};
        if (this.isVerMode) {
            this.tipoExtension.disable(noChange);
            this.delimitadorColumnas.disable(noChange);
            this.directorioSalida.disable(noChange);
        } else {
            this.tipoExtension.enable(noChange);
            this.delimitadorColumnas.enable(noChange);
            this.directorioSalida.enable(noChange);
        }
    }

    protected onChangeTipoExtension() {
        this.formSvc.salidaTipoExtensionSubject.next(this.tipoExtension.value);
        const extensionId = this.tipoExtension.value.id;
        
        const esEstructurado: boolean = (extensionId === TipoExtensionEnum.ESTRUCTURADO);
        if (esEstructurado) {
            if (this.delimitadorColumnas.enabled) {
                this.delimitadorColumnas.disable();
            }
            if (!this.isVerMode) {
                this.core.enable();
            }
        } else {
            if (this.core.enabled) {
                this.core.disable();
            }

            if (extensionId === TipoExtensionEnum.CSV || extensionId === TipoExtensionEnum.PLANO) {
                if (!this.isVerMode) {
                    this.delimitadorColumnas.enable();
                }
                
                if (extensionId === TipoExtensionEnum.CSV) {
                    this.delimitadorColumnas.setValidators(Validators.required);
                } else {
                    this.delimitadorColumnas.setValidators(null);
                }
                this.delimitadorColumnas.updateValueAndValidity();
            } else {
                this.delimitadorColumnas.disable();
                this.delimitadorColumnas.setValue('');
                this.delimitadorColumnas.setValidators(null);
            }
        }
        
        this.esSalidaEstructurada = esEstructurado;
    }

    public forceChangeForm() {
        this.formSvc.completitudFormularioSubject.next(true);
        this.forceDirtyForm = true;
    }

    @Input() public set Plantilla(_plantilla: PlantillaEModel) {

        if (_plantilla) {
            const idTipoSalida = _plantilla.tipoSalida.idTipoSalida;
            if (idTipoSalida === TipoSalidaEnum.MANUAL &&  _plantilla.archivo) {
                const archivoE: ArchivoEModel = _plantilla.archivo;

                this.tipoExtension.setValue(archivoE.tipoExtension);
                this.directorioSalida.setValue(_plantilla.directorioSalida);

                this.onChangeTipoExtension();

                if (this.esSalidaEstructurada) {

                    if (this._coresSalida) {
                        const plantillaCore = this._coresSalida.find(c => c.id === _plantilla.core.id);
                        this.core.setValue(plantillaCore);
                    } else {
                        this.core.setValue(_plantilla.core);
                    }
                    // setTimeout es un hack menor, concede tiempo de renderizado
                    setTimeout(()=>{this.formularioEstructurado.plantillaEdicion = _plantilla;},500);
                } else {
                    if (archivoE.delimitador) {
                        this.delimitadorColumnas.setValue(archivoE.delimitador);
                    }
                    // setTimeout es un hack menor, concede tiempo de renderizado
                    setTimeout(()=>{this.formularioEstandar.plantillaEdicion = _plantilla;},500);
                }
                this.formSvc.completitudFormularioSubject.next(true);
            }
        }
    }

}
