import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { ColumnaArchivoSalida } from 'src/models/plantillas/salida/ColumnaArchivoSalida';
import { CommonDataHttpService } from 'src/http-services/common-data.http-service';
import { PlantillasHttpService } from 'src/http-services/plantillas.http-service';
import { FormularioEstandarPlantillaSalidaComponent } from '../../../plantillas-crear/salida/estandar/salida-estandar.component';
import { FormularioPlantillasService } from '../../../../../services/formulario-plantillas/formulario-plantillas.service';
import { FontawesomeIconsService } from 'src/services/fontawesome-icons/fontawesome-icons.service';
import { TipoExtensionEnum } from 'src/enums/TipoExtensionEnum';
import { PlantillaEModel } from '../../../../../models/edicion/plantillas/PlantillaEModel';
import { ArchivoEModel } from '../../../../../models/edicion/plantillas/ArchivoEMode';
import { AtributosSalidaOutput } from '../../../../../models/edicion/plantillas/AtributosSalidaOutput';
import { ColumnaDestinoEntrada } from '../../../../../models/plantillas/entrada/ColumnaDestinoEntrada';


@Component({
    selector: 'app-salida-estandar-edicion',
    templateUrl: './salida-estandar-edicion.component.html',
    styleUrls: ['../../../plantillas-crear/salida/estandar/salida-estandar.component.css']
})
export class EstandarSalidaEdicionComponent 
    extends FormularioEstandarPlantillaSalidaComponent {
    
    @Input() accionUser: string;

    @Output() change: EventEmitter<void> = new EventEmitter<void>();

    constructor(
        protected commonSvc: CommonDataHttpService,
        protected localSvc: PlantillasHttpService,
        protected formSvc: FormularioPlantillasService,
        protected fb: FormBuilder,
        protected snackBar: MatSnackBar,
        public icons: FontawesomeIconsService
    ) {
        super(commonSvc, localSvc, formSvc, fb, snackBar, icons);
    }

    public get isVerMode(): boolean { return this.accionUser.toLocaleLowerCase() === 'ver' };

    protected onChangeTipoExtension(): void {
        if (this._tipoExtension.id === TipoExtensionEnum.PLANO && !this._delimitadorColumnas) {
            this.columnasDisplayedColumns = ['nombre', 'posicion', 'accion'];
        } else {
            this.columnasDisplayedColumns = ['nombre', 'accion'];
        }
    }

    public onClickAgregarColumna(): void {
        if (!this.campoDisponibleSeleccionado) {
            this.snackBar.open('Debe seleccionar un campo de datos de origen disponible.');
        } else {
            let columna: ColumnaArchivoSalida = new ColumnaArchivoSalida();
            let columnasEmtpy: boolean = false;
            if (!this._columnasSalida) {
                this._columnasSalida = [];
                columnasEmtpy = true;
            }
            columna.orden = this._columnasSalida.length + 1;
            columna.columnaOrigen = this.campoDisponibleSeleccionado;
            this._columnasSalida.push(columna);
            this.campoDisponibleSeleccionado = null;

            this.formSvc.completitudFormularioSubject.next(true);
            this.formSvc.bloquearDatosBasicosPlantillaSubject.next(true);
            if (!columnasEmtpy) {
                this.tablaColumnasSalida.renderRows();
                this.change.emit();
            }
        }
    }

    public onClickEditarColumna(index: number): void {
        if (this._columnasSalida[index] !== this.columnaSeleccionada && !this._delimitadorColumnas) {
            this.columnaSeleccionada = this._columnasSalida[index];
            this.change.emit();
        } else {
            this.columnaSeleccionada = null;
        }
    }

    @Input() public set plantillaEdicion(_plantilla: PlantillaEModel) {
        const archivoE: ArchivoEModel = _plantilla.archivo;

        this._columnasSalida = archivoE.columnasSalida.map(
            (columnaE: AtributosSalidaOutput) => {
                let columna: ColumnaArchivoSalida = new ColumnaArchivoSalida();
                let columnaOrigen: ColumnaDestinoEntrada = new ColumnaDestinoEntrada();
                columnaOrigen.idAtributoUsuario = columnaE.columnaOrigenEM.idAtributoUsuario;
                columnaOrigen.nombreColumna = columnaE.atributo.nombre;

                columna.id = columnaE.atributo.id;
                columna.columnaOrigen = columnaOrigen;
                columna.orden = columnaE.atributo.orden;
                columna.posicionInicial = columnaE.atributo.posicionInicial;
                columna.posicionFinal = columnaE.atributo.posicionFinal;

                return columna;
            }
        );
        this.formSvc.completitudFormularioSubject.next(true);
    }
}
