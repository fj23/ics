import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlantillaSalidaEdicionComponent } from './plantilla-salida-edicion.component';

describe('PlantillaSalidaEdicionComponent', () => {
  let component: PlantillaSalidaEdicionComponent;
  let fixture: ComponentFixture<PlantillaSalidaEdicionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlantillaSalidaEdicionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlantillaSalidaEdicionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
