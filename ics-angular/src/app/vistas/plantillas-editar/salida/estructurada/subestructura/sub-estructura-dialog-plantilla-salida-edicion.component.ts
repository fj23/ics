import { Component, OnInit, ViewChild, OnDestroy, Input, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef, MatCheckboxChange, MatSnackBar } from '@angular/material';
import { Subscription, Observable } from 'rxjs';
import { MapeoIntegracion } from 'src/models/plantillas/integracion/MapeoIntegracion';
import { ColumnaMetadatos } from 'src/models/compartido/ColumnaMetadatos';
import { PlantillasHttpService } from 'src/http-services/plantillas.http-service';
import { FormularioPlantillasService } from 'src/services/formulario-plantillas/formulario-plantillas.service';


@Component({
  selector: 'app-sub-estructura-dialog-plantilla-salida-edicion',
  templateUrl: './sub-estructura-dialog-plantilla-salida-edicion.component.html',
  styleUrls: ['./sub-estructura-dialog-plantilla-salida-edicion.component.css']
})
export class SubEstructuraDialogPlantillaSalidaEdicionComponent implements OnInit, OnDestroy {

  columnasDestinoDisponibles$: Observable<MapeoIntegracion[]>;
  indiceColumnaSeleccionada: number;

  columnaDestinoSeleccionada: MapeoIntegracion;

  subEstructuraColumnasDisplayedColumns: string[] = ["codigo", "nombre", "tipoDato", "acciones"];

  filtroForm: FormGroup;
  campoFiltrarRegistrosSub: Subscription;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { columnaOrigen: ColumnaMetadatos },
    private localSvc: PlantillasHttpService,
    private formSvc: FormularioPlantillasService,
    private selfDialog: MatDialogRef<SubEstructuraDialogPlantillaSalidaEdicionComponent>,
    private snackBar: MatSnackBar,
    private fb: FormBuilder
  ) {
    this.filtroForm = this.fb.group({
      filtrarRegistros: [false],
      valorFiltrado: ['']
    });
  }

  get filtrarRegistros() { return this.filtroForm.get("filtrarRegistros"); }
  get valorFiltrado() { return this.filtroForm.get("valorFiltrado"); }


  ngOnInit(): void {

    this.campoFiltrarRegistrosSub = this.filtrarRegistros.valueChanges.subscribe(
      (filtrar: boolean) => {
        if (filtrar) {
          this.valorFiltrado.enable({ onlySelf: true, emitEvent: false });
          this.valorFiltrado.setValidators(Validators.required);
        }
        else {
          this.valorFiltrado.disable({ onlySelf: true, emitEvent: false });
          this.valorFiltrado.patchValue("");
          this.valorFiltrado.setValidators(null);
        }

      }
    );
  }

  ngOnDestroy(): void {
    this.campoFiltrarRegistrosSub.unsubscribe();
  }

  onClickSelectColumna(event: MatCheckboxChange, index: number): void {
    this.indiceColumnaSeleccionada = (event.checked) ? index : null;
  }

  onClickMapear(): void {
    if (!this.columnaDestinoSeleccionada) {
      this.snackBar.open("Debe seleccionar una columna de destino para mapear.");
    }
    else if (!this.indiceColumnaSeleccionada && this.indiceColumnaSeleccionada !== 0) {
      this.snackBar.open("Debe seleccionar una columna de la subestructura para mapear.");
    }
    else {
      this.selfDialog.close({
        columnaDestino: JSON.parse(JSON.stringify(this.columnaDestinoSeleccionada)),
        columnaSubEstructuraIndex: this.indiceColumnaSeleccionada,
        valorFiltrado: this.valorFiltrado.value
      });
    }

  }

}
