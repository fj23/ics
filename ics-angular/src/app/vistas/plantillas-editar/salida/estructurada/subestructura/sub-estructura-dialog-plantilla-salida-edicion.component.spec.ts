import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubEstructuraDialogPlantillaSalidaEdicionComponent } from './sub-estructura-dialog-plantilla-salida-edicion.component';

describe('SubEstructuraDialogPlantillaSalidaEdicionComponent', () => {
  let component: SubEstructuraDialogPlantillaSalidaEdicionComponent;
  let fixture: ComponentFixture<SubEstructuraDialogPlantillaSalidaEdicionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubEstructuraDialogPlantillaSalidaEdicionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubEstructuraDialogPlantillaSalidaEdicionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
