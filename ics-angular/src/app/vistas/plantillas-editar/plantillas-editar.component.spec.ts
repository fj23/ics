import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlantillaEdicionComponent } from './plantillas-editar.component';

describe('PlantillaEdicionComponent', () => {
  let component: PlantillaEdicionComponent;
  let fixture: ComponentFixture<PlantillaEdicionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlantillaEdicionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlantillaEdicionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
