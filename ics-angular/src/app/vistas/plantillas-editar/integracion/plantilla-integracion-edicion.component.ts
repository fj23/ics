import { Component, OnInit, ViewChild, Input, OnDestroy, DoCheck, ApplicationRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatTable, MatSnackBar, MatDialog, MatTableDataSource } from '@angular/material';
import { Observable, Subscription, BehaviorSubject, of } from 'rxjs';
import { ColumnaTrabajoIntegracion } from 'src/models/plantillas/integracion/ColumnaTrabajoIntegracion';
import { ColumnaDestinoIntegracion } from 'src/models/plantillas/integracion/ColumnaDestinoIntegracion';
import { MapeoIntegracion } from 'src/models/plantillas/integracion/MapeoIntegracion';
import { ColumnaArchivoIntegracion } from 'src/models/plantillas/integracion/ColumnaArchivoIntegracion';
import { CommonDataHttpService } from 'src/http-services/common-data.http-service';
import { FormularioPlantillasService } from '../../../../services/formulario-plantillas/formulario-plantillas.service';
import { PlantillaEModel } from 'src/models/edicion/plantillas/PlantillaEModel';
import { AtributosPlantillaIntgOutputEModel } from 'src/models/edicion/plantillas/AtributosPlantillaIntgOutputEModel';
import { AtributoEnriquecimiento } from 'src/models/plantillas/AtributoEnriquecimiento';
import { AtributosIntgOutputEModel } from 'src/models/edicion/plantillas/AtributosIntgOutputEModel';
import { MapeoIntegracionInputEModel } from 'src/models/edicion/plantillas/MapeoIntegracionInputEModel';
import { ColumnaArchivoSalidaIntegracionInputEModel } from 'src/models/edicion/plantillas/ColumnaArchivoSalidaIntegracionInputEModel';
import { FormulaIntegracion as Formulas, FormulaIntegracion } from 'src/models/plantillas/integracion/FormulaIntegracion';
import { TipoExtensionEnum } from 'src/enums/TipoExtensionEnum';
import { FontawesomeIconsService } from 'src/services/fontawesome-icons/fontawesome-icons.service';
import { FormularioPlantillaIntegracionComponent } from '../../plantillas-crear/integracion/plantilla-integracion.component';
import { PlantillasHttpService } from 'src/http-services/plantillas.http-service';
import { TipoSalidaEnum } from '../../../../enums/TipoSalidaEnum';

@Component({
    selector: 'app-plantilla-integracion-edicion',
    templateUrl: './plantilla-integracion-edicion.component.html',
    styleUrls: [
        '../../plantillas-crear/integracion/plantilla-integracion.component.css'
    ]
})
export class PlantillaIntegracionEdicionComponent 
extends FormularioPlantillaIntegracionComponent implements OnInit, OnDestroy {

    private _accionUser: string;
    public accionUser$: Observable<string>;

    public plantilla: PlantillaEModel;

    public forceDirtyForm: boolean = false;

    constructor(
        protected commonSvc: CommonDataHttpService,
        protected localSvc: PlantillasHttpService,
        protected formSvc: FormularioPlantillasService,
        protected fb: FormBuilder,
        protected snackBar: MatSnackBar,
        protected dialog: MatDialog,
        protected appRef: ApplicationRef,
        public icons: FontawesomeIconsService
    ) {
        super(commonSvc, localSvc, formSvc, fb, snackBar, dialog, appRef, icons);
    }
    
    public get isVerMode(): boolean {
        if (this._accionUser) {
            return this._accionUser.toLocaleLowerCase() === 'ver';
        } else {
            return true;
        }
    }
    
    @Input() public set AccionUser(accionUser: string) {
        this._accionUser = accionUser;
        this.accionUser$ = of(accionUser);
        const noChange = {onlySelf: true, emitEvent: false};
        
        if (this.isVerMode) {
            this.tipoSalida.disable(noChange);
            this.core.disable(noChange);
            this.formatoSalida.disable(noChange);
            this.delimitadorColumnas.disable(noChange);
            this.columnaDestinoIntegracion.disable(noChange);
        } else  {
            this.tipoSalida.enable(noChange);
            this.core.enable(noChange);
            this.camposMapeoDisponibles$ = this.localSvc.getColumnasDestinoIntegracionDisponibles(this.evento.id);
            this.columnaDestinoIntegracion.enable(noChange);
        }
    }

    private convertFormula(json: string, columnasTrabajo: ColumnaTrabajoIntegracion[]) : { orden: number, cuerpo: FormulaIntegracion.IFormulaFactor, jsonStr: string } {
        console.log(json);
        
        let formula = JSON.parse(json);
        
        formula = this.initializeFormulaDependencies(formula, columnasTrabajo);
        return {
            orden: 0,
            cuerpo: formula,
            jsonStr: json
        };
    }

    private mapeoModelFromMapeoE(mapeoE: MapeoIntegracionInputEModel, columna: ColumnaTrabajoIntegracion): MapeoIntegracion {
        let mapeo: MapeoIntegracion = new MapeoIntegracion();
        mapeo.id = mapeoE.id;
        mapeo.obligatorio = mapeoE.esObligatorio;
        mapeo.columnaOrigen = columna;
        let columnaDestino: ColumnaDestinoIntegracion = new ColumnaDestinoIntegracion();
        columnaDestino.idAtributoIntegracionEvento = mapeoE.columnaDestinoEM.idAtributoIntegracionEvento;
        columnaDestino.nombreColumna = mapeoE.columnaDestinoEM.nombreColumna;
        columnaDestino.idAtributoUsuario = mapeoE.columnaDestinoEM.idAtributoUser;
        columnaDestino.idAtributoUser = mapeoE.columnaDestinoEM.idAtributoUser;
        mapeo.columnaDestino = columnaDestino;
        mapeo.validar = mapeoE.seValida;
        return mapeo;
    }

    private setAtributoIntegracionData(columna: ColumnaTrabajoIntegracion, atributoIntegracion: AtributosIntgOutputEModel) {
        columna.atributoUser = atributoIntegracion.idIntegracionEvento;
        if (atributoIntegracion.columnaEnriquecimiento) {
            columna.columnaEnriquecimiento = atributoIntegracion.columnaEnriquecimiento;
        }
        else {
            columna.columnaOrigen = atributoIntegracion.columnaOrigen;
        }
        if (atributoIntegracion.idFormato) {
            columna.formato = { orden: atributoIntegracion.ordenFormato, id: atributoIntegracion.idFormato };
        }
        if (atributoIntegracion.idTipoHomologacion) {
            columna.homologacion = { orden: atributoIntegracion.ordenHomologacion, id: atributoIntegracion.idTipoHomologacion };
        }
    }

    private columnaTrabajoFromAtributoPlantillaIntgEModel(columnaE: AtributosPlantillaIntgOutputEModel): ColumnaTrabajoIntegracion {        
        const atributoIntegracion: AtributosIntgOutputEModel = columnaE.atributoIntegracion;
        let columna: ColumnaTrabajoIntegracion = new ColumnaTrabajoIntegracion();
        columna.id = columnaE.id;
        columna.orden = columnaE.orden;
        columna.nombre = columnaE.nombre;
        columna.esObligatorio = (columnaE.esObligatorio === 'Y');
        columna.seValida = (columnaE.seValida === 'Y');

        if (atributoIntegracion != null) {
            this.setAtributoIntegracionData(columna, atributoIntegracion);
        }
        return columna;
    }

    private initializeFormulaDependencies(formula: Formulas.IFormulaFactor, columnasTrabajo: ColumnaTrabajoIntegracion[]): Formulas.IFormulaFactor {
        let clase = Formulas[formula.tipo];
        let c1 = new clase();

        if ('valor' in formula) {
            if (c1 instanceof Formulas.CampoIntegracion) {
                let colTrabajo: ColumnaTrabajoIntegracion = columnasTrabajo.find(
                    (col: ColumnaTrabajoIntegracion) => { 
                        return formula.valor.orden === col.orden; 
                });
                c1 = new Formulas.CampoIntegracion(colTrabajo);
            }
            else {
                c1.valor = formula.valor;
            }
        } else if ('hijos' in formula && c1 instanceof Formulas.FormulaParent) {

            if ('atrEnriquecimientoId' in formula) {
                const enriquecimientoId: number = (<Formulas.FncEnriquecimiento>formula).atrEnriquecimientoId;
                let enriquecimiento = new AtributoEnriquecimiento();
                enriquecimiento.idAtributoEnriquecimiento = enriquecimientoId;
                (<Formulas.FncEnriquecimiento> c1).atrEnriquecimiento = enriquecimiento;
                (<Formulas.FncEnriquecimiento> c1).socioId = (<Formulas.FncEnriquecimiento>formula).socioId;
            }

            for (let i = 0; i < formula.hijos.length; i++) {
                const nodoHijo = formula.hijos[i];
                let hijo = this.initializeFormulaDependencies(nodoHijo, columnasTrabajo);
                hijo.padre = c1;
                c1.hijos[i] = hijo;
            }
        }

        return c1;
    }

    private setColumnasArchivoIntegracion(columnasArchivoE: ColumnaArchivoSalidaIntegracionInputEModel[]): void {
        
        for (const columnaArchivoE of columnasArchivoE) {
            const mapeoCorrespondiente: MapeoIntegracion = this._mapeosIntegracion.find(map => (map.id  === columnaArchivoE.idIntegracion));
            let columnaArchivoExiste: boolean = this._columnasArchivoIntegracion.some(col => col.id === columnaArchivoE.id);
            if (!columnaArchivoExiste) {
                mapeoCorrespondiente.enArchivoSalida = true;

                let columnaArchivoNueva = new ColumnaArchivoIntegracion();
                columnaArchivoNueva.origen = mapeoCorrespondiente;

                columnaArchivoNueva.id = columnaArchivoE.id;
                columnaArchivoNueva.posicionInicial = columnaArchivoE.posicionInicial;
                columnaArchivoNueva.posicionFinal = columnaArchivoE.posicionFinal;
                columnaArchivoNueva.separarArchivo = columnaArchivoE.separarArchivo;
                columnaArchivoNueva.incluir = columnaArchivoE.incluir;

                columnaArchivoNueva.flgVisible = columnaArchivoE.flgVisible;
                

                this._columnasArchivoIntegracion.push(columnaArchivoNueva);
            }
        }
        
        this.actualizarOrdenColumnasArchivoIntegracion();
        this._columnasArchivoIntegracion$.next(this._columnasArchivoIntegracion);
    }


    /**
     * Filtra los mapeos ingresados, que hayan sido originados desde la columna ingresada.
     * @param mapeosE 
     * @param columna 
     */
    private getMapeosDePlantillaParaColumna(mapeosE: MapeoIntegracionInputEModel[], columna: ColumnaTrabajoIntegracion): MapeoIntegracion[] {
        return mapeosE.filter((mapeoE: MapeoIntegracionInputEModel) => {
            return mapeoE.idAtributo === columna.id && 
                !!mapeoE.columnaDestinoEM;
        }).map((mapeoE: MapeoIntegracionInputEModel) => {
            return this.mapeoModelFromMapeoE(mapeoE, columna);
        });
    }

    public forceChangeForm(): void {
        this.forceDirtyForm = true;
    }

    public onChangeTipoSalida(): void {
        const noChange = { onlySelf: true, emitEvent: false };
        if (this.tipoSalida.value && this.tipoSalida.value.idTipoSalida === TipoSalidaEnum.MANUAL) {
            if (!this.isVerMode) { 
                this.formatoSalida.enable(noChange);
                this.formatoSalida.setValidators(Validators.required);
                this.incluirEncabezados.enable();   
            }
        } else {
            this.delimitadorColumnas.setValidators(null);
            this.formatoSalida.setValidators(null);
            this.formatoSalida.reset({value: null, disabled: true}, noChange);
            this.delimitadorColumnas.reset({value: null, disabled: true}, noChange);
            this.incluirEncabezados.reset({value: false, disabled: true}, noChange);
        }
        this.formatoSalida.updateValueAndValidity({emitEvent: false});
        this.actualizarValidezPlantilla();
    }

    
    @Input() public set Plantilla(_plantilla: PlantillaEModel) {

        if (_plantilla) {
            this.core.setValue(_plantilla.coreDestino);
            this.incluirEncabezados.setValue(_plantilla.incluirEncabezados);

            if (this.isVerMode) {
                this.filtrarOrigenesPlantilla.disable();
                this.incluirEncabezados.disable();
            }
            
            this.tipoSalida.setValue(_plantilla.tipoSalida);

            if (_plantilla.tipoSalida.idTipoSalida === TipoSalidaEnum.MANUAL) {
                const tipoExtension = _plantilla.tipoExtensionIntg;
                this.formatoSalida.setValue(tipoExtension);
                
                if (_plantilla.delimitadorColumnas) {
                    this.delimitadorColumnas.setValue(_plantilla.delimitadorColumnas);
                }

                this.delimitadorColumnas.disable({ onlySelf: true, emitEvent: false });
                if (!this.isVerMode && (tipoExtension.id === TipoExtensionEnum.CSV || tipoExtension.id === TipoExtensionEnum.PLANO)) {
                    this.delimitadorColumnas.enable({ onlySelf: true, emitEvent: false });
                }
            }

            this.columnasTrabajo = [];
            this._mapeosIntegracion = [];
            this._columnasArchivoIntegracion = [];

            let columnasTrabajoTmp: ColumnaTrabajoIntegracion[] = [];
            const mapeosE = _plantilla.mapeosIntegracion;            

            _plantilla.columnasTrabajo.forEach(
                (columnaE: AtributosPlantillaIntgOutputEModel) => {
                    const columna = this.columnaTrabajoFromAtributoPlantillaIntgEModel(columnaE);
                    const mapeosParaColumna = this.getMapeosDePlantillaParaColumna(mapeosE, columna);
                    this._mapeosIntegracion.push(...mapeosParaColumna);

                    columna.mapeada = (mapeosParaColumna.length > 0);
                    
                    columnasTrabajoTmp.push(columna);
                }
            );
            this._mapeosIntegracion$.next(this._mapeosIntegracion);            

            this.setColumnasArchivoIntegracion(_plantilla.columnasArchivoSalida);

            for (let i = 0; i < _plantilla.columnasTrabajo.length; i++) {
                const columnaE: AtributosPlantillaIntgOutputEModel = _plantilla.columnasTrabajo[i];

                const columna = columnasTrabajoTmp[i];
                if (columnaE.formulaJson) {
                    
                    // orden siempre viene con valor en base 1
                    columna.formula = this.convertFormula(columnaE.formulaJson, columnasTrabajoTmp);
                    if (columna.formato) {
                        columna.formato.orden--;
                    }
                    if (columna.homologacion) {
                        columna.homologacion.orden--;
                    }
                }
            }

            this.columnasTrabajo.push(...columnasTrabajoTmp);
            
            if (this.columnasTrabajo.length > 0) {
                this.columnaSeleccionada = this.columnasTrabajo[0];
            }
            this.actualizarValidezPlantilla();
        }
    }
}
