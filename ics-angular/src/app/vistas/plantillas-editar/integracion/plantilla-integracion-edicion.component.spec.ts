import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlantillaIntegracionEdicionComponent } from './plantilla-integracion-edicion.component';

describe('PlantillaIntegracionEdicionComponent', () => {
  let component: PlantillaIntegracionEdicionComponent;
  let fixture: ComponentFixture<PlantillaIntegracionEdicionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlantillaIntegracionEdicionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlantillaIntegracionEdicionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
