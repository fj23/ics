import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColumnaArchivoEstandarPlantillaEntradaEdicionComponent } from './columna-archivo-entrada-edicion.component';

describe('ColumnaArchivoEstandarPlantillaEntradaEdicionComponent', () => {
  let component: ColumnaArchivoEstandarPlantillaEntradaEdicionComponent;
  let fixture: ComponentFixture<ColumnaArchivoEstandarPlantillaEntradaEdicionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ColumnaArchivoEstandarPlantillaEntradaEdicionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColumnaArchivoEstandarPlantillaEntradaEdicionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
