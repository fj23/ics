import { Component, OnInit, Input, ViewChild, OnDestroy } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { Subscription } from 'rxjs';
import { EstructuraAneto } from 'src/models/estructuras/EstructuraAneto';
import { DetallesEstructuraPlantillaEntradaEdicionComponent } from './detalles/estructura-archivo-entrada-edicion.component';
import { CommonDataHttpService } from 'src/http-services/common-data.http-service';
import { PlantillasHttpService } from 'src/http-services/plantillas.http-service';
import { EstructurasHttpService } from 'src/http-services/estructuras.http-service';
import { ColumnaMetadatos } from 'src/models/compartido/ColumnaMetadatos';
import { Evento } from 'src/models/compartido/Evento';
import { ArchivoEstructuradoPlantillaEntradaComponent } from '../../../plantillas-crear/entrada/estructurada/archivo-entrada-estructurada.component';
import { FontawesomeIconsService } from 'src/services/fontawesome-icons/fontawesome-icons.service';
import { FormularioPlantillasService } from '../../../../../services/formulario-plantillas/formulario-plantillas.service';


@Component({
    selector: 'app-archivo-entrada-estructurada-edicion',
    templateUrl: './archivo-entrada-estructurada-edicion.component.html',
    styleUrls: ['../../../plantillas-crear/entrada/estructurada/archivo-entrada-estructurada.component.css']
})
export class ArchivoEstructuradoPlantillaEntradaEdicionComponent 
extends ArchivoEstructuradoPlantillaEntradaComponent implements OnInit, OnDestroy {


    @ViewChild("formularioEstructura") public formularioEstructura: DetallesEstructuraPlantillaEntradaEdicionComponent;

    public _accionUser: string;
    @Input() public set AccionUser(accionUser: string) {
        this._accionUser = accionUser;
        if (this.isVerMode) {
            this.creacionArchivoEntradaForm.disable();
        } else {
            this.creacionArchivoEntradaForm.enable();
        }
    }

    @Input() public evento: Evento;

    
    private formChangeSub: Subscription;

    constructor(
        protected commonSvc: CommonDataHttpService,
        protected localSvc: PlantillasHttpService,
        protected formSvc: FormularioPlantillasService,
        protected estSvc: EstructurasHttpService,
        protected fb: FormBuilder,
        protected snackBar: MatSnackBar,
        public icons: FontawesomeIconsService
    ) {
        super(commonSvc, localSvc, formSvc, estSvc, fb, snackBar, icons);
    }

    ngOnInit(): void {
        this.estructurasSimples$ = this.commonSvc.getEstructuras();
        this.onCambioArchivo();

        this.formChangeSub = this.creacionArchivoEntradaForm.valueChanges.subscribe(
            (formControls) => {
                this._archivo.nombre = formControls.nombre;
            }
        );
    }

    ngOnDestroy(): void {
        this.formChangeSub.unsubscribe();
    }

    public get isVerMode(): boolean {
        return this._accionUser === 'ver';
    }

    public onClickAgregarEstructura(): void {
        if (this.estructura.value === null) {
            this.snackBar.open("Debe seleccionar una estructura para agregarla");
        }
        else {
            if (this._archivo.estructuras.some((estructura: EstructuraAneto) => { return this.estructura.value.id === estructura.id; })) {
                this.snackBar.open("Esta estructura ya se encuentra asignada al archivo.");
            }
            else {
                this.estructura.disable({ onlySelf: true, emitEvent: false });
                this.cargandoEstructura = true;
                this.estSvc.buscar(this.estructura.value.id).subscribe(
                    (estr: EstructuraAneto) => {
                        this.cargandoEstructura = false;
                        this._archivo.estructuras.push(estr);
                        this.estructurasTable.renderRows();
                        this.estructura.setValue(null);
                        this.estructura.enable({ onlySelf: true, emitEvent: false });
                    }
                );
            }
        }
    }

    public onClickVerDetalles(index: number) {
        if (this.estructuraSeleccionada !== this._archivo.estructuras[index]) {
            this.estructuraSeleccionada = this._archivo.estructuras[index];
            if (this.formularioEstructura) {
                this.formularioEstructura.indiceColumnaSeleccionada = null;
            }
        }
        else {
            this.estructuraSeleccionada = null;
        }
    }

    public onClickQuitarEstructura(index: number) {
        const estructura: EstructuraAneto = this._archivo.estructuras[index];
        if (estructura.columnas.some(
            (columna: ColumnaMetadatos) => {
                return columna.mapeada || (columna.subEstructura && columna.subEstructura.columnas.some(
                    (subcolumna: ColumnaMetadatos) => { return subcolumna.mapeada; }
                ));
            })
        ) {
            this.snackBar.open("No puede quitar esta estructura; primero elimine todo mapeo asociado a sus columnas o subestructuras.");
        } else {
            if (estructura === this.estructuraSeleccionada) {
                this.estructuraSeleccionada = null;
            }
            this._archivo.estructuras.splice(index, 1);
            this.estructurasTable.renderRows();
        }
    }

}
