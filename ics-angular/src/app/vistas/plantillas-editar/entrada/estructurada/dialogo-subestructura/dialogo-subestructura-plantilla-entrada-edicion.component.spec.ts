import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogoSubestructuraPlantillaEntradaEdicionComponent } from './dialogo-subestructura-plantilla-entrada-edicion.component';

describe('DialogoSubestructuraPlantillaEntradaEdicionComponent', () => {
  let component: DialogoSubestructuraPlantillaEntradaEdicionComponent;
  let fixture: ComponentFixture<DialogoSubestructuraPlantillaEntradaEdicionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogoSubestructuraPlantillaEntradaEdicionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogoSubestructuraPlantillaEntradaEdicionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
