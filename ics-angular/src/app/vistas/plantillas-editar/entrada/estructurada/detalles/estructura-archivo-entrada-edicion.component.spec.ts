import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetallesEstructuraPlantillaEntradaEdicionComponent } from './estructura-archivo-entrada-edicion.component';

describe('DetallesEstructuraPlantillaEntradaEdicionComponent', () => {
  let component: DetallesEstructuraPlantillaEntradaEdicionComponent;
  let fixture: ComponentFixture<DetallesEstructuraPlantillaEntradaEdicionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetallesEstructuraPlantillaEntradaEdicionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetallesEstructuraPlantillaEntradaEdicionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
