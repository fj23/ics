import { Component, OnInit, Input, ViewChild, OnDestroy, Output, EventEmitter } from '@angular/core';
import { MatTable, MatSnackBar, MatCheckboxClickAction, MatCheckboxChange, MatDialog } from '@angular/material';
import { CommonDataHttpService } from 'src/http-services/common-data.http-service';
import { EstructuraAneto } from 'src/models/estructuras/EstructuraAneto';
import { ColumnaMetadatos } from 'src/models/compartido/ColumnaMetadatos';
import { SubEstructurasHttpService } from 'src/http-services/subestructuras.http-service';
import { PlantillasHttpService } from 'src/http-services/plantillas.http-service';
import { SubEstructuraAneto } from 'src/models/estructuras/SubEstructuraAneto';
import { ColumnaDestinoEntrada } from 'src/models/plantillas/entrada/ColumnaDestinoEntrada';
import { MapeoEntrada } from 'src/models/plantillas/entrada/MapeoEntrada';
import { DialogoSubestructuraPlantillaEntradaEdicionComponent } from '../dialogo-subestructura/dialogo-subestructura-plantilla-entrada-edicion.component';
import { Evento } from 'src/models/compartido/Evento';
import { EstructuraArchivoEstructuradoPlantillaEntradaComponent } from '../../../../plantillas-crear/entrada/estructurada/detalles/estructura-archivo-entrada.component';
import { FontawesomeIconsService } from 'src/services/fontawesome-icons/fontawesome-icons.service';
import { FormularioPlantillasService } from 'src/services/formulario-plantillas/formulario-plantillas.service';


@Component({
  selector: 'app-estructura-archivo-entrada-edicion',
  templateUrl: './estructura-archivo-entrada-edicion.component.html',
  styleUrls: ['../../../../plantillas-crear/entrada/estructurada/detalles/estructura-archivo-entrada.component.css']
})
export class DetallesEstructuraPlantillaEntradaEdicionComponent extends EstructuraArchivoEstructuradoPlantillaEntradaComponent {

  @Input() public estructura: EstructuraAneto;

  @ViewChild(MatTable) public estructuraColumnasTable: MatTable<ColumnaMetadatos>;
  estructuraColumnasDisplayedColumns: string[] = ["nombre", "tipoDato", "acciones"];

  public indiceColumnaSeleccionada: number;

  public columnaSubEstructuraSeleccionada: ColumnaMetadatos;

  accionUser: string;
  @Input() set AccionUser(accionUser: string) {
    this.accionUser = accionUser;
    if (this.accionUser === "editar") {
      this.loading = false;
    }
  }

  @Input() public evento: Evento;

  constructor(
    protected commonSvc: CommonDataHttpService,
    protected formSvc: FormularioPlantillasService,
    protected subEstSvc: SubEstructurasHttpService,
    protected localSvc: PlantillasHttpService,
    protected snackBar: MatSnackBar,
    protected dialog: MatDialog,
    public icons: FontawesomeIconsService
  ) {
    super(commonSvc, formSvc, subEstSvc, localSvc, snackBar, dialog, icons);
  }

  public onClickSelectColumna(event: MatCheckboxChange, index: number): void {
    this.columnaSubEstructuraSeleccionada = null;
    this.indiceColumnaSeleccionada = (event.checked) ? index : null;
  }

  public onClickVerDetalles(index: number): void {
    let columnaEstructura: ColumnaMetadatos = this.estructura.columnas[index];
    if (!columnaEstructura.subEstructura) {
      this.loading = true;
      this.subEstSvc.buscar(columnaEstructura.idSubEstructura).subscribe(
        (subEstructura: SubEstructuraAneto) => {
          this.loading = false;
          if (subEstructura) {
            columnaEstructura.subEstructura = subEstructura;
            this.realizarMapeoSubEstructura(columnaEstructura);
          }
        }
      );
    }
    else {
      this.realizarMapeoSubEstructura(columnaEstructura);
    }

  }

  protected realizarMapeoSubEstructura(columnaOrigen: ColumnaMetadatos): void {

    this.dialog.open(DialogoSubestructuraPlantillaEntradaEdicionComponent,
      {
        data: {
          columnaOrigen: columnaOrigen,
          accionUser: this.accionUser,
          evento: this.evento
        },
        height: "35rem",
        width: "80rem"
      }).afterClosed().subscribe( //ejecuta la funcion siguiente tras realizar el mapeo o hacer clic en cancelar
        (data: { columnaDestino: ColumnaDestinoEntrada, columnaSubEstructuraIndex: number, valorFiltrado: string }) => {
          if (data) {

            let mapeo = new MapeoEntrada();
            mapeo.columnaDestino = data.columnaDestino;
            mapeo.valorParaFiltrar = data.valorFiltrado;
            mapeo.estructuraOrigen = this.estructura;
            mapeo.columnaOrigenDatos = columnaOrigen;
            mapeo.subEstructuraColumnaOrigen = columnaOrigen.subEstructura.columnas[data.columnaSubEstructuraIndex];

            mapeo.subEstructuraColumnaOrigen.mapeada = true;
            this.formSvc.entradaMapeoColumnaSubEstructuraSubject.next(mapeo);
          }
        }
      );
  }

}
