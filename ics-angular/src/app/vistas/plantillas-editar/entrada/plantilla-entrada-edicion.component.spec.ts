import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { PlantillaEntradaEdicionComponent } from './plantilla-entrada-edicion.component';


describe('PlantillaEntradaEdicionComponent', () => {
  let component: PlantillaEntradaEdicionComponent;
  let fixture: ComponentFixture<PlantillaEntradaEdicionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlantillaEntradaEdicionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlantillaEntradaEdicionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
