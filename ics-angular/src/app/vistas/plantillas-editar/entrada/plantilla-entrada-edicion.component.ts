import { Component, OnInit, OnDestroy, Input, ApplicationRef, EventEmitter, Output, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialog, MatSnackBar } from '@angular/material';
import { Observable, of } from 'rxjs';
import { TipoExtension } from 'src/models/compartido/TipoExtension';
import { ArchivoEntrada } from 'src/models/plantillas/entrada/ArchivoEntrada';
import { MapeoEntrada } from 'src/models/plantillas/entrada/MapeoEntrada';
import { CommonDataHttpService } from 'src/http-services/common-data.http-service';
import { FormularioPlantillasService } from '../../../../services/formulario-plantillas/formulario-plantillas.service';
import { ColumnaMetadatos } from 'src/models/compartido/ColumnaMetadatos';
import { PlantillaEModel } from 'src/models/edicion/plantillas/PlantillaEModel';
import { MapeoColumnaDestinoInputEModel } from 'src/models/edicion/plantillas/MapeoColumnaDestinoInputEModel';
import { EstructuraAneto } from 'src/models/estructuras/EstructuraAneto';
import { TipoExtensionEnum } from 'src/enums/TipoExtensionEnum';
import { TipoExtensionEModel } from 'src/models/edicion/plantillas/TipoExtensionEModel';
import { ArchivoEntradaInputEModel } from 'src/models/edicion/plantillas/ArchivoEntradaInputEModel';
import { ColumnaMetadatosInputEModel } from 'src/models/edicion/plantillas/ColumnaMetadatosInputEModel';
import { TipoDato } from 'src/models/compartido/TipoDato';
import { FormularioPlantillaEntradaComponent } from '../../plantillas-crear/entrada/plantilla-entrada.component';
import { ArchivoEstandarPlantillaEntradaEdicionComponent } from './estandar/archivo-entrada-estandar-edicion.component';
import { ArchivoEstructuradoPlantillaEntradaEdicionComponent } from './estructurada/archivo-entrada-estructurada-edicion.component';
import { FontawesomeIconsService } from 'src/services/fontawesome-icons/fontawesome-icons.service';
import { PlantillasHttpService } from 'src/http-services/plantillas.http-service';
import { ConfirmationDialogComponent } from 'src/app/dialogs/confirmation/confirmation.dialog.component';


@Component({
    selector: 'app-plantilla-entrada-edicion',
    templateUrl: './plantilla-entrada-edicion.component.html',
    styleUrls: ['../../plantillas-crear/entrada/plantilla-entrada.component.css']
})
export class PlantillaEntradaEdicionComponent 
    extends FormularioPlantillaEntradaComponent implements OnInit, OnDestroy {

    private _accionUser: string = "";
    public accionUser$: Observable<string> = of(this._accionUser);
    @Input() public set AccionUser(accionUser: string) {
        this._accionUser = accionUser;
        this.accionUser$ = of(this._accionUser);
        this.updateViewToUserAction();
        //this.actualizarValidezPlantilla();
    }

    public loading$: Observable<boolean> = of(true);
    
    @ViewChild("formularioEstandar") public formularioArchivo: ArchivoEstandarPlantillaEntradaEdicionComponent;
    @ViewChild("formularioEstructurado") public formularioArchivoEstructurado: ArchivoEstructuradoPlantillaEntradaEdicionComponent;

    constructor(
        protected appRef: ApplicationRef,
        protected commonSvc: CommonDataHttpService,
        protected localSvc: PlantillasHttpService,
        protected formSvc: FormularioPlantillasService,
        protected fb: FormBuilder,
        protected dialog: MatDialog,
        protected snackBar: MatSnackBar,
        public icons: FontawesomeIconsService
    ) {
        super(appRef, commonSvc, localSvc, formSvc, fb, dialog, snackBar, icons);
    }
    
    public get isVerMode(): boolean { return this._accionUser.toLocaleLowerCase() === 'ver'; }

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }
    

    private makeColumnaModelFromColumnaInput(columnaE: ColumnaMetadatosInputEModel) {
        let columna: ColumnaMetadatos = new ColumnaMetadatos();
        columna.id = columnaE.id;
        columna.descripcion = columnaE.nombre;
        columna.orden = columnaE.orden;
        columna.largo = columnaE.largo;
        columna.tipoDato = <TipoDato>columnaE.tipoDato;
        columna.posicionInicial = columnaE.posicionInicial;
        columna.posicionFinal = columnaE.posicionFinal;
        return columna;
    }

    private makeArchivoModelFromArchivoInput(archivoE: ArchivoEntradaInputEModel): ArchivoEntrada {
        
        let archivo: ArchivoEntrada = new ArchivoEntrada();
        archivo.id = archivoE.idArchivo;
        archivo.nombre = archivoE.nombreArchivo;
        archivo.vigencia = archivoE.vigenciaArchivo;
        archivo.delimitadorColumnas = archivoE.delimitador;
        archivo.numeroHoja = archivoE.numeroHoja? archivoE.numeroHoja : null;
        archivo.saltarRegistrosIniciales = archivoE.lineaInicial;
        archivo.saltarRegistrosFinales = archivoE.lineaFinal;
        archivo.noRealizarTransformaciones = (archivoE.flgSinTransformacion==="Y");
        archivo.tipo = <TipoExtension>archivoE.tipoExtension;

        if (archivoE.columnas && archivoE.columnas.length) {
            archivo.columnas = archivoE.columnas.map((col) => { return this.makeColumnaModelFromColumnaInput(col); });
        } else if (archivoE.estructurasOutput && archivoE.estructurasOutput.length) {
            archivo.estructuras = [];
            archivo.estructuras.push(...archivoE.estructurasOutput);
        }

        return archivo;
    }

    private updateViewToUserAction() {
        if (this._archivos.length > 0) {
            this.tipoExtension.disable();
        }

        if (this._accionUser === "editar") {
            // this.valorFiltrado.enable();
            this.filtrarRegistros.enable({onlySelf: true, emitEvent: false});
        } else {
            this.tipoExtension.disable({onlySelf: true, emitEvent: false});
            this.mapearColumnasForm.disable({onlySelf: true, emitEvent: false});
        }
    }
    
    private mapeoFromEModel(mapeoE: MapeoColumnaDestinoInputEModel, maxGrupo: number) {
        let mapeo: MapeoEntrada = new MapeoEntrada();
        mapeo.archivoOrigenDatos = this._archivos.find((arch: ArchivoEntrada) => { return mapeoE.archivoOrigen === arch.id || (mapeoE.archivoOrigenDatosEM && mapeoE.archivoOrigenDatosEM.idArchivo === arch.id); });
        if (mapeo.archivoOrigenDatos.tipo.id !== TipoExtensionEnum.ESTRUCTURADO) {
            mapeo.columnaOrigenDatos = mapeo.archivoOrigenDatos.columnas.find((col: ColumnaMetadatos) => { return col.orden === mapeoE.columnaOrigenDatosEM.orden; });
            mapeo.columnaOrigenDatos.mapeada = true;
        }
        else {
            mapeo.estructuraOrigen = mapeo.archivoOrigenDatos.estructuras.find((estr: EstructuraAneto) => { return estr.id === mapeoE.estructuraOrigenEM.id; });
            mapeo.columnaOrigenDatos = mapeo.estructuraOrigen.columnas.find((col: ColumnaMetadatos) => { return col.id === mapeoE.columnaOrigenDatosEM.id; });
            if (mapeoE.subEstructuraColumnaOrigen) {
                let subestructura: ColumnaMetadatos = new ColumnaMetadatos();
                subestructura.id = mapeoE.subEstructuraColumnaOrigen;
                const colSubEst: ColumnaMetadatos = mapeoE.subEstructuraColumnaOrigenEM.columnas.find((col: ColumnaMetadatos) => {
                    return col.id === subestructura.id;
                });
                subestructura.descripcion = colSubEst.descripcion;
                subestructura.codigo = colSubEst.codigo;
                subestructura.vigencia = colSubEst.vigencia;
                mapeo.subEstructuraColumnaOrigen = subestructura;
                mapeo.subEstructuraColumnaOrigen.mapeada = true;
            }
            else {
                mapeo.columnaOrigenDatos.mapeada = true;
            }
        }
        mapeo.columnaDestino = mapeoE.columnaDestinoEM;
        mapeo.grupoColumnaDestino = mapeoE.lote;
        mapeo.valorParaFiltrar = mapeoE.valorParaFiltrar;
        mapeo.esObligatorio = mapeoE.esObligatorio;
        mapeo.filtroExclusion = mapeoE.filtroExclusion;
        mapeo.archivoOrigenDatos.mapeado = true;
        mapeo.encriptar = mapeoE.encriptar;
        if (mapeoE.lote > maxGrupo) {
            maxGrupo = mapeoE.lote;
        }
        return { __return: mapeo, maxGrupo };
    }

    /**
     * Lee los mapeos de la plantilla y los asigna a la plantilla actual.
     * Además, establece los grupos de mapeo (por personas).
     * @param plantilla 
     */
    private setMapeosFromPlantillaInput(plantilla: PlantillaEModel) {

        this.gruposMapeo = [];
        let maxGrupo: number = 0;
        
        let i = 0;
        this._mapeos = plantilla.mapeosEntrada.map((mapeoE: MapeoColumnaDestinoInputEModel) => {
            i++;
            let __return;
            ({ __return, maxGrupo } = this.mapeoFromEModel(mapeoE, maxGrupo));
            return __return;
        });
        this._mapeos$.next(this._mapeos);
        
        if (maxGrupo > 0) {
            for (let i = 0; i < maxGrupo; i++) {
                this.gruposMapeo[i] = i+1; 
            }
        }

    }

    protected quitarArchivo(index: number): void {
        const archivoAEliminar: ArchivoEntrada = this._archivos[index];
        if (archivoAEliminar === this.archivoSeleccionado) {
            this.archivoSeleccionado = null;
        }

        this._archivos.splice(index, 1);
        this._archivos$.next(this._archivos);

        if (this._archivos.length === 0) {
            this.tipoExtension.enable();
        }
        this.actualizarValidezPlantilla();
    }

    public onClickQuitarArchivo(index: number): void {
        if (index <= this._archivos.length - 1) {
            const archivo: ArchivoEntrada = this._archivos[index];
            const mapeosCountForThisArchivo = this.cantidadMapeosParaArchivo(archivo);
            if (mapeosCountForThisArchivo > 0) {
                this.snackBar.open("Este archivo posee "+mapeosCountForThisArchivo+" mapeos. Primero debe deshacerlos antes de eliminar el archivo de la plantilla.");
            } else {
                const confirmationDialog = this.dialog.open(ConfirmationDialogComponent, {
                    data: {
                        titulo: "Quitar Archivo",
                        mensaje: "Si quita este archivo del listado, no podrá recuperar la configuración efectuada en él. ¿Desea confirmar esta acción?",
                        boton_si_clases: "btn-warning",
                        boton_no_clases: ""
                    }
                });
    
                confirmationDialog.afterClosed().subscribe(
                    (confirmed: boolean) => {
                        if (confirmed) {
                            this.quitarArchivo(index);
                        }
                    }
                );
            }
        }
    }

    public onClickQuitarColumnaDestinoMapeada(index: number): void {
        super.onClickQuitarColumnaDestinoMapeada(index);
        this.actualizarValidezPlantilla();
    }

    public onChangeValidezArchivoSeleccionado(valido: boolean) {
        if (valido) {
            if (!this.isVerMode) {
                this.filtrarRegistros.enable();
                this.columnaDestinoSeleccionada.enable();
            }
            this.onToggleFiltrarRegistros();
        }
        this.actualizarValidezPlantilla();
    }
    
    @Input() public set Plantilla(plantilla: PlantillaEModel) {

        this.loading$ = of(true);
        this.mapearColumnasForm.reset();
        this.creacionPlantillaEntradaForm.markAsPristine();
        this.creacionPlantillaEntradaForm.markAsUntouched();

        if (this.formularioArchivo &&
            this.formularioArchivo.creacionArchivoEntradaForm) {

            this.formularioArchivo.creacionArchivoEntradaForm.markAsPristine();
            this.formularioArchivo.creacionArchivoEntradaForm.markAsUntouched();

        } else if (this.formularioArchivoEstructurado &&
            this.formularioArchivoEstructurado.creacionArchivoEntradaForm) {

            this.formularioArchivoEstructurado.creacionArchivoEntradaForm.markAsPristine();
            this.formularioArchivoEstructurado.creacionArchivoEntradaForm.markAsUntouched();
        }

        if (plantilla != null) {

            const plantillaExtension: TipoExtensionEModel = plantilla.archivos[0].tipoExtension;
            this.tipoExtension.setValue( <TipoExtension>plantillaExtension );

            if (plantillaExtension.id === TipoExtensionEnum.ESTRUCTURADO) { //estructurado
                this.columnasDestinoMapeadasDisplayedColumns = ["nombre", "origenEstructurado", "encriptar", "filtro", "obligatorio", "grupo", "acciones"];
            } else {
                this.columnasDestinoMapeadasDisplayedColumns = ["nombre", "origenEstandar", "encriptar", "filtro", "obligatorio", "grupo", "acciones"];
            }

            this._archivos = plantilla.archivos.map(arch => this.makeArchivoModelFromArchivoInput(arch));
            this._archivos$.next(this._archivos);
            
            if (this._archivos.length > 0) {
                this.tipoExtension.disable();
            } else {
                this.tipoExtension.enable();
            }

            this.formSvc.entradaFormatoSubject.next(plantillaExtension.id);
            this.setMapeosFromPlantillaInput(plantilla);

            this.actualizarValidezPlantilla();

        }
            
        this.loading$ = of(false);
    }


}
