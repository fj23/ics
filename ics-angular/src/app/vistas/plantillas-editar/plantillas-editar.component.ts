import { Component, OnInit, OnDestroy, ApplicationRef, DoCheck, ViewChild } from '@angular/core';
import { Observable, Subscription, of, merge, Subject, BehaviorSubject } from 'rxjs';
import { FormBuilder } from '@angular/forms';
import { CommonDataHttpService } from 'src/http-services/common-data.http-service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormularioPlantillasService } from '../../../services/formulario-plantillas/formulario-plantillas.service';
import { MatSnackBar, MatDialog } from '@angular/material';
import { PlantillaEModel } from 'src/models/edicion/plantillas/PlantillaEModel';
import { PlantillasCrearComponent } from '../plantillas-crear/plantillas-crear.component';
import { PlantillaEntradaEdicionComponent } from './entrada/plantilla-entrada-edicion.component';
import { PlantillaSalidaEdicionComponent } from './salida/plantilla-salida-edicion.component';
import { FontawesomeIconsService } from 'src/services/fontawesome-icons/fontawesome-icons.service';
import { Title } from '@angular/platform-browser';
import { PlantillasHttpService } from 'src/http-services/plantillas.http-service';
import { TipoSalidaEnum } from '../../../enums/TipoSalidaEnum';
import { PlantillaIntegracionEdicionComponent } from './integracion/plantilla-integracion-edicion.component';

@Component({
    selector: 'app-plantillas-editar',
    templateUrl: './plantillas-editar.component.html',
    styleUrls: ['../plantillas-crear/plantillas-crear.component.css']
})
export class PlantillaEdicionComponent 
    extends PlantillasCrearComponent 
    implements OnInit, OnDestroy {

    protected readonly acciones = [ 'ver', 'editar', 'copiar' ];
    protected _accionUser: string;

    public accionUser$: Observable<string>;

    private set AccionUser(accionUser: string) {
        if (!accionUser) {
            this._accionUser = this.acciones[0];
        } else {
            this._accionUser = accionUser;
            if (this.isViewingMode) {
                this.title.setTitle("ICS | Ver Plantilla");
            } else if (this.isEditingMode) {
                this.title.setTitle("ICS | Editar Plantilla");
            } else if (this.isCopyingMode) {
                this.title.setTitle("ICS | Copiar Plantilla");
            }
        }
        this.loadPlantillaFromId(this.idPlantilla);

        this.accionUser$ = of(this._accionUser);
    }

    protected _plantilla$: Subject<PlantillaEModel>;
    public plantilla$: Observable<PlantillaEModel>;

    @ViewChild("plantillaEntrada") public plantillaEntrada: PlantillaEntradaEdicionComponent;
    @ViewChild("plantillaIntegracion") public plantillaIntegracion: PlantillaIntegracionEdicionComponent;
    @ViewChild("plantillaSalida") public plantillaSalida: PlantillaSalidaEdicionComponent;

    private routerAccionUserSub: Subscription;

    constructor(
        protected appRef: ApplicationRef,
        protected commonSvc: CommonDataHttpService,
        protected localSvc: PlantillasHttpService,
        protected router: Router,
        protected formSvc: FormularioPlantillasService,
        protected fb: FormBuilder,
        protected snackBar: MatSnackBar,
        protected dialog: MatDialog,
        private route: ActivatedRoute,
        public title: Title,
        public icons: FontawesomeIconsService
    ) {
        super(appRef, commonSvc, localSvc, router, formSvc, fb, snackBar, dialog, title, icons);
        this._plantilla$ = new BehaviorSubject(null);
        this.plantilla$ = this._plantilla$.asObservable();
    }

    public get isViewingMode(): boolean { return this._accionUser.toLocaleLowerCase() === this.acciones[0]; }
    public get isEditingMode(): boolean { return this._accionUser.toLocaleLowerCase() === this.acciones[1]; }
    public get isCopyingMode(): boolean { return this._accionUser.toLocaleLowerCase() === this.acciones[2]; }
    
    public get canDeactivateForm(): boolean { return this.isViewingMode || this.formSubmitted; }

    public get idPlantilla(): number { 
        const id = this.route.snapshot.paramMap.get('id'); 
        if (id) {
            const nId = Number(id);
            if (!isNaN(nId)) {
                return nId;
            }
        }
        return null;
    }

    ngOnInit(): void {
        // NO USAR super.ngOnInit() 
        // iria a consultar la version de plantilla
        // y provocaria un loop infinito

        this.eventos$ = this.commonSvc.getEventos();
        this.socios$ = this.commonSvc.getSocios();
        this.tiposPlantilla$ = this.commonSvc.getTiposPlantilla();
        this.nombresPlantilla$ = this.localSvc.getNombresPlantilla();

        this.changeBloqueoDatosBasicosSub = this.formSvc.bloquearDatosBasicosPlantillaSubject.subscribe((bloquear) => { this.onChangeBloqueoDatosBasicosPlantilla(bloquear); });
        this.changeCompletitudFormularioSub = this.formSvc.completitudFormularioSubject.subscribe((completo) => { this.onChangeCompletitudFormulario(completo); });

        //este setter desencadena el proceso completo de carga de la plantilla en base al tipo de accion ver/editar
        this.routerAccionUserSub = this.route.params.subscribe((params) => {
            const noChange = { onlySelf: true, emitEvent: false };
            this.creacionPlantillaForm.enable(noChange);
            this.evento.disable(noChange);
            this.tipo.disable(noChange);
            this.version.disable(noChange);
            this.AccionUser = params.accion;
        });
    }

    ngOnDestroy(): void {
        if (this.routerAccionUserSub) { this.routerAccionUserSub.unsubscribe(); }
        if (this.changeBloqueoDatosBasicosSub) { this.changeBloqueoDatosBasicosSub.unsubscribe(); }
        if (this.changeCompletitudFormularioSub) { this.changeCompletitudFormularioSub.unsubscribe(); }
        if (this.changeIdentifierSub) { this.changeIdentifierSub.unsubscribe(); }
    }

    private loadPlantillaFromId(id: number) {

        if (!id) {
            this.snackBar.open("Id de plantilla inválido.");
            this.router.navigate(["/plantillas"]);
        } else {
            this._plantilla$.next(null);
            this.localSvc.getPlantillaByIdEdicion(id)
                .subscribe((plantilla: PlantillaEModel) => {
                    
                    if (plantilla) {
                        const noChange = { onlySelf: true, emitEvent: false };
                        this.plantillaId = plantilla.id;

                        this.nombre.setValue(plantilla.nombre, noChange);
                        this.observaciones.setValue(plantilla.observaciones, noChange);
                        this.tipo.setValue(plantilla.tipo, noChange);
                        this.evento.setValue(plantilla.evento, noChange);
                        this.socio.setValue(plantilla.socio, noChange);

                        this.creacionPlantillaForm.updateValueAndValidity();
                        if (this.isViewingMode) {
                            this.creacionPlantillaForm.disable(noChange);
                        } else {
                            if (this.isEditingMode) {
                                this.nombre.disable(noChange);
                                this.evento.disable(noChange);
                                this.socio.disable(noChange);
                                this.tipo.disable(noChange);
                            } else if (this.isCopyingMode) {
                                this.nombre.reset("", noChange);
                                this.socio.reset(null, noChange);
                            }
                        }

                        if (plantilla.tipo.id === 100002 && plantilla.tipoSalida.idTipoSalida === TipoSalidaEnum.AUTOMATICA) {
                            this.salidaFormatoOriginalEstado.setValue(true, noChange);
                            this.formSvc.completitudFormularioSubject.next(true);
                        }

                        if (this.isViewingMode) {
                            this.version.setValue(plantilla.version, noChange);
                        } else if (this.isEditingMode) {
                            this.version.setValue("Cargando...", noChange);
                            this.onChangeIdentificadoresPlantilla();
                        } else if (this.isCopyingMode) {
                            this.version.setValue("", noChange);
                            this.changeIdentifierSub = merge( 
                                this.nombre.valueChanges, 
                                this.socio.valueChanges
                            ).subscribe( () => { this.onChangeIdentificadoresPlantilla(); } );
                        }
                        this._plantilla$.next(plantilla);
                    }
                }, (err) => {
                    console.error(err);
                }
            );
        }
    }

    public isPlantillaSalidaEditable(): boolean {
        if (this.salidaFormatoOriginalEstado.value) {
            return false;
        } else {
            if ((this.tipo.value && this.tipo.value.id === 100002) && this.idPlantilla) {
                return true;
            }
        }
        return false;
    }

}
