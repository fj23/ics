import { DataSource } from "@angular/cdk/table";
import { CollectionViewer } from "@angular/cdk/collections";
import { Observable, BehaviorSubject, of, Subject, Subscription } from "rxjs";
import { catchError, finalize } from "rxjs/operators";

import { TrazabilidadArchivoModel } from "src/models/trazabilidad/TrazabilidadArchivoModel";
import { FiltrosTrazabilidadArchivosModel } from "src/models/filters/FiltrosTrazabilidadArchivosModel";
import { PaginaRegistros } from "src/models/compartido/PaginaRegistros";
import { TrazabilidadArchivosHttpService } from 'src/http-services/trazabilidad-archivos.http-service';

export class TrazabilidadDataSource
  implements DataSource<TrazabilidadArchivoModel> {
  private cargaSubject = new BehaviorSubject<boolean>(false);

  /** Recibe y transmite el array de archivos activo a la vista. */
  private archivosSubject = new Subject<TrazabilidadArchivoModel[]>();
  public archivos$: Observable<TrazabilidadArchivoModel[]> = this.archivosSubject.asObservable();

  /** Recibe y transmite la cantidad de archivos conseguidas por los filtros activos.
   * Esta cantidad suele superar el número de registros en la página actual. */
  private archivosCountSubject = new BehaviorSubject<number>(0);

  //observables
  public archivosCount$ = this.archivosCountSubject.asObservable();
  public carga$ = this.cargaSubject.asObservable();

  private filtrosChangeSub: Subscription;

  constructor(private localSvc: TrazabilidadArchivosHttpService) {
	  //cuando los filtros, el paginado y/o el orden de la tabla cambien, obtiene las homologaciones
		this.filtrosChangeSub = this.localSvc.filtrosChange$.subscribe(
			() => this.getArchivos()
		);
  }

  connect(collectionViewer: CollectionViewer): Observable<TrazabilidadArchivoModel[] | ReadonlyArray<TrazabilidadArchivoModel>> {
    return this.archivosSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this.archivosSubject.complete();
    this.cargaSubject.complete();
  }

  /**
   * Solicita la página de archivos respectiva.
   * @param pageIndex El índice (con base 0) de la página.
   * @param pageSize La cantidad de registros a mostrar por página.
   */
  public getArchivosPage(
    pageIndex: number,
    pageSize: number,
    sortColumn: string,
    sortOrder: string
  ): void {
    let filtros: FiltrosTrazabilidadArchivosModel = this.localSvc.getFiltros();
    filtros.pageIndex = pageIndex;
    filtros.pageSize = pageSize;
    filtros.sortColumn = sortColumn;
    filtros.sortOrder = sortOrder;

    this.localSvc.setFiltros(filtros);
  }

  /**
   * Recarga directamente las archivos haciendo una llamada al servicio de archivos.
   * Éste debiera ser el último método llamado en la cadena de ejecución para este DataSource.
   */
  private getArchivos(): void {
    //si ya está cargando archivos, evita sobrecargar las peticiones
    if (!this.cargaSubject.getValue()) {
      this.cargaSubject.next(true);

      this.localSvc
        .listar()
        .pipe(
          catchError(() => of([])),
          finalize(() => this.cargaSubject.next(false)) //termine bien o mal la peticion, la carga se detiene
        )
        .subscribe(
          (payload: PaginaRegistros<TrazabilidadArchivoModel>) => {
            this.archivosSubject.next(payload.items); //el listado de archivos se almacena
            if (this.archivosCountSubject.getValue() != payload.count) {
              this.archivosCountSubject.next(payload.count);
            }
          }
        );
    }
  }
}
