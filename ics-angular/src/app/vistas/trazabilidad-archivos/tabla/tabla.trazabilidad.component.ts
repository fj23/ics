import { Component, OnInit, ViewChild, OnDestroy } from "@angular/core";
import {
  MatSort,
  MatPaginator,
  MatDialog,
  MatSnackBar
} from "@angular/material";

import { MatPaginatorES } from "src/app/compartido/mat.paginator.es";
import { TrazabilidadDataSource } from "./tabla.trazabilidad.datasource";
import { merge, Subscription } from "rxjs";
import { saveAs } from "file-saver";
import { InformationDialogComponent } from "src/app/dialogs/information/information.dialog.component";
import { TrazabilidadArchivosHttpService } from '../../../../http-services/trazabilidad-archivos.http-service';

export const ESTADOS_CON_LOG_DESHABILITADO: string[] = [ "Recibido", "Validado", "Entrada Socio" ];

@Component({
  selector: "app-tabla-trazabilidad",
  templateUrl: "./tabla.trazabilidad.component.html",
  styleUrls: ["./tabla.trazabilidad.component.css"]
})
export class TablaTrazabilidadArchivosComponent implements OnInit, OnDestroy {
  /*Bar Spinner*/
  loadingProcess: boolean = false;
  barspinner = {
    color: "primary",
    mode: "indeterminate",
    bufferValue: 75
  };

  selectedRow = null;
  displayedColumns: string[] = [
    "id",
    "idProceso",
    "nombreSocio",
    "nombreEvento",
    "nombreArchivo",
    "estadoTransaccion",
    "fechaInicio",
    "fechaFin",
    "lineasOriginal",
    "lineasIcsOk",
    "lineasCoreOk",
    "lineasIcsError",
    "lineasCoreError"
  ];

  procesos = [];
  procesoFinalizado: boolean;
  procesosLoading: boolean = false;
  finalizarProcesoEnabled: boolean;

  detallesDisplayedColumns: string[] = [ "estado", "nombre", "fechas" ];

  selectRow(row) {
    if (this.selectedRow != row && row.tipoTransaccion != 'ANU') {
      console.debug("Id Transaccion Seleccionado: " + row.id);
      this.procesos = [];
      this.procesoFinalizado = false;
      this.finalizarProcesoEnabled = false;
      this.procesosLoading = true;
      this.selectedRow = row;
      this.localSvc.detalles(row.id).subscribe(
        resp => {
          for (let r of resp) {
            this.procesos.push({
              nombre: r.detalleEtapa,
              fechaInicio: r.fechaInicio,
              fechaFin: r.fechaFin
            });
          }
          for (let r of resp) {
            if ( r.detalleEtapa === "Terminado core" ) {
              this.procesoFinalizado = true; 
            } else if ( r.detalleEtapa === "Pendiente core" && !!!r.fechaFin) {
              this.finalizarProcesoEnabled = true;
            }
          }
          this.procesosLoading = false;
        },
        err => {
            console.error(err);
            
          this.dialog.open(InformationDialogComponent, {
            width: "475px",
            data: {
              title: "Error",
              content: err.statusText
            }
          });
        }
      );
    }
  }

  @ViewChild(MatPaginator) private paginator: MatPaginator;
  @ViewChild(MatSort) private sort: MatSort;

  private reloadTableSub: Subscription;
  private archivosCountSub: Subscription;
  private archivosSub: Subscription;

  public dataSource: TrazabilidadDataSource;

  constructor(
    private localSvc: TrazabilidadArchivosHttpService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar
  ) {
    this.dataSource = new TrazabilidadDataSource(this.localSvc);
  }

  ngOnInit() {
    this.paginator._intl = new MatPaginatorES();

    //cuando el orden aplicado a los datos cambie, vuelve a la pagina 1
    this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

    //cuando el orden o el numero de pagina cambie, recarga los datos
    this.reloadTableSub = merge(
      this.sort.sortChange,
      this.paginator.page
    ).subscribe(() => this.recargar());

    this.archivosCountSub = this.dataSource.archivosCount$.subscribe(
      count => (this.paginator.length = count)
    );

    this.archivosSub = this.dataSource.archivos$.subscribe(
      () => {
        this.selectedRow = null;
      }
    );
  }

  /**
   * Envía la página y el tamaño de ésta al DataSource para solicitar nuevamente las homologaciones.
   */
  private recargar(): void {
    this.dataSource.getArchivosPage(
      this.paginator.pageIndex,
      this.paginator.pageSize,
      this.sort.active,
      this.sort.direction
    );
  }

  ngOnDestroy(): void {
    if (this.archivosCountSub) this.archivosCountSub.unsubscribe();
    if (this.reloadTableSub) this.reloadTableSub.unsubscribe();
    if (this.archivosSub) this.archivosSub.unsubscribe();
  }

  public estadoLogDeshabilitado(estado: string) { return ESTADOS_CON_LOG_DESHABILITADO.includes(estado); }

  protected descargarArchivo(resp: any): void {
    var blob = new Blob([resp.body], { type: resp.headers.get("Content-Type") });
    let filename: string = resp.headers.get("File-Name");
    saveAs(blob, filename);
  }

  downloadOriginal(id: number) {
    this.loadingProcess = true;
    this.localSvc.downloadOriginal(id).subscribe(
      this.descargarArchivo,
      err => {
        console.error(err);
        this.loadingProcess = false;
        this.dialog.open(InformationDialogComponent, {
          width: "475px",
          data: {
            title: "Error",
            content: err.headers.get("Exception-Message")
          }
        });
      },
      () => {
        this.loadingProcess = false;
      }
    );
  }

  downloadResponse(id: number) {
    this.loadingProcess = true;
    this.localSvc.downloadResponse(id).subscribe(
      this.descargarArchivo,
      err => {
        console.error(err);
        this.loadingProcess = false;
        try {
          this.dialog.open(InformationDialogComponent, {
            width: "475px",
            data: {
              title: "Error",
              content: err.headers.get("Exception-Message")
            }
          });
        } catch (error) {
          this.dialog.open(InformationDialogComponent, {
            width: "475px",
            data: {
              title: "Error",
              content: err.statusText
            }
          });
        }
      },
      () => {
        this.loadingProcess = false;
      }
    );
  }

  finalizarProceso(id: number) {
    this.loadingProcess = true;
    this.localSvc.finalizarProceso(id).subscribe(
      resp => {
        this.snackBar.open("Proceso Finalizado");
        this.procesos = [];
        this.procesoFinalizado = false;
        this.procesosLoading = true;
        this.finalizarProcesoEnabled = false;
        this.localSvc.detalles(id).subscribe(
          resp => {
            for (let r of resp) {
              this.procesos.push({
                nombre: r.detalleEtapa,
                fechaInicio: r.fechaInicio,
                fechaFin: r.fechaFin
              });
              if (
                r.detalleEtapa.includes("Terminado core") &&
                r.fechaFin != null
              ) {
                this.procesoFinalizado = true;
              }
            }
            this.loadingProcess = false;
            this.procesosLoading = false;
          },
          err => {
              console.error(err);
              
            this.loadingProcess = false;
            this.dialog.open(InformationDialogComponent, {
              width: "475px",
              data: {
                title: "Error",
                content: err.statusText
              }
            });
          }
        );
      },
      err => {
        console.error(err);
        this.loadingProcess = false;
        try {
          this.dialog.open(InformationDialogComponent, {
            width: "475px",
            data: {
              title: "Error",
              content: err.error.message
            }
          });
        } catch (error) {
          this.dialog.open(InformationDialogComponent, {
            width: "475px",
            data: {
              title: "Error",
              content: err.statusText
            }
          });
        }
      },
      () => {
        this.loadingProcess = false;
      }
    );
  }

  downloadLog(id: number) {
    this.loadingProcess = true;
    this.localSvc.downloadLog(id).subscribe(
      this.descargarArchivo,
      err => {
        console.error(err);
        this.loadingProcess = false;
        try {
          this.dialog.open(InformationDialogComponent, {
            width: "475px",
            data: {
              title: "Error",
              content: err.headers.get("Exception-Message")
            }
          });
        } catch (error) {
          this.dialog.open(InformationDialogComponent, {
            width: "475px",
            data: {
              title: "Error",
              content: err.statusText
            }
          });
        }
      },
      () => {
        this.loadingProcess = false;
      }
    );
  }
}
