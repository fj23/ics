import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcesosCargaComponent } from './procesos-carga.component';

describe('ProcesosCargaComponent', () => {
  let component: ProcesosCargaComponent;
  let fixture: ComponentFixture<ProcesosCargaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcesosCargaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcesosCargaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
