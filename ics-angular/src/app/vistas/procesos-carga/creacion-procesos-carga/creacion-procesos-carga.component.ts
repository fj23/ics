import { Component, OnInit, OnDestroy } from '@angular/core';
import { Socio } from 'src/models/compartido/Socio';
import { Observable, Subject, Subscription, BehaviorSubject, merge, of } from 'rxjs';
import { FormGroup, FormBuilder, Validators, ValidatorFn, AbstractControl } from '@angular/forms';
import { Evento } from 'src/models/compartido/Evento';
import { CommonDataHttpService } from 'src/http-services/common-data.http-service';
import { MatSnackBar } from '@angular/material';
import { ProcesosCargaModel } from 'src/models/procesosCarga/procesosCargaModel';
import { PlantillasHttpService } from 'src/http-services/plantillas.http-service';
import { FiltrosPlantillasModel } from 'src/models/filters/FiltrosPlantillasModel';
import { Plantilla } from 'src/models/plantillas/Plantilla';
import { PaginaRegistros } from 'src/models/compartido/PaginaRegistros';
import { Router } from '@angular/router';
import { ProcesosCargaHttpService } from 'src/http-services/procesos-carga.http-service';
import { CallCenter } from 'src/models/compartido/CallCenter';
import { ProcesoCargaOutputModel } from 'src/output_models/ProcesoCargaOutputModel';
import { TipoArchivo } from '../../../../models/compartido/TipoArchivo';
import { DirectorioSalida } from '../../../../models/compartido/DirectorioSalida';


export function mailFormat(): ValidatorFn {

    const EMAIL_REGEXP: RegExp = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;

    return (control: AbstractControl): {[key: string]: any} | null => {

        if (control.value) {
            const tooShort = control.value.length <= 5;
            const failsRegex = !EMAIL_REGEXP.test(control.value);
            const forbidden = tooShort || failsRegex;
            return forbidden ? {'mailFormat': {value: control.value}} : null;
        }
        return null;
    };
}  

export const EVENTO_ALTAS_ID = 200005;

@Component({
    selector: 'app-creacion-procesos-carga',
    templateUrl: './creacion-procesos-carga.component.html',
    styleUrls: ['./creacion-procesos-carga.component.css']
})
export class CreacionProcesosCargaComponent implements OnInit, OnDestroy {

    protected _esProcesoNuevo$: Subject<boolean>;
    protected _cargandoPlantillas$: Subject<boolean>;

    protected _procesoCarga: ProcesosCargaModel;
    protected set ProcesoCarga(proceso: ProcesosCargaModel) {
        this._procesoCarga = proceso;
        this.alCambiarProcesoCarga();
        this._esProcesoNuevo$.next(this.esProcesoNuevo);
    }
    protected get esProcesoNuevo(): boolean { 
        return (!this._procesoCarga || isNaN(this._procesoCarga.id)); 
    }

    public esProcesoNuevo$: Observable<boolean>;

    public procesosCargaFormGroup: FormGroup;

    public socios$: Observable<Socio[]>;
    public eventos$: Observable<Evento[]>;
    public callCenter$: Observable<CallCenter[]>;
    public tiposArchivo$: Observable<TipoArchivo[]>;
    public directoriosError$: Observable<DirectorioSalida[]>;
    public plantillaEntrada$: Subject<Plantilla[]>;
    public plantillaIntegracion$: Subject<Plantilla[]>;
    public plantillaSalidaSocio$: Subject<Plantilla[]>;
    public cargandoPlantillas$: Observable<boolean>;

    protected alElegirFiltrosPlantillasSub: Subscription;
    protected alAlternarSalidaAnetoSub: Subscription;
    protected alSeleccionarProcesoSub: Subscription;
    protected alSeleccionarEventoSub: Subscription;
    protected alCambiarDependenciasTipoArchivo: Subscription;

    public onEditChange: boolean = false;

    constructor(
        protected commonSvc: CommonDataHttpService,
        protected localSvc: ProcesosCargaHttpService,
        protected templateSvc: PlantillasHttpService,
        protected fb: FormBuilder,
        protected snackBar: MatSnackBar,
        protected router: Router,
    ) {
        this._esProcesoNuevo$ = new Subject();
        this._cargandoPlantillas$ = new BehaviorSubject(false);

        this.esProcesoNuevo$ = this._esProcesoNuevo$.asObservable();
        this.cargandoPlantillas$ = this._cargandoPlantillas$.asObservable();

        this.procesosCargaFormGroup = this.fb.group({
            nombreProceso: [null, Validators.required],
            esSalidaAneto: [false],
            evento: [null, Validators.required],
            socio: [null, Validators.required],
            callCenter: [{ value: null, disabled: true }],
            tipoArchivo: [{ value: null, disabled: true }],
            directorioError: [null],
            plantillaEntrada: [null, Validators.required],
            plantillaIntegracion: [null, Validators.required],
            plantillaSalidaSocio: [null],
            errorCarga: [null, Validators.compose([Validators.required, mailFormat()])],
            errorIntegracion: [null, Validators.compose([Validators.required, mailFormat()])],
            hitosProceso: [null, Validators.compose([Validators.required, mailFormat()])],
        });
    }

    public get nombreProceso() { return this.procesosCargaFormGroup.get('nombreProceso'); }
    public get esSalidaAneto() { return this.procesosCargaFormGroup.get('esSalidaAneto'); }
    public get evento() { return this.procesosCargaFormGroup.get('evento'); }
    public get socio() { return this.procesosCargaFormGroup.get('socio'); }
    public get callCenter() { return this.procesosCargaFormGroup.get('callCenter'); }
    public get tipoArchivo() { return this.procesosCargaFormGroup.get('tipoArchivo'); }
    public get directorioError() { return this.procesosCargaFormGroup.get('directorioError'); }
    
    public get plantillaEntrada() { return this.procesosCargaFormGroup.get('plantillaEntrada'); }
    public get plantillaIntegracion() { return this.procesosCargaFormGroup.get('plantillaIntegracion'); }
    public get plantillaSalidaSocio() { return this.procesosCargaFormGroup.get('plantillaSalidaSocio'); }
    public get errorCarga() { return this.procesosCargaFormGroup.get('errorCarga'); }
    public get errorIntegracion() { return this.procesosCargaFormGroup.get('errorIntegracion'); }
    public get hitosProceso() { return this.procesosCargaFormGroup.get('hitosProceso'); }

    public get debeMostrarCheckboxTipoArchivo(): boolean { return this.evento.value && this.evento.value === EVENTO_ALTAS_ID && this.callCenter.value; }

    ngOnInit(): void {

        this.socios$ = this.commonSvc.getSocios();
        this.eventos$ = this.commonSvc.getEventos();
        this.callCenter$ = of([]);
        this.tiposArchivo$ = this.localSvc.getTiposArchivo();
        this.directoriosError$ = this.localSvc.getDirectoriosError();
        this.plantillaEntrada$ = new Subject<Plantilla[]>();
        this.plantillaIntegracion$ = new Subject<Plantilla[]>();
        this.plantillaSalidaSocio$ = new Subject<Plantilla[]>();

        this.alCambiarDependenciasTipoArchivo = merge(
            this.callCenter.valueChanges, 
            this.evento.valueChanges
        ).subscribe(() => {
            this.tipoArchivo.setValue(undefined);
            if (this.debeMostrarCheckboxTipoArchivo) {
                this.tipoArchivo.enable();
                this.tipoArchivo.setValidators([Validators.required]);
            } else {
                this.tipoArchivo.disable();
                this.tipoArchivo.setValidators(null);
            }
        });
        
        this.alElegirFiltrosPlantillasSub = merge(
            this.evento.valueChanges,
            this.socio.valueChanges
        ).subscribe(() => {
            this.recargarListaPlantillasSegunFiltros();
        });

        this.alAlternarSalidaAnetoSub = this.esSalidaAneto.valueChanges.subscribe(() => {
            this.alAlternarSalidaAneto();
        });

        this.alSeleccionarProcesoSub = this.localSvc.editingTargetSubject.subscribe(
            proc => { this.ProcesoCarga = proc; }
        );
    }

    ngOnDestroy(): void {
        if (this.alSeleccionarProcesoSub) {
            this.alSeleccionarProcesoSub.unsubscribe();
        }
        
        if (this.alElegirFiltrosPlantillasSub) {
            this.alElegirFiltrosPlantillasSub.unsubscribe();
        }
    }

    protected resetForm(): void {
        const noChange = { onlySelf: true, emitEvent: false };
        this.procesosCargaFormGroup.reset(noChange);
        if (this._procesoCarga) {
            this.nombreProceso.disable(noChange);
            this.evento.disable(noChange);
            this.socio.disable(noChange);
            this.callCenter.disable(noChange);
            this.tipoArchivo.disable(noChange);
        } else {
            this.nombreProceso.enable(noChange);
            this.evento.enable(noChange);
            this.socio.enable(noChange);
        }
    }

    protected actualizarFiltrosPlantillasDesdeFormulario() {
        const filtros = new FiltrosPlantillasModel();
        filtros.socio = this.socio.value;
        filtros.evento = this.evento.value;
        filtros.vigencia = 'Y';
        filtros.pageSize = 9999;
        this.templateSvc.setFiltros(filtros);
    }

    protected actualizarFiltrosPlantillasDesdeProcesoCarga() {
        if (this.esProcesoNuevo) {
            this.templateSvc.setFiltros(null);
        } else {
            const filtrosPlantilla = new FiltrosPlantillasModel();
            filtrosPlantilla.vigencia = 'Y';
            filtrosPlantilla.socio = this._procesoCarga.plantillaEntradaSocio.socio.id;
            filtrosPlantilla.evento = this._procesoCarga.plantillaEntradaSocio.evento.id;
            this.templateSvc.setFiltros(filtrosPlantilla);
        }
    }

    protected restablecerSeleccionPlantillas(): void {
        const noChange = {emitEvent: false, onlySelf: true};
        if (this.esProcesoNuevo) {
            this.plantillaEntrada.setValue(null, noChange);
            this.plantillaIntegracion.setValue(null, noChange);
            this.plantillaSalidaSocio.setValue(null, noChange);
        } else {
            const ptEntrada = this._procesoCarga.plantillaEntradaSocio;
            const ptIntg = this._procesoCarga.plantillaIntegracion;
            const ptSalida = this._procesoCarga.plantillaSalidaSocio;
            
            if (!this.esSalidaAneto.value) {
                if (ptEntrada && ptEntrada.id) {
                    this.plantillaEntrada.setValue(ptEntrada, noChange);
                }
                if (ptIntg && ptIntg.id) {
                    this.plantillaIntegracion.setValue(ptIntg, noChange);
                }
            }
            if (ptSalida && ptSalida.id) {
                this.plantillaSalidaSocio.setValue(ptSalida, noChange);
            }

            if (this._procesoCarga.tipoArchivo) {
                this.tipoArchivo.setValue(this._procesoCarga.tipoArchivo);
            }
        }
    }

    protected cargarListaPlantillas(): void {
        const idSocio = this.socio.value;
        const idEvento = this.evento.value;

        this.localSvc.getCallCenters(idSocio, idEvento).subscribe(
          (callCenters) => {
            this.callCenter$ = of(callCenters);
            if (callCenters.length > 0) {
                this.callCenter.enable();
            } else {
                this.callCenter.setValue(null);
                this.callCenter.disable();
            }

            if (this._procesoCarga && this._procesoCarga.callCenter) {
                this.callCenter.setValue(this._procesoCarga.callCenter);
            }
          }
        );

        if (this.esSalidaAneto.value) {
            this.localSvc.listarPlantillasSalidaEstructurada(idSocio, idEvento).subscribe(
                (templateList: PaginaRegistros<Plantilla>) => {
                    const arraySalida: Plantilla[] = [];
                    for (const item of templateList.items) {
                        arraySalida.push(item);
                    }

                    this.plantillaSalidaSocio$.next(arraySalida);

                    this.restablecerSeleccionPlantillas();
                }
            );
        } else {
            this.localSvc.listarPlantillas(idSocio, idEvento).subscribe(
                (templateList: PaginaRegistros<Plantilla>) => {
                    const arrayEntrada: Plantilla[] = [];
                    const arrayIntegracion: Plantilla[] = [];
                    const arraySalida: Plantilla[] = [];
                    for (const item of templateList.items) {
                        switch (item.tipo.id) {
                            case 100000:
                                arrayEntrada.push(item); 
                                break;
                            case 100001:
                                arrayIntegracion.push(item);
                                break;
                            case 100002:
                                arraySalida.push(item);
                                break;
                            default: break;
                        }
                    }

                    this.plantillaEntrada$.next(arrayEntrada);
                    this.plantillaIntegracion$.next(arrayIntegracion);
                    this.plantillaSalidaSocio$.next(arraySalida);

                    this.restablecerSeleccionPlantillas();
                }
            );
        }
    }

    public recargarListaPlantillasSegunFiltros(): void {
        const noChange = { onlySelf: true, emitEvent: false };
        if (this.socio.value && this.evento.value) {
            this.actualizarFiltrosPlantillasDesdeFormulario();
            this.plantillaEntrada.setValue(null, noChange);
            this.plantillaIntegracion.setValue(null, noChange);
            this.plantillaSalidaSocio.setValue(null, noChange);
            this.cargarListaPlantillas();
        }
    }

    protected alCambiarProcesoCarga() {
        const noChange = { emitEvent: false, onlySelf: true };
        if (this.esProcesoNuevo) {
            this.resetForm();
            this.nombreProceso.setValue("", noChange);
            this.evento.setValue(null, noChange);
            this.socio.setValue(null, noChange);
            this.callCenter.setValue(null, noChange);
            this.directorioError.setValue(null, noChange);
            this.errorCarga.setValue("", noChange);
            this.errorIntegracion.setValue("", noChange);
            this.hitosProceso.setValue("", noChange);
            this.esSalidaAneto.setValue(false, noChange);
        } else {
            this.nombreProceso.setValue(this._procesoCarga.nombre, noChange);
            this.errorCarga.setValue(this._procesoCarga.correoSalida, noChange);
            this.errorIntegracion.setValue(this._procesoCarga.correoEmail, noChange);
            this.hitosProceso.setValue(this._procesoCarga.correoHitos, noChange);
            this.directorioError.setValue(this._procesoCarga.directorioSalidaError, noChange);

            const esSalidaAneto = this._procesoCarga.esSalidaAneto;
            if (esSalidaAneto) {
                this.evento.setValue(this._procesoCarga.plantillaSalidaSocio.evento.id, noChange);
                this.socio.setValue(this._procesoCarga.plantillaSalidaSocio.socio.id, noChange);
            } else {
                this.evento.setValue(this._procesoCarga.plantillaEntradaSocio.evento.id, noChange);
                this.socio.setValue(this._procesoCarga.plantillaEntradaSocio.socio.id, noChange);
            }
            this.esSalidaAneto.setValue(esSalidaAneto);
            
        }
        this.actualizarFiltrosPlantillasDesdeProcesoCarga();
        this.recargarListaPlantillasSegunFiltros();
    }
    
    protected alAlternarSalidaAneto() {
        const noChange = { emitEvent: false, onlySelf: true };
        const esAneto = this.esSalidaAneto.value;
        if (esAneto) {
            this.plantillaEntrada.disable(noChange);
            this.plantillaIntegracion.disable(noChange);
            this.plantillaSalidaSocio.setValidators([Validators.required]);
            if (this._procesoCarga && this._procesoCarga.esSalidaAneto) {
                this._procesoCarga.plantillaEntradaSocio = null;
                this._procesoCarga.plantillaIntegracion = null;
            }
        } else {
            this.plantillaEntrada.enable(noChange);
            this.plantillaIntegracion.enable(noChange);
            this.plantillaSalidaSocio.setValidators(null);
            if (this._procesoCarga && this._procesoCarga.esSalidaAneto) {
                this._procesoCarga.plantillaSalidaSocio = null;
            }
        }
        this.plantillaEntrada.setValue(null, noChange);
        this.plantillaIntegracion.setValue(null, noChange);
        this.plantillaSalidaSocio.setValue(null, noChange);
        this.recargarListaPlantillasSegunFiltros();
    }

    protected convertirProcesoCargaAOutput(): ProcesoCargaOutputModel {
      const proceso: ProcesoCargaOutputModel = new ProcesoCargaOutputModel();
      proceso.id = this._procesoCarga && this._procesoCarga.id ? this._procesoCarga.id : 0;
      proceso.nombre = this.nombreProceso.value;
      proceso.esSalidaAneto = this.esSalidaAneto.value;
      proceso.callCenter = this.callCenter.value;
      proceso.plantillaEntradaSocio = this.plantillaEntrada.value;
      proceso.plantillaIntegracion = this.plantillaIntegracion.value;
      proceso.plantillaSalidaSocio = this.plantillaSalidaSocio.value;
      proceso.correoSalida = (<string>this.errorCarga.value).toLowerCase();
      proceso.correoEmail = (<string>this.errorIntegracion.value).toLowerCase();
      proceso.correoHitos = (<string>this.hitosProceso.value).toLowerCase();
      proceso.callCenter = this.callCenter.value;
      proceso.directorioSalidaError = this.directorioError.value;
      if (this.debeMostrarCheckboxTipoArchivo) {
        proceso.tipoArchivo = this.tipoArchivo.value;
      }
      return proceso;
    }

    protected crearProcesoCarga(): ProcesoCargaOutputModel {
        const proceso: ProcesoCargaOutputModel = this.convertirProcesoCargaAOutput();
        if (this.esProcesoNuevo) {
            proceso.vigencia = 'Y';
            proceso.estado = 0;
            proceso.detalleProceso = [];
        }
        return proceso;
    }

    /**
     * Envía los datos del formulario como un objeto.
     */
    public submit(): void {
        this.procesosCargaFormGroup.updateValueAndValidity();

        if (this.procesosCargaFormGroup.invalid) {
            this.snackBar.open("Debe completar todos los datos requeridos del formulario.");
        } else {
            
            const procesoCargaOutput: ProcesoCargaOutputModel = this.crearProcesoCarga();
            const procesoCarga: ProcesosCargaModel = {
              id: procesoCargaOutput.id,
              esSalidaAneto: procesoCargaOutput.esSalidaAneto,
              callCenter: { id: procesoCargaOutput.callCenter, nombre: undefined,  vigencia: undefined },
              plantillaEntradaSocio: procesoCargaOutput.plantillaEntradaSocio,
              plantillaIntegracion: procesoCargaOutput.plantillaIntegracion,
              plantillaSalidaSocio: procesoCargaOutput.plantillaSalidaSocio,
              correoSalida: procesoCargaOutput.correoSalida,
              correoEmail: procesoCargaOutput.correoEmail,
              correoHitos: procesoCargaOutput.correoHitos,
              detalleProceso: undefined,
              estado: procesoCargaOutput.estado,
              nombre: procesoCargaOutput.nombre,
              vigencia: procesoCargaOutput.vigencia,
              tipoArchivo: procesoCargaOutput.tipoArchivo? { id: procesoCargaOutput.tipoArchivo, nombre: undefined, vigencia: undefined } : undefined,
              directorioSalidaError: procesoCargaOutput.directorioSalidaError? procesoCargaOutput.directorioSalidaError : undefined
            };

            if (procesoCargaOutput.id) {
                this.localSvc.actualizar(procesoCargaOutput).subscribe(
                    () => {
                        this.localSvc.newProcesoSubject.next();
                        this.localSvc.forzarActualizacionFiltros();
                        this.snackBar.open("Proceso actualizado con éxito.");
                    },
                    (err) => {
                        console.log(err);
                        this.snackBar.open(
                            "Hubo un problema al actualizar el proceso: "+err.error,
                            "OK",
                            { duration: -1 }
                        );
                    }
                );
            } else {
                this.localSvc.crear(procesoCargaOutput).subscribe(
                    () => {
                        this.localSvc.editingTargetSubject.next(procesoCarga);
                        this.localSvc.newProcesoSubject.next();
                        this.procesosCargaFormGroup.reset();
                        this.localSvc.forzarActualizacionFiltros();
                        this.snackBar.open('Proceso de carga registrado correctamente.');
                    },
                    (err) => {
                        console.log(err);
                        this.snackBar.open(
                            "Hubo un problema al crear el proceso: "+err.error,
                            "OK",
                            { duration: -1 }
                        );
                    }
                );
            }
        }
    }


}
