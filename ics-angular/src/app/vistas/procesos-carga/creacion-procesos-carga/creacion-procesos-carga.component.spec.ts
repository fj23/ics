import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreacionProcesosCargaComponent } from './creacion-procesos-carga.component';

describe('CreacionProcesosCargaComponent', () => {
  let component: CreacionProcesosCargaComponent;
  let fixture: ComponentFixture<CreacionProcesosCargaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreacionProcesosCargaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreacionProcesosCargaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
