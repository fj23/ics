import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ProcesosCargaModel } from 'src/models/procesosCarga/ProcesosCargaModel';
import { Subscription, Observable } from 'rxjs';
import {CreacionProcesosCargaComponent} from './creacion-procesos-carga/creacion-procesos-carga.component';
import { Title } from '@angular/platform-browser';
import { ProcesosCargaHttpService } from 'src/http-services/procesos-carga.http-service';
import { FormCanDeactivate } from 'src/services/routing/form-can-deactivate';
import { Permisos } from 'src/app/compartido/Permisos';
import { ConfirmationDialogComponent } from 'src/app/dialogs/confirmation/confirmation.dialog.component';

@Component({
  selector: 'app-procesos-carga',
  templateUrl: './procesos-carga.component.html',
  styleUrls: ['./procesos-carga.component.css'],
  providers: [ ProcesosCargaHttpService ]
})
export class ProcesosCargaComponent extends FormCanDeactivate  implements OnInit, OnDestroy{
  
  /**
   * Acción elegida en el mantenedor. Según esta, se muestran u ocultan ciertos sub-componentes de este módulo.
   */
  public selectedAction: string = "filtrar";

  private newProcesoSub: Subscription;
  public ocultarPorPermisos : boolean;

  @ViewChild(CreacionProcesosCargaComponent) formChild: CreacionProcesosCargaComponent;

  get canDeactivateForm():boolean {
    if (this.formChild != null) {
      if (this.formChild.procesosCargaFormGroup.dirty || this.formChild.procesosCargaFormGroup.touched) {
        return false;
      }
    }
    return true;
  }


  constructor(
    private localSvc: ProcesosCargaHttpService,
    private title: Title,
    public dialog: MatDialog
  ) {
    super();
    this.title.setTitle("ICS | Procesos de Carga");
    this.ocultarPorPermisos = Permisos.DenegarAcceso(Permisos.MantenerProcesoCarga());
  }

  ngOnInit() {
      this.newProcesoSub = this.localSvc.newProcesoSubject.subscribe(() => { this.activarVistaFiltros(); }
    );
  }

    
    protected activarVistaFiltros() {
        this.selectedAction = "filtrar";
    }

  ngOnDestroy(): void {
    if (this.newProcesoSub) this.newProcesoSub.unsubscribe();
  }

  /**
   * Cuando se desea cambiar el sub-componente mostrado sobre la tabla del mantenedor.
   * @param action La acción que se llama.
   */
  onClickAccion(action: string): void {
    if (this.selectedAction === "editar" ) {//} && this.localSvc.editingTargetSubject.getValue()) {

      this.dialogoConfirmacionEditarProceso().subscribe(
        (confirmed: boolean) => {
          if (confirmed) {
            //si el usuario confirmó querer dejar de editar y elige crear (el botón para crear llama a 'editar')
            //significa que quiere crear una nueva homologa ción; sólo cambia la homologación activa por una vacía.
            if (action === "editar") {
              this.localSvc.editingTargetSubject.next(null);
            }
            else {
              //si la misma accion ya estaba seleccionada, se deselecciona
              this.selectedAction = (this.selectedAction !== action)? action : "";
              this.localSvc.editingTargetSubject.next(null);
            }
          }
        }
      );
    } else {
      
      //si la misma accion ya estaba seleccionada, se minimiza componente
      this.selectedAction = (this.selectedAction !== action)? action : "";
      if (action === "editar") {
        this.localSvc.editingTargetSubject.next(null);
      }
    }
  }

  onClickEditProcesosCarga(proceso:ProcesosCargaModel) {
    //si ya se estaba editando -otro- proceso, se pregunta al usuario si realmente quiere dejar de lado la anterior antes
    if (this.selectedAction === "editar") {
      this.dialogoConfirmacionEditarProceso().subscribe(
        (confirmed: boolean) => {
          if (confirmed) {
            this.localSvc.editingTargetSubject.next(proceso);
          }
        }
      );
    }
    else {
      this.selectedAction = "editar";
      this.localSvc.editingTargetSubject.next(proceso);
    }
  }

  /**
   * Genera un diálogo que pide confirmación para soltar el proceso que esté siendo creado o editado.
   */
  private dialogoConfirmacionEditarProceso(): Observable<any> {
    let mensaje: string;
    if (this.localSvc.editingTargetSubject.getValue()) {
      mensaje = "Un proceso está siendo editado. Los cambios se perderán. ¿Está seguro/a que desea salir?";
    }
    else{
      mensaje = "Un proceso está siendo creado y aún no ha sido guardado. ¿Está seguro/a que desea salir?";
    
    }
    
    const confirmationDialog = this.dialog.open(
      ConfirmationDialogComponent, 
      { data: {
          titulo: "Descartar Acción Actual", 
          mensaje: mensaje,
          boton_si_clases: "btn-warning",
          boton_no_clases: ""
      } }
    );

    return confirmationDialog.beforeClosed();
  }

}
