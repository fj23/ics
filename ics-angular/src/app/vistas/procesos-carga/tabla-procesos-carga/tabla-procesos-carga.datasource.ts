import { DataSource } from "@angular/cdk/table";
import { OnDestroy } from '@angular/core';
import { CollectionViewer } from "@angular/cdk/collections";
import { Observable, BehaviorSubject, of, Subscription, Subject } from 'rxjs';
import { catchError, finalize, tap } from 'rxjs/operators';

import { PaginaRegistros } from 'src/models/compartido/PaginaRegistros';
import { ProcesosCargaModel } from "src/models/procesosCarga/procesosCargaModel";
import { FiltrosProcesosCargaModel } from "src/models/filters/FiltrosProcesosCargaModel";
import { ProcesosCargaHttpService } from 'src/http-services/procesos-carga.http-service';

export class ProcesosCargaDataSource implements OnDestroy, DataSource<ProcesosCargaModel> {

	private filtrosChangeSub: Subscription;

	/** Recibe y transmite el array de homologaciones activo a la vista. */
	public procesosCargaSubject = new BehaviorSubject<ProcesosCargaModel[]>([]);

	/** Recibe y transmite la cantidad de homologaciones conseguidas por los filtros activos.
	 * Esta cantidad suele superar el número de registros en la página actual. */
	private procesosCargaCountSubject = new BehaviorSubject<number>(0);

	private cargaSubject = new BehaviorSubject<boolean>(false);

	//observables
	public procesosCargaCount$ = this.procesosCargaCountSubject.asObservable();
	public carga$ = this.cargaSubject.asObservable();

	constructor(
		private localSvc: ProcesosCargaHttpService
	) {
		//cuando los filtros, el paginado y/o el orden de la tabla cambien, obtiene las homologaciones
		this.filtrosChangeSub = this.localSvc.filtrosChange$.subscribe(
			() => this.getProcesosCarga()
		);
	}

	ngOnDestroy(): void {
		this.filtrosChangeSub.unsubscribe();
	}

	connect(collectionViewer: CollectionViewer): Observable<ProcesosCargaModel[] | ReadonlyArray<ProcesosCargaModel>> {
		return this.procesosCargaSubject.asObservable();
	}

	disconnect(collectionViewer: CollectionViewer): void {
		this.procesosCargaSubject.complete();
		this.cargaSubject.complete();
	}

	/**
	 * Solicita la página de homologaciones respectiva.
	 * @param pageIndex El índice (con base 0) de la página.
	 * @param pageSize La cantidad de registros a mostrar por página.
	 */
	public getProcesosCargaPage(pageIndex: number, pageSize: number, sortColumn: string, sortOrder: string): void {
		let filtros: FiltrosProcesosCargaModel = this.localSvc.getFiltros();
		filtros.pageIndex = pageIndex;
		filtros.pageSize = pageSize;
		filtros.sortColumn = sortColumn;
		filtros.sortOrder = sortOrder;

		this.localSvc.setFiltros(filtros);
	}

	/**
	 * Recarga directamente las homologaciones haciendo una llamada al servicio de homologaciones.
	 * Éste debiera ser el último método llamado en la cadena de ejecución para este DataSource.
	 */
	private getProcesosCarga(): void {

		//si ya está cargando homologaciones, evita sobrecargar las peticiones
		if (!this.cargaSubject.getValue()) {

			this.cargaSubject.next(true);

			this.localSvc.listar().pipe(
				catchError(() => of([])),
				finalize(() => this.cargaSubject.next(false)) //termine bien o mal la peticion, la carga se detiene
			).subscribe(
				(payload: PaginaRegistros<ProcesosCargaModel>) => {
					this.procesosCargaSubject.next(payload.items); //el listado de homologaciones se almacena
					if (this.procesosCargaCountSubject.getValue() != payload.count) {
						this.procesosCargaCountSubject.next(payload.count);
					}
				}
			);
		}
	}

}
