import { Component, OnInit, OnDestroy, Output, EventEmitter, ViewChild } from '@angular/core';
import { Subscription, merge } from 'rxjs';
import { ProcesosCargaModel } from 'src/models/procesosCarga/procesosCargaModel';
import { MatPaginator, MatSort, MatDialog, MatSnackBar } from '@angular/material';
import { MatPaginatorES } from 'src/app/compartido/mat.paginator.es';
import { ConfirmationDialogComponent } from 'src/app/dialogs/confirmation/confirmation.dialog.component';
import { catchError } from 'rxjs/operators';
import { ProcesosCargaDataSource } from './tabla-procesos-carga.datasource';
import { Angular5Csv } from 'angular5-csv/dist/Angular5-csv';
import { forEach } from '@angular/router/src/utils/collection';
import { Permisos } from 'src/app/compartido/Permisos';
import { FontawesomeIconsService } from 'src/services/fontawesome-icons/fontawesome-icons.service';
import { ProcesosCargaHttpService } from '../../../../http-services/procesos-carga.http-service';

@Component({
    selector: 'app-tabla-procesos-carga',
    templateUrl: './tabla-procesos-carga.component.html',
    styleUrls: ['./tabla-procesos-carga.component.css']
})
export class TablaProcesosCargaComponent implements OnInit, OnDestroy {
    selectedAction: string = "";

    reloadTableSub: Subscription;
    procesosCargaCountSub: Subscription;
    public ocultarPorPermisos: boolean;

    @Output() public editProcesosCarga: EventEmitter<ProcesosCargaModel> = new EventEmitter<ProcesosCargaModel>();

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    displayedColumns: string[] = ['idProceso', 'nombre', 'plantillaEntradaSocio', 'plantillaIntegracion', 'plantillaSalidaSocio', 'estado', 'acciones'];
    dataSource: ProcesosCargaDataSource;

    constructor(
        private localSvc: ProcesosCargaHttpService,
        public dialog: MatDialog,
        private snackBar: MatSnackBar,
        public icons: FontawesomeIconsService
    ) {
        this.dataSource = new ProcesosCargaDataSource(this.localSvc);
        this.ocultarPorPermisos = Permisos.DenegarAcceso(Permisos.MantenerPlantilla());
    }

    ngOnInit(): void {
        this.paginator._intl = new MatPaginatorES();

        //cuando el orden aplicado a los datos cambie, vuelve a la pagina 1
        this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

        //cuando el orden o el numero de pagina cambie, recarga los datos
        this.reloadTableSub = merge(
            this.sort.sortChange,
            this.paginator.page
        ).subscribe(
            () => this.recargar()
        );

        this.procesosCargaCountSub = this.dataSource.procesosCargaCount$.subscribe(
            count => this.paginator.length = count
        );
    }

    ngOnDestroy(): void {
        this.procesosCargaCountSub.unsubscribe();
        this.reloadTableSub.unsubscribe();
    }

    /**
     * Envía la página y el tamaño de ésta al DataSource para solicitar nuevamente las homologaciones.
     */
    recargar(): void {
        this.dataSource.getProcesosCargaPage(
            this.paginator.pageIndex,
            this.paginator.pageSize,
            this.sort.active,
            this.sort.direction
        );
    }

    /** 
     * Intenta atar la homologación seleccionada al formulario de creación/edición 
    */
    editarProcesoCarga(procesoCarga: ProcesosCargaModel): void {
        this.editProcesosCarga.emit(procesoCarga);
    }

    /** 
     * Solicita al usuario confirmar la acción. Si confirma, establece la homologación como activa. 
    */
    activarProcesoCarga(procesoCarga: ProcesosCargaModel): void {
        const confirmationDialog = this.dialog.open(ConfirmationDialogComponent, {
            data: {
                titulo: "Activar Proceso de Carga",
                mensaje: "Al hacer clic en \"Sí\", este proceso podrá ser ejecutado nuevamente. ¿Desea confirmar esta acción?",
                boton_si_clases: "btn-success",
                boton_no_clases: ""
            }
        });

        confirmationDialog.afterClosed().subscribe(
            (confirmed: boolean) => {
                if (confirmed) {
                    this.localSvc.activar(procesoCarga)
                        .subscribe(
                            () => {
                                this.snackBar.open("Proceso activado.");
                                procesoCarga.vigencia = "Y";
                            },
                            (err) => {
                                console.log(err);
                                this.snackBar.open(
                                    "Hubo un problema al activar el proceso: "+err.error,
                                    "OK",
                                    { duration: -1 }
                                );
                            }
                        );
                }
            }
        );
    }

    /** 
     * Solicita al usuario confirmar la acción. Si confirma, establece la homologación como inactiva.
    */
    desactivarProcesoCarga(procesoCarga: ProcesosCargaModel): void {
        const confirmationDialog = this.dialog.open(ConfirmationDialogComponent, {
            data: {
                titulo: "Desactivar Proceso",
                mensaje: "Al hacer clic en \"Sí\", este proceso de carga dejará de ser ejecutado hasta que sea activado nuevamente. ¿Desea confirmar esta acción?",
                boton_si_clases: "btn-danger",
                boton_no_clases: ""
            }
        });

        confirmationDialog.afterClosed().subscribe(
            (confirmed: boolean) => {
                if (confirmed) {
                    this.localSvc.desactivar(procesoCarga)
                        .pipe(
                            catchError((err, observable) => { return observable; })
                        ).subscribe(
                            () => {
                                this.snackBar.open("Proceso desactivado.");
                                procesoCarga.vigencia = "N";
                            },
                            (err) => {
                                console.log(err);
                                this.snackBar.open(
                                    "Hubo un problema al desactivar el proceso: "+err.error,
                                    "OK",
                                    { duration: -1 }
                                );
                            }
                        );
                }
            }
        );
    }

    public onClickExportar(): void {
        let options = {
            fieldSeparator: ';',
            quoteStrings: '"',
            decimalseparator: ',',
            showLabels: true,
            showTitle: false,
            useBom: true,
            noDownload: false,
            title: '',
            headers: ["ID Proceso", "Nombre Proceso", "Plantilla Entrada", "Plantilla Integracion", "Plantilla Salida", "Estado", "(Mail) Error de carga", "(Mail) Error de integracion", "(Mail) Hitos del proceso"],
            nullToEmptyString: true
        };
        
        let auxData = this.dataSource.procesosCargaSubject.getValue();
        let data = [];
        auxData.forEach(proceso => {
            let estado = proceso.vigencia === 'Y' ? 'Vigente' : 'No vigente';
            data.push({
                idProceso: proceso.id,
                nombreProceso: proceso.nombre,
                plantillaEntradaSocio: proceso.plantillaEntradaSocio.nombre,
                plantillaIntegracion: proceso.plantillaIntegracion.nombre,
                plantillaSalidaSocio: proceso.plantillaSalidaSocio ? proceso.plantillaSalidaSocio.nombre : "",
                estado: estado,
                correoSalida: proceso.correoSalida,
                correoEmail: proceso.correoEmail,
                correoHitos: proceso.correoHitos
            })
        });

        let csv: Angular5Csv = new Angular5Csv(data, 'Procesos de carga', options);
    }

    public canExport(): boolean {
        return (this.paginator && this.paginator.length > 0);
    }

}
