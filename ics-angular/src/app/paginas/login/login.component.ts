import { Component } from "@angular/core";

import { Credenciales } from "src/models/compartido/Credenciales";
import { MatSnackBar, MatDialog } from "@angular/material";
import { Router } from "@angular/router";
import { Title } from "@angular/platform-browser";
import { map } from "rxjs/operators";
import { HttpErrorResponse } from "@angular/common/http";
import { AuthHttpService } from '../../../http-services/auth.http-service';
import { FontawesomeIconsService } from 'src/services/fontawesome-icons/fontawesome-icons.service';
import { InformationDialogComponent } from 'src/app/dialogs/information/information.dialog.component';

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent {
  public loading = false;

  public errorMessage: string = "";

  public loginModel: Credenciales = new Credenciales();
  public tried = false;

  constructor(
    private authSvc: AuthHttpService,
    private snackBar: MatSnackBar,
    public dialog: MatDialog,
    private title: Title,
    private router: Router,
    public icons: FontawesomeIconsService
  ) {
    this.title.setTitle("Integracion Con Socios");

    if (localStorage.getItem("token")) {
      this.router.navigateByUrl("/");
    }

    this.loginModel.username="";
    this.loginModel.password="";
  }

  submit() {

    if (this.loginModel.username.trim()=="") {
      this.snackBar.open("Ingrese ID de Usuario");
      return;
    }

    if (this.loginModel.password.trim()=="") {
      this.snackBar.open("Ingrese Contraseña");
      return;
    }

    this.errorMessage = "";
    this.loading = true;
    this.tried = true;
    this.authSvc
      .validarLogin(this.loginModel)
      .pipe(
          map(this.authSvc.extractToken)
      ).subscribe(
        user => {
          this.loading = false;
          this.tried = false;
          //notifica el éxito del intento de inicio de sesión.
          this.snackBar.open("Sesión iniciada correctamente");
          this.router.navigateByUrl("/");
        },
        (err: HttpErrorResponse) => {
            console.error(err);
          this.dialog.open(InformationDialogComponent, {
            width: "475px",
            data: {
              title: "Acceso Denegado",
              content:
                "El servicio de autenticación ha rechazado sus credenciales"
            }
          });
          this.loading = false;
          this.tried = false;
        }
      );
  }

  limpiar() {
    this.loginModel.username = "";
    this.loginModel.password = "";
    this.errorMessage = "";
  }
}
