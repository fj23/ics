import { Component, Output, Input, EventEmitter, OnDestroy, HostListener } from "@angular/core";
import { MatDialog } from "@angular/material";
import { AuthHttpService } from "src/http-services/auth.http-service";
import { FontawesomeIconsService } from "src/services/fontawesome-icons/fontawesome-icons.service";
import { SessionNoticeDialog } from "../../../dialogs/session-notice/session-notice-dialog";
import { finalize } from 'rxjs/operators';

@Component({
    selector: "app-cabecera",
    templateUrl: "./cabecera.component.html",
    styleUrls: ["./cabecera.component.css"]
})
export class CabeceraComponent implements OnDestroy {
    
    protected maxInactivityChecks = 10;
    
    protected inactivityChecks = 0;
    protected sessionValidateInterval = 60000;
    protected sessionValidateTimer: any;
    protected isDialogOpen = false;
    protected wasClickPressed = false;

    public username: string;

    @Input() public sidenavOpen: boolean;
    @Output() public sidenavChange = new EventEmitter<boolean>();

    constructor(
        private authSvc: AuthHttpService, 
        public dialog: MatDialog, 
        public icons: FontawesomeIconsService
    ) {
        this.username = localStorage.getItem("username");

        this.authSvc.obtenerConfigValidacion().pipe(
            finalize(() => {
                this.activarTimerValidacion();
            })
        ).subscribe(
            intervalo => { 
                this.sessionValidateInterval = intervalo[0];
                this.maxInactivityChecks = intervalo[1];
            }
        );
    }

    @HostListener("document:mousedown", ["$event"])
    ngMousedown() {
        if (!this.wasClickPressed) {
            this.wasClickPressed = true;
        }
    }

    protected activarTimerValidacion(): void {
        this.sessionValidateTimer = setInterval(() => {
            if (this.wasClickPressed) {
                this.authSvc.validarToken().subscribe(
                    valido => {
                        if (!valido) {
                            this.authSvc.logout("Token invalido", true);
                        }
                    }
                );
                this.wasClickPressed = false;
                this.inactivityChecks = 0;
            } else if (!this.isDialogOpen) {
                this.inactivityChecks++;
                if (this.inactivityChecks >= this.maxInactivityChecks) {
                    this.abrirDialogoSesionInactiva();
                }
            }
        }, this.sessionValidateInterval);
    }

    protected abrirDialogoSesionInactiva(): void {
        this.isDialogOpen = true;
        this.dialog.open(SessionNoticeDialog, {
            width: "500px"
        }).afterClosed().subscribe(x => {
            this.isDialogOpen = false;
            this.wasClickPressed = true;
            this.inactivityChecks = 0;
        });
    }

    ngOnDestroy(): void {
        if (this.sessionValidateTimer) {
            clearInterval(this.sessionValidateTimer);
        }
    }

    public triggerLogout(): void {
        this.authSvc.logout("Sesión cerrada correctamente", true);
        clearInterval(this.sessionValidateTimer);
    }

    public toggleSidenav(): void {
        this.sidenavOpen = !this.sidenavOpen;
        this.sidenavChange.emit(this.sidenavOpen);
        localStorage.setItem("sidenavOpen", this.sidenavOpen.toString());
    }
}