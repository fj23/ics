import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
    selector: 'app-file-upload-dialog',
    template: `
  <form [formGroup]="form" (ngSubmit)="submit()">
    <h2 mat-dialog-title>{{data.titulo}}</h2>
    <mat-dialog-content>
      <div class="form-group">
        <input type="file" (change)="setFile($event)" formControlName="archivo" accept=".csv,text/csv" />
      </div>
      <div class="alert alert-warning">
        <p>Sólo se aceptan archivos en formato CSV, separados por punto y comas.<br/>
        Cada fila del archivo que suba representa la metadata de una columna del archivo de entrada que configura.</p>
        <h6>El archivo debe contener:</h6>
        <ul>
            <li>Nombre de columna</li>
            <li>Id del tipo de dato</li>
            <li>Posición inicial</li>
            <li>Posición final</li>
        </ul>
      </div>
    </mat-dialog-content>

    <mat-dialog-actions class="d-flex justify-content-around">
      <button style="width: 8em;" class="btn border-secondary {{data.boton_si_clases}}" [disabled]="archivo.invalid" mat-button type="submit">Enviar</button>
      <button style="width: 8em;" class="btn border-secondary {{data.boton_no_clases}}" mat-button [mat-dialog-close]="file" autofocus>Cancelar</button>
    </mat-dialog-actions>
  </form> 
  `
})
export class FileUploadDialogComponent {
    public form: FormGroup;
    file: File = null;

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: any,
        public formBuilder: FormBuilder,
        public dialogRef: MatDialogRef<FileUploadDialogComponent>
    ) { 
        this.form = this.formBuilder.group({
            archivo: [null, Validators.required]
        })
    }
    
    public get archivo() { return this.form.get("archivo"); }

    public setFile($event: any): void {
        if ($event.target.files.length !== 0) {
            this.file = $event.target.files.item(0);
        }
    }

    public submit(): void {
        if (this.file) {
            this.dialogRef.close(this.file);
        }
        else {
            return;
        }
    }
}
