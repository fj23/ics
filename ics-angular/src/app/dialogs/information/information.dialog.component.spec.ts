import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Information.DialogComponent } from './information.dialog.component';

describe('Information.DialogComponent', () => {
  let component: Information.DialogComponent;
  let fixture: ComponentFixture<Information.DialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Information.DialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Information.DialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
