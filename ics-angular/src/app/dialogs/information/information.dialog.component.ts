import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";

export interface InformationDialogData {
  title: string;
  content: string;
}

@Component({
  selector: "app-information.dialog",
  templateUrl: "./information.dialog.component.html",
  styleUrls: ["./information.dialog.component.css"]
})
export class InformationDialogComponent {
  constructor(
    public dialogRef: MatDialogRef<InformationDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: InformationDialogData
  ) {}

  onCloseClick(): void {
    this.dialogRef.close();
  }
}
