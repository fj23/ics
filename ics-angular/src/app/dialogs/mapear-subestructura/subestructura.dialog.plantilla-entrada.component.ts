import { Component, OnInit, ViewChild, OnDestroy, Input, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef, MatCheckboxChange, MatSnackBar } from '@angular/material';
import { Subscription, Observable } from 'rxjs';
import { ColumnaDestinoEntrada } from 'src/models/plantillas/entrada/ColumnaDestinoEntrada';
import { Evento } from 'src/models/compartido/Evento';
import { ColumnaMetadatos } from 'src/models/compartido/ColumnaMetadatos';
import { PlantillasHttpService } from 'src/http-services/plantillas.http-service';
import { FormularioPlantillasService } from 'src/services/formulario-plantillas/formulario-plantillas.service';

export interface MapearSubEstructuraPlantillaEntradaDialogData {
    columnaOrigen: ColumnaMetadatos;
    evento: Evento;
}

@Component({
  selector: 'app-subestructura-formulario-plantilla-entrada-dialog',
  templateUrl: './subestructura.dialog.plantilla-entrada.component.html',
  styleUrls: ['./subestructura.dialog.plantilla-entrada.component.css']
})
export class MapearSubEstructuraPlantillaEntradaDialogComponent implements OnInit, OnDestroy {

  public columnasDestinoDisponibles$: Observable<ColumnaDestinoEntrada[]>;
  public indiceColumnaSeleccionada: number;

  public columnaDestinoSeleccionada: ColumnaDestinoEntrada;

  public subEstructuraColumnasDisplayedColumns: string[] = ["codigo", "nombre", "tipoDato", "acciones"];

  public filtroForm: FormGroup;
  private campoFiltrarRegistrosSub: Subscription;

  private evento: Evento;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: MapearSubEstructuraPlantillaEntradaDialogData,
    private localSvc: PlantillasHttpService,
    private formSvc: FormularioPlantillasService,
    private selfDialog: MatDialogRef<MapearSubEstructuraPlantillaEntradaDialogComponent>,
    private snackBar: MatSnackBar,
    private fb: FormBuilder
  ) {
    this.filtroForm = this.fb.group({
      filtrarRegistros: [false],
      valorFiltrado: [{value: '', disabled: true}]
    });

    this.evento = data.evento;
  }

  get filtrarRegistros() { return this.filtroForm.get("filtrarRegistros"); }
  get valorFiltrado() { return this.filtroForm.get("valorFiltrado"); }


  ngOnInit(): void {
    this.columnasDestinoDisponibles$ = this.localSvc.getColumnasDestinoEntradaDisponiblesUsuarios(this.evento.id);

    this.campoFiltrarRegistrosSub = this.filtrarRegistros.valueChanges.subscribe(
      () => {
        if (this.filtrarRegistros.value) {
          this.valorFiltrado.enable({ onlySelf: true, emitEvent: false });
          this.valorFiltrado.setValidators(Validators.required);
        }
        else {
          this.valorFiltrado.disable({ onlySelf: true, emitEvent: false });
          this.valorFiltrado.setValue("");
          this.valorFiltrado.setValidators(null);
        }

      }
    );
  }

  ngOnDestroy(): void {
    this.campoFiltrarRegistrosSub.unsubscribe();
  }

  public onClickSelectColumna(event: MatCheckboxChange, index: number): void {
    this.indiceColumnaSeleccionada = (event.checked) ? index : null;
  }

  public onClickMapear(): void {
    if (!this.columnaDestinoSeleccionada) {
      this.snackBar.open("Debe seleccionar una columna de destino para mapear.");
    }
    else if (!this.indiceColumnaSeleccionada && this.indiceColumnaSeleccionada !== 0) {
      this.snackBar.open("Debe seleccionar una columna de la subestructura para mapear.");
    }
    else {
        const valorParaFiltrar = this.filtrarRegistros.value? this.valorFiltrado.value : null;
      this.selfDialog.close({
        columnaDestino: JSON.parse(JSON.stringify(this.columnaDestinoSeleccionada)),
        columnaSubEstructuraIndex: this.indiceColumnaSeleccionada,
        valorFiltrado: valorParaFiltrar
      });
    }

  }

}
