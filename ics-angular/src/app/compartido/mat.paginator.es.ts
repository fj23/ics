import { MatPaginatorIntl } from '@angular/material';

export class MatPaginatorES extends MatPaginatorIntl {
  firstPageLabel = "Ir a primera página";
  lastPageLabel = "Ir a última página";
  nextPageLabel = "Página siguiente";
  previousPageLabel = "Pagina anterior";
  itemsPerPageLabel = "Resultados por página:";
  getRangeLabel = (page, pageSize, length) => (length>0? ('Viendo: '+((page*pageSize)+1) +'-'+( ((page+1)*pageSize) < length? (((page+1)*pageSize)) : length )+' de '+length) : 'Sin datos');
}
