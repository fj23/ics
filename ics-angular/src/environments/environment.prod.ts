export const environment = {
  production: true,
  serviceURL: "/ics_frontend/backend/api/",
  fileServiceURL: "/ics_frontend/zuul/backend/api/"
};
