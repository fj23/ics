import { Injectable } from '@angular/core';

import { faKey, faEraser, faSignOutAlt, faBars, faCaretUp, faCaretDown, faTimes, faCheck, faBan, faEllipsisH, faEdit, faFileImport, faPlus, faPlusCircle, faPlusSquare, faChevronDown, faChevronRight, faCogs, faCopy } from '@fortawesome/free-solid-svg-icons';

@Injectable({providedIn: 'root'})
export class FontawesomeIconsService {

  public faKey = faKey;
  public faEraser = faEraser;
  public faSignOutAlt = faSignOutAlt;
  public faBars = faBars;
  public faCaretUp = faCaretUp;
  public faCaretDown = faCaretDown;
  public faTimes = faTimes;
  public faCheck = faCheck;
  public faBan = faBan;
  public faEllipsisH = faEllipsisH;
  public faEdit = faEdit;
  public faFileImport = faFileImport;
  public faPlus = faPlus;
  public faPlusCircle = faPlusCircle;
  public faPlusSquare = faPlusSquare;
  public faChevronDown = faChevronDown;
  public faChevronRight = faChevronRight;
  public faCogs = faCogs;
  public faCopy = faCopy;

}
