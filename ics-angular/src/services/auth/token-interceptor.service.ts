import { Injectable } from "@angular/core";
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse,
  HttpResponse
} from "@angular/common/http";
import { Observable, throwError, empty } from "rxjs";
import { catchError, map } from "rxjs/operators";
import { Router } from "@angular/router";
import { MatSnackBar } from "@angular/material";
import { AuthHttpService } from "src/http-services/auth.http-service";
import { Permisos } from "src/app/compartido/Permisos";

// Extending the Http class so connect a OAuth token if present in the cookies
// When the request is recieved on the server authenticated endpoints will
// have varification that give them the ability to execute
@Injectable({providedIn: 'root'})
export class TokenInterceptor implements HttpInterceptor {
  constructor(
    public snackbar: MatSnackBar,
    public router: Router,
    public authSvc: AuthHttpService
  ) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (request.headers.has("X-Skip-Interceptor")) {
      const headers = request.headers.delete("X-Skip-Interceptor");
      return next.handle(request.clone({ headers }));
    }
    // get the token from a service
    const token = localStorage.getItem("token");

    if (token) {
      request = request.clone({
        setHeaders: { Authorization: `Bearer ${token}` }
      });
    }
    
    return next.handle(request).pipe(
      map((event: HttpEvent<any>) => {
          
        if (event instanceof HttpResponse &&
            this.router.url !== "/" &&
            this.router.url !== "/login" &&
            Permisos.DenegarAccesoByUrl(this.router.url)
        ) {
            this.authSvc.logout("Ha habido un error en el servidor: No tiene autorización", true);
        }
        return event;
      }),
      catchError((error: HttpErrorResponse) => {
        if (this.router.url !== "/" && this.router.url != "/login") {
          if (error.status == 401) {
            try {
              this.authSvc.logout(error.error.split(": ")[1], true);
            } catch (e) {
              this.authSvc.logout("Error al validar su sesión", true);
            }
            return empty();
          }
          if (error.status == 403) {
            this.authSvc.logout("No tiene autorización", true);
            return empty();
          }
        }
        return throwError(error);
      })
    );
  }
}
