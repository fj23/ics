import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { ComponentCanDeactivate } from "./component-can-deactivate";
import { ConfirmationDialogComponent } from "../../app/dialogs/confirmation/confirmation.dialog.component";
import { MatDialog } from '@angular/material';
import { Observable, of } from 'rxjs';

@Injectable({providedIn: 'root'})
export class CanDeactivateGuard implements CanDeactivate<ComponentCanDeactivate> {

    constructor(
        private dialog: MatDialog
    ) {}

    canDeactivate(component: ComponentCanDeactivate): Observable<boolean> {
        if (sessionStorage.getItem("force_exit")) {
            sessionStorage.clear();
            return of(true);
        }
        if (!component.canDeactivate()){
            return this.dialog.open(ConfirmationDialogComponent, {
                data: {
                    titulo: "¿Realmente desea salir?",
                    mensaje: "Cualquier cambio que haya realizado en la página actual no será guardado.",
                    boton_si_clases: "btn-warning",
                    boton_no_clases: ""
                }
            }).afterClosed();
        } else {
            return of(true);
        }
    }
}