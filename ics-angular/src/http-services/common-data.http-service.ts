import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs'

import { BaseHttpService } from './base.http-service';

import { Evento } from 'src/models/compartido/Evento';
import { Socio } from 'src/models/compartido/Socio';
import { SistemaCore } from 'src/models/compartido/SistemaCore';
import { TipoExtension } from 'src/models/compartido/TipoExtension';
import { TipoPlantilla } from 'src/models/compartido/TipoPlantilla';
import { TipoFormula } from 'src/models/compartido/TipoFormula';
import { ColumnaDestinoEntrada } from 'src/models/plantillas/entrada/ColumnaDestinoEntrada';
import { SubEstructuraAneto } from 'src/models/estructuras/SubEstructuraAneto';
import { TipoDato } from 'src/models/compartido/TipoDato';
import { EstructuraAneto } from 'src/models/estructuras/EstructuraAneto';
import { TipoSalidaIntegracion } from 'src/models/compartido/TipoSalidaIntegracion';
import { Plantilla } from 'src/models/plantillas/Plantilla';
import { Parametro } from 'src/models/compartido/Parametro';
import { TipoFormato } from 'src/models/compartido/TipoFormato';
import { ProcesosCargaModel } from '../models/procesosCarga/ProcesosCargaModel';

/** 
 * Servicio que obtiene y provee datos de uso común, como socios y eventos.
*/
@Injectable({providedIn: 'root'})
export class CommonDataHttpService extends BaseHttpService {

  private urls = {
    getSocios: 'socios',
    getEventos: 'eventos',
    getSistemasCore: 'cores',
    getTiposDato: 'tipos_datos',
    getProcesos: 'procesos',

    getTiposPlantilla: 'tipos_plantilla',
    getEstadosPlantilla: 'estados_plantilla',
    getTiposExtension: 'tipos_extension',

    getEstructuras: 'estructuras/simple',
    getSubEstructuras: 'subestructuras/simple',
    getColumnasDestinoEntrada: 'columnas_destino_entrada',

    getTiposFormato: 'tipos_formato',
    getTiposFormula: 'tipos_formula',

    getTipoSalida: 'tipos_salida',

    getPlantillas: 'plantillas/all',

    getParametrosTrazabilidad: 'trazabilidad/archivos'
    
  };

  constructor(http: HttpClient) { 
    super(http);
  }

  public getSocios(): Observable<Socio[]> {
    return this.http.get<Socio[]>( 
      this.getServiceURL()+this.urls.getSocios
    );
  }

  public getEventos(): Observable<Evento[]> {
    return this.http.get<Evento[]>(
      this.getServiceURL()+this.urls.getEventos
    );
  }

  public getSistemasCore(): Observable<SistemaCore[]> {
    return this.http.get<SistemaCore[]>(
      this.getServiceURL()+this.urls.getSistemasCore
    );
  }

  public getTiposDato(): Observable<TipoDato[]> {
    return this.http.get<TipoDato[]>(
      this.getServiceURL() + this.urls.getTiposDato
    );
  }

  public getTiposPlantilla(): Observable<TipoPlantilla[]> {
    return this.http.get<TipoPlantilla[]>(
      this.getServiceURL() + this.urls.getTiposPlantilla
    );
  }

  public getTiposExtension(): Observable<TipoExtension[]> {
    return this.http.get<TipoExtension[]>(
      this.getServiceURL() + this.urls.getTiposExtension
    );
  }

  public getEstructuras(): Observable<EstructuraAneto[]> {
    return this.http.get<EstructuraAneto[]>(
      this.getServiceURL() + this.urls.getEstructuras
    );
  }

  public getSubEstructuras(): Observable<SubEstructuraAneto[]> {
    return this.http.get<SubEstructuraAneto[]>(
      this.getServiceURL() + this.urls.getSubEstructuras
    );
  }

  public getColumnasDestinoEntrada(): Observable<ColumnaDestinoEntrada[]> {
    return this.http.get<ColumnaDestinoEntrada[]>(
      this.getServiceURL() + this.urls.getColumnasDestinoEntrada
    );
  }

  public getTiposFormato(): Observable<TipoFormato[]> {
    return this.http.get<TipoFormato[]>(
      this.getServiceURL() + this.urls.getTiposFormato
    );
  }

  public getTiposFormula(): Observable<TipoFormula[]> {
    return this.http.get<TipoFormula[]>(
      this.getServiceURL() + this.urls.getTiposFormula
    );
  }

  public getTiposSalidaIntegracion(): Observable<TipoSalidaIntegracion[]> {
    return this.http.get<TipoSalidaIntegracion[]>(
      this.getServiceURL() + this.urls.getTipoSalida
    );
  }

  public getPlantillas(): Observable<Plantilla[]> {
    return this.http.get<Plantilla[]>(
      this.getServiceURL() + this.urls.getPlantillas
    )
  }

  public getParametrosTrazabilidad(): Observable<Parametro[]> {
    return this.http.get<Parametro[]>( 
      this.getServiceURL()+this.urls.getParametrosTrazabilidad+"/parametros"
    );
  }

    /**
   * Obtiene Informacion para Select de Nombre Proceso
   */
  public getProcesos(socio?, evento?): Observable<ProcesosCargaModel[]> {
    let parameters = new HttpParams();

    if (socio) {
      parameters = parameters.set("idSocio", socio.toString());
    }

    if (evento) {
      parameters = parameters.set("idEvento", evento.toString());
    }

    return this.http.get<ProcesosCargaModel[]>(
      this.getServiceURL() + this.urls.getProcesos, { params: parameters }
    );
  }

/*
  public getCurrentDateDDMMYYYY(): string {
    let today = new Date();
    let dd: string;
    let mm: string;

    if (today.getDate() < 10) {
      dd = '0' + today.getDate().toString();
    }
    else {
      dd = today.getDate().toString();
    }

    if (today.getMonth() + 1  < 10) {
      mm = '0' + (today.getMonth() + 1).toString();
    }

    return dd + "/" + mm + "/" + today.getFullYear().toString();
  }
*/
}
