import { Injectable } from "@angular/core";
import { HttpClient, HttpResponse } from "@angular/common/http";

import { Subject, Observable } from 'rxjs';


import { BaseHttpService } from "./base.http-service";
import { MatSnackBar } from "@angular/material";
import { Router } from "@angular/router";
import { Credenciales } from '../models/compartido/Credenciales';

interface Role {
  name: string;
  authority: string;
}

interface TokenUser {
    roles: Role[];
    username: string;
    versionBackend: string;
    inactivityNotice: number;
    email?: string;
    expires?: number;
}

interface TokenExtraction extends HttpResponse<any> {
  accessToken: string;
  tokenType: string;
  user: TokenUser;
}

/**
 * Servicio que obtiene datos de sesión y realiza solicitudes de inicio de sesión
 */
@Injectable({ providedIn: "root" })
export class AuthHttpService extends BaseHttpService {
  private urls = {
    auth: "auth",
    salir: "logout",
    validar: "validar",
    configValidacion: "validation"
  };

  public logStateChange = new Subject<boolean>();

  constructor(
    http: HttpClient,
    private snackBar: MatSnackBar,
    private router: Router
  ) {
    super(http);
  }

  public extractToken(res: TokenExtraction) {
    let roles: string = "";
    for (const role of res.user.roles) {
      if (role.name.indexOf("EVENT_") === -1) {
        roles += "F" + role.name + "R;";
      }
    }    
    localStorage.setItem("roles", roles);
    localStorage.setItem("token", res.accessToken);
    localStorage.setItem("username", res.user.username);
    localStorage.setItem("versionBackend", res.user.versionBackend);
    return res.user;
  }

  /**
   * Intenta iniciar sesión con las credenciales
   * @param loginModel
   */
  validarLogin(loginModel: Credenciales): Observable<TokenExtraction> {
    return this.http.post<TokenExtraction>(this.getServiceURL() + this.urls.auth, loginModel);
  }

  /**
   * Valida el token de sesion para mantenerlo activo
   * @param token
   */
  validarToken(): Observable<boolean> {
    return this.http.get<boolean>(this.getServiceURL() + this.urls.validar);
  }

  /**
   * Elimina los datos de sesión del usuario.
   */
  public logout(msg: string, force: boolean): void {
    let token = localStorage.getItem("token");
    if (token != null) {
      this.http.get(this.getServiceURL() + this.urls.salir + `/${token}`).subscribe();
    }
    localStorage.clear();
    sessionStorage.clear();
    if (force) sessionStorage.setItem("force_exit", "T");
    this.snackBar.open(msg);
    this.logStateChange.next(false);
    this.router.navigateByUrl("/login");
  }

  public obtenerConfigValidacion(): Observable<number[]> {
      return this.http.get<number[]>(this.getServiceURL() + this.urls.configValidacion);
  }
}
