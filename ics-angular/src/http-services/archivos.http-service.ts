import { BaseHttpService } from "./base.http-service";
import { Injectable } from "@angular/core";
import {
  HttpClient,
  HttpParams,
  HttpHeaders
} from "@angular/common/http";

import { Observable } from "rxjs";
import { PaginaRegistros } from "src/models/compartido/PaginaRegistros";
import { HistorialImportacionesModel } from "src/models/cargaArchivo/HistorialImportacionesModel";
import { FiltrosBaseModel } from "src/models/filters/FiltrosBaseModel";
import { CargaArchivoOutputModel } from 'src/output_models/CargaArchivoOutputModel';

@Injectable({ providedIn: "root" })
export class ArchivosHttpService extends BaseHttpService {
  // Cargar Archivos Services Inicio

  private urls = {
    setFile: "cargaArchivos/setFile",
    setCargaArchivo: "cargaArchivos/setCargaArchivo",
    verifyFileName: "cargaArchivos/verifyFileName"
  };

  constructor(http: HttpClient) {
    super(http);
  }

  // public crearCargaArchivo(cargaArchivo: CargaArchivoOutputModel): Observable<any> {
  //   return this.http.post(
  //     this.host + this.urls.crear, cargaArchivo
  //   );
  // }

  public setFileCargaArchivo(file: File): Observable<any> {
    let formdata: FormData = new FormData();
    formdata.append("file", file, file.name);

    return this.http.post(this.getFileServiceURL() + this.urls.setFile, formdata);
  }

  public setCargaArchivo(
    cargaArchivo: CargaArchivoOutputModel
  ): Observable<any> {
    return this.http.post(this.getServiceURL() + this.urls.setCargaArchivo, cargaArchivo);
  }

  public verifyFileName(
    cargaArchivo: CargaArchivoOutputModel
  ): Observable<any> {
    return this.http.post(this.getServiceURL() + this.urls.verifyFileName, cargaArchivo);
  }

  // Cargar Archivos Services Fin

  // Importar Archivo Services Inicio

  private base_url_importar_archivos = `importarArchivos`;

  /**
   * Obtiene Informacion para Select de Transacciones segun proceso
   */
  public getEventos(idProceso: number) {
    return this.http.get<any>(
      this.getServiceURL() + this.base_url_importar_archivos + `/eventos/${idProceso}`
    );
  }

  /**
   * Obtiene los archivos guardados en la base de datos para select
   */
  public getArchivos(filtros) {
    let params = this.modelToHttpParametersImportarArchivos(filtros);

    return this.http.get<any>(
      this.getServiceURL() + this.base_url_importar_archivos + "/archivos/filters",
      { params: params }
    );
  }

  httpFileOptions: { headers; observe } = {
    headers: new HttpHeaders({
      "Content-Type": "multipart/form-data; charset=utf-8; boundary=file",
      "Content-Disposition": "form-data; name=file;"
    }),
    observe: "response"
  };

  /**
   * Decargar archivo
   */
  public descargarArchivo(idTransaccion: number) {
    let headers = new HttpHeaders();
    headers = headers.set("Content-Type", "application/json");
    headers = headers.set("Accept", "text/plain");
    return this.http.get(
      this.getServiceURL() +
        this.base_url_importar_archivos +
        `/descargar/${idTransaccion}`,
      { headers: headers, responseType: "blob", observe: "response" }
    );
  }

  /**
   * Carga archivo importado
   */
  public cargarArchivoImportado(fileToUpload: File, idTransaccion: number) {
    let formData = new FormData();
    formData.append("file", fileToUpload, fileToUpload.name);
    return this.http.post(
      this.getFileServiceURL() + this.base_url_importar_archivos + `/cargar/${idTransaccion}`,
      formData,
      this.httpFileOptions
    );
  }

  /**
   * Transforma los filtros activos en un objeto de parámetros adjuntables a una petición HTTP.
   */
  private modelToHttpParametersImportarArchivos(filtros): HttpParams {
    let parameters = new HttpParams();

    if (filtros.socio != null) {
      parameters = parameters.set("idSocio", filtros.socio.toString());
    }

    if (filtros.proceso != null) {
      parameters = parameters.set("idProceso", filtros.proceso.toString());
    }

    if (filtros.transaccion != null) {
      parameters = parameters.set(
        "idTransaccion",
        filtros.transaccion.toString()
      );
    }

    if (filtros.evento != null) {
      parameters = parameters.set("idEvento", filtros.evento.toString());
    }

    if (filtros.fechaDesde != null) {
      parameters = parameters.set(
        "fechaInicio",
        filtros.fechaDesde.getTime().toString()
      );
    }

    return parameters;
  }

  /**
   * Solicita y obtiene los archivos guardados en la base de datos.
   */
  public listarHistorialImportaciones(idTransaccion, filtros?: FiltrosBaseModel): Observable<
    PaginaRegistros<HistorialImportacionesModel>
  > {
    let filtrosAux = new FiltrosBaseModel();
    if (filtros) {
      filtrosAux = filtros;
    }
    let paging = "/page/" + filtrosAux.pageIndex;
    let pageSizing = "/size/" + filtrosAux.pageSize;
    let sortOrder = "";
    if (filtrosAux.sortColumn && filtrosAux.sortOrder) {
      sortOrder =
        "/sort/" + filtros.sortColumn + "/" + filtros.sortOrder;
    }

    return this.http.get<PaginaRegistros<HistorialImportacionesModel>>(
      this.getServiceURL() + this.base_url_importar_archivos + `/historial/${idTransaccion}` + paging + pageSizing + sortOrder
    );
  }

  public exportar(idTransaccion): Observable<HistorialImportacionesModel[]> {

    return this.http.get<HistorialImportacionesModel[]>(
      this.getServiceURL() + this.base_url_importar_archivos + `/historial/${idTransaccion}/exportar`
    );
  }

  // Importar Archivo Services Fin
}
