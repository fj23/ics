import { Injectable } from "@angular/core";
import { HttpClient, HttpParams, HttpHeaders } from "@angular/common/http";
import { Subject, Observable } from "rxjs";

import { BaseHttpService } from "./base.http-service";
import { FiltrosTrazabilidadArchivosModel } from "src/models/filters/FiltrosTrazabilidadArchivosModel";
import { TrazabilidadArchivoModel } from "src/models/trazabilidad/TrazabilidadArchivoModel";
import { PaginaRegistros } from "src/models/compartido/PaginaRegistros";
import { DetalleTransaccion } from "src/models/trazabilidad/DetalleTransaccion";
import { map } from 'rxjs/operators';

@Injectable({providedIn: 'root'})
export class TrazabilidadArchivosHttpService extends BaseHttpService {
  private base_url = `trazabilidad/archivos`;

  private filtros: FiltrosTrazabilidadArchivosModel;

  /**
   * Transmite el evento de cambio de filtros.
   */
  private filtrosChangeSubject = new Subject<void>();

  private listarTrazabilidadSource = new Subject<any>();
  listarTrazabilidad$ = this.listarTrazabilidadSource.asObservable();

  constructor(http: HttpClient) {
    super(http);
  }

  /**
   * Solicita y obtiene los archivos guardados en la base de datos.
   */
  public listar(): Observable<
    PaginaRegistros<TrazabilidadArchivoModel>
  > {
    if (!!!this.filtros) {
      this.filtros = new FiltrosTrazabilidadArchivosModel();
    }
    let paging = "/page/" + this.filtros.pageIndex;
    let pageSizing = "/size/" + this.filtros.pageSize;
    let sortOrder = "";
    if (this.filtros.sortColumn && this.filtros.sortOrder) {
      sortOrder =
        "/sort/" + this.filtros.sortColumn + "/" + this.filtros.sortOrder;
    }
    let params = this.modelToHttpParameters();

    return this.http.get<PaginaRegistros<TrazabilidadArchivoModel>>(
      this.getServiceURL() + this.base_url + paging + pageSizing + sortOrder + "/filters",
      { params: params }
    );
  }

  public exportar(): Observable<TrazabilidadArchivoModel[]> {
    if (!!!this.filtros) {
      this.filtros = new FiltrosTrazabilidadArchivosModel();
    }

    let params = this.modelToHttpParameters();

    return this.http.get<TrazabilidadArchivoModel[]>(
      this.getServiceURL() + this.base_url + "/exportar",
      { params: params }
    );
  }

  /**
   * Solicita y obtiene los archivos guardados en la base de datos.
   */
  public detalles(idTransaccion: number): Observable<DetalleTransaccion[]> {
    return this.http.get<DetalleTransaccion[]>(
      this.getServiceURL() + this.base_url + `/detalle/${idTransaccion}`
    );
  }

  
  /**
   * Solicita archivo original de la transacción.
   */
  downloadOriginal(id: number): Observable<Object> {
    return this.http.get(
      this.getServiceURL() + this.base_url + `/descargar/original/${id}`,
      { responseType: "blob", observe: "response" }
    );
  }

  /**
   * Solicita archivo respuesta de la transacción.
   */
  downloadResponse(id: number): Observable<Object> {
    return this.http.get(
      this.getServiceURL() + this.base_url + `/descargar/respuesta_socio/${id}`,
      { responseType: "blob", observe: "response" }
    );
  }

  downloadLog(id: number): Observable<Object> {
    return this.http.get(
      this.getServiceURL() + this.base_url + `/descargar/log/${id}`,
      { responseType: "blob", observe: "response" }
    );
  }

  /**
   * Consolida la transaccion
   */
  public consolidar(): Observable<any> {
    return this.http.get(
      this.getServiceURL() + this.base_url + `/consultarConsolidacion`
    );
  }

  /**
   * Finalizar proceso de transaccion
   */
  public finalizarProceso(idTransaccion: number): Observable<any> {
    return this.http.get(
      this.getServiceURL() + this.base_url + `/finalizar_proceso/${idTransaccion}`
    );
  }

  /**
   * Transforma los filtros activos en un objeto de parámetros adjuntables a una petición HTTP.
   */
  private modelToHttpParameters(): HttpParams {
    let parameters = new HttpParams();

    if (this.filtros.socio > 0) {
      parameters = parameters.set("socio", this.filtros.socio.toString());
    }

    if (this.filtros.idCarga > 0) {
      parameters = parameters.set("idCarga", this.filtros.idCarga.toString());
    }

    if (this.filtros.idExterno) {
      parameters = parameters.set("idExterno", this.filtros.idExterno.toString());
    }

    if (this.filtros.evento > 0) {
      parameters = parameters.set("evento", this.filtros.evento.toString());
    }

    if (this.filtros.estado != null) {
      parameters = parameters.set("estado", this.filtros.estado);
    }

    if (this.filtros.recepcionadoDesde) {
      parameters = parameters.set(
        "recepcionadoDesde",
        this.filtros.recepcionadoDesde.getTime().toString()
      );
    }

    if (this.filtros.recepcionadoHasta) {
      parameters = parameters.set(
        "recepcionadoHasta",
        this.filtros.recepcionadoHasta.getTime().toString()
      );
    }

    if (this.filtros.cargadoDesde) {
      parameters = parameters.set(
        "cargadoDesde",
        this.filtros.cargadoDesde.getTime().toString()
      );
    }

    if (this.filtros.cargadoHasta) {
      parameters = parameters.set(
        "cargadoHasta",
        this.filtros.cargadoHasta.getTime().toString()
      );
    }

    return parameters;
  }

  public getFiltros(): FiltrosTrazabilidadArchivosModel {
    if (!!!this.filtros) {
      this.filtros = new FiltrosTrazabilidadArchivosModel();
    }
    return this.filtros;
  }

  public setFiltros(filtros: FiltrosTrazabilidadArchivosModel): void {
    this.filtros = filtros;
    this.filtrosChangeSubject.next();
  }

  //observables
  public filtrosChange$ = this.filtrosChangeSubject.asObservable();
}
