import { Injectable } from '@angular/core';
import { BaseHttpService } from './base.http-service';
import { FiltrosProcesosCargaModel } from 'src/models/filters/FiltrosProcesosCargaModel';
import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { ProcesosCargaModel } from 'src/models/procesosCarga/procesosCargaModel';
import { HttpClient, HttpParams } from '@angular/common/http';
import { PaginaRegistros } from 'src/models/compartido/PaginaRegistros';
import { Plantilla } from '../models/plantillas/Plantilla';
import { CallCenter } from 'src/models/compartido/CallCenter';
import { ProcesoCargaOutputModel } from 'src/output_models/ProcesoCargaOutputModel';
import { TipoArchivo } from 'src/models/compartido/TipoArchivo';
import { DirectorioSalida } from '../models/compartido/DirectorioSalida';

@Injectable({providedIn: 'root'})
export class ProcesosCargaHttpService  extends BaseHttpService {

  private urls = {
    listar: 'procesos',
    crear: 'procesos/nueva',
    actualizar: 'procesos/actualizar',
    activar: 'procesos/activar',
    desactivar: 'procesos/desactivar',
    conceptos: 'procesos/conceptos',
    crearMultiple: 'procesos/nuevo_grupo',
    tipos: 'procesos/tipos',
    vigentes: 'procesos/getVigentes',
    listarNombres: 'procesos/nombresAll',
    listarPlantillas: 'procesos/plantillas',
    listarPlantillasSalidaEstructurada: 'procesos/plantillas_salida_estructurada',
    listarCallCenters: 'procesos/call_centers',
    listarTiposArchivo: 'procesos/tipos_archivo',
    listarDirectoriosError: 'procesos/directorios_error'
  };

  private filtros: FiltrosProcesosCargaModel;

  /**
   * Transmite el evento de cambio de filtros.
   */
  private filtrosChangeSubject = new Subject<void>();
  
  /**
   * Transmite el evento de cambio de filtros.
   */
  public newProcesoSubject = new Subject<void>();

  /**
   * Recibe y transmite el proceso de carga actualmente en edición.
   */
  public editingTargetSubject = new BehaviorSubject<ProcesosCargaModel>(null);
  
  //observables
  public filtrosChange$ = this.filtrosChangeSubject.asObservable();

  constructor(http: HttpClient) {
    super(http);
  }

  public setFiltros(filtros: FiltrosProcesosCargaModel): void {
    this.filtros = filtros;
    this.filtrosChangeSubject.next();
  }

  public getFiltros(): FiltrosProcesosCargaModel {
    if (!!!this.filtros) {
      this.filtros = new FiltrosProcesosCargaModel();
    }
    return this.filtros;
  }

  public forzarActualizacionFiltros(): void {
    this.filtrosChangeSubject.next();
  }

  /**
   * Transforma los filtros activos en un objeto de parámetros adjuntables a una petición HTTP.
   */
  private modelToHttpParameters( ): HttpParams {
    let parameters = new HttpParams();
    if (this.filtros.socio > 0){
      parameters = parameters.set("socio", this.filtros.socio.toString());
    }

    if (this.filtros.evento > 0){
      parameters = parameters.set("evento", this.filtros.evento.toString());
    }

    if (this.filtros.nombreProceso){
      parameters = parameters.set("nombreProceso", this.filtros.nombreProceso.toString());
    }

    if (this.filtros.estado){
      parameters = parameters.set("estado", this.filtros.estado.toString());
    }
    return parameters;
  }
  
  /**
   * Solicita y obtiene los (distintos) conceptos homologados en la base de datos.
   */
  public getConceptosHomologacion(): Observable<String[]> {
    return this.http.get<String[]>(
      this.getServiceURL() + this.urls.conceptos
    );
  }

  /**
   * Solicita y obtiene las procesos guardadas en la base de datos.
   */
  public listar(): Observable<PaginaRegistros<ProcesosCargaModel>> {
    if (!!!this.filtros) {
      this.filtros = new FiltrosProcesosCargaModel();
    }
    let paging = "/page/"+this.filtros.pageIndex;
    let pageSizing = "/size/"+this.filtros.pageSize;
    let sortOrder = "";
    if (this.filtros.sortColumn && this.filtros.sortOrder) {
      sortOrder = "/sort/"+this.filtros.sortColumn+"/"+this.filtros.sortOrder;
    }
    let params = this.modelToHttpParameters();
  

    return this.http.get<PaginaRegistros<ProcesosCargaModel>>( 
      this.getServiceURL() + this.urls.listar + paging + pageSizing + sortOrder + "/filters", 
      { params: params } 
    );
  }

    public listarPlantillas(idSocio: number, idEvento: number): Observable<PaginaRegistros<Plantilla>> {
        const params = new HttpParams()
            .append("idSocio", idSocio.toString())
            .append("idEvento", idEvento.toString());
        
        return this.http.get<PaginaRegistros<Plantilla>>( 
            this.getServiceURL() + this.urls.listarPlantillas, { params: params }
        );
    }

    public listarPlantillasSalidaEstructurada(idSocio: number, idEvento: number): Observable<PaginaRegistros<Plantilla>> {
        const params = new HttpParams()
            .append("idSocio", idSocio.toString())
            .append("idEvento", idEvento.toString());
        
        return this.http.get<PaginaRegistros<Plantilla>>( 
            this.getServiceURL() + this.urls.listarPlantillasSalidaEstructurada, { params: params }
        );
    }

  /**
   * Hace una petición POST para insertar un proceso en la base de datos.
   * @param model El objeto que va a intentar ser insertado.
   */
  public crear(model: ProcesoCargaOutputModel): Observable<any> {
    return this.http.post(
      this.getServiceURL() + this.urls.crear, 
      model
    );
  }

  /**
   * Hace una petición PUT para actualizar un proceso en la base de datos.
   * @param model El objeto que va a intentar ser actualizado.
   */
  public actualizar(model: ProcesoCargaOutputModel): Observable<any> {
    return this.http.put( 
      this.getServiceURL() + this.urls.actualizar, 
      model 
    );
  }

  /**
   * Hace una petición POST para activar un proceso de la base de datos.
   * @param model El objeto que va a intentar ser actualizado.
   */
  public activar(proceso: ProcesosCargaModel): Observable<any> {
    return this.http.get( 
      this.getServiceURL() + this.urls.activar + "/" + proceso.id
    );
  }

  /**
   * Hace una petición POST para desactivar un proceso de la base de datos.
   * @param model El objeto que va a intentar ser actualizado.
   */
  public desactivar(proceso: ProcesosCargaModel): Observable<any> {
    return this.http.get( 
      this.getServiceURL() + this.urls.desactivar + "/" + proceso.id
    );
  }

  public crearMultiple(proceso: ProcesosCargaModel[]): Observable<any> {
    return this.http.post(
      this.getServiceURL() + this.urls.crearMultiple,
      proceso
    )
  }


  public getTipos(): Observable<ProcesosCargaModel[]> {
    return this.http.get<ProcesosCargaModel[]>(
      this.getServiceURL() + this.urls.tipos
    );
  }

  
  public getVigentes(): Observable<ProcesosCargaModel[]> {
    return this.http.get<ProcesosCargaModel[]>(
      this.getServiceURL() + this.urls.vigentes
    );
  }

  public getNombresProcesos(): Observable<string[]> {
    return this.http.get<string[]>(
        this.getServiceURL() + this.urls.listarNombres
    );
  }

  public getCallCenters(idSocio: number, idEvento: number): Observable<CallCenter[]> {
    const params = new HttpParams()
            .append('idSocio', idSocio.toString())
            .append('idEvento', idEvento.toString());

    return this.http.get<CallCenter[]>(
      this.getServiceURL() + this.urls.listarCallCenters,
      { params: params }
    );
  }

  public getTiposArchivo(): Observable<TipoArchivo[]> {
    return this.http.get<TipoArchivo[]>(
        this.getServiceURL() + this.urls.listarTiposArchivo
    );
  }

  public getDirectoriosError(): Observable<DirectorioSalida[]> {
    return this.http.get<TipoArchivo[]>(
        this.getServiceURL() + this.urls.listarDirectoriosError
    );
  }
}
