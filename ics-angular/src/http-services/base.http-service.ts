import { HttpClient } from "@angular/common/http";
import { environment } from '../environments/environment';

export class BaseHttpService {

    // was local
    //private host = "http://localhost:9080";
    // was desarrollo
    private host = "http://logicad.cardif.cl";
    // was testing
    //private host = "http://logicat.cardif.cl";

    constructor(protected http: HttpClient) {
        if (location.hostname != "localhost") {
            this.host = location.protocol + "//" + location.host;
        }
    }

    public getServiceURL(): string {
        return this.host + environment.serviceURL;
    }

    public getFileServiceURL(): string {
        return this.host + environment.fileServiceURL;
    }
}
