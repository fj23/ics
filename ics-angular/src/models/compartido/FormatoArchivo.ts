export class FormatoArchivo {
    public id: number;
    public formato: string;
    public nombre: string;
    public vigencia: string;
}