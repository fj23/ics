export class Parametro {
	public idParametro: number;
	public codParametro: string;
	public descripcionParametro: string;
	public conceptoParametro: string;
	public valorParametro: string;
	public vigencia: string;
	public valorAuxiliar: string;
}