/**
 * Clase que envuelve arrays de objetos obtenidos mediante peticiones al servidor de backend.
 * Esencial para el correcto funcionamiento de la paginación en las grillas de los mantenedores.
 */
export class PaginaRegistros<T>{
    items: T[] = [];
    count: number = 0;
    message: string = "";
}