import { TipoDato } from './TipoDato';
import { SubEstructuraAneto } from '../estructuras/SubEstructuraAneto';

export class ColumnaMetadatos {
    public id: number;
    public codigo: string;
    public descripcion: string;
    public orden: number;
    public tipoDato: TipoDato;
    public esObligatorio: boolean = true;
    public esLlave: boolean = false;
    public posicionInicial: number = 0;
    public posicionFinal: number = 0;
    public idSubEstructura?: number;
    public idSubEstructuraPadre?: number;
    public subEstructura: SubEstructuraAneto;
    public vigencia: string;
    public mapeada: boolean = false;
    public largo: number = 0 ;
}
