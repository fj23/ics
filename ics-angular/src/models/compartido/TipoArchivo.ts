export class TipoArchivo {
    public id: number;
    public nombre: string;
    public vigencia: string;
}