export class Credenciales {
    public username: string;
    public password: string;
    public token: string;
}