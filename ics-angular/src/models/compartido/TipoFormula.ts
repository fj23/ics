
export class TipoFormula {
    public id: number;
    public nombre: string;
    public tipoDato: string;
    public estructura: string;
    public vigencia: string;
    public orden: number;
}