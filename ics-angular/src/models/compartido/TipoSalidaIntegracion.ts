export class TipoSalidaIntegracion{
    public idTipoSalida: number;
    public descripcion: string;
    public vigencia: string;
}