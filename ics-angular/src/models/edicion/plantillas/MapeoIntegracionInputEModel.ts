import { ColumnaDestinoIntegracion } from "src/models/plantillas/integracion/ColumnaDestinoIntegracion";

export class MapeoIntegracionInputEModel {
    public id: number;
	public columnaDestinoEM: ColumnaDestinoIntegracion;
	public esObligatorio: boolean;
    public idAtributo: number;
    public seValida: boolean;
}