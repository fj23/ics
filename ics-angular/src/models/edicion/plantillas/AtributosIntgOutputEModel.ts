import { ColumnaDestinoEntrada } from "src/models/plantillas/entrada/ColumnaDestinoEntrada";
import { AtributoEnriquecimiento } from "src/models/plantillas/AtributoEnriquecimiento";

export class AtributosIntgOutputEModel {
	public idAtributoIntegracion: number;
	public flgObligatorio: string;
	public idAtributoEnriquecimiento: number;
	public idAtributoNegocio: number;
	public idAtributoUsuario: number;
	public idAtributoUser: number;
	public idIntegracionEvento: number;
	public idPlantilla: number;
    public idTipoFormula: number;
    
	public idFormato: number;
    public ordenFormato: number;
	public idTipoHomologacion: number;
    public ordenHomologacion: number;

	public columnaOrigen: ColumnaDestinoEntrada;
	public columnaEnriquecimiento: AtributoEnriquecimiento;
}