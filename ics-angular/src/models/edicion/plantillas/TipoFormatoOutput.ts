export class TipoFormatoOutput {
    public idTipoFormato: number;
    public nombreFormato: string;
    public vigenciaFormato: string;
    public orden: number;
}