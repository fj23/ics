import { TipoDatoEModel } from './TipoDatoEModel';
export class ColumnaMetadatosInputEModel {
    public id: number;
	public codigo: string;
    public nombre: string;
    public largo: number;
	public orden: number;
	public tipoDato: TipoDatoEModel;
	public posicionInicial: number;
	public posicionFinal: number;
	public valorParaFiltrar: string;
	public vigencia: string;
    public columnaOrigen: number;
    public flgAgrupacion: string;
	public estructura?: number;
	public subEstructura?: number;
	public numeroColumna?: number;
	public mapeada?: boolean;
}