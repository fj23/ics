import { ArchivoEModel } from "./ArchivoEMode";
import { EstructuraAneto } from "src/models/estructuras/EstructuraAneto";
import { ColumnaMetadatos } from "src/models/compartido/ColumnaMetadatos";
import { ColumnaDestinoEntrada } from "src/models/plantillas/entrada/ColumnaDestinoEntrada";
import { SubEstructuraAneto } from "src/models/estructuras/SubEstructuraAneto";

export class MapeoColumnaDestinoInputEModel {
	public columnaDestino: number
	public atributoNegocioNombre: string;
	public archivoOrigen: number;
	public archivoNombre: string;	
	public columnaOrigen: number;
	public columnaNombre: string;
	public columnaOrder: string;
	public esObligatorio: boolean;
	public estructuraOrigenId: number;
	public subEstructuraColumnaOrigen: number;
    public valorParaFiltrar: string;
    public filtroExclusion: boolean;
    public encriptar: boolean;

	public grupoColumnaDestino: number;
	public idAtributoUsuario: number;

	public archivoOrigenDatosEM?: ArchivoEModel;
	public columnaOrigenDatosEM: ColumnaMetadatos;
	public columnaDestinoEM: ColumnaDestinoEntrada;	
	public estructuraOrigenEM: EstructuraAneto;
	public subEstructuraColumnaOrigenEM: SubEstructuraAneto;

	public lote: number;
}