import { AtributosIntgOutputEModel } from "./AtributosIntgOutputEModel";


export class AtributosPlantillaIntgOutputEModel {
    public id: number;
	public idPlantilla: number;
	public nombre: string;	
	public orden: number;	
	public formulaSql: string;
	public formulaJson: string;
	//public columnaOrigen: number;
	//public columnaEnriquecimiento: number;
    public esObligatorio: string;
    public seValida: string;
	//public formato: string;
	//public formula: string;
	//public homologacion: string;
	public atributoIntegracion: AtributosIntgOutputEModel;
	public ordenColumnaArchivo: number;

	
}