import { TipoFormatoOutput } from "./TipoFormatoOutput";

export class DetalleFormatoEModel {
    public idFormato: number;
	public tipoFormato: TipoFormatoOutput;
	public formulaFormato: string;
	public tipoDato: string;
	public formato: string;
	public nombreDetalleFormato: string;

}