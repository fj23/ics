export class EstructurasAgrupadaEModel {
    public descripcionAgrupacion: string;
	public idEstructura: number;
	public idGrupoEstructuras: number;
	public indice: string;
}