export class ColumnaArchivoSalidaIntegracionInputEModel {
    public id: number;
	public orden: number;
	public idColumnaOrigen: number;
	public posicionInicial: number;
	public posicionFinal: number;

    public separarArchivo: boolean = false;
    public incluir: boolean = true;

	public flgVisible: string;
	public idColumnaDestino: number;

	public idIntegracion: number;
}