import { ColumnaMetadatosInputEModel } from "./ColumnaMetadatosInputEModel";
import { TipoExtensionEModel } from './TipoExtensionEModel';
import { EstructuraAneto } from '../../estructuras/EstructuraAneto';

export class ArchivoEntradaInputEModel {
    public idArchivo: number;
	public nombreArchivo: string;
	public vigenciaArchivo: string;
	public flgSinTransformacion: string;
	public delimitador: string;
	public numeroHoja: number;
	public lineaInicial: number;
	public lineaFinal: number;
	public columnas: ColumnaMetadatosInputEModel[];
	public estructurasOutput: EstructuraAneto[];
    public idGrupoEstructuras: number;
    public tipoExtension: TipoExtensionEModel;
}