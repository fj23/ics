export class TipoHomologacionOutput {
    public id: number;
	public concepto: string;
	public vigencia: string;
	
	public idSistema1: number;
	public idSistema2: number;
	public idEmpresa1: number;
	public idEmpresa2: number;
	public nombreHomologacion: string;

	public orden: number;
}