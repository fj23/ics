export class ColumnaMetadatosModel {
    public id: number;
    public nombre: string;
    public fechaRecepcion: Date;
    public codigo: number = null;
    public observacion: string;
    public archivo: Blob;
}
