import { ColumnaMetadatos } from '../compartido/ColumnaMetadatos';

export class SubEstructuraAneto {
    public id: number;
    public codigo: string;
    public descripcion: string;
    public columnas: ColumnaMetadatos[];
    public vigencia: string;
    public largo?: number;
}