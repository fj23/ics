import { FiltrosBaseModel } from './FiltrosBaseModel';
export class FiltrosProcesosCargaModel extends FiltrosBaseModel {
    public socio: number = 0;
    public evento: number = 0;
    public nombreProceso:  string = '';
    public estado: string = '';
}