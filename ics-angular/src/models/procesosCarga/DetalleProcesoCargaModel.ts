import { Plantilla } from "../plantillas/Plantilla";

export class DetalleProcesoCargaModel {
    public id : number;
    public idProceso : number;
    public plantilla : Plantilla;

}