import { FormulaIntegracion } from 'src/models/plantillas/integracion/FormulaIntegracion';
import { EstructuraAnetoSalida } from './EstructuraAnetoSalida';


export class ColumnaEstructuraAnetoSalida {
    public id: number;
    public codigo: string;
    public descripcion: string;
    public tipoDato: number;
    public valorFijo: string;
    public atributoDiccionario: number;
    public subestructura: EstructuraAnetoSalida;

    public homologacion: { 
        orden: number, 
        valor: number 
    } = null;
    public formato: { 
        orden: number, 
        valor: number 
    } = null;
    public formula: { 
        orden: number, 
        valor: FormulaIntegracion.IFormulaFactor 
    } = null;

    public get esValida() {
        return !!this.valorFijo || !!this.atributoDiccionario;
    }
}