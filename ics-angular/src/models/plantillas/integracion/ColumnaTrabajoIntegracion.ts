import { ColumnaDestinoEntrada } from '../entrada/ColumnaDestinoEntrada';
import { AtributoEnriquecimiento } from '../AtributoEnriquecimiento';
import { FormulaIntegracion } from './FormulaIntegracion';
import { TipoFormato } from 'src/models/compartido/TipoFormato';
import { TipoFormula } from 'src/models/compartido/TipoFormula';
import { TipoHomologacion } from 'src/models/compartido/TipoHomologacion';


export class ColumnaTrabajoIntegracion {
    public id: number;
    public orden: number;
    public nombre: string;
    public columnaOrigen: ColumnaDestinoEntrada = null;
    public columnaEnriquecimiento: AtributoEnriquecimiento = null;
    public esObligatorio: boolean = true;
    public seValida: boolean = true;
    public mapeada: boolean = false;
    
    public tipoFormato: TipoFormato;
    public tipoFormula: TipoFormula;
    public tipoHomologacion: TipoHomologacion;
    public atributoUser?: number;
    

    public formato: {
        orden: number,
        id: number
    } = null;
    public formula: {
        orden: number,
        cuerpo: FormulaIntegracion.IFormulaFactor,
        jsonStr: string;
    } = null;
    public homologacion: {
        orden: number,
        id: number
    } = null;

    public get tieneOrigen(): boolean {
        return (
            !!this.atributoUser ||
            !!this.columnaOrigen ||
            !!this.columnaEnriquecimiento || 
            (!!this.formula && !!this.formula.cuerpo)
        );
    }
}