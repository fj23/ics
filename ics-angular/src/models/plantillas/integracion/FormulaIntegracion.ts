import { ColumnaTrabajoIntegracion } from './ColumnaTrabajoIntegracion';
import { AtributoEnriquecimiento } from '../AtributoEnriquecimiento';
import { FontawesomeIconsService } from 'src/services/fontawesome-icons/fontawesome-icons.service';
export namespace FormulaIntegracion {

    export function cloneFormulaObject(formula: IFormulaFactor, columnas: ColumnaTrabajoIntegracion[], enriquecimientos: AtributoEnriquecimiento[]): IFormulaFactor {
        let clase = FormulaIntegracion[formula.tipo];
        let c1 = new clase();

        if ('valor' in formula) {
            if (c1 instanceof CampoIntegracion) {

                let colTrabajo: ColumnaTrabajoIntegracion = columnas.find( 
                    (col: ColumnaTrabajoIntegracion) => { 
                        return formula.valor.orden === col.orden; 
                    } 
                );
                
                c1 = new CampoIntegracion(colTrabajo);
            }
            else {
                c1.valor = formula.valor;
            }
        }
        else if ('hijos' in formula && c1 instanceof FormulaParent) {

            if (formula instanceof FncEnriquecimiento && formula.atrEnriquecimiento && enriquecimientos) {
                (<FncEnriquecimiento>c1).socioId = formula.socioId;
                
                const enriquecimientoId: number = formula.atrEnriquecimiento.idAtributoEnriquecimiento;
                (<FncEnriquecimiento>c1).atrEnriquecimiento = enriquecimientos.find(
                    (enriq: AtributoEnriquecimiento) => {
                        return enriq.idAtributoEnriquecimiento === enriquecimientoId;
                    }
                );
            }

            for (let i = 0; i < formula.hijos.length; i++) {
                const nodoHijo = formula.hijos[i];
                let hijo = this.cloneFormulaObject(nodoHijo, columnas, enriquecimientos);
                hijo.padre = c1;
                c1.hijos[i] = hijo;
            }
        }
        
        return c1;
    }
	
	export interface IFormulaFactor {
		tipo: string;
		descripcion?: string;
		padre?: IFormulaFactor;
		hijos?: IFormulaFactor[];
		maxHijos?: number;
		valor?: any;
        getNodoInvalido?(): FormulaErrorNotifierWrapper;
        getNotacionExcel?(): string;
        valido?: boolean;
        categoria?: string;
	}

	export interface ILogicFactor extends IFormulaFactor { isLogic: true; }
	export interface IMathFactor extends IFormulaFactor { isMath: true; }
    export interface IStringFactor extends IFormulaFactor { isString: true; }
    
    export class FormulaErrorNotifierWrapper {
        public origen: FormulaFactor;
        public mensaje: string;
    }

	export class FormulaFactor implements IFormulaFactor {
		public tipo: string;
		public descripcion?: string;
		public padre?: IFormulaFactor;
		public hijos?: IFormulaFactor[];
		public maxHijos?: number;
		public valor?: any;
		public getNodoInvalido?(): FormulaErrorNotifierWrapper { return null; };
        public getNotacionExcel?(): string { return null; };
        public valido?: boolean = true;
        public categoria?: string;
        public icons?: FontawesomeIconsService;

		constructor() {
            this.icons = new FontawesomeIconsService();
         }
	}

	export abstract class FormulaParent extends FormulaFactor {
		public tipo: string;
		public descripcion: string;
		public padre: IFormulaFactor;
		public hijos: IFormulaFactor[];
		public maxHijos: number = Infinity;

		constructor(hijos: FormulaFactor[]) {
			super();
			this.hijos = hijos? hijos : [ ];
        }
	
		public getNodoInvalido(): FormulaErrorNotifierWrapper { 
			if (!!!this.tipo) {
                return {
                    origen: this,
                    mensaje: "Error interno: nodo inválido."
                };
            } 
            else if (this.hijos.length === 0) {
                return {
                    origen: this,
                    mensaje: "No hay nodos hijos."
                };
            }
            else if (this.hijos.length > this.maxHijos) {
				return {
                    origen: this,
                    mensaje: "La cantidad de nodos hijos excede el límite máximo."
                };
			}
            else if (this.hijos.length != this.maxHijos && this.maxHijos !== Infinity) {
				return {
                    origen: this,
                    mensaje: "Error interno: no hay " + this.maxHijos + " nodos hijos en el operador '"+this.descripcion+"'."
                };
            }
			else {
				for (let i = 0; i < this.hijos.length; i++) {
					const nodoInvalido = this.hijos[i].getNodoInvalido()
					if (nodoInvalido != null) {
						return nodoInvalido;
					}
				}
			}
			this.valido = true;
			return null;
		}

		public addHijo(nuevoHijo: FormulaFactor): boolean {
			if (this.hijos.length < this.maxHijos) {
				this.hijos.push(nuevoHijo);
				return true;
			}
			else {
				return false;
			}
		}

		public setHijo(nuevoHijo: FormulaFactor, indice: number): void {
			this.hijos[indice] = nuevoHijo;
		}

		protected actualizarReferenciasHijosAPadre() {
			this.hijos.forEach( (hijo: FormulaFactor) => { hijo.padre = this; }, this );
		}
    }

	export class CampoIntegracion extends FormulaFactor implements IStringFactor, IMathFactor {
        public isString: true = true;
        public isMath: true = true;
		public tipo: string = "CampoIntegracion";
		public descripcion: string = "Columna de Trabajo";
		public valor: ColumnaTrabajoIntegracion;

		constructor(columna: ColumnaTrabajoIntegracion) {
			super();
			if (columna) {
				this.valor = columna;
				this.descripcion += ": '" + columna.nombre + "'";
			}
		}

		public getNodoInvalido(): FormulaErrorNotifierWrapper {

			//sólo pasa si hay fórmula válida u origen de datos definido
			if (!this.valor) {
				return {
                    origen: this,
                    mensaje: "El campo ingresado no es válido."
                };
            }
            else if ( !(
                this.valor.columnaEnriquecimiento || 
                this.valor.columnaOrigen ||
                (this.valor.formula && this.valor.formula.cuerpo && this.valor.formula.cuerpo.getNodoInvalido() === null)
            ) ) {
                return {
                    origen: this,
                    mensaje: "El campo ingresado no posee un origen de datos válido."
                }
            }

			this.valido = true;
			return null;
		}
        
        public getNotacionExcel(): string {
		    if (this.valor) {
                return this.valor.nombre;
            }
		    return "";

		}
	}

	export class IfThenElse extends FormulaParent implements IStringFactor, IMathFactor, ILogicFactor {
        public isMath: true = true;
        public isLogic: true = true;
        public isString: true = true;
        public categoria: string = "Estructuras de Control";
		public tipo: string = "IfThenElse";
		public descripcion: string = "Condicional";
		public maxHijos: number = 3;

		public get condition(): FormulaFactor { return this.hijos[0]; }
		public get case1(): FormulaFactor { return this.hijos[1]; }
		public get case2(): FormulaFactor { return this.hijos[2]; }

		constructor(condition: ILogicFactor, val1: FormulaFactor, val2: FormulaFactor) {
			super(undefined);
			this.hijos[0] = condition? condition : new NullLiteral();
			this.hijos[1] = val1? val1 : new NullLiteral();
			this.hijos[2] = val2? val2 : new NullLiteral();

			this.actualizarReferenciasHijosAPadre();
		}
	
		public getNodoInvalido(): FormulaErrorNotifierWrapper { 
            const superNodoInvalido = super.getNodoInvalido();
            if (superNodoInvalido) {
                return superNodoInvalido;
            }
			else if (this.condition instanceof NullLiteral) {
				return {
                    origen: this.condition,
                    mensaje: "La condición no puede ser un valor nulo."
                };
			}
			else if (this.case1 instanceof NullLiteral && this.case2 instanceof NullLiteral) {
				return {
                    origen: this,
                    mensaje: "El valor obtenido al cumplirse o no la condición, no puede ser nulo en ambos casos."
                };
            }
            else {
                const bothLogic = ('isLogic' in this.case1 && 'isLogic' in this.case2);
                const bothMath = ('isMath' in this.case1 && 'isMath' in this.case2);
                const bothString = ('isString' in this.case1 && 'isString' in this.case2);

                if (!bothLogic && !bothMath && !bothString) {
                    return {
                        origen: this,
                        mensaje: "Los tipos de dato resultantes de la condicional deben coincidir."
                    };
                }
                else {
                    const invalidCond = this.condition.getNodoInvalido();
                    if (invalidCond != null) { return invalidCond; }
                    
                    const invalidCase1 = this.case1.getNodoInvalido();
                    if (invalidCase1 != null) { return invalidCase1; }
                    
                    const invalidCase2 = this.case2.getNodoInvalido();
                    if (invalidCase2 != null) { return invalidCase2; }
                }
            }
			this.valido = true;
			return null;
		}
        
        public getNotacionExcel(): string { 
            let notacionExcel: string = "SI"; 

            notacionExcel += "(";
            notacionExcel += this.condition.getNotacionExcel() + "; ";
            notacionExcel += this.case1.getNotacionExcel() + "; ";
            notacionExcel += this.case2.getNotacionExcel();
            notacionExcel += ")";

            
            return notacionExcel;
        }
	}


	/* LOGICO */

	export class EqualTo extends FormulaParent implements ILogicFactor {
        public isLogic: true = true;
        public categoria: string = "Comparación";
		public tipo: string = "EqualTo";
		public descripcion: string = "Igualdad de valores";
		public maxHijos: number = 2;

		public get val1(): FormulaFactor { return this.hijos[0]; }
		public get val2(): FormulaFactor { return this.hijos[1]; }

		constructor(val1: FormulaFactor, val2: FormulaFactor) {
			super(undefined);
			this.hijos[0] = val1? val1 : new NullLiteral();
			this.hijos[1] = val2? val2 : new NullLiteral();

			this.actualizarReferenciasHijosAPadre();
		}
        
        public getNotacionExcel(): string { 
            return this.val1.getNotacionExcel() + " = " + this.val2.getNotacionExcel();
        }
	}

	export class NotEqualTo extends FormulaParent implements ILogicFactor {
        public isLogic: true = true;
        public categoria: string = "Comparación";
		public tipo: string = "NotEqualTo";
		public descripcion: string = "Desigualdad de valores";
		public maxHijos: number = 2;

		public get val1(): FormulaFactor { return this.hijos[0]; }
		public get val2(): FormulaFactor { return this.hijos[1]; }

		constructor(val1: FormulaFactor, val2: FormulaFactor) {
			super(undefined);
			this.hijos[0] = val1? val1 : new NullLiteral();
			this.hijos[1] = val2? val2 : new NullLiteral();

			this.actualizarReferenciasHijosAPadre();
		}
        
        public getNotacionExcel(): string { 
            return this.val1.getNotacionExcel() + " <> " + this.val2.getNotacionExcel();
        }
	}
	
	export class HigherEqualThan extends FormulaParent implements ILogicFactor {
        public isLogic: true = true;
        public categoria: string = "Comparación";
		public tipo: string = "HigherEqualThan";
		public descripcion: string = "Valor mayor o igual a otro";
        public maxHijos: number = 2;

		public get val1(): IFormulaFactor { return this.hijos[0]; }
		public get val2(): IFormulaFactor { return this.hijos[1]; }

		constructor(val1: IMathFactor, val2: IMathFactor) {
			super(undefined);
			this.hijos[0] = val1? val1 : new NullLiteral();
            this.hijos[1] = val2? val2 : new NullLiteral();

			this.actualizarReferenciasHijosAPadre();
		}
        
        public getNodoInvalido(): FormulaErrorNotifierWrapper { 
            let superNodoInvalido = super.getNodoInvalido();
            if (superNodoInvalido) {
                return superNodoInvalido;
            }
            else {
                let hijoInvalido: IFormulaFactor = this.hijos.find( 
                    (hijo: IFormulaFactor) => { 
                        return hijo instanceof NullLiteral || !('isMath' in hijo);
                    } 
                );
                if (hijoInvalido) {
                    return {
                        origen: hijoInvalido,
                        mensaje: "Valor numérico no válido."
                    };
                }
            }
			this.valido = true;
			return null;
        }
        
        public getNotacionExcel(): string { 
            return this.val1.getNotacionExcel() + " >= " + this.val2.getNotacionExcel();
        }
	}

	export class HigherThan extends FormulaParent implements ILogicFactor {
        public isLogic: true = true;
        public categoria: string = "Comparación";
		public tipo: string = "HigherThan";
		public descripcion: string = "Valor mayor a otro";
		public maxHijos: number = 2;

		public get val1(): IFormulaFactor { return this.hijos[0]; }
		public get val2(): IFormulaFactor { return this.hijos[1]; }

		constructor(val1: IMathFactor, val2: IMathFactor) {
			super(undefined);
			this.hijos[0] = val1? val1 : new NullLiteral();
			this.hijos[1] = val2? val2 : new NullLiteral();

			this.actualizarReferenciasHijosAPadre();
		}
        
        public getNodoInvalido(): FormulaErrorNotifierWrapper { 
            let superNodoInvalido = super.getNodoInvalido();
            if (superNodoInvalido) {
                return superNodoInvalido;
            }
            else {
                let hijoInvalido: IFormulaFactor = this.hijos.find( 
                    (hijo: IFormulaFactor) => { 
                        return hijo instanceof NullLiteral || !('isMath' in hijo); 
                    } 
                );
                if (hijoInvalido) {
                    return {
                        origen: hijoInvalido,
                        mensaje: "Valor numérico no válido."
                    };
                }
            }
			this.valido = true;
			return null;
        }
        
        public getNotacionExcel(): string { 
            return this.val1.getNotacionExcel() + " > " + this.val2.getNotacionExcel();
        }
	}

	export class LowerEqualThan extends FormulaParent implements ILogicFactor {
        public isLogic: true = true;
        public categoria: string = "Comparación";
		public tipo: string = "LowerEqualThan";
		public descripcion: string = "Valor menor o igual a otro";
		public maxHijos: number = 2;

		public get val1(): IFormulaFactor { return this.hijos[0]; }
		public get val2(): IFormulaFactor { return this.hijos[1]; }

		constructor(val1: IMathFactor, val2: IMathFactor) {
			super(undefined);
			this.hijos[0] = val1? val1 : new NullLiteral();
			this.hijos[1] = val2? val2 : new NullLiteral();

			this.actualizarReferenciasHijosAPadre();
		}
        
        public getNodoInvalido(): FormulaErrorNotifierWrapper { 
            let superNodoInvalido = super.getNodoInvalido();
            if (superNodoInvalido) {
                return superNodoInvalido;
            }
            else {
                let hijoInvalido: IFormulaFactor = this.hijos.find( 
                    (hijo: IFormulaFactor) => { 
                        return hijo instanceof NullLiteral || !('isMath' in hijo); 
                    } 
                    );
                if (hijoInvalido) {
                    return {
                        origen: hijoInvalido,
                        mensaje: "Valor numérico no válido."
                    };
                }
            }
			this.valido = true;
			return null;
        }
        
        public getNotacionExcel(): string { 
            return this.val1.getNotacionExcel() + " <= " + this.val2.getNotacionExcel();
        }
	}

	export class LowerThan extends FormulaParent implements ILogicFactor {
        public isLogic: true = true;
        public categoria: string = "Comparación";
		public tipo: string = "LowerThan";
		public descripcion: string = "Valor menor a otro";
		public maxHijos: number = 2;

		public get val1(): IFormulaFactor { return this.hijos[0]; }
		public get val2(): IFormulaFactor { return this.hijos[1]; }

		constructor(val1: IMathFactor, val2: IMathFactor) {
			super(undefined);
			this.hijos[0] = val1? val1 : new NullLiteral();
			this.hijos[1] = val2? val2 : new NullLiteral();

			this.actualizarReferenciasHijosAPadre();
		}
        
        public getNodoInvalido(): FormulaErrorNotifierWrapper { 
            let superNodoInvalido = super.getNodoInvalido();
            if (superNodoInvalido) {
                return superNodoInvalido;
            }
            else {
                let hijoInvalido: IFormulaFactor = this.hijos.find( 
                    (hijo: IFormulaFactor) => { 
                        return hijo instanceof NullLiteral || !('isMath' in hijo); 
                    } 
                );
                if (hijoInvalido) {
                    return {
                        origen: hijoInvalido,
                        mensaje: "Valor numérico no válido."
                    };
                }
            }
			this.valido = true;
			return null;
        }
        
        public getNotacionExcel(): string { 
            return this.val1.getNotacionExcel() + " < " + this.val2.getNotacionExcel();
        }
	}

	export class And extends FormulaParent implements ILogicFactor {
        public isLogic: true = true;
        public categoria: string = "Estructuras de Control";
		public tipo: string = "And";
		public descripcion: string = "Varias condiciones...";

		constructor(hijos: IFormulaFactor[]) {
			super(hijos);

			this.actualizarReferenciasHijosAPadre();
		}
        
        public getNodoInvalido(): FormulaErrorNotifierWrapper { 
            let superNodoInvalido = super.getNodoInvalido();
            if (superNodoInvalido) {
                return superNodoInvalido;
            }
            else {
                let hijoInvalido: IFormulaFactor = this.hijos.find( 
                    (hijo: IFormulaFactor) => { 
                        return hijo instanceof NullLiteral || !('isLogic' in hijo); 
                    } 
                );
                if (hijoInvalido) {
                    return {
                        origen: hijoInvalido,
                        mensaje: "Valor lógico no válido."
                    };
                }
            }
			this.valido = true;
			return null;
        }
        
        public getNotacionExcel(): string { 
            let notacionExcel: string = "Y"; 

            notacionExcel += "(";
            for (let i = 0; i < this.hijos.length; i++) {
                const hijo = this.hijos[i];

                notacionExcel += hijo.getNotacionExcel();

                if (i+1 < this.hijos.length) {
                    notacionExcel += "; ";
                }
            }
            notacionExcel += ")";

            
            return notacionExcel;
        }
	}

	export class Or extends FormulaParent implements ILogicFactor {
        public isLogic: true = true;
        public categoria: string = "Estructuras de Control";
		public tipo: string = "Or";
		public descripcion: string = "Alguna de las condiciones...";
		public hijos: ILogicFactor[];

		constructor(hijos: ILogicFactor[]) {
			super(hijos);

			this.actualizarReferenciasHijosAPadre();
		}
        
        public getNodoInvalido(): FormulaErrorNotifierWrapper { 
            let superNodoInvalido = super.getNodoInvalido();
            if (superNodoInvalido) {
                return superNodoInvalido;
            }
            else {
                let hijoInvalido: IFormulaFactor = this.hijos.find( 
                    (hijo: IFormulaFactor) => { 
                        return hijo instanceof NullLiteral || !('isLogic' in hijo); 
                    } 
                );
                if (hijoInvalido) {
                    return {
                        origen: hijoInvalido,
                        mensaje: "Valor lógico no válido."
                    };
                }
            }
			this.valido = true;
			return null;
        }
        
        public getNotacionExcel(): string { 
            let notacionExcel: string = "O"; 

            notacionExcel += "(";
            for (let i = 0; i < this.hijos.length; i++) {
                const hijo = this.hijos[i];

                notacionExcel += hijo.getNotacionExcel();

                if (i+1 < this.hijos.length) {
                    notacionExcel += "; ";
                }
            }
            notacionExcel += ")";

            
            return notacionExcel;
        };
	}
	


	/* NUMERICO */

	export class NumberLiteral extends FormulaFactor implements IMathFactor {
        public isMath: true = true;
		public tipo: string = "NumberLiteral";
		public descripcion: string = "Constante numérica";
		public valor: number;

		constructor(valor: number) {
			super();
			if (valor || valor === 0) { this.valor = valor; }
		}
	
		public getNodoInvalido(): FormulaErrorNotifierWrapper { 
			if (isNaN(Number(this.valor))) {
				return {
                    origen: this,
                    mensaje: "Valor numérico no válido."
                };
            }
            this.valido = true;
			return null;
		}
        
        public getNotacionExcel(): string { 
            let notacionExcel: string = this.valor.toString();             
            return notacionExcel;
        };
	}

	export class Division extends FormulaParent implements IMathFactor {
        public isMath: true = true;
        public categoria: string = "Operaciones aritméticas";
		public tipo: string = "Division";
		public descripcion: string = "División";
        public hijos: IMathFactor[];

		constructor(hijos: IMathFactor[]) {
			super(hijos);

			this.actualizarReferenciasHijosAPadre();
		}
	
		public getNodoInvalido(): FormulaErrorNotifierWrapper { 
            let superNodoInvalido = super.getNodoInvalido();
            if (superNodoInvalido) {
                return superNodoInvalido;
            }
            else {
                let hijoInvalido: IFormulaFactor = this.hijos.find( 
                    (hijo: IFormulaFactor) => { 
                        return hijo instanceof NullLiteral || !('isMath' in hijo); 
                    } 
                );
                if (hijoInvalido) {
                    return {
                        origen: hijoInvalido,
                        mensaje: "Valor numérico no válido."
                    };
                }
            }
            this.valido = true;
            return null;
		}
        
        public getNotacionExcel(): string { 
            let notacionExcel: string = ""; 

            notacionExcel += "(";
            for (let i = 0; i < this.hijos.length; i++) {
                const hijo = this.hijos[i];

                if (i === 0) {
                    notacionExcel += hijo.getNotacionExcel();
                }
                else {
                    notacionExcel += " / " + hijo.getNotacionExcel();
                }
            }
            notacionExcel += ")";

            
            return notacionExcel;
        }
	}
	
	export class Multiplication extends FormulaParent implements IMathFactor {
        public isMath: true = true;
        public categoria: string = "Operaciones aritméticas";
		public tipo: string = "Multiplication";
		public descripcion: string = "Multiplicación";
		public hijos: IMathFactor[];

		constructor(hijos: IMathFactor[]) {
			super(hijos);

			this.actualizarReferenciasHijosAPadre();
		}
	
		public getNodoInvalido(): FormulaErrorNotifierWrapper { 
            let superNodoInvalido = super.getNodoInvalido();
            if (superNodoInvalido) {
                return superNodoInvalido;
            }
            else {
                let hijoInvalido: IFormulaFactor = this.hijos.find( 
                    (hijo: IFormulaFactor) => { 
                        return hijo instanceof NullLiteral || !('isMath' in hijo); 
                    } 
                );
                if (hijoInvalido) {
                    return {
                        origen: hijoInvalido,
                        mensaje: "Valor numérico no válido."
                    };
                }
            }
            this.valido = true;
            return null;
		}
        
        public getNotacionExcel(): string { 
            let notacionExcel: string = ""; 

            notacionExcel += "(";
            for (let i = 0; i < this.hijos.length; i++) {
                const hijo = this.hijos[i];

                if (i === 0) {
                    notacionExcel += hijo.getNotacionExcel();
                }
                else {
                    notacionExcel += " * " + hijo.getNotacionExcel();
                }
            }
            notacionExcel += ")";

            
            return notacionExcel;
        }
	}

	export class Subtraction extends FormulaParent implements IMathFactor {
        public isMath: true = true;
        public categoria: string = "Operaciones aritméticas";
		public tipo: string = "Subtraction";
		public descripcion: string = "Sustracción";
		public hijos: IMathFactor[];

		constructor(hijos: IMathFactor[]) {
			super(hijos);

			this.actualizarReferenciasHijosAPadre();
		}
	
		public getNodoInvalido(): FormulaErrorNotifierWrapper { 
            let superNodoInvalido = super.getNodoInvalido();
            if (superNodoInvalido) {
                return superNodoInvalido;
            }
            else {
                let hijoInvalido: IFormulaFactor = this.hijos.find( 
                    (hijo: IFormulaFactor) => { 
                        return hijo instanceof NullLiteral || !('isMath' in hijo); 
                    } 
                );
                if (hijoInvalido) {
                    return {
                        origen: hijoInvalido,
                        mensaje: "Valor numérico no válido."
                    };
                }
            }
            this.valido = true;
            return null;
		}
        
        public getNotacionExcel(): string { 
            let notacionExcel: string = ""; 

            notacionExcel += "(";
            for (let i = 0; i < this.hijos.length; i++) {
                const hijo = this.hijos[i];

                if (i === 0) {
                    notacionExcel += hijo.getNotacionExcel();
                }
                else {
                    notacionExcel += " - " + hijo.getNotacionExcel();
                }
            }
            notacionExcel += ")";

            
            return notacionExcel;
        }
	}

	export class Sum extends FormulaParent implements IMathFactor {
        public isMath: true = true;
        public categoria: string = "Operaciones aritméticas";
		public tipo: string = "Sum";
		public descripcion: string = "Suma";
		public hijos: IMathFactor[];

		constructor(hijos: IMathFactor[]) {
			super(hijos);

			this.actualizarReferenciasHijosAPadre();
		}
	
		public getNodoInvalido(): FormulaErrorNotifierWrapper { 
            let superNodoInvalido = super.getNodoInvalido();
            if (superNodoInvalido) {
                return superNodoInvalido;
            }
            else {
                let hijoInvalido: IFormulaFactor = this.hijos.find( 
                    (hijo: IFormulaFactor) => { 
                        return hijo instanceof NullLiteral || !('isMath' in hijo);
                    } 
                );
                if (hijoInvalido) {
                    return {
                        origen: hijoInvalido,
                        mensaje: "Valor numérico no válido."
                    };
                }
            }
            this.valido = true;
            return null;
		}
        
        public getNotacionExcel(): string { 
            let notacionExcel: string = ""; 

            notacionExcel += "(";
            for (let i = 0; i < this.hijos.length; i++) {
                const hijo = this.hijos[i];

                if (i === 0) {
                    notacionExcel += hijo.getNotacionExcel();
                }
                else {
                    notacionExcel += " + " + hijo.getNotacionExcel();
                }
            }
            notacionExcel += ")";

            
            return notacionExcel;
        }
	}

	export class Round extends FormulaParent implements IMathFactor {
        public isMath: true = true;
        public categoria: string = "Funciones matemáticas";
		public tipo: string = "Round";
		public descripcion: string = "Redondear";
        public hijos: IMathFactor[];
        public maxHijos = 1;

        public get numero() { return this.hijos[0]; }

		constructor(hijos: IMathFactor[]) {
            super(hijos);

            if (!this.numero) { this.hijos[0] = new NullLiteral(); }
            
            this.actualizarReferenciasHijosAPadre();
		}
	
		public getNodoInvalido(): FormulaErrorNotifierWrapper { 
            this.valido = true;
            return this.numero.getNodoInvalido();
		}
        
        public getNotacionExcel(): string { 
            let notacionExcel: string = "REDONDEAR"; 

            notacionExcel += "(";
            notacionExcel += this.numero.getNotacionExcel() + "; 0"
            notacionExcel += ")";

            
            return notacionExcel;
        }
	}

	export class Ceil extends FormulaParent implements IMathFactor {
        public isMath: true = true;
        public categoria: string = "Funciones matemáticas";
		public tipo: string = "Ceil";
		public descripcion: string = "Redondear hacia arriba";
        public hijos: IMathFactor[];
        public maxHijos = 1;

        public get numero() { return this.hijos[0]; }

		constructor(hijos: IMathFactor[]) {
            super(hijos);

            if (!this.numero) { this.hijos[0] = new NullLiteral(); }
            
            this.actualizarReferenciasHijosAPadre();
		}
	
		public getNodoInvalido(): FormulaErrorNotifierWrapper { 
            this.valido = true;
            return this.numero.getNodoInvalido();
		}
        
        public getNotacionExcel(): string { 
            let notacionExcel: string = "REDONDEAR.MAS"; 

            notacionExcel += "(";
            notacionExcel += this.numero.getNotacionExcel() + "; 0"
            notacionExcel += ")";

            
            return notacionExcel;
        }
	}

	export class Floor extends FormulaParent implements IMathFactor {
        public isMath: true = true;
        public categoria: string = "Funciones matemáticas";
		public tipo: string = "Floor";
		public descripcion: string = "Redondear hacia abajo";
        public hijos: IMathFactor[];
        public maxHijos = 1;

        public get numero() { return this.hijos[0]; }

		constructor(hijos: IMathFactor[]) {
            super(hijos);

            if (!this.numero) { this.hijos[0] = new NullLiteral(); }
            
            this.actualizarReferenciasHijosAPadre();
		}
	
		public getNodoInvalido(): FormulaErrorNotifierWrapper { 
            this.valido = true;
            return this.numero.getNodoInvalido();
		}
        
        public getNotacionExcel(): string { 
            let notacionExcel: string = "REDONDEAR.MENOS"; 

            notacionExcel += "(";
            notacionExcel += this.numero.getNotacionExcel() + "; 0"
            notacionExcel += ")";

            
            return notacionExcel;
        }
	}

	export class Trunc extends FormulaParent implements IMathFactor {
        public isMath: true = true;
        public categoria: string = "Funciones matemáticas";
		public tipo: string = "Trunc";
		public descripcion: string = "Truncar decimales";
		public hijos: IMathFactor[];
        public maxHijos: number = 2;
        
        public get numero() { return this.hijos[0]; }
        public get decimales() { return this.hijos[1]; }

		constructor(hijos: IMathFactor[]) {
            super(hijos);
            
            if (!this.hijos[0]) { this.hijos[0] = new NullLiteral(); }
            if (!this.hijos[1]) { this.hijos[1] = new NumberLiteral(0); }

			this.actualizarReferenciasHijosAPadre();
		}
	
		public getNodoInvalido(): FormulaErrorNotifierWrapper { 
			if (!!!this.tipo) {
                return {
                    origen: this,
                    mensaje: "Error interno: nodo inválido."
                };
            } 
            else if (this.hijos.length === 0) {
                return {
                    origen: this,
                    mensaje: "No hay nodos hijos."
                };
            }
            else if (this.hijos.length > this.maxHijos) {
				return {
                    origen: this,
                    mensaje: "La cantidad de nodos hijos excede el límite máximo."
                };
			}
            else if (this.hijos.length < 2) {
				return {
                    origen: this,
                    mensaje: "Error interno: hay menos de 2 nodos hijos."
                };
            }
			else {
				for (let i = 0; i < this.hijos.length; i++) {
					const nodoInvalido = this.hijos[i].getNodoInvalido()
					if (nodoInvalido != null) {
						return nodoInvalido;
					}
				}
			}
            this.valido = true;
            return null;
		}
        
        public getNotacionExcel(): string { 
            let notacionExcel: string = "TRUNCAR"; 

            notacionExcel += "(";
            notacionExcel += this.numero.getNotacionExcel() + "; ";
            notacionExcel += this.decimales.getNotacionExcel();
            notacionExcel += ")";

            
            return notacionExcel;
        }
	}


	/* TEXTO */

	export class StringLiteral extends FormulaFactor implements IStringFactor {
        public isString: true = true;
		public tipo: string = "StringLiteral";
		public descripcion: string = "Constante textual";
		public valor: string;

		constructor(valor: string) {
			super();
			this.valor = valor;
		}
	
		public getNodoInvalido(): FormulaErrorNotifierWrapper { 
			if (!!!this.valor && this.valor !== '') {
				return {
                    origen: this,
                    mensaje: "Valor alfanumérico no válido."
                };
			}
            this.valido = true;
            return null;
		}
        
        public getNotacionExcel(): string { 
            return this.valor.replace(";", "';'");
        }
	}

	export class Concat extends FormulaParent implements IStringFactor {
        public isString: true = true;
        public categoria: string = "Funciones de cadena";
		public tipo: string = "Concat";
		public descripcion: string = "Concatenar";
		public hijos: IStringFactor[];

		constructor(hijos: IStringFactor[]) {
			super(hijos);

			this.actualizarReferenciasHijosAPadre();
		}
	
		public getNodoInvalido(): FormulaErrorNotifierWrapper { 
            let superNodoInvalido = super.getNodoInvalido();
            if (superNodoInvalido) {
                return superNodoInvalido;
            }
            else {
                let hijoInvalido: IFormulaFactor = this.hijos.find( 
                    (hijo: IFormulaFactor) => { 
                        return hijo instanceof NullLiteral; 
                    } 
                );
                if (hijoInvalido) {
                    return {
                        origen: hijoInvalido,
                        mensaje: "Valor no válido."
                    };
                }
            }
            this.valido = true;
            return null;
		}
        
        public getNotacionExcel(): string { 
            let notacionExcel: string = "CONCATENAR"; 

            notacionExcel += "(";
            for (let i = 0; i < this.hijos.length; i++) {
                const hijo = this.hijos[i];

                notacionExcel += hijo.getNotacionExcel();

                if (i+1 < this.hijos.length) {
                    notacionExcel += "; ";
                }
            }
            notacionExcel += ")";

            
            return notacionExcel;
        }
	}

	export class RPad extends FormulaParent implements IStringFactor {
        public isString: true = true;
        public categoria: string = "Funciones de cadena";
		public tipo: string = "RPad";
		public descripcion: string = "Rellenar a la derecha";
		public hijos: IFormulaFactor[];
        public maxHijos: number = 3;
        
        public get cadena() { return this.hijos[0]; }
        public get largo() { return this.hijos[1]; }
        public get caracter() { return this.hijos[2]; }

		constructor(hijos: IFormulaFactor[]) {
			super(hijos);
            
            if (!this.hijos[0]) { this.hijos[0] = new NullLiteral(); }
            if (!this.hijos[1]) { this.hijos[1] = new NullLiteral(); }
            if (!this.hijos[2]) { this.hijos[2] = new NullLiteral(); }

			this.actualizarReferenciasHijosAPadre();
		}
	
		public getNodoInvalido(): FormulaErrorNotifierWrapper { 
            let superNodoInvalido = super.getNodoInvalido();
            if (superNodoInvalido) {
                return superNodoInvalido;
            }
            else if (!('isMath' in this.largo)) {
                return {
                    origen: this.largo,
                    mensaje: "Largo inválido."
                };
            } 
            else {
                let hijoInvalido: IFormulaFactor = this.hijos.find( 
                    (hijo: IFormulaFactor) => { 
                        return hijo instanceof NullLiteral; 
                    } 
                );
                if (hijoInvalido) {
                    return {
                        origen: hijoInvalido,
                        mensaje: "Valor no válido."
                    };
                }
            }
            this.valido = true;
            return null;
		}
        
        public getNotacionExcel(): string { 
            let notacionExcel: string = "RELLENAR_DERECHA"; 

            notacionExcel += "(";
            for (let i = 0; i < this.hijos.length; i++) {
                const hijo = this.hijos[i];

                notacionExcel += hijo.getNotacionExcel();

                if (i+1 < this.hijos.length) {
                    notacionExcel += "; ";
                }
            }
            notacionExcel += ")";

            
            return notacionExcel;
        }
	}

	export class LPad extends FormulaParent implements IStringFactor {
        public isString: true = true;
        public categoria: string = "Funciones de cadena";
		public tipo: string = "LPad";
		public descripcion: string = "Rellenar a la izquierda";
		public hijos: IFormulaFactor[];
        public maxHijos: number = 3;
        
        public get cadena() { return this.hijos[0]; }
        public get largo() { return this.hijos[1]; }
        public get caracter() { return this.hijos[2]; }

		constructor(hijos: IFormulaFactor[]) {
			super(hijos);
            
            if (!this.hijos[0]) { this.hijos[0] = new NullLiteral(); }
            if (!this.hijos[1]) { this.hijos[1] = new NullLiteral(); }
            if (!this.hijos[2]) { this.hijos[2] = new NullLiteral(); }

			this.actualizarReferenciasHijosAPadre();
		}
	
		public getNodoInvalido(): FormulaErrorNotifierWrapper { 
            let superNodoInvalido = super.getNodoInvalido();
            if (superNodoInvalido) {
                return superNodoInvalido;
            }
            else {
                let hijoInvalido: IFormulaFactor = this.hijos.find( 
                    (hijo: IFormulaFactor) => { 
                        return hijo instanceof NullLiteral;
                    } 
                );
                if (hijoInvalido) {
                    return {
                        origen: hijoInvalido,
                        mensaje: "Valor no válido."
                    };
                }
            }
            this.valido = true;
            return null;
		}
        
        public getNotacionExcel(): string { 
            let notacionExcel: string = "RELLENAR_IZQUIERDA"; 

            notacionExcel += "(";
            for (let i = 0; i < this.hijos.length; i++) {
                const hijo = this.hijos[i];

                notacionExcel += hijo.getNotacionExcel();

                if (i+1 < this.hijos.length) {
                    notacionExcel += "; ";
                }
            }
            notacionExcel += ")";

            
            return notacionExcel;
        }
	}

	export class Substring extends FormulaParent implements IStringFactor {
        public isString: true = true;
        public categoria: string = "Funciones de cadena";
		public tipo: string = "Substring";
		public descripcion: string = "Subcadena de texto";
        public maxHijos: number = 3;
        
        public get cadena() { return this.hijos[0]; }
        public get posicion() { return this.hijos[1]; }
        public get largo() { return this.hijos[2]; }

		constructor(hijos: FormulaFactor[]) {
			super(hijos);
            
            if (!this.hijos[0]) { this.hijos[0] = new NullLiteral(); }
            if (!this.hijos[1]) { this.hijos[1] = new NullLiteral(); }
            if (!this.hijos[2]) { this.hijos[2] = new NullLiteral(); }

			this.actualizarReferenciasHijosAPadre();
		}
	
		public getNodoInvalido(): FormulaErrorNotifierWrapper { 
            let superNodoInvalido = super.getNodoInvalido();
            if (superNodoInvalido) {
                return superNodoInvalido;
            }
            else {
                if (!('isString' in this.cadena)) {
                    return {
                        origen: this.cadena,
                        mensaje: "Valor alfanumérico no válido."
                    };
                }
                else if (!('isMath' in this.posicion)) {
                    return {
                        origen: this.posicion,
                        mensaje: "Valor numérico no válido."
                    };
                }
                else if (!('isMath' in this.largo)) {
                    return {
                        origen: this.largo,
                        mensaje: "Valor numérico no válido."
                    };
                }
            }
            this.valido = true;
            return null;
		}
        
        public getNotacionExcel(): string { 
            let notacionExcel: string = "EXTRAE"; 

            notacionExcel += "(";
            notacionExcel += this.cadena.getNotacionExcel() + "; ";

            let posicionExcel: string = this.posicion.getNotacionExcel();
            if ('isString' in this.posicion) {
                posicionExcel.replace("'", "");
            }

            notacionExcel += posicionExcel + "; ";

            
            let largoExcel: string = this.largo.getNotacionExcel();
            if ('isString' in this.posicion) {
                posicionExcel.replace("'", "");
            }   
            notacionExcel += largoExcel;
            notacionExcel += ")";
            
            return notacionExcel;
        }
	}

	export class FncSplitFormula extends FormulaParent implements IStringFactor {
        public isString: true = true;
        public categoria: string = "Funciones de cadena";
		public tipo: string = "FncSplitFormula";
		public descripcion: string = "Extraer de cadena delimitada";
        public maxHijos: number = 3;
        
        public get cadena(): IFormulaFactor { return this.hijos[0]; }
        public get delimitador(): IFormulaFactor { return this.hijos[1]; }
        public get indice(): IFormulaFactor { return this.hijos[2]; }
		
		constructor(hijos: IFormulaFactor[]) {
            super(hijos);
            
            if (!this.cadena) { this.hijos[0] = new NullLiteral(); }
            if (!this.delimitador) { this.hijos[1] = new NullLiteral(); }
            if (!this.indice) { this.hijos[2] = new NullLiteral(); }
            
            this.actualizarReferenciasHijosAPadre();
		}

		public getNodoInvalido(): FormulaErrorNotifierWrapper {
            let superNodoInvalido = super.getNodoInvalido();
            if (superNodoInvalido) {
                return superNodoInvalido;
            }
			else {
                const cadenaNotString = !('isString' in this.cadena);
                const delimitadorNotString = !('isString' in this.delimitador);
                const indiceNotMath = !('isMath' in this.indice);


                if (cadenaNotString) {
                    return {
                        origen: this.cadena,
                        mensaje: "Cadena de texto no válida."
                    };
                }
                else if (delimitadorNotString) {
                    return {
                        origen: this.delimitador,
                        mensaje: "Delimitador de columnas no válido."
                    };
                }
                else if (indiceNotMath) {
                    return {
                        origen: this.indice,
                        mensaje: "Índice de subcadena no válido."
                    };
                }
                else {
                    for (let i = 0; i < this.hijos.length; i++) {
                        const nodoInvalido = this.hijos[i].getNodoInvalido();
                        if (nodoInvalido != null) {
                            return nodoInvalido;
                        }
                    }
                }
            }
			this.cadena.valido = true;
			this.delimitador.valido = true;
			this.indice.valido = true;
			this.valido = true;
			return null;
		}
        
        public getNotacionExcel(): string { 
            let notacionExcel: string = "EXTRAE"; 

            notacionExcel += "(";
            notacionExcel += this.cadena.getNotacionExcel() + "; ";
            notacionExcel += "'" + this.delimitador.getNotacionExcel() + "'; ";
            notacionExcel +=  this.indice.getNotacionExcel() + "; ";
            notacionExcel += ")";
            
            return notacionExcel;
        }
	}

	export class Replace extends FormulaParent implements IStringFactor {
        public isString: true = true;
        public categoria: string = "Funciones de cadena";
		public tipo: string = "Replace";
		public descripcion: string = "Sustituir subcadena";
        public maxHijos: number = 3;
        
        public get cadena(): IFormulaFactor { return this.hijos[0]; }
        public get subcadena(): IFormulaFactor { return this.hijos[1]; }
        public get reemplazo(): IFormulaFactor { return this.hijos[2]; }
		
		constructor(hijos: IFormulaFactor[]) {
            super(hijos);
            
            if (!this.cadena) { this.hijos[0] = new NullLiteral(); }
            if (!this.subcadena) { this.hijos[1] = new NullLiteral(); }
            if (!this.reemplazo) { this.hijos[2] = new NullLiteral(); }
            
            this.actualizarReferenciasHijosAPadre();
		}

		public getNodoInvalido(): FormulaErrorNotifierWrapper {
            let superNodoInvalido = super.getNodoInvalido();
            if (superNodoInvalido) {
                return superNodoInvalido;
            } else {
                let hijoInvalido: IFormulaFactor = this.hijos.find( 
                    (hijo: IFormulaFactor) => { 
                        return hijo instanceof NullLiteral && hijo != this.reemplazo;
                    } 
                );
                if (hijoInvalido) {
                    return {
                        origen: hijoInvalido,
                        mensaje: "Valor no válido."
                    };
                } else {
                    const cadenaNotString = !('isString' in this.cadena);
                    const subcadenaNotString = !('isString' in this.subcadena);
                    const indiceNotString = !('isString' in this.reemplazo);
    
    
                    if (cadenaNotString) {
                        return {
                            origen: this.cadena,
                            mensaje: "Cadena de texto no válida."
                        };
                    } else if (subcadenaNotString) {
                        return {
                            origen: this.subcadena,
                            mensaje: "Subcadena no válida."
                        };
                    } else if (indiceNotString) {
                        return {
                            origen: this.reemplazo,
                            mensaje: "Reemplazo no válido."
                        };
                    } else {
                        for (let i = 0; i < this.hijos.length; i++) {
                            const nodoInvalido = this.hijos[i].getNodoInvalido();
                            if (nodoInvalido != null) {
                                return nodoInvalido;
                            }
                        }
                    }
                }
            }
			this.cadena.valido = true;
			this.subcadena.valido = true;
			this.reemplazo.valido = true;
			this.valido = true;
			return null;
		}
        
        public getNotacionExcel(): string { 
            let notacionExcel: string = "SUSTITUIR"; 

            notacionExcel += "(";
            notacionExcel += this.cadena.getNotacionExcel() + "; ";
            notacionExcel += "'" + this.subcadena.getNotacionExcel() + "'; ";
            notacionExcel +=  this.reemplazo.getNotacionExcel() + "; ";
            notacionExcel += ")";
            
            return notacionExcel;
        }
	}


	/* FECHAS */

	export class SysDate extends FormulaFactor implements IStringFactor {
        public isString: true = true;
        public categoria: string = "Funciones de fecha";
		public tipo: string = "SysDate";
		public descripcion: string = "Fecha actual";
		public valor: string = "SYSDATE";

		constructor() { super(); }
        
        public getNotacionExcel(): string { 
            let notacionExcel: string = "AHORA()"; 
            return notacionExcel;
        }
	}

	export class MonthsBetween extends FormulaParent implements IMathFactor {
        public isMath: true = true;
        public categoria: string = "Funciones de fecha";
		public tipo: string = "MonthsBetween";
        public descripcion: string = "Diferencia de meses";
        public hijos: IStringFactor[];
		public maxHijos: number = 2;

		public get desde(): IStringFactor { return this.hijos[0]; }
		public get hasta(): IStringFactor { return this.hijos[1]; }

		constructor(desde: FormulaFactor, hasta: FormulaFactor) {
			super(undefined);
			this.hijos[0] = desde && desde instanceof CampoIntegracion? desde : new NullLiteral();
			this.hijos[1] = hasta && hasta instanceof CampoIntegracion? hasta : new NullLiteral();

			this.actualizarReferenciasHijosAPadre();
		}

		public getNodoInvalido(): FormulaErrorNotifierWrapper {
            let superNodoInvalido = super.getNodoInvalido();
            if (superNodoInvalido) {
                return superNodoInvalido;
            }
			// else if (this.desde !instanceof CampoIntegracion) {
			// 	return {
            //         origen: this.desde,
            //         mensaje: "Fecha no válida."
            //     };
			// }
			// else if (this.hasta !instanceof CampoIntegracion) {
			// 	return {
            //         origen: this.hasta,
            //         mensaje: "Fecha no válida."
            //     };
			// }
			else {
				for (let i = 0; i < this.hijos.length; i++) {
					const nodoInvalido = this.hijos[i].getNodoInvalido();
					if (nodoInvalido != null) {
						return nodoInvalido;
					}
				}
			}
			this.desde.valido = true;
			this.hasta.valido = true;
			this.valido = true;
			return null;
		}
        
        public getNotacionExcel(): string { 
            let notacionExcel: string = "SIFECHA"; 

            notacionExcel += "(";
            notacionExcel += this.desde.getNotacionExcel() + "; ";
            notacionExcel += this.hasta.getNotacionExcel() + "; ";
            notacionExcel += "\"m\""
            notacionExcel += ")";
            
            return notacionExcel;
        }
	}

	export class DaysBetween extends FormulaParent implements IMathFactor {
        public isMath: true = true;
        public categoria: string = "Funciones de fecha";
		public tipo: string = "DaysBetween";
        public descripcion: string = "Diferencia de días";
        public hijos: IStringFactor[];
		public maxHijos: number = 2;

		public get desde(): IStringFactor { return this.hijos[0]; }
		public get hasta(): IStringFactor { return this.hijos[1]; }

		constructor(desde: FormulaFactor, hasta: FormulaFactor) {
			super(undefined);
			this.hijos[0] = desde && desde instanceof CampoIntegracion? desde : new NullLiteral();
			this.hijos[1] = hasta && hasta instanceof CampoIntegracion? hasta : new NullLiteral();

			this.actualizarReferenciasHijosAPadre();
		}

		public getNodoInvalido(): FormulaErrorNotifierWrapper {
            let superNodoInvalido = super.getNodoInvalido();
            if (superNodoInvalido) {
                return superNodoInvalido;
            }
			// else if (this.desde !instanceof CampoIntegracion) {
			// 	return {
            //         origen: this.desde,
            //         mensaje: "Fecha no válida."
            //     }; 
			// }
			// else if (this.hasta !instanceof CampoIntegracion) {
			// 	return {
            //         origen: this.hasta,
            //         mensaje: "Fecha no válida."
            //     };
			// }
			else {
				for (let i = 0; i < this.hijos.length; i++) {
					const nodoInvalido = this.hijos[i].getNodoInvalido();
					if (nodoInvalido != null) {
						return nodoInvalido;
					}
				}
			}
			this.desde.valido = true;
			this.hasta.valido = true;
			this.valido = true;
			return null;
		}
        
        public getNotacionExcel(): string { 
            let notacionExcel: string = "SIFECHA"; 

            notacionExcel += "(";
            notacionExcel += this.desde.getNotacionExcel() + "; ";
            notacionExcel += this.hasta.getNotacionExcel() + "; ";
            notacionExcel += "\"d\""
            notacionExcel += ")";
            
            return notacionExcel;
        }
	}

	export class AddMonths extends FormulaParent implements IStringFactor {
        public isString: true = true;
        public categoria: string = "Funciones de fecha";
		public tipo: string = "AddMonths";
        public descripcion: string = "Sumar meses";
        public hijos: IFormulaFactor[];
		public maxHijos: number = 2;

		public get fecha(): IFormulaFactor { return this.hijos[0]; }
		public get meses(): IFormulaFactor { return this.hijos[1]; }

		constructor(desde: FormulaFactor, meses: FormulaFactor) {
			super(undefined);
			this.hijos[0] = desde && desde instanceof CampoIntegracion? desde : new NullLiteral();
			this.hijos[1] = meses? meses : new NullLiteral();

			this.actualizarReferenciasHijosAPadre();
		}

		public getNodoInvalido(): FormulaErrorNotifierWrapper {
            let superNodoInvalido = super.getNodoInvalido();
            if (superNodoInvalido) {
                return superNodoInvalido;
            }
			// else if (this.fecha !instanceof CampoIntegracion) {
			// 	return {
            //         origen: this.fecha,
            //         mensaje: "Fecha no válida."
            //     }; 
			// }
			else {
                const mesesNotMath = !('isMath' in this.meses);
                if (mesesNotMath) {
                    return {
                        origen: this.meses,
                        mensaje: "Cantidad de meses a sumar inválida."
                    };
                }
                else {
                    for (let i = 0; i < this.hijos.length; i++) {
                        const nodoInvalido = this.hijos[i].getNodoInvalido();
                        if (nodoInvalido != null) {
                            return nodoInvalido;
                        }
                    }
                }
            }
			this.fecha.valido = true;
			this.meses.valido = true;
			this.valido = true;
			return null;
		}
        
        public getNotacionExcel(): string { 
            let notacionExcel: string = "FECHA.MES"; 

            notacionExcel += "(";
            notacionExcel += this.fecha.getNotacionExcel() + "; ";
            notacionExcel += this.meses.getNotacionExcel();
            notacionExcel += ")";
            
            return notacionExcel;
        }
	}

	export class AddDays extends FormulaParent implements IStringFactor {
        public isString: true = true;
        public categoria: string = "Funciones de fecha";
		public tipo: string = "AddDays";
        public descripcion: string = "Sumar días";
        public hijos: IFormulaFactor[];
		public maxHijos: number = 2;

		public get fecha(): IFormulaFactor { return this.hijos[0]; }
		public get dias(): IFormulaFactor { return this.hijos[1]; }

		constructor(desde: FormulaFactor, dias: FormulaFactor) {
			super(undefined);
			this.hijos[0] = desde && desde instanceof CampoIntegracion? desde : new NullLiteral();
			this.hijos[1] = dias? dias : new NullLiteral();

			this.actualizarReferenciasHijosAPadre();
		}

		public getNodoInvalido(): FormulaErrorNotifierWrapper {
            let superNodoInvalido = super.getNodoInvalido();
            if (superNodoInvalido) {
                return superNodoInvalido;
            }
			// else if (this.fecha !instanceof CampoIntegracion) {
			// 	return {
            //         origen: this.fecha,
            //         mensaje: "Fecha inválida."
            //     }; 
			// }
			else { 
                const diasNotMath = !('isMath' in this.dias);
                if (diasNotMath) {
                    return {
                        origen: this.dias,
                        mensaje: "La cantidad de días a sumar no es un número inválido."
                    };
                }
                else {
                    for (let i = 0; i < this.hijos.length; i++) {
                        const nodoInvalido = this.hijos[i].getNodoInvalido();
                        if (nodoInvalido != null) {
                            return nodoInvalido;
                        }
                    }
                }
            }
			this.fecha.valido = true;
			this.dias.valido = true;
			this.valido = true;
			return null;
		}
        
        public getNotacionExcel(): string { 
            let notacionExcel: string = ""; 

            notacionExcel += "(";
            notacionExcel += this.fecha.getNotacionExcel() + " + " + this.dias.getNotacionExcel();
            notacionExcel += ")";
            
            return notacionExcel;
        }
	}

	/* UF */

	export class ValorUFActual extends FormulaFactor implements IMathFactor, IStringFactor {
        public isMath: true = true;
        public isString: true = true;
        public categoria: string = "Funciones de UF";
		public tipo: string = "ValorUFActual";
		public descripcion: string = "UF actual";
		public valor: string = "UFActual";

		constructor() { super(); }
		
		public getNodoInvalido (): FormulaErrorNotifierWrapper { return null; }
        
        public getNotacionExcel(): string { 
            let notacionExcel: string = "VALOR_UF_ACTUAL()";
            return notacionExcel;
        }
	}

	export class ValorUFPrimerDia extends FormulaFactor implements IMathFactor, IStringFactor {
        public isMath: true = true;
        public isString: true = true;
        public categoria: string = "Funciones de UF";
		public tipo: string = "ValorUFPrimerDia";
		public descripcion: string = "UF en primer día mes";
		public valor: string = "UFPrimerDia";

		constructor() { super(); }
		
		public getNodoInvalido (): FormulaErrorNotifierWrapper { return null; }
        
        public getNotacionExcel(): string { 
            let notacionExcel: string = "VALOR_UF_PRIMERDIA()";
            return notacionExcel;
        }
	}

	export class ValorUFUltimoDia extends FormulaFactor implements IMathFactor, IStringFactor {
        public isMath: true = true;
        public isString: true = true;
        public categoria: string = "Funciones de UF";
		public tipo: string = "ValorUFUltimoDia";
		public descripcion: string = "UF en último día mes";
		public valor: string = "UFUltimoDia";

		constructor() { super(); }
		
		public getNodoInvalido (): FormulaErrorNotifierWrapper { return null; }
        
        public getNotacionExcel(): string { 
            let notacionExcel: string = "VALOR_UF_ULTIMODIA()";
            return notacionExcel;
        }
	}

	export class ValorUFDia extends FormulaParent implements IMathFactor, IStringFactor {
        public isMath: true = true;
        public isString: true = true;
        public categoria: string = "Funciones de UF";
		public tipo: string = "ValorUFDia";
		public descripcion: string = "UF en fecha";
        public hijos: IFormulaFactor[];
        public maxHijos = 1;
        
		public get fecha(): IFormulaFactor { return this.hijos[0]; }

		constructor(fecha: FormulaFactor) { 
            super(undefined);
            this.hijos[0] = fecha && fecha instanceof CampoIntegracion? fecha : new NullLiteral();

            this.actualizarReferenciasHijosAPadre();
        }
		
		public getNodoInvalido (): FormulaErrorNotifierWrapper {
            // if (this.fecha !instanceof CampoIntegracion) {
            //     return {
            //         origen: this.fecha,
            //         mensaje: "Fecha de UF inválida."
            //     };
            // }
            this.valido = true;
            return null;
        }
        
        public getNotacionExcel(): string { 
            let notacionExcel: string = "VALOR_UF_FECHA(";
            notacionExcel += this.fecha.getNotacionExcel();
            notacionExcel += ")";
            return notacionExcel;
        }
	}

	/* OTROS */

	export class NullLiteral extends FormulaFactor implements ILogicFactor, IMathFactor, IStringFactor {
        public isLogic: true = true;
        public isMath: true = true;
        public isString: true = true;
		public tipo: string = "NullLiteral";
		public descripcion: string = "Nulo";
		public valor: null = null;

		constructor() { super(); }
		
		public getNodoInvalido (): FormulaErrorNotifierWrapper { return null; }
        
        public getNotacionExcel(): string { 
            let notacionExcel: string = "null";
            return notacionExcel;
        }
    }
    

	export class FncEnriquecimiento extends FormulaParent implements IMathFactor, IStringFactor {
        public isMath: true = true;
        public isString: true = true;
        public categoria: string = "Otros";
		public tipo: string = "FncEnriquecimiento";
		public descripcion: string = "Enriquecimiento con valor";
        public hijos: IFormulaFactor[];
        public maxHijos = 3;
        public atrEnriquecimiento: AtributoEnriquecimiento;
        public atrEnriquecimientoId?: number;
        public socioId?: number;

        public get ctrlExterno(): IFormulaFactor { return this.hijos[0]; }
        public get poliza(): IFormulaFactor { return this.hijos[1]; }
        public get rut(): IFormulaFactor { return this.hijos[2]; }
        
		constructor(socioId: number, atribEnriq: AtributoEnriquecimiento) { 
            super(undefined); 
            this.hijos[0] = new NullLiteral();
            this.hijos[1] = new NullLiteral();
            this.hijos[2] = new NullLiteral();            

            this.actualizarReferenciasHijosAPadre();

            if (socioId) {
                this.socioId = socioId;
            }
            if (atribEnriq) {
                this.atrEnriquecimiento = atribEnriq;
            }

        }
		
		public getNodoInvalido (): FormulaErrorNotifierWrapper { 
            let superNodoInvalido = super.getNodoInvalido();
            if (superNodoInvalido) {
                return superNodoInvalido;
            } else if (!this.ctrlExterno.valido || this.ctrlExterno instanceof NullLiteral) {
                return {
                    origen: this.ctrlExterno,
                    mensaje: "El control externo debe poseer un valor válido."
                };
            }
			this.ctrlExterno.valido = true;
			this.valido = true;
            return null;
        }
        
        public getNotacionExcel(): string { 
            let notacionExcel: string = "ENRIQUECER";

            notacionExcel += "(";
            notacionExcel += this.ctrlExterno.getNotacionExcel();
            if (this.poliza.valido) {
                notacionExcel += "; " + this.poliza.getNotacionExcel();
            }
            if (this.rut.valido) {
                notacionExcel += "; " + this.rut.getNotacionExcel();
            }
            notacionExcel += ")";

            return notacionExcel;
        }
	}
    

    /* SIN USAR */

	// export class FncValidaRut extends FormulaFactor implements ILogicFactor {
    //     public isLogic: true = true;
	// 	public tipo: string = "FncValidaRut";
	// 	public descripcion: string = "Validar Rut";
	// 	public valor: FormulaFactor;

	// 	constructor(valor: FormulaFactor) {
	// 		super();
	// 		if (valor) { this.valor = valor; }
	// 	}
	
	// 	public getNodoInvalido(): FormulaErrorNotifierWrapper { 
	// 		if (!!!this.valor) {
	// 			return {
    //                 origen: this,
    //                 mensaje: "Error interno: nodo inválido."
    //             };
	// 		}
    //         this.valido = true;
    //         return this.valor.getNodoInvalido();
	// 	}
    // }

	// export class Min extends FormulaFactor implements IStringFactor, IMathFactor {
    //     public isString: true = true;
    //     public isMath: true = true;
	// 	public tipo: string = "Min";
	// 	public descripcion: string = "Valor mínimo";
	// 	public valor: FormulaFactor = null;
		
	// 	constructor(valor: FormulaFactor) {
	// 		super();
	// 		if (valor) {
	// 			this.valor = valor;
	// 			this.valor.padre = this;
	// 		}
	// 		else {
	// 			this.valor = null;
	// 		}
	// 	}
	
	// 	public getNodoInvalido(): FormulaErrorNotifierWrapper { 
	// 		if (!!!this.valor) {
	// 			return {
    //                 origen: this,
    //                 mensaje: "Valor no válido."
    //             };
	// 		}
	// 		else {
	// 			this.valido = true;
	// 			return this.valor.getNodoInvalido();
	// 		}
	// 	}
	// }

	// export class Max extends FormulaFactor implements IStringFactor, IMathFactor {
    //     public isString: true = true;
    //     public isMath: true = true;
	// 	public tipo: string = "Max";
	// 	public descripcion: string = "Valor máximo";
	// 	public valor: FormulaFactor;

	// 	constructor(valor: FormulaFactor) {
	// 		super();
	// 		if (valor) {
	// 			this.valor = valor;
	// 			this.valor.padre = this;
	// 		}
	// 		else {
	// 			this.valor = null;
	// 		}
	// 	}
	
	// 	public getNodoInvalido(): FormulaErrorNotifierWrapper { 
	// 		if (!!!this.valor) {
	// 			return { 
    //                 origen: this,
    //                 mensaje: "Valor no válido."
    //             };
    //         }
    //         this.valido = true;
	// 		return this.valor.getNodoInvalido();
	// 	}
	// }
	
}
