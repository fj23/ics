import { Plantilla } from '../Plantilla';
import { ArchivoEntrada } from './ArchivoEntrada';
import { UnionArchivosEntrada } from './UnionArchivosEntrada';
import { TipoPlantilla } from 'src/models/compartido/TipoPlantilla';
import { Socio } from '../../compartido/Socio';
import { Evento } from '../../compartido/Evento';
import { SistemaCore } from '../../compartido/SistemaCore';
import { TipoExtension } from '../../compartido/TipoExtension';
import { MapeoEntrada } from './MapeoEntrada';

export class PlantillaEntrada extends Plantilla {
    public id: number;
    public nombre: string;
    public version: string;
    public vigencia: string;
    public observaciones: string;
    public tipo: TipoPlantilla;
    public socio: Socio;
    public evento: Evento;
    public core: SistemaCore = null;
    public fechaCreacion: string;
    public formatoEntrada: TipoExtension;
    public archivos: ArchivoEntrada[];
    public uniones: UnionArchivosEntrada[];
    public columnasDestino: MapeoEntrada[];
}