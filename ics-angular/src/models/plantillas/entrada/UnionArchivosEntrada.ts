import { ColumnaMetadatos } from '../../compartido/ColumnaMetadatos';
import { ArchivoEntrada } from './ArchivoEntrada';

export class UnionArchivosEntrada {
    public archivo1: ArchivoEntrada;
    public columna1: ColumnaMetadatos;
    public archivo2: ArchivoEntrada;
    public columna2: ColumnaMetadatos;
}