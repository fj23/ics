export class ColumnaDestinoEntrada {
    public idAtributoUsuario: number;
    public tipoPersona: string;
    public nombreColumna: string;
    public flgCardinalidad: string;

    public idAtributoNegocio: number;
    public nombreColumnaNegocio: string;
    public nombreTablaDestino: string;
    public cardinalidadMaxima: number;
}